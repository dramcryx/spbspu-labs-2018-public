#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

using namespace agapov;

int main()
{
  try
  {
    Circle circ(6.9, {3.2, 3.3});
    Shape *circle = &circ;
    Rectangle rectang(60.0,70.0,{0.0,0.0});
    Shape *rectangle = &rectang;
    Triangle triang({2.0,2.0},{3.0,6.0},{4.0,1.0});
    Shape *triangle = &triang;

    std::cout << "Triangle: " << std::endl;
    triangle->info();
    rectangle_t limrectforTriangle = triangle->getFrameRect();
    std::cout << "Rectangle for Triangle Height: " << limrectforTriangle.height << " Width " << limrectforTriangle.width
    <<" x = " << limrectforTriangle.pos.x << " y = " << limrectforTriangle.pos.y << std::endl;

    std::cout << std::endl << "Circle: " << std::endl;
    circle->info();
    rectangle_t limrectforCircle = circle ->getFrameRect();
    std::cout << "Rectangle fo Circle Height: " << limrectforCircle.height << " Width " << limrectforCircle.width
    <<" x = " << limrectforCircle.pos.x << " y = " << limrectforCircle.pos.y << std::endl;

    std::cout <<std::endl << "Rectangle: " << std::endl;
    rectangle->info();
    rectangle_t limrectforRectangle = rectangle ->getFrameRect();
    std::cout << "Rectangle for Rectnagle Height: " << limrectforRectangle.height
    << " Widght " << limrectforRectangle.width
    <<" x = " << limrectforRectangle.pos.x << " y = " << limrectforRectangle.pos.y << std::endl;

    std::cout << std::endl << "Circle Area: " << circle ->getArea() << std::endl;
    std::cout << "Rectangle Area: " << rectangle ->getArea() << std::endl;
    std::cout << "Triangle Area: " << triangle ->getArea() << std::endl;

    triangle->move({1.0,1.0});
    rectangle->move({3.0,4.0});
    circle->move({4.0,6.0});

    std::cout << std::endl << "After move" << std::endl <<std::endl;

    std::cout << "Triangle: " << std::endl;
    triangle->info();
    limrectforTriangle = triangle->getFrameRect();
    std::cout << "Rectangle for Triangle Height: " << limrectforTriangle.height << " Width " << limrectforTriangle.width
    <<" x = " << limrectforTriangle.pos.x << " y = " << limrectforTriangle.pos.y << std::endl;

    std::cout << std::endl << "Circle: " << std::endl;
    circle->info();
    limrectforCircle = circle ->getFrameRect();
    std::cout << "Rectangle fo Circle Height: " << limrectforCircle.height << " Width " << limrectforCircle.width
    <<" x = " << limrectforCircle.pos.x << " y = " << limrectforCircle.pos.y << std::endl;

    std::cout <<std::endl << "Rectangle: " << std::endl;
    rectangle->info();
    limrectforRectangle = rectangle ->getFrameRect();
    std::cout << "Rectangle for Rectnagle Height: " << limrectforRectangle.height << " Width " << limrectforRectangle.width
    <<" x = " << limrectforRectangle.pos.x << " y = " << limrectforRectangle.pos.y << std::endl;

    std::cout << std::endl << "Circle Area: " << circle ->getArea() << std::endl;
    std::cout << "Rectangle Area: " << rectangle ->getArea() << std::endl;
    std::cout << "Triangle Area: " << triangle ->getArea() << std::endl;
  }
  catch (const std::invalid_argument &e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }


  return 0;
}
