#include <boost/test/unit_test.hpp>
#include "circle.hpp"
#include <cmath>
#include <stdexcept>

using namespace agapov;

BOOST_AUTO_TEST_SUITE(Circ)

BOOST_AUTO_TEST_CASE(circle_initialization_radius)
{
  BOOST_CHECK_THROW(Circle circ(-1, {3.2, 3.3}), std::invalid_argument );
}

BOOST_AUTO_TEST_CASE(circle_area)
{
  Circle circle(70.0,{0.0,0.0});
  double area = 70.0*70.0*M_PI;
  BOOST_CHECK_EQUAL(circle.getArea(), area);
}

BOOST_AUTO_TEST_CASE(circle_area_new_center)
{
  Circle circle(70.0,{1.0,1.0});
  double area = 70.0*70.0*M_PI;
  BOOST_CHECK_EQUAL(circle.getArea(), area);
}

BOOST_AUTO_TEST_CASE(circle_area_new_center2)
{
  Circle circle(60.0,{-1.0,-1.0});
  double area = 60.0*60.0*M_PI;
  BOOST_CHECK_EQUAL(circle.getArea(), area);
}

BOOST_AUTO_TEST_CASE(circle_rectangle_new_center_and_radius)
{
  Circle circ(2.0, {0.0, 0.0});
  rectangle_t chek = circ.getFrameRect();
  BOOST_CHECK_CLOSE(chek.height, 2.0*2.0 , 0.01 );
  BOOST_CHECK_CLOSE(chek.width, 2.0*2.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.x, 0.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.y, 0.0, 0.01 );
}

BOOST_AUTO_TEST_CASE(circle_rectangle_new_radius)
{
  Circle circ(1, {0.0, 0.0});
  rectangle_t chek = circ.getFrameRect();
  BOOST_CHECK_CLOSE(chek.height, 1.0*2.0 , 0.01 );
  BOOST_CHECK_CLOSE(chek.width, 1.0*2.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.x, 0.0, 0.01 );
  BOOST_CHECK_CLOSE(chek.pos.y, 0.0, 0.01 );
}

BOOST_AUTO_TEST_CASE(ciecle_move_pos)
{
  Circle circle(1.0,{0.0,0.0});
  circle.move({1.0,1.0});
  double check_radius = circle.getRadius();
  point_t check = circle.getCenter();
  BOOST_CHECK_CLOSE(check.x,1.0,0.01);
  BOOST_CHECK_CLOSE(check.y,1.0,0.01);
  BOOST_CHECK_CLOSE(check_radius,1.0,0.01);
}

BOOST_AUTO_TEST_CASE(ciecle_move_pos_2)
{
  Circle circle(1.0,{1.0,1.0});
  circle.move({6.0,6.0});
  double check_radius = circle.getRadius();
  point_t check = circle.getCenter();
  BOOST_CHECK_CLOSE(check.x,6.0,0.01);
  BOOST_CHECK_CLOSE(check.y,6.0,0.01);
  BOOST_CHECK_CLOSE(check_radius,1.0,0.01);
}

BOOST_AUTO_TEST_CASE(ciecle_move_pos_new_radius)
{
  Circle circle(8.0,{1.0,1.0});
  circle.move({6.0,6.0});
  double check_radius = circle.getRadius();
  point_t check = circle.getCenter();
  BOOST_CHECK_CLOSE(check.x,6.0,0.01);
  BOOST_CHECK_CLOSE(check.y,6.0,0.01);
  BOOST_CHECK_CLOSE(check_radius,8.0,0.01);
}

BOOST_AUTO_TEST_CASE(ciecle_move_dx)
{
  Circle circle(8.0,{1.0,1.0});
  circle.move(6.0,0.0);
  double check_radius = circle.getRadius();
  point_t check = circle.getCenter();
  BOOST_CHECK_CLOSE(check.x,7.0,0.01);
  BOOST_CHECK_CLOSE(check.y,1.0,0.01);
  BOOST_CHECK_CLOSE(check_radius,8.0,0.01);
}

BOOST_AUTO_TEST_CASE(ciecle_move_dy)
{
  Circle circle(8.0,{1.0,1.0});
  circle.move(0.0,6.0);
  double check_radius = circle.getRadius();
  point_t check = circle.getCenter();
  BOOST_CHECK_CLOSE(check.x,1.0,0.01);
  BOOST_CHECK_CLOSE(check.y,7.0,0.01);
  BOOST_CHECK_CLOSE(check_radius,8.0,0.01);
}

BOOST_AUTO_TEST_CASE(ciecle_move_dy_and_dx)
{
  Circle circle(8.0,{1.0,1.0});
  circle.move(5.0,8.0);
  double check_radius = circle.getRadius();
  point_t check = circle.getCenter();
  BOOST_CHECK_CLOSE(check.x,6.0,0.01);
  BOOST_CHECK_CLOSE(check.y,9.0,0.01);
  BOOST_CHECK_CLOSE(check_radius,8.0,0.01);
}

BOOST_AUTO_TEST_CASE(ciecle_move_dy_and_dx_1)
{
  Circle circle(8.0,{1.0,1.0});
  circle.move(8.0,8.0);
  double check_radius = circle.getRadius();
  point_t check = circle.getCenter();
  BOOST_CHECK_CLOSE(check.x,9.0,0.01);
  BOOST_CHECK_CLOSE(check.y,9.0,0.01);
  BOOST_CHECK_CLOSE(check_radius,8.0,0.01);
}

BOOST_AUTO_TEST_CASE(circle_scale)
{
  Circle circle(6.0,{0.0,0.0});
  double checkArea = circle.getArea();
  double coefficient = 5.0;
  bool condition = false;
  circle.scale(coefficient);
  if ((circle.getArea()/checkArea) == coefficient*coefficient)
  {
    condition = true;
  }
  BOOST_CHECK(condition);
}

BOOST_AUTO_TEST_CASE(circle_scale_2)
{
  Circle circle(4.0,{0.0,0.0});
  double checkArea = circle.getArea();
  double coefficient = 0.5;
  bool condition = false;
  circle.scale(coefficient);
  if ((circle.getArea()/checkArea) == coefficient*coefficient)
  {
    condition = true;
  }
  BOOST_CHECK(condition);
}

BOOST_AUTO_TEST_SUITE_END()
