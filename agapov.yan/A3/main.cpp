#include <iostream>
#include "composite-shape.hpp"

using namespace agapov;

int main()
{
  try
  {
  Circle circle(5.0, {5.0, 5.0});
  Triangle triangle1({3.0,5.0},{0.0,6.0},{3.0,3.0});

  CompositeShape compositeShape(&circle);
  compositeShape.addShape(&triangle1);
  }
  catch (const std::invalid_argument &e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }

  return 0;
}
