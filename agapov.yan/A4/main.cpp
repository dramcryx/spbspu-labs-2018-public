#include "matrix.hpp"
#include <iostream>
#include <stdexcept>

using namespace agapov;

int main()
{
  Circle circle1(3.0, {3.0 ,0.0});
  Circle circle2(5.0, {3.0, -5.0});
  Triangle triangle1({3.0, 5.0}, {0.0, 6.0}, {3.0, 3.0});
  Triangle triangle2({6.0, 6.0},{8.0, 8.0},{0.0, 0.0});

  Matrix matrix(&circle1);
  matrix.addElem(&circle2);
  matrix.addElem(&triangle1);
  matrix.addElem(&triangle2);

  for( size_t i = 0; i < matrix.getRows(); i++)
  {
    for (size_t j =0; j < matrix.getColumns(); j++)
    {
      try
      {
        if (&matrix.getElem(i, j) != nullptr)
        {
          std::cout << "In " << i << " line and " << j << " column next shape " << '\n';
          matrix.getElem(i, j).info();
        }
      }
      catch(std::out_of_range)
      {

      }
    }
  }

  return 0;
}
