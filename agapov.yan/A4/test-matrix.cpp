#include <boost/test/unit_test.hpp>
#include "matrix.hpp"
#include "triangle.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include <stdexcept>

using namespace agapov;

BOOST_AUTO_TEST_CASE(_matrix_1)
{
  Triangle triangle({6.0,6.0},{8.0,8.0},{0.0,0.0});
  Matrix matrix(&triangle);
  BOOST_CHECK(&matrix.getElem(0, 0) == &triangle);
  BOOST_CHECK_THROW(matrix.getElem(0, 1), std::out_of_range);
  BOOST_CHECK_THROW(matrix.getElem(1, 0), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(_matrix_2)
{
  Circle circle( 3.0, { 3.0,0.0 });
  Matrix matrix(&circle);

  BOOST_CHECK(&matrix.getElem(0, 0) == &circle);

  BOOST_CHECK_THROW(matrix.getElem(0, 1), std::out_of_range);
  BOOST_CHECK_THROW(matrix.getElem(1, 0), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(triangle_turn)
{
  Triangle triangle({ 3.0,1.0 }, { 6.0,1.0 }, { 6.0,7.0 });
  triangle.rotate(-90);
  rectangle_t frame_rect = { 6.0,3.0, { 6.0, 3.5 } };
  BOOST_CHECK(triangle.getFrameRect().height == frame_rect.height);
  BOOST_CHECK(triangle.getFrameRect().width == frame_rect.width);
}

BOOST_AUTO_TEST_CASE(triangle_turn_area)
{
  Triangle triangle({ 2.0,1.0 }, { 4.0,1.0 }, { 4.0,5.0 });
  double area = triangle.getArea();
  triangle.rotate(-45);
  BOOST_CHECK_CLOSE(area, triangle.getArea(), 0.001);
}

BOOST_AUTO_TEST_CASE(circle_turn)
{
  Circle cir( 7.0, { 2.0,2.0 });
  rectangle_t frame_rect = cir.getFrameRect();
  cir.rotate(-40);
  BOOST_CHECK(cir.getFrameRect().height == frame_rect.height);
  BOOST_CHECK(cir.getFrameRect().width == frame_rect.width);
}

BOOST_AUTO_TEST_CASE(circle_turn_area)
{
  Circle cir( 4.0, { 3.0,3.0 });
  double area = cir.getArea();
  cir.rotate(60);
  BOOST_CHECK_CLOSE(area, cir.getArea(), 0.001);
}

BOOST_AUTO_TEST_CASE(rectangle_turn)
{
  Rectangle rect( 5.0, 7.0, { 2.0,2.0 });
  rectangle_t frame_rect = rect.getFrameRect();
  rect.rotate(-90);
  BOOST_CHECK(rect.getFrameRect().height == frame_rect.width);
  BOOST_CHECK(rect.getFrameRect().width == frame_rect.height);
}

BOOST_AUTO_TEST_CASE(rectangle_turn_area)
{
  Rectangle rect( 5.0, 4.0, { 3.0,3.0 });
  double area = rect.getArea();
  rect.rotate(45);
  BOOST_CHECK_CLOSE(area, rect.getArea(), 0.001);
}

