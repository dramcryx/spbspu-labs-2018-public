#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

using namespace agapov;

CompositeShape::CompositeShape(Shape *shape):
  size_array_(1),
  composite_(new Shape*[1])
{
  if (shape == nullptr)
  {
     throw std::invalid_argument("Object not found!");
  }
  composite_[0]=shape;
}

CompositeShape::CompositeShape(CompositeShape&& shape):
  size_array_(shape.size_array_),
  composite_(std::move(shape.composite_))
{
}

CompositeShape::CompositeShape(const CompositeShape& shape):
  size_array_(shape.size_array_)
{
  composite_ = std::unique_ptr<Shape*[]>(new Shape*[size_array_]);
  for(size_t i = 0; i < size_array_; i++)
  {
    composite_[i]=shape.composite_[i];
  }
}

CompositeShape &CompositeShape::operator=(CompositeShape&& rhs)
{
  if (this == &rhs)
  {
    return *this;
  }
  size_array_ = rhs.size_array_;
  composite_ = std::unique_ptr<Shape*[]>(new Shape*[size_array_]);
  for (size_t i = 0; i < size_array_; i++)
  {
    composite_[i] = nullptr;
  }
  composite_.swap(rhs.composite_);
  return *this;
  
}

void CompositeShape::addShape(Shape *shape)
{
  if ( shape != nullptr )
  {
    size_array_++;
    std::unique_ptr <Shape*[]> new_array (new Shape*[size_array_]);
    for(size_t i = 0; i < size_array_-1; i++)
    {
      new_array[i] = composite_[i];
    }
    new_array[size_array_-1]=shape;
    composite_.swap(new_array);
  }
}

Shape& CompositeShape::operator[](size_t n) const
{
  if (n >= size_array_)
  {
    throw std::out_of_range("Index is out of range of array");
  }
  return *(composite_[n]);
}

double CompositeShape::getArea() const
{
  double area_for_all = 0.0;
  for(size_t i = 0; i < size_array_; i++ )
  {
    area_for_all+= composite_[i]->getArea();
  }
  return area_for_all;
}

rectangle_t CompositeShape::getFrameRect() const
{
  point_t pos1 = {0.0,0.0};
  point_t pos2 = {0.0,0.0};
  rectangle_t rect = composite_[0]->getFrameRect();
  double max_x =rect.pos.x + rect.width/2.0;
  double max_y =rect.pos.y + rect.height/2.0;
  double min_x =rect.pos.x - rect.width/2.0;
  double min_y =rect.pos.y - rect.height/2.0;
  for(size_t i=1; i < size_array_; i++)
  {
    rect = composite_[i]->getFrameRect();
    pos1 = {rect.pos.x + rect.width/2.0, rect.pos.y + rect.height/2.0};
    pos2 = {rect.pos.x - rect.width/2.0, rect.pos.y - rect.height/2.0};
    if (max_x < pos1.x)
    {
      max_x = pos1.x;
    }
    if (max_y < pos1.y)
    {
      max_y = pos1.y;
    }
    if (min_y > pos2.y)
    {
      min_y = pos2.y;
    }
    if (min_x > pos2.x)
    {
      min_x = pos2.x;
    }
  }
  return {max_x-min_x,max_y-min_y,(max_x+min_x)/2.0,(max_y+min_y)/2.0};
}

void CompositeShape::move(const double Ox, const double Oy)
{
  for(size_t i = 0; i < size_array_; i++)
  {
    composite_[i]->move(Ox,Oy);
  }
}

void CompositeShape::move(const point_t &poss)
{
  point_t center = getFrameRect().pos;
  for(size_t i=0; i<size_array_; i++)
  {
    composite_[i]->move(poss.x-center.x,poss.y-center.y);
  }
}

void CompositeShape::info() const
{
  for(size_t i=0; i < size_array_; i++)
  {
    composite_[i]->info();
  }
  rectangle_t rect=getFrameRect();
  std::cout << "Rectangle for composite Shape : " << "\n" << "Height: " << rect.height << "\n"
            << "Width: " << rect.width << "\n" << "Center: " << rect.pos.x << " " <<rect.pos.y << "\n";
}

void CompositeShape::scale(const double coefficient)
{
  point_t center = getFrameRect().pos;
  point_t for_move = {0.0,0.0};
  for(size_t i = 0; i < size_array_; i++)
  {
    for_move = composite_[i]->getFrameRect().pos;
    composite_[i]->move(center.x + (for_move.x-center.x)*coefficient, center.y + (for_move.y-center.y)*coefficient);
    composite_[i]->scale(coefficient);
  }
}

void CompositeShape::rotate(const double angle)
{
  point_t centre = getFrameRect().pos;
  const double sin_a = sin((angle * M_PI) / 180.0);
  const double cos_a = cos((angle * M_PI) / 180.0);
  for (size_t i = 0; i < size_array_; i++)
  {
    point_t centre_shape = composite_[i]->getFrameRect().pos;
    composite_[i]->move({ centre.x + (centre_shape.x - centre.x) * cos_a
                                   - (centre_shape.y - centre.y) * sin_a,
                          centre.y + (centre_shape.y - centre.y) * cos_a
                                   + (centre_shape.x - centre.x) * sin_a });
    composite_[i]->rotate(angle);
  }
}

size_t CompositeShape::getSize() const
{
  return size_array_;
}
