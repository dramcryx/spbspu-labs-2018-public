#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"
#include "triangle.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

namespace agapov
{
  class CompositeShape:public Shape
  {
  public:
    CompositeShape(Shape *shape);
    CompositeShape(CompositeShape&& shape); 
    CompositeShape(const CompositeShape& shape); 
    CompositeShape &operator=(CompositeShape&& rhs);

    void addShape(Shape *shape);
    Shape& operator[](size_t) const;
    
    double getArea() const;
    rectangle_t getFrameRect() const override;
    void move(const double Ox, const double Oy) override;
    void move(const point_t& poss) override;
    void info() const override;
    void scale(const double coefficient) override;
    void rotate(const double angle) override;
    size_t getSize() const;

  private:
    size_t size_array_;
    std::unique_ptr<Shape*[]> composite_;

  };
}

#endif
