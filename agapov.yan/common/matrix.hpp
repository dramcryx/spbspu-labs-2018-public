#ifndef MATRIX_HPP_
#define MATRIX_HPP_

#include "composite-shape.hpp"
#include "shape.hpp"
#include <memory>

namespace agapov
{
  class Matrix
  {
  public:
    Matrix(Shape *shape);
    Matrix(const Matrix& matrix);
    Matrix(Matrix&& matrix);
    size_t getRows() const;
    size_t getColumns() const;
    void addElem(Shape *shape);
    Shape& getElem(size_t columnm, size_t line);
    bool shapesIntersect(Shape *shape1, Shape *shape2) const;

  private:
    size_t rows_;
    size_t columns_;
    std::unique_ptr<Shape*[]> matrix_;

  };
}

#endif
