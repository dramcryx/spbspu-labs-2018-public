#include "rectangle.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

using namespace agapov;

Rectangle::Rectangle(const double height, const double width, const point_t &pos):
  height_(height),
  width_(width),
  center_(pos)
{ 
  if (width <= 0.0)
  {
    throw std::invalid_argument("Rectangle's width must be more than 0");
  }
  if (height <= 0.0)
  {
    throw std::invalid_argument("Rectangle's height must be more than 0");
  }

  angle_ = 0;
}

double Rectangle::getArea() const
{
  return height_*width_;
}

rectangle_t Rectangle::getFrameRect() const
{
  const double alpha = angle_*M_PI / 180;

  const double width = height_ * fabs(sin(alpha)) + width_ * fabs(cos(alpha));
  const double height = height_  * fabs(cos(alpha)) + width_ * fabs(sin(alpha));

  return {width, height, center_};
}
void Rectangle::move(const point_t &pos)
{
  center_.x = pos.x;
  center_.y = pos.y; 
}

void Rectangle::move(const double dx, const double dy)
{
  center_ = {center_.x + dx, center_.y + dy};
}

void Rectangle::scale(const double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Coefficient must be more than 0!" );
  }
  else
  {
    width_ = width_ * coefficient;
    height_ = height_ * coefficient;
  }
 
}

void Rectangle::info() const
{
  std::cout << "Rectangle " << '\n' << "Width = " << getWidth()
            << '\n' << "Height = " << getHeight() << '\n'
            <<"Center x: " << getFrameRect().pos.x << "y: " << getFrameRect().pos.y << '\n';
}

double Rectangle::getWidth() const
{
  return width_;
}

double Rectangle::getHeight() const
{
  return height_;
}

point_t Rectangle::getCenter() const
{
  return center_;
}
void Rectangle::rotate(const double alpha)
{
  angle_ += alpha;
}
