#ifndef RECTANGLE_HPP_
#define RECTANGLE_HPP_

#include "shape.hpp"

namespace agapov
{
  class Rectangle:public Shape
  {
  public:
    Rectangle(const double width, const double height, const point_t &pos);
    double getArea() const;
    rectangle_t getFrameRect() const ;
    void move(const point_t &pos);
    void move(const double Ox, const double Oy);
    void info() const;
    void scale(const double coefficient);
    double getHeight() const;
    double getWidth() const;
    point_t getCenter() const;
    void rotate(const double angle);

  private:
    double height_;
    double width_;
    point_t center_;
    double angle_;

  };
}

#endif
