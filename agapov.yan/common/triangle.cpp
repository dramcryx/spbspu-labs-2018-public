#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <stdexcept>
#include <cmath>

using namespace agapov;

Triangle::Triangle(const point_t &a, const point_t &b, const point_t &c):
  a_(a),
  b_(b),
  c_(c)
{
  if ((a.x == b.x)&&(a.y == b.y))
  {
    throw std::invalid_argument("Dots is are equal");
  }

  if ((a.x == c.x)&&(a.y == c.y))
  {
    throw std::invalid_argument("Dots is are equal");
  }

  if ((c.x == b.x)&&(c.y == b.y))
  {
    throw std::invalid_argument("Dots is are equal");
  }
}

double Triangle::getArea() const
{
  return fabs(((a_.x - c_.x) * (b_.y - c_.y) - (b_.x - c_.x) * (a_.y - c_.y))) * 0.5;
}

rectangle_t Triangle::getFrameRect() const
{
  double maxY = a_.y > b_.y ? (a_.y > c_.y ? a_.y : c_.y) : (b_.y > c_.y ? b_.y : c_.y);
  double minY = a_.y < b_.y ? (a_.y < c_.y ? a_.y : c_.y) : (b_.y < c_.y ? b_.y : c_.y);
  double maxX = a_.x > b_.x ? (a_.x > c_.x ? a_.x : c_.x) : (b_.x > c_.x ? b_.x : c_.x);
  double minX = a_.x < b_.x ? (a_.x < c_.x ? a_.x : c_.x) : (b_.x < c_.x ? b_.x : c_.x);
  return {maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2}};
}

void Triangle::move(const point_t &pos)
{
  move(pos.x - (a_.x+b_.x+c_.x) / 3.0, pos.y - (a_.y+b_.y+c_.y) / 3.0);
}

void Triangle::move(const double Ox, const double Oy)
{
  a_ = {a_.x + Ox, a_.y + Oy};
  b_ = {b_.x + Ox, b_.y + Oy};
  c_ = {c_.x + Ox, c_.y + Oy};
}

void Triangle::info() const 
{
  std::cout << "A: x = " << a_.x << " y = "<< a_.y << std::endl;
  std::cout << "B: x = " << b_.x << " y = "<< b_.y << std::endl;
  std::cout << "C: x = " << c_.x << " y = "<< c_.y << std::endl;
}

void Triangle::scale(const double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Coefficient must to be more than 0!");
  }
  else
  {
    point_t center = {(a_.x+b_.x+c_.x) / 3.0, ( a_.y+b_.y+c_.y) / 3.0};
    a_ = {center.x + (a_.x - center.x ) * coefficient,
          center.y + (a_.y - center.y ) * coefficient};
    b_ = {center.x + (b_.x - center.x ) * coefficient,
          center.y + (b_.y - center.y ) * coefficient};
    c_ = {center.x + (c_.x - center.x ) * coefficient,
          center.y + (c_.y - center.y ) * coefficient};
  }
}

point_t Triangle::getA() const
{
  return a_;
}

point_t Triangle::getB() const 
{
  return b_;
}

point_t Triangle::getC() const
{
  return c_;
}

void Triangle::rotate(double angle)
{
  point_t centre = getFrameRect().pos;
  angle = (angle * M_PI) / 180.0;
  const double sin_a = sin(angle);
  const double cos_a = cos(angle);
  a_ = { centre.x + (a_.x - centre.x) * cos_a - (a_.y - centre.y) * sin_a,
         centre.y + (a_.y - centre.y) * cos_a + (a_.x - centre.x) * sin_a };
  b_ = { centre.x + (b_.x - centre.x) * cos_a - (b_.y - centre.y) * sin_a,
         centre.y + (b_.y - centre.y) * cos_a + (b_.x - centre.x) * sin_a };
  c_ = { centre.x + (c_.x - centre.x) * cos_a - (c_.y - centre.y) * sin_a,
         centre.y + (c_.y - centre.y) * cos_a + (c_.x - centre.x) * sin_a };
}
