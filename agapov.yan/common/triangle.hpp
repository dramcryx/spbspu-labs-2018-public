#ifndef TRIANGLE_HPP_
#define TRIANGLE_HPP_

#include "shape.hpp"

namespace agapov
{
  class Triangle: public Shape 
  {
  public:
    Triangle(const point_t &a, const point_t &b, const point_t &c);
    double getArea() const;
    rectangle_t getFrameRect() const;
    void move(const point_t &pos);
    void move(const double Ox,const double Oy);
    void info() const;
    void scale(const double coefficient);
    point_t getA() const;
    point_t getB() const;
    point_t getC() const;
    void rotate(const double angle);

  private:
    point_t a_,b_,c_;
  };
}

#endif 
