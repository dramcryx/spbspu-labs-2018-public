#include <iostream>
#include <memory>

#include "shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace aknazarova;

void printInfo(const std::shared_ptr < Shape > shape)
{
  std::cout << "Area:" << shape->getArea() << std::endl;
  std::cout << "Angle:" << shape->getAngle() << std::endl;
  const rectangle_t FrameRect = shape->getFrameRect();
  std::cout << "GetFrame rectangle:" << std::endl;
  std::cout << "Position centre:" << "(" << FrameRect.pos.x << " ; " << FrameRect.pos.y << ")" << std::endl;
  std::cout << "Width, height:" << FrameRect.width << ", " << FrameRect.height << std::endl << std::endl;
}

void printInfo(const Shape & shape)
{
  std::cout << "Area:" << shape.getArea() << std::endl;
  std::cout << "Angle:" << shape.getAngle() << std::endl;
  const rectangle_t FrameRect = shape.getFrameRect();
  std::cout << "GetFrame rectangle:" << std::endl;
  std::cout << "Position centre:" << "(" << FrameRect.pos.x << " ; " << FrameRect.pos.y << ")" << std::endl;
  std::cout << "Width, height:" << FrameRect.width << ", " << FrameRect.height << std::endl << std::endl;
}

int main()
{
  try
  {
    Circle circ ({20.0, 0.0}, 10.0);
    std::cout << "Parametrs circle before rotate:" << std::endl;
    printInfo(circ);
    circ.rotate(90);
    std::cout << "Paramentrs circle after rotation (90 degrees):" << std::endl;
    printInfo(circ);

    Rectangle rect ({ 0.0, 0.0 }, 40.0, 20.0);
    std::cout << "Parametrs rectangle before rotation:" << std::endl;
    printInfo(rect);
    rect.rotate(90);
    std::cout << "Paramentrs rectangle after rotation (90 degrees):" << std::endl;
    printInfo(rect);

    std::shared_ptr < Shape > circPtr = std::make_shared < Circle > (circ);
    std::shared_ptr < Shape > rectPtr = std::make_shared < Rectangle > (rect);

    CompositeShape shape(circPtr);
    std::cout << "Composite Shape = Circle:" << std::endl;
    printInfo(shape);
    shape.addShape(rectPtr);
    std::cout << "Composite Shape = Circle + Rectangle:" << std::endl;
    printInfo(shape);
    shape.rotate(90);
    std::cout << "Composite Shape after rotation (90 degrees):" << std::endl;
    printInfo(shape);

    Circle circM1 ({ -10.0, 0.0 }, 10.0);
    Circle circM2 ({ 40.0, 30.0 }, 20.0);
    Rectangle rectM1 ({ 20.0, 30.0 }, 20.0, 40.0);
    Rectangle rectM2 ({ 30.0, 0.0 }, 20.0, 40.0);

    std::shared_ptr < Shape > circPtrM1 = std::make_shared < Circle > (circM1);
    std::shared_ptr < Shape > circPtrM2 = std::make_shared < Circle > (circM2);
    std::shared_ptr < Shape > rectPtrM1 = std::make_shared < Rectangle > (rectM1);
    std::shared_ptr < Shape > rectPtrM2 = std::make_shared < Rectangle > (rectM2);

    Matrix matrix(circPtrM1);
    matrix.addShape(rectPtrM1);
    matrix.addShape(rectPtrM2);
    matrix.addShape(circPtrM2);

    std::unique_ptr < std::shared_ptr < Shape >[] > level0 = matrix[0];
    std::unique_ptr < std::shared_ptr < Shape >[] > level1 = matrix[1];
    std::unique_ptr < std::shared_ptr < Shape >[] > level2 = matrix[2];
    
    std::cout << "Matrix" << std::endl;

    if (level0[0] == circPtrM1)
    {
      std::cout << "level 0, element 0 - circle 1" << std::endl;
    }
    if (level0[1] == rectPtrM1)
    {
      std::cout << "Level 0, element 1 - rectangle 1" << std::endl;
    }
    if (level1[0] == rectPtrM2)
    {
      std::cout << "Level 1, element 0 - rectangle 2" << std::endl;
    }
    if (level1[1] == nullptr)
    {
      std::cout << "Level 1, element 1 - nullptr" << std::endl;
    }
    if (level2[0] == circPtrM2)
    {
      std::cout << "Level 2, element 0 - circle 2" << std::endl;
    }
    if (level2[1] == nullptr)
    {
      std::cout << "Level 2, element 1 - nullptr" << std::endl;
    }
  }
  catch(std::invalid_argument &e)
  {
    std::cerr << e.what() << std::endl;
  }
  catch(std::out_of_range &e)
  {
    std::cerr << e.what() << std::endl;
  }
  return 0;
}
