#define BOOST_TEST_MODULE MAIN

#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <stdexcept>
#include <cmath>
#include <memory>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace aknazarova;

const double EPSI = 0.0001;

BOOST_AUTO_TEST_SUITE(RectangleTests)
  BOOST_AUTO_TEST_CASE (MoveToPoint)
  {
    Rectangle rect ({30,20}, 60, 40);
    rect.move(15,23);
    BOOST_CHECK_EQUAL (rect.getFrameRect ().height, 40);
    BOOST_CHECK_EQUAL (rect.getFrameRect ().width, 60);
    BOOST_CHECK_CLOSE_FRACTION (rect.getFrameRect().pos.x, 45, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (rect.getFrameRect().pos.y, 43, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (rect.getArea (), 60 * 40, EPSI);
  }
  
  BOOST_AUTO_TEST_CASE (MoveOnPoint)
  {
    Rectangle rect ({30,20}, 60, 40);
    rect.move({15,23});
    BOOST_CHECK_EQUAL (rect.getFrameRect().height, 40);
    BOOST_CHECK_EQUAL (rect.getFrameRect().width, 60);
    BOOST_CHECK_CLOSE_FRACTION (rect.getFrameRect().pos.x, 15, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (rect.getFrameRect().pos.y, 23, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (rect.getArea (), 60 * 40, EPSI);
  }
  
  BOOST_AUTO_TEST_CASE (Change_Area_Scale)
  {
    Rectangle rect ({30,20}, 60, 40);
    rect.scale(2);
    BOOST_CHECK_CLOSE_FRACTION (rect.getArea (), 60 * 40*2*2, EPSI);
  }
  
  BOOST_AUTO_TEST_CASE (Rotate)
  {
    Rectangle rect ({30,20}, 60, 40);
    rect.rotate(210.0);
    BOOST_CHECK_CLOSE_FRACTION(rect.getAngle(), 210.0, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (rect.getFrameRect().height, 40, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (rect.getFrameRect().width, 60, EPSI);
    BOOST_CHECK_EQUAL (rect.getFrameRect().pos.x, 30);
    BOOST_CHECK_EQUAL (rect.getFrameRect().pos.y, 20);
    BOOST_CHECK_CLOSE_FRACTION (rect.getArea (), 60 * 40, EPSI);
  }
  
  BOOST_AUTO_TEST_CASE(Invalid_Constructor_Parametr)
  {
    BOOST_CHECK_THROW (Rectangle ({1, 2}, -10.2, -15.1), std::invalid_argument);
  }
  
  BOOST_AUTO_TEST_CASE (invalid_scale_parameter)
  {
    Rectangle rect ({5.2, 16}, 32, 13.2);
  
    BOOST_CHECK_THROW (rect.scale (-15), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CircleTests)
  BOOST_AUTO_TEST_CASE (MoveToPoint)
  {
    Circle circ ({30,20}, 20);
    circ.move(15,23);
    BOOST_CHECK_EQUAL (circ.getFrameRect ().height, 40);
    BOOST_CHECK_CLOSE_FRACTION (circ.getFrameRect().pos.x, 45, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (circ.getFrameRect().pos.y, 43, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (circ.getArea (), 20*20*M_PI, EPSI);
  }
  
  BOOST_AUTO_TEST_CASE (MoveOnPoint)
  {
    Circle circ ({30,20}, 20);
    circ.move({15,23});
    BOOST_CHECK_EQUAL (circ.getFrameRect ().height, 40);
    BOOST_CHECK_CLOSE_FRACTION (circ.getFrameRect().pos.x, 15, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (circ.getFrameRect().pos.y, 23, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (circ.getArea (), 20*20*M_PI, EPSI);
  }
  
  BOOST_AUTO_TEST_CASE (Change_Area_Scale)
  {
    Circle circ ({30,20}, 20);
    circ.scale(2);
    BOOST_CHECK_CLOSE_FRACTION (circ.getArea (), 20*20*2*2*M_PI, EPSI);
  }
  
  BOOST_AUTO_TEST_CASE (Rotate)
  {
    Circle circ ({30,20}, 20);
    circ.rotate(210.0);
    BOOST_CHECK_CLOSE_FRACTION(circ.getAngle(), 210.0, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (circ.getFrameRect ().height, 40, EPSI);
    BOOST_CHECK_EQUAL (circ.getFrameRect().pos.x, 30);
    BOOST_CHECK_EQUAL (circ.getFrameRect().pos.y, 20);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Constructor_Parametr)
  {
    BOOST_CHECK_THROW (Circle ({1, 2}, -15.1), std::invalid_argument);
  }
  
  BOOST_AUTO_TEST_CASE (invalid_scale_parameter)
  {
    Circle circ ({5.2, 16}, 13.2);
  
    BOOST_CHECK_THROW (circ.scale (-15), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)
  BOOST_AUTO_TEST_CASE(Move)
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle ({{20.0, 20.0}, 60.0, 40.0}));
    CompositeShape shape(rectPtr);
    
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle ({0.0,-10.0},10.0));
    shape.addShape(circPtr);
    
    double area = shape.getArea();
    
    shape.move(15.0, 5.0);
    
    BOOST_CHECK_CLOSE_FRACTION(area, shape.getArea(), EPSI);
    BOOST_CHECK_CLOSE_FRACTION(60, shape.getFrameRect().width, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(60, shape.getFrameRect().height, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(35.0, shape.getFrameRect().pos.x, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(15.0, shape.getFrameRect().pos.y, EPSI);
  }
  
  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle ({{20.0, 20.0}, 60.0, 40.0}));
    CompositeShape shape(rectPtr);
    
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle ({0.0, -10.0}, 10.0));
    shape.addShape(circPtr);
    
    double area = shape.getArea();
    
    shape.move({15.0, 5.0});
    
    BOOST_CHECK_CLOSE_FRACTION(area, shape.getArea(), EPSI);
    BOOST_CHECK_CLOSE_FRACTION(60, shape.getFrameRect().width, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(60, shape.getFrameRect().height, EPSI);
    BOOST_CHECK_EQUAL(15.0,shape.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(5.0,shape.getFrameRect().pos.y);
  }
  
  BOOST_AUTO_TEST_CASE(RotateTest)
  {
    std::shared_ptr < Shape > rectPtr = std::make_shared < Rectangle > (Rectangle ({{20.0,20.0}, 60.0, 40.0}));
    CompositeShape shape(rectPtr);

    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle ({0.0, -10.0}, 10.0));
    shape.addShape(circPtr);
    
    shape.rotate(210.0);
    
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().pos.x, 25.0, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().pos.y, 14.33, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getAngle(), 210.0, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().width, 60.0, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().height, 65.98, EPSI);
    
  }

  BOOST_AUTO_TEST_CASE(CopyInfTest)
  {
    Circle circ({0, 0}, 20);
    Rectangle rect({20, 60},80,40);
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle> (circ);
    std::shared_ptr <Shape> rectPtr = std::make_shared <Rectangle> (rect);
    CompositeShape shape(circPtr);
    shape.addShape(rectPtr);
    CompositeShape shape1 = shape;
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().height, 100, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().width, 80, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().pos.x, 20, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().pos.y, 30, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().height, shape1.getFrameRect().height, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().width, shape1.getFrameRect().width, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().pos.x, shape1.getFrameRect().pos.x, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().pos.y, shape1.getFrameRect().pos.y, EPSI);
  }

  BOOST_AUTO_TEST_CASE(CopyInfOperatorTest)
  {
    Circle circ({0, 0}, 20);
    Rectangle rect({20, 60}, 60, 40);
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle> (circ);
    std::shared_ptr <Shape> rectPtr = std::make_shared <Rectangle> (rect);
    CompositeShape shape(circPtr);
    shape.addShape(rectPtr);
    CompositeShape shape1(rectPtr);
    shape = shape1;
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().height, 40, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().width, 60, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().pos.x, 20, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().pos.y, 60, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().height, shape1.getFrameRect().height, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().width, shape1.getFrameRect().width, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().pos.x, shape1.getFrameRect().pos.x, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().pos.y, shape1.getFrameRect().pos.y, EPSI);
  }
 
 BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle({ 0,0 },20));
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle({ 40,0 },40,40));
    CompositeShape shape(circPtr);
    shape.addShape(rectPtr);
    CompositeShape shape1(rectPtr);
    shape1 = std::move(shape);
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().width, 80 , EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().height, 40, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().pos.x, 20 , EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().pos.y, 0, EPSI);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle({ 0,0 },20));
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle({ 40,0 },40,40));
    CompositeShape shape(circPtr);
    shape.addShape(rectPtr);
    CompositeShape shape1(std::move(shape));
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().width, 80 , EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().height, 40 , EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().pos.x, 20, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape1.getFrameRect().pos.y, 0, EPSI);
  }

  BOOST_AUTO_TEST_CASE(AreaScaling)
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle ({{20.0, 20.0}, 60.0, 40.0}));
    CompositeShape shape(rectPtr);
    
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle ({0.0, -10.0}, 10.0));
    shape.addShape(circPtr);
    
    double area = shape.getArea();
   
    shape.scale(0.5);
    
    BOOST_CHECK_CLOSE_FRACTION(shape.getArea(), area * 0.5 * 0.5, EPSI);
  }
  
  BOOST_AUTO_TEST_CASE(DeleteShapes)
  {
    Circle circ({0, 0}, 10);
    Rectangle rect({10, 20}, 40, 20);
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle> (circ);
    std::shared_ptr <Shape> rectPtr = std::make_shared <Rectangle> (rect);
    CompositeShape shape(circPtr);
    shape.addShape(rectPtr);
    shape.deleteShapes();
    BOOST_REQUIRE_EQUAL(shape.getSize(), 0);
    BOOST_CHECK_CLOSE_FRACTION(shape.getArea(), 0, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().pos.x, 0, EPSI);
    BOOST_CHECK_CLOSE_FRACTION(shape.getFrameRect().pos.y, 0, EPSI);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentOfConstructor)
  {
    std::shared_ptr<Shape> circPtr = nullptr;
    BOOST_CHECK_THROW(CompositeShape shape(circPtr), std::invalid_argument);
  }
  
  BOOST_AUTO_TEST_CASE(InvalidArgumentOfAddedElement)
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle ({{20.0, 10.0}, 20.0, 40.0}));
    CompositeShape shape(rectPtr);
    
    std::shared_ptr<Shape> circPtr = nullptr;
    BOOST_CHECK_THROW(shape.addShape(circPtr), std::invalid_argument);
  }
  
  BOOST_AUTO_TEST_CASE(InvalidArgumentOfScaling)
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle ({{20.0, 10.0}, 20.0, 40.0}));
    CompositeShape shape(rectPtr);
    
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle ({40.0, 80.0}, 10.0));
    shape.addShape(circPtr);
    BOOST_CHECK_THROW(shape.scale(-2.0), std::invalid_argument);
  }
  
  BOOST_AUTO_TEST_CASE(GeneralParameters)
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle ({{20.0, 10.0}, 20.0, 40.0}));
    CompositeShape shape(rectPtr);
    BOOST_REQUIRE_EQUAL(rectPtr -> getFrameRect().width, shape.getFrameRect().width);
    BOOST_REQUIRE_EQUAL(rectPtr -> getFrameRect().height, shape.getFrameRect().height);
    BOOST_REQUIRE_EQUAL(rectPtr -> getFrameRect().pos.x, shape.getFrameRect().pos.x);
    BOOST_REQUIRE_EQUAL(rectPtr -> getFrameRect().pos.y, shape.getFrameRect().pos.y);
  }
  
  BOOST_AUTO_TEST_CASE(EmptyCompositeShapeWhenRemovingTest)
  {
    Circle circ({0, 10}, 20);
    Rectangle rect({10, 20}, 40, 20);
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle> (circ);
    std::shared_ptr <Shape> rectPtr = std::make_shared <Rectangle> (rect);
    CompositeShape shape(circPtr);
    shape.addShape(rectPtr);
    shape.deleteShapes();
    BOOST_CHECK_THROW(shape.removeShape(1), std::out_of_range);
  }
  
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTests)
  BOOST_AUTO_TEST_CASE(MatrixConstructorTest)
  {
    Circle circM( { -20.0, -20.0 },20.0);
    Rectangle rectM1({-20.0, 0.0},20,20);
    Rectangle rectM2({10.0, 10.0},60,30);
    Rectangle rectM3({30.0, 10.0},20,40);
    Rectangle rectM4({30.0, 30.0},40,40);
    
    std::shared_ptr<Shape> circPtrM = std::make_shared<Circle>(circM);
    std::shared_ptr<Shape> rectPtrM1 = std::make_shared<Rectangle>(rectM1);
    std::shared_ptr<Shape> rectPtrM2 = std::make_shared<Rectangle>(rectM2);
    std::shared_ptr<Shape> rectPtrM3 = std::make_shared<Rectangle>(rectM3);
    std::shared_ptr<Shape> rectPtrM4 = std::make_shared<Rectangle>(rectM4);

    Matrix matrix(circPtrM);
    matrix.addShape(rectPtrM1);
    matrix.addShape(rectPtrM2);
    matrix.addShape(rectPtrM3);
    matrix.addShape(rectPtrM4);

    std::unique_ptr<std::shared_ptr<Shape>[]> level1 = matrix[0];
    std::unique_ptr<std::shared_ptr<Shape>[]> level2 = matrix[1];
    std::unique_ptr<std::shared_ptr<Shape>[]> level3 = matrix[2];

    BOOST_CHECK(level1[0] == circPtrM);
    BOOST_CHECK(level1[1] == rectPtrM3);
    BOOST_CHECK(level2[0] == rectPtrM1);
    BOOST_CHECK(level2[1] == rectPtrM4);
    BOOST_CHECK(level3[0] == rectPtrM2);

    BOOST_CHECK_CLOSE_FRACTION (level1[0]->getFrameRect().pos.x, -20, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (level1[0]->getFrameRect().pos.y, -20, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (level1[1]->getFrameRect().pos.x, 30, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (level1[1]->getFrameRect().pos.y, 10, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (level2[0]->getFrameRect().pos.x, -20, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (level2[0]->getFrameRect().pos.y, 0, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (level2[1]->getFrameRect().pos.x, 30, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (level2[1]->getFrameRect().pos.y, 30, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (level3[0]->getFrameRect().pos.x, 10, EPSI);
    BOOST_CHECK_CLOSE_FRACTION (level3[0]->getFrameRect().pos.y, 10, EPSI);
  }
  
  BOOST_AUTO_TEST_CASE(CopyInformationConstructorTest)
  {
    Circle circM({20,20}, 20);
    Rectangle rectM1({40,40}, 40, 40);
    Rectangle rectM2({-20,-25}, 40, 50);
    std::shared_ptr <Shape> circPtrM = std::make_shared <Circle> (circM);
    std::shared_ptr <Shape> rectPtrM1 = std::make_shared <Rectangle> (rectM1);
    std::shared_ptr <Shape> rectPtrM2 = std::make_shared <Rectangle> (rectM2);
    Matrix matrix(circPtrM);
    matrix.addShape(rectPtrM1);
    matrix.addShape(rectPtrM2);
    Matrix matrix1(matrix);
    std::unique_ptr <std::shared_ptr<Shape>[] > new1_level1 = matrix1[0];
    std::unique_ptr <std::shared_ptr<Shape>[] > new1_level2 = matrix1[1];
    BOOST_CHECK(new1_level1[0] == circPtrM);
    BOOST_CHECK(new1_level2[0] == rectPtrM1);
    std::unique_ptr <std::shared_ptr<Shape>[] > new_level1 = matrix[0];
    std::unique_ptr <std::shared_ptr<Shape>[] > new_level2 = matrix[1];
    BOOST_CHECK(new1_level1[0] == new_level1[0]);
    BOOST_CHECK(new1_level2[0] == new_level2[0]);
  }

  BOOST_AUTO_TEST_CASE(CopyInformationOperatorTest)
  {
    Circle circM({50,0}, 20);
    Rectangle rectM1({10,20}, 40, 80);
    Rectangle rectM2({70,70}, 40, 20);
    std::shared_ptr <Shape> circPtrM = std::make_shared <Circle> (circM);
    std::shared_ptr <Shape> rectPtrM1 = std::make_shared <Rectangle> (rectM1);
    std::shared_ptr <Shape> rectPtrM2 = std::make_shared <Rectangle> (rectM2);;
    Matrix matrix(circPtrM);
    matrix.addShape(rectPtrM1);
    Matrix matrix1(rectPtrM2);
    matrix1 = matrix;
    std::unique_ptr <std::shared_ptr<Shape>[] > new1_level1 = matrix1[0];
    std::unique_ptr <std::shared_ptr<Shape>[] > new1_level2 = matrix1[1];
    BOOST_CHECK(new1_level1[0] == circPtrM);
    BOOST_CHECK(new1_level2[0] == rectPtrM1);
  }

  BOOST_AUTO_TEST_CASE(MoveInformationConstructorTest)
  {
    Circle circM({50,0}, 20);
    Rectangle rectM1({10,20}, 40, 80);
    Rectangle rectM2({70,75}, 40, 20);
    std::shared_ptr <Shape> circPtrM = std::make_shared <Circle> (circM);
    std::shared_ptr <Shape> rectPtrM1 = std::make_shared <Rectangle> (rectM1);
    std::shared_ptr <Shape> rectPtrM2 = std::make_shared <Rectangle> (rectM2);
    Matrix matrix(circPtrM);
    matrix.addShape(rectPtrM1);
    matrix.addShape(rectPtrM2);
    Matrix matrix1(std::move(matrix));
    std::unique_ptr <std::shared_ptr<Shape>[] > new_level1 = matrix1[0];
    std::unique_ptr <std::shared_ptr<Shape>[] > new_level2 = matrix1[1];
    BOOST_CHECK(new_level1[0] == circPtrM);
    BOOST_CHECK(new_level2[0] == rectPtrM1);
    BOOST_REQUIRE_EQUAL(matrix.getLevelNum(), 0);
    BOOST_REQUIRE_EQUAL(matrix.getLevelSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(MoveInformationOperatorTest)
  {
    Circle circM({20,20}, 20);
    Rectangle rectM1({40,40}, 40, 40);
    Rectangle rectM2({-20,-25}, 40, 50);
    std::shared_ptr <Shape> circPtrM = std::make_shared <Circle> (circM);
    std::shared_ptr <Shape> rectPtrM1 = std::make_shared <Rectangle> (rectM1);
    std::shared_ptr <Shape> rectPtrM2 = std::make_shared <Rectangle> (rectM2);
    Matrix matrix(circPtrM);
    matrix.addShape(rectPtrM1);
    Matrix matrix1(rectPtrM2);
    matrix1 = std::move(matrix);
    std::unique_ptr <std::shared_ptr<Shape>[] > new_level1 = matrix1[0];
    std::unique_ptr <std::shared_ptr<Shape>[] > new_level2 = matrix1[1];
    BOOST_CHECK(new_level1[0] == circPtrM);
    BOOST_CHECK(new_level2[0] == rectPtrM1);
    BOOST_REQUIRE_EQUAL(matrix.getLevelNum(), 0);
    BOOST_REQUIRE_EQUAL(matrix.getLevelSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(EqualityOperatorTest)
  {
    Circle circ({50,0}, 20);
    Rectangle rect({10,20}, 40, 80);
    std::shared_ptr <Shape> circPtrM = std::make_shared <Circle> (circ);
    std::shared_ptr <Shape> rectPtrM1 = std::make_shared <Rectangle> (rect);
    Matrix matrix(circPtrM);
    matrix.addShape(rectPtrM1);
    Matrix matrix1(circPtrM);
    matrix1.addShape(rectPtrM1);
    bool equal = (matrix == matrix1);
    BOOST_CHECK(equal == true);
  }

  BOOST_AUTO_TEST_CASE(NonEqualityOperatorTest)
  {
    Circle circ({50,0}, 20);
    Rectangle rect({10,20}, 40, 80);
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle> (circ);
    std::shared_ptr <Shape> rectPtr = std::make_shared <Rectangle> (rect);
    Matrix matrix(circPtr);
    Matrix matrix1( rectPtr);
    bool notequal = (matrix != matrix1);
    BOOST_CHECK(notequal == true);
  }

BOOST_AUTO_TEST_SUITE_END()
