#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace aknazarova
{
  class Circle : public Shape 
  {
  public:
    Circle(const point_t &pos, const double rad);
    double getRad() const noexcept;
    double getArea() const noexcept override;
    double getAngle() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void scale(double kof) override;
    void move(const point_t &pos) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void rotate(const double angle) noexcept override;

  private:
    point_t point_;
    double radius_;
    double angle_;
  };
}
#endif
