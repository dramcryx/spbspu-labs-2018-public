#include <memory>
#include <stdexcept>
#include <cmath>

#include "composite-shape.hpp"

using namespace aknazarova;

CompositeShape::CompositeShape(const std::shared_ptr <Shape> shape):
  shapes_(nullptr),
  size_(0),
  angle_(0.0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null pointer!!");
  }
  addShape(shape);
}

CompositeShape::CompositeShape(const CompositeShape &compositeShape):
  shapes_ (std::unique_ptr<std::shared_ptr<Shape>[]>(new std::shared_ptr <Shape> [compositeShape.size_])),
  size_ (compositeShape.size_),
  angle_(compositeShape.angle_)
{ 
  for (int i=0; i<size_; ++i)
  {
    shapes_[i] = compositeShape.shapes_[i];
  }
}

CompositeShape::CompositeShape(CompositeShape &&compositeShape):
  shapes_(std::move(compositeShape.shapes_)),
  size_(compositeShape.size_),
  angle_(compositeShape.angle_)
{
  compositeShape.size_ = 0;
  compositeShape.shapes_.reset();
}

CompositeShape &CompositeShape::operator = (const CompositeShape &compositeShape)
{
  if (this !=  &compositeShape)
  {
    size_ = compositeShape.size_;
    angle_ = compositeShape.angle_;
    shapes_.reset(new std::shared_ptr <Shape> [compositeShape.size_]);
    for (int i=0; i<size_; ++i)
    {
      shapes_[i]=compositeShape.shapes_[i];
    }
  }
  return *this;
}

CompositeShape &CompositeShape::operator= (CompositeShape &&compositeShape)
{
  if (this != &compositeShape)
  {
    size_ = compositeShape.size_;
    angle_ = compositeShape.angle_;
    shapes_ = std::move(compositeShape.shapes_);
    compositeShape.size_ = 0;
  }
  return *this;
}

bool CompositeShape::operator ==(const CompositeShape &compositeShape) const
{
  if ((this->size_ != compositeShape.size_) || (this->angle_ != compositeShape.angle_))
  {
    return false;
  }
  for (int i = 0; i < size_; ++i)
    {
      if (this->shapes_[i] != compositeShape.shapes_[i])
      {
        return false;
      }
    }
  return true;
}

bool CompositeShape::operator != (const CompositeShape &compositeShape) const
{
  if ((this->size_ != compositeShape.size_) || (this->angle_ != compositeShape.angle_))
  {
    return true;
  }
  for (int i = 0; i < size_; ++i)
    {
      if (this->shapes_[i] != compositeShape.shapes_[i])
      {
        return true;
      }
    }
  return false;
}

double CompositeShape::getArea() const noexcept
{
  double area=0.0;
  for (int i=0; i<size_; ++i)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

double CompositeShape::getAngle() const noexcept
{
  return angle_;
}

rectangle_t CompositeShape::getFrameRect() const noexcept
{
  if (size_ == 0)
  {
    return {{ 0.0, 0.0 }, 0.0, 0.0};
  }
  else
  {
    rectangle_t frameRect = shapes_[0]->getFrameRect();
    double xmin = frameRect.pos.x - frameRect.width/2.0;
    double xmax = frameRect.pos.x + frameRect.width/2.0;
    double ymin = frameRect.pos.y - frameRect.height/2.0;
    double ymax = frameRect.pos.y + frameRect.height/2.0;
    
    for (int i=1; i<size_; ++i)
    {
      frameRect = shapes_[i]->getFrameRect();
      if (xmin>frameRect.pos.x - frameRect.width/2.0)
      {
        xmin = frameRect.pos.x - frameRect.width/2.0;
      }
      if (ymin>frameRect.pos.y - frameRect.height/2.0)
      {
        ymin = frameRect.pos.y - frameRect.height/2.0;
      }
      if (xmax<frameRect.pos.x + frameRect.width/2.0)
      {
        xmax = frameRect.pos.x + frameRect.width/2.0;
      }
      if (ymax<frameRect.pos.y + frameRect.height/2.0)
      {
        ymax = frameRect.pos.y + frameRect.height/2.0;
      }
    }
    return rectangle_t {{(xmax+xmin)/2.0,(ymax+ymin)/2.0},xmax-xmin,ymax-ymin};
  }
}

void CompositeShape::scale(const double kof)
{
  if (kof <= 0.0)
  {
    throw std::invalid_argument("Invalid koefficent");
  }
  point_t cent=getFrameRect().pos;
  for (int i=0; i<size_; ++i)
  {
    const point_t Shapecent = shapes_[i]->getFrameRect().pos;
    shapes_[i]->scale(kof);
    shapes_[i]->move({(Shapecent.x-cent.x)*kof,(Shapecent.y-cent.y)*kof});
  }
}

void CompositeShape::move(const double dx, const double dy) noexcept
{
  for (int i = 0; i<size_; i++)
  {
    shapes_[i]->move(dx,dy);
  }
}

void CompositeShape::move(const point_t &pos) noexcept
{
  point_t cent = getFrameRect().pos;
  for (int i=0; i<size_; i++)
  {
    shapes_[i]->move(pos.x-cent.x,pos.y-cent.y);
  }
}

void CompositeShape::rotate(const double angle) noexcept
{
  angle_ += angle;
  if (fabs(angle_) >= 360.0)
  {
    angle_ = std::fmod(angle_, 360.0);
  }
  const double sinr = sin(angle_ * M_PI / 180.0);
  const double cosr = cos(angle_ * M_PI / 180.0);
  const point_t pointPos = getFrameRect().pos;
  for (int i = 0; i < size_; ++i)
  {
    const point_t shapePos = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move({ pointPos.x + cosr * (shapePos.x - pointPos.x) - sinr * (shapePos.y - pointPos.y), 
        pointPos.y + cosr * (shapePos.y - pointPos.y) + sinr * (shapePos.x - pointPos.x) });
    shapes_[i]->rotate(angle);
  }
}

std::shared_ptr <Shape> &CompositeShape::operator[] (int i) const
{
  if (i >= size_) 
  {
    throw std::out_of_range("Incorrect index");
  }
  return shapes_[i];
}

void CompositeShape::removeShape(const int num)
{
  if (size_ == 0)
  {
    throw std::out_of_range("CompositeShape is empty");
  }
  else
  {
    if ((num<=0)||(num>=size_))
    {
      throw std::out_of_range("Incorrect number");
    }
  }
  if (size_==1)
  {
    deleteShapes();
  }
  else
  {
    std::unique_ptr<std::shared_ptr<Shape>[]>newShapes(new std::shared_ptr<Shape>[size_-1]);
    for (int i=0; i<(num-1); ++i)
    {
      newShapes[i]=shapes_[i];
    }
    for (int i=num; i<size_;++i)
    {
      newShapes[i-1]=shapes_[i];
    }
    shapes_.swap(newShapes);
    --size_;
  }
}

void CompositeShape::addShape(const std::shared_ptr <Shape> shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid shape");
  }
  std::unique_ptr <std::shared_ptr <Shape> []> newShapes (new std::shared_ptr <Shape> [size_ + 1]);
  for (int i = 0; i<size_; ++i)
  {
    newShapes[i]=shapes_[i];
  }
  newShapes[size_++]=shape;
  shapes_.swap(newShapes);
}

void CompositeShape::deleteShapes()
{
  shapes_.reset();
  shapes_=nullptr;
  size_=0;
}

int CompositeShape::getSize() const
{
  return size_;
}
