#include <iostream>
#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"

using namespace aknazarova;
Rectangle::Rectangle(const point_t &pos, double w, double h):
  width_(w),
  height_(h),
  point_(pos),
  angle_(0.0)
{
  if ((height_<=0.0)||(width_<=0.0))
  {
    throw std::invalid_argument("Invalid argument rectangle");
  }
}

double Rectangle::getArea() const noexcept
{
  return height_ * width_;
}

double Rectangle::getWidth() const noexcept
{
  return width_;
}

double Rectangle::getHeight() const noexcept
{
  return height_;
}

double Rectangle::getAngle() const noexcept
{
  return angle_;
}

rectangle_t Rectangle::getFrameRect() const noexcept
{
  return {point_, width_, height_};
}

void Rectangle::scale(double kof)
{
  if (kof<=0.0)
  {
    throw std::invalid_argument("Invalid kof");
  }
  else
  {
    height_ *= kof;
    width_ *= kof;
  }
}
  
void Rectangle::move(const point_t &pos) noexcept
{
  point_ = pos;
}

void Rectangle::move(const double dx, const double dy) noexcept
{
  point_.x += dx;
  point_.y += dy;
}

void Rectangle::rotate(const double angle) noexcept
{
  angle_ += angle;
  if (fabs(angle_) >= 360.0)
  {
    angle_=fmod(angle_, 360.0);
  }
}
