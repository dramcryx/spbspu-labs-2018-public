#define BOOST_TEST_MODULE LAB_A3
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#define _USE_MATH_DEFINES
#include <math.h>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"



const double eps = 0.001;

BOOST_AUTO_TEST_SUITE(TestSimpleShapes)
BOOST_AUTO_TEST_CASE(TestMoveAxisRectangle)
{
  aleksovski::Rectangle r(12, 21, { -7, 19 });
  const double width_old = r.getFrameRect().width;
  const double height_old = r.getFrameRect().height;
  const double area_old = r.getArea();
  r.move(7.53, 92.58);
  BOOST_CHECK_CLOSE(width_old, r.getFrameRect().width, eps);
  BOOST_CHECK_CLOSE(height_old, r.getFrameRect().height, eps);
  BOOST_CHECK_CLOSE(area_old, r.getArea(), eps);
}
BOOST_AUTO_TEST_CASE(TestMoveToPointRectangle)
{
  aleksovski::Rectangle r(19.8, 3.29, { -83, 142.4 });
  const double width_old = r.getFrameRect().width;
  const double height_old = r.getFrameRect().height;
  const double area_old = r.getArea();
  r.move({ 1.63, -49.13 });
  BOOST_CHECK_CLOSE(width_old, r.getFrameRect().width, eps);
  BOOST_CHECK_CLOSE(height_old, r.getFrameRect().height, eps);
  BOOST_CHECK_CLOSE(area_old, r.getArea(), eps);
}
BOOST_AUTO_TEST_CASE(TestScaleRectangle)
{
  aleksovski::Rectangle r(9, 6, { 17,23 });
  const double area_old = r.getArea();
  const double k = 2.5;
  r.scale(k);
  BOOST_CHECK_CLOSE(area_old * k * k, r.getArea(), eps);
}
BOOST_AUTO_TEST_CASE(TestInvalidConstructorRectangle)
{
  BOOST_CHECK_THROW(aleksovski::Rectangle(-3, -5, { 1, 1 }), std::invalid_argument);
  BOOST_CHECK_THROW(aleksovski::Rectangle(3, -5, { 1, 1 }), std::invalid_argument);
  BOOST_CHECK_THROW(aleksovski::Rectangle(-3, 5, { 1, 1 }), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(TestInvalidScaleRectangle)
{
  aleksovski::Rectangle r(11, 15, { 2, 7 });
  BOOST_CHECK_THROW(r.scale(-3), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(TestMoveAxisCircle)
{
  aleksovski::Circle c(20, { -6, 18 });
  const double width_old = c.getFrameRect().width;
  const double height_old = c.getFrameRect().height;
  const double area_old = c.getArea();
  c.move(3.36, 0);
  BOOST_CHECK_CLOSE(width_old, c.getFrameRect().width, eps);
  BOOST_CHECK_CLOSE(height_old, c.getFrameRect().height, eps);
  BOOST_CHECK_CLOSE(area_old, c.getArea(), eps);
}
BOOST_AUTO_TEST_CASE(TestMoveToPointCircle)
{
  aleksovski::Circle c(2, { -77, 111.7 });
  const double width_old = c.getFrameRect().width;
  const double height_old = c.getFrameRect().height;
  const double area_old = c.getArea();
  c.move({ 3.46, -51.54 });
  BOOST_CHECK_CLOSE(width_old, c.getFrameRect().width, eps);
  BOOST_CHECK_CLOSE(height_old, c.getFrameRect().height, eps);
  BOOST_CHECK_CLOSE(area_old, c.getArea(), eps);
}
BOOST_AUTO_TEST_CASE(TestScaleCircle)
{
  aleksovski::Circle c(5, { 11,16 });
  const double area_old = c.getArea();
  const double k = 1.2;
  c.scale(k);
  BOOST_CHECK_CLOSE(area_old * k * k, c.getArea(), eps);
}
BOOST_AUTO_TEST_CASE(TestInvalidConstructorCircle)
{
  BOOST_CHECK_THROW(aleksovski::Circle(-20, { 1, 1 }), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(TestInvalidScaleCircle)
{
  aleksovski::Circle c(8, { 2, 9 });
  BOOST_CHECK_THROW(c.scale(-3), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(TestRightAdditionRectangle)
{
  aleksovski::Rectangle rect1 = { 5.3, 6.2,{ 3.5, 2.2 } };
  aleksovski::CompositeShape comshape;
  comshape.add(rect1);
  BOOST_CHECK_CLOSE(comshape.getArea(), 5.3 * 6.2, eps);
  BOOST_CHECK_CLOSE(comshape.getFrameRect().width, 5.3, eps);
  BOOST_CHECK_CLOSE(comshape.getFrameRect().height, 6.2, eps);
}

BOOST_AUTO_TEST_CASE(TestRightAdditionCircle)
{
  aleksovski::Circle circ1{ 4.1,{ 3.5, 2.2 } };
  aleksovski::CompositeShape comshape;
  comshape.add(circ1);
  aleksovski::rectangle_t rect = comshape.getFrameRect();
  BOOST_CHECK_CLOSE(comshape.getArea(), 4.1 * 4.1 * M_PI, eps);
  BOOST_CHECK_CLOSE(rect.width, 4.1 * 2, eps);
  BOOST_CHECK_CLOSE(rect.height, 4.1 * 2, eps);
}

BOOST_AUTO_TEST_CASE(TestIncorrectScaleValue)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  aleksovski::Circle circ1{ 4.1,{ 3.5, 2.2 } };
  aleksovski::CompositeShape comshape;
  comshape.add(rect1);
  comshape.add(circ1);
  BOOST_CHECK_THROW(comshape.scale(-4), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestIncorrectScaleValueTwo)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  aleksovski::CompositeShape comshape;
  comshape.add(rect1);
  BOOST_CHECK_THROW(comshape.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestMoveAxis)
{
  aleksovski::CompositeShape comshape;
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  aleksovski::Circle circ1{ 1,{ 3.5, 2.2 } };
  comshape.add(rect1);
  comshape.add(circ1);
  double s = comshape.getArea();
  comshape.move(5.9, 1.5);
  aleksovski::rectangle_t rect = comshape.getFrameRect();
  BOOST_CHECK_CLOSE(comshape.getArea(), s, eps);
  BOOST_CHECK_CLOSE(comshape.getFrameRect().width, rect.width, eps);
  BOOST_CHECK_CLOSE(comshape.getFrameRect().height, rect.height, eps);
}

BOOST_AUTO_TEST_CASE(TestScale)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  aleksovski::Circle circ1{ 4.1,{ 3.5, 2.2 } };
  aleksovski::CompositeShape comshape;
  comshape.add(rect1);
  comshape.add(circ1);
  double s = comshape.getArea();
  comshape.scale(3);
  BOOST_CHECK_CLOSE(comshape.getArea(), s * 3 * 3, eps);
}

BOOST_AUTO_TEST_CASE(TestSize)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  aleksovski::Circle circ1{ 4.1,{ 3.5, 2.2 } };
  aleksovski::CompositeShape comshape;
  comshape.add(rect1);
  comshape.add(circ1);
  size_t k = 2;
  BOOST_CHECK_EQUAL(comshape.getSize(), k);
}

BOOST_AUTO_TEST_CASE(TestTwoScale)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  aleksovski::CompositeShape comshape;
  comshape.add(rect1);
  double s = comshape.getArea();
  comshape.scale(0.5);
  BOOST_CHECK_CLOSE(comshape.getArea(), s * 0.5 * 0.5, eps);
}
BOOST_AUTO_TEST_CASE(TestTwoShape)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  aleksovski::CompositeShape comshape;
  aleksovski::CompositeShape comshape1;
  comshape.add(rect1);
  comshape1.add(rect1);
  BOOST_CHECK_CLOSE(comshape.getArea(), comshape1.getArea(), eps);
  BOOST_CHECK_CLOSE(comshape.getArea(), comshape1.getArea(), eps);
  BOOST_CHECK_CLOSE(comshape.getFrameRect().width, comshape1.getFrameRect().width, eps);
  BOOST_CHECK_CLOSE(comshape.getFrameRect().height, comshape1.getFrameRect().height, eps);
}

BOOST_AUTO_TEST_SUITE_END()

