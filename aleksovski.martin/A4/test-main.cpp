#define BOOST_TEST_MODULE LAB_A4

#include <stdexcept>
#include <cmath>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp" 
#include "composite-shape.hpp"

const double eps = 0.001;

using namespace aleksovski;

BOOST_AUTO_TEST_SUITE(simple_shapes)
BOOST_AUTO_TEST_CASE(rectangle_move_dxy)
{
  aleksovski::Rectangle r(15, 29, { -9, 27 });
  const double width_before = r.getFrameRect().width;
  const double height_before = r.getFrameRect().height;
  const double area_before = r.getArea();
  r.move(6.66, 32.38);
  BOOST_CHECK_CLOSE(width_before, r.getFrameRect().width, eps);
  BOOST_CHECK_CLOSE(height_before, r.getFrameRect().height, eps);
  BOOST_CHECK_CLOSE(area_before, r.getArea(), eps);
}

BOOST_AUTO_TEST_CASE(rectangle_move_point)
{
  aleksovski::Rectangle r(15.8, 1.29, { -91, 127.4 });
  const double width_before = r.getFrameRect().width;
  const double height_before = r.getFrameRect().height;
  const double area_before = r.getArea();
  r.move({ 1.81, -57.11 });
  BOOST_CHECK_CLOSE(width_before, r.getFrameRect().width, eps);
  BOOST_CHECK_CLOSE(height_before, r.getFrameRect().height, eps);
  BOOST_CHECK_CLOSE(area_before, r.getArea(), eps);
}

BOOST_AUTO_TEST_CASE(rectangle_scale)
{
  aleksovski::Rectangle r(8, 5, { 13, 18 });
  const double area_before = r.getArea();
  const double k = 1.8;
  r.scale(k);
  BOOST_CHECK_CLOSE(area_before * k * k, r.getArea(), eps);
}

BOOST_AUTO_TEST_CASE(rectangle_invalid_constructor)
{
  BOOST_CHECK_THROW(aleksovski::Rectangle(-1, 0, { 1, 1 }), std::invalid_argument);
  BOOST_CHECK_THROW(aleksovski::Rectangle(1, -2, { 1, 1 }), std::invalid_argument);
  BOOST_CHECK_THROW(aleksovski::Rectangle(-1, 2, { 1, 1 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangle_invalid_scale)
{
  aleksovski::Rectangle r(10, 12, { 1, 5 });
  BOOST_CHECK_THROW(r.scale(-3), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangle_rotate_const_area)
{
  aleksovski::Rectangle r(5, 41, { 1, 7 });
  const double area_before = r.getArea();
  r.rotate(82);
  BOOST_CHECK_CLOSE(area_before, r.getArea(), eps);
}

BOOST_AUTO_TEST_CASE(rectangle_rotate_const_center)
{
  aleksovski::Rectangle r(5, 41, { 1, 7 });
  const point_t pos_before = r.getFrameRect().pos;
  r.rotate(-56);
  const point_t newPos = r.getFrameRect().pos;
  BOOST_CHECK_CLOSE(pos_before.x, newPos.x, eps);
  BOOST_CHECK_CLOSE(pos_before.y, newPos.y, eps);
}

BOOST_AUTO_TEST_CASE(rectangle_rotate_360_const)
{
  aleksovski::Rectangle r(5, 41, { 1, 7 });
  const rectangle_t frame_before = r.getFrameRect();
  r.rotate(M_PI * 360 / 180);
  rectangle_t newFrame = r.getFrameRect();
  BOOST_CHECK_CLOSE(frame_before.pos.x, newFrame.pos.x, eps);
  BOOST_CHECK_CLOSE(frame_before.pos.y, newFrame.pos.y, eps);
  BOOST_CHECK_CLOSE(frame_before.height, newFrame.height, eps);
  BOOST_CHECK_CLOSE(frame_before.width, newFrame.width, eps);
}

BOOST_AUTO_TEST_CASE(rectangle_rotate_90)
{
  aleksovski::Rectangle r(5, 41, { 1, 7 });
  const rectangle_t frame_before = r.getFrameRect();
  r.rotate(M_PI * 90 / 180);
  rectangle_t newFrame = r.getFrameRect();
  BOOST_CHECK_CLOSE(frame_before.height, newFrame.width, eps);
  BOOST_CHECK_CLOSE(frame_before.width, newFrame.height, eps);
}

BOOST_AUTO_TEST_CASE(circle_move_dxy)
{
  Circle c(20, { -9, 27 });
  const double width_before = c.getFrameRect().width;
  const double height_before = c.getFrameRect().height;
  const double area_before = c.getArea();
  c.move(5.51, 0);
  BOOST_CHECK_CLOSE(width_before, c.getFrameRect().width, eps);
  BOOST_CHECK_CLOSE(height_before, c.getFrameRect().height, eps);
  BOOST_CHECK_CLOSE(area_before, c.getArea(), eps);
}

BOOST_AUTO_TEST_CASE(circle_move_point)
{
  Circle c(2, { -91, 127.4 });
  const double width_before = c.getFrameRect().width;
  const double height_before = c.getFrameRect().height;
  const double area_before = c.getArea();
  c.move({ 1.81, -57.11 });
  BOOST_CHECK_CLOSE(width_before, c.getFrameRect().width, eps);
  BOOST_CHECK_CLOSE(height_before, c.getFrameRect().height, eps);
  BOOST_CHECK_CLOSE(area_before, c.getArea(), eps);
}

BOOST_AUTO_TEST_CASE(circle_invalid_constructor)
{
  BOOST_CHECK_THROW(Circle(-10, { 1, 1 }), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circle_invalid_scale)
{
  Circle c(5, { 1, 5 });
  BOOST_CHECK_THROW(c.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestRemovement)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  Circle circ1{ 4.1,{ 3.5, 2.2 } };
  CompositeShape comshape;
  comshape.add(rect1);
  comshape.add(circ1);
  comshape.removeElement(1);
  BOOST_CHECK_EQUAL(comshape.getSize(), 1);
}

BOOST_AUTO_TEST_CASE(TestOutOfRange)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  Circle circ1{ 4.1,{ 3.5, 2.2 } };
  CompositeShape comshape;
  comshape.add(rect1);
  comshape.add(circ1);
  BOOST_CHECK_THROW(comshape.removeElement(3), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(TestOperator)
{
  Circle circ1{ 4.1,{ 3.5, 2.2 } };
  CompositeShape comshape;
  comshape.add(circ1);
  BOOST_CHECK_EQUAL((comshape)[0], &circ1);
}

BOOST_AUTO_TEST_CASE(TestOperatorOutOfRange)
{
  Circle circ1{ 4.1,{ 3.5, 2.2 } };
  CompositeShape comshape;
  comshape.add(circ1);
  BOOST_CHECK_THROW(comshape[3], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(TestMoveOperator)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  Circle circ1{ 4.1,{ 3.5, 2.2 } };
  CompositeShape comshape;
  comshape.add(circ1);
  CompositeShape *comshape1 = new CompositeShape;
  comshape1->add(circ1);
  comshape1->add(rect1);
  size_t k = comshape1->getSize();
  comshape = std::move(*comshape1);
  BOOST_CHECK_EQUAL(comshape.getSize(), k);
  delete comshape1;
}

BOOST_AUTO_TEST_CASE(TestCopyOperator)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  Circle circ1{ 4.1,{ 3.5, 2.2 } };
  Circle circ2{ 4.2,{ 3.2, 2.2 } };
  CompositeShape comshape;
  comshape.add(circ1);
  CompositeShape *comshape1 = new CompositeShape;
  comshape1->add(circ2);
  comshape1->add(rect1);
  size_t k = comshape1->getSize();
  comshape = (*comshape1);
  BOOST_CHECK_EQUAL(comshape.getSize(), k);
  delete comshape1;
}

BOOST_AUTO_TEST_CASE(TestMoveConstructor)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  Circle circ1{ 4.1,{ 3.5, 2.2 } };
  CompositeShape *comshape = new CompositeShape;
  comshape->add(circ1);
  size_t k = comshape->getSize();
  CompositeShape comshape1(std::move(*comshape));
  BOOST_CHECK_EQUAL(comshape1.getSize(), k);
  delete comshape;
}

BOOST_AUTO_TEST_CASE(TestCopyConstructor)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 3.5, 2.2 } };
  Circle circ1{ 4.1,{ 3.5, 2.2 } };
  CompositeShape *comshape = new CompositeShape;
  comshape->add(circ1);
  size_t k = comshape->getSize();
  CompositeShape comshape1(*comshape);
  BOOST_CHECK_EQUAL(k, comshape1.getSize());
  delete comshape;
}

BOOST_AUTO_TEST_CASE(TestAdditionToMatrix)
{
  Circle circ1{ 4.1,{ 3.5, 2.2 } };
  CompositeShape comshape;
  comshape.add(circ1);
  Matrix layers{ comshape.getSize() };
  comshape.addtoMatrix(layers);
  BOOST_CHECK_EQUAL(layers.getNumberLayers(), 1);
}

BOOST_AUTO_TEST_CASE(TestAdditionToMatrix1)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 7.3, 5.1 } };
  Circle circ1{ 4.1,{ 3.5, 2.2 } };
  CompositeShape comshape;
  comshape.add(circ1);
  comshape.add(rect1);
  Matrix layers{ comshape.getSize() };
  comshape.addtoMatrix(layers);
  BOOST_CHECK_EQUAL(layers.getNumberLayers(), 1);
}

BOOST_AUTO_TEST_CASE(TestAdditionToMatrix2)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 7.3, 5.1 } };
  Circle circ1{ 4.8,{ 7.3, 5.4 } };
  CompositeShape comshape;
  comshape.add(circ1);
  comshape.add(rect1);
  Matrix layers{ comshape.getSize() };
  comshape.addtoMatrix(layers);
  BOOST_CHECK_EQUAL(layers.getNumberLayers(), 2);
}

BOOST_AUTO_TEST_CASE(TestAdditionToMatrix3)
{
  aleksovski::Rectangle rect1{ 5.3, 6.2,{ 7.3, 5.1 } };
  Circle circ1{ 4.8,{ 7.3, 5.4 } };
  Circle circ = { 2.1,{ 1.1, 2.4 } };
  CompositeShape comshape;
  comshape.add(rect1);
  comshape.add(circ1);
  comshape.add(circ);
  Matrix layers{ comshape.getSize() };
  comshape.addtoMatrix(layers);
  BOOST_CHECK_EQUAL(layers.getNumberLayers(), 3);
}

BOOST_AUTO_TEST_SUITE_END()
