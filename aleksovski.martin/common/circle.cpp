#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

using namespace aleksovski;

Circle::Circle(double radius, const point_t & point) :
  Shape(point),
  m_radius(radius),
  m_angle(0)
{
  if (m_radius < 0.0)
  {
    throw std::invalid_argument("Incorrect value");
  }
}

double Circle::getRadius() const
{
  return m_radius;
}

double Circle::getArea() const
{
  return(m_radius * m_radius * M_PI);
}

rectangle_t Circle::getFrameRect() const
{ 
  rectangle_t rect {m_radius * 2, m_radius * 2, m_centre};
  return rect;
}

point_t Circle::getCentre() const
{
  return m_centre;
  
}

void Circle::move(const point_t & p)
{
  m_centre.x = p.x;
  m_centre.y = p.y;
}

void Circle::move(double dx, double dy)
{
  m_centre.x += dx;
  m_centre.y += dy;
}

void Circle::scale(double k)
{
  if (k < 0.0)
  {
    throw std::invalid_argument("Incorrect value");
  }
  m_radius = m_radius * k;
}

void Circle::rotate(double alpha)
{
  m_angle = alpha;
}

void Circle::printf() const
{
  std::cout << "Centre circle(" << m_centre.x << ";" << m_centre.y << ")" << std::endl;
  std::cout << "Radius of circle = " << m_radius << std::endl;
  rectangle_t rect = getFrameRect();
  std::cout << "Centre of FrameRectangle (" << rect.pos.x << ";" << rect.pos.y << ")" << std::endl;
}
