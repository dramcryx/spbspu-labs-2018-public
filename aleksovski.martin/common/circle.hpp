#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace aleksovski
{
  class Circle : public Shape
  {
  public:
    Circle(double radius, const point_t & point);
    double getRadius() const;
    point_t getCentre() const;
    double getArea() const override;
    void printf() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t & p) override;
    void scale(double k) override;
    void rotate(double alpha) override;
  private:
    double m_radius;
    double m_angle;
  };
}
#endif //CIRCLE_HPP
