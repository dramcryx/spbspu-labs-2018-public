#include <iostream>
#include <stdexcept>
#include <memory>
#include "matrix.hpp"

using namespace aleksovski;

Matrix::Matrix( size_t numberOfFigures):
  m_count(0),
  m_numberLayers(1),
  m_layers (new Shape*[numberOfFigures+1])
{
}

void Matrix::addFigure(std::unique_ptr <Shape *[]> & figure)
{
  size_t iter = 0,
  count = 0,
  anotherCount = 0,
  layer = 0;
  while (layer < m_numberOfFigures)
  {
    for (size_t z = iter; z < m_numberOfFigures; z++)
    {
      if (m_count == 0 )
      {
         m_layers[0] = figure[z];
         for (size_t i = 1; i <= m_numberOfFigures; i++)
         {
           m_layers[i] = nullptr;
           m_count++;
         }
         layer++;
      }
      else
      {
        rectangle_t rect = {0, 0, {0, 0}};
        rectangle_t rect1 = {0, 0, {0, 0}};
        for (size_t i = count; i < m_numberOfFigures * m_numberLayers; i++)
        {
          if (m_layers[i] != nullptr)
          {
            rect = m_layers[i]->getFrameRect();
            rect1 = figure[z]->getFrameRect();
            if ((((rect1.pos.x + rect1.width / 2) < (rect.pos.x + rect.width / 2)) & (( rect1.pos.y + rect1.height / 2) < (rect.pos.y + rect.height / 2))) ||
                   (((rect1.pos.x - rect1.width / 2) < (rect.pos.x - rect.width / 2)) & ((rect1.pos.y + rect1.height / 2) < (rect.pos.y + rect.height / 2))) ||
                   (((rect1.pos.x - rect1.width / 2) < (rect.pos.x - rect.width / 2)) & ((rect1.pos.y - rect1.height / 2) < (rect.pos.y - rect.height / 2)))||
                   (((rect1.pos.x + rect1.width / 2) < (rect.pos.x + rect.width / 2)) & ((rect1.pos.y - rect1.height / 2) < (rect.pos.y - rect.height / 2))))
            {
              if (anotherCount == 0)
              {
                layer++;
                m_layers[z*m_numberLayers]=nullptr;
                std::unique_ptr <Shape*[]> temp_array (new Shape *[(m_numberLayers+1)*m_numberOfFigures]);
                for (size_t t = 0; t<(m_numberOfFigures*m_numberLayers); t++)
                {
                  temp_array[t] = m_layers[t];
                }
                for (size_t w = (m_numberOfFigures*m_numberLayers); w<((m_numberLayers+1)*m_numberOfFigures); w++)
                {
                  temp_array[w] = nullptr;
                }
                temp_array[z + (m_numberLayers*m_numberOfFigures)] = figure[z];
                m_layers.swap(temp_array);
                anotherCount++;
                break;
              }
              else
              {
                m_layers[z*m_numberLayers]=nullptr;
                m_layers[z+(m_numberLayers*m_numberOfFigures)] = figure[z];
                break;
              }
            }
           else if ((((rect1.pos.x + rect1.width / 2) == (rect.pos.x + rect.width / 2)) & ((rect1.pos.y + rect1.height / 2) == (rect.pos.y + rect.height / 2))) ||
                   (((rect1.pos.x - rect1.width / 2) == (rect.pos.x - rect.width / 2)) & ((rect1.pos.y + rect1.height / 2) == (rect.pos.y + rect.height / 2))) ||
                   (((rect1.pos.x - rect1.width / 2) == (rect.pos.x - rect.width / 2)) & ((rect1.pos.y - rect1.height / 2) == (rect.pos.y - rect.height / 2)))|
                    (((rect1.pos.x + rect1.width / 2) == (rect.pos.x + rect.width / 2)) & ((rect1.pos.y - rect1.height / 2) == (rect.pos.y - rect.height / 2))))
           {
             break;
           }
           else
           {
             m_layers[z*m_numberLayers]=figure[z];
             layer++;
           }
          }
        }
      }
    }
    if (anotherCount != 0)
    {
      m_numberLayers++;
    }
    anotherCount = 0;
    count =+ m_numberOfFigures;
    iter++;
  }
}

Shape* Matrix:: operator[](size_t number) const
{
  if (number > m_numberLayers*m_numberOfFigures)
  {
    throw std::out_of_range("Incorrect value");
  }
  else
  {
    return m_layers[number];
  }
}

void Matrix::setNumberOfFigures(size_t number)
{
  m_numberOfFigures = number;
}

point_t Matrix::getCentre() const
{
  return m_centre;
}

size_t Matrix::getNumberLayers() const
{
  return m_numberLayers;
}

void Matrix::printf() const
{
  size_t count = 0,
  k = 1,
  number = 0,
  number1 = m_numberOfFigures;
  while (k <= m_numberLayers)
  {
    for (size_t i = number; i < number1; i++)
      {
        if (m_layers[i] != nullptr)
        {
          count++;
        }
      }
      number = number + m_numberOfFigures;
      number1 = number1 + m_numberOfFigures;
      std::cout << "Figures on " << k <<" = " << count << std::endl;
      count = 0;
      k++;
   }
}
