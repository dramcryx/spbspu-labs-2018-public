#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace aleksovski
{
  class Matrix
  {
  public:
    Matrix(size_t numberOfFigures);
    Matrix(const Matrix &) = delete;
    Matrix(const Matrix &&) = delete;
    Shape *operator[](size_t number) const;
    Matrix &operator = (const Matrix &) = delete;
    Matrix &operator = (const Matrix &&) = delete;
    void addFigure(std::unique_ptr <Shape *[]> & figure);
    size_t getNumberLayers() const;
    void setNumberOfFigures(size_t number);
    point_t getCentre() const;
    void printf() const;
  private:
    point_t m_centre;
    size_t m_count;
    size_t m_numberLayers;
    size_t m_numberOfFigures;
    std::unique_ptr<Shape*[]> m_layers;
  };
}
#endif // MATRIX_HPP
