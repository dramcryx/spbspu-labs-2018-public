#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace aleksovski {
  class Rectangle : public Shape
  {
  public:
    Rectangle(double width, double height, const point_t & point);
    double getWidth() const;
    double getHeight() const;
    point_t getCentre() const;
    void rotate(double alpha) override;
    void printf() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t & p) override;
    void scale(double k) override;
  private:
    double m_width;
    double m_height;
    rectangle_t rect_;
    double m_angle;
  };
}
#endif //RECTANGLE_HPP
