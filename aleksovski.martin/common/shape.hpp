#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

namespace aleksovski
{
  class Shape
  {
  public:
    point_t m_centre;
    virtual ~Shape() = default;
    virtual point_t getCentre() const = 0;
    virtual void printf() const = 0;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect()const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t & p) = 0;
    virtual void scale(double k) = 0;
    virtual void rotate(double alpha) = 0;
  protected:
    Shape(const point_t & pos);
  };
}
#endif // SHAPE_HPP
