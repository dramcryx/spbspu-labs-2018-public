#include <iostream>
#include <cmath>
#include "composite-shape.hpp"

almukhametov::CompositeShape::CompositeShape(Shape *shape):
  composition(new Shape *[1]),
  size_(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Error with pointer");
  }
  composition[0] = shape;
}

almukhametov::CompositeShape::CompositeShape():
  composition(new Shape *[0]),
  size_(0)
{

}

almukhametov::CompositeShape::~CompositeShape()
{

}

double almukhametov::CompositeShape::getArea() const
{
  double Area=0;
  for (int i=0;i<size_;i++)
  {
    Area+=composition[i]->getArea();
  }
  return Area;
}

almukhametov::rectangle_t almukhametov::CompositeShape::getFrameRect() const
{
  rectangle_t rec = composition[0]->getFrameRect();
  double xMax = rec.pos.x + rec.width/2;
  double xMin = rec.pos.x - rec.width/2;
  double yMax = rec.pos.y + rec.height/2;
  double yMin = rec.pos.y - rec.height/2;

  for (int i = 1; i < size_; i++)
  {
    rec = composition[i]->getFrameRect();

    if ((rec.pos.x + rec.width/2) > xMax)
    {
      xMax = rec.pos.x + rec.width/2;
    }
    if ((rec.pos.x - rec.width/2) > xMin)
    {
      xMin = rec.pos.x - rec.width/2;
    }
    if ((rec.pos.y + rec.height/2) > yMax)
    {
      yMax = rec.pos.y + rec.height/2;
    }
    if ((rec.pos.y - rec.height/2) > yMin)
    {
      yMin = rec.pos.y - rec.height/2;
    }
  }

  point_t center = {xMax - (xMax-xMin)/2,yMax - (yMax-yMin)/2};
  return {xMax-xMin,yMax-yMin,center};
}

void almukhametov::CompositeShape::move(point_t c)
{
  for (int i=0;i<size_;i++)
  {
    composition[i]->move(c);
  }
}

void almukhametov::CompositeShape::move(double dx, double dy)
{
  for (int i=0;i<size_;i++)
  {
    composition[i]->move(dx,dy);
  }
}

void almukhametov::CompositeShape::scale(double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Factor must be >= 0");
  }
  for (int i=0;i<size_;i++)
  {
    composition[i]->scale(factor);
  }
}

void almukhametov::CompositeShape::newShape(Shape *shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Error with pointer");
  }
  std::unique_ptr<Shape *[]> newComp (new Shape *[size_+1]);
  for (int i=0;i<size_;i++)
  {
    newComp[i]=composition[i];
  }
  newComp[size_]=shape;
  composition.swap(newComp);
  size_++;
}

void almukhametov::CompositeShape::delShape(int index)
{
  if (index > size_-1)
  {
    throw std::invalid_argument("There is no element with that index");
  }

  std::unique_ptr<Shape *[]> newComp(new Shape *[size_-1]);
  for (int i=0;i<size_;i++)
  {
    if (i!=index)
    {
      if (i > index)
      {
        newComp[i - 1] = composition[i];
      }
      else
      {
        newComp[i] = composition[i];
      }
    }
  }
  composition.swap(newComp);
  size_--;
}

void almukhametov::CompositeShape::rotate(double angle)
{
  point_t center_ = CompositeShape::getFrameRect().pos;
  angle = (angle*M_PI)*180;
  const double sinAngle = sin(angle);
  const double cosAngle = cos(angle);
  for (int i=0;i<size_;i++)
  {
    point_t shapeCenter_ = composition[i]->getFrameRect().pos;
    composition[i]->move({ center_.x + (shapeCenter_.x - center_.x) * cosAngle - (shapeCenter_.y - center_.y) *
    sinAngle, center_.y + (shapeCenter_.y - center_.y)*cosAngle + (shapeCenter_.x - center_.x)*sinAngle});
    composition[i]->rotate(angle);
  }
}
