#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void showInfo(const Shape &shape)
{
  std::cout << "Frame's width: ";
  std::cout << shape.getFrameRect().width << std::endl;
  std::cout << "Frame's height: ";
  std::cout << shape.getFrameRect().height << std::endl;
  std::cout << "Frame's center (" << shape.getFrameRect().pos.x << " , "
  << shape.getFrameRect().pos.y << ")" << std::endl;
  std::cout << "Area: ";
  std::cout << shape.getArea() << std::endl << std::endl;
}

int main()
{
  Rectangle rect(30, 50, {7, 5});
  Circle circle(30, {20, 5});
  Triangle tri({0, 0}, {5, 10}, {90, 0});
  
  std::cout << "Rectangle info: " << std::endl;
  showInfo(rect);

  std::cout << "Circle info: " << std::endl;
  showInfo(circle);

  std::cout << "Triangle info: " << std::endl;
  showInfo(tri);
  
  std::cout << "Move rectangle to (60,70)" << std::endl << std::endl;
  rect.move({60, 70});
  showInfo(rect);

  std::cout << "Move circle x+80, y+50" << std::endl << std::endl;
  circle.move(80, 50);
  showInfo(circle);

  std::cout << "Move triangle to (5,78); move triangle x+3, y+7" << std::endl << std::endl;
  tri.move({5, 78});
  tri.move(3, 7);
  showInfo(tri);
  
  return 0;
}
