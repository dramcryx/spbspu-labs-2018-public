#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(const double &w, const double &h, const point_t &p);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &placement) override;
  void move(const double x, const double y) override;
private:
  double width_;
  double height_;
  point_t center_;
};

#endif
