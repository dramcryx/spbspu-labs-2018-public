#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t &a, const point_t &b, const point_t &c);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &placement) override;
  void move(const double x, const double y) override;
private:
  point_t pA_;
  point_t pB_;
  point_t pC_;
  point_t center_;
  double getMin(const double &A, const double &B, const double &C) const;
  double getMax(const double &A, const double &B, const double &C) const;
};

#endif
