#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

const double EPS = 0.00001;

BOOST_AUTO_TEST_SUITE(Rectangle_tests)
  
  BOOST_AUTO_TEST_CASE(Move_to_point_test)
  {
    anikin::Rectangle rectangle(80, 60, {10, 20});
    rectangle.move({60,80});
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().pos.x, 60, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().pos.y, 80, EPS);
  }

  BOOST_AUTO_TEST_CASE(Move_dx_dy_test)
  {
    anikin::Rectangle rectangle(80, 60, {10, 20});
    rectangle.move(15, -20);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().pos.x, 25, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().pos.y, 0, EPS);
  }
  
  BOOST_AUTO_TEST_CASE(GetArea_test)
  {
    anikin::Rectangle rectangle(80, 60, {10, 20});
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), 80*60, EPS);
  }

  BOOST_AUTO_TEST_CASE(Frame_rect_test)
  {
    anikin::Rectangle rectangle(80, 60, {10, 20});
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().width, 80, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().height, 60, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().pos.x, 10, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().pos.y, 20, EPS);
  }

  BOOST_AUTO_TEST_CASE(Scale_area_test)
  {
    anikin::Rectangle rectangle(80, 60, {10, 20});
    rectangle.scale(5);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), 80*60*pow(5, 2), EPS);
  }

  BOOST_AUTO_TEST_CASE(Move_to_point_area_height_width_test)
  {
    anikin::Rectangle rectangle(80, 60, {10, 20});
    rectangle.move({60, 80});
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), 80*60, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().width, 80, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().height, 60, EPS);
  }

  BOOST_AUTO_TEST_CASE(Move_dx_dy_area_height_width_test)
  {
    anikin::Rectangle rectangle(80, 60, {10, 20});
    rectangle.move(15, -20);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getArea(), 80*60, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().height, 60, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().width, 80, EPS);
  }

  BOOST_AUTO_TEST_CASE(Invalid_scale_coefficient_test)
  {
    anikin::Rectangle rectangle(80, 60, {10, 20});
    BOOST_CHECK_THROW(rectangle.scale(-10), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_height_test)
  {
    BOOST_CHECK_THROW(anikin::Rectangle(80, -60, {10, 20}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_width_test)
  {
    BOOST_CHECK_THROW(anikin::Rectangle(-80, 60, {10, 20}), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Circle_tests)
  
  BOOST_AUTO_TEST_CASE(Move_to_point_test)
  {
    anikin::Circle circle (80, {50, 70});
    circle.move({290,110});
    BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().pos.x, 290, EPS);
    BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().pos.y, 110, EPS);
  }

  BOOST_AUTO_TEST_CASE(Move_dx_dy_test)
  {
    anikin::Circle circle (80, {50, 70});
    circle.move(-10, 140);
    BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().pos.x, 40, EPS);
    BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().pos.y, 210, EPS);
  }
  
  BOOST_AUTO_TEST_CASE(GetArea_test)
  {
      anikin::Circle circle (80, {50, 70});
      BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), M_PI * pow(80, 2), EPS);
  }

  BOOST_AUTO_TEST_CASE(Frame_rect_test)
  {
    anikin::Circle circle (80, {50, 70});
    BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().width, 80 * 2, EPS);
    BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().height, 80 * 2, EPS);
    BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().pos.x, 50, EPS);
    BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().pos.y, 70, EPS);
  }

  BOOST_AUTO_TEST_CASE(Scale_area_test)
  {
    anikin::Circle circle (80, {50, 70});
    circle.scale(10);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), M_PI * pow(80, 2) * pow(10, 2), EPS);
  }

  BOOST_AUTO_TEST_CASE(Move_area_radius_test)
  {
    anikin::Circle circle (80, {50, 70});
    circle.move({290,110});
    circle.move(-10, 140);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), M_PI * pow(80, 2), EPS);
    BOOST_CHECK_CLOSE_FRACTION(circle.getFrameRect().width/2, 80, EPS);
  }

  BOOST_AUTO_TEST_CASE(Invalid_scale_coefficient_test)
  {
    anikin::Circle circle (80, {50, 70});
    BOOST_CHECK_THROW(circle.scale(-10), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_radius_test)
  {
    BOOST_CHECK_THROW(anikin::Circle(-80, {50, 70}), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()
  
BOOST_AUTO_TEST_SUITE(Triangle_tests)

  BOOST_AUTO_TEST_CASE(area_move_on_tri)
  {
    anikin::Triangle tri{{0, 0}, {10, 10}, {20, 0}};
    double area = tri.getArea();
    tri.move(99, 99);
    BOOST_CHECK_CLOSE_FRACTION(tri.getArea(), area, EPS);
  }
  BOOST_AUTO_TEST_CASE(area_move_to_tri)
  {
    anikin::Triangle tri{{0, 0}, {10, 10}, {20, 0}};
    double area = tri.getArea();
    tri.move({99, 99});
    BOOST_CHECK_CLOSE_FRACTION(tri.getArea(), area, EPS);
  }
  BOOST_AUTO_TEST_CASE(scaling_area_bigger_tri)
  {
    anikin::Triangle tri{{0, 0}, {10, 10}, {20, 0}};
    double area = tri.getArea();
    tri.scale(2);
    BOOST_CHECK_CLOSE_FRACTION(tri.getArea(), 4*area, EPS);
  }
  BOOST_AUTO_TEST_CASE(scaling_area_smaller_tri)
  {
    anikin::Triangle tri{{0, 0}, {10, 10}, {20, 0}};
    double area = tri.getArea();
    tri.scale(0.5);
    BOOST_CHECK_CLOSE_FRACTION(tri.getArea(), area/4, EPS);
  }
  BOOST_AUTO_TEST_CASE(vertexs_tri)
  {
    BOOST_CHECK_THROW(anikin::Triangle tri({10, 10}, {10, 10}, {10, 10}), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(scale_tri)
  {
    anikin::Triangle tri{{0, 0}, {10, 10}, {20, 0}};
    BOOST_CHECK_THROW(tri.scale(-99), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()
