#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"


void showInfo(const anikin::Shape &shape)
{
  std::cout << "Frame's width: ";
  std::cout << shape.getFrameRect().width << std::endl;
  std::cout << "Frame's height: ";
  std::cout << shape.getFrameRect().height << std::endl;
  std::cout << "Frame's center (" << shape.getFrameRect().pos.x << " , "
  << shape.getFrameRect().pos.y << ")" << std::endl;
  std::cout << "Area: ";
  std::cout << shape.getArea() << std::endl << std::endl;
}

int main()
{
  anikin::Rectangle rect(30, 50, {7, 5});
  anikin::Rectangle rect1(30, 70, {20, 5});
  
  std::shared_ptr <anikin::Shape> pRect = std::make_shared <anikin::Rectangle> (rect);
  std::shared_ptr <anikin::Shape> pRect1 = std::make_shared <anikin::Rectangle> (rect1);
  anikin::CompositeShape compositeShape(pRect);


  std::cout << "rect info: " << std::endl;
  showInfo(rect);

  std::cout << "rect1 info: " << std::endl;
  showInfo(rect1);

  std::cout << "Composite shape only with first shape:" << std::endl << std::endl;
  showInfo(compositeShape);
  std::cout << "Add second shape:" << std::endl << std::endl;
  compositeShape.addShape(pRect1);
  showInfo(compositeShape);
  std::cout << "Scale x5" << std::endl << std::endl;
  compositeShape.scale(5.0);
  showInfo(compositeShape);
  std::cout << "Move composite shape to (30, 60)" << std::endl << std::endl;
  compositeShape.move({30.0, 60.0});
  showInfo(compositeShape);
  std::cout << "Delete shape (1)" << std::endl << std::endl;
  compositeShape.deleteShape(1);
  showInfo(compositeShape);
  std::cout << "Delete all shapes" << std::endl << std::endl;
  compositeShape.deleteAllShapes();
  showInfo(compositeShape);

  return 0;
}
