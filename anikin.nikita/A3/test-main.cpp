#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double EPS = 0.00001;

BOOST_AUTO_TEST_SUITE(Composite_shape_tests)

  BOOST_AUTO_TEST_CASE(Move_to_point_test)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);
    compositeShape.move({25.0, 40.0});

    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().pos.x, 25.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().pos.y, 40.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Move_dx_dy_and_area_and_frame_rect_test)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);
    compositeShape.move(50.0, -40.0);

    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().pos.x, 100.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().pos.y, 15.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getArea(), M_PI * pow(45.0, 2) + 30.0 * 40.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().width, 90.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().height, 120.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Scale_width_and_height_test)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);
    const anikin::point_t frame_center = compositeShape.getFrameRect().pos;
    compositeShape.scale(3.0);

    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().width, ((50.0 + (3.0 - 1.0) * (50.0 - frame_center.x)) + 3.0 * 45.0 - (20.0 + (3.0 - 1.0) * (20.0 - frame_center.x)) + 3.0 * 15.0), EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().height, ((70.0 + (3.0 - 1.0) * (70.0 - frame_center.y)) + 3.0 * 45.0 - (15.0 + (3.0 - 1.0) * (15.0 - frame_center.y)) + 3.0 * 20.0), EPS);

    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getArea(), (M_PI * pow(45.0, 2) + (30.0 * 40.0)) * pow(3.0, 2), EPS);
  }

  BOOST_AUTO_TEST_CASE(Move_to_point_area_test)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);
    compositeShape.move({25.0, 40.0});

    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getArea(), M_PI * pow(45.0, 2) + 30.0 * 40.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Move_dx_dy_area_test)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);
    compositeShape.move(45.0, 80.0);

    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getArea(), M_PI * pow(45.0, 2) + 30.0 * 40.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Move_dx_dy_height_width_test)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);
    compositeShape.move(45.0, 80.0);

    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().height, 120.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().width, 90.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Move_to_point_height_width_test)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);
    compositeShape.move({25.0, 40.0});

    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().height, 120.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().width, 90.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Delete_shape_test)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);
    compositeShape.deleteShape(1);

    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getArea(), rectangle.getArea(), EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().pos.x, rectangle.getFrameRect().pos.x, EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().pos.y, rectangle.getFrameRect().pos.y, EPS);
  }

  BOOST_AUTO_TEST_CASE(Delete_all_shape_test)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle > (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle > (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);
    compositeShape.deleteAllShapes();

    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getArea(), 0.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().pos.x, 0.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(compositeShape.getFrameRect().pos.y, 0.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Invalid_scale_coefficient_test)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);

    BOOST_CHECK_THROW(compositeShape.scale(-10.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_shape_number_when_deleting)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);

    BOOST_CHECK_THROW(compositeShape.deleteShape(7), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Empty_composite_shape_when_deleting)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pCircle);
    compositeShape.addShape(pRectangle);
    compositeShape.deleteAllShapes();

    BOOST_CHECK_THROW(compositeShape.deleteShape(1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Null_pointer_error)
  {
    BOOST_CHECK_THROW(anikin::CompositeShape compositeShape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Null_pointer_when_adding_shape)
  {
    anikin::Circle circle (45.0, {50.0, 70.0});
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pCircle = std::make_shared <anikin::Circle> (circle);
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pRectangle);
    pCircle = nullptr;

    BOOST_CHECK_THROW(compositeShape.addShape(pCircle), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Adding_already_existed_shape)
  {
    anikin::Rectangle rectangle (30.0, 40.0, {20.0, 15.0});
    std::shared_ptr <anikin::Shape> pRectangle = std::make_shared <anikin::Rectangle> (rectangle);
    anikin::CompositeShape compositeShape(pRectangle);

    BOOST_CHECK_THROW(compositeShape.addShape(pRectangle), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()
