#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"


BOOST_AUTO_TEST_SUITE(Matrix_tests)

  BOOST_AUTO_TEST_CASE(Matrix_shapes_test)
  {
    anikin::Circle circle{2.0, {0.0, 0.0}};
    anikin::Rectangle rectangle{2.0, 2.0, {1.0, 0.0}};
    anikin::Rectangle rectangle2{4.0, 4.0, {-3.0, 0.0}};
    anikin::Circle circle2{5.0, {3.0, 2.0}};

    std::shared_ptr< anikin::Shape > pCircle = std::make_shared<anikin::Circle>(circle);
    std::shared_ptr< anikin::Shape > pRectangle = std::make_shared<anikin::Rectangle>(rectangle);
    std::shared_ptr< anikin::Shape > pRectangle2 = std::make_shared<anikin::Rectangle>(rectangle2);
    std::shared_ptr< anikin::Shape > pCircle2 = std::make_shared<anikin::Circle>(circle2);

    anikin::Matrix matrix(pCircle);
    matrix.addShape(pRectangle);
    matrix.addShape(pRectangle2);
    matrix.addShape(pCircle2);

    std::shared_ptr <anikin::Shape>layer1_shape0 = matrix[0][0];
    std::shared_ptr <anikin::Shape>layer2_shape0 = matrix[1][0];
    std::shared_ptr <anikin::Shape>layer2_shape1 = matrix[1][1];
    std::shared_ptr <anikin::Shape>layer3_shape0 = matrix[2][0];

    BOOST_REQUIRE_EQUAL(layer1_shape0, pCircle);
    BOOST_REQUIRE_EQUAL(layer2_shape0, pRectangle);
    BOOST_REQUIRE_EQUAL(layer2_shape1, pRectangle2);
    BOOST_REQUIRE_EQUAL(layer3_shape0, pCircle2);
  }

  BOOST_AUTO_TEST_CASE(Matrix_shapes_frameRects_test)
  {
    anikin::Circle circle{2.0, {0.0, 0.0}};
    anikin::Rectangle rectangle{2.0, 2.0, {1.0, 0.0}};
    anikin::Rectangle rectangle2{4.0, 4.0, {-3.0, 0.0}};
    anikin::Circle circle2{5.0, {3.0, 2.0}};

    std::shared_ptr<anikin::Shape> pCircle = std::make_shared<anikin::Circle >(circle);
    std::shared_ptr<anikin::Shape> pRectangle = std::make_shared<anikin::Rectangle >(rectangle);
    std::shared_ptr<anikin::Shape> pRectangle2 = std::make_shared<anikin::Rectangle >(rectangle2);
    std::shared_ptr<anikin::Shape> pCircle2 = std::make_shared<anikin::Circle >(circle2);

    anikin::Matrix matrix(pCircle);
    matrix.addShape(pRectangle);
    matrix.addShape(pRectangle2);
    matrix.addShape(pCircle2);

    std::unique_ptr<std::shared_ptr<anikin::Shape>[]>layer1 = matrix[0];
    std::unique_ptr<std::shared_ptr<anikin::Shape>[]>layer2 = matrix[1];
    std::unique_ptr<std::shared_ptr<anikin::Shape>[]>layer3 = matrix[2];

    BOOST_REQUIRE_EQUAL(layer1[0]->getFrameRect().pos.x, 0.0);
    BOOST_REQUIRE_EQUAL(layer2[0]->getFrameRect().pos.x, 1.0);
    BOOST_REQUIRE_EQUAL(layer2[1]->getFrameRect().pos.x, -3.0);
    BOOST_REQUIRE_EQUAL(layer3[0]->getFrameRect().pos.x, 3.0);

    BOOST_REQUIRE_EQUAL(layer1[0]->getFrameRect().pos.y, 0.0);
    BOOST_REQUIRE_EQUAL(layer2[0]->getFrameRect().pos.y, 0.0);
    BOOST_REQUIRE_EQUAL(layer2[1]->getFrameRect().pos.y, 0.0);
    BOOST_REQUIRE_EQUAL(layer3[0]->getFrameRect().pos.y, 2.0);
  }

  BOOST_AUTO_TEST_CASE(Matrix_nullptr_shape)
  {
    anikin::Circle circle{2.0, {0.0, 0.0}};
    std::shared_ptr< anikin::Shape > pCircle = std::make_shared< anikin::Circle >(circle);
    pCircle = nullptr;

    BOOST_CHECK_THROW(anikin::Matrix matrix(pCircle), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
