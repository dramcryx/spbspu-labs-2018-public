#include <cmath>
#include "circle.hpp"
#include <stdexcept>

anikin::Circle::Circle(const double &r, const anikin::point_t &p) : radius_(r), angle_(0.0), center_(p)
{
  if (radius_ <= 0)
  {
    throw std::invalid_argument("Invalid radius");
  }
}

double anikin::Circle::getArea() const noexcept
{
  return M_PI * pow(radius_, 2);
}

double anikin::Circle::getAngle() const noexcept
{
  return angle_;
}

anikin::rectangle_t anikin::Circle::getFrameRect() const noexcept
{
  return {2 * radius_, 2 * radius_, center_};
}

void anikin::Circle::move(const anikin::point_t &placement) noexcept
{
  center_ = placement;
}

void anikin::Circle::move(const double x, const double y) noexcept
{
  center_.x += x;
  center_.y += y;
}

void anikin::Circle::scale(const double coef)
{
  if (coef<0.0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  else
  {
    radius_ *= coef;
  }
}

void anikin::Circle::rotate(const double angle) noexcept
{
  angle_ += angle;
}
