#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace anikin
{
  class Circle : public anikin::Shape
  {
  public:
    Circle(const double &r, const anikin::point_t &p);

    double getArea() const noexcept override;
    double getAngle() const noexcept override;
    anikin::rectangle_t getFrameRect() const noexcept override;

    void move(const anikin::point_t &placement) noexcept override;
    void move(const double x, const double y) noexcept override;

    void scale(const double coef) override;
    void rotate(const double angle) noexcept override;

  private:
    double radius_;
    double angle_;
    anikin::point_t center_;
  };
}

#endif
