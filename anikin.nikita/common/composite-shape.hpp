#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace anikin
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape(const std::shared_ptr <Shape>shape);
    CompositeShape(const CompositeShape &compositeShape);
    CompositeShape(CompositeShape && compositeShape);
    ~CompositeShape();

    CompositeShape & operator = (const CompositeShape &compositeShape);
    CompositeShape & operator = (CompositeShape && compositeShape);
    bool operator == (const CompositeShape & compositeShape) const;
    bool operator != (const CompositeShape & compositeShape) const;

    rectangle_t getFrameRect() const noexcept override;
    double getArea() const noexcept override;
    double getAngle() const noexcept override;

    void move(const double dx, const double dy) noexcept override;
    void move(const point_t & newPoint) noexcept override;

    void scale(const double coef) override;
    void rotate(const double angle) noexcept override;

    void addShape(std::shared_ptr <Shape> shape);
    void deleteShape(const int shapeNumber);
    void deleteAllShapes() noexcept;

  private:
    std::unique_ptr <std::shared_ptr <Shape>[]>shapes_;
    int size_;
    double angle_;
  };
}
#endif
