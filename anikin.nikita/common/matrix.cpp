#include "matrix.hpp"

#include <memory>
#include <stdexcept>

using namespace anikin;

Matrix::Matrix(const std::shared_ptr<Shape>shape):
  matrixShapes_(nullptr),
  layersNumber_(0),
  layerSize_(0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null pointer");
  }
  addShape(shape);
}

Matrix::Matrix(const Matrix & matrix):
  matrixShapes_(new std::shared_ptr<Shape>[matrix.layersNumber_ * matrix.layerSize_]),
  layersNumber_(matrix.layersNumber_),
  layerSize_(matrix.layerSize_)
{
  for (int i = 0; i < layersNumber_ * layerSize_; ++i)
  {
    matrixShapes_[i] = matrix.matrixShapes_[i];
  }
}

Matrix::Matrix(Matrix && matrix):
  matrixShapes_(nullptr),
  layersNumber_(matrix.layersNumber_),
  layerSize_(matrix.layerSize_)
{
  matrixShapes_.swap(matrix.matrixShapes_);
  matrix.layersNumber_ = 0;
  matrix.layerSize_ = 0;
}

Matrix::~Matrix()
{
  matrixShapes_.reset();
  matrixShapes_ = nullptr;
  layersNumber_ = 0;
  layerSize_ = 0;
}

Matrix & Matrix::operator=(const Matrix & matrix)
{
  if (this != & matrix)
  {
    layersNumber_ = matrix.layersNumber_;
    layerSize_ = matrix.layerSize_;
    std::unique_ptr < std::shared_ptr <Shape>[] >
      newMatrixShapes(new std::shared_ptr <Shape> [layersNumber_ * layerSize_]);
    for (int i = 0; i < layersNumber_ * layerSize_; ++i)
    {
      newMatrixShapes[i] = matrix.matrixShapes_[i];
    }
    matrixShapes_.swap(newMatrixShapes);
  }
  return *this;
}

Matrix & Matrix::operator=(Matrix && matrix)
{
  if (this != & matrix)
  {
    layersNumber_ = matrix.layersNumber_;
    layerSize_ = matrix.layerSize_;
    matrixShapes_.reset();
    matrixShapes_.swap(matrix.matrixShapes_);
    matrix.layersNumber_ = 0;
    matrix.layerSize_ = 0;
  }
  return *this;
}

bool Matrix::operator==(const Matrix & matrix) const
{
  if ((this->layersNumber_ != matrix.layersNumber_) || (this->layerSize_ != matrix.layerSize_))
  {
    return false;
  }
  for (int i = 0; i < layersNumber_ * layerSize_; ++i)
    {
      if (this->matrixShapes_[i] != matrix.matrixShapes_[i])
      {
        return false;
      }
    }
  return true;
}

bool Matrix::operator!=(const Matrix & matrix) const
{
  if ((this->layersNumber_ != matrix.layersNumber_) || (this->layerSize_ != matrix.layerSize_))
  {
    return true;
  }
    for (int i = 0; i < layersNumber_ * layerSize_; ++i)
    {
      if (this->matrixShapes_[i] != matrix.matrixShapes_[i])
      {
        return true;
      }
    }
  return false;
}

std::unique_ptr <std::shared_ptr <Shape>[]> Matrix::operator[](const int layerNumber) const
{
  if ((layerNumber < 0) || (layerNumber >= layersNumber_))
  {
    throw std::out_of_range("invalid layer index");
  }
  std::unique_ptr <std::shared_ptr <Shape>[]>
    layer(new std::shared_ptr <Shape> [layerSize_]);
  for (int i = 0; i < layerSize_; ++i)
  {
    layer[i] = matrixShapes_[layerNumber * layerSize_ + i];
  }
  return layer;
}

void Matrix::addShape(const std::shared_ptr<Shape> shape) noexcept
{
  if (layersNumber_ == 0)
  {
    ++layersNumber_;
    ++layerSize_;
    std::unique_ptr <std::shared_ptr <Shape>[]>
      newMatrixShapes(new std::shared_ptr <Shape> [layersNumber_ * layerSize_]);
    matrixShapes_.swap(newMatrixShapes);
    matrixShapes_[0] = shape;
  }
  else
  {
    bool addedShape = false;
    for (int i = 0; !addedShape ; ++i)
    {
      for (int j = 0; j < layerSize_; ++j)
      {
        if (!matrixShapes_[i * layerSize_ + j])
        {
          matrixShapes_[i * layerSize_ + j] = shape;
          addedShape = true;
          break;
        }
        else
        {
          if (checkForOverlap(i * layerSize_ + j, shape))
          {
            break;
          }
        }

        if (j == (layerSize_ - 1))
        {
          layerSize_++;
          std::unique_ptr <std::shared_ptr <Shape>[]>
            newMatrixShapes(new std::shared_ptr <Shape>[layersNumber_ * layerSize_]);
          for (int n = 0; n < layersNumber_; ++n)
          {
            for (int m = 0; m < layerSize_ - 1; ++m)
            {
              newMatrixShapes[n * layerSize_ + m] = matrixShapes_[n * (layerSize_ - 1) + m];
            }
            newMatrixShapes[(n + 1) * layerSize_ - 1] = nullptr;
          }
          newMatrixShapes[(i + 1) * layerSize_ - 1] = shape;
          matrixShapes_.swap(newMatrixShapes);
          addedShape = true;
          break;
        }
      }
      if ((i == (layersNumber_ - 1)) && !addedShape)
      {
        layersNumber_++;
        std::unique_ptr < std::shared_ptr <Shape>[]>
          newMatrixShapes(new std::shared_ptr<Shape>[layersNumber_ * layerSize_]);
        for (int n = 0; n < ((layersNumber_ - 1) * layerSize_); ++n)
        {
          newMatrixShapes[n] = matrixShapes_[n];
        }
        for (int n = ((layersNumber_  - 1) * layerSize_) ; n < (layersNumber_ * layerSize_); ++n)
        {
          newMatrixShapes[n] = nullptr;
        }
        newMatrixShapes[(layersNumber_ - 1 ) * layerSize_] = shape;
        matrixShapes_.swap(newMatrixShapes);
        addedShape = true;
      }
    }
  }
}

bool Matrix::checkForOverlap(const int index, std::shared_ptr <Shape> shape) const noexcept
{
  rectangle_t nShapeFrameRect = shape->getFrameRect();
  rectangle_t mShapeFrameRect = matrixShapes_[index]->getFrameRect();
  point_t newShapePoints[4] = {
    {nShapeFrameRect.pos.x - nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y + nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x + nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y + nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x + nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y - nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x - nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y - nShapeFrameRect.height / 2.0}
  };

  point_t matrixShapePoints[4] = {
    {mShapeFrameRect.pos.x - mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y + mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x + mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y + mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x + mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y - mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x - mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y - mShapeFrameRect.height / 2.0}
  };

  for (int i = 0; i < 4; ++i)
  {
    if (((newShapePoints[i].x >= matrixShapePoints[0].x) && (newShapePoints[i].x <= matrixShapePoints[2].x)
      && (newShapePoints[i].y >= matrixShapePoints[3].y) && (newShapePoints[i].y <= matrixShapePoints[1].y))
        || ((matrixShapePoints[i].x >= newShapePoints[0].x) && (matrixShapePoints[i].x <= newShapePoints[2].x)
          && (matrixShapePoints[i].y >= newShapePoints[3].y) && (matrixShapePoints[i].y <= newShapePoints[1].y)))
    {
      return true;
    }
  }
  return false;
}
