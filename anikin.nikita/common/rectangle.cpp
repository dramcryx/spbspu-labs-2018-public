#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

anikin::Rectangle::Rectangle(const double &w, const double &h, const point_t &p) : width_(w), height_(h), angle_(0.0), center_(p)
{
  if (w <= 0)
  {
    throw std::invalid_argument("Invalid rectangle width");
  }
  if (h <= 0)
  {
    throw std::invalid_argument("Invalid rectangle height");
  }
}

double anikin::Rectangle::getArea() const noexcept
{
  return width_ * height_;
}

double anikin::Rectangle::getAngle() const noexcept
{
  return angle_;
}

anikin::rectangle_t anikin::Rectangle::getFrameRect() const noexcept
{
  double angle = (angle_ * M_PI) / 180;
  const double frameWidth = width_ * abs(cos(angle)) + height_ * abs(sin(angle));
  const double frameHeight = width_ * abs(sin(angle)) + height_ * abs(cos(angle));
  return {frameWidth, frameHeight, center_};
}


void anikin::Rectangle::move(const anikin::point_t &placement) noexcept
{
  center_ = placement;
}

void anikin::Rectangle::move(const double x, const double y) noexcept
{
  center_.x += x;
  center_.y += y;
}

void anikin::Rectangle::scale(const double coef)
{
  if (coef < 0.0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  else 
  {
    width_ *= coef;
    height_ *= coef;
  }
}

void anikin::Rectangle::rotate(const double angle) noexcept
{
  angle_ += angle;
}
