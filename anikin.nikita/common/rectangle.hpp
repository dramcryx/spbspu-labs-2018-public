#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace anikin
{
  class Rectangle : public anikin::Shape
  {
  public:
    Rectangle(const double &w, const double &h, const anikin::point_t &p);

    double getArea() const noexcept override;
    double getAngle() const noexcept override;
    anikin::rectangle_t getFrameRect() const noexcept override;

    void move(const anikin::point_t &placement) noexcept override;
    void move(const double x, const double y) noexcept override;

    void scale(const double coef) override;
    void rotate(const double angle) noexcept override;

  private:
    double width_;
    double height_;
    double angle_;
    anikin::point_t center_;
  };
}

#endif
