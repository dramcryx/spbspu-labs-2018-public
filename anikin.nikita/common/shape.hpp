#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"

namespace anikin
{
  class Shape
  {
  public:
    virtual ~Shape() = default;

    virtual double getArea() const noexcept = 0;
    virtual double getAngle() const noexcept = 0;
    virtual anikin::rectangle_t getFrameRect() const noexcept = 0;

    virtual void move(const anikin::point_t &placement) noexcept = 0;
    virtual void move(const double x, const double y) noexcept = 0;

    virtual void scale(const double coef) = 0;
    virtual void rotate(const double angle) noexcept = 0;
  };
}

#endif
