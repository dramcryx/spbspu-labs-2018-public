#include "triangle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

const double delta = 0.00001;
anikin::Triangle::Triangle(const anikin::point_t &a, const anikin::point_t &b, const anikin::point_t &c) :
pA_(a), pB_(b), pC_(c), center_({(a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3})
{
  if (getArea() <= delta)
  {
    throw std::invalid_argument("Invalid points");
  }
}

double anikin::Triangle::getMin(const double &pA_, const double &pB_, const double &pC_) const noexcept
{
  double min = pA_;
  if (pB_ < min)
  {
    min=pB_;
  }
  if (pC_ < min)
  {
    min = pC_;
  }
  return min;
}

double anikin::Triangle::getMax(const double &pA_, const double &pB_, const double &pC_) const noexcept
{
  double max = pA_;
  if (pB_ > max)
  {
    max = pB_;
  }
  if (pC_ > max)
  {
    max = pC_;
  }
  return max;
}

double anikin::Triangle::getArea() const noexcept
{
  return ((std::abs(((pA_.x - pC_.x) * (pB_.y - pC_.y)) - ((pA_.y - pC_.y) * (pB_.x - pC_.x))) / 2));
}

double anikin::Triangle::getAngle() const noexcept
{
  std::cout << "Triangle has not field angle_, rotation just changes vertices" << std::endl;
  return 0;
}

anikin::rectangle_t anikin::Triangle::getFrameRect() const noexcept
{
  double max_x = getMax(pA_.x, pB_.x, pC_.x);
  double min_x = getMin(pA_.x, pB_.x, pC_.x);
  double max_y = getMax(pA_.y, pB_.y, pC_.y);
  double min_y = getMin(pA_.y, pB_.y, pC_.y);
  return anikin::rectangle_t{max_x - min_x, max_y - min_y, point_t {min_x + (max_x - min_x) / 2,min_y + (max_y - min_y) / 2}};
}

void anikin::Triangle::move(const anikin::point_t &placement) noexcept
{
  pA_.x += placement.x - center_.x;
  pB_.x += placement.x - center_.x;
  pC_.x += placement.x - center_.x;
  pA_.y += placement.y - center_.y;
  pB_.y += placement.y - center_.y;
  pC_.y += placement.y - center_.y;
  center_ = placement;
}

void anikin::Triangle::move(const double x, const double y) noexcept
{
  center_.x += x;
  pA_.x += x;
  pB_.x += x;
  pC_.x += x;
  center_.y += y;
  pA_.y += y;
  pB_.y += y;
  pC_.y += y;
}

void anikin::Triangle::scale(const double coef)
{
  if (coef <= 0.0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  pA_ = {center_.x + (pA_.x - center_.x) * coef, center_.y + (pA_.y - center_.y) * coef};
  pB_ = {center_.x + (pB_.x - center_.x) * coef, center_.y + (pB_.y - center_.y) * coef};
  pC_ = {center_.x + (pC_.x - center_.x) * coef, center_.y + (pC_.y - center_.y) * coef};
}

void anikin::Triangle::rotate(double angle) noexcept
{
  angle = (angle * M_PI) / 180;
  const double sin_a = sin(angle);
  const double cos_a = cos(angle);
  pA_ = {center_.x + (pA_.x - center_.x) * cos_a - (pA_.y - center_.y) * sin_a,
      center_.y + (pA_.y - center_.y) * cos_a + (pA_.x - center_.x) * sin_a};
  pB_ = {center_.x + (pB_.x - center_.x) * cos_a - (pB_.y - center_.y) * sin_a,
      center_.y + (pB_.y - center_.y) * cos_a + (pB_.x - center_.x) * sin_a};
  pC_ = {center_.x + (pC_.x - center_.x) * cos_a - (pC_.y - center_.y) * sin_a,
      center_.y + (pC_.y - center_.y) * cos_a + (pC_.x - center_.x) * sin_a};
}
