#include <iostream>
#include "circle.hpp"
#include "base-types.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"
using std::cout;
using std::endl;

int main() 
{
  try
  {
    std::shared_ptr<averin::Rectangle> rec = std::make_shared<averin::Rectangle>(1,1,0,0);
    std::shared_ptr<averin::Rectangle> rec2 = std::make_shared<averin::Rectangle>(2,2,0,0);
    std::shared_ptr<averin::Rectangle> rec3 = std::make_shared<averin::Rectangle>(4,4,0,0);
    std::shared_ptr<averin::Rectangle> rec4 = std::make_shared<averin::Rectangle>(6,6,0,0);
    averin::Rectangle rec5(15,10,1,7);
    std::shared_ptr<averin::CompositeShape> shp = std::make_shared<averin::CompositeShape>(&rec5);  
    averin::Circle cir1(3,5,9);
    shp->addShape(&cir1);
    shp->rotate(90);
    shp->printInfo();
    std::shared_ptr<averin::Circle> cir2 = std::make_shared<averin::Circle>(1,1000,1000);
    std::shared_ptr<averin::Circle> cir3 = std::make_shared<averin::Circle>(10,-1000,1);
    averin::Matrix matrix;
    matrix.addShape(rec);
    matrix.addShape(cir3);
    matrix.addShape(rec2);
    matrix.addShape(rec3);
    matrix.addShape(rec4);  
    matrix.addShape(cir2);
    matrix.addShape(shp);
    matrix.printInfo();
  }
  
  catch (const std::exception& e)
  {
    cout << e.what();
    return 1;
  }
  return 0; 
}
