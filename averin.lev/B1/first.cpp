#include "declarations.hpp"
#include <cmath>

void doFirstTask(char* direction)
{
  if (strcmp(direction,"ascending") && strcmp(direction,"descending"))
  {
    throw std::invalid_argument("Invalid argument");
  }
  std::vector<int> vectorBr;
  int num = 0;
  while (std::cin >> num) 
  {
    if (std::cin.fail())
    {
      throw std::invalid_argument("Not a integer value or empty file");
    }
    vectorBr.push_back(num);
  }
  if (!std::cin.eof())
  {
    throw std::invalid_argument("Wrong data");
  }
  
  std::forward_list<int> list(vectorBr.begin(),vectorBr.end());
  std::vector<int> vectorAt = vectorBr;
  sort<detail::BrAccess>(vectorBr,direction);
  sort<detail::AtAccess>(vectorAt,direction);
  sort<detail::ItAccess>(list,direction);
  printCollection(vectorBr);
  printCollection(vectorAt);
  printCollection(list);
}
