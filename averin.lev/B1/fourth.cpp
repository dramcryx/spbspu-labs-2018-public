#include "declarations.hpp"
#include <random>
#include <ctime>

void fillRandom(double *array, int size)
{
  std::mt19937 gen(time(0));
  std::uniform_real_distribution<double> dist(-1, 1);
  for (int i = 0; i < size; i++)
  {
    array[i] = dist(gen);
  }
}


void doFourthTask(char* direction, long long int size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Invalid size of vector");
  }
  if (strcmp(direction,"ascending") && strcmp(direction,"descending"))
  {
    throw std::invalid_argument("Invalid argument");
  }
  std::vector<double> vector(size);
  fillRandom(&vector[0],size);
  printCollection(vector);
  sort<detail::ItAccess>(vector,direction);
  printCollection(vector);
}
