#include "declarations.hpp"

int main(int argc, char* argv[]) 
{
  try 
  {
    int num = 0;
    if (std::stoi(argv[1]))
    {
      num = std::stoi(argv[1]);
    }
    else
    {
      throw std::invalid_argument("Invalid argument");
    }
    if ((argc != 3 && num != 3 && num != 4) || (num == 3 && argc != 2) ||
        (num == 4 && argc != 4))
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
    switch (num)
    {
    case 1:
    {
      doFirstTask(argv[2]);
      break;
    }
    case 2:
    {
      doSecondTask(argv[2]);
      break;
    }
    case 3:
    {
      doThirdTask();
      break;
    }
    case 4:
    {
      doFourthTask(argv[2],atoi(argv[3]));
      break;
    }
    default:
    {
      throw std::invalid_argument("Invalid argument");
    }
    }
  }
  catch (const std::exception& e)
  {
    std::cout << e.what() <<std::endl;
    return 1;
  }
  return 0;
}
