#include <cstdlib>
#include <iostream>
#include "circle.hpp"
#include "base-types.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

using std::cout;
using std::endl;
using namespace babaev;

void test(Shape& obj)
{
  obj.printInfo();
  point_t cc;
  cc.x = 350;
  cc.y = 150;
  obj.scale(4);
  obj.move(cc);
  obj.move(75, 65);
  obj.printInfo();
}

int main()
{
  try
  {
    Rectangle rec(10, 15, 30, 30);
    Circle cir(5, 10, 5);
    Triangle tri(5, 5, 5, 15, 10, 10);
    CompositeShape shp(&cir);
    shp.printInfo();
    shp.addShape(&rec);
    shp.printInfo();
    shp.addShape(&tri);
    shp.printInfo();
    test(shp);
    shp.deleteShape(2);
    shp.printInfo();
  }
  catch (const std::exception& e)
  {
    cout << e.what();
    return 1;
  }
  return 0;
}
