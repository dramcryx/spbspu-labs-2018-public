#include <iostream>
#include <cmath>
#include "circle.hpp"

using std::cout;
using std::endl;
using namespace babaev;

Circle::Circle(double r, double x, double y) :
  rad_(r),
  center_({x, y}),
  angle_(0)
{
  if (r < 0.0)
  {
    throw std::invalid_argument("Radius must be > 0, or = 0");
  }
}

double Circle::getArea() const
{
  return rad_ * rad_ * M_PI;
}

rectangle_t Circle::getFrameRect() const
{
  return {2 * rad_, 2 * rad_, center_};
}

void Circle::move(point_t c)
{
  center_ = c;
}

void Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Circle::printInfo() const
{
  cout << "CIRCLE"<< endl;
  cout << "Area of circle: " << getArea() << endl;
  cout << "Cords of center: " << center_.x << " " << center_.y << endl;
  rectangle_t frec;
  frec = getFrameRect();
  cout << "FRAME RECTANGLE" << endl << "H: " << frec.height << endl;
  cout << "W: " << frec.width << endl << "x, y: " << frec.pos.x << " " << frec.pos.y << endl << endl;
}

void Circle::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Ratio must be >= 0");
  }
  rad_ *= ratio;
}

double Circle::getRad() const
{
  return rad_;
}

void Circle::rotate(double deg)
{
  angle_ -= deg;
  while (angle_ < 0.0)
  {
    angle_ += 360.0;
   
  }
  while (angle_ > 360.0)
  {
    angle_ -= 360.0;
  }
}
