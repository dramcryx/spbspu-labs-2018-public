#include <iostream>
#include <cmath>
#include "composite-shape.hpp"

using std::cout;
using std::endl;
using namespace babaev;

CompositeShape::CompositeShape(Shape *obj) :
  a_(new Shape *[1]),
  size_(1),
  angle_(0)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Wrong pointer");
  }
  a_[0] = obj;
}

CompositeShape::CompositeShape() :
  a_(new Shape *[0]),
  size_(0),
  angle_(0)
{
  
}


double CompositeShape::getArea() const
{
  double sumArea = 0;
  for (int i = 0; i < size_; i++)
  {
    sumArea += a_[i]->getArea();
  }
  return sumArea;
}

rectangle_t CompositeShape::getFrameRect() const
{
  rectangle_t rect = a_[0]->getFrameRect();
  double maxX = rect.pos.x + rect.width/2;
  double minX = rect.pos.x - rect.width/2;
  double maxY = rect.pos.y + rect.height/2;
  double minY = rect.pos.y - rect.height/2;
  
  for (int i = 1; i < size_; i++)
  {
    rect = a_[i]->getFrameRect();
    if ((rect.pos.x + rect.width/2) > maxX)
    {
      maxX = rect.pos.x + rect.width/2;
    }
    if ((rect.pos.x - rect.width/2) > minX)
    {
      minX = rect.pos.x - rect.width/2;
    } 
    if ((rect.pos.y + rect.height/2) > maxY)
    {
      maxY = rect.pos.y + rect.height/2;
    }
    if ((rect.pos.y - rect.height/2) > minY)
    {
      minY = rect.pos.y - rect.height/2;
    }     
  }
  point_t center;
  center.x = maxX - (maxX-minX)/2;
  center.y = maxY - (maxY-minY)/2;
  return {maxX-minX,maxY-minY,center}; 
}

void CompositeShape::move(point_t c)
{
  for (int i = 0; i < size_; i++)
  {
    a_[i]->move(c);
  }
}

void CompositeShape::move(double dx, double dy)
{
  for (int i = 0; i < size_; i++)
  {
    a_[i]->move(dx,dy);
  }
}

void CompositeShape::printInfo() const
{
  cout << endl << "COMPOSITE SHAPE<<<"<< endl;
  cout << "Number of elements: " << size_ << endl;
  cout << "Area " << getArea() << endl;
  rectangle_t frect;
  frect = getFrameRect();
  cout << endl << "FRAME RECTANGLE" << endl << "H: " << frect.height << endl;
  cout << "W: " << frect.width << endl << "x, y: " << frect.pos.x << " " << frect.pos.y << endl;
}

void CompositeShape::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Ratio must be >= 0");
  }
  for (int i = 0; i < size_; i++)
  {
    a_[i]->scale(ratio);
  }
}

void CompositeShape::addShape(Shape* obj)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Wrong pointer");
  }
  std::unique_ptr<Shape *[]> tempArr (new Shape*[size_+1]);
  for (int i = 0; i < size_; i++)
  {
    tempArr[i] = a_[i];
  }
  tempArr[size_] = obj;
  a_.swap(tempArr);
  size_++;
}

void CompositeShape::deleteShape(int index)
{
  if (index > size_ - 1)
  {
    throw std::invalid_argument("Wrong index");
  }
  std::unique_ptr<Shape *[]> tempArr (new Shape*[size_-1]);
  for (int i = 0; i < size_; i++)
  {
    if (i != index)
    {
      if (i > index)
      {
        tempArr[i-1] = a_[i]; 
      }
      else
      {
        tempArr[i] = a_[i];
      }
    }
  }
  a_.swap(tempArr);
  size_--;
}

void CompositeShape::rotate(double deg)
{
  angle_ = deg;
  for (int i = 0; i < size_; i++)
  {
    a_[i]->rotate(deg);
    rectangle_t recComp = getFrameRect();
    rectangle_t recShp = a_[i]->getFrameRect();
    double x = recComp.pos.x;
    double y = recComp.pos.y;
    double x0 = recShp.pos.x;
    double y0 = recShp.pos.y;
    double degRad = deg * (M_PI/180);
    double rx = x0 - x;
    double ry = y0 - y;
    double c = cos(degRad);
    double s = sin(degRad);
    double x1 = x + rx * c - ry * s;
    double y1 = y + rx * s + ry * c;
    a_[i]->move({x1,y1});
  } 
}
