#include <iostream>
#include <cmath>
#include "rectangle.hpp"

using std::cout;
using std::endl;
using namespace babaev;

Rectangle::Rectangle(double h, double w, double x, double y) :
  center_({x, y}),
  height_(h),
  width_(w),
  angle_(0),
  TopLeft_({x-w/2,y+h/2})
{
  if (h < 0.0 || w < 0.0)
  {
    throw std::invalid_argument("W and h of the rectangle must be >= 0");
  }
}

void Rectangle::printInfo() const
{
  cout << "RECTANGLE"<< endl;
  cout << "Area of rectangle: " << getArea() << endl;
  cout << "Cords of center of rectangle: " << center_.x << " " << center_.y << endl;
  rectangle_t frec;
  frec = getFrameRect();
  cout << "FRAME RECTANGLE" << endl << "H: " << frec.height << endl;
  cout << "W: " << frec.width << endl << "x, y: " << frec.pos.x << " " << frec.pos.y << endl << endl;
}

void Rectangle::move(point_t c)
{
  center_ = c;
}

void Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

double Rectangle::getArea() const
{
  return height_ * width_;
}

rectangle_t Rectangle::getFrameRect() const
{
 return {width_, height_, center_};
}

void Rectangle::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("ratio must be >= 0");
  }
  height_ *= ratio;
  width_ *= ratio;
}

double Rectangle::getHeight() const
{
  return height_;
}

double Rectangle::getWidth() const
{
  return width_;
}

void Rectangle::rotate(double deg)
{
  angle_ -= deg;
  while (angle_ < 0.0)
  {
    angle_ += 360.0;
  }
  while (angle_ > 360.0)
  {
    angle_ -= 360.0;
  }
    double x = center_.x;
    double y = center_.y;
    double x0 = TopLeft_.x;
    double y0 = TopLeft_.y;
    double degRad = deg * (M_PI/180);
    double rx = x0 - x;
    double ry = y0 - y;
    double c = cos(degRad);
    double s = sin(degRad);
    TopLeft_.x = x + rx * c - ry * s;
    TopLeft_.y = y + rx * s + ry * c;
}
