#include <iostream>
#include <stdexcept>
#include <cmath>

#include "composite-shape.hpp"

betuganov::CompositeShape::CompositeShape(const std::shared_ptr<betuganov::Shape> shape_ptr):
shapes_(nullptr),
shapeAmount_(0),
angle_(0.0)

{
  if (shape_ptr == nullptr)
  {
    throw std::invalid_argument("this object doesn't exist");
  }
  
  addShape(shape_ptr);
}

betuganov::CompositeShape::CompositeShape(const betuganov::CompositeShape & obj):
shapeAmount_(obj.shapeAmount_),
angle_(obj.angle_)
{
  shapes_ = std::unique_ptr<std::shared_ptr<betuganov::Shape>[]>(new std::shared_ptr<betuganov::Shape>[shapeAmount_]);
  for (size_t i = 0; i < shapeAmount_; i++)
  {
    shapes_[i] = obj.shapes_[i];
  }
}

betuganov::CompositeShape::CompositeShape(betuganov::CompositeShape && obj):
shapes_(std::move(obj.shapes_)),
shapeAmount_(obj.shapeAmount_),
angle_(obj.angle_)
{
  obj.shapeAmount_ = 0;
  obj.shapes_ = nullptr;
}

betuganov::CompositeShape &betuganov::CompositeShape::operator =(const betuganov::CompositeShape & obj)
{
  if (this != &obj)
  {
    shapeAmount_ = obj.shapeAmount_;
    shapes_.reset(new std::shared_ptr<betuganov::Shape>[shapeAmount_]);
    for (size_t i = 0; i < shapeAmount_; i++)
    {
      shapes_[i] = obj.shapes_[i];
    }
  }
  return *this;
}

betuganov::CompositeShape &betuganov::CompositeShape::operator =(betuganov::CompositeShape && obj)
{
  if (this == &obj)
  {
    return *this;
  }
  
  shapeAmount_ = (obj.shapeAmount_);
  shapes_ = std::move(obj.shapes_);
  angle_ = (obj.angle_);
  
  obj.shapeAmount_ = 0;
  obj.shapes_ = nullptr;
  obj.angle_ = 0.0;
  
  return *this;
}

std::shared_ptr<betuganov::Shape> betuganov::CompositeShape::operator [](size_t index)
{
  if(index >= shapeAmount_)
  {
    throw std::out_of_range("index out of range");
  }
  
  return shapes_[index];
}

void betuganov::CompositeShape::addShape(std::shared_ptr <betuganov::Shape> shape_ptr)
{
  if (shape_ptr == nullptr)
  {
    throw std::invalid_argument("this object doesn't exist");
  }
  
  std::unique_ptr<std::shared_ptr <betuganov::Shape>[]> local_array(new std::shared_ptr<betuganov::Shape>[shapeAmount_+1]);
  for (size_t i = 0; i < shapeAmount_; i++)
  {
    local_array[i] = shapes_[i];
  }
  local_array[shapeAmount_++] = shape_ptr;
  shapes_.swap(local_array);
}

void betuganov::CompositeShape::delShape(const size_t index)
{
  if (index >= shapeAmount_)
  {
    throw std::invalid_argument("index is out of range");
  }
  
  if (shapeAmount_ == 0)
  {
    throw std::invalid_argument("this object is empty.");
  }
  
  std::unique_ptr<std::shared_ptr<Shape>[]> local_array(new std::shared_ptr<betuganov::Shape>[shapeAmount_ - 1]);
  for (size_t i = 0; i < index; i++)
  {
    local_array[i] = shapes_[i];
  }
  for (size_t i = index; i < shapeAmount_ - 1; i++)
  {
    local_array[i] = shapes_[i + 1];
  }
  
  shapes_.swap(local_array);
  --shapeAmount_;
}

double betuganov::CompositeShape::getArea() const noexcept
{
  double area = 0.0;
  for (size_t i = 0; i < shapeAmount_; i++)
  {
    area += shapes_[i] -> getArea();
  }
  return area;
}

betuganov::rectangle_t betuganov::CompositeShape::getFrameRect() const noexcept
{
  rectangle_t frameRect = shapes_[0] -> getFrameRect();
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  for (size_t i = 1; i < shapeAmount_; i++)
  {
    frameRect = shapes_[i] -> getFrameRect();
    if (maxX < frameRect.pos.x + frameRect.width / 2)
    {
      maxX = frameRect.pos.x + frameRect.width / 2;
    }
    if (minX > frameRect.pos.x - frameRect.width / 2)
    {
      minX = frameRect.pos.x - frameRect.width / 2;
    }
    if (maxY < frameRect.pos.y + frameRect.height / 2)
    {
      maxY = frameRect.pos.y + frameRect.height / 2;
    }
    if (minY > frameRect.pos.y - frameRect.height / 2)
    {
      minY = frameRect.pos.y - frameRect.height / 2;
    }
  }
  return {maxX - minX, maxY - minY, {(maxX + minX) / 2, (maxY + minY) / 2}};
}

void betuganov::CompositeShape::move(double dx, double dy) noexcept
{
  for (size_t i = 0; i < shapeAmount_; i++)
  {
    shapes_[i] -> move(dx, dy);
  }
}

void betuganov::CompositeShape::move(const betuganov::point_t & p) noexcept
{
  point_t center = getFrameRect().pos;
  double dx = p.x - center.x;
  double dy = p.y - center.y;
  for (size_t i = 0; i < shapeAmount_; i++)
  {
    shapes_[i] -> move(dx, dy);
  }
}

void betuganov::CompositeShape::scale(const double k)
{
  if (k < 0.0)
  {
    throw std::invalid_argument("coefficient should be > 0");
  }
  const point_t comp_shape_pos = getFrameRect().pos;
  for (size_t i =0; i < shapeAmount_; i++)
  {
    point_t shape_pos = shapes_[i] -> getFrameRect().pos;
    shapes_[i] -> move(k * (shape_pos.x - comp_shape_pos.x), k * (shape_pos.y - comp_shape_pos.y));
    shapes_[i] -> scale(k);
  }
}

void betuganov::CompositeShape::rotate(double angle) noexcept
{
  angle_ += angle;
  if (angle_ > 360.0)
  {
    angle_ = fmod(angle_, 360.0);
  }
  point_t center = getFrameRect().pos;
  double sinA = sin(angle_ * M_PI / 180);
  double cosA = cos(angle_ * M_PI / 180);
  for (size_t i = 0; i < shapeAmount_; i++)
  {
    betuganov::point_t shapePos = shapes_[i] -> getFrameRect().pos;
    double newX = center.x + (shapePos.x - center.x) * cosA - (shapePos.y - center.y) * sinA;
    double newY = center.y + (shapePos.x - center.x) * sinA + (shapePos.y - center.y) * cosA;
    shapes_[i] -> move({newX, newY});
    shapes_[i] -> rotate(angle);
  }
}

