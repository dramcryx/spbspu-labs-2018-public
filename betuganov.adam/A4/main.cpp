#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

int main()
{
  betuganov::point_t p = {128.0, 256.0};
  std::shared_ptr<betuganov::Shape> rectPtr (new betuganov::Rectangle(p,24.0, 48.0));
  std::shared_ptr<betuganov::Shape> circlePtr(new betuganov::Circle(p,4.0));
  std::shared_ptr<betuganov::Shape> trianglePtr(new betuganov::Triangle({2.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));

  betuganov::CompositeShape comp_shape(rectPtr);
  comp_shape.addShape(circlePtr);
  comp_shape.addShape(trianglePtr);
  std::cout<< comp_shape.getArea() << "\n";
  comp_shape.rotate(90.0);
  std::cout<< comp_shape.getArea() << "\n";
  comp_shape.move(34.0, 43.0);
  comp_shape.rotate(20.0);
  std::cout<< comp_shape.getArea() << "\n";
  comp_shape.deleteShape(1);

  betuganov::Matrix matr(rectPtr);
  matr.addShape(circlePtr);
  return 0;
}
