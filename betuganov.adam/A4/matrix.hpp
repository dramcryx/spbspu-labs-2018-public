#ifndef A3_MATRIX_HPP
#define A3_MATRIX_HPP
#include "shape.hpp"
#include "composite-shape.hpp"
#include <memory>

namespace betuganov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const std::shared_ptr<betuganov::Shape> shape_ptr);
    Matrix(const betuganov::Matrix & obj);
    Matrix(betuganov::Matrix && obj);
    ~Matrix();

    Matrix &operator =(const betuganov::Matrix & obj);
    Matrix &operator =(betuganov::Matrix && obj);
    std::unique_ptr<std::shared_ptr<betuganov::Shape>[]> operator [](const size_t index);

    void addShape(const std::shared_ptr <betuganov::Shape> shape_ptr);

  private:
    std::unique_ptr<std::shared_ptr<betuganov::Shape>[]> matrix_;
    size_t layersNumber_;
    size_t layersSize_;

    bool checkOverlapping(const int index, std::shared_ptr<betuganov::Shape> obj) const;
  };
}

#endif
