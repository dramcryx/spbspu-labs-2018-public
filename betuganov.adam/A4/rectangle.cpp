#include <stdexcept>
#include <cmath>
#include "base-types.hpp"
#include "rectangle.hpp"

betuganov::Rectangle::Rectangle(const point_t &center, double width, double height):
  center_(center),
  width_(width),
  height_(height),
  alpha_(0.0)
{
  if(width < 0.0 || height < 0.0)
  {
    throw std::invalid_argument("Invalid Rectangle parameters!");
  }
}

double betuganov::Rectangle::getArea() const
{
  return width_*height_ ;
}

betuganov::rectangle_t betuganov::Rectangle::getFrameRect() const
{
  return rectangle_t{center_,width_, height_};
}

void betuganov::Rectangle::move(const point_t &point)
{
  center_ = point;
}

void betuganov::Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void betuganov::Rectangle::scale(const double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("coefficient should be not negative");
  }
  width_ *= ratio;
  height_ *= ratio;
}

betuganov::point_t betuganov::Rectangle::getPosition() const
{
  return {(corners_[0].x + corners_[2].x) / 2, (corners_[1].y + corners_[3].y) / 2};
}

void betuganov::Rectangle::rotate(const double angle)
{
  double msin = sin(angle * M_PI / 180);
  double mcos = cos(angle * M_PI / 180);

  point_t center = getPosition();
  for (size_t i =0; i < 4; i++)
  {
    corners_[i] = {center.x + (corners_[i].x - center.x) * mcos - (corners_[i].y - center.y) * msin,
    center.y + (corners_[i].y - center.y) * mcos + (corners_[i].x - center.x) * msin};
  }
}
