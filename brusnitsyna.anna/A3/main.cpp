#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

void testShape(brusnitsyna::Shape & myShape)
{
  std::cout << "Area: " << myShape.getArea() << std::endl;
  std::cout << "Frame: width = " << myShape.getFrameRect().width;
  std::cout << " height = " << myShape.getFrameRect().height << std::endl;
  myShape.scale(0.5);
  std::cout << "Area: " << myShape.getArea() << std::endl;
  std::cout << "Frame: width = " << myShape.getFrameRect().width;
  std::cout << " height = " << myShape.getFrameRect().height << std::endl;

  myShape.move(1.1, 2.0);
  std::cout << "Pos: " << myShape.getFrameRect().pos.x << ", " << myShape.getFrameRect().pos.y << std::endl;
  myShape.move({2.2, 1.0});
  std::cout << "Pos: " << myShape.getFrameRect().pos.x << ", " << myShape.getFrameRect().pos.y << std::endl;
  std::cout << std::endl;
}

int main()
{
  try {
    brusnitsyna::Rectangle myRectangle({10.0, 5.0, {5.0, 1.0}});
    brusnitsyna::Circle myCircle({{2.0, 1.0}, 5.0});

    testShape(myRectangle);
    testShape(myCircle);

    std::shared_ptr<brusnitsyna::Shape> circ =
      std::make_shared<brusnitsyna::Circle>(brusnitsyna::Circle({{1.0, 3.0}, 10.0}));
    std::shared_ptr<brusnitsyna::Shape> rect =
      std::make_shared<brusnitsyna::Rectangle>(brusnitsyna::Rectangle({1.0, 2.0, {3.0, 4.0}}));
    brusnitsyna::CompositeShape myShape(circ);
    myShape.add(rect);
    std::cout << myShape.getSize() << " figures in this composite shape" << std::endl;
    std::cout << "MyShape area: " << myShape.getArea() << std::endl;
    std::cout << "Frame: width = " << myShape.getFrameRect().width;
    std::cout << " height = " << myShape.getFrameRect().height << std::endl;
    myShape.move({2.2, 1.0});
    std::cout << "Pos: " << myShape.getFrameRect().pos.x << ", " << myShape.getFrameRect().pos.y << std::endl;
    myShape.remove(1);
    std::cout << "Now " << myShape.getSize() << " figures in this composite shape" << std::endl;
    std::cout << "MyShape area: " << myShape.getArea() << std::endl;
    myShape.add(rect);
    myShape.scale(3.0);
    std::cout << "Now myShape area: " << myShape.getArea() << std::endl;
  } catch (std::invalid_argument & error) {
    std::cerr << error.what() << std::endl;
    return 1;
  } catch (std::out_of_range & error) {
    std::cerr << error.what() << std::endl;
    return 1;
  }

  return 0;
}
