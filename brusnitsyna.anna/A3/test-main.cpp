#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double EPSILON = 1e-8;

BOOST_AUTO_TEST_SUITE(CompositeShape)

  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    std::shared_ptr<brusnitsyna::Shape> rectangle =
      std::make_shared<brusnitsyna::Rectangle> (brusnitsyna::rectangle_t {10.0, 5.0, {2.0, 3.0}});
    std::shared_ptr<brusnitsyna::Shape> circle =
      std::make_shared<brusnitsyna::Circle> (brusnitsyna::Circle {{1.0, 3.0}, 10.0});
    brusnitsyna::CompositeShape testObject(rectangle);
    testObject.add(circle);
    double tmpHeight = testObject.getFrameRect().height;
    double tmpWidth = testObject.getFrameRect().width;
    double tmpArea = testObject.getArea();
    testObject.move({2.0, 2.0});
    BOOST_CHECK_CLOSE(tmpHeight, testObject.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(tmpWidth, testObject.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(tmpArea, testObject.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.x, 2.0, EPSILON);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.y, 2.0, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(MoveByAxes)
  {
    std::shared_ptr<brusnitsyna::Shape> rectangle =
      std::make_shared<brusnitsyna::Rectangle> (brusnitsyna::rectangle_t {8.0, 4.0, {3.0, 2.0}});
    std::shared_ptr<brusnitsyna::Shape> circle =
      std::make_shared<brusnitsyna::Circle> (brusnitsyna::Circle {{1.0, 3.0}, 4.0});
    brusnitsyna::CompositeShape testObject(rectangle);
    testObject.add(circle);
    double tmpHeight = testObject.getFrameRect().height;
    double tmpWidth = testObject.getFrameRect().width;
    double tmpArea = testObject.getArea();
    brusnitsyna::point_t positionAfterMoving = {5.0, 2.0};
    testObject.move(3.0, -1.0);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.x, positionAfterMoving.x, EPSILON);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.y, positionAfterMoving.y, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmpHeight, testObject.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmpWidth, testObject.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmpArea, testObject.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(InvalidScalingParametr)
  {
    std::shared_ptr<brusnitsyna::Shape> rectangle =
      std::make_shared<brusnitsyna::Rectangle> (brusnitsyna::rectangle_t {8.0, 4.0, {3.0, 2.0}});
    std::shared_ptr<brusnitsyna::Shape> circle =
      std::make_shared<brusnitsyna::Circle> (brusnitsyna::Circle {{1.0, 3.0}, 4.0});
    brusnitsyna::CompositeShape testObject(rectangle);
    testObject.add(circle);
    BOOST_CHECK_THROW(testObject.scale(-2.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(ScaleTest)
  {
    std::shared_ptr<brusnitsyna::Shape> rectangle =
      std::make_shared<brusnitsyna::Rectangle> (brusnitsyna::rectangle_t {8.0, 4.0, {3.0, 2.0}});
    std::shared_ptr<brusnitsyna::Shape> circle =
      std::make_shared<brusnitsyna::Circle> (brusnitsyna::Circle {{1.0, 3.0}, 4.0});
    brusnitsyna::CompositeShape testObject(rectangle);
    testObject.add(circle);
    double tmpArea = testObject.getArea();
    testObject.scale(2.0);
    BOOST_CHECK_CLOSE(2.0 * 2.0 * tmpArea, testObject.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentAdd)
  {
    std::shared_ptr<brusnitsyna::Shape> rectangle =
      std::make_shared<brusnitsyna::Rectangle> (brusnitsyna::rectangle_t {8.0, 4.0, {3.0, 2.0}});
    brusnitsyna::CompositeShape testObject(rectangle);
    std::shared_ptr<brusnitsyna::Shape> circle = nullptr;
    BOOST_CHECK_THROW(testObject.add(circle), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidConstructorArgument)
  {
    std::shared_ptr<brusnitsyna::Shape> rectangle = nullptr;
    BOOST_CHECK_THROW(brusnitsyna::CompositeShape testObject(rectangle), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvariantCoordinates)
  {
    std::shared_ptr<brusnitsyna::Shape> rectangle =
      std::make_shared<brusnitsyna::Rectangle> (brusnitsyna::rectangle_t {8.0, 4.0, {3.0, 2.0}});
    brusnitsyna::CompositeShape testObject(rectangle);
    double posX = testObject.getFrameRect().pos.x;
    double posY = testObject.getFrameRect().pos.y;
    testObject.scale(2.0);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.x, posX, EPSILON);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.y, posY, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(GeneralPosition)
  {
    brusnitsyna::point_t position = {3.0, 2.0};
    double width = 8.0, height = 4.0;
    std::shared_ptr<brusnitsyna::Shape> rectangle =
      std::make_shared<brusnitsyna::Rectangle> (brusnitsyna::rectangle_t{width, height, position});
    brusnitsyna::CompositeShape testObject(rectangle);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.x, position.x, EPSILON);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.y, position.y, EPSILON);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().width, width, EPSILON);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().height, height, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(CommonArea)
  {
    brusnitsyna::point_t position = {3.0, 2.0};
    const double width = 8.0, height = 4.0;
    std::shared_ptr<brusnitsyna::Shape> rectangle =
      std::make_shared<brusnitsyna::Rectangle> (brusnitsyna::rectangle_t {width, height, position});
    const double radius = 2.0;
    std::shared_ptr<brusnitsyna::Shape> circle =
      std::make_shared<brusnitsyna::Circle>(brusnitsyna::Circle {position, radius});
    brusnitsyna::CompositeShape testObject(rectangle);
    testObject.add(circle);
    BOOST_CHECK_CLOSE(testObject.getArea(), width * height + M_PI * radius * radius, EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()
