#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace brusnitsyna {
  class CompositeShape:
    public Shape {
  public:
    CompositeShape(const std::shared_ptr<Shape> & object);
    CompositeShape(const CompositeShape & object);
    CompositeShape(CompositeShape && object);
    CompositeShape & operator= (const CompositeShape & object);
    CompositeShape & operator= (CompositeShape && object);
    std::shared_ptr<Shape> operator[] (const size_t index) const;

    void add(const std::shared_ptr<Shape> & object);
    void remove(const size_t index);
    size_t getSize() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(double dx, double dy) override;
    void scale(double k) override;

  private:
    std::unique_ptr< std::shared_ptr<Shape>[] > compositeShape_;
    size_t size_;
    };
}

#endif // COMPOSITE-SHAPE_HPP
