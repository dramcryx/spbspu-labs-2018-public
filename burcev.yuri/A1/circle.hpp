#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

class Circle: public Shape 
{
public:
  Circle(const point_t & circlecenter, double rad);
  double getArea() const override;
  virtual rectangle_t getFrameRect() const override;
  virtual void move(const point_t & newpoint) override;
  virtual void move(double disx, double disy) override;

private:
  double rad_;
  point_t circlecenter_;
};

#endif // CIRCLE_HPP
