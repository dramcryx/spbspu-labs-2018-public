#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"

using std::cout;
using std::endl;

void ShapeInfo(const Shape & shape)
{
  cout << "AreA = " << shape.getArea() << endl;
  rectangle_t rectan = shape.getFrameRect();
  cout << "Frame rectangle:" << endl;
  cout << "x = " << rectan.pos.x << endl;
  cout << "y = " << rectan.pos.y << endl;
  cout << "width = " << rectan.width << endl;
  cout << "height = " << rectan.height << endl << endl;
}

int main()
{
  try
  {
    Rectangle rectangle{ { {7.0,7.7}, 3.0, 11.1 } };
    Circle circle{ {10.0, 11.1}, 7.7 };

    cout << "Rectangle" << endl;
    ShapeInfo(rectangle);
    cout << "Circle" << endl;
    ShapeInfo(circle);

    rectangle.move({ 1.1, 1.1 });
    circle.move({ 1.1, 1.1 });

    cout << "Rectangle" << endl;
    ShapeInfo(rectangle);
    cout << "Circle" << endl;
    ShapeInfo(circle);

    rectangle.move( 15.0, 15.0 );
    rectangle.move( -15.0, -15.0 );

    cout << "Rectangle" << endl;
    ShapeInfo(rectangle);
    cout << "Circle" << endl;
    ShapeInfo(circle);
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  return 0;
}

