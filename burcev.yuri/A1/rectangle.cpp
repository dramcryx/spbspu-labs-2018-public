#include "rectangle.hpp"
#include <stdexcept>

Rectangle::Rectangle(const rectangle_t & rectan):
  rectan_(rectan)
{
  if ((rectan_.height < 0.0) || (rectan_.width < 0.0))
  {
    throw std::invalid_argument("Error. Invalid height or width of rectangle.");
  }
}

double Rectangle::getArea() const
{
  return (rectan_.height * rectan_.width);
}

rectangle_t Rectangle::getFrameRect() const
{
  return rectan_;
}

void Rectangle::move(const point_t & newpoint)
{
  rectan_.pos.x = newpoint.x;
  rectan_.pos.y = newpoint.y;
}

void Rectangle::move(double disx, double disy)
{
  rectan_.pos.x += disx;
  rectan_.pos.y += disy;
}
