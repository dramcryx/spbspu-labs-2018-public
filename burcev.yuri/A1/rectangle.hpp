#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

class Rectangle: public Shape
{
public:
  Rectangle(const rectangle_t & rectan);
  double getArea() const override;
  virtual rectangle_t getFrameRect() const override;
  virtual void move(const point_t & newpoint) override;
  virtual void move(double disx, double disy) override;

private:
  rectangle_t rectan_;
};

#endif // RECTANGLE_HPP
