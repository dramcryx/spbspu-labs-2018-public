#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace burcev;

int main()
{
  try 
  {
    std::shared_ptr<Shape> rectPtr1
        = std::make_shared<Rectangle>(Rectangle({24.0, 48.0, {128.0, 256.0}}));
    CompositeShape comp_shape(rectPtr1);

    std::shared_ptr<Shape> rectPtr2
        = std::make_shared<Rectangle>(Rectangle({24.0, 48.0, {150.0, 256.0}}));
    comp_shape.addShape(rectPtr2);
    
    std::shared_ptr<Shape> circlePtr
        = std::make_shared<Circle>(Circle(10.0, {130.0, 256.0}));
    comp_shape.addShape(circlePtr);

    std::shared_ptr<Shape> trianglePtr
        = std::make_shared<Triangle>(Triangle({5.0, 11.0}, {11.0, 10.0}, {10.0, 15.0}));
    comp_shape.addShape(trianglePtr);

    rectangle_t frameRect = comp_shape.getFrameRect();
    std::cout << "Composite Shape: " << std::endl;
    std::cout << "Coords: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
    std::cout << "Width = " << frameRect.width << " Height = " << frameRect.height << std::endl;
    std::cout << "Area of Composite Shape = " << comp_shape.getArea() << std::endl;
    std::cout << std::endl;

    comp_shape.rotate(90.0);
    frameRect = comp_shape.getFrameRect();
    std::cout << "Composite Shape: " << std::endl;
    std::cout << "Coords: (" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl;
    std::cout << "Width = " << frameRect.width << " Height = " << frameRect.height << std::endl;
    std::cout << "Area of Composite Shape after rotating = " << comp_shape.getArea() << std::endl;
    std::cout << std::endl;

    MatrixShape matrix_shape(rectPtr1);
    matrix_shape.addShape(rectPtr2);
    matrix_shape.addShape(circlePtr);
    matrix_shape.addShape(trianglePtr);
    std::cout << "Matrix of shapes:" << std::endl;
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  catch (std::out_of_range & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  return 0;
}
