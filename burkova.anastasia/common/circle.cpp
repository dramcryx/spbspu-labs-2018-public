#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>
#include <stdexcept>

#include "circle.hpp"


using namespace burkova;

Circle::Circle(const point_t &pos, const double rad) :
  point_(pos),
  radius_(rad),
  angle_(0)
{
  if (rad <= 0.0)
  {
    throw std::invalid_argument("invalid argument circle");
  }
}

double Circle::getArea()const noexcept
{
  return radius_ * radius_ * M_PI;
}

double Circle::getAngle() const noexcept
{
  return angle_;
}


rectangle_t Circle::getFrameRect() const noexcept
{
  return {point_, radius_ * 2, radius_ * 2};
}

void Circle::scale(double koef)
{
  if (koef < 0.0)
  {
   throw std::invalid_argument("invalid koef!");
  }
  else
  {
    radius_ = radius_ * koef;
  }
}
  
void Circle::move(const point_t &pos) noexcept
{
  point_ = pos;
}

void Circle::move(const double dx, const double dy) noexcept
{
  point_.x += dx;
  point_.y += dy;
}
void Circle::rotate(const double angle) noexcept
{
  angle_ += angle;
  if (fabs(angle_) >= 360.0)
  {
    angle_ = std::fmod(angle_,360.0);
  }
}
