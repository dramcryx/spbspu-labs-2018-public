#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace burkova
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape(const std::shared_ptr <Shape> shape);
    CompositeShape(const CompositeShape &compositeShape);
    CompositeShape(CompositeShape &&compositeShape);
    CompositeShape &operator = (const CompositeShape &compositeShape);
    CompositeShape &operator = (CompositeShape &&compositeShape);
    bool operator == (const CompositeShape &compositeShape) const;
    bool operator != (const CompositeShape &compositeShape) const;
  
    
    double getArea() const noexcept override;
    double getAngle() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void scale(double koef) override;
    void move(const double dx, const double dy) noexcept override;
    void move(const point_t &pos) noexcept override;
    void rotate(const double angle) noexcept override;
    
    std::shared_ptr <Shape> & operator[](int i) const;
    void removeShape(const int num);
    void addShape(const std::shared_ptr <Shape> shape);
    void deleteShapes();
    int getSize() const;
    
  private:
    std::unique_ptr <std::shared_ptr <Shape> [] > shapes_;
    int size_;
    double angle_;
  };
}

#endif
