#include <memory>
#include <stdexcept>
#include <iostream>

#include "matrix.hpp"

using namespace burkova;

Matrix::Matrix (const std::shared_ptr <Shape> shape):
  shapes_ (nullptr),
  levelNum_ (0),
  levelSize_ (0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Null pointer");
  }
  addShape(shape);
}

Matrix::Matrix (const Matrix &matrix):
  shapes_(new std::shared_ptr <Shape> [matrix.levelNum_ * matrix.levelSize_]),
  levelNum_(matrix.levelNum_),
  levelSize_(matrix.levelSize_)
{
  for ( int i=0 ; i<matrix.levelNum_ * matrix.levelSize_; ++i )
  {
    shapes_[i] = matrix.shapes_[i];
  }
}

Matrix::Matrix (Matrix &&matrix):
  shapes_(nullptr),
  levelNum_(matrix.levelNum_),
  levelSize_(matrix.levelSize_)
{
  shapes_.swap(matrix.shapes_);
  matrix.levelNum_=0;
  matrix.levelSize_=0;
}

Matrix &Matrix::operator=(const Matrix &matrix)
{
  if (this != &matrix)
  {
    levelNum_ = matrix.levelNum_;
    levelSize_ = matrix.levelSize_;
    std::unique_ptr <std::shared_ptr <Shape> []> shape (new std::shared_ptr <Shape>
      [matrix.levelNum_ * matrix.levelSize_]);
    for (int i=0; i<matrix.levelNum_ * matrix.levelSize_; ++i)
    {
      shape[i] = matrix.shapes_[i];
    }
    shapes_.swap(shape);
  }
  return *this;
}

Matrix &Matrix::operator=(Matrix &&matrix)
{
  if (this != &matrix)
  {
    levelNum_ = matrix.levelNum_;
    levelSize_ = matrix.levelSize_;
    shapes_.reset();
    shapes_.swap(matrix.shapes_);
    matrix.levelNum_ = 0;
    matrix.levelSize_ = 0;
  }
  return *this;
}

bool Matrix::operator == (const Matrix &matrix) const
{
  if ((this->levelNum_ != matrix.levelNum_) || (this-> levelSize_ != matrix.levelSize_))
  {
    return false;
  }
  for (int i=0; i<levelNum_*levelSize_; ++i)
  {
    if (this->shapes_[i] != matrix.shapes_[i])
    {
      return false;
    }
  }
  return true;
}

bool Matrix::operator != (const Matrix &matrix) const
{
  if ((this->levelNum_ != matrix.levelNum_) || (this->levelSize_ != matrix.levelSize_))
  {
    return true;
  }
  for (int i = 0; i < levelNum_ * levelSize_; ++i)
    {
      if (this->shapes_[i] != matrix.shapes_[i])
      {
        return true;
      }
    }
  return false;
}

std::unique_ptr < std::shared_ptr < Shape >[] > Matrix::operator [](const int levelIndex) const
{
  if (levelNum_ == 0)
  {
    throw std::out_of_range("Matrix is empty");
  }
  if ((levelIndex < 0) || (levelIndex > levelNum_ - 1))
  {
    throw std::invalid_argument("Invalid lavel index");
  }
  std::unique_ptr < std::shared_ptr < Shape >[] > level (new std::shared_ptr < Shape >[levelSize_]);
  for (int i = 0; i < levelSize_; ++i)
  {
    level[i] = shapes_[levelIndex * levelSize_ + i];
  }
  return level;
}

int Matrix::getLevelNum() const noexcept
{
  return levelNum_;
}

int Matrix::getLevelSize() const noexcept
{
  return levelSize_;
}

void Matrix::addShape(const std::shared_ptr <Shape> shape)
{
  if (levelNum_ == 0)
  {
    ++levelNum_;
    ++levelSize_;
    std::unique_ptr <std::shared_ptr<Shape> []> newShape (new std::shared_ptr <Shape> [levelNum_ * levelSize_]);
    shapes_.swap(newShape);
    shapes_[0]=shape;
  }
  else
  {
    bool addedShapes = false;
    for (int i = 0; !addedShapes; ++i)
    {
      for (int j = 0; j<levelSize_; ++j)
      {
        if (shapes_[i*levelSize_+j] == nullptr)
        {
          shapes_[i*levelSize_+j] = shape;
          addedShapes = true;
          break;
        }
        else
        {
          if (checkOverlapping(i*levelSize_+j,shape))
          {
            break;
          }
        }
        if (j==(levelSize_-1))
        {
          ++levelSize_;
          std::unique_ptr<std::shared_ptr<Shape>[]>newShape(new std::shared_ptr<Shape>[levelNum_*levelSize_]);
          for (int n=0; n<levelNum_; ++n)
          {
            for (int s=0; s<levelSize_-1; ++s)
            {
              newShape[n*levelSize_+s] = shapes_[n*(levelSize_-1)+s];
            }
            newShape[(n+1)*levelSize_-1] = nullptr;
          }
          newShape[(i+1)*levelSize_-1]=shape;
          shapes_.swap(newShape);
          addedShapes = true;
          break;
        }
      }
      if ((i == (levelNum_ - 1)) && !(addedShapes))
      {
        ++levelNum_;
        std::unique_ptr<std::shared_ptr<Shape>[]>newShape(new std::shared_ptr<Shape>[levelNum_*levelSize_]);
        for (int n=0; n<(levelNum_-1)*levelSize_; ++n)
        {
          newShape[n]=shapes_[n];
        }
        for (int n=(levelNum_-1)*levelSize_; n<levelNum_*levelSize_; ++n)
        {
          newShape[n] = nullptr;
        }
        newShape[(levelNum_-1)*levelSize_]= shape;
        shapes_.swap(newShape);
        addedShapes = true;
      }
    }
  }
}

bool Matrix::checkOverlapping(const int i, std::shared_ptr <Shape> shape) const noexcept
{
  rectangle_t FrameRect = shape->getFrameRect();
  rectangle_t matrixShapeFrameRect = shapes_[i]->getFrameRect();
  point_t newShapePoint[4] = 
  {
    {FrameRect.pos.x - FrameRect.width/2.0, FrameRect.pos.y + FrameRect.height/2.0},
    {FrameRect.pos.x + FrameRect.width/2.0, FrameRect.pos.y + FrameRect.height/2.0},
    {FrameRect.pos.x + FrameRect.width/2.0, FrameRect.pos.y - FrameRect.height/2.0},
    {FrameRect.pos.x - FrameRect.width/2.0, FrameRect.pos.y - FrameRect.height/2.0},
  };   

  point_t matrixShapePoint[4] =
  {
    {matrixShapeFrameRect.pos.x - matrixShapeFrameRect.width/2.0, matrixShapeFrameRect.pos.y + matrixShapeFrameRect.height/2.0},
    {matrixShapeFrameRect.pos.x + matrixShapeFrameRect.width/2.0, matrixShapeFrameRect.pos.y + matrixShapeFrameRect.height/2.0},
    {matrixShapeFrameRect.pos.x + matrixShapeFrameRect.width/2.0, matrixShapeFrameRect.pos.y - matrixShapeFrameRect.height/2.0},
    {matrixShapeFrameRect.pos.x - matrixShapeFrameRect.width/2.0, matrixShapeFrameRect.pos.y - matrixShapeFrameRect.height/2.0},
  };
  
  for (int i=0; i<4; ++i)
  {
    if (((newShapePoint[i].x >= matrixShapePoint[0].x) && (newShapePoint[i].x <= matrixShapePoint[2].x)
      && (newShapePoint[i].y >= newShapePoint[3].y) && (newShapePoint[i].y <= newShapePoint[1].y))
        || ((matrixShapePoint[i].x >= newShapePoint[0].x) && (matrixShapePoint[i].x <=newShapePoint[2].x)
          && (matrixShapePoint[i].y >= newShapePoint[3].y) && (matrixShapePoint[i].y <= newShapePoint [1].y)))
    {
      return true;
    }
  }
  return false;
}
