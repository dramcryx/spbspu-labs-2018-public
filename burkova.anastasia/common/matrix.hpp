#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace burkova
{
  class Matrix
  {
  public:
    Matrix (const std::shared_ptr <Shape> shape);
    Matrix (const Matrix &matrix);
    Matrix (Matrix &&matrix);
    Matrix &operator = (const Matrix &matrix);
    Matrix &operator = (Matrix &&matrix);
    bool operator == (const Matrix &matrix) const;
    bool operator != (const Matrix &matrix) const;
    std::unique_ptr<std::shared_ptr <Shape> []>operator [] (const int levelIndex) const;
    
    int getLevelNum() const noexcept;
    int getLevelSize() const noexcept;
    void addShape (const std::shared_ptr <Shape> shape);
    
  private:
    std::unique_ptr <std::shared_ptr <Shape> [] > shapes_;
    int levelNum_;
    int levelSize_;
    bool checkOverlapping (const int i, std::shared_ptr <Shape> shape) const noexcept;
  };
}

#endif
