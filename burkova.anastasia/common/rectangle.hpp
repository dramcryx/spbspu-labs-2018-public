#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace burkova
{
  class Rectangle : public Shape
  {
    public:
    Rectangle(const point_t &pos, const double w, const double h);
    double getArea() const noexcept override;
    double getWidth() const noexcept;
    double getHeight() const noexcept;
    double getAngle() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void scale(const double koef) override;
    void move(const point_t &pos)noexcept override;
    void move(const double dx,const double dy) noexcept override;
    void rotate(const double angle) noexcept override;
    
    private:
    double width_;
    double height_;
    point_t point_;
    double angle_;
  };
}
#endif
