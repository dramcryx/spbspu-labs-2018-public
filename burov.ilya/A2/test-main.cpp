#define BOOST_TEST_MODULE
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"

const double EPSILON = 0.00001;
using namespace burov;

BOOST_AUTO_TEST_SUITE(RectangleTest)
  BOOST_AUTO_TEST_CASE(InvalidConstructorParam)
  {
    BOOST_CHECK_THROW(Rectangle rect({ {4.0, 4.0}, -6.0, 6.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(RectangleMovingtopointTest)
  {
    Rectangle rect{ { {4.0, 4.0}, 6.0, 6.0} };
    rectangle_t rectBeforeMove = rect.getFrameRect();
    double areaBeforeMove = rect.getArea();
    rect.move( {11.0, 8.3} );
    BOOST_CHECK_CLOSE(areaBeforeMove, rect.getArea(), EPSILON );
    BOOST_CHECK_CLOSE(rectBeforeMove.width, rect.getFrameRect().width, EPSILON );
    BOOST_CHECK_CLOSE(rectBeforeMove.height, rect.getFrameRect().height, EPSILON );
    BOOST_CHECK_CLOSE(11.0, rect.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE(8.3, rect.getFrameRect().pos.y, EPSILON );
  }
  BOOST_AUTO_TEST_CASE(CoordinateMovingTest)
  {
    Rectangle rect{ { {4.0, 4.0}, 6.0, 6.0} };
    rectangle_t rectBeforeMove = rect.getFrameRect();
    double areaBeforeMove = rect.getArea();
    double xBeforeMoving = rect.getFrameRect().pos.x;
    double yBeforeMoving = rect.getFrameRect().pos.y;
    rect.move(100, 100);
    BOOST_CHECK_CLOSE(areaBeforeMove, rect.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(rectBeforeMove.height, rect.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(rectBeforeMove.width, rect.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(xBeforeMoving + 100, rect.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE(yBeforeMoving + 100, rect.getFrameRect().pos.y, EPSILON );

  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    Rectangle rect{ { {4.0, 4.0}, 6.0, 6.0 } };
    double areaBeforeScale = rect.getArea();
    const double koeff = 2.0;
    rect.scale(koeff);
    double result = koeff * koeff * areaBeforeScale;
    BOOST_CHECK_CLOSE(result, rect.getArea(), EPSILON);
  }
  BOOST_AUTO_TEST_CASE(WrongRectScale)
  {
    Rectangle rect{ { {4.0, 4.0}, 6.0, 6.0} };
    BOOST_CHECK_THROW(rect.scale(-4.0), std::invalid_argument);
  }


BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(CircleTest)
  BOOST_AUTO_TEST_CASE(InvalidConstructorParam)
  {
    BOOST_CHECK_THROW(Circle circ( {10.0, 10.0}, -30.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CoordinateMovingTest)
  {
    Circle circ{ {6.0, 6.0}, 3.0};
    double areaBeforeMove = circ.getArea();
    rectangle_t rectBeforeMove = circ.getFrameRect();
    double xBeforeMoving = circ.getFrameRect().pos.x;
    double yBeforeMoving = circ.getFrameRect().pos.y;
    circ.move(100, 100);
    BOOST_CHECK_CLOSE(areaBeforeMove, circ.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(rectBeforeMove.width, circ.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(rectBeforeMove.height, circ.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(xBeforeMoving + 100, circ.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE(yBeforeMoving + 100, circ.getFrameRect().pos.y, EPSILON );
  }
  BOOST_AUTO_TEST_CASE(CircleMovingtopointTest)
  {
    Circle circ{ {10.0, 10.0}, 4.0};
    double areaBeforeMove = circ.getArea();
    rectangle_t rectBeforeMove = circ.getFrameRect();
    circ.move( {-10.0, -10.0} );
    BOOST_CHECK_CLOSE(areaBeforeMove, circ.getArea(), EPSILON );
    BOOST_CHECK_CLOSE(rectBeforeMove.width, circ.getFrameRect().width, EPSILON );
    BOOST_CHECK_CLOSE(rectBeforeMove.height, circ.getFrameRect().height, EPSILON );
    BOOST_CHECK_CLOSE(-10.0, circ.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE(-10.0, circ.getFrameRect().pos.y, EPSILON );
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    Circle circ{ {4.0, 4.0}, 2.0};
    const double koeff = 2.0;
    double areaBeforeScaling = circ.getArea();
    circ.scale(koeff);
    double result = koeff * koeff * areaBeforeScaling;
    BOOST_CHECK_CLOSE(result, circ.getArea(), EPSILON);
  }
  BOOST_AUTO_TEST_CASE(WrongCircleScale)
  {
    Circle circ{ {6.0, 6.0}, 3.0};
    BOOST_CHECK_THROW(circ.scale(-4.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()





