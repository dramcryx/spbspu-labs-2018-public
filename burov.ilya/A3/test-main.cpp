#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

using namespace burov;

const double EPSILON = 0.0001;

BOOST_AUTO_TEST_SUITE(CompositeShapetests)

  BOOST_AUTO_TEST_CASE(CopingConstructor)
  {
    std::shared_ptr< Shape > rect1(new Rectangle({{40.0, 40.0}, 10.0, 20.0}));
    std::shared_ptr< Shape > circ1(new Circle({3.0, 2.3}, 100));
    CompositeShape testComp;
    testComp.addShape(rect1);
    testComp.addShape(circ1);
    CompositeShape newComposition(testComp);
    BOOST_CHECK_EQUAL(testComp[0], newComposition[0]);
    BOOST_CHECK_EQUAL(testComp[1], newComposition[1]);
  }

  BOOST_AUTO_TEST_CASE(MovingConstructor)
  {
    std::shared_ptr< Shape > ptr1(new Rectangle({{40.0, 40.0}, 10.0, 20.0}));
    std::shared_ptr< Shape > ptr2(new Circle({3.0, 2.3}, 100));
    CompositeShape testComp;
    testComp.addShape(ptr1);
    testComp.addShape(ptr2);
    CompositeShape newComposition(std::move(testComp));
    BOOST_CHECK_EQUAL(newComposition[0], ptr1);
    BOOST_CHECK_EQUAL(newComposition.getSize(), 2);
    BOOST_CHECK_EQUAL(newComposition[1], ptr2);

  }

  BOOST_AUTO_TEST_CASE(invalidadd)
  {
    std::shared_ptr<Shape> testRect = nullptr;
    CompositeShape testComp;
    BOOST_CHECK_THROW( testComp.addShape(testRect), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingToPoint)
  {
    std::shared_ptr< Shape > rect1(new Rectangle({{35.0, 35.0}, 5.0, 15.0}));
    std::shared_ptr< Shape > circ1(new Circle({3.0, 2.5}, 105));
    CompositeShape testComp;
    testComp.addShape(rect1);
    testComp.addShape(circ1);
    double Startheight = testComp.getFrameRect().height;
    double Startwidth = testComp.getFrameRect().width;
    double areaBeforeChange = testComp.getArea();
    point_t movedpos = {5.0, 5.0};
    testComp.move(movedpos);
    BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.x, movedpos.x, EPSILON);
    BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.y, movedpos.y, EPSILON);
    BOOST_CHECK_EQUAL(Startheight, testComp.getFrameRect().height);
    BOOST_CHECK_EQUAL(areaBeforeChange, testComp.getArea());
    BOOST_CHECK_EQUAL(Startwidth, testComp.getFrameRect().width);


  }
  BOOST_AUTO_TEST_CASE(Coordinatemoving)
  {
    std::shared_ptr< Shape > rect1(new Rectangle({{36.0, 36.0}, 6.0, 16.0}));
    std::shared_ptr< Shape > circ1(new Circle({4.0, 3.5}, 106));
    CompositeShape testComp;
    testComp.addShape(rect1);
    testComp.addShape(circ1);
    double Startheight = testComp.getFrameRect().height;
    double Startwidth = testComp.getFrameRect().width;
    double areaBeforeChange = testComp.getArea();
    point_t posAfterMove = {testComp.getFrameRect().pos.x + 4.0,testComp.getFrameRect().pos.y + 6.0};
    testComp.move(4.0,6.0);
    BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.x, posAfterMove.x, EPSILON);
    BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.y, posAfterMove.y, EPSILON);
    BOOST_CHECK_EQUAL(Startheight, testComp.getFrameRect().height);
    BOOST_CHECK_EQUAL(areaBeforeChange, testComp.getArea());
    BOOST_CHECK_EQUAL(Startwidth, testComp.getFrameRect().width);
  }

  BOOST_AUTO_TEST_CASE(emptyShape)
  {
     CompositeShape TestComp;
     BOOST_CHECK_EQUAL(TestComp.getFrameRect().width, 0);
     BOOST_CHECK_EQUAL(TestComp.getFrameRect().height, 0);
  }
  BOOST_AUTO_TEST_CASE(WrongScaling)
  {
    std::shared_ptr< Shape > rect1(new Rectangle({{40.0, 40.0}, 10.0, 20.0}));
    std::shared_ptr< Shape > circ1(new Circle({3.0, 2.3}, 100));
    CompositeShape testComp;
    testComp.addShape(rect1);
    testComp.addShape(circ1);
    BOOST_CHECK_THROW(testComp.scale(-3.0), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(TestScale)
  {
    std::shared_ptr< Shape > rect1(new Rectangle({{40.0, 40.0}, 10.0, 20.0}));
    std::shared_ptr< Shape > circ1(new Circle({3.0, 2.3}, 100));
    CompositeShape testComp;
    testComp.addShape(rect1);
    testComp.addShape(circ1);
    double StartArea = testComp.getArea();
    testComp.scale(3.0);
    BOOST_CHECK_CLOSE(3.0 * 3.0 * StartArea, testComp.getArea(), EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()
