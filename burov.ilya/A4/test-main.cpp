#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "polygon.hpp"

using namespace burov;

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(testMatrix)

  BOOST_AUTO_TEST_CASE(Matrixlayerstest)
  {
    Rectangle testingRect1({{5.0, -5.0}, 4.0, 2.0});
    Circle testingCirc1({ 8.0, -5.0 }, 3.0);
    Circle testingCirc2({ 4.0, 8.0 }, 1.0);
    std::shared_ptr<Shape> circPtr1 = std::make_shared<Circle>(testingCirc1);
    std::shared_ptr<Shape> rectPtr1 = std::make_shared<Rectangle>(testingRect1);
    std::shared_ptr<Shape> circPtr2 = std::make_shared<Circle>(testingCirc2);
    Matrix matrix;
    matrix.addShape(rectPtr1);
    matrix.addShape(circPtr1);
    matrix.addShape(circPtr2);
    std::unique_ptr<std::shared_ptr<Shape>[]> layer1 = matrix[0];
    std::unique_ptr<std::shared_ptr<Shape>[]> layer2 = matrix[1];
    BOOST_CHECK(layer1[0] == rectPtr1);
    BOOST_CHECK(layer1[1] == circPtr2);
    BOOST_CHECK(layer2[0] == circPtr1);

  }
  BOOST_AUTO_TEST_CASE(CheckingMatrixLayers)
  {
    std::shared_ptr<Shape> testRect1 = std::make_shared<Rectangle>(Rectangle({{0.0, 0.0}, 5.0, 3.0}));
    std::shared_ptr<Shape> testCirc1 = std::make_shared<Circle>(Circle({ 0.0, 0.0 }, 2.0));
    std::shared_ptr<Shape> testCirc2 = std::make_shared<Circle>(Circle({ 0.0, 0.0 }, 1.0));
    Matrix matrix;
    matrix.addShape(testRect1);
    matrix.addShape(testCirc1);
    matrix.addShape(testCirc2);
    int count = 3;
    BOOST_CHECK_EQUAL(count, matrix.getLayers());

  }
  BOOST_AUTO_TEST_CASE(CheckingmatrixLayerSize)
  {
    std::shared_ptr<Shape> rectptr1 = std::make_shared<Rectangle>(Rectangle({{26.0, 26.0}, 2.0, 2.0}));
    std::shared_ptr<Shape> circptr1 = std::make_shared<Circle>(Circle({ 0.0, 0.0 }, 2.0));
    std::shared_ptr<Shape> circptr2 = std::make_shared<Circle>(Circle({ -6.0, -8.0 }, 2.0));
    Matrix matrix;
    matrix.addShape(rectptr1);
    matrix.addShape(circptr1);
    matrix.addShape(circptr2);
    int count = 3;
    BOOST_CHECK_EQUAL(matrix.getLayerSize(), count);
  }
  BOOST_AUTO_TEST_CASE(MatrixCopyConstructortest)
  {
    std::shared_ptr<Shape> rectptr1 = std::make_shared<Rectangle>(Rectangle({{4.0, 5.0}, 2.0, 3.0}));
    std::shared_ptr<Shape> circptr1 = std::make_shared<Circle>(Circle({ -1.0, -2.0 }, 1.0));
    Matrix matrix;
    matrix.addShape(rectptr1);
    matrix.addShape(circptr1);
    Matrix matrix2(matrix);
    std::unique_ptr<std::shared_ptr<Shape>[]> layer1 = matrix[0];
    std::unique_ptr<std::shared_ptr<Shape>[]> layer2 = matrix2[0];
    BOOST_CHECK(layer1[0] == rectptr1);
    BOOST_CHECK(layer1[1] == circptr1);
    BOOST_CHECK(layer2[0] == rectptr1);
    BOOST_CHECK(layer2[1] == circptr1);
  }
  BOOST_AUTO_TEST_CASE(MatrixCopyngOperator)
  {
    std::shared_ptr<Shape> rectptr1 = std::make_shared<Rectangle>(Rectangle({{4.0, 5.0}, 2.0, 3.0}));
    std::shared_ptr<Shape> circptr1 = std::make_shared<Circle>(Circle({ -1.0, -2.0 }, 1.0));
    Matrix matrix1;
    matrix1.addShape(rectptr1);
    matrix1.addShape(circptr1);
    Matrix matrix2;
    matrix2 = matrix1;
    std::unique_ptr<std::shared_ptr<Shape>[]> layer1 = matrix1[0];
    std::unique_ptr<std::shared_ptr<Shape>[]> layer2 = matrix2[0];
    BOOST_CHECK(layer1[0] == rectptr1);
    BOOST_CHECK(layer1[1] == circptr1);
    BOOST_CHECK(layer2[0] == rectptr1);
    BOOST_CHECK(layer2[1] == circptr1);
  }
  BOOST_AUTO_TEST_CASE(Matrixrotatetest)
  {
    std::shared_ptr< Shape > ptr1(new Circle({ 1.0, 1.0 }, 2));
    std::shared_ptr< Shape > ptr2(new Rectangle({{5.0, 5.0}, 3.0, 5.0}));
    CompositeShape testComp1;
    testComp1.addShape(ptr1);
    testComp1.addShape(ptr2);
    Matrix matrix1(testComp1);
    int count = 2;
    BOOST_CHECK_EQUAL(matrix1.getLayerSize(),count);
    testComp1.rotate(55.0);
    Matrix matrix2(testComp1);
    BOOST_CHECK_EQUAL(matrix2.getLayerSize(), count);

  }
  BOOST_AUTO_TEST_CASE(AddCompositeShapeinlayertest)
  {
    std::shared_ptr<Shape> rectptr1 = std::make_shared<Rectangle>(Rectangle({{0.0, 0.0}, 4.0, 4.0}));
    std::shared_ptr<Shape> circptr1 = std::make_shared<Circle>(Circle({ 9.0, 9.0 }, 4.0));
    std::shared_ptr<Shape> rectptr2 = std::make_shared<Rectangle>(Rectangle({{-7.0, -7.0}, 4.0, 3.4}));
    Matrix matrix;
    CompositeShape testComp;
    testComp.addShape(rectptr1);
    testComp.addShape(circptr1);
    testComp.addShape(rectptr2);
    matrix.addCompShape(testComp);
    int count = 3;
    BOOST_CHECK_EQUAL(matrix.getFiguresInLayer(0), count);

  }
BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(RotatingTests)
  BOOST_AUTO_TEST_CASE(rotatingCompositeShapetest)
  {
    std::shared_ptr< Shape > ptr1(new Rectangle({{10.0, 10.0}, 2.0, 2.0}));
    std::shared_ptr< Shape > ptr2(new Rectangle({{0.0, 0.0}, 4.0, 4.0}));
    CompositeShape testComp1;
    testComp1.addShape(ptr1);
    testComp1.addShape(ptr2);
    CompositeShape testComp2(testComp1);
    testComp1.rotate(60);
    testComp1.move(5.0, 5.0);
    BOOST_CHECK_CLOSE(testComp1.getFrameRect().pos.y, 11.18301270, EPSILON);
    BOOST_CHECK_CLOSE(testComp1.getFrameRect().pos.x, 10.68301270, EPSILON);
    BOOST_CHECK_CLOSE(testComp1.getArea(), testComp2.getArea(),EPSILON);
    BOOST_CHECK_CLOSE(testComp1.getArea(),20, EPSILON);
    BOOST_CHECK_CLOSE(testComp2.getArea(),20, EPSILON);
    BOOST_CHECK_CLOSE(testComp1.getFrameRect().width, 6.66025403, EPSILON);
  }
  BOOST_AUTO_TEST_CASE(Rectangletesting)
  {
    Rectangle rect1( {{8.0, 8.0}, 6.0, 5.0} );
    Rectangle rect2( {{8.0, 8.0}, 6.0, 5.0} );
    rect1.rotate(40);
    BOOST_CHECK_CLOSE(rect1.getArea(), rect2.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(rect1.getFrameRect().pos.x, rect2.getFrameRect().pos.x , EPSILON);
    BOOST_CHECK_CLOSE(rect1.getFrameRect().pos.y, rect2.getFrameRect().pos.y , EPSILON);
    BOOST_CHECK_CLOSE( 12 , 2 * rect2.getFrame().width, EPSILON);
    BOOST_CHECK_CLOSE( 10 , 2 * rect2.getFrame().height, EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()
