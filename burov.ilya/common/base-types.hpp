#ifndef BASETYPES_HPP
#define BASETYPES_HPP
namespace burov
{
  struct point_t
  {
    double x, y;
  };

  struct rectangle_t
  {
    point_t pos;
    double width, height;
  };
}
#endif // BASETYPES_HPP
