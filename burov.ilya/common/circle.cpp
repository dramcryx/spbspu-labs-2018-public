#include "circle.hpp"
#include <cmath>
#include <stdexcept>

using namespace burov;
Circle::Circle(const point_t &center, double r):
  m_center(center), m_radius(r)
{
  if(m_radius < 0.0)
  {
    throw std::invalid_argument("Incorrect Radius");
  }
}

double Circle::getArea() const
{
  return {M_PI * m_radius * m_radius};
}

rectangle_t Circle::getFrameRect() const
{
  return { m_center, m_radius * 2, m_radius * 2};
}
void Circle::move(const point_t & newpos)
{
  m_center = newpos;
}
void Circle::move(double dx, double dy)
{
  m_center.x += dx;
  m_center.y += dy;
}
void Circle::scale(double koeff)
{
  if(koeff < 0.0)
  {
    throw std::invalid_argument("scale koeff is incorrect!");

  }
  m_radius = m_radius * koeff;

}
void Circle::rotate(double)
{
  //Rotation does not change the circle
}
