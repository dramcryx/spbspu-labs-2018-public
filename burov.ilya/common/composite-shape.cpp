#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

using namespace burov;


CompositeShape::CompositeShape() :
  items_(nullptr),
  size_(0)
{
}

CompositeShape::CompositeShape(const CompositeShape & object) :
  items_(new std::shared_ptr<Shape>[object.size_]),
  size_(object.size_)
{
  for (size_t i = 0; i < object.size_; i++)
  {
    items_[i] = object.items_[i];

  }
}
CompositeShape::CompositeShape(CompositeShape && object):
  size_(object.size_)
{
  items_ = std::move(object.items_);
  object.size_ = 0;
  object.items_.reset();
}

std::shared_ptr<Shape> CompositeShape::operator [] (const size_t index) const
{
  if ((index >= size_) )
  {
    throw std::out_of_range("Invalid index");

  }
  return items_[index];
}
CompositeShape & CompositeShape::operator= (const CompositeShape & elem) noexcept
{
  if (this != &elem)
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> buf(new std::shared_ptr<Shape>[elem.size_]);
    for (size_t i = 0; i < elem.size_; i++)
    {
      buf[i] = elem.items_[i];

    }
    items_.swap(buf);
    size_ = elem.size_;
  }
  return *this;
}
CompositeShape & CompositeShape::operator= (CompositeShape && elem)
{
  items_ = std::move(elem.items_);
  size_ = elem.size_;
  elem.size_ = 0;
  return *this;
}

void CompositeShape::addShape(const std::shared_ptr<Shape> & object)
{
  if (object == nullptr)
  {
    throw std::invalid_argument("Wrong ptr");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> newArray(new std::shared_ptr<Shape> [size_ + 1]);
  for (size_t i = 0; i < size_; i++)
  {
    newArray[i] = items_[i];
  }
  newArray[size_] = object;
  size_++;
  items_.swap(newArray);

}
void CompositeShape::removeShape (const size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("Wrong index");

  }
  if ((size_ == 1) && (index == 0))
  {
    items_.reset();
    size_ = 0;
    return;

  }
  std::unique_ptr<std::shared_ptr <Shape >[]> newArray(new std::shared_ptr<Shape> [size_ - 1]);
  for (size_t i = 0; i < index; i++)
  {
    newArray[i] = items_[i];

  }
  for (size_t i = index; i < size_ - 1; ++i)
  {
    newArray[i] = items_[i + 1];

  }
  items_.swap(newArray);
  size_--;

}
double CompositeShape::getArea() const
{
  double totalArea = 0;
  for (size_t i = 0; i < size_; i++)
  {
    totalArea += items_[i] -> getArea();
  }
  return totalArea;
}

size_t CompositeShape::getSize() const
{
  return size_;
}
void CompositeShape::move(const point_t & point)
{
  move(point.x - getFrameRect().pos.x, point.y - getFrameRect().pos.y);
}

void CompositeShape::move(const double dx, const double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    items_[i] -> move(dx, dy);

  }
}

void CompositeShape::scale(const double koeff)
{
  if (koeff < 0.0)
  {
    throw std::invalid_argument("Incorrect koeff");

  }
  point_t firstshapepos = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    point_t ShapePos = items_[i] -> getFrameRect().pos;
    items_[i] -> move((koeff - 1.0) * (ShapePos.x - firstshapepos.x),
      (koeff - 1.0)* (ShapePos.y - firstshapepos.y));
    items_[i] -> scale(koeff);
  }

}
void CompositeShape::rotate(double angle)
{
  double cosAngle = cos(angle * M_PI / 180);
  double sinAngle = sin(angle * M_PI / 180);
  point_t compositeShapeCenter = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    point_t shapeCenter = items_[i]->getFrameRect().pos;
    items_[i]->move ({compositeShapeCenter.x + cosAngle * (shapeCenter.x - compositeShapeCenter.x)
        - sinAngle * (shapeCenter.y - compositeShapeCenter.y),
            compositeShapeCenter.y + cosAngle * (shapeCenter.y - compositeShapeCenter.y)
                + sinAngle * (shapeCenter.x - compositeShapeCenter.x)});
    items_[i]->rotate(angle);

  }
}

rectangle_t CompositeShape::getFrameRect() const
{
  if (size_ <= 0)
  {
    return {{0.0 , 0.0}, 0.0 , 0.0};
  }
  rectangle_t Rectframe = items_[0] -> getFrameRect();
  double minx = Rectframe.pos.x - Rectframe.width / 2;
  double highestx = Rectframe.pos.x + Rectframe.width / 2;
  double highesty = Rectframe.pos.y + Rectframe.width / 2;
  double miny = Rectframe.pos.y - Rectframe.width / 2;
  for (size_t i = 1; i < size_; i++)
  {
    Rectframe = items_[i]->getFrameRect();
    if ((Rectframe.pos.x - Rectframe.width / 2) < minx)
    {
      minx = Rectframe.pos.x - Rectframe.width / 2;

    }
    if ((Rectframe.pos.x + Rectframe.width / 2) > highestx)
    {
      highestx = Rectframe.pos.x + Rectframe.width / 2;

    }
    if ((Rectframe.pos.y + Rectframe.height / 2) < highesty)
    {
      highesty = Rectframe.pos.y + Rectframe.height / 2;

    }
    if ((Rectframe.pos.y - Rectframe.height / 2) > miny)
    {
       miny = Rectframe.pos.y - Rectframe.height / 2;
    }
  }
  return {{(minx + (highestx - minx) / 2), (miny + (highesty - miny) / 2)}, (highestx - minx), (highesty - miny)};
}
