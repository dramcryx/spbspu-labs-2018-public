#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"
#include <memory>
namespace burov

{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & object);
    CompositeShape(CompositeShape && object);
    std::shared_ptr<Shape> operator [] (const size_t index) const;
    CompositeShape & operator= (const CompositeShape & elem) noexcept;
    CompositeShape & operator= (CompositeShape && elem);
    void addShape(const std::shared_ptr<Shape> & object);
    void removeShape(const size_t index);
    double getArea() const override;
    size_t getSize() const;
    void move(const double dx, const double dy) override;
    void move(const point_t & point) override;
    void scale(const double koeff) override;
    void rotate(double angle);
    rectangle_t getFrameRect() const override;
  private:
    std::unique_ptr< std::shared_ptr<Shape>[]> items_;
    size_t size_;
  };

}
#endif // COMPOSITESHAPE_HPP
