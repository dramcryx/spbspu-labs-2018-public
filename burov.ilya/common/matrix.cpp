#include "matrix.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <memory>
#include <vector>

using namespace burov;

Matrix::Matrix():
  items_(nullptr),
  figuresInLayer_(0),
  layers_(0),
  layerSize_(0)
{
}
Matrix::Matrix(const CompositeShape &object):
  Matrix()
{
  addCompShape(object);
}

Matrix::Matrix(const Matrix &object):
  items_(new std::shared_ptr<Shape>[object.layers_ * object.layerSize_]),
  figuresInLayer_(object.figuresInLayer_),
  layers_(object.layers_),
  layerSize_(object.layerSize_)
{
  for (int i = 0; i < (layers_ * layerSize_); i++)
  {
    items_[i] = object.items_[i];
  }
}
Matrix::~Matrix()
{
  items_.reset();
  items_ = nullptr;
  figuresInLayer_.resize(1);
  figuresInLayer_ = {0};
  layers_ = 0;
  layerSize_ = 0;
}
Matrix & Matrix::operator=(const Matrix &object)
{
  if (this == &object)
  {
    return *this;
  }
  items_.reset(new std::shared_ptr<Shape> [object.layerSize_*object.layers_] );
  figuresInLayer_ = object.figuresInLayer_;
  layers_=object.layers_;
  layerSize_=object.layerSize_;
  for (int i=0; i < (layers_*layerSize_); ++i)
  {
    items_[i]=object.items_[i];
  }
  return *this;
}
std::unique_ptr<std::shared_ptr<Shape>[]>Matrix::operator[](const int index) const
{
  if ((index < 0) || (index >= layers_))
  {
     throw std::out_of_range("Incorrect index");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> layer(new std::shared_ptr<Shape>[layerSize_]);
  for (int i = 0; i < layerSize_; ++i)
  {
    layer[i] = items_[index * layerSize_ + i];
  }
  return layer;
}

int Matrix::getLayerSize() const noexcept
{
  return layerSize_;
}

int Matrix::getFiguresInLayer(const int index)
{
  return figuresInLayer_[index];
}
int Matrix::getLayers() const noexcept
{
  return layers_;
}

bool Matrix::checkIntersection(const std::shared_ptr<Shape> &firstShape,const std::shared_ptr<Shape> &secondShape) const
{
  if (firstShape == nullptr || secondShape == nullptr)
  {
    return false;
  }
  rectangle_t firstShapeFrameRect = firstShape -> getFrameRect();
  rectangle_t secondShapeFramerect = secondShape -> getFrameRect();
  return ((fabs(firstShapeFrameRect.pos.x - secondShapeFramerect.pos.x)
      < ((firstShapeFrameRect.width / 2) + (secondShapeFramerect.width / 2)))
          && ((fabs(firstShapeFrameRect.pos.y - secondShapeFramerect.pos.y)
               < ((firstShapeFrameRect.height / 2) + (secondShapeFramerect.height / 2)))));

}

void Matrix::addShape(const std::shared_ptr<Shape> &object)
{
  if (object == nullptr)
  {
    throw std::invalid_argument("Incorrect pointer to shape");
  }
  if ((layers_ == 0) && (layerSize_ == 0))
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> copymatrix (new std::shared_ptr<Shape>[1]);
    items_.swap(copymatrix);
    layers_ = layerSize_ = 1;
    items_[0] = object;
    figuresInLayer_= {1};
    return;
  }
  int i = 0;
  for (; i < layers_; ++i)
  {
    int j = 0;
    for(; j < layerSize_; ++j)
    {
      if (!items_[i * layerSize_ + j])
      {
        items_[i * layerSize_ + j] = object;
        ++figuresInLayer_[i];
        return;
      }
      if (checkIntersection(items_[i * layerSize_ + j], object))
      {
        break;
      }
    }
    if (j == layerSize_)
    {
      std::unique_ptr<std::shared_ptr<Shape>[]> newMatrix(new std::shared_ptr<Shape>[layers_ * (layerSize_ + 1)]);
      for (int k = 0; k < layers_; ++k)
      {
        for (j = 0; j < layerSize_; ++j)
        {
          newMatrix[k * layerSize_ + j + k] = items_[k * layerSize_ + j];
        }
      }
      ++layerSize_;
      newMatrix[(i + 1) * layerSize_ - 1] = object;
      items_ = std::move(newMatrix);
      ++figuresInLayer_[i];
      return;
    }
  }
  if (i == layers_)
  {
  std::unique_ptr<std::shared_ptr<Shape>[]> newMatrix(new std::shared_ptr<Shape>[(layers_ + 1) * layerSize_]);
  for (int k = 0; k < layers_ * layerSize_; ++k)
  {
    newMatrix[k] = items_[k];
  }
  newMatrix[layers_ * layerSize_] = object;
  ++layers_;
  figuresInLayer_.resize(layers_);
  ++figuresInLayer_[layers_-1];
  items_ = std::move(newMatrix);
  }

}
void Matrix::addCompShape(const CompositeShape &composite)
{
  for (size_t i = 0; i < composite.getSize(); i++)
  {
    addShape(composite[i]);
  }
}






