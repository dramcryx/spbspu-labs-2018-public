#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include <vector>
#include "composite-shape.hpp"
#include "shape.hpp"

namespace burov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const CompositeShape &object);
    Matrix(const Matrix &object);
    ~Matrix();
    Matrix & operator=(const Matrix &object);
    std::unique_ptr<std::shared_ptr<Shape>[]> operator[](const int index) const;
    int getLayerSize() const noexcept;
    int getFiguresInLayer(const int index);
    int getLayers() const noexcept;
    bool checkIntersection(const std::shared_ptr<Shape> &firstShape, const std::shared_ptr<Shape> &secondShape) const;
    void addShape(const std::shared_ptr<Shape> &object);
    void addCompShape(const CompositeShape &object);
  private:
    std::unique_ptr <std::shared_ptr <Shape>[]> items_;
    std::vector<int> figuresInLayer_;
    int layers_;
    int layerSize_;
  };

};
#endif // MATRIX_HPP
