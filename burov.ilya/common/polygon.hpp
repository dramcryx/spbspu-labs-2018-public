#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <cstdlib>
#include <memory>

#include "shape.hpp"

namespace burov
{
  class Polygon : public Shape
  {
  public:
    Polygon(std::initializer_list <point_t> points);
    rectangle_t getFrameRect() const override;
    double getArea() const override;
    void move (double dx, double dy) override;
    void move (point_t point);
    point_t operator[] (size_t index) const;
    void scale (const double coeff) override;
    point_t getCenter() const ;
    void rotate (const double angle) override;
    void getInfo() const ;
  private:
    std::unique_ptr <point_t[]> vertexes_;
    size_t count_;
    point_t center_;
  };
}

#endif // POLYGON_HPP
