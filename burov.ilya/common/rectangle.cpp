#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

using namespace burov;
Rectangle::Rectangle(const rectangle_t &rect_init):
  m_rect(rect_init),
  angle_(0)
{
  if(m_rect.height < 0.0 || m_rect.width < 0.0)
  {
    throw std::invalid_argument("Wrong parametrs");
  }
}

double Rectangle::getArea() const
{
  return m_rect.height * m_rect.width;
}
rectangle_t Rectangle::getFrameRect() const
{
  return m_rect;
}

void Rectangle::move(const point_t & newpos)
{
  m_rect.pos = newpos;
}

void Rectangle::move(double dx, double dy)
{
  m_rect.pos.x += dx;
  m_rect.pos.y += dy;
}
void Rectangle::scale(double koeff)
{
  if (koeff < 0.0)
  {
    throw std::invalid_argument("scale koeff is incorrect!");
  }
  m_rect.height = m_rect.height * koeff;
  m_rect.width = m_rect.width * koeff;
}
rectangle_t Rectangle::getFrame() const
{
  double sinusAngle = sin(angle_ * M_PI / 180);
  double cosineAngle = cos(angle_ * M_PI / 180);
  double width = fabs(m_rect.width * cosineAngle ) + fabs(m_rect.height * sinusAngle);
  double height = fabs(m_rect.height * cosineAngle) + fabs(m_rect.width * sinusAngle);
  return  {m_rect.pos, width, height,};
}

void Rectangle::rotate(double angle)
{
  angle_+= angle;
  if (angle_ >= 360 || angle_ <= -360)
  {
    angle_ = fmod (angle_,360);
  }
}
