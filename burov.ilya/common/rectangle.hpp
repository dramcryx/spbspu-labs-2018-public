#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP


#include "shape.hpp"
namespace burov
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const rectangle_t & rect_init);
    virtual double getArea() const override;
    virtual rectangle_t getFrameRect() const override;
    virtual void move(const point_t & newpos) override;
    virtual void move(double dx, double dy) override;
    virtual void scale(double koeff) override;
    virtual rectangle_t getFrame() const;
    virtual void rotate (double angle) override;
  private:
    rectangle_t m_rect;
    double angle_;

  };
}

#endif // RECTANGLE_HPP
