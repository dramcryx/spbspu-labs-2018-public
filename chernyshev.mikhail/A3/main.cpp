#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

namespace chernyshev
{
void printInfo(Shape &shape)
{
  std::cout<<"Pos (x,y): (" << shape.getFrameRect().pos.x << "," << shape.getFrameRect().pos.y << ")" <<std::endl;
  std::cout<<"Area "<<shape.getArea()<<std::endl;
  std::cout<<"Width "<<shape.getFrameRect().width<<std::endl;
  std::cout<<"Heigt "<<shape.getFrameRect().height<<std::endl;
}
}
int main()
{
  try
  {
    std::shared_ptr<chernyshev::Shape> Rectangle1 = std::make_shared<chernyshev::Rectangle>(chernyshev::Rectangle(7.9, 8.2,{1.6, 2.0}));
    std::shared_ptr<chernyshev::Shape> Circle1 = std::make_shared<chernyshev::Circle>(chernyshev::Circle(6.9, {3.2, 2.6}));
    std::shared_ptr<chernyshev::Shape> Triangle1 = std::make_shared<chernyshev::Triangle>(chernyshev::Triangle({0.0,0.0},{2.0,2.0},{4.0,0.0}));
  
   chernyshev::CompositeShape compositeShape;
   compositeShape.addShape(Rectangle1);
   compositeShape.addShape(Circle1);
   compositeShape.addShape(Triangle1);
   chernyshev::printInfo(compositeShape);    
  
   
    compositeShape.scale(10);
    std::cout << "Scale on 10" << std::endl;
    chernyshev::printInfo(compositeShape);

    compositeShape.move({20, 30});
    std::cout << "Move to (20, 30) "<< std::endl;
    chernyshev::printInfo(compositeShape);

    compositeShape.move(5, 5);
    std::cout << "Move on dx = , dy = " << std::endl;
    chernyshev::printInfo(compositeShape);

    compositeShape.deleteShape(0);
    std::cout << "Deleted shape[0] " << std::endl;
    chernyshev::printInfo(compositeShape);
  }
  


  catch(std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
return 0;
}
