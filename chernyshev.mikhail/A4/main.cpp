#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

namespace chernyshev
{
void printInfo(Shape &shape)
{
  std::cout<<"Pos (x,y): (" << shape.getFrameRect().pos.x << "," << shape.getFrameRect().pos.y << ")" <<std::endl;
  std::cout<<"Area "<<shape.getArea()<<std::endl;
  std::cout<<"Width "<<shape.getFrameRect().width<<std::endl;
  std::cout<<"Heigt "<<shape.getFrameRect().height<<std::endl;
}
}
int main()
{
  try
  {
    chernyshev::Rectangle rectangle(7.9, 8.2,{1.6, 2.0});
    chernyshev::Circle circle(6.9, {3.2, 2.6});
    chernyshev::Triangle triangle({0.0,0.0},{2.0,2.0},{4.0,0.0});
    
    std::shared_ptr<chernyshev::Shape> Rectangle1 = std::make_shared<chernyshev::Rectangle>(rectangle);
    std::shared_ptr<chernyshev::Shape> Circle1 = std::make_shared<chernyshev::Circle>(circle);
    std::shared_ptr<chernyshev::Shape> Triangle1 = std::make_shared<chernyshev::Triangle>(triangle);
  
    std::cout << "Before rotete" << std::endl;
    chernyshev::printInfo(rectangle);
    rectangle.rotate(90);
    std::cout << "After rotate" << std::endl;
    chernyshev::printInfo(rectangle);
    
    std::cout << "Before rotete" << std::endl;
    chernyshev::printInfo(triangle);
    triangle.rotate(90);
    std::cout << "After rotate" << std::endl;
    chernyshev::printInfo(triangle);
    
    chernyshev::CompositeShape compositeShape;
    compositeShape.addShape(Rectangle1);
    compositeShape.addShape(Circle1);
    compositeShape.addShape(Triangle1);
   
    std::cout << "Before rotete" << std::endl;
    chernyshev::printInfo(compositeShape);
    compositeShape.rotate(90);
    std::cout << "After rotate" << std::endl;
    chernyshev::printInfo(compositeShape);
    
    chernyshev::Matrix matrix(Rectangle1);
    matrix.addShape(Circle1);
    matrix.addShape(Triangle1);
    std::cout << matrix.showLayers() << std::endl;
  }
  
  catch(std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  
return 0;
}
