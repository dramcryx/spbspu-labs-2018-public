#define BOOST_TEST_MODULE mainTest
#include <stdexcept>
#include <cmath>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace chernyshev;

const double delta = 0.01;

BOOST_AUTO_TEST_SUITE(TestRotate)
  BOOST_AUTO_TEST_CASE(RectangleRotate90x2)
  {
    Rectangle rectangle(5.0, 6.0,{3.0, 3.0});
    rectangle.rotate(90);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 6.0 , delta);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 5.0 , delta);
    rectangle.rotate(90);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 5.0 , delta);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 6.0 , delta);
  }

  BOOST_AUTO_TEST_CASE(RectangleRotate180)
  {
    Rectangle rectangle(5.0, 6.0,{3.0, 3.0});
    rectangle.rotate(180);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 5.0 , delta);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 6.0 , delta);
  }
  
  BOOST_AUTO_TEST_CASE(RectangleRotate360)
  {
    Rectangle rectangle(5.0, 6.0,{3.0, 3.0});
    rectangle.rotate(360);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 5.0 , delta);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 6.0 , delta);
  }
  
  
  BOOST_AUTO_TEST_CASE(TriangleRotate90x2)
  {
    Triangle triangle({0.0,0.0},{2.0,2.0},{4.0,0.0});
    triangle.rotate(90);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, 2.0 , delta);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, 4.0 , delta);
    triangle.rotate(90);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, 4.0 , delta);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, 2.0 , delta);
    triangle.rotate(90);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, 2.0 , delta);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, 4.0 , delta);
  }
  
  BOOST_AUTO_TEST_CASE(TriangleRotate180)
  {
    Triangle triangle({0.0,0.0},{2.0,2.0},{4.0,0.0});
    triangle.rotate(360);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, 4.0 , delta);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, 2.0 , delta);
  }
  
  BOOST_AUTO_TEST_CASE(CompositeShapeRotate)
  {
    chernyshev::Rectangle rectangle(5.0, 6.0 ,{1.0, 1.0});
    chernyshev::Circle circle(3.0 , {5.0, 5.0});
    std::shared_ptr< chernyshev::Shape > rectanglePtr = std::make_shared< chernyshev::Rectangle >(rectangle);
    std::shared_ptr< chernyshev::Shape > circlePtr = std::make_shared< chernyshev::Circle >(circle);
    chernyshev::CompositeShape comp(rectanglePtr);
    comp.addShape(circlePtr);
    comp.rotate(90);
    BOOST_CHECK_CLOSE(comp.getFrameRect().width, 10.0 , delta);
    BOOST_CHECK_CLOSE(comp.getFrameRect().height, 9.5 , delta);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, 1.0, delta);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, 1.0, delta);
    BOOST_CHECK_CLOSE(circle.getFrameRect().pos.x, 5.0, delta);
    BOOST_CHECK_CLOSE(circle.getFrameRect().pos.y, 5.0, delta);
  }
BOOST_AUTO_TEST_SUITE_END()
  
BOOST_AUTO_TEST_SUITE(TestMatrix)

  BOOST_AUTO_TEST_CASE(TestingLayersMatrix)
  {
    chernyshev::Rectangle rectangle1(10.0, 10.0,{3.0, 2.0});
    chernyshev::Rectangle rectangle2(5.0, 6.0, {4.0, 3.0});
    chernyshev::Circle circle1(3.0, {50.0, 50.0});

    std::shared_ptr< chernyshev::Shape > rectanglePtr1 = std::make_shared< chernyshev::Rectangle >(rectangle1);
    std::shared_ptr< chernyshev::Shape > rectanglePtr2 = std::make_shared< chernyshev::Rectangle >(rectangle2);
    std::shared_ptr< chernyshev::Shape > circlePtr1 = std::make_shared< chernyshev::Circle >(circle1);

    chernyshev::Matrix matrix(rectanglePtr1);
    matrix.addShape(rectanglePtr2);
    matrix.addShape(circlePtr1);

    std::unique_ptr< std::shared_ptr< chernyshev::Shape >[] > layer1 = matrix[0];
    std::unique_ptr< std::shared_ptr< chernyshev::Shape >[] > layer2 = matrix[1];

    BOOST_CHECK_CLOSE(layer1[0]->getFrameRect().pos.x, 3.0 , delta);
    BOOST_CHECK_CLOSE(layer1[0]->getFrameRect().pos.y, 2.0 , delta);
    BOOST_CHECK_CLOSE(layer1[1]->getFrameRect().pos.x, 50.0 , delta);
    BOOST_CHECK_CLOSE(layer1[1]->getFrameRect().pos.y, 50.0 , delta);
    BOOST_CHECK_CLOSE(layer2[0]->getFrameRect().pos.x, 4.0 , delta);
    BOOST_CHECK_CLOSE(layer2[0]->getFrameRect().pos.y, 3.0 , delta);
  }

BOOST_AUTO_TEST_SUITE_END()
