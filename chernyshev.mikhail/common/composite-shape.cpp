#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

chernyshev::CompositeShape::CompositeShape():
  size_(0),
  shapes_(nullptr),
  angle_(0.0)
{
};

chernyshev::CompositeShape::CompositeShape(const std::shared_ptr<Shape>& object):
  size_(0),
  shapes_(nullptr),
  angle_(0.0)
{
  if (object == nullptr)
  {
    throw std::invalid_argument("Error!");
  }
  addShape(object);
}
chernyshev::CompositeShape::CompositeShape(const CompositeShape &object):
  size_(object.takeSize())    
{
  std::unique_ptr<Shape::ptr_type []> copyArr(new Shape::ptr_type [object.takeSize()]);
  for (size_t i = 0;i < object.takeSize(); i++)
  {
    copyArr[i] = object.shapes_[i];
  }
  shapes_.swap(copyArr);
}
   
chernyshev::CompositeShape::CompositeShape(CompositeShape&& object):
  size_(object.takeSize())
{
  shapes_.swap(object.shapes_);
}

chernyshev::CompositeShape::~CompositeShape()
{
}

chernyshev::CompositeShape & chernyshev::CompositeShape::operator =(const CompositeShape& object)
{
  if (this != &object)
  {
    std::unique_ptr<Shape::ptr_type []> copyArr(new Shape::ptr_type[object.takeSize()]);
    for(size_t i = 0; i < object.takeSize(); i++)
    {
      copyArr[i] = object.shapes_[i];
    }
    shapes_.swap(copyArr);
  }
  return *this;
}

chernyshev::CompositeShape & chernyshev::CompositeShape::operator =(CompositeShape&& object)
{
  if (this != &object)
  {
    shapes_=std::move(object.shapes_);
    object.shapes_=nullptr;
    object.size_=0;
  }
  return *this;
}

chernyshev::Shape::ptr_type chernyshev::CompositeShape::operator [](size_t index)
{
  if (index >= takeSize())
  {
    throw std::out_of_range("Index error");
  }
  return shapes_[index];
}

void chernyshev::CompositeShape::addShape(const Shape::ptr_type& shape)
{
  if (!shape)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  if (shape.get() == this)
  {
    throw std::invalid_argument("Same shape");
  }
  
  std::unique_ptr<Shape::ptr_type []> copyArr(new Shape::ptr_type[takeSize()+1]);
  
  for (size_t i = 0; i < takeSize(); i++)
  {
    copyArr[i] = shapes_[i];
  }
  copyArr[size_] = shape;
  shapes_.swap(copyArr);
  size_ ++;
}

void chernyshev::CompositeShape::deleteShape(size_t index)
{
  if (index >= takeSize())
  {
    std::out_of_range("Index out of range");
  }
  if (takeSize() == 0)
  {
    std::invalid_argument("Shape is empty");
  }
  
  std::unique_ptr<Shape::ptr_type [] > copyArr(new Shape::ptr_type [takeSize()-1]);
  for (size_t i = 0; i < index; i++)
  {
    copyArr[i] = shapes_[i];
  }
  
  for (size_t i = index; i < (takeSize() - 1); i++)
  {
    copyArr[i] = shapes_[i+1];
  }
  shapes_.swap(copyArr);
  size_ --;
}

double chernyshev::CompositeShape::getArea() const
{
  double allArea = 0.0;
  for (size_t i = 0; i < takeSize(); i++)
  {
    allArea += shapes_[i]->getArea();
  }
  return allArea;
}

chernyshev::rectangle_t chernyshev::CompositeShape::getFrameRect() const
{
  if (takeSize() == 0)
  {
    return {0.0,0.0,{0.0,0.0}};
  }
  rectangle_t rectangle = shapes_[0]->getFrameRect();
  double minX = rectangle.pos.x - rectangle.width/2;
  double maxX = rectangle.pos.x - rectangle.width/2;
  double minY = rectangle.pos.y - rectangle.height/2;
  double maxY = rectangle.pos.y - rectangle.height/2;
  
  for (size_t i = 0; i < takeSize(); i++)
  {
    rectangle_t rectangle = shapes_[i]->getFrameRect();
    double newMinX = rectangle.pos.x - rectangle.width/2;
    double newMaxX = rectangle.pos.x + rectangle.width/2;
    double newMinY = rectangle.pos.y - rectangle.height/2;
    double newMaxY = rectangle.pos.y + rectangle.height/2;
    
    minX = std::min(minX, newMinX);
    maxX = std::max(maxX, newMaxX);
    minY = std::min(minY, newMinY);
    maxY = std::max(maxY, newMaxY);
  }
  return chernyshev::rectangle_t {maxX-minX,maxY - minY,{((minX + maxX) / 2.0), ((minY + maxY) / 2.0)}};
}

void chernyshev::CompositeShape::move(double delta_x, double delta_y)
{
  for (size_t i = 0; i < takeSize(); i++)
  {
    shapes_[i]->move(delta_x,delta_y);
  }
}

void chernyshev::CompositeShape::move(chernyshev::point_t newPoint)
{
  double dx = newPoint.x - getFrameRect().pos.x;
  double dy = newPoint.y - getFrameRect().pos.y;
  
  for (size_t i = 0; i < takeSize(); i++)
  {
    shapes_[i]->move(dx,dy);
  }
}

void chernyshev::CompositeShape::scale(double size)
{
  if (size < 0.0)
  {
    throw std::invalid_argument("Error ratio");
  }
  
  for (size_t i = 0; i < takeSize(); i++)
  {
    shapes_[i]->scale(size);
    shapes_[i]->move((shapes_[i]->getFrameRect().pos.x - getFrameRect().pos.x) * size,
                     (shapes_[i]->getFrameRect().pos.y - getFrameRect().pos.y) * size);
    
  }
}

size_t chernyshev::CompositeShape::takeSize() const
{
  return size_;
}

void chernyshev::CompositeShape::rotate(const double angle)
{
  angle_ += angle;
  if (std::abs(angle_) >= 360.0)
  {
    angle_ = fmod(angle, 360.0);
  }
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  const chernyshev::point_t curPos = getFrameRect().pos;
  for (size_t i = 0; i < size_; ++i)
  {
    const chernyshev::point_t shapePos = shapes_[i]-> getFrameRect().pos;
    shapes_[i]->move({(shapePos.x - curPos.x) * cosine - (shapePos.y - curPos.y) * sine + curPos.x,
      (shapePos.y - curPos.y) * cosine + (shapePos.x - curPos.x) * sine + curPos.y});
    shapes_[i]->rotate(angle);
  }
}
