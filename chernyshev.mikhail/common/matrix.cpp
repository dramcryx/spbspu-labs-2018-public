#include "matrix.hpp"
#include <memory>
#include <stdexcept>

chernyshev::Matrix::Matrix(const Shape::ptr_type shape):
  list_(nullptr),
  layersNumber_(0),
  layerSize_(0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Invalid pointeer");
  }
  addShape(shape);
}

chernyshev::Matrix::Matrix(const chernyshev::Matrix & matrix):
  list_(new Shape::ptr_type [matrix.layersNumber_ * matrix.layerSize_]),
  layersNumber_(matrix.layersNumber_),
  layerSize_(matrix.layerSize_)
{
  for (size_t i = 0; i < layersNumber_ * layerSize_; ++i)
  {
    list_[i] = matrix.list_[i];
  }
}

chernyshev::Matrix::Matrix(chernyshev::Matrix && matrix):
  list_(nullptr),
  layersNumber_(matrix.layersNumber_),
  layerSize_(matrix.layerSize_)
{
  list_.swap(matrix.list_);
  matrix.layersNumber_ = 0;
  matrix.layerSize_ = 0;
}

chernyshev::Matrix & chernyshev::Matrix::operator = (const chernyshev::Matrix & matrix)
{
  if (this != & matrix)
  {
    layersNumber_ = matrix.layersNumber_;
    layerSize_ = matrix.layerSize_;
    std::unique_ptr< Shape::ptr_type [] > newList(new Shape::ptr_type [layersNumber_ * layerSize_]);
    for (size_t i = 0; i < layersNumber_ * layerSize_; ++i)
    {
      newList[i] = matrix.list_[i];
    }
    list_.swap(newList);
  }
  return *this;
}

chernyshev::Matrix & chernyshev::Matrix::operator = (chernyshev::Matrix && matrix)
{
  if (this != & matrix)
  {
    layerSize_ = matrix.layerSize_;
    layersNumber_ = matrix.layersNumber_;
    list_.swap(matrix.list_);
    matrix.list_ = nullptr;
    matrix.layersNumber_ = 0;
    matrix.layerSize_ = 0;
  }
  return *this;
}

std::unique_ptr< chernyshev::Shape::ptr_type [] > chernyshev::Matrix::operator[](const size_t layerIndex) const
{
  if (layerIndex >= layersNumber_)
  {
    throw std::invalid_argument("Invalid index");
  }
  std::unique_ptr< Shape::ptr_type [] > layer(new Shape::ptr_type [layerSize_]);
  for (size_t i = 0; i < layerSize_; ++i)
  {
    layer[i] = list_[layerIndex * layerSize_ + i];
  }
  return layer;
}

void chernyshev::Matrix::addShape(const Shape::ptr_type shape)
{
  if (layersNumber_ == 0)
  {
    ++layerSize_;
    ++layersNumber_;
    std::unique_ptr< Shape::ptr_type [] > newList(new Shape::ptr_type [layersNumber_ * layerSize_]);
    list_.swap(newList);
    list_[0] = shape;
  }
  else
  {
    bool addedShape = false;
    for (size_t i = 0; !addedShape ; ++i)
    {
      for (size_t j = 0; j < layerSize_; ++j)
      {
        if (list_[i * layerSize_ + j] == nullptr)
        {
          list_[i * layerSize_ + j] = shape;
          addedShape = true;
          break;
        }
        else
        {
          if (checkOverlapping(i * layerSize_ + j, shape))
          {
            break;
          }
        }

        if (j == (layerSize_ - 1))
        {
          layerSize_++;
          std::unique_ptr< Shape::ptr_type [] > newList(new Shape::ptr_type  [layersNumber_ * layerSize_]);
          for (size_t n = 0; n < layersNumber_; ++n)
          {
            for (size_t m = 0; m < layerSize_ - 1; ++m)
            {
              newList[n * layerSize_ + m] = list_[n * (layerSize_ - 1) + m];
            }
            newList[(n + 1) * layerSize_ - 1] = nullptr;
          }
          newList[(i + 1) * layerSize_ - 1] = shape;
          list_.swap(newList);
          addedShape = true;
          break;
        }
      }
      if ((i == (layersNumber_ - 1)) && !addedShape)
      {
        layersNumber_++;
        std::unique_ptr< Shape::ptr_type [] > newList(new Shape::ptr_type [layersNumber_ * layerSize_]);
        for (size_t n = 0; n < ((layersNumber_ - 1) * layerSize_); ++n)
        {
          newList[n] = list_[n];
        }
        for (size_t n = ((layersNumber_  - 1) * layerSize_) ; n < (layersNumber_ * layerSize_); ++n)
        {
          newList[n] = nullptr;
        }
        newList[(layersNumber_ - 1 ) * layerSize_ ] = shape;
        list_.swap(newList);
        addedShape = true;
      }
    }
  }
}

bool chernyshev::Matrix::checkOverlapping(const size_t index, Shape::ptr_type shape) const
{
  rectangle_t sShapeFrameRect = shape->getFrameRect();
  rectangle_t lShapeFrameRect = list_[index]->getFrameRect();
  point_t newPoints[4] = {
    {sShapeFrameRect.pos.x - sShapeFrameRect.width / 2.0, sShapeFrameRect.pos.y + sShapeFrameRect.height / 2.0},
    {sShapeFrameRect.pos.x + sShapeFrameRect.width / 2.0, sShapeFrameRect.pos.y + sShapeFrameRect.height / 2.0},
    {sShapeFrameRect.pos.x + sShapeFrameRect.width / 2.0, sShapeFrameRect.pos.y - sShapeFrameRect.height / 2.0},
    {sShapeFrameRect.pos.x - sShapeFrameRect.width / 2.0, sShapeFrameRect.pos.y - sShapeFrameRect.height / 2.0}
  };

  point_t matrixPoints[4] = {
    {lShapeFrameRect.pos.x - lShapeFrameRect.width / 2.0, lShapeFrameRect.pos.y + lShapeFrameRect.height / 2.0},
    {lShapeFrameRect.pos.x + lShapeFrameRect.width / 2.0, lShapeFrameRect.pos.y + lShapeFrameRect.height / 2.0},
    {lShapeFrameRect.pos.x + lShapeFrameRect.width / 2.0, lShapeFrameRect.pos.y - lShapeFrameRect.height / 2.0},
    {lShapeFrameRect.pos.x - lShapeFrameRect.width / 2.0, lShapeFrameRect.pos.y - lShapeFrameRect.height / 2.0}
  };

  for (int i = 0; i < 4; ++i)
  {
    if (((newPoints[i].x >= matrixPoints[0].x) && (newPoints[i].x <= matrixPoints[2].x)
      && (newPoints[i].y >= matrixPoints[3].y) && (newPoints[i].y <= matrixPoints[1].y))
        || ((matrixPoints[i].x >= newPoints[0].x) && (matrixPoints[i].x <= newPoints[2].x)
          && (matrixPoints[i].y >= newPoints[3].y) && (matrixPoints[i].y <= newPoints[1].y)))
    {
      return true;
    }
  }
  return false;
}

size_t chernyshev::Matrix::showLayers() const
{
  return layersNumber_;
}
