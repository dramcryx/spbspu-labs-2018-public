#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace chernyshev
{
  class Matrix
  {
  public:
    Matrix(const Shape::ptr_type shape);
    Matrix(const Matrix & matrix);
    Matrix(Matrix && matrix);
    Matrix & operator = (const Matrix & matrix);
    Matrix & operator = (Matrix && matrix);
    std::unique_ptr< Shape::ptr_type[] > operator[](const size_t layerIndex) const;
    void addShape(const Shape::ptr_type shape);
    size_t showLayers()const;
  private:
    std::unique_ptr < Shape::ptr_type [] > list_;
    size_t layersNumber_;//количество слоев
    size_t layerSize_;
    bool checkOverlapping(const size_t index, Shape::ptr_type shape) const;
  };
}
#endif /* MATRIX_HPP */
