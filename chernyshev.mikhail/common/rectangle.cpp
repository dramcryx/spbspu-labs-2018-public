#include "rectangle.hpp"
#include <iostream>
#include <cassert>
#include <cmath>

chernyshev::Rectangle::Rectangle(double h,double w,point_t c): height_(h),width_(w),center_(c),angle_(0)
{
  if (h<0.0)
  {
    throw std::invalid_argument("Hight must be >=0");
  }
  if (w<0.0)
  {
    throw std::invalid_argument("Width must be >=0");
  }
}

double chernyshev::Rectangle::getArea()const
{
  return height_*width_;
}

chernyshev::rectangle_t chernyshev::Rectangle::getFrameRect()const
{ 
  /*rectangle_t rect;
  rect.width = (height_ * std::abs(cos(angle_ * M_PI / 180))) + (width_ * std::abs(sin(angle_ * M_PI / 180)));
  rect.height = (height_ * std::abs(sin(angle_ *M_PI / 180))) + (width_ * std::abs(cos(angle_ * M_PI / 180)));
  rect.pos = center_;
  return rectangle_t{width_,height_,center_};;*/
  double tmpSin = std::abs(sin((angle_*M_PI)/180.0));
  double tmpCos = std::abs(cos((angle_*M_PI)/180.0));
  return rectangle_t{width_*tmpSin+height_*tmpCos,width_*tmpCos+height_*tmpSin,center_};
}    

void chernyshev::Rectangle::move(double delta_x,double delta_y)
{
  center_.x += delta_x;
  center_.y += delta_y;
}
 
void chernyshev::Rectangle::move(point_t newPoint)
{
  center_.x = newPoint.x;
  center_.y = newPoint.y;
}

void chernyshev::Rectangle::scale(double size)
{
  if (size < 0.0)
  {
    throw std::invalid_argument("Koeff must be >=0");
  }
  height_ *= size;
  width_ *= size;
}

void chernyshev::Rectangle::printScale() const
{
  std::cout<<"Scale. Height is "<<height_<<std::endl;
  std::cout<<"Scale. Width is "<<width_<<std::endl;
}

void chernyshev::Rectangle::rotate(const double angle)
{
  angle_+=angle;
  if (abs(angle_) >= 360.0)
  {
    angle_ = std::fmod(angle_,360.0);
  }
}
