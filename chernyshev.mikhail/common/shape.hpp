#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <memory>
#include "base-types.hpp"

namespace chernyshev
{
  class Shape 
  {
  public:    
    using ptr_type=std::shared_ptr<Shape>;
    
    virtual ~Shape() = default;
    
    virtual double getArea()const = 0;
    virtual rectangle_t getFrameRect()const = 0;
    virtual void move(double delta_x,double delta_y) = 0;
    virtual void move(point_t newPoint) = 0;
    virtual void scale(double size) = 0; 
    virtual void rotate(const double angle) =0;
  };
}
#endif
