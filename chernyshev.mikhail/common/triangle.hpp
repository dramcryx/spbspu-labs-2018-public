#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include <cstdlib>
#include "shape.hpp"
#include "base-types.hpp"

namespace chernyshev
{
  class Triangle : public Shape
  {
  public:
    Triangle(point_t a ,point_t b ,point_t c);
    double getArea()const override;
    rectangle_t getFrameRect() const override;
    void move(double delta_x,double delta_y) override;
    void move(point_t newPoint) override;
    void scale(double size) override;//koeff- коэффициент увеличения(уменьшения) фигуры
    void printScale()const;//для вывода данных
    void rotate(const double angle) override;
  private:
    point_t center_;
    point_t a_;
    point_t b_;
    point_t c_;    
    double angle_;
  };
}
#endif
