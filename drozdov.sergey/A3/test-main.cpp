#define BOOST_TEST_MODULE test-Main
#include <boost/test/included/unit_test.hpp>
#include <cmath>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

using namespace drozdov;

const double EPS = 0.01;


BOOST_AUTO_TEST_SUITE(Rectangle_Tests)

  BOOST_AUTO_TEST_CASE(Permanency_Of_Rectangle_After_Centre_Movement)
  {
    Rectangle objRect({17.32, 52.47, {30.54, 55.98}});
    objRect.move(3.12, 7.75);
    BOOST_REQUIRE_CLOSE((objRect.getFrameRect().width), 17.32, EPS);
    BOOST_REQUIRE_CLOSE((objRect.getFrameRect().height), 52.47, EPS);
    BOOST_REQUIRE_CLOSE(objRect.getArea(), (17.32 * 52.47), EPS);
  }

  BOOST_AUTO_TEST_CASE(Permanecy_Of_Rectangle_After_Axis_Offset)
  {
    Rectangle objRect({17.32, 52.47, {30.54, 55.98}});
    objRect.move(43.21, 54.22);
    BOOST_REQUIRE_CLOSE((objRect.getFrameRect().width), 17.32, EPS);
    BOOST_REQUIRE_CLOSE((objRect.getFrameRect().height), 52.47, EPS);
    BOOST_REQUIRE_CLOSE(objRect.getArea(), (17.32 * 52.47), EPS);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_Quadratic_Change_Area_After_Scale)
  {
    Rectangle objRect({17.32, 52.47, {30.54, 55.98}});
    objRect.scale(3);
    BOOST_REQUIRE_CLOSE(objRect.getArea(), (17.32 * 52.47 * pow(3,2)), EPS);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_Check_Incorrect_Parametrs)
  {
    BOOST_CHECK_THROW(Rectangle objRect({-1, 52.47, {30.54, 55.98}}), std::invalid_argument);
    BOOST_CHECK_THROW(Rectangle objRect({17.32, -10, {30.54, 55.98}}), std::invalid_argument);
    Rectangle objRect({17.32, 52.47, {30.54, 55.98}});
    BOOST_CHECK_THROW(objRect.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rectangle_Check_Incorrect_Parametrs_2)
  {
    BOOST_CHECK_THROW(Rectangle objRect({-1, -52.47, {30.54, 55.98}}), std::invalid_argument);
    BOOST_CHECK_THROW(Rectangle objRect({-17.32, -10, {30.54, 55.98}}), std::invalid_argument);
    Rectangle objRect({17.32, 52.47, {30.54, 55.98}});
    BOOST_CHECK_THROW(objRect.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Circle_Tests)

  BOOST_AUTO_TEST_CASE(Permanency_Of_Circle_After_Center_Movement)
  {
    Circle objCircle(4.12, {18.08, 37.01});
    objCircle.move({13.51, 12.25});
    BOOST_REQUIRE_CLOSE((objCircle.getFrameRect().width / 2), 4.12, EPS);
    BOOST_REQUIRE_CLOSE(objCircle.getArea(), (M_PI * 4.12 * 4.12), EPS);
  }

  BOOST_AUTO_TEST_CASE(Permanency_Of_Circle_After_Axis_Offset)
  {
    Circle objCircle(4.12, {30.15, 41.81});
    objCircle.move(43.21, 54.22);
    BOOST_REQUIRE_CLOSE((objCircle.getFrameRect().width / 2), 4.12, EPS);
    BOOST_REQUIRE_CLOSE(objCircle.getArea(), (M_PI * 4.12 * 4.12), EPS);
  }

  BOOST_AUTO_TEST_CASE(Circle_Quadratic_Area_Change_After_Scaling)
  {
    Circle objCircle(4.12, {30.15, 41.81});
    objCircle.scale(3);
    BOOST_REQUIRE_CLOSE(objCircle.getArea(), (M_PI * 4.12 * 4.12 * pow(3,2)), EPS);
  }

  BOOST_AUTO_TEST_CASE(Circle_Check_Incorrect_Parametrs)
  {
    BOOST_CHECK_THROW(Circle objCircle(-1, {30.15, 41.81}), std::invalid_argument);
    Circle objCircle(4.12, {30.15, 41.81});
    BOOST_CHECK_THROW(objCircle.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Triangle_Tests)

  BOOST_AUTO_TEST_CASE(Absolute_Move_Test_Triangle)
  {
    Triangle objTriangle({10.6, 13.2}, {15.2, 18.0}, {11.1, 15.6});
    objTriangle.move({53.3, 20.4});
    BOOST_CHECK_CLOSE_FRACTION(objTriangle.getPos().x, 53.3, EPS);
    BOOST_CHECK_CLOSE_FRACTION(objTriangle.getPos().y, 20.4, EPS);
  }

  BOOST_AUTO_TEST_CASE(Relative_Move_Test_Triangle)
  {
    Triangle objTriangle({10.6, 13.2}, {15.2, 18.0}, {11.1, 15.6});
    objTriangle.move(-10.1, 0.4);
    BOOST_CHECK_CLOSE_FRACTION(objTriangle.getPos().x, 2.2, EPS);
    BOOST_CHECK_CLOSE_FRACTION(objTriangle.getPos().y, 16.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Area_Test_Triangle)
  {
    Triangle objTriangle({10.4, 13.2}, {15.3, 18.13}, {11.1, 15.6});
    BOOST_CHECK_CLOSE_FRACTION(objTriangle.getArea(), 4.155, EPS);
  }

  BOOST_AUTO_TEST_CASE(Area_Test_With_Move_Triangle)
  {
    Triangle objTriangle({10.4, 13.2}, {15.3, 18.13}, {11.1, 15.6});
    objTriangle.move(10, 5);
    BOOST_CHECK_CLOSE_FRACTION(objTriangle.getArea(), 4.155, EPS);
  }

  BOOST_AUTO_TEST_CASE(Frame_Rect_Test_Triangle)
  {
    Triangle objTriangle({10.4, 13.2}, {15.3, 18.13}, {11.1, 15.6});
    BOOST_REQUIRE_EQUAL(objTriangle.getFrameRect().width, 4.9);
    BOOST_REQUIRE_EQUAL(objTriangle.getFrameRect().height, 4.93);
    BOOST_CHECK_CLOSE_FRACTION(objTriangle.getFrameRect().pos.x, 12.85, EPS);
    BOOST_CHECK_CLOSE_FRACTION(objTriangle.getFrameRect().pos.y, 15.665, EPS);
  }

  BOOST_AUTO_TEST_CASE(Scale_Test_Triangle)
  {
    Triangle objTriangle({0.0, 2.0}, {-2.0, -1.0}, {2.0, -1.0});
    objTriangle.scale(3.0);
    BOOST_CHECK_CLOSE_FRACTION(objTriangle.getArea(), 54, EPS);
  }

  BOOST_AUTO_TEST_CASE(Inavalid_Argumend_Constructor_Test_Triangle)
  {
    BOOST_CHECK_THROW(Triangle({0, 0}, {-2, 0}, {2, 0}), std::invalid_argument);
    BOOST_CHECK_THROW(Triangle({-2, 0}, {0, 0}, {2, 0}), std::invalid_argument);
    BOOST_CHECK_THROW(Triangle({-2, 0}, {2, 0}, {0, 0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Argument_Scale_Test_Triangle)
  {
    Triangle objTriangle({10.6, 13.2}, {15.2, 18.0}, {11.1, 15.6});
    BOOST_CHECK_THROW(objTriangle.scale(-1), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(COMP_TESTS)

  BOOST_AUTO_TEST_CASE(Nopointer_And_Index)
  {
    BOOST_CHECK_THROW(CompositeShape compShape(nullptr), std::invalid_argument);
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle({5.0, 5.0, {0.0, 0.0}}));
    CompositeShape compShape(rectPtr);
    BOOST_CHECK_THROW(compShape.removeShape(2),std::invalid_argument);
  }

    BOOST_AUTO_TEST_CASE(Test_Information_Move)
  {
    Circle circ(5.0, {0.0, 0.0});
    Rectangle rect({5.0, 5.0, {0.0, 0.0}});
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle> (circ);
    std::shared_ptr <Shape> rectPtr = std::make_shared <Rectangle> (rect);
    CompositeShape comp(circPtr);
    comp.addShape(rectPtr);
    CompositeShape comp1 = std::move(comp);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().height, 10, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().width, 10, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.x, 0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.y, 0, EPS);
  }

   BOOST_AUTO_TEST_CASE(Test_Information_Operator_Move)
  {
    Circle circ(5.0, {0.0, 0.0});
    Rectangle rect({5.0, 5.0, {0.0, 0.0}});
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle> (circ);
    std::shared_ptr <Shape> rectPtr = std::make_shared <Rectangle> (rect);
    CompositeShape comp(circPtr);
    comp.addShape(rectPtr);
    CompositeShape comp1(rectPtr);
    comp = std::move(comp1);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, 5, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, 5, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().height, 0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().width, 0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.x, 0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.y, 0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Test_Information_Copy)
  {
    Circle circ(5.0, {0.0, 0.0});
    Rectangle rect({5.0, 5.0, {0.0, 0.0}});
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle> (circ);
    std::shared_ptr <Shape> rectPtr = std::make_shared <Rectangle> (rect);
    CompositeShape comp(circPtr);
    comp.addShape(rectPtr);
    CompositeShape comp1 = comp;
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().height, 10, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().width, 10, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.x, 0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp1.getFrameRect().pos.y, 0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, comp1.getFrameRect().height, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, comp1.getFrameRect().width, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, comp1.getFrameRect().pos.x, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, comp1.getFrameRect().pos.y, EPS);
  }

BOOST_AUTO_TEST_CASE(Test_Information_Operator_Copy)
  {
    Circle circ(5.0, {0.0, 0.0});
    Rectangle rect({5.0, 5.0, {0.0, 0.0}});
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle> (circ);
    std::shared_ptr <Shape> rectPtr = std::make_shared <Rectangle> (rect);
    CompositeShape comp(circPtr);
    comp.addShape(rectPtr);
    CompositeShape comp1(rectPtr);
    comp = comp1;
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, 5, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, 5, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, comp1.getFrameRect().height, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, comp1.getFrameRect().width, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, comp1.getFrameRect().pos.x, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, comp1.getFrameRect().pos.y, EPS);
  }

  BOOST_AUTO_TEST_CASE(Rect)
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle({5.0, 5.0, {0.0, 0.0}}));
    CompositeShape compShape(rectPtr);

    BOOST_CHECK_CLOSE(compShape.getArea(), 25.0, EPS);
    compShape.scale(2.0);
    BOOST_CHECK_CLOSE(compShape.getArea(), 100.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Rect_Plus_Circ)
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle ({5.0, 5.0, {0.0, 0.0}}));
    CompositeShape compShape(rectPtr);
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle (5.0, {0.0, 0.0}));
    compShape.addShape(circPtr);

    BOOST_CHECK_CLOSE(compShape.getArea(),25.0 + M_PI * 5.0 * 5.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Rect_Plus_Triangle)
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle ({5.0, 5.0, {0.0, 0.0}}));
    CompositeShape compShape(rectPtr);
    std::shared_ptr<Shape> trianglePtr = std::make_shared<Triangle>(Triangle ({10.4, 13.2}, {15.3, 18.13}, {11.1, 15.6}));
    compShape.addShape(trianglePtr);

    BOOST_CHECK_CLOSE(compShape.getArea(), (25.0 + 4.155), EPS);
  }

  BOOST_AUTO_TEST_CASE(Circle_Plus_Triangle)
  {
    std::shared_ptr<Shape> circlePtr = std::make_shared<Circle>(Circle (5.0, {0.0, 0.0}));
    CompositeShape compShape(circlePtr);
    std::shared_ptr<Shape> trianglePtr = std::make_shared<Triangle>(Triangle ({10.4, 13.2}, {15.3, 18.13}, {11.1, 15.6}));
    compShape.addShape(trianglePtr);

    BOOST_CHECK_CLOSE(compShape.getArea(), (M_PI * 5.0 * 5.0 + 4.155), EPS);
  }


  BOOST_AUTO_TEST_CASE(All_Figures)
  {
    CompositeShape compShape;
    BOOST_CHECK_CLOSE(compShape.getArea(), 0, EPS);
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle ({5.0, 5.0, {0.0, 0.0}}));
    compShape.addShape(rectPtr);
    BOOST_CHECK_CLOSE(compShape.getArea(), 25.0, EPS);
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle (5.0, {0.0, 0.0}));
    compShape.addShape(circPtr);
    BOOST_CHECK_CLOSE(compShape.getArea(), 25 + M_PI * 5.0 * 5.0, EPS);
    std::shared_ptr<Shape> trianglePtr = std::make_shared<Triangle>(Triangle ({10.4, 13.2}, {15.3, 18.13}, {11.1, 15.6}));
    compShape.addShape(trianglePtr);
    BOOST_CHECK_CLOSE(compShape.getArea(), (25 + M_PI * 5.0 * 5.0 + 4.155), EPS);
  }

  BOOST_AUTO_TEST_CASE(Movement)
  {
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(Rectangle ({5.0, 5.0, {0.0, 0.0}}));
    CompositeShape compShape(rectPtr);
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle (5.0, {0.0, 0.0}));
    compShape.addShape(circPtr);
    std::shared_ptr<Shape> trianglePtr = std::make_shared<Triangle>(Triangle ({10.4, 13.2}, {15.3, 18.13}, {11.1, 15.6}));
    compShape.addShape(trianglePtr);
    compShape.move({5.0, 5.0});

    BOOST_CHECK_CLOSE(compShape.getArea(), (25.0 + M_PI * 5.0 * 5.0 + 4.155), EPS);
    BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.y, 5.0, EPS);
    BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.x, 5.0, EPS);

    compShape.move(5.0, 5.0);

    BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.x, 10.0, EPS);
    BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.y, 10.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Scale_Area_Test_CS)
  {
    Circle circ(5.0, {0.0, 0.0});
    Rectangle rect({5.0, 5.0, {0.0, 0.0}});
    Triangle triangle({10.4, 13.2}, {15.3, 18.13}, {11.1, 15.6});
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(circ);
    std::shared_ptr<Shape> rectPtr = std::make_shared<Rectangle>(rect);
    std::shared_ptr<Shape> trianglePtr = std::make_shared<Triangle>(triangle);
    CompositeShape compShape(circPtr);
    compShape.addShape(rectPtr);
    compShape.addShape(trianglePtr);
    compShape.scale(2.0);
    BOOST_CHECK_CLOSE_FRACTION(compShape.getArea(), (circ.getArea() + rect.getArea() + 
      triangle.getArea()) * 2.0 * 2.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Argument_Scale_Test_CS)
  {
    std::shared_ptr<Shape> circPtr = std::make_shared<Circle>(Circle (5.0, {0.0, 0.0}));
    CompositeShape compShape(circPtr);
    BOOST_CHECK_THROW(compShape.scale(-1.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
