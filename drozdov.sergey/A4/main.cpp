#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace std;
using namespace drozdov;

int main()
{
  try
  {
    Rectangle testRect({ 8.0, 3.0, { 4.0, 4.0 } });
    Triangle testTrian({ 2.0, 3.0 }, { 4.0, 1.0 },{ 6.0, 5.0 });

    cout << "ROTATE SHAPES" << endl;

    cout << "\nRectangle center (x, y): " << testRect.getPos().x << ", " << testRect.getPos().y << endl;
    cout << "Rectangle width: " << testRect.getFrameRect().width << endl;
    cout << "Rectangle height: " << testRect.getFrameRect().height << endl;
    cout << "Rectangle angle: " << testRect.getAngle() << endl;

    testRect.rotate(90);

    cout << "ROTATE WITH ANGLE = 90" << endl;
    cout << "\nRectangle center (x, y): " << testRect.getPos().x << ", " << testRect.getPos().y << endl;
    cout << "Rectangle width: " << testRect.getFrameRect().width << endl;
    cout << "Rectangle height: " << testRect.getFrameRect().height << endl;
    cout << "Rectangle angle: " << testRect.getAngle() << endl;


    cout << "\nTriangle center (x,y): " << testTrian.getPos().x << ", " << testTrian.getPos().y << endl;
    cout << "Triangle frame rect: " << endl;
    cout << "Width: " << testTrian.getFrameRect().width << endl;
    cout << "Height: " << testTrian.getFrameRect().height << endl;

    testTrian.rotate(90);

    cout << "ROTATE WITH ANGLE = 90" << endl;
    cout << "\nTriangle center (x,y): " << testTrian.getPos().x << ", " << testTrian.getPos().y << endl;
    cout << "Triangle frame rect: " << endl;
    cout << "Width: " << testTrian.getFrameRect().width << endl;
    cout << "Height: " << testTrian.getFrameRect().height << endl;

    Rectangle rect({ 8.0, 3.0, { 4.0, 4.0 } });
    Circle circ(4.0, { 2.0, 3.0 });
    Triangle trian({ 1.0, 3.0 }, { 4.0, 1.0 }, { 6.0, 4.0 });

    cout << "\nROTATE COMPOSITION" << endl;
    shared_ptr<Shape> rectPtr = make_shared<Rectangle>(rect);
    shared_ptr<Shape> circPtr = make_shared<Circle>(circ);
    shared_ptr<Shape> trianPtr = make_shared<Triangle>(trian);
    CompositeShape compShape(rectPtr);
    compShape.addShape(circPtr);
    compShape.addShape(trianPtr);

    cout << "\nComposition size: " << compShape.getCount() << endl;
    cout << "Composition center (x,y): " << compShape.getFrameRect().pos.x << ", " << compShape.getFrameRect().pos.y << endl;
    cout << "Composition width: " << compShape.getFrameRect().width << endl;
    cout << "Composition height: " << compShape.getFrameRect().height << endl;

    compShape.rotate(90);

    cout << "ROTATE WITH ANGLE = 90" << endl;
    cout << "\nComposition size: " << compShape.getCount() << endl;
    cout << "Composition center (x,y): " << compShape.getFrameRect().pos.x << ", " << compShape.getFrameRect().pos.y << endl;
    cout << "Composition width: " << compShape.getFrameRect().width << endl;
    cout << "Composition height: " << compShape.getFrameRect().height << endl;

    Rectangle rect1({ 4.0, 1.0, { 2.0, 2.0 } });
    Rectangle rect2({ 2.0, 2.0, { 10.0, 3.0 } });
    Circle circ1(3.0, { 8.0, 6.0 });
    Circle circ2(2.0, { 7.0, 8.0 });
    Triangle trian1({ 1.0, 3.0 }, { 4.0, 1.0 }, { 6.0, 4.0 });

    Matrix testMatr;

    cout << "\nMATRIX" << endl;
    testMatr.add(&rect1);
    testMatr.printCurrentInfo();
    testMatr.add(&circ1);
    testMatr.printCurrentInfo();
    testMatr.add(&compShape);
    testMatr.add(&trian1);
    testMatr.printCurrentInfo();
    testMatr.add(&circ2);
    testMatr.add(&rect2);
    testMatr.printCurrentInfo();
  }
  catch(invalid_argument & error)
  {
    cerr << error.what() << endl;
    return 1;
  }
  return 0;
}
