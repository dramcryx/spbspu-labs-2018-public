#define BOOST_TEST_MODULE A4_Test
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double EPS = 0.001;

BOOST_AUTO_TEST_SUITE(TestMatrix)
  BOOST_AUTO_TEST_CASE(Lines)
  {
    drozdov::Rectangle testRec({ 2.0, 2.0, { 2.0, 2.0} });
    drozdov::Circle testCir1(2.0, { 5.0, 6.0 });
    drozdov::Circle testCir2(2.0, { 3.0, 4.0 });
    drozdov::Matrix testMatrix;
    testMatrix.add(&testRec);
    testMatrix.add(&testCir1);
    testMatrix.add(&testCir2);
    BOOST_CHECK_EQUAL(testMatrix[0][0]->getPos().x, 2.0);
    BOOST_CHECK_EQUAL(testMatrix[0][0]->getPos().y, 2.0);
    BOOST_CHECK_EQUAL(testMatrix[0][1]->getPos().x, 5.0);
    BOOST_CHECK_EQUAL(testMatrix[0][1]->getPos().y, 6.0);
    BOOST_CHECK_EQUAL(testMatrix[1][0]->getPos().x, 3.0);
    BOOST_CHECK_EQUAL(testMatrix[1][0]->getPos().y, 4.0);
  }
  BOOST_AUTO_TEST_CASE(InvalidIndex)
  {
    drozdov::Rectangle testRec({ 2.0, 2.0, { 2.0, 2.0} });
    drozdov::Circle testCir1(2.0, { 5.0, 6.0 });
    drozdov::Circle testCir2(2.0, { 3.0, 4.0 });
    drozdov::Matrix testMatrix;
    testMatrix.add(&testRec);
    testMatrix.add(&testCir1);
    testMatrix.add(&testCir2);
    BOOST_CHECK_THROW(testMatrix[3][0],std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(AddNullptr)
  {
    drozdov::Matrix testMatrix;
    BOOST_CHECK_THROW(testMatrix.add(nullptr),std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestCompositeShape)
  BOOST_AUTO_TEST_CASE(AreaAfterScale)
  {
    drozdov::Rectangle testRec1({ 5.0, 6.0, { 3.0, 4.0 } });
    drozdov::Circle testCir1(6.0, { 4.0 ,5.0 });
    drozdov::Triangle testTri1({ 4.0, 4.0 }, { 8.0, 7.0 }, { 6.0, 5.0 });
    std::shared_ptr<drozdov::Shape> rectPtr = std::make_shared<drozdov::Rectangle>(testRec1);
    std::shared_ptr<drozdov::Shape> circPtr = std::make_shared<drozdov::Circle>(testCir1);
    std::shared_ptr<drozdov::Shape> trianPtr = std::make_shared<drozdov::Triangle>(testTri1);
    drozdov::CompositeShape testCompShape(circPtr);
    testCompShape.addShape(rectPtr);
    testCompShape.addShape(trianPtr);
    double initialArea = testCompShape.getArea();
    testCompShape.scale(5.0);
    BOOST_CHECK_CLOSE(testCompShape.getArea(), 5.0 * 5.0 * initialArea, EPS);
  }
  BOOST_AUTO_TEST_CASE(Size)
  {
    drozdov::Rectangle testRec1({ 5.0, 6.0, { 3.0, 4.0 } });
    drozdov::Circle testCir1(6.0, { 4.0 ,5.0 });
    drozdov::Triangle testTri1({ 1.0, 2.0 }, { 8.0, 7.0 }, { 6.0, 5.0 });
    std::shared_ptr<drozdov::Shape> rectPtr = std::make_shared<drozdov::Rectangle>(testRec1);
    std::shared_ptr<drozdov::Shape> circPtr = std::make_shared<drozdov::Circle>(testCir1);
    std::shared_ptr<drozdov::Shape> trianPtr = std::make_shared<drozdov::Triangle>(testTri1);
    drozdov::CompositeShape testCompShape(circPtr);
    testCompShape.addShape(rectPtr);
    testCompShape.addShape(trianPtr);
    BOOST_CHECK_EQUAL(testCompShape.getCount(), 3);
  }
  BOOST_AUTO_TEST_CASE(AddNullptr)
  {
    drozdov::CompositeShape testCompShape;
    BOOST_CHECK_THROW(testCompShape.addShape(nullptr), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(RemoveWithInvalidIndex)
  {
    drozdov::Rectangle testRec1({ 5.0, 6.0, { 3.0, 4.0 } });
    std::shared_ptr<drozdov::Shape> rectPtr = std::make_shared<drozdov::Rectangle>(testRec1);
    drozdov::CompositeShape testCompShape(rectPtr);
    BOOST_CHECK_THROW(testCompShape.removeShape(4), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(InvalidParametersInScale)
  {
    drozdov::Rectangle testRec1({ 5.0, 6.0, { 3.0, 4.0 } });
    std::shared_ptr<drozdov::Shape> rectPtr = std::make_shared<drozdov::Rectangle>(testRec1);
    drozdov::CompositeShape testCompShape(rectPtr);
    BOOST_CHECK_THROW(testCompShape.scale(-2.0), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(AreaAndCenterAfterRotate)
  {
    drozdov::Circle testCir1(6.0, { 4.0 ,5.0 });
    std::shared_ptr<drozdov::Shape> circPtr = std::make_shared<drozdov::Circle>(testCir1);
    drozdov::CompositeShape testCompShape(circPtr);
    double initialArea = testCompShape.getArea();
    drozdov::point_t initialCenter = testCompShape.getPos();
    testCompShape.rotate(30);
    BOOST_CHECK_CLOSE(testCompShape.getArea(),initialArea,EPS);
    BOOST_CHECK_CLOSE(testCompShape.getPos().x,initialCenter.x,EPS);
    BOOST_CHECK_CLOSE(testCompShape.getPos().y,initialCenter.y,EPS);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestCircle)
  BOOST_AUTO_TEST_CASE(AreaAndCenterAfterRotate)
  {
    drozdov::Circle testCir(5.0, { 3.0, 4.0 });
    double initialArea = testCir.getArea();
    drozdov::point_t initialCenter = testCir.getPos();
    testCir.rotate(30);
    BOOST_CHECK_CLOSE(testCir.getArea(), initialArea, EPS);
    BOOST_CHECK_CLOSE(testCir.getPos().x, initialCenter.x, EPS);
    BOOST_CHECK_CLOSE(testCir.getPos().y, initialCenter.y, EPS);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestRectangle)
  BOOST_AUTO_TEST_CASE(AreaAndCenterAfterRotate)
  {
    drozdov::Rectangle testRec({ 5.0, 6.0, { 3.0, 4.0 } });
    double initialArea = testRec.getArea();
    drozdov::point_t initialCenter = testRec.getPos();
    testRec.rotate(30);
    BOOST_CHECK_CLOSE(testRec.getArea(), initialArea, EPS);
    BOOST_CHECK_CLOSE(testRec.getPos().x, initialCenter.x, EPS);
    BOOST_CHECK_CLOSE(testRec.getPos().y, initialCenter.y, EPS);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestTriangle)
  BOOST_AUTO_TEST_CASE(AreaAndCenterAfterRotate)
  {
    drozdov::Triangle testTri({ 1.0, 3.0 }, { 4.0, 1.0 }, { 6.0, 4.0 });
    double initialArea = testTri.getArea();
    drozdov::point_t initialCenter = testTri.getPos();
    testTri.rotate(30);
    BOOST_CHECK_CLOSE(testTri.getArea(), initialArea, EPS);
    BOOST_CHECK_CLOSE(testTri.getPos().x, initialCenter.x, EPS);
    BOOST_CHECK_CLOSE(testTri.getPos().y, initialCenter.y, EPS);
  }
BOOST_AUTO_TEST_SUITE_END()
