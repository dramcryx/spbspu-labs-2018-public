#include "triangle.hpp"
#include <stdexcept>
#include <cmath>
#include <algorithm>

using namespace drozdov;

drozdov::Triangle::Triangle(const point_t & point1, const point_t & point2, const point_t & point3):
point1_(point1),
point2_(point2),
point3_(point3),
centre_({ ((point1.x + point2.x + point3.x) / 3.0),
  ((point1.y + point2.y + point3.y) / 3.0) })
{
  double AB = sqrt(pow(point2.x - point1.x, 2) + pow(point2.y - point1.y, 2));
  double BC = sqrt(pow(point3.x - point2.x, 2) + pow(point3.y - point2.y, 2));
  double AC = sqrt(pow(point3.x - point1.x, 2) + pow(point3.y - point1.y, 2));

  if ((AB + BC) == AC)
  {
    throw std::invalid_argument("(AB + BC) mustn't be = AC");
  }
  else if ((AC + BC) == AB)
  {
    throw std::invalid_argument("(AC + BC) mustn't be = AB");
  }
  else if ((AB + AC) == BC)
  {
    throw std::invalid_argument("(AB + AC) mustn't be = BC");
  }
}

double drozdov::Triangle::getArea() const noexcept
{
  return (std::abs((((point1_.x - point3_.x) * (point2_.y - point3_.y))
    - ((point1_.y - point3_.y) * (point2_.x - point3_.x))) / 2.0));
}

point_t drozdov::Triangle::getPos() noexcept
{
  return centre_;
}

rectangle_t drozdov::Triangle::getFrameRect() const noexcept
{
  double width = std::max(std::max(point1_.x, point2_.x), point3_.x)
    - std::min(std::min(point1_.x, point2_.x), point3_.x);
  double height = std::max(std::max(point1_.y, point2_.y), point3_.y)
    - std::min(std::min(point1_.y, point2_.y), point3_.y);

  double shiftX = width / 2.0 - (centre_.x - std::min(std::min(point1_.x, point2_.x), point3_.x));
  double shiftY = height / 2.0 - (centre_.y - std::min(std::min(point1_.y, point2_.y), point3_.y));

  return rectangle_t({width, height, {centre_.x + shiftX, centre_.y + shiftY}});
}

void drozdov::Triangle::move(const point_t & point) noexcept
{
  double dx = point.x - centre_.x;
  double dy = point.y - centre_.y;

  point1_.x += dx;
  point1_.y += dy;
  point2_.x += dx;
  point2_.y += dy;
  point3_.x += dx;
  point3_.y += dy;

  centre_ = point;
}

void drozdov::Triangle::move(const double dx, const double dy) noexcept
{
  point1_.x += dx;
  point1_.y += dy;
  point2_.x += dx;
  point2_.y += dy;
  point3_.x += dx;
  point3_.y += dy;

  centre_.x += dx;
  centre_.y += dy;
}

void drozdov::Triangle::scale(const double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("factor must be > 0");
  }

  point1_.x = centre_.x + ((point1_.x - centre_.x) * factor);
  point1_.y = centre_.y + ((point1_.y - centre_.y) * factor);
  point2_.x = centre_.x + ((point2_.x - centre_.x) * factor);
  point2_.y = centre_.y + ((point2_.y - centre_.y) * factor);
  point3_.x = centre_.x + ((point3_.x - centre_.x) * factor);
  point3_.y = centre_.y + ((point3_.y - centre_.y) * factor);
}

void drozdov::Triangle::rotate(const double angle)
{
  double tmpAngle = (angle * M_PI) / 180.0;
  point_t newPoint1;
  point_t newPoint2;
  point_t newPoint3;
  newPoint1.x = centre_.x + (point1_.x - centre_.x) * cos(tmpAngle) - (point1_.y - centre_.y) * sin(tmpAngle);
  newPoint1.y = centre_.y + (point1_.x - centre_.x) * sin(tmpAngle) + (point1_.y - centre_.y) * cos(tmpAngle);
  point1_ = newPoint1;
  newPoint2.x = centre_.x + (point2_.x - centre_.x) * cos(tmpAngle) - (point2_.y - centre_.y) * sin(tmpAngle);
  newPoint2.y = centre_.y + (point2_.x - centre_.x) * sin(tmpAngle) + (point2_.y - centre_.y) * cos(tmpAngle);
  point2_ = newPoint2;
  newPoint3.x = centre_.x + (point3_.x - centre_.x) * cos(tmpAngle) - (point3_.y - centre_.y) * sin(tmpAngle);
  newPoint3.y = centre_.y + (point3_.x - centre_.x) * sin(tmpAngle) + (point3_.y - centre_.y) * cos(tmpAngle);
  point3_ = newPoint3;
}
