#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"



void testOfShape(dudina::Shape & cObject)
{
  std::cout << "Shape test:" << std::endl;
  cObject.printInformation();
  std::cout << "Area of figure:" << cObject.getArea() << std::endl;
  std::cout << "Rectangle frame:" << std::endl;
  std::cout << " Center: {" << cObject.getFrameRect().pos.x << ";" << cObject.getFrameRect().pos.y << "}" << std::endl;
  std::cout << " Width:" << cObject.getFrameRect().width << std::endl;
  std::cout << " Height:" << cObject.getFrameRect().height << std::endl;

  cObject.move({ 5,2 });
  std::cout << "After move to {5,2}:" << std::endl;
  std::cout << "Rectangle frame:" << std::endl;
  std::cout << " Center: {" << cObject.getFrameRect().pos.x << ";" << cObject.getFrameRect().pos.y << "}" << std::endl;
  std::cout << " Width:" << cObject.getFrameRect().width << std::endl;
  std::cout << " Height:" << cObject.getFrameRect().height << std::endl;

  cObject.move(5, 2);
  std::cout << "After move on x=5 and y=2:" << std::endl;
  std::cout << "Rectangle frame:" << std::endl;
  std::cout << " Center: {" << cObject.getFrameRect().pos.x << ";" << cObject.getFrameRect().pos.y << "}" << std::endl;
  std::cout << " Width:" << cObject.getFrameRect().width << std::endl;
  std::cout << " Height:" << cObject.getFrameRect().height << std::endl;

  cObject.scale(2);
  std::cout << "After scale:" << std::endl;
  std::cout << "Area after scale:" << cObject.getArea() << std::endl;
  std::cout << "Rectangle frame:" << std::endl;
  std::cout << " Center: {" << cObject.getFrameRect().pos.x << ";" << cObject.getFrameRect().pos.y << "}" << std::endl;
  std::cout << " Width:" << cObject.getFrameRect().width << std::endl;
  std::cout << " Height:" << cObject.getFrameRect().height << std::endl;


};


int main()
{
  try
  {
    dudina::Rectangle rect({ 2.0, 3.0 }, 4, 5);
    dudina::Circle circl({ 2.0, 3.0 }, 4);
    dudina::Triangle triangl({ 0.0, 0.0 }, { 2.0, 4.0 }, { 4.0, 0.0 });

    std::cout << "Rectangle" << std::endl;
    testOfShape(rect);
    std::cout << "Circle" << std::endl;
    testOfShape(circl);
    std::cout << "Triangle" << std::endl;
    testOfShape(triangl);

    dudina::Rectangle rectan({ 1.0, 2.0 }, 5, 2);
    dudina::Circle circ({ 4.0, 0.0 }, 3);
    dudina::Triangle trian({ 0.0, 0.0 }, { 4.0, 8.0 }, { 8.0, 0.0 });

    std::cout << "Composite Shape" << std::endl;
    std::shared_ptr<dudina::Shape> rectPtr = std::make_shared<dudina::Rectangle>(rectan);
    dudina::CompositeShape List(rectPtr);
    List.printInformation();

    std::shared_ptr<dudina::Shape> cirPtr = std::make_shared<dudina::Circle>(circ);
    List.addShape(cirPtr);
    List.printInformation();

    List.deleteShape(2);
    List.printInformation();

    List.move(5, 2);
    List.printInformation();

    List.move({ 5, 2 });
    List.printInformation();

    List.scale(2.0);
    List.printInformation();

  }
  catch (const std::invalid_argument & e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }

  return 0;
}

