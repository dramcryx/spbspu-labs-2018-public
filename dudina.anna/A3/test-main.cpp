#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>

#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"



BOOST_AUTO_TEST_SUITE(COMPOSITESHAPE_TESTS)
 const double accuracy = 0.0001;

  BOOST_AUTO_TEST_CASE(InvalidArgumentOfConstructor)
  {
    std::shared_ptr<dudina::Shape> circlePtr = nullptr;

    BOOST_CHECK_THROW(dudina::CompositeShape compShape(circlePtr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(TEST_SIZES_AFTER_MOVE_TO_POINT)
  {
    dudina::Circle circle({ 1.0, 10.0 }, 5.0);
    dudina::Rectangle rectangle({ -2.0, 3.0 }, 12.0, 8.0);
    std::shared_ptr <dudina::Shape> circlePtr = std::make_shared <dudina::Circle>(circle);
    std::shared_ptr <dudina::Shape> rectanglePtr = std::make_shared <dudina::Rectangle>(rectangle);
    dudina::CompositeShape compShape(circlePtr);
    compShape.addShape(rectanglePtr);

    double composShapeWidthBeforeMove = compShape.getFrameRect().width;
    double composShapeHeightBeforeMove = compShape.getFrameRect().height;
    double composShapeAreaBeforeMove = compShape.getArea();

    compShape.move({ 5.0, 5.0 });

    BOOST_CHECK_CLOSE(composShapeWidthBeforeMove, compShape.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(composShapeHeightBeforeMove, compShape.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(composShapeAreaBeforeMove, compShape.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(TEST_SIZES_AFTER_MOVE_BY_dx_and_dy)
  {
    dudina::Circle circle({ 1.0, 10.0 }, 5.0);
    dudina::Rectangle rectangle({ -2.0, 3.0 }, 12.0, 8.0);
    std::shared_ptr <dudina::Shape> circlePtr = std::make_shared <dudina::Circle>(circle);
    std::shared_ptr <dudina::Shape> rectanglePtr = std::make_shared <dudina::Rectangle>(rectangle);
    dudina::CompositeShape compShape(circlePtr);
    compShape.addShape(rectanglePtr);

    double composShapeWidthBeforeMove = compShape.getFrameRect().width;
    double composShapeHeightBeforeMove = compShape.getFrameRect().height;
    double composShapeAreaBeforeMove = compShape.getArea();

    compShape.move(5.0, 5.0);

    BOOST_CHECK_CLOSE(composShapeWidthBeforeMove, compShape.getFrameRect().width, accuracy);
    BOOST_CHECK_CLOSE(composShapeHeightBeforeMove, compShape.getFrameRect().height, accuracy);
    BOOST_CHECK_CLOSE(composShapeAreaBeforeMove, compShape.getArea(), accuracy);
  }

  BOOST_AUTO_TEST_CASE(TEST_SCALE)
  {
    dudina::Circle circle({ 1.0, 1.0 }, 5.0);
    dudina::Rectangle rectangle({ 5.0, 3.0 }, 2.0, 2.0);
    std::shared_ptr <dudina::Shape> circlePtr = std::make_shared <dudina::Circle>(circle);
    std::shared_ptr <dudina::Shape> rectanglePtr = std::make_shared <dudina::Rectangle>(rectangle);
    dudina::CompositeShape compShape(circlePtr);
    compShape.addShape(rectanglePtr);

    dudina::point_t CenterBeforeSacling = compShape.getFrameRect().pos;
    double composShapeAreaBeforeScale = compShape.getArea();
    double coeffic = 2.0;

    compShape.scale(coeffic);

    BOOST_CHECK_CLOSE(coeffic * coeffic * composShapeAreaBeforeScale, compShape.getArea(), accuracy);
    BOOST_CHECK_CLOSE(CenterBeforeSacling.x, compShape.getFrameRect().pos.x, accuracy);
    BOOST_CHECK_CLOSE(CenterBeforeSacling.y, compShape.getFrameRect().pos.y, accuracy);
    BOOST_CHECK_THROW(compShape.scale(-3.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(REMOVE_TEST)
  {
    dudina::Circle circle({ 3.0, 5.0 }, 5.0);
    dudina::Rectangle rectangle({ -6.0, -3.0 }, 12.0, 8.0);
    std::shared_ptr <dudina::Shape> circlePtr = std::make_shared <dudina::Circle>(circle);
    std::shared_ptr <dudina::Shape> rectanglePtr = std::make_shared <dudina::Rectangle>(rectangle);
    dudina::CompositeShape compShape(rectanglePtr);
    compShape.addShape(circlePtr);

    compShape.deleteShape(1);

    BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.x, rectangle.getFrameRect().pos.x, accuracy);
    BOOST_CHECK_CLOSE(compShape.getFrameRect().pos.y, rectangle.getFrameRect().pos.y, accuracy);
    BOOST_CHECK_CLOSE(compShape.getArea(), rectangle.getArea(), accuracy);
    BOOST_CHECK_EQUAL(compShape.getAmount(), 1);
  }



  BOOST_AUTO_TEST_CASE(INVALID_ARGUMENT_OF_ELEMENT)
  {
    dudina::Rectangle rectangle({ -2.0, 2.0 }, 13.0, 4.0);
    std::shared_ptr <dudina::Shape> circlePtr = nullptr;
    std::shared_ptr <dudina::Shape> rectanglePtr = std::make_shared <dudina::Rectangle>(rectangle);
    dudina::CompositeShape compShape(rectanglePtr);

    BOOST_CHECK_THROW(compShape.addShape(circlePtr), std::invalid_argument);
  }


  BOOST_AUTO_TEST_CASE(OUT_OF_RANGE_TEST)
  {
    dudina::Circle circle({ 7.0, 0.0 }, 5.0);
    std::shared_ptr <dudina::Shape> circlePtr = std::make_shared <dudina::Circle>(circle);
    dudina::CompositeShape compShape(circlePtr);

    BOOST_CHECK_THROW(compShape.deleteShape(2), std::out_of_range);
    BOOST_CHECK_THROW(compShape.deleteShape(-1), std::out_of_range);
  }


BOOST_AUTO_TEST_SUITE_END()

