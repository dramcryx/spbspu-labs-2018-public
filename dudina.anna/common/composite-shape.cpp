#include "composite-shape.hpp"
#include "shape.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>


dudina::CompositeShape::CompositeShape(const std::shared_ptr<Shape> & shape):
  shapeList_(new std::shared_ptr<Shape>[1]),
  shapeAmount_(0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Empty shape");
  }

  shapeList_[0] = shape;
  shapeAmount_++;

}

dudina::CompositeShape::CompositeShape(const CompositeShape & composShape) :
  shapeList_(new std::shared_ptr<Shape>[composShape.shapeAmount_]),
  shapeAmount_(composShape.shapeAmount_)
{
  for (int i = 0; i < shapeAmount_; i++)
  {
    shapeList_[i] = composShape.shapeList_[i];
  }
}

dudina::CompositeShape::CompositeShape(CompositeShape && composShape) :
  shapeAmount_(composShape.shapeAmount_)
{
  shapeList_ = std::move(composShape.shapeList_);
  composShape.shapeList_ = nullptr;
  composShape.shapeAmount_ = 0;
}

dudina::CompositeShape & dudina::CompositeShape::operator=(const CompositeShape & composShape)
{
  if (this != &composShape)
  {
    shapeAmount_ = composShape.shapeAmount_;
    shapeList_.reset(new std::shared_ptr<Shape>[shapeAmount_]);

    for (int i = 0; i < shapeAmount_; i++)
    {
      shapeList_[i] = composShape.shapeList_[i];
    }
  }

  return *this;
}

dudina::CompositeShape & dudina::CompositeShape::operator=(CompositeShape && composShape)
{
  if (this != &composShape)
  {
    shapeAmount_ = composShape.shapeAmount_;
    shapeList_.reset(new std::shared_ptr<Shape>[shapeAmount_]);
    shapeList_ = std::move(composShape.shapeList_);

    composShape.shapeList_ = nullptr;
    composShape.shapeAmount_ = 0;
  }

  return *this;
}

std::shared_ptr<dudina::Shape> & dudina::CompositeShape::operator [](size_t index)
{
  if (shapeAmount_ == 0)
  {
    throw std::out_of_range("Empty CompositeShape! ");
  }

  return shapeList_[index];
}


double dudina::CompositeShape::getArea() const
{
  double area = 0.0;

  for (int i = 0; i < shapeAmount_; i++)
  {
    area += shapeList_[i] -> getArea();// i didn't take into account the imposition of figures
  }

  return area;
}

dudina::rectangle_t dudina::CompositeShape::getFrameRect() const
{
  if (shapeAmount_ == 0)
  {
    return { 0.0, 0.0, { 0.0, 0.0 } };
  }
  else
  {
    rectangle_t rectFrame = shapeList_[0] -> getFrameRect();

    double maxX = rectFrame.pos.x + rectFrame.width / 2;
    double minX = rectFrame.pos.x - rectFrame.width / 2;
    double maxY = rectFrame.pos.y + rectFrame.height / 2;
    double minY = rectFrame.pos.y - rectFrame.height / 2;


    for (int i = 0; i < shapeAmount_; i++)
    {
      rectFrame = shapeList_[i] -> getFrameRect();
      if ((rectFrame.pos.x + rectFrame.width / 2) > maxX)
      {
        maxX = rectFrame.pos.x + rectFrame.width / 2;
      }
      if ((rectFrame.pos.x - rectFrame.width / 2) < minX)
      {
        minX = rectFrame.pos.x - rectFrame.width / 2;
      }
      if ((rectFrame.pos.y + rectFrame.height / 2) > maxY)
      {
        maxY = rectFrame.pos.y + rectFrame.height / 2;
      }
      if ((rectFrame.pos.y - rectFrame.height / 2) < minY)
      {
        minY = rectFrame.pos.y - rectFrame.height / 2;
      }
    }

    return {  maxX - minX, maxY - minY, { ((maxX + minX) / 2), ((maxY + minY) / 2) } };
  }
}

void dudina::CompositeShape::move(const point_t & pos) noexcept
{
  move(pos.x - getFrameRect().pos.x, pos.y - getFrameRect().pos.y);
}

void dudina::CompositeShape::move(const double dx, const double dy) noexcept
{
  for (int i = 0; i < shapeAmount_; i++)
  {
    shapeList_[i] -> move(dx, dy);
  }
}

void dudina::CompositeShape::scale(const double coeff)
{
  if (coeff < 0.0)
  {
    throw std::invalid_argument("Scale coefficient must be > 0");
  }

  const dudina::point_t centerCompos = getFrameRect().pos;
  for (int i = 0; i < shapeAmount_; i++)
  {
    double dx = shapeList_[i]->getFrameRect().pos.x - centerCompos.x;
    double dy = shapeList_[i]->getFrameRect().pos.y - centerCompos.y;

    dudina::point_t shapePos;
    shapePos.x = centerCompos.x + coeff * dx;
    shapePos.y = centerCompos.y + coeff * dy;

    shapeList_[i]->move(shapePos);
    shapeList_[i]->scale(coeff);

  }
}

int dudina::CompositeShape::getAmount() const
{
  return shapeAmount_;
}

void dudina::CompositeShape::addShape (std::shared_ptr<Shape> & shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Empty shape!");
  }


  std::unique_ptr< std::shared_ptr< Shape >[] > newAr (new std::shared_ptr< Shape >[shapeAmount_ + 1]);
  for (int i = 0; i < shapeAmount_; i++)
  {
    newAr[i] = shapeList_[i];
  }

  newAr[shapeAmount_] = shape;
  shapeAmount_++;
  newAr.swap(shapeList_);
}

void dudina::CompositeShape::deleteShape(const int index)
{
  if (shapeAmount_ == 0)
  {
    throw std::out_of_range(" The list is empty");
  }

  if ((index <= 0) || (index >= shapeAmount_))
  {
    throw std::out_of_range(" Shape out of range!");
  }

  if (shapeAmount_ == 1)
  {
    shapeList_.reset();
    shapeAmount_ = 0;

    return;
  }

  std::unique_ptr <std::shared_ptr <Shape>[]> newAr(new std::shared_ptr <Shape>[shapeAmount_ - 1]);
  for (int i = 0; i < index; i++)
  {
    newAr[i] = shapeList_[i];
  }
  for (int i = index; i < shapeAmount_ - 1; i++)
  {
    newAr[i] = shapeList_[i + 1];
  }

  newAr.swap(shapeList_);
  shapeAmount_--;
}

void dudina::CompositeShape::rotate(const double degrees)
{
  double angleCos = cos(degrees * M_PI / 180);
  double angleSin = sin(degrees * M_PI / 180);

  point_t center = getFrameRect().pos;

  for (int i = 0; i < shapeAmount_; i++)
  {
    point_t centerOfShape = shapeList_[i]->getFrameRect().pos;

    shapeList_[i]->move({ center.x + angleCos * (centerOfShape.x - center.x) - angleSin * (centerOfShape.y - center.y),
      center.y + angleCos * (centerOfShape.y - center.y) + angleSin * (centerOfShape.x - center.x) });
    shapeList_[i]->rotate(degrees);
  }
}

std::string dudina::CompositeShape::getName() const 
{
  return "CS";
}


void dudina::CompositeShape::printInformation() 
{
  std::cout << "CompositeShape is composed of" << shapeAmount_ << "shapes" << std::endl;
  std::cout << "Area of CompositeShape:" << getArea() << std::endl;
  std::cout << "Rectangle Frame:" << std::endl;
  std::cout << "Height:" << getFrameRect().height << std::endl;
  std::cout << "Width:" << getFrameRect().width << std::endl;
  std::cout << "Position of center: {" << getFrameRect().pos.x << ";" << getFrameRect().pos.y << "}" << std::endl;

  for (int i = 0; i < shapeAmount_; i++)
  {
    std::cout << "Shape #" << i << ":" << std::endl;
    shapeList_[i] -> printInformation();
  }

}

size_t dudina::CompositeShape::getSize() const noexcept
{
  return shapeAmount_;
}



