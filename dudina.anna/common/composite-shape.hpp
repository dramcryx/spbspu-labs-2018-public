#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace dudina
{
  class CompositeShape :
    public Shape
  {
  public:
    CompositeShape(const std::shared_ptr<Shape> & shape);
    CompositeShape(const CompositeShape & composShape);
    CompositeShape(CompositeShape && composShape);

    CompositeShape & operator=(const CompositeShape & composShape);
    CompositeShape & operator=(CompositeShape && composShape);
    std::shared_ptr<Shape> & operator[] (size_t index);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double coeff) override;
    void rotate(const double degrees) override;
    virtual std::string getName() const;
    int getAmount() const;

    void addShape(std::shared_ptr<Shape> & shape);
    void deleteShape(const int index);
    void printInformation() override;
    size_t getSize() const noexcept;

  

  private:
    std::unique_ptr <std::shared_ptr <Shape>[]> shapeList_;
    int shapeAmount_;
  };
}

#endif
