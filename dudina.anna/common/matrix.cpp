#include"matrix.hpp"
#include"shape.hpp"

#include <stdexcept>
#include <iostream>
#include <new>
#include <algorithm>
#include <iomanip>
#include <string>


dudina::Matrix::Matrix(const std::shared_ptr<Shape> & shape) :
  shapes_(nullptr),
  layers_(0),
  sizeOfLayer_(0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Empty shape");
  }

  std::unique_ptr<std::shared_ptr<Shape>[]> element(new std::shared_ptr<Shape>[(layers_ + 1) * (sizeOfLayer_ + 1)]);
  element[0] = shape;
  shapes_ = std::move(element);
  sizeOfLayer_++;
  layers_++;
}

dudina::Matrix::Matrix(const Matrix & matrix) :
  shapes_(new std::shared_ptr<Shape>[matrix.layers_ * matrix.sizeOfLayer_]),
  layers_(matrix.layers_),
  sizeOfLayer_(matrix.sizeOfLayer_)
{
  for (size_t i = 0; i<layers_*sizeOfLayer_; ++i)
  {
    shapes_[i] = matrix.shapes_[i];
  }
}

dudina::Matrix::Matrix(Matrix && matrix) :
  shapes_(std::move(matrix.shapes_)),
  layers_(matrix.layers_),
  sizeOfLayer_(matrix.sizeOfLayer_)
{
  matrix.shapes_ = nullptr;
  matrix.layers_ = 0;
  matrix.sizeOfLayer_ = 0;
}

dudina::Matrix::~Matrix()
{
  shapes_ = nullptr;
  layers_ = 0;
  sizeOfLayer_ = 0;
}

dudina::Matrix & dudina::Matrix::operator= (const dudina::Matrix & matrix)
{
  if (this == &matrix)
  {
    return *this;
  }
  shapes_.reset(new std::shared_ptr<dudina::Shape>[matrix.sizeOfLayer_*matrix.layers_]);
  layers_ = matrix.layers_;
  sizeOfLayer_ = matrix.sizeOfLayer_;
  for (size_t i = 0; i<layers_*sizeOfLayer_; ++i)
  {
    shapes_[i] = matrix.shapes_[i];
  }
  return *this;
}

dudina::Matrix & dudina::Matrix::operator= (dudina::Matrix && matrix)
{
  if (this == &matrix)
  {
    return *this;
  }
  shapes_.reset(new std::shared_ptr<dudina::Shape>[matrix.layers_* matrix.sizeOfLayer_]);
  sizeOfLayer_ = matrix.sizeOfLayer_;
  layers_ = matrix.layers_;
  shapes_ = std::move(matrix.shapes_);
  matrix.shapes_ = nullptr;
  matrix.layers_ = 0;
  matrix.sizeOfLayer_ = 0;
  return *this;
}

std::unique_ptr<std::shared_ptr<dudina::Shape>[]> dudina::Matrix::operator[](const size_t index) const
{
  if (layers_ == 0)
  {
    throw std::out_of_range("Matrix is empty");
  }
  if (index > layers_ - 1)
  {
    throw std::invalid_argument("Invalid index");
  }
  std::unique_ptr<std::shared_ptr<dudina::Shape>[]>
    layer(new std::shared_ptr<dudina::Shape>[sizeOfLayer_]);
  for (size_t i = 0; i < sizeOfLayer_; ++i) {
    layer[i] = shapes_[index * sizeOfLayer_ + i];
  }
  return layer;
}

bool dudina::Matrix::checkInsert(const std::shared_ptr<Shape> & shape1, const std::shared_ptr<Shape> & shape2) const
{
  if ((shape1 == nullptr) || (shape2 == nullptr))
  {
    return false;
  }
  dudina::rectangle_t shapeFrameRect1 = shape1->getFrameRect();
  dudina::rectangle_t shapeFrameRect2 = shape2->getFrameRect();

  bool checkX = (abs(shapeFrameRect1.pos.x - shapeFrameRect2.pos.x)
    < (shapeFrameRect1.width / 2 + shapeFrameRect2.width / 2));
  bool checkY = (abs(shapeFrameRect1.pos.y - shapeFrameRect2.pos.y)
    < (shapeFrameRect1.height / 2 + shapeFrameRect2.height / 2));

  return ((checkX) && (checkY));
}



void dudina::Matrix::addShape(const std::shared_ptr< dudina::Shape > & shape) noexcept
{
  bool addedShape = false;
  for (size_t i = 0; !addedShape; ++i)
  {
    for (size_t j = 0; j < sizeOfLayer_; ++j)
    {
      if (!shapes_[i * sizeOfLayer_ + j])
      {
        shapes_[i * sizeOfLayer_ + j] = shape;
        addedShape = true;
        break;
      }
      else
      {
        if (checkInsert(shapes_[i * sizeOfLayer_ + j], shape))
        {
          break;
        }
      }

      if (j == (sizeOfLayer_ - 1))
      {
        sizeOfLayer_++;
        std::unique_ptr< std::shared_ptr< dudina::Shape >[] > elements(
          new std::shared_ptr< dudina::Shape >[layers_ * sizeOfLayer_]);
        for (size_t n = 0; n < layers_; ++n)
        {
          for (size_t m = 0; m < sizeOfLayer_ - 1; ++m)
          {
            elements[n * sizeOfLayer_ + m] = shapes_[n * (sizeOfLayer_ - 1) + m];
          }
          elements[(n + 1) * sizeOfLayer_ - 1] = nullptr;
        }
        elements[(i + 1) * sizeOfLayer_ - 1] = shape;
        shapes_.swap(elements);
        addedShape = true;
        break;
      }
    }
    if ((i == (layers_ - 1)) && !addedShape)
    {
      layers_++;
      std::unique_ptr< std::shared_ptr< dudina::Shape >[] > elements(
        new std::shared_ptr< dudina::Shape >[layers_ * sizeOfLayer_]);
      for (size_t n = 0; n < ((layers_ - 1) * sizeOfLayer_); ++n)
      {
        elements[n] = shapes_[n];
      }
      for (size_t n = ((layers_ - 1) * sizeOfLayer_); n < (layers_ * sizeOfLayer_); ++n)
      {
        elements[n] = nullptr;
      }
      elements[(layers_ - 1) * sizeOfLayer_] = shape;
      shapes_.swap(elements);
      addedShape = true;
    }
  }
}

size_t dudina::Matrix::getLayerNumber() const noexcept
{
  return layers_;
}

size_t dudina::Matrix::getLayerSize() const noexcept
{
  return sizeOfLayer_;
}

void dudina::Matrix::printInformation()
{
  if (layers_ == 0 && sizeOfLayer_ == 0)
  {
    std::cout << "Matrix is empty" << std::endl;
  }

  for (size_t i = 0; i < layers_; i++)
  {
    for (size_t j = 0; j < sizeOfLayer_; j++)
    {
      if (shapes_[i * sizeOfLayer_ + j] != nullptr)
      {
        std::cout << std::setw(3) << std::left << shapes_[i * sizeOfLayer_ + j]->getName();
      }
    }
    std::cout << std::endl;
  }
}

void dudina::Matrix::addComposShape(const std::shared_ptr<dudina::CompositeShape> & compos, size_t size) 
{
  for (size_t i = 0; i < size; ++i)
  {
    this->addShape((*compos)[i]);
  }
}




