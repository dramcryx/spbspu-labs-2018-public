#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include "composite-shape.hpp"

#include <memory>

namespace dudina
{
  class Matrix
  {
  public:

    Matrix(const std::shared_ptr<Shape> &shape);
    Matrix(const Matrix & matrix);
    Matrix(Matrix && matrix);

    ~Matrix();

    Matrix & operator=(const Matrix & matrix);
    Matrix & operator=(Matrix && matrix);
    std::unique_ptr<std::shared_ptr<Shape>[]> operator[](const size_t index) const;

    void addShape(const std::shared_ptr<Shape> &shape) noexcept;
    void addComposShape(const std::shared_ptr<CompositeShape> & compos, size_t size) ;
    size_t getLayerNumber() const noexcept;
    size_t getLayerSize() const noexcept;
    void printInformation();

  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> shapes_;
    size_t layers_;
    size_t sizeOfLayer_;

    bool checkInsert(const std::shared_ptr<Shape> & shape1, const std::shared_ptr<Shape> & shape2) const;
  };
}

#endif
