#include "rectangle.hpp"
#include <cmath>
#include <iostream>


dudina::Rectangle::Rectangle(const point_t & pos, const double width, const double height) :
  pos_(pos),
  width_(width),
  height_(height)
{
  if (width_ < 0.0)
  {
    throw std::invalid_argument("Width must be > 0");
  }
  if (height_ < 0.0)
  {
    throw std::invalid_argument("Height must be > 0");
  }
  vertex_[0] = { pos_.x + width_ / 2, pos_.y + height_ / 2 }; //right top
  vertex_[1] = { pos_.x - width_ / 2, pos_.y + height_ / 2 }; //left top
  vertex_[2] = { pos_.x - width_ / 2, pos_.y - height_ / 2 }; //left bottom
  vertex_[3] = { pos_.x + width_ / 2, pos_.y - height_ / 2 }; //right bottom

}



double dudina::Rectangle::getArea() const 
{
  return width_ * height_;
}

dudina::rectangle_t dudina::Rectangle::getFrameRect() const 
{

  double right = vertex_[0].x;
  double left = vertex_[0].x;
  double top = vertex_[0].y;
  double bottom = vertex_[0].y;
  for (int i = 1; i < 4; i++)
  {
    if (vertex_[i].x > right)
    {
      right = vertex_[i].x;
    }
    if (vertex_[i].x < left)
    {
      left = vertex_[i].x;
    }
    if (vertex_[i].y > top)
    {
      top = vertex_[i].y;
    }
    if (vertex_[i].y < bottom)
    {
      bottom = vertex_[i].y;
    }
  }
  return {(top - bottom), (right - left), { ((right + left) / 2), ((top + bottom) / 2) } };

}

void dudina::Rectangle::move(const point_t & pos) 
{
  for (int i = 0; i < 4; i++)
  {
    vertex_[i].x += pos.x - pos_.x;
    vertex_[i].y += pos.y - pos_.y;
  }

  pos_= pos;
}

void dudina::Rectangle::move(double dx, double dy) 
{
  pos_.x += dx;
  pos_.y += dy;

  for (int i = 0; i < 4; i++)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }

}

void dudina::Rectangle::scale(const double coeff)
{
  if (coeff < 0.0)
  {
    throw std::invalid_argument("Scale coefficient must be > 0");
  }

  width_ *= coeff;
  height_ *= coeff;

  vertex_[0] = { pos_.x + width_ / 2, pos_.y + height_ / 2 };
  vertex_[1] = { pos_.x - width_ / 2, pos_.y + height_ / 2 };
  vertex_[2] = { pos_.x - width_ / 2, pos_.y - height_ / 2 };
  vertex_[3] = { pos_.x + width_ / 2, pos_.y - height_ / 2 };

}


void dudina::Rectangle::printInformation()
{
  std::cout << "Height of rectangle: " << height_ << std::endl;
  std::cout << "Width of rectangle: " << width_ << std::endl;
  std::cout << "Center of rectangle: {" << pos_.x << "," << pos_.y << "}" << std::endl;
}

void dudina::Rectangle::rotate(const double degrees) 
{ 
  double angleSin = sin(degrees * M_PI / 180);
  double angleCos = cos(degrees * M_PI / 180);
  dudina::point_t center = getFrameRect().pos;
  for (int i = 0; i < 4; i++)
  {
    vertex_[i] = { center.x + angleCos * (vertex_[i].x - center.x) - angleSin * (vertex_[i].y - center.y),
      center.y + angleCos * (vertex_[i].y - center.y) + angleSin * (vertex_[i].x - center.x) };
  }

 }

std::string dudina::Rectangle::getName() const 
{
  return "R";
}


