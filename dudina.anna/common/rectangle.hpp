#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"
#include <string>

namespace dudina
{
  class Rectangle :
    public Shape
  {
  public:
    Rectangle(const point_t & pos, const double width, const double height);
    virtual double getArea() const  override;
    virtual rectangle_t getFrameRect() const override;
    virtual void move(const point_t & pos) override;
    virtual void move(double dx, const double dy) override;
    void scale(const double coeff) override;
    void printInformation() override;
    void rotate(const double degrees) override;
    std::string getName() const override;

  private:
    point_t pos_;
    double width_;
    double height_;
    point_t vertex_[4];


  };
}

#endif // !RECTANGLE_HPP
