#include "circle.hpp"

#include <iostream>
#include <cmath>


Circle::Circle(const point_t &startPos, const double radius):
  pos_{startPos},
  radius_(0.0)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument("Invalid Radius ! ");
  }
  radius_ = radius;
}

double Circle::getArea() const
{
  return M_PI*radius_*radius_;
}

rectangle_t Circle::getFrameRect() const
{
  return {pos_, 2*radius_, 2*radius_};
}

void Circle::move(const point_t &newPos)
{
  pos_ = newPos;
}

void Circle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void Circle::info() const
{
  const rectangle_t temp = getFrameRect();
  std::cout << "X = " << pos_.x << "; Y = " << pos_.y << ";" << std::endl;
  std::cout << "Radius = " << radius_ << ";" << std::endl;
  std::cout << "Circle Area: " << getArea() << std::endl;
  std::cout << "Frame properties: " << "pos = {" << temp.pos.x << ", " << temp.pos.y << "} ";
  std::cout << "Height = " << temp.height << "; Width = " << temp.width << "." << std::endl;
}
