#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void moveShape(Shape & shape, const point_t & newPos)
{
  std::cout << "OLD DATA: " << std::endl;
  shape.info();
  shape.move(newPos);
  std::cout << std::endl << "NEW DATA: " << std::endl;
  shape.info();
  std::cout << std::endl << std::endl;
}

void moveShape(Shape & shape, const double dx, const double dy)
{
  std::cout << "OLD DATA: " << std::endl;
  shape.info();
  shape.move(dx, dy);
  std::cout << std::endl << "NEW DATA: " << std::endl;
  shape.info();
  std::cout << std::endl << std::endl;
}


int main()
{
  try
  {
    Rectangle rectangle({0.0, 0.0}, 20.0, 13.5);
    Circle circle({10.0, 18.0}, 100.0);
    Triangle triangle({0.0, 0.0}, {12.5, 0.0}, {5.0, 5.0});

    std::cout << "Triangle test:_________________" << std::endl;
    moveShape(triangle, 10.5, 20.12);
    moveShape(triangle, 126.0, 300.0);

    std::cout << "Circle test:_________________" << std::endl;
    moveShape(circle, 31.23, 98.66);
    moveShape(circle, {300.134, 431.31});

    std::cout << "Rectangle test:________________" << std::endl;
    moveShape(rectangle, 100.0, 234.55);
    moveShape(rectangle, {321.3, 0.0});

  }
  catch(std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }


  return 0;
}
