#ifndef A1_RECTANGLE_HPP
#define A1_RECTANGLE_HPP

#include "shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(const point_t & startPos, const double height, const double width);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t & newPos) override;
  void move(const double dx, const double dy) override;
  void info() const override;
private:
  rectangle_t data_;
};

#endif //A1_RECTANGLE_HPP
