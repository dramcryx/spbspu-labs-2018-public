#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double precision = 1e-3;

BOOST_AUTO_TEST_SUITE(RectangleTest)
  BOOST_AUTO_TEST_CASE(InvalidParameters)
  {
    BOOST_CHECK_THROW(fedorov::Rectangle({3.5, 0.0}, -21.53, 53.6), std::invalid_argument);
    BOOST_CHECK_THROW(fedorov::Rectangle({3.5, 0.0}, 24.6, -2.0), std::invalid_argument);
    BOOST_CHECK_THROW(fedorov::Rectangle({3.5, 0.0}, -1001.53, -10.6), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingTest)
  {
    fedorov::Rectangle rectangle({10.0, -24.56}, 10.0, 10.0);
    const fedorov::rectangle_t prevData = rectangle.getFrameRect();
    const double prevArea = rectangle.getArea();

    rectangle.move(32.53, 12.31);
    BOOST_CHECK_CLOSE(rectangle.getArea(), prevArea, precision);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, prevData.height, precision);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, prevData.width, precision);

    rectangle.move({100.0, 200.425});
    BOOST_CHECK_CLOSE(rectangle.getArea(), prevArea, precision);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, prevData.height, precision);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, prevData.width, precision);
  }

  BOOST_AUTO_TEST_CASE(Scalling)
  {
    fedorov::Rectangle rectangle({20.32, 426.64}, 12.5, 10.2);
    const double k = 2.7;
    const double prevArea = rectangle.getArea();
    rectangle.scale(k);

    BOOST_CHECK_THROW(rectangle.scale(-42.3), std::invalid_argument);
    BOOST_CHECK_CLOSE(rectangle.getArea(), prevArea*k*k, precision);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CircleTest)

  BOOST_AUTO_TEST_CASE(InvalidParameters)
  {
    BOOST_CHECK_THROW(fedorov::Circle({42.25, 0.0}, -42.1), std::invalid_argument);
    BOOST_CHECK_THROW(fedorov::Circle({42.25, 0.0}, -0.001), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingTest)
  {
    fedorov::Circle circle({31.0, 10.6}, 10.0);
    const fedorov::rectangle_t prevData = circle.getFrameRect();
    const double prevArea = circle.getArea();

    circle.move(1.45, -3.7);
    BOOST_CHECK_CLOSE(circle.getArea(), prevArea, precision);
    BOOST_CHECK_CLOSE(circle.getFrameRect().height, prevData.height, precision);
    BOOST_CHECK_CLOSE(circle.getFrameRect().width, prevData.width, precision);

    circle.move(0.0, -20.5);
    BOOST_CHECK_CLOSE(circle.getArea(), prevArea, precision);
    BOOST_CHECK_CLOSE(circle.getFrameRect().height, prevData.height, precision);
    BOOST_CHECK_CLOSE(circle.getFrameRect().width, prevData.width, precision);
  }

  BOOST_AUTO_TEST_CASE(Scalling)
  {
    fedorov::Circle circle({42.25, 12.67}, 14.0);
    const double prevArea = circle.getArea();
    const double k = 2.0;
    circle.scale(k);

    BOOST_CHECK_THROW(circle.scale(-24.6), std::invalid_argument);
    BOOST_CHECK_CLOSE(prevArea*k*k, circle.getArea(), precision);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TriangleTest)

  BOOST_AUTO_TEST_CASE(MovingTest)
  {
    fedorov::Triangle triangle({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    const double prevArea = triangle.getArea();
    const fedorov::rectangle_t prevData = triangle.getFrameRect();

    triangle.move({20.0, 0.0});

    BOOST_CHECK_CLOSE(triangle.getArea(), prevArea, precision);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, prevData.height, precision);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, prevData.width, precision);

    triangle.move(42.6, 421.3);

    BOOST_CHECK_CLOSE(triangle.getArea(), prevArea, precision);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, prevData.height, precision);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, prevData.width, precision);
  }

  BOOST_AUTO_TEST_CASE(Scalling)
  {
    fedorov::Triangle triangle({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});
    const double prevArea = triangle.getArea();
    const double k = 2.4;

    triangle.scale(k);

    BOOST_CHECK_THROW(triangle.scale(-42.6), std::invalid_argument);
    BOOST_CHECK_CLOSE(triangle.getArea(), prevArea*k*k, precision);
  }

BOOST_AUTO_TEST_SUITE_END()
