#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include  "composite-shape.hpp"

int main()
{
  std::shared_ptr<fedorov::Shape> rectangle(new fedorov::Rectangle({32.0, 12.34}, 42.5, 1.12));
  std::shared_ptr<fedorov::Shape> circle(new fedorov::Circle({32.2, -2.4}, 10.0));
  std::shared_ptr<fedorov::Shape> triangle(new fedorov::Triangle({0.0, 10.0}, {-10.35, 0.0}, {42.4, -4.3}));

  fedorov::CompositeShape composite;
  std::cout << "Created empy composite-shape" << std::endl;
  composite.add(rectangle);
  composite.add(circle);
  composite.add(triangle);
  std::cout << "Added all 3 types of shape!" << std::endl;

  fedorov::CompositeShape composite1(rectangle);
  std::cout << "Created composite-shape with ctor parameter!" << std::endl;


  composite.info();
  std::cout << std::endl;
  composite[0].info();
  composite[1].info();
  composite[2].info();
  std::cout << std::endl;

  composite1.info();
  composite1[0].info();


  return 0;
}
