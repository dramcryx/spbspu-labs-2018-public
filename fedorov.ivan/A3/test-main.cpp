#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double precision = 1e-3;


BOOST_AUTO_TEST_SUITE(CompositeTest)

  BOOST_AUTO_TEST_CASE(InvalidParameters)
  {
    fedorov::CompositeShape composite;
    fedorov::CompositeShape compositeF;

    std::shared_ptr<fedorov::Shape> rectangle(new fedorov::Rectangle({21.35, 64.7}, 1.24, 126.542));
    std::shared_ptr<fedorov::Shape> circle(new fedorov::Circle({31.35, 24.75}, 12.2));
    std::shared_ptr<fedorov::Shape> triangle(new fedorov::Triangle({0.0, 0.0}, {-2.42, 0.0}, {0.0, -32.6}));

    compositeF.add(rectangle);
    compositeF.add(circle);
    compositeF.add(triangle);

    BOOST_CHECK_THROW(composite.add(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(fedorov::CompositeShape(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(compositeF[3], std::out_of_range);
    BOOST_CHECK_THROW(compositeF[-23], std::out_of_range);
    BOOST_CHECK_THROW(compositeF.scale(-31), std::logic_error);
    BOOST_CHECK_THROW(compositeF.remove(3), std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(MovingTest)
  {
    fedorov::CompositeShape composite;
    std::shared_ptr<fedorov::Shape> circle(new fedorov::Circle({0.0, 6.0}, 10.3));
    std::shared_ptr<fedorov::Shape> rectangle(new fedorov::Rectangle({-12.5, 0.0}, 10.0, 20.0));
    std::shared_ptr<fedorov::Shape> triangle(new fedorov::Triangle({0.0, 0.0}, {-2.42, 0.0}, {-1.0, 21.6}));

    composite.add(circle);
    composite.add(rectangle);

    const double prevArea = composite.getArea();
    const fedorov::rectangle_t prevData = composite.getFrameRect();

    composite.move(80.0, -2.4);

    BOOST_CHECK_EQUAL(composite.getArea(), prevArea);
    BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().pos.x, prevData.pos.x+80.0, precision);
    BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().pos.y, prevData.pos.y-2.4, precision);
    BOOST_CHECK_EQUAL(composite.getFrameRect().width, prevData.width);
    BOOST_CHECK_EQUAL(composite.getFrameRect().height, prevData.height);

    composite.move(prevData.pos);

    BOOST_CHECK_EQUAL(composite.getArea(), prevArea);
    BOOST_CHECK_EQUAL(composite.getFrameRect().pos.x, prevData.pos.x);
    BOOST_CHECK_EQUAL(composite.getFrameRect().pos.y, prevData.pos.y);
    BOOST_CHECK_EQUAL(composite.getFrameRect().width, prevData.width);
    BOOST_CHECK_EQUAL(composite.getFrameRect().height, prevData.height);
  }

  BOOST_AUTO_TEST_CASE(Scalling)
  {
    fedorov::CompositeShape composite;
    std::shared_ptr<fedorov::Shape> rectangle(new fedorov::Rectangle({10.0, 0.0}, 25.0, 12.35));
    std::shared_ptr<fedorov::Shape> circle(new fedorov::Circle({32.12, 26.7}, 12.2));
    composite.add(rectangle);
    composite.add(circle);
    const fedorov::point_t prevPos = composite.getFrameRect().pos;
    const double areaBefore = composite.getArea();
    const double k = 3.0;
    composite.scale(3.0);

    BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().pos.x, prevPos.x, precision);
    BOOST_CHECK_CLOSE_FRACTION(composite.getFrameRect().pos.y, prevPos.y, precision);
    BOOST_CHECK_CLOSE(composite.getArea(), k*k*areaBefore, precision);
    BOOST_CHECK_CLOSE_FRACTION(composite.getArea(), k*k*areaBefore, precision);
  }

  BOOST_AUTO_TEST_CASE(Area)
  {
    fedorov::CompositeShape composite;
    std::shared_ptr<fedorov::Shape> circle(new fedorov::Circle({0.0, 6.0}, 10.3));
    std::shared_ptr<fedorov::Shape> rectangle(new fedorov::Rectangle({-12.5, 0.0}, 10.0, 20.0));
    std::shared_ptr<fedorov::Shape> triangle(new fedorov::Triangle({0.0, 0.0}, {-2.42, 0.0}, {-1.0, 21.6}));
    const double predictedArea = circle->getArea() + rectangle->getArea() + triangle->getArea();

    composite.add(circle);
    composite.add(rectangle);
    composite.add(triangle);

    BOOST_CHECK_EQUAL(predictedArea, composite.getArea());
  }

  BOOST_AUTO_TEST_CASE(TestMoveOperator)
  {
    fedorov::CompositeShape composite;
    std::shared_ptr<fedorov::Shape> circle(new fedorov::Circle({0, 10}, 15));
    std::shared_ptr<fedorov::Shape> rectangle(new fedorov::Rectangle({10, 15}, 20, 30));

    composite.add(circle);
    composite.add(rectangle);
    
    fedorov::CompositeShape compositeF(std::move(composite));
    BOOST_CHECK_EQUAL(compositeF.getFrameRect().pos.x, 5);
    BOOST_CHECK_EQUAL(compositeF.getFrameRect().pos.y, 10);
    BOOST_CHECK_EQUAL(compositeF.getFrameRect().width, 40);
    BOOST_CHECK_EQUAL(compositeF.getFrameRect().height, 30);
  }

  BOOST_AUTO_TEST_CASE(TestCopyOperator)
  {
    fedorov::CompositeShape composite;
    fedorov::CompositeShape compositeF;
    std::shared_ptr<fedorov::Shape> circle(new fedorov::Circle({0.0, 6.0}, 10.3));
    std::shared_ptr<fedorov::Shape> rectangle(new fedorov::Rectangle({-12.5, 0.0}, 10.0, 20.0));
    std::shared_ptr<fedorov::Shape> triangle(new fedorov::Triangle({0.0, 0.0}, {-2.42, 0.0}, {-1.0, 21.6}));

    composite.add(circle);
    composite.add(rectangle);
    composite.add(triangle);

    compositeF = composite;
    BOOST_CHECK_EQUAL(compositeF.getFrameRect().pos.x, composite.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(compositeF.getFrameRect().pos.y, composite.getFrameRect().pos.y);
    BOOST_CHECK_EQUAL(compositeF.getFrameRect().width, composite.getFrameRect().width);
    BOOST_CHECK_EQUAL(compositeF.getFrameRect().height, composite.getFrameRect().height);
  }


BOOST_AUTO_TEST_SUITE_END()
