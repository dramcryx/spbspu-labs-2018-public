#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

int main()
{
  try
  {
    std::shared_ptr<fedorov::Shape> rectangle1(new fedorov::Rectangle({10.0, 10.0}, 4.0, 5.0));
    std::shared_ptr<fedorov::Shape> rectangle2(new fedorov::Rectangle({0.0, 0.0}, 6.0, 4.0));
    std::shared_ptr<fedorov::Shape> circle(new fedorov::Circle({0.0}, 5));
    std::shared_ptr<fedorov::CompositeShape> composite(new fedorov::CompositeShape(rectangle1));
    composite->add(circle);


    rectangle1->info();
    std::cout << std::endl;
    rectangle1->rotate(90);
    std::cout << "Rotared rectangle by 90 degrees: " << std::endl;
    rectangle1->info();
    rectangle1->rotate(20);
    std::cout << "Rotated rectangle by 20 degrees: " << std::endl;
    rectangle1->info();
    std::cout << std::endl;

    std::cout << std::endl;
    circle->info();
    circle->rotate(100);
    std::cout << "Rotated circle by 100 degrees (nothing happened) : " << std::endl;
    circle->info();
    std::cout << std::endl;

    composite->info();
    (*composite)[0].info();
    (*composite)[1].info();
    composite->rotate(31);
    std::cout << "Rotated composite-shape by 31 degrees :" << std::endl;
    composite->info();
    std::cout << std::endl;
    (*composite)[0].info();
    (*composite)[1].info();
    std::cout << std::endl;

    fedorov::Matrix matrix;
    std::cout << "Rows: " << matrix.getRowsCount() << " " << "Columns: " << matrix.getColumnsCount() << std::endl;
    matrix.addShape(rectangle1);
    std::cout << "Rows: " << matrix.getRowsCount() << " " << "Columns: " << matrix.getColumnsCount() << std::endl;
    matrix.addShape(rectangle2);
    std::cout << "Rows: " << matrix.getRowsCount() << " " << "Columns: " << matrix.getColumnsCount() << std::endl;
    matrix.addShape(circle);
    std::cout << "Rows: " << matrix.getRowsCount() << " " << "Columns: " << matrix.getColumnsCount() << std::endl;
    matrix.addShape(composite);
    std::cout << "Rows: " << matrix.getRowsCount() << " " << "Columns: " << matrix.getColumnsCount() << std::endl;
    matrix.view();
  }
  catch (std::invalid_argument& e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }

  return 0;
}
