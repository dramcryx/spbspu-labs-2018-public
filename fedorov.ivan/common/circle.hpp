#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace fedorov
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t& startPos, const double radius);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& newPos) override;
    void move(const double dx, const double dy) override;
    void scale(const double multiplier) override;
    void rotate(const double angle) override;
    void info() const override;
    void printName() const override;
  private:
    point_t pos_;
    double radius_;
    double angle_;
  };
}

#endif //CIRCLE_HPP
