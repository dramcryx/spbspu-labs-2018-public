#include <iostream>
#include <cmath>
#include "composite-shape.hpp"

fedorov::CompositeShape::CompositeShape():
  shapeList_(nullptr),
  arrSize_(0),
  angle_(0.0)
{
}

fedorov::CompositeShape::CompositeShape(const std::shared_ptr<Shape>& shape):
  shapeList_(new std::shared_ptr<Shape>[1]),
  arrSize_(1),
  angle_(0.0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Error! Shape expected !");
  }
  shapeList_[0] = shape;
}

const fedorov::Shape& fedorov::CompositeShape::operator[](const int index) const
{
  if (index >= arrSize_ || index < 0)
  {
    throw std::out_of_range("Out of range!");
  }
  return *shapeList_[index];
}

fedorov::CompositeShape::CompositeShape(const CompositeShape &CShape) :
  shapeList_(new std::shared_ptr<Shape>[CShape.arrSize_]),
  arrSize_(CShape.arrSize_)
{
  for (int i = 0; i < CShape.arrSize_; i++)
  shapeList_[i] = CShape.shapeList_[i];
}

fedorov::CompositeShape::CompositeShape(CompositeShape &&CShape) :
  shapeList_(nullptr),
  arrSize_(CShape.arrSize_)
{
  shapeList_.swap(CShape.shapeList_);
  CShape.arrSize_ = 0;
}

fedorov::CompositeShape &fedorov::CompositeShape::operator=(const CompositeShape &CShape)
{
  if (this != &CShape)
  {
    CompositeShape new_composite_shape(CShape);
    this->shapeList_.swap(new_composite_shape.shapeList_);
    this->arrSize_ = CShape.arrSize_;
  }
  return *this;
}

fedorov::CompositeShape &fedorov::CompositeShape::operator=(CompositeShape &&CShape)
{
  this->shapeList_.swap(CShape.shapeList_);
  this->arrSize_ = CShape.arrSize_;
  CShape.shapeList_.reset();
  CShape.arrSize_ = 0;
  return *this;
}


double fedorov::CompositeShape::getArea() const
{
  if (arrSize_ == 0)
  {
    throw std::logic_error("Can't get area without shape!");
  }

  double area = 0.0;
  for (int i = 0; i < arrSize_; i++)
  {
    area += shapeList_[i]->getArea();
  }
  return area;
}

fedorov::rectangle_t fedorov::CompositeShape::getFrameRect() const
{
  if (arrSize_ == 0)
  {
    throw std::logic_error("Can't get frame from nothing !");
  }

  rectangle_t rect = shapeList_[0]->getFrameRect();
  double minX = rect.pos.x - rect.width / 2;
  double minY = rect.pos.y - rect.height / 2;
  double maxX = rect.pos.x + rect.width / 2;
  double maxY = rect.pos.y + rect.height / 2;
  point_t minXY = {minX, minY};
  point_t maxXY = {maxX, maxY};

  for (int i = 1; i < arrSize_; i++)
  {
    rect = shapeList_[i]->getFrameRect();
    minX = rect.pos.x - rect.width / 2;
    minY = rect.pos.y - rect.height / 2;
    maxX = rect.pos.x + rect.width / 2;
    maxY = rect.pos.y + rect.height / 2;

    if (minX < minXY.x)
    {
      minXY.x = minX;
    }

    if (minY < minXY.y)
    {
      minXY.y = minY;
    }

    if (maxX > maxXY.x)
    {
      maxXY.x = maxX;
    }

    if (maxY > maxXY.y)
    {
      maxXY.y = maxY;
    }
  }

  point_t center = {minXY.x + (maxXY.x - minXY.x) / 2, minXY.y + (maxXY.y - minXY.y) / 2};
  return {center, maxXY.y - minXY.y, maxXY.x - minXY.x};
}

void fedorov::CompositeShape::move(const point_t& newPos)
{
  point_t center = getFrameRect().pos;
  move(newPos.x - center.x, newPos.y - center.y);
}

void fedorov::CompositeShape::move(const double dx, const double dy)
{
  for (int i = 0; i < arrSize_; i++)
  {
    shapeList_[i]->move(dx, dy);
  }
}

void fedorov::CompositeShape::scale(const double multiplier)
{
  if (multiplier < 0 || arrSize_ == 0)
  {
    throw std::logic_error("Invalid coefficient/composite-shape!");
  }

  point_t center = getFrameRect().pos;


  for (int i = 0; i < arrSize_; i++)
  {
    point_t centerShape = shapeList_[i]->getFrameRect().pos;

    shapeList_[i]->move({
      center.x + multiplier * (centerShape.x - center.x),
      center.y + multiplier * (centerShape.y - center.y)
    });
    shapeList_[i]->scale(multiplier);
  }
}

void fedorov::CompositeShape::add(const std::shared_ptr<Shape> shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Error! Shape expected!");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(new std::shared_ptr<Shape>[arrSize_ + 1]);
  for (int i = 0; i < arrSize_; i++)
  {
    tempArr[i] = shapeList_[i];
  }

  tempArr[arrSize_] = shape;
  shapeList_.swap(tempArr);
  arrSize_++;
}

void fedorov::CompositeShape::remove(const int index)
{
  if (index >= arrSize_ || index < 0)
  {
    throw std::out_of_range("Error! Out of range!");
  }

  std::unique_ptr<std::shared_ptr<Shape>[]> tempArr(new std::shared_ptr<Shape>[arrSize_ - 1]);
  for (int i = 0; i < index; i++)
  {
    tempArr[i] = shapeList_[i];
  }

  for (int i = index; i < arrSize_ - 1; i++)
  {
    tempArr[i] = shapeList_[i + 1];
  }
  shapeList_.swap(tempArr);
  arrSize_--;
}

void fedorov::CompositeShape::info() const
{
  const rectangle_t rect = getFrameRect();
  std::cout << "Information about CompositeShape: " << std::endl;
  std::cout << "FrameRect: Height = " << rect.height << "; Width = " << rect.width << "; " << std::endl;
  std::cout << "Center position = (" << rect.pos.x << ", " << rect.pos.y << ");" << std::endl;
  std::cout << "Area = " << getArea() << std::endl;
}

void fedorov::CompositeShape::rotate(const double angle)
{
  angle_ += angle;

  double angle1 = angle * M_PI / 180;
  double angle2 = 0;
  point_t center = getFrameRect().pos;

  for (int i = 0; i < arrSize_; i++)
  {
    point_t centerShape = shapeList_[i]->getFrameRect().pos;
    double radius = sqrt(pow(center.x - centerShape.x, 2) + pow(center.y - centerShape.y, 2));

    if (centerShape.y > center.y)
    {
      angle2 = acos((centerShape.x - center.x) / radius);
    }
    else
    {
      angle2 = 2 * M_PI - acos((centerShape.x - center.x) / radius);
    }
    shapeList_[i]->move((cos(angle2 + angle1) - cos(angle2)) * radius,
                       (sin(angle2 + angle1) - sin(angle2)) * radius);
    shapeList_[i]->rotate(angle);
  }
}

void fedorov::CompositeShape::printName() const
{
  std::cout <<  "Composite ";
}
