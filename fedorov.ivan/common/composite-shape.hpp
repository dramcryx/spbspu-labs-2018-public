#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace fedorov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const std::shared_ptr<Shape> & shape);
    const Shape & operator[](int index) const;
    CompositeShape(const CompositeShape &CShape);
    CompositeShape(CompositeShape &&CShape);
    CompositeShape &operator=(const CompositeShape &CShape);
    CompositeShape &operator=(CompositeShape &&CShape);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& newPos) override;
    void move(const double dx, const double dy) override;
    void scale(const double multiplier) override;
    void rotate(const double angle) override;
    void add(const std::shared_ptr<Shape> shape);
    void remove(const int index);
    void info() const override;
    virtual void printName() const override;
  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> shapeList_;
    int arrSize_;
    double angle_;
  };
}

#endif
