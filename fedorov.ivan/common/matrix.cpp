#include <iostream>
#include <cmath>
#include "matrix.hpp"

fedorov::Matrix::Matrix():
  lineArr_(nullptr),
  rows_(0),
  columns_(0)
{
}

fedorov::Matrix::Matrix(const Matrix& matrix):
  lineArr_(new std::shared_ptr<Shape>[matrix.rows_ * matrix.columns_]),
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  for (int i = 0; i < rows_ * columns_; i++)
  {
    lineArr_[i] = matrix.lineArr_[i];
  }
}

fedorov::Matrix::Matrix(Matrix&& matrix):
  lineArr_(std::move(matrix.lineArr_)),
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  matrix.rows_ = 0;
  matrix.columns_ = 0;
}

fedorov::Matrix& fedorov::Matrix::operator=(const Matrix& matrix)
{
  if (this == &matrix)
  {
    return *this;
  }

  lineArr_.reset(new std::shared_ptr<Shape>[matrix.rows_ * matrix.columns_]);
  rows_ = matrix.rows_;
  columns_ = matrix.columns_;
  for (int i = 0; i < rows_ * columns_; i++)
  {
    lineArr_[i] = matrix.lineArr_[i];
  }
  return *this;
}

fedorov::Matrix& fedorov::Matrix::operator=(Matrix&& matrix)
{
  lineArr_ = std::move(matrix.lineArr_);
  rows_ = matrix.rows_;
  columns_ = matrix.columns_;
  for (int i = 0; i < rows_ * columns_; i++)
  {
    lineArr_[i] = matrix.lineArr_[i];
  }
  return *this;
}

std::unique_ptr<std::shared_ptr<fedorov::Shape>[]> fedorov::Matrix::operator[](const int index)
{
  if (index >= rows_ || index < 0)
  {
    throw std::out_of_range("Out of range! ");
  }

  std::unique_ptr<std::shared_ptr<Shape>[]> array(new std::shared_ptr<Shape>[columns_]);

  for (int i = 0; i < rows_; i++)
  {
    array[i] = lineArr_[index * rows_ * i];
  }
  return array;
}

void fedorov::Matrix::addShape(const std::shared_ptr<fedorov::Shape> figure)
{
  if (figure == nullptr)
  {
    throw std::invalid_argument("It can not be empty figure!");
  }
  if ((rows_ == 0) && (columns_ == 0))
  {
    lineArr_.reset(new std::shared_ptr<Shape>[1]);
    rows_ = 1;
    columns_ = 1;
    lineArr_[0] = figure;
    return;
  }
  int count1 = 0;
  for (int i = 0; i < rows_; i++)
  {
    int count2 = 0;
    for (int j = 0; j < columns_; j++)
    {
      if (lineArr_[i * columns_ + j] == nullptr)
      {
        lineArr_[i * columns_ + j] = figure;
        return;
      }
      if (checkIntersection(lineArr_[i * columns_ + j], figure))
      {
        count2 = j + 1;
        break;
      }
      count2 = j + 1;
    }
    if (count2 == columns_)
    {
      std::unique_ptr<std::shared_ptr<Shape> []> newmap(new std::shared_ptr<Shape>[rows_ * (columns_ + 1)]);
      for (int m = 0; m < rows_; m++)
      {
        for (int n = 0; n < columns_; n++)
        {
          newmap[m * columns_ + n + m] = lineArr_[m * columns_ + n];
        }
      }
      columns_++;
      newmap[(i + 1) * columns_ - 1] = figure;
      lineArr_ = std::move(newmap);
      return;
    }
    count1 = i + 1;
  }
  if (count1 == rows_)
  {
    std::unique_ptr<std::shared_ptr<Shape> []> newmap(new std::shared_ptr<Shape>[(rows_ + 1) * columns_]);
    for (int i = 0; i < rows_ * columns_; i++)
    {
      newmap[i] = lineArr_[i];
    }
    newmap[rows_ * columns_] = figure;
    rows_++;
    lineArr_ = std::move(newmap);
  }
}


int fedorov::Matrix::getRowsCount() const
{
  return rows_;
}

int fedorov::Matrix::getColumnsCount() const
{
  return columns_;
}

void fedorov::Matrix::view() const
{
  for (int i = 0; i < rows_; i++)
  {
    for (int j = 0; j < columns_; j++)
    {
      if (lineArr_[i * columns_ + j] != nullptr)
      {
        lineArr_[i * columns_ + j]->printName();
      }
    }
    std::cout << std::endl;
  }
}

bool fedorov::Matrix::checkIntersection(const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2)
{
  {
    if (shape1 == nullptr || shape2 == nullptr)
    {
      throw std::invalid_argument("Shape expected, found nullptr !");
    }
    fedorov::rectangle_t frame1 = shape1->getFrameRect();
    fedorov::rectangle_t frame2 = shape2->getFrameRect();
    return ((fabs(frame1.pos.x - frame2.pos.x) < frame1.width / 2 + frame2.width / 2) &&
            fabs(frame1.pos.y - frame2.pos.y) < frame1.height / 2 + frame2.height / 2);
  }
}
