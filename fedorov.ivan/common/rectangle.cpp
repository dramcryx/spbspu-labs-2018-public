#include <iostream>
#include <cmath>
#include "rectangle.hpp"

fedorov::Rectangle::Rectangle(const point_t& startPos, const double height, const double width):
  data_{startPos, 0.0, 0.0},
  angle_(0.0)
{
  if (height < 0.0 || width < 0.0)
  {
    throw std::invalid_argument("Invalid height/width !");
  }
  data_.height = height;
  data_.width = width;
}

double fedorov::Rectangle::getArea() const
{
  return data_.height * data_.width;
}

fedorov::rectangle_t fedorov::Rectangle::getFrameRect() const
{
  double minX =
    data_.pos.x - (data_.width / 2) * cos(angle_ * M_PI / 180) - (data_.height / 2) * sin(angle_ * M_PI / 180);
  double minY =
    data_.pos.y - (data_.height / 2) * cos(angle_ * M_PI / 180) - (data_.width / 2) * sin(angle_ * M_PI / 180);
  double maxY =
    data_.pos.y + (data_.height / 2 ) * cos(angle_ * M_PI / 180) + (data_.width / 2) * sin(angle_ * M_PI / 180);
  double maxX =
    data_.pos.x + (data_.width / 2) * cos(angle_ * M_PI / 180) + (data_.height / 2) * sin(angle_ * M_PI / 180);

  double newWidth = std::abs(maxX - minX);
  double newHeight = std::abs(minY - maxY);

  return {data_.pos, newHeight, newWidth};
}

void fedorov::Rectangle::move(const point_t& newPos)
{
  data_.pos = newPos;
}

void fedorov::Rectangle::move(const double dx, const double dy)
{
  data_.pos.x += dx;
  data_.pos.y += dy;
}

void fedorov::Rectangle::scale(const double multiplier)
{
  if (multiplier < 0.0)
  {
    throw std::invalid_argument("Invalid multiplier !");
  }
  data_.height *= multiplier;
  data_.width *= multiplier;
}

void fedorov::Rectangle::info() const
{
  std::cout << "X = " << data_.pos.x << "; Y = " << data_.pos.y << ";" << std::endl;
  std::cout << "Height = " << data_.height << "; Width = " << data_.width << ";" << std::endl;
  std::cout << "Rectangle Frame - same." << std::endl;
}

void fedorov::Rectangle::rotate(const double angle)
{
  angle_ += angle;
}

void fedorov::Rectangle::printName() const
{
  std::cout << "Rectangle ";
}
