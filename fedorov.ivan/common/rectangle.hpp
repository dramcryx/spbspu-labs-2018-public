#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace fedorov
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t& startPos, const double height, const double width);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& newPos) override;
    void move(const double dx, const double dy) override;
    void scale(const double multiplier) override;
    void rotate(const double angle) override;
    void info() const override;
    void printName() const override;
  private:
    rectangle_t data_;
    double angle_;
  };
}

#endif //RECTANGLE_HPP
