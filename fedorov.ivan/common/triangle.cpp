#include <iostream>
#include <cmath>
#include "triangle.hpp"

fedorov::Triangle::Triangle(const point_t& a, const point_t& b, const point_t& c):
  a_(a),
  b_(b),
  c_(c),
  center_{0.0, 0.0}
{
  center_ = {(a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3};
}

double fedorov::Triangle::getArea() const
{
  return fabs((a_.x - c_.x) * (b_.y - c_.y) - (a_.y - c_.y) * (b_.x - c_.x)) * 0.5;
}

fedorov::rectangle_t fedorov::Triangle::getFrameRect() const
{
  double maxX = a_.x > b_.x ? (a_.x > c_.x ? a_.x : c_.x) : (b_.x > c_.x ? b_.x : c_.x);
  double minX = a_.x < b_.x ? (a_.x < c_.x ? a_.x : c_.x) : (b_.x < c_.x ? b_.x : c_.x);
  double maxY = a_.y > b_.y ? (a_.y > c_.y ? a_.y : c_.y) : (b_.y > c_.y ? b_.y : c_.y);
  double minY = a_.y < b_.y ? (a_.y < c_.y ? a_.y : c_.y) : (b_.y < c_.y ? b_.y : c_.y);
  return {{minX + (maxX - minX) / 2, minY + (maxY - minY) / 2}, maxY - minY, maxX - minX};
}

void fedorov::Triangle::move(const point_t& newPos)
{
  a_.x -= center_.x - newPos.x;
  a_.y -= center_.y - newPos.y;
  b_.x -= center_.x - newPos.x;
  b_.y -= center_.y - newPos.y;
  c_.x -= center_.x - newPos.x;
  c_.y -= center_.y - newPos.y;
  center_ = newPos;
}

void fedorov::Triangle::move(double dx, const double dy)
{
  a_.x += dx;
  a_.y += dy;
  b_.x += dx;
  b_.y += dy;
  c_.x += dx;
  c_.y += dy;
  center_.x += dx;
  center_.y += dy;
}

void fedorov::Triangle::scale(const double multiplier)
{
  if (multiplier < 0.0)
  {
    throw std::invalid_argument("Invalid multiplier !");
  }
  a_.x = center_.x + multiplier * (a_.x - center_.x);
  b_.x = center_.x + multiplier * (b_.x - center_.x);
  c_.x = center_.x + multiplier * (c_.x - center_.x);
  a_.y = center_.y + multiplier * (a_.y - center_.y);
  b_.y = center_.y + multiplier * (b_.y - center_.y);
  c_.y = center_.y + multiplier * (c_.y - center_.y);
}

void fedorov::Triangle::info() const
{
  const rectangle_t temp = getFrameRect();
  std::cout << "A = (" << a_.x << ", " << a_.y << ");" << std::endl;
  std::cout << "B = (" << b_.x << ", " << b_.y << ");" << std::endl;
  std::cout << "C = (" << c_.x << ", " << c_.y << ");" << std::endl;
  std::cout << "CenterX = " << center_.x << "; CenterY = " << center_.y << ";" << std::endl;
  std::cout << "Triangle Area: " << getArea() << std::endl;
  std::cout << "Frame properties: " << "pos = {" << temp.pos.x << ", " << temp.pos.y << "} ";
  std::cout << "Height = " << temp.height << "; Width = " << temp.width << "." << std::endl;
}

void fedorov::Triangle::rotate(const double angle)
{
  point_t centre = getFrameRect().pos;
  double angle1 = (angle * M_PI) / 180;
  const double sin_a = sin(angle1);
  const double cos_a = cos(angle1);
  a_ = { centre.x + (a_.x - centre.x) * cos_a - (a_.y - centre.y) * sin_a,
         centre.y + (a_.y - centre.y) * cos_a + (a_.x - centre.x) * sin_a };
  b_ = { centre.x + (b_.x - centre.x) * cos_a - (b_.y - centre.y) * sin_a,
         centre.y + (b_.y - centre.y) * cos_a + (b_.x - centre.x) * sin_a };
  c_ = { centre.x + (c_.x - centre.x) * cos_a - (c_.y - centre.y) * sin_a,
         centre.y + (c_.y - centre.y) * cos_a + (c_.x - centre.x) * sin_a };

}

void fedorov::Triangle::printName() const
{
  std::cout << "Triangle ";
}
