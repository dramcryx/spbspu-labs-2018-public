#include <iostream>
#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

using namespace gerastyonok;

void testShape(Shape & shape)
{
  std::cout << "Area: " << shape.getArea() << std::endl;
  std::cout << "Frame: height = " << shape.getFrameRect().height << " width = " << shape.getFrameRect().width << std::endl;

  shape.move({ 7, 3 });
  std::cout << "Area: " << shape.getArea() << std::endl;
  shape.move(3, 7);
  std::cout << "Area: " << shape.getArea() << std::endl;
}

int main()
{
  try
  {
    Rectangle sRectangle({ 2.0 , 7.0 , { 0.0 , 0.0 } });
    Circle sCircle(5.0, {0.0, 0.0});
    Triangle sTriangle({1.0, 3.0} , {4.0 , 1.0} , {6.0 , 4.0});

    std::cout << "Rectangle: " << std::endl;
    testShape(sRectangle);
    std::cout << "Circle: " << std::endl;
    testShape(sCircle);
    std::cout << "Triangle: " << std::endl;
    testShape(sTriangle);

    std::cout << "[CompositeShape]" << std::endl;
    std::shared_ptr<gerastyonok::Shape> rect1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::Rectangle({13.0, 10.0, {2.0, 5.0}}));
    std::shared_ptr<gerastyonok::Shape> circ1 =
      std::make_shared<gerastyonok::Circle>(gerastyonok::Circle(10.9, {0.0, 0.0}));
    gerastyonok::CompositeShape testComp(rect1);
    testComp.addShape(circ1);
    std::cout << "From Composite Shape1: " << std::endl;
    std::cout << testComp.getSize() << " Figures here" << std::endl;
    std::cout << "testComp getArea: " << testComp.getArea() << std::endl;
    std::cout << "testComp getFrameRect: height = " << testComp.getFrameRect().height
      << " width = " << testComp.getFrameRect().width << std::endl;
    testComp.removeShape(0);
    std::cout << "Now " << testComp.getSize() << " Figures here" << std::endl;
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }

  catch (std::out_of_range & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  return 0;
}
