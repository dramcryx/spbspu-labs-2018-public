#define BOOST_TEST_MODULE

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>

#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double POGR = 1e-8;

BOOST_AUTO_TEST_SUITE(RectangleTest)

  BOOST_AUTO_TEST_CASE(InvalidArgumentsConstructor)
  {
    BOOST_CHECK_THROW( gerastyonok::Rectangle({-3.0, 88.5, {0.0, 0.0}}), std::invalid_argument );
    BOOST_CHECK_THROW( gerastyonok::Rectangle({7.0, -21.0, {0.0, 0.0}}), std::invalid_argument );
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentScale)
  {
    gerastyonok::Rectangle rect{ {3.0, 7.0, {0.0, 0.0}} };
    BOOST_CHECK_THROW( rect.scale(-1.0), std::invalid_argument );
  }

  BOOST_AUTO_TEST_CASE(MoveByAxes)
  {
    gerastyonok::Rectangle rect{ {5.0, 6.0, {0.0, 0.0}} };
    gerastyonok::rectangle_t rectBeforeMoving = rect.getFrameRect();
    double areaBeforeMoving = rect.getArea();
    gerastyonok::point_t positionBeforeMoving = {0.0, 0.0};
    BOOST_CHECK_CLOSE(areaBeforeMoving, rect.getArea(), POGR);
    BOOST_CHECK_EQUAL(positionBeforeMoving.x, rect.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(positionBeforeMoving.y, rect.getFrameRect().pos.y);
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, rect.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, rect.getFrameRect().height);

    gerastyonok::point_t positionAfterMoving = {-7.0, 3.0};
    rect.move(-7.0, 3.0);
    BOOST_CHECK_EQUAL(areaBeforeMoving, rect.getArea());
    BOOST_CHECK_CLOSE(positionAfterMoving.x, rect.getFrameRect().pos.x, POGR);
    BOOST_CHECK_CLOSE(positionAfterMoving.y, rect.getFrameRect().pos.y, POGR);
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, rect.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, rect.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(Scale)
  {
    gerastyonok::Rectangle rect{ {5.0, 6.0, {0.0, 0.0}} };
    double areaBeforeScaling = rect.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScaling, rect.getArea(), POGR);

    const double k = 2.0;
    rect.scale(k);
    BOOST_CHECK_CLOSE(k * k * areaBeforeScaling, rect.getArea(), POGR);
  }

  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(Circle)

  BOOST_AUTO_TEST_CASE(InvalidArgumentScale)
  {
    gerastyonok::Circle circle{ 7.0, {0.0, 0.0}};
    BOOST_CHECK_THROW( circle.scale(-1.0), std::invalid_argument );
  }

  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    gerastyonok::Circle circle{ 3.0, {0.0, 0.0} };
    gerastyonok::rectangle_t rectBeforeMoving = circle.getFrameRect();
    double areaBeforeMoving = circle.getArea();
    gerastyonok::point_t positionBeforeMoving = {0.0, 0.0};
    BOOST_CHECK_EQUAL(positionBeforeMoving.x, circle.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(positionBeforeMoving.y, circle.getFrameRect().pos.y);
    BOOST_CHECK_CLOSE(areaBeforeMoving, circle.getArea(), POGR);
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, circle.getFrameRect().height);

    gerastyonok::point_t positionAfterMoving = {3.0, 7.0};
    circle.move({3.0, 7.0});
    BOOST_CHECK_EQUAL(positionAfterMoving.x, circle.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(positionAfterMoving.y, circle.getFrameRect().pos.y);
    BOOST_CHECK_CLOSE(areaBeforeMoving, circle.getArea(), POGR);
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, circle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MoveByAxes)
  {
    gerastyonok::Circle circle{ 3.0, {0.0, 0.0} };
    gerastyonok::rectangle_t rectBeforeMoving = circle.getFrameRect();
    double areaBeforeMoving = circle.getArea();
    gerastyonok::point_t positionBeforeMoving = {0.0, 0.0};
    BOOST_CHECK_EQUAL(positionBeforeMoving.x, circle.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(positionBeforeMoving.y, circle.getFrameRect().pos.y);
    BOOST_CHECK_CLOSE(areaBeforeMoving, circle.getArea(), POGR);
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, circle.getFrameRect().height);

    gerastyonok::point_t positionAfterMoving = {-5.0, 3.0};
    circle.move(-5.0, 3.0);
    BOOST_CHECK_CLOSE(positionAfterMoving.x, circle.getFrameRect().pos.x, POGR);
    BOOST_CHECK_CLOSE(positionAfterMoving.y, circle.getFrameRect().pos.y, POGR);
    BOOST_CHECK_CLOSE(areaBeforeMoving, circle.getArea(), POGR);
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, circle.getFrameRect().height);
  }

 BOOST_AUTO_TEST_CASE(Scale)
 {
   gerastyonok::Circle circle{ 3.0, {0.0, 0.0} };
   double areaBeforeScaling = circle.getArea();
   BOOST_CHECK_CLOSE(areaBeforeScaling, circle.getArea(), POGR);

   const double k = 7.7;
   circle.scale(k);
   BOOST_CHECK_CLOSE(k * k * areaBeforeScaling, circle.getArea(), POGR);
 }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Triangle)

  BOOST_AUTO_TEST_CASE(MoveByAxes)
  {
    gerastyonok::Triangle triangle({ 1.0, 1.0 }, { 2.0, 10.0 }, { 0.0, 0.0 });
    gerastyonok::rectangle_t rectBeforeMoving = triangle.getFrameRect();
    double areaBeforeMoving = triangle.getArea();
    gerastyonok::point_t positionBeforeMoving = { 1.0, 5.0 };
    BOOST_CHECK_EQUAL(positionBeforeMoving.x, triangle.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(positionBeforeMoving.y, triangle.getFrameRect().pos.y);
    BOOST_CHECK_CLOSE(areaBeforeMoving, triangle.getArea(), POGR);
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, triangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, triangle.getFrameRect().height);

    gerastyonok::point_t positionAfterMoving = { -4.0, 6.6666666666666661 };
    triangle.move(-5.0, 3.0);
    BOOST_CHECK_CLOSE(positionAfterMoving.x, triangle.getCenter().x, POGR);
    BOOST_CHECK_CLOSE(positionAfterMoving.y, triangle.getCenter().y, POGR);
    BOOST_CHECK_CLOSE(areaBeforeMoving, triangle.getArea(), POGR);
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, triangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, triangle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    gerastyonok::Triangle triangle({1.0, 1.0}, {2.0, 10.0}, {0.0, 0.0});
    gerastyonok::rectangle_t rectBeforeMoving = triangle.getFrameRect();
    double areaBeforeMoving = triangle.getArea();
    gerastyonok::point_t positionBeforeMoving = {1.0, 3.6666666666666665 };
    BOOST_CHECK_EQUAL(positionBeforeMoving.x, triangle.getCenter().x);
    BOOST_CHECK_EQUAL(positionBeforeMoving.y, triangle.getCenter().y);
    BOOST_CHECK_CLOSE(areaBeforeMoving, triangle.getArea(), POGR);
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, triangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, triangle.getFrameRect().height);

    gerastyonok::point_t positionAfterMoving = {1.0, 2.0};
    triangle.move({1.0, 2.0});
    BOOST_CHECK_EQUAL(positionAfterMoving.x, triangle.getCenter().x);
    BOOST_CHECK_EQUAL(positionAfterMoving.y, triangle.getCenter().y);
    BOOST_CHECK_CLOSE(areaBeforeMoving, triangle.getArea(), POGR);
    BOOST_CHECK_EQUAL(rectBeforeMoving.width, triangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(rectBeforeMoving.height, triangle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(Scale)
  {
    gerastyonok::Triangle triangle({1.0, 1.0}, {2.0, 10.0}, {0.0, 0.0});
    double areaBeforeScaling = triangle.getArea();
    BOOST_CHECK_CLOSE(areaBeforeScaling, triangle.getArea(), POGR);

    const double k = 3.3;
    triangle.scale(k);
    BOOST_CHECK_CLOSE(k * k * areaBeforeScaling, triangle.getArea(), POGR);
  }

  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(CompositeShapeTests)

  BOOST_AUTO_TEST_CASE(CopyConstructor)
  {
    std::shared_ptr<gerastyonok::Shape> rectangle1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{21.0, 10.0, {3.0, 7.0}});
    std::shared_ptr<gerastyonok::Shape> circle1 =
      std::make_shared<gerastyonok::Circle>(5.0, gerastyonok::point_t{0.0, 0.0});
    gerastyonok::CompositeShape testObject(rectangle1);
    testObject.addShape(circle1);
    gerastyonok::CompositeShape newTest(testObject);
    BOOST_CHECK_EQUAL(testObject[0], newTest[0]);
    BOOST_CHECK_EQUAL(testObject[1], newTest[1]);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructor)
  {
    std::shared_ptr<gerastyonok::Shape> rectangle1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{21.0, 10.0, {3.0, 7.0}});
    std::shared_ptr<gerastyonok::Shape> circle1 =
      std::make_shared<gerastyonok::Circle>(5.0, gerastyonok::point_t{0.0, 0.0});
    gerastyonok::CompositeShape testObject(rectangle1);
    testObject.addShape(circle1);
    gerastyonok::CompositeShape newTest(std::move(testObject));
    BOOST_CHECK_EQUAL(newTest[0], rectangle1);
    BOOST_CHECK_EQUAL(newTest[1], circle1);
    size_t quantity = 2;
    BOOST_CHECK_EQUAL(newTest.getSize(), quantity);
  }

  BOOST_AUTO_TEST_CASE(CopyOperator)
  {
    std::shared_ptr< gerastyonok::Shape > ptr1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{21.0, 10.0, {3.0, 7.0}});
    std::shared_ptr< gerastyonok::Shape > ptr2 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{2.0, 1.0, {31.0, 72.0}});
    std::shared_ptr< gerastyonok::Shape > ptr3 =
        std::make_shared<gerastyonok::Circle>(5.0, gerastyonok::point_t{0.0, 0.0});
    std::shared_ptr< gerastyonok::Shape > ptr4 =
      std::make_shared<gerastyonok::Circle>(15.0, gerastyonok::point_t{4.0, 10.0});
    gerastyonok::CompositeShape testComp(ptr1);
    testComp.addShape(ptr2);
    gerastyonok::CompositeShape newComp(ptr3);
    newComp.addShape(ptr4);
    newComp = testComp;
    BOOST_CHECK_EQUAL(newComp[0],testComp[0]);
    BOOST_CHECK_EQUAL(newComp[1],testComp[1]);
  }

  BOOST_AUTO_TEST_CASE(MoveOperator)
  {
    std::shared_ptr< gerastyonok::Shape > ptr1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{21.0, 10.0, {3.0, 7.0}});
    std::shared_ptr< gerastyonok::Shape > ptr2 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{2.0, 1.0, {31.0, 72.0}});
    std::shared_ptr< gerastyonok::Shape > ptr3 =
        std::make_shared<gerastyonok::Circle>(5.0, gerastyonok::point_t{0.0, 0.0});
    std::shared_ptr< gerastyonok::Shape > ptr4 =
      std::make_shared<gerastyonok::Circle>(15.0, gerastyonok::point_t{4.0, 10.0});
    gerastyonok::CompositeShape testComp{ptr1};
    testComp.addShape(ptr2);
    gerastyonok::CompositeShape newComp(ptr3);
    newComp.addShape(ptr4);
    newComp = std::move(testComp);
    BOOST_CHECK_EQUAL(newComp[0],ptr1);
    BOOST_CHECK_EQUAL(newComp[1],ptr2);
  }

  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    std::shared_ptr<gerastyonok::Shape> rectangle1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{21.0, 10.0, {3.0, 7.0}});
    std::shared_ptr<gerastyonok::Shape> circle1 =
      std::make_shared<gerastyonok::Circle>(5.0, gerastyonok::point_t{0.0, 0.0});
    gerastyonok::CompositeShape testObject(rectangle1);
    testObject.addShape(circle1);
    double tmpHeight = testObject.getFrameRect().height;
    double tmpWidth = testObject.getFrameRect().width;
    double tmpArea = testObject.getArea();
    gerastyonok::point_t positionAfterMoving = {5.0, 5.0};
    testObject.move(positionAfterMoving);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.x, positionAfterMoving.x, POGR);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.y, positionAfterMoving.y, POGR);
    BOOST_CHECK_EQUAL(tmpHeight, testObject.getFrameRect().height);
    BOOST_CHECK_EQUAL(tmpWidth, testObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(tmpArea, testObject.getArea());
    BOOST_CHECK_CLOSE(tmpHeight, testObject.getFrameRect().height, POGR);
    BOOST_CHECK_CLOSE(tmpWidth, testObject.getFrameRect().width, POGR);
    BOOST_CHECK_CLOSE(tmpArea, testObject.getArea(), POGR);
  }

  BOOST_AUTO_TEST_CASE(MoveByAxes)
  {
    std::shared_ptr<gerastyonok::Shape> rectangle1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{2.0, 2.0, {2.0, 2.0}});
    std::shared_ptr<gerastyonok::Shape> circle1 =
      std::make_shared<gerastyonok::Circle>(1.0, gerastyonok::point_t{0.0, 0.0});
    gerastyonok::CompositeShape testObject(rectangle1);
    testObject.addShape(circle1);
    double tmpHeight = testObject.getFrameRect().height;
    double tmpWidth = testObject.getFrameRect().width;
    double tmpArea = testObject.getArea();
    gerastyonok::point_t positionAfterMoving = {4.0, 3.0};
    testObject.move(3.0, 2.0);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.x, positionAfterMoving.x, POGR);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.y, positionAfterMoving.y, POGR);
    BOOST_CHECK_EQUAL(tmpHeight, testObject.getFrameRect().height);
    BOOST_CHECK_EQUAL(tmpWidth, testObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(tmpArea, testObject.getArea());
  }

  BOOST_AUTO_TEST_CASE(InvalidParametrsScaling)
  {
    std::shared_ptr<gerastyonok::Shape> rectangle1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{10.0, 10.0, {2.0, 4.0}});
    gerastyonok::CompositeShape testObject(rectangle1);
    std::shared_ptr<gerastyonok::Shape> circle1 =
      std::make_shared<gerastyonok::Circle>(5.0, gerastyonok::point_t{0.0, 0.0});
    testObject.addShape(circle1);
    BOOST_CHECK_THROW(testObject.scale(-2.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(ScaleTest)
  {
    std::shared_ptr<gerastyonok::Shape> rectangle1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{10.0, 10.0, {2.0, 4.0}});
    std::shared_ptr<gerastyonok::Shape> circle1 =
      std::make_shared<gerastyonok::Circle>(5.0, gerastyonok::point_t{0.0, 0.0});;
    gerastyonok::CompositeShape testObject(rectangle1);
    testObject.addShape(circle1);
    double tmpArea = testObject.getArea();
    testObject.scale(2.0);
    BOOST_CHECK_CLOSE(2.0 * 2.0 * tmpArea, testObject.getArea(), POGR);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentAdd)
  {
    std::shared_ptr<gerastyonok::Shape> rectangle1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{10.0, 10.0, {2.0, 4.0}});
    gerastyonok::CompositeShape testObject(rectangle1);
    std::shared_ptr<gerastyonok::Shape> circle1 = nullptr;
    BOOST_CHECK_THROW(testObject.addShape(circle1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentConstructorTest)
  {
    std::shared_ptr<gerastyonok::Shape> rectangle1 = nullptr;
    BOOST_CHECK_THROW(gerastyonok::CompositeShape testObject(rectangle1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvariantCoordinatesTest)
  {
    std::shared_ptr<gerastyonok::Shape> rectangle1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{10.0, 10.0, {2.0, 4.0}});
    gerastyonok::CompositeShape testObject(rectangle1);
    double posX = testObject.getFrameRect().pos.x;
    double posY = testObject.getFrameRect().pos.y;
    testObject.scale(2.0);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.x, posX, POGR);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.y, posY, POGR);
  }

  BOOST_AUTO_TEST_CASE(GeneralPosition)
  {
    gerastyonok::point_t position = {2.0, 4.0};
    double width = 10.0, height = 10.0;
    std::shared_ptr<gerastyonok::Shape> rectangle1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::rectangle_t{width, height, position});
    gerastyonok::CompositeShape testObject(rectangle1);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.x, position.x, POGR);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().pos.y, position.y, POGR);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().width, width, POGR);
    BOOST_CHECK_CLOSE(testObject.getFrameRect().height, height, POGR);
  }

BOOST_AUTO_TEST_SUITE_END()
