#include <iostream>
#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace gerastyonok;

int main()
{
  try
  {
    std::shared_ptr<gerastyonok::Shape> rect1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::Rectangle({13.0, 10.0, {2.0, 5.0}}));
    std::shared_ptr<gerastyonok::Shape> circ1 =
      std::make_shared<gerastyonok::Circle>(gerastyonok::Circle(10.9, {0.0, 0.0}));
    std::shared_ptr<gerastyonok::Shape> tri1 =
      std::make_shared<gerastyonok::Triangle>(gerastyonok::Triangle({0.0, 0.0}, {3.0, 3.0}, {10.0, 0.1}));

    gerastyonok::Matrix matrix(circ1);
    matrix.addShape(rect1);
    matrix.addShape(tri1);
    std::cout << "Quantity of layers" << std::endl;
    std::cout << matrix.getLayersNumber();
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }

  catch (std::out_of_range & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  return 0;
}
