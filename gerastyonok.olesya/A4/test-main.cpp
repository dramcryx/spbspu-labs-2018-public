#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double POGR = 1e-3;

BOOST_AUTO_TEST_SUITE(RotateTests)

  BOOST_AUTO_TEST_CASE(RotateTestRectangle)
  {
    gerastyonok::Rectangle rect({ 3.0, 7.0, {0.0, 0.0}});
    gerastyonok::rectangle_t test = rect.getFrameRect();
    double tmpArea = rect.getArea();
    rect.rotate(180.0);
    BOOST_CHECK_CLOSE_FRACTION(test.width, rect.getFrameRect().width, POGR);
    BOOST_CHECK_CLOSE_FRACTION(test.height, rect.getFrameRect().height, POGR);
    BOOST_CHECK_CLOSE_FRACTION(rect.getArea(), tmpArea, POGR);
  }

 BOOST_AUTO_TEST_CASE(RotateTestCircle)
  {
    gerastyonok::Circle circle(3.0, {0.0, 0.0});
    gerastyonok::rectangle_t test = circle.getFrameRect();
    double tmpArea = circle.getArea();
    circle.rotate(180.0);
    BOOST_CHECK_CLOSE_FRACTION(test.width, circle.getFrameRect().width, POGR);
    BOOST_CHECK_CLOSE_FRACTION(test.height, circle.getFrameRect().height, POGR);
    BOOST_CHECK_CLOSE_FRACTION(circle.getArea(), tmpArea, POGR);
  }

 BOOST_AUTO_TEST_CASE(RotateTestTriangle)
  {
    gerastyonok::Triangle triangle({0.0, 0.0}, {3.0, 3.0}, {10.0, 3.0});
    gerastyonok::rectangle_t test = triangle.getFrameRect();
    double tmpArea = triangle.getArea();
    triangle.rotate(180.0);
    BOOST_CHECK_CLOSE_FRACTION(test.width, triangle.getFrameRect().width, POGR);
    BOOST_CHECK_CLOSE_FRACTION(test.height, triangle.getFrameRect().height, POGR);
    BOOST_CHECK_CLOSE_FRACTION(triangle.getArea(), tmpArea, POGR);
  }

 BOOST_AUTO_TEST_CASE(RotateTestCompositeShape)
  {
    std::shared_ptr<gerastyonok::Shape> rect1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::Rectangle({10.0, 10.0, {0.0, 0.0}}));
    gerastyonok::CompositeShape test(rect1);
    double tmpArea = test.getArea();
    gerastyonok::rectangle_t tmpTest = test.getFrameRect();
    test.rotate(180.0);
    BOOST_CHECK_CLOSE_FRACTION(tmpTest.width, test.getFrameRect().width, POGR);
    BOOST_CHECK_CLOSE_FRACTION(tmpTest.height, test.getFrameRect().height, POGR);
    BOOST_CHECK_CLOSE_FRACTION(test.getArea(), tmpArea, POGR);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTest)

  BOOST_AUTO_TEST_CASE(Matrix_Test)
  {
    gerastyonok::Circle circle( 3.0, { 0.0, 0.0 });
    gerastyonok::Rectangle rectangle1( {2.0, 2.0, { -2.0, 0.0 }} );
    gerastyonok::Rectangle rectangle2({ 6.0, 3.0, { 1.0, 1.0 }} );
    gerastyonok::Rectangle rectangle3( {4.0, 4.0, { 6.0, 6.0 } });
    gerastyonok::Triangle triangle({4.0, 3.0}, {5.0, 5.0}, {10.0, 3.0});

    std::shared_ptr<gerastyonok::Shape> circlePtr = std::make_shared<gerastyonok::Circle>(circle);
    std::shared_ptr<gerastyonok::Shape> rectanglePtr1 = std::make_shared<gerastyonok::Rectangle>(rectangle1);
    std::shared_ptr<gerastyonok::Shape> rectanglePtr2 = std::make_shared<gerastyonok::Rectangle>(rectangle2);
    std::shared_ptr<gerastyonok::Shape> rectanglePtr3 = std::make_shared<gerastyonok::Rectangle>(rectangle3);
    std::shared_ptr<gerastyonok::Shape> trianglePtr = std::make_shared<gerastyonok::Triangle>(triangle);

    gerastyonok::Matrix matrix(circlePtr);
    matrix.addShape(rectanglePtr1);
    matrix.addShape(rectanglePtr2);
    matrix.addShape(rectanglePtr3);
    matrix.addShape(trianglePtr);

    std::unique_ptr<std::shared_ptr<gerastyonok::Shape>[]> layer1 = matrix[0];
    std::unique_ptr<std::shared_ptr<gerastyonok::Shape>[]> layer2 = matrix[1];
    std::unique_ptr<std::shared_ptr<gerastyonok::Shape>[]> layer3 = matrix[2];

    BOOST_CHECK(layer1[0] == circlePtr);
    BOOST_CHECK(layer1[1] == rectanglePtr3);
    BOOST_CHECK(layer2[0] == rectanglePtr1);
    BOOST_CHECK(layer2[1] == trianglePtr);
    BOOST_CHECK(layer3[0] == rectanglePtr2);
  }


  BOOST_AUTO_TEST_CASE(CheckLayers)
  {
    std::shared_ptr<gerastyonok::Shape> circle1 =
      std::make_shared<gerastyonok::Circle>(gerastyonok::Circle( 3.0, {10.0, 10.0}));
    std::shared_ptr<gerastyonok::Shape> circle2 =
      std::make_shared<gerastyonok::Circle>(gerastyonok::Circle( 5.0,{3.0, 3.0}));
    std::shared_ptr<gerastyonok::Shape> rect1 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::Rectangle({3.0, 3.0, {0.0, 0.0}}));
    std::shared_ptr<gerastyonok::Shape> rect2 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::Rectangle({1.0, 1.0, {-7.0, -7.0}}));
    std::shared_ptr<gerastyonok::Shape> rect3 =
      std::make_shared<gerastyonok::Rectangle>(gerastyonok::Rectangle({1.0, 1.0, {0.0, 25.0}}));
    gerastyonok::Matrix matrix(circle1);
    matrix.addShape(circle2);
    matrix.addShape(rect1);
    matrix.addShape(rect2);
    matrix.addShape(rect3);
    size_t value = 2;
    BOOST_CHECK_EQUAL(value, matrix.getLayersNumber());
  }

BOOST_AUTO_TEST_SUITE_END()
