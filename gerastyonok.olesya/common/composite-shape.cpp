#include "composite-shape.hpp"
#include <cmath>
#include <stdexcept>
#include <iostream>


gerastyonok::CompositeShape::CompositeShape(const std::shared_ptr<Shape> & object) :
  mass_(nullptr),
  size_(0)
{
  if (object == nullptr)
  {
  throw std::invalid_argument("Invalid pointer");
  }
  addShape(object);
}

gerastyonok::CompositeShape::CompositeShape(const CompositeShape & object) :
  mass_(new std::shared_ptr<Shape>[object.size_]),
  size_(object.size_)
{
  for (size_t i = 0; i < object.size_; i++)
  {
  mass_[i] = object.mass_[i];
  }
}

gerastyonok::CompositeShape::CompositeShape(CompositeShape && object) :
  mass_(std::move(object.mass_)),
  size_(object.size_)
{
  object.size_ = 0;
  object.mass_.reset();
}

std::shared_ptr<gerastyonok::Shape> gerastyonok::CompositeShape::operator [] (const size_t index) const
{
  if ((index >= size_))
  {
    throw std::out_of_range("Index is out of range");
  }
  return mass_[index];
}

gerastyonok::CompositeShape & gerastyonok::CompositeShape::operator= (const gerastyonok::CompositeShape & object)
{
  if (this == &object)
  {
    return *this;
  }
  size_ = object.size_;
  std::unique_ptr <std::shared_ptr<gerastyonok::Shape>[]>
  newArray(new std::shared_ptr<gerastyonok::Shape>[size_]);
  for (size_t i = 0; i < size_; i++)
  {
    newArray[i] = object.mass_[i];
  }
  mass_.swap(newArray);
  return *this;
}


gerastyonok::CompositeShape & gerastyonok::CompositeShape::operator= (gerastyonok::CompositeShape && object)
{
  size_ = object.size_;
  mass_ = std::move(object.mass_);
  object.size_ = 0;
  return *this;
}

void gerastyonok::CompositeShape::addShape(const std::shared_ptr<Shape> & object)
{
  if (object == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }

  std::unique_ptr<std::shared_ptr<Shape>[]> newArray(new std::shared_ptr<Shape>[size_ + 1]);
  for (size_t i = 0; i < size_; i++)
  {
    newArray[i] = mass_[i];
  }
  newArray[size_] = object;
  size_++;
  mass_.swap(newArray);
}

void gerastyonok::CompositeShape::removeShape(const size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("Index is out of array size");
  }
  if ((size_ == 1) && (index == 0))
  {
    mass_.reset();
    size_ = 0;
    return;
  }
  std::unique_ptr<std::shared_ptr <Shape >[]> newArray(new std::shared_ptr<Shape>[size_ - 1]);
  for (size_t i = 0; i < index; i++)
  {
    newArray[i] = mass_[i];
  }
  for (size_t i = index; i < size_ - 1; ++i)
  {
    newArray[i] = mass_[i + 1];
  }
  mass_.swap(newArray);
  size_--;
}

gerastyonok::point_t gerastyonok::CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}

double gerastyonok::CompositeShape::getArea() const
{
  double commonArea = 0;
  for (size_t i = 0; i < size_; i++)
  {
    commonArea += mass_[i]->getArea();
  }
  return commonArea;
}

gerastyonok::rectangle_t gerastyonok::CompositeShape::getFrameRect() const
{
  if (size_ <= 0)
  {
  return { 0.0 , 0.0,{ 0.0 , 0.0 } };
  }

  gerastyonok::rectangle_t frameRect = mass_[0]->getFrameRect();
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;

  for (size_t i = 1; i < size_; i++)
  {
    frameRect = mass_[i]->getFrameRect();
    if ((frameRect.pos.x - frameRect.width / 2) < minX)
    {
      minX = frameRect.pos.x - frameRect.width / 2;
    }
    if ((frameRect.pos.x + frameRect.width / 2) > maxX)
    {
      maxX = frameRect.pos.x + frameRect.width / 2;
    }
    if ((frameRect.pos.y + frameRect.height / 2) > maxY)
    {
      maxY = frameRect.pos.y + frameRect.height / 2;
    }
    if ((frameRect.pos.y - frameRect.height / 2) < minY)
    {
      minY = frameRect.pos.y - frameRect.height / 2;
    }
  }
  return { (maxX - minX), (maxY - minY),{ (minX + (maxX - minX) / 2), (minY + (maxY - minY) / 2) } };
}

void gerastyonok::CompositeShape::move(const point_t & posTo)
{
  move(posTo.x - getFrameRect().pos.x, posTo.y - getFrameRect().pos.y);
}

void gerastyonok::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    mass_[i] -> move(dx, dy);
  }
}

void gerastyonok::CompositeShape::scale(double k)
{
  if (k < 0.0)
  {
    throw std::invalid_argument("Invalid k");
  }
  gerastyonok::point_t initialShapePos = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    gerastyonok::point_t shapePos = mass_[i] -> getFrameRect().pos;
    mass_[i]->move((k - 1) * (shapePos.x - initialShapePos.x),
      (k - 1)* (shapePos.y - initialShapePos.y));
    mass_[i] -> scale(k);
  }
}
void gerastyonok::CompositeShape::rotate(double alpha)
{
  double sinAlpha = sin(alpha * M_PI / 180);
  double cosAlpha = cos(alpha * M_PI / 180);
  gerastyonok::point_t center = getFrameRect().pos;
  for(size_t i = 0; i < size_; i++)
  {
    gerastyonok::point_t shapePos = mass_[i] -> getFrameRect().pos;
    double newX = center.x + (shapePos.x - center.x) * cosAlpha - (shapePos.y - center.y) * sinAlpha;
    double newY = center.y + (shapePos.y - center.y) * cosAlpha + (shapePos.x - center.x) * sinAlpha;
    mass_[i] -> move({newX, newY});
    mass_[i] -> rotate(alpha);
  }
}

size_t gerastyonok::CompositeShape::getSize() const
{
  return size_;
}
