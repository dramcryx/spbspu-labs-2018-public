#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace gerastyonok
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape(const std::shared_ptr<Shape> & object);
    CompositeShape(const CompositeShape & object);
    CompositeShape(CompositeShape && object);

    std::shared_ptr<Shape> operator [] (const size_t index) const;
    CompositeShape & operator= (const CompositeShape & object);
    CompositeShape & operator= (CompositeShape && object);

    void addShape(const std::shared_ptr<Shape> & object);
    void removeShape(const size_t index);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double px, double py) override;
    void move(const point_t & pos) override;
    void scale(double k) override;
    void rotate(double alpha) override;
    size_t getSize() const;
    point_t getCenter() const override;

    private:
    std::unique_ptr< std::shared_ptr<Shape>[]> mass_;
    size_t size_;
    };
}

#endif
