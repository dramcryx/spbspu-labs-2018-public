#include "rectangle.hpp"
#include <iostream>
#include <cmath>

using namespace gerastyonok;

gerastyonok::Rectangle::Rectangle(const rectangle_t & rect) :
  rect_(rect),
  angle_(0)
{
  if (rect.height < 0.0 || rect.width < 0.0)
  {
    throw std::invalid_argument("Error! Invalid height or width");
  }
  corner_[0] = {rect_.pos.x - rect_.width /2, rect_.pos.y - rect_.height / 2};
  corner_[1] = {rect_.pos.x - rect_.width /2, rect_.pos.y + rect_.height / 2};
  corner_[2] = {rect_.pos.x + rect_.width /2, rect_.pos.y + rect_.height / 2};
  corner_[3] = {rect_.pos.x + rect_.width /2, rect_.pos.y - rect_.height / 2};

}

double gerastyonok::Rectangle::getArea() const
{
  return rect_.height * rect_.width;
}

rectangle_t gerastyonok::Rectangle::getFrameRect() const
{
  double maxY = std::max(std::max(corner_[0].y, corner_[1].y),
    std::max(corner_[2].y, corner_[3].y));
  double maxX = std::max(std::max(corner_[0].x, corner_[1].x),
    std::max(corner_[2].x, corner_[3].x));
  double minY = std::min(std::min(corner_[0].y, corner_[1].y),
    std::max(corner_[2].y, corner_[3].y));
  double minX = std::min(std::min(corner_[0].x, corner_[1].x),
    std::max(corner_[2].x, corner_[3].x));
  return { (maxX - minX), (maxY - minY), { (minX + maxX) / 2, (minY + maxY) / 2 } };

}

void gerastyonok::Rectangle::move(const point_t & pos)
{
  rect_.pos = pos;
   for (size_t i = 0; i < 4; i++)
  {
    corner_[i].x += pos.x - rect_.pos.x;
    corner_[i].y += pos.y - rect_.pos.y;
  }

}

void gerastyonok::Rectangle::move(double px, double py)
{
  rect_.pos.x += px;
  rect_.pos.y += py;
  for (size_t i = 0; i < 4; i++)
  {
    corner_[i].x += px;
    corner_[i].y += py;
  }
}

void gerastyonok::Rectangle::scale(const double k)
{
  if (k < 0.0)
  {
    throw std::invalid_argument("Error!");
  }
  for (size_t i = 0; i < 4; i++)
  {
    corner_[i].x = (corner_[i].x - rect_.pos.x) * k + rect_.pos.x;
    corner_[i].y = (corner_[i].y - rect_.pos.y) * k + rect_.pos.y;
  }

  rect_.height *= k;
  rect_.width *= k;
}

point_t gerastyonok::Rectangle::getCenter() const
{
  return rect_.pos;
}

void gerastyonok::Rectangle::rotate(double alpha)
{
  double sinAlpha = sin(alpha * M_PI / 180);
  double cosAlpha = cos(alpha * M_PI / 180);
  gerastyonok::point_t center = getFrameRect().pos;
  for (size_t i = 0; i < 4; i++)
  {
    corner_[i].x = center.x + (corner_[i].x - center.x) * cosAlpha - (corner_[i].y - center.y) * sinAlpha;
    corner_[i].y = center.y + (corner_[i].y - center.y) * cosAlpha + (corner_[i].x - center.x) * sinAlpha;
  }
}
