#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <iostream>
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace gremiachenskii;

const double EPSILON = 1e-2;

BOOST_AUTO_TEST_SUITE(Rotation)

  BOOST_AUTO_TEST_CASE(RectangleRotation)
  {
    Rectangle rect({15.5, 15.5}, 50.0, 30.0);
    rectangle_t tmp = rect.getFrameRect();
    double tmpArea = rect.getArea();
    rect.rotate(90.0);

    BOOST_CHECK_CLOSE_FRACTION(tmp.pos.x, rect.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmp.pos.y, rect.getFrameRect().pos.y, EPSILON);

    BOOST_CHECK_CLOSE_FRACTION(tmp.width, rect.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmp.height, rect.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(rect.getArea(), tmpArea, EPSILON);
  }

 BOOST_AUTO_TEST_CASE(circRotation)
  {
    Circle circ({120.0, 30.0}, 10.0);
    rectangle_t tmp = circ.getFrameRect();
    double tmpArea = circ.getArea();

    circ.rotate(180.0);
    BOOST_CHECK_CLOSE_FRACTION(tmp.pos.x, circ.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmp.pos.y, circ.getFrameRect().pos.y, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmp.width, circ.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmp.height, circ.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(circ.getArea(), tmpArea, EPSILON);
  }


 BOOST_AUTO_TEST_CASE(CompositeShapeRotation)
  {
    std::shared_ptr<Shape> rect1 =
      std::make_shared<Rectangle>(Rectangle( {10.0, 10.0}, 15.0, 15.0));
    CompositeShape test(rect1);
    double tmpArea = test.getArea();
    rectangle_t tmp = test.getFrameRect();
    test.rotate(90.0);

    BOOST_CHECK_CLOSE_FRACTION(tmp.pos.x, test.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmp.pos.y, test.getFrameRect().pos.y, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmp.width, test.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmp.height, test.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(test.getArea(), tmpArea, EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTest)

  BOOST_AUTO_TEST_CASE(Matrix_Test)
  {
    Circle circle( { -2.0, -2.0 }, 2.0 );
    Rectangle rectangle1( { -2.0, 0.0 }, 2.0, 2.0 );
    Rectangle rectangle2( { 1.0, 1.0 },  6.0, 3.0 );
    Rectangle rectangle3( { 3.0, 1.0 }, 2.0, 4.0);
    Rectangle rectangle4( { 3.0, 3.0 }, 4.0, 4.0);

    std::shared_ptr<Shape> circlePtr = std::make_shared<Circle>(circle);
    std::shared_ptr<Shape> rectanglePtr1 = std::make_shared<Rectangle>(rectangle1);
    std::shared_ptr<Shape> rectanglePtr2 = std::make_shared<Rectangle>(rectangle2);
    std::shared_ptr<Shape> rectanglePtr3 = std::make_shared<Rectangle>(rectangle3);
    std::shared_ptr<Shape> rectanglePtr4 = std::make_shared<Rectangle>(rectangle4);

    Matrix matrix(circlePtr);
    matrix.addElement(rectanglePtr1);
    matrix.addElement(rectanglePtr2);
    matrix.addElement(rectanglePtr3);
    matrix.addElement(rectanglePtr4);

    std::unique_ptr<std::shared_ptr<Shape>[]> layer1 = matrix[0];
    std::unique_ptr<std::shared_ptr<Shape>[]> layer2 = matrix[1];
    std::unique_ptr<std::shared_ptr<Shape>[]> layer3 = matrix[2];

    BOOST_CHECK(layer1[0] == circlePtr);
    BOOST_CHECK(layer1[1] == rectanglePtr3);
    BOOST_CHECK(layer2[0] == rectanglePtr1);
  }

  BOOST_AUTO_TEST_CASE(CheckLayers)
  {
    std::shared_ptr<Shape> circ1 = std::make_shared<Circle>( Circle({3.0, 0.0}, 3.0) );
    std::shared_ptr<Shape> circ2 = std::make_shared<Circle>( Circle({5.0, 5.0}, 5.0) );
    std::shared_ptr<Shape> rect1 = std::make_shared<Rectangle>( Rectangle( {8.0, 4.0}, 10.0, 4.0 ) );
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>( Rectangle( {8.0, -5.0}, 6.0, 2.0 ) );
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>( Rectangle( {2.0, 2.0}, 10.0, 40.0 ) );
    
    Matrix matrix(circ1);
    matrix.addElement(circ2);
    matrix.addElement(rect1);
    matrix.addElement(rect2);
    
    matrix.addElement(rect3);
    size_t quantity = 4;
    BOOST_CHECK_EQUAL(quantity, matrix.getLayersCount());
  }

BOOST_AUTO_TEST_SUITE_END()
