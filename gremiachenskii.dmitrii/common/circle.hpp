#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace gremiachenskii
{
  class Circle : public Shape
  {
  public:
    Circle(const point_t & pos, double rad);
    void move(const point_t & pos) noexcept override;
    void move(double dx, double dy) noexcept override;
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void scale(double ratio) override;
    void rotate(double alpha) noexcept override;

  private:
    point_t center_;
    double radius_;
    double angle_;
  };
}

#endif // CIRCLE_HPP
