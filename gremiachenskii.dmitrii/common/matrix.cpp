#include "matrix.hpp"
#include <stdexcept>

using namespace gremiachenskii;

Matrix::Matrix(const std::shared_ptr<Shape> rhs):
  matrix_(nullptr),
  numberOfLayers_(0),
  numberOfElements_(0)
{
  if (rhs == nullptr)
  {
    throw std::invalid_argument("ERROR: Invalid argument");
  }
  addElement(rhs);
}

Matrix::Matrix(const Matrix & rhs):
  matrix_(new std::shared_ptr<Shape>[rhs.numberOfLayers_ * rhs.numberOfElements_]),
  numberOfLayers_(rhs.numberOfLayers_),
  numberOfElements_(rhs.numberOfElements_)
{
  for(size_t i = 0; i < numberOfElements_ * numberOfLayers_; i++)
  {
    matrix_[i] = rhs.matrix_[i];
  }
}

Matrix::Matrix(Matrix && rhs):
  matrix_(nullptr),
  numberOfLayers_(rhs.numberOfLayers_),
  numberOfElements_(rhs.numberOfElements_)
{
  matrix_.swap(rhs.matrix_);
  rhs.numberOfElements_ = 0;
  rhs.numberOfLayers_ = 0;
}

Matrix::~Matrix()
{
  matrix_.reset();
  matrix_ = nullptr;
  numberOfElements_ = 0;
  numberOfLayers_ = 0;
}

Matrix & Matrix::operator=(const Matrix & rhs)
{
  if (this == &rhs)
  {
    return *this;
  }

  std::unique_ptr<std::shared_ptr<Shape> []> tmpMatrix(
    new std::shared_ptr<Shape> [rhs.numberOfElements_ * rhs.numberOfLayers_]);

  numberOfLayers_ = rhs.numberOfLayers_;
  numberOfElements_ = rhs.numberOfElements_;

  for(size_t i = 0; i < numberOfLayers_ * numberOfElements_; i++)
  {
    tmpMatrix[i] = rhs.matrix_[i];
  }

  matrix_.swap(tmpMatrix);

  return *this;
}

bool Matrix::operator==(const Matrix & rhs) const
{
  if ((numberOfElements_ == rhs.numberOfElements_) && (numberOfLayers_ == rhs.numberOfLayers_))
  {
    for (size_t i = 0; i < numberOfElements_ * numberOfLayers_; i++)
    {
      if (matrix_[i] != rhs.matrix_[i])
      {
        return false;
      }
    }
    return true;
  }

  return false;
}

bool Matrix::operator !=(const Matrix & rhs) const
{
  if ((numberOfElements_ != rhs.numberOfElements_) || (numberOfLayers_ != rhs.numberOfLayers_))
  {
    return true;
  }
  else
  {
    for (size_t i = 0; i < numberOfElements_ * numberOfLayers_; i++)
    {
      if (matrix_[i] != rhs.matrix_[i])
      {
        return true;
      }
    }
    return false;
  }
}

Matrix & Matrix::operator =(Matrix && rhs)
{
  if (this != &rhs)
  {
    numberOfElements_ = rhs.numberOfElements_;
    numberOfLayers_ = rhs.numberOfLayers_;
    matrix_.reset();
    matrix_.swap(rhs.matrix_);
    rhs.numberOfElements_ = 0;
    rhs.numberOfLayers_ = 0;
  }

  return *this;
}

std::unique_ptr<std::shared_ptr<Shape>[]> Matrix::operator[](size_t index) const
{
  if (index >= numberOfLayers_)
  {
    throw std::out_of_range("Invalid index");
  }

  std::unique_ptr<std::shared_ptr<Shape>[]> layer(
    new std::shared_ptr<Shape>[numberOfElements_]);
    
  for (size_t i = 0; i < numberOfElements_; ++i)
  {
    layer[i] = matrix_[index * numberOfElements_ + i];
  }
  return layer;
}

void Matrix::addElement(const std::shared_ptr<Shape> object)
{
  if (object == nullptr)
  {
    throw std::invalid_argument("ERROR: Invalid argument");
  }
  
  if (numberOfLayers_ == 0)
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> newMatrix(
      new std::shared_ptr<Shape>[(numberOfLayers_ + 1) * (numberOfElements_ + 1)]);
    numberOfElements_++;
    numberOfLayers_++;
    matrix_.swap(newMatrix);
    matrix_[0] = object;
  }
  else
  {
    bool addedShape = false;
    for (unsigned int i = 0; !addedShape ; ++i)
    {
      for (unsigned int j = 0; j < numberOfElements_; ++j)
      {
        if (!matrix_[i * numberOfElements_ + j])
        {
          matrix_[i * numberOfElements_ + j] = object;
          addedShape = true;
          break;
        }
        else
        {
          if (check(i * numberOfElements_ + j, object))
          {
            break;
          }
        }

        if (j == (numberOfElements_ - 1))
        {
          std::unique_ptr<std::shared_ptr<Shape>[]> newMatrix(
            new std::shared_ptr<Shape>[numberOfLayers_ * (numberOfElements_ + 1)]);
          numberOfElements_++;
          for (unsigned int n = 0; n < numberOfLayers_; ++n)
          {
            for (unsigned int m = 0; m < numberOfElements_ - 1; ++m)
            {
              newMatrix[n * numberOfElements_ + m] = matrix_[n * (numberOfElements_ - 1) + m];
            }
            newMatrix[(n + 1) * numberOfElements_ - 1] = nullptr;
          }
          newMatrix[(i + 1) * numberOfElements_ - 1] = object;
          matrix_.swap(newMatrix);
          addedShape = true;
          break;
        }
      }
      if ((i == (numberOfLayers_ - 1)) && !addedShape)
      {
        std::unique_ptr<std::shared_ptr<Shape>[]> newMatrix(
          new std::shared_ptr<Shape>[(numberOfLayers_ + 1) * numberOfElements_]);
        numberOfLayers_++;
        for (unsigned int n = 0; n < ((numberOfLayers_ - 1) * numberOfElements_); ++n)
        {
          newMatrix[n] = matrix_[n];
        }
        for (unsigned int n = ((numberOfLayers_ - 1) * numberOfElements_) ; n < (numberOfLayers_ * numberOfElements_); ++n)
        {
          newMatrix[n] = nullptr;
        }
        newMatrix[(numberOfLayers_ - 1) * numberOfElements_ ] = object;
        matrix_.swap(newMatrix);
        addedShape = true;
      }
    }
  }
}

bool Matrix::check(size_t index, const std::shared_ptr<Shape> rhs) const noexcept
{
  rectangle_t newFrameRect = rhs->getFrameRect();
  rectangle_t oldFrameRect = matrix_[index]->getFrameRect();

  //The OY axis aims up
  //Points are numerated clockwise from the top-left one
  point_t newPoints[4] =
  {
    {newFrameRect.pos.x - newFrameRect.width / 2, newFrameRect.pos.y + newFrameRect.height / 2},
    {newFrameRect.pos.x + newFrameRect.width / 2, newFrameRect.pos.y + newFrameRect.height / 2},
    {newFrameRect.pos.x + newFrameRect.width / 2, newFrameRect.pos.y - newFrameRect.height / 2},
    {newFrameRect.pos.x - newFrameRect.width / 2, newFrameRect.pos.y - newFrameRect.height / 2}
  };

  point_t oldPoints[4] =
  {
    {oldFrameRect.pos.x - oldFrameRect.width / 2, oldFrameRect.pos.y + oldFrameRect.height / 2},
    {oldFrameRect.pos.x + oldFrameRect.width / 2, oldFrameRect.pos.y + oldFrameRect.height / 2},
    {oldFrameRect.pos.x + oldFrameRect.width / 2, oldFrameRect.pos.y - oldFrameRect.height / 2},
    {oldFrameRect.pos.x - oldFrameRect.width / 2, oldFrameRect.pos.y - oldFrameRect.height / 2}
  };

  //At least one of FrameRect points is inside another FrameRect
  for(short int i = 0; i < 4; i++)
  {
    //For newFrameRect
    if ((newPoints[i].x >= oldPoints[0].x) && (newPoints[i].x <= oldPoints[2].x)
        && (newPoints[i].y >= oldPoints[3].y) && (newPoints[i].y <= oldPoints[1].y))
    {
      return true;
    }

    //For oldFrameRect
    for(short int i = 0; i < 4; i++)
    {
      if ((oldPoints[i].x >= newPoints[0].x) && (oldPoints[i].x <= newPoints[2].x)
          && (oldPoints[i].y >= newPoints[3].y) && (oldPoints[i].y <= newPoints[1].x))
      {
        return true;
      }
    }
  }

  //X-style intersection
  //New FrameRect is vertical
  if ((newPoints[0].x >= oldPoints[0].x) && (newPoints[0].y >= oldPoints[0].y)
      && (newPoints[2].x <= oldPoints[2].x) && (newPoints[2].y <= oldPoints[2].y))
  {
    return true;
  }

  //New FrameRect is horizontal
  if ((newPoints[0].x <= oldPoints[0].x) && (newPoints[0].y <= oldPoints[0].y)
      && (newPoints[2].x >= oldPoints[2].x) && (newPoints[2].y >= oldPoints[2].y))
  {
    return true;
  }

  return false;
}

size_t Matrix::getLayersCount() const noexcept
{
  return numberOfLayers_;
}
