#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace gremiachenskii
{
  class Matrix
  {
  public:
    Matrix(const std::shared_ptr<Shape> rhs);
    Matrix(const Matrix & rhs);
    Matrix(Matrix && rhs);
    ~Matrix();

    Matrix & operator=(const Matrix & rhs);
    Matrix & operator=(Matrix && rhs);
    std::unique_ptr<std::shared_ptr<Shape> []> operator[](size_t index) const;
    bool operator==(const Matrix & rhs) const;
    bool operator!=(const Matrix & rhs) const;

    size_t getLayersCount() const noexcept;
    void addElement(const std::shared_ptr<Shape> rhs);

  private:
    std::unique_ptr<std::shared_ptr<Shape> []> matrix_;
    size_t numberOfLayers_;
    size_t numberOfElements_;

    bool check(size_t index, const std::shared_ptr<Shape> rhs) const noexcept;
  };
}

#endif // MATRIX_HPP
