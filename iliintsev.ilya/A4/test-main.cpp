#define BOOST_TEST_MAIN

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include <boost/test/included/unit_test.hpp>

const double EPSILON = 1e-6;

BOOST_AUTO_TEST_SUITE(CompositeShape_Test)

  BOOST_AUTO_TEST_CASE(Constructorr)
    {
     BOOST_CHECK_THROW(iliintsev::CompositeShape testCompositeShape(nullptr), std::invalid_argument);
    }

  BOOST_AUTO_TEST_CASE(Constructor_Copying)
    {
      iliintsev::Rectangle rect1({1, 1}, 2, 2);
      iliintsev::Rectangle rect2( {5, 5}, 3, 3);
      iliintsev::Circle circ({10, 10}, 4);
      iliintsev::Rectangle rect3({15, 15}, 4, 4);
    
      std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
      std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
      std::shared_ptr<iliintsev::Shape> circ11 = std::make_shared<iliintsev::Circle>(circ);
      std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);
    
      iliintsev::CompositeShape testcompositeShape(rect11);

      testcompositeShape.add(rect22);
      testcompositeShape.add(circ11);
      testcompositeShape.add(rect33);

      iliintsev::CompositeShape testcompositeShape1(testcompositeShape);

      for (size_t i = 0; i < testcompositeShape.getSize(); ++i)
      {
        iliintsev::rectangle_t rectangle = testcompositeShape[i]->getFrameRect();
        iliintsev::rectangle_t rectangle1 = testcompositeShape1[i]->getFrameRect();

        BOOST_CHECK_CLOSE_FRACTION(rectangle.height, rectangle1.height,EPSILON);
        BOOST_CHECK_CLOSE_FRACTION(rectangle.width, rectangle1.width, EPSILON);
        BOOST_CHECK_CLOSE_FRACTION(rectangle.pos.x, rectangle1.pos.x, EPSILON);
        BOOST_CHECK_CLOSE_FRACTION(rectangle.pos.y, rectangle1.pos.y, EPSILON);
      }
    }

  BOOST_AUTO_TEST_CASE(Constructor_Moving)
    {
      iliintsev::Rectangle rect1({5, 6},3, 4);
      iliintsev::Rectangle rect2( {9, 1}, 7, 8);
      iliintsev::Circle circ({12, 13},11);
      iliintsev::Rectangle rect3({6, 7},4, 1);

      std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
      std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
      std::shared_ptr<iliintsev::Shape> circ11 = std::make_shared<iliintsev::Circle>(circ);
      std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);

      iliintsev::CompositeShape testcompositeShape(rect11);

      testcompositeShape.add(rect22);
      testcompositeShape.add(circ11);
      testcompositeShape.add(rect33);

      iliintsev::CompositeShape testcompositeShape1(testcompositeShape);
      iliintsev::CompositeShape testcompositeShape2(std::move(testcompositeShape));

      for (size_t i = 0; i < testcompositeShape2.getSize(); ++i) 
      {
        iliintsev::rectangle_t rectangle = testcompositeShape1[i]->getFrameRect();
        iliintsev::rectangle_t rectangle1 = testcompositeShape2[i]->getFrameRect();

        BOOST_CHECK_CLOSE_FRACTION(rectangle.height, rectangle1.height,EPSILON);
        BOOST_CHECK_CLOSE_FRACTION(rectangle.width, rectangle1.width, EPSILON);
        BOOST_CHECK_CLOSE_FRACTION(rectangle.pos.x, rectangle1.pos.x, EPSILON);
        BOOST_CHECK_CLOSE_FRACTION(rectangle.pos.y, rectangle1.pos.y, EPSILON);
      }
    }

  BOOST_AUTO_TEST_CASE(Assignment_Copy)
  {
    iliintsev::Rectangle rect1({5, 6},3, 4);
    iliintsev::Rectangle rect2( {9, 1}, 7, 8);
    iliintsev::Circle circ({12, 13},11);
    iliintsev::Rectangle rect3({6, 7},4, 1);

    std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
    std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
    std::shared_ptr<iliintsev::Shape> circ11 = std::make_shared<iliintsev::Circle>(circ);
    std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);

    iliintsev::CompositeShape testcompositeShape(rect11);

    testcompositeShape.add(rect22);
    testcompositeShape.add(circ11);
    testcompositeShape.add(rect33);

    iliintsev::CompositeShape testcompositeShape1(rect11);
    testcompositeShape1 = testcompositeShape;

    for (size_t i = 0; i < testcompositeShape.getSize(); ++i)
    {
      iliintsev::rectangle_t rectangle = testcompositeShape[i]->getFrameRect();
      iliintsev::rectangle_t rectangle1 = testcompositeShape1[i]->getFrameRect();

      BOOST_CHECK_EQUAL(rectangle.height, rectangle1.height);
      BOOST_CHECK_EQUAL(rectangle.width, rectangle1.width);
      BOOST_CHECK_EQUAL(rectangle.pos.x, rectangle1.pos.x);
      BOOST_CHECK_EQUAL(rectangle.pos.y, rectangle1.pos.y);
    }
  }

  BOOST_AUTO_TEST_CASE(Move_Test)
  {
    iliintsev::Rectangle rect1({1.5, 1.5},1, 1);
    iliintsev::Rectangle rect2({2.5, 2.5},1, 1);
    iliintsev::Rectangle rect3({3.5, 3.5},1, 1);

    std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
    std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
    std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);

    iliintsev::CompositeShape testCompositeShape(rect11);

    testCompositeShape.add(rect22);
    testCompositeShape.add(rect33);

    testCompositeShape.move({15, 10});

    BOOST_CHECK_CLOSE_FRACTION(testCompositeShape.getFrameRect().pos.x, 15, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testCompositeShape.getFrameRect().pos.y, 10, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Relative_Move_Test_CS)
  {
    iliintsev::Rectangle rect1({1.5, 1.5},1, 1);
    iliintsev::Rectangle rect2({2.5, 2.5},1, 1);
    iliintsev::Rectangle rect3({3.5, 3.5},1, 1);

    std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
    std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
    std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);

    iliintsev::CompositeShape testcompositeShape(rect11);
    testcompositeShape.add(rect22);
    testcompositeShape.add(rect33);

    testcompositeShape.move(-1, 1);

    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.x, 1.5, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.y, 3.5, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Area_Test_CS)
  {
    iliintsev::Rectangle rect1({1.5, 1.5},1, 1);
    iliintsev::Rectangle rect2({2.5, 2.5},1, 1);
    iliintsev::Rectangle rect3({3.5, 3.5},1, 1);

    std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
    std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
    std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);

    iliintsev::CompositeShape testcompositeShape(rect11);

    testcompositeShape.add(rect22);
    testcompositeShape.add(rect33);

    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getArea(), 3, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Area_Test_Move_CS)
  {
    iliintsev::Rectangle rect1({1.5, 1.5},1, 1);
    iliintsev::Rectangle rect2({2.5, 2.5},1, 1);
    iliintsev::Rectangle rect3({3.5, 3.5},1, 1);

    std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
    std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
    std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);

    iliintsev::CompositeShape testcompositeShape(rect11);

    testcompositeShape.add(rect22);
    testcompositeShape.add(rect33);

    testcompositeShape.move(5, 5);

    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getArea(), 3, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Frame_Rect_Test_CS)
  {
    iliintsev::Rectangle rect1({1.5, 1.5},1, 1);
    iliintsev::Rectangle rect2({2.5, 2.5},1, 1);
    iliintsev::Rectangle rect3({3.5, 3.5},1, 1);

    std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
    std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
    std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);

    iliintsev::CompositeShape testcompositeShape(rect11);

    testcompositeShape.add(rect22);
    testcompositeShape.add(rect33);

    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.x, 2.5, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.y, 2.5, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().width, 3, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().height, 3, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Scale_Test_CS)
  {
    iliintsev::Rectangle rect1({1.5, 1.5},1, 1);
    iliintsev::Rectangle rect2({2.5, 2.5},1, 1);
    iliintsev::Rectangle rect3({3.5, 3.5},1, 1);

    std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
    std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
    std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);

    iliintsev::CompositeShape testcompositeShape(rect11);

    testcompositeShape.add(rect22);
    testcompositeShape.add(rect33);

    testcompositeShape.scale(4);

    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getArea(), 48, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.x, 2.5, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.y, 2.5, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().width, 12, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().height, 12, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Remove_Element_Test_CS)
  {
    iliintsev::Rectangle rect1({1.5, 1.5},1, 1);
    iliintsev::Rectangle rect2({2.5, 2.5},1, 1);
    iliintsev::Rectangle rect3({3.5, 3.5},1, 1);

    std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
    std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
    std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);

    iliintsev::CompositeShape testcompositeShape(rect11);

    testcompositeShape.add(rect22);
    testcompositeShape.add(rect33);

    testcompositeShape.remove(0);

    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.x, 3, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.y, 3, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().width, 2, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().height, 2, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Argument_Scale_Test_CS)
  {
    iliintsev::Circle circ({0, 0},10);

    std::shared_ptr<iliintsev::Shape> circ1 = std::make_shared<iliintsev::Circle>(circ);

    iliintsev::CompositeShape testcompositeShape(circ1);

    BOOST_CHECK_THROW(testcompositeShape.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Argument_RemoveShape_Test_CS)
  {
    iliintsev::Circle circ({0, 0},10);

    std::shared_ptr<iliintsev::Shape> circ1 = std::make_shared<iliintsev::Circle>(circ);

    iliintsev::CompositeShape testcompositeShape(circ1);

    BOOST_CHECK_THROW(testcompositeShape.remove(2), std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(Size_Remove_Test_CS)
  {
    iliintsev::Circle circ1({0, 0},10);
    iliintsev::Circle circ2({10, 10},20);
    iliintsev::Circle circ3({20, 20},30);

    std::shared_ptr<iliintsev::Shape> circ11 = std::make_shared<iliintsev::Circle>(circ1);
    std::shared_ptr<iliintsev::Shape> circ22 = std::make_shared<iliintsev::Circle>(circ2);
    std::shared_ptr<iliintsev::Shape> circ33 = std::make_shared<iliintsev::Circle>(circ3);

    iliintsev::CompositeShape testcompositeShape(circ11);

    testcompositeShape.add(circ22);
    testcompositeShape.add(circ33);

    testcompositeShape.remove(2);

    BOOST_CHECK_EQUAL(testcompositeShape.getSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(Out_Of_Range_Error_Get_Element_Test_CS)
  {
    iliintsev::Circle circ({0, 0},10);

    std::shared_ptr<iliintsev::Shape> circ1 = std::make_shared<iliintsev::Circle>(circ);

    iliintsev::CompositeShape testcompositeShape(circ1);

    BOOST_CHECK_THROW(testcompositeShape[2], std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(rotate)
  {
    iliintsev::Rectangle rect1({5, 6},3, 4);
    iliintsev::Rectangle rect2({9, 1},7, 8);
    iliintsev::Circle circ({12, 13},11);
    iliintsev::Rectangle rect3({6, 7},4, 1);

    std::shared_ptr<iliintsev::Shape> rect11 = std::make_shared<iliintsev::Rectangle>(rect1);
    std::shared_ptr<iliintsev::Shape> rect22 = std::make_shared<iliintsev::Rectangle>(rect2);
    std::shared_ptr<iliintsev::Shape> circ11 = std::make_shared<iliintsev::Circle>(circ);
    std::shared_ptr<iliintsev::Shape> rect33 = std::make_shared<iliintsev::Rectangle>(rect3);

    iliintsev::CompositeShape testcompositeShape(rect11);

    testcompositeShape.add(rect22);
    testcompositeShape.add(circ11);
    testcompositeShape.add(rect33);

    iliintsev::CompositeShape testcompositeShape1(testcompositeShape);

    testcompositeShape1.rotate(70.0);

    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getArea(), testcompositeShape1.getArea(), EPSILON);

    for (int i = 0; i < 3; ++i) 
    {
      BOOST_CHECK_CLOSE_FRACTION(testcompositeShape[i]->getArea(), testcompositeShape1[i]->getArea(), EPSILON);
    }
  }

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(MatrixTests)

  BOOST_AUTO_TEST_CASE(Constructor1)
  {
    BOOST_CHECK_THROW(iliintsev::Matrix testMatrix(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Constructor2)
  {
    iliintsev::Circle testCircle({100.0, 0.0},10.0);

    std::shared_ptr<iliintsev::Shape> circle1 = std::make_shared<iliintsev::Circle>(testCircle);

    iliintsev::Matrix testMatrix(circle1);

    BOOST_CHECK_THROW(testMatrix[-1], std::invalid_argument);
  }


  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    iliintsev::Circle testCircleM({-10.0, 0.0},10.0);
    iliintsev::Rectangle testRectangleM1({20.0, 30.0},20.0, 40.0);
    iliintsev::Rectangle testRectangleM2({30.0, 0.0},20.0, 40.0);

    std::shared_ptr<iliintsev::Shape> circle1 = std::make_shared<iliintsev::Circle>(testCircleM);
    std::shared_ptr<iliintsev::Shape> rectangle1 = std::make_shared<iliintsev::Rectangle>(testRectangleM1);
    std::shared_ptr<iliintsev::Shape> rectangle2 = std::make_shared<iliintsev::Rectangle>(testRectangleM2);

    iliintsev::Matrix testMatrix1(circle1);

    testMatrix1.add(rectangle1);
    testMatrix1.add(rectangle2);

    iliintsev::Matrix testMatrix2(testMatrix1);

    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer0 = testMatrix2[0];
    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer1 = testMatrix2[1];

    BOOST_CHECK(layer0[0] == circle1);
    BOOST_CHECK(layer0[1] == rectangle1);
    BOOST_CHECK(layer1[0] == rectangle2);
    BOOST_CHECK_EQUAL(testMatrix2.getLayerNum(), 2);
    BOOST_CHECK_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    iliintsev::Circle testCircleM({-10.0, 0.0},10.0);
    iliintsev::Rectangle testRectangleM1({20.0, 30.0},20.0, 40.0);
    iliintsev::Rectangle testRectangleM2({30.0, 0.0},20.0, 40.0);

    std::shared_ptr<iliintsev::Shape> circle1 = std::make_shared<iliintsev::Circle>(testCircleM);
    std::shared_ptr<iliintsev::Shape> rectangle1 = std::make_shared<iliintsev::Rectangle>(testRectangleM1);
    std::shared_ptr<iliintsev::Shape> rectangle2 = std::make_shared<iliintsev::Rectangle>(testRectangleM2);

    iliintsev::Matrix testMatrix1(circle1);

    testMatrix1.add(rectangle1);
    testMatrix1.add(rectangle2);

    iliintsev::Matrix testMatrix2(std::move(testMatrix1));

    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer0 = testMatrix2[0];
    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer1 = testMatrix2[1];

    BOOST_CHECK(layer0[0] == circle1);
    BOOST_CHECK(layer0[1] == rectangle1);
    BOOST_CHECK(layer1[0] == rectangle2);
    BOOST_CHECK_EQUAL(testMatrix1.getLayerNum(), 0);
    BOOST_CHECK_EQUAL(testMatrix1.getLayerSize(), 0);
    BOOST_CHECK_EQUAL(testMatrix2.getLayerNum(), 2);
    BOOST_CHECK_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    iliintsev::Circle testCircleM2({40.0, 30.0},20.0);
    iliintsev::Circle testCircleM1({-10.0, 0.0},10.0);
    iliintsev::Rectangle testRectangleM1({20.0, 30.0},20.0, 40.0);
    iliintsev::Rectangle testRectangleM2({30.0, 0.0},20.0, 40.0);

    std::shared_ptr<iliintsev::Shape> circle1 = std::make_shared<iliintsev::Circle>(testCircleM1);
    std::shared_ptr<iliintsev::Shape> circle2 = std::make_shared<iliintsev::Circle>(testCircleM2);
    std::shared_ptr<iliintsev::Shape> rectangle1 = std::make_shared<iliintsev::Rectangle>(testRectangleM1);
    std::shared_ptr<iliintsev::Shape> rectangle2 = std::make_shared<iliintsev::Rectangle>(testRectangleM2);

    iliintsev::Matrix testMatrix1(circle1);

    testMatrix1.add(rectangle1);
    testMatrix1.add(rectangle2);

    iliintsev::Matrix testMatrix2(circle2);

    testMatrix2 = testMatrix1;

    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer0 = testMatrix2[0];
    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer1 = testMatrix2[1];

    BOOST_CHECK(layer0[0] == circle1);
    BOOST_CHECK(layer0[1] == rectangle1);
    BOOST_CHECK(layer1[0] == rectangle2);
    BOOST_CHECK_EQUAL(testMatrix2.getLayerNum(), 2);
    BOOST_CHECK_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    iliintsev::Circle testCircleM2({40.0, 30.0},20.0);
    iliintsev::Circle testCircleM1({-10.0, 0.0},10.0);
    iliintsev::Rectangle testRectangleM1({20.0, 30.0},20.0, 40.0);
    iliintsev::Rectangle testRectangleM2({30.0, 0.0},20.0, 40.0);

    std::shared_ptr<iliintsev::Shape> circle1 = std::make_shared<iliintsev::Circle>(testCircleM1);
    std::shared_ptr<iliintsev::Shape> circle2 = std::make_shared<iliintsev::Circle>(testCircleM2);
    std::shared_ptr<iliintsev::Shape> rectangle1 = std::make_shared<iliintsev::Rectangle>(testRectangleM1);
    std::shared_ptr<iliintsev::Shape> rectangle2 = std::make_shared<iliintsev::Rectangle>(testRectangleM2);

    iliintsev::Matrix testMatrix1(circle1);

    testMatrix1.add(rectangle1);
    testMatrix1.add(rectangle2);

    iliintsev::Matrix testMatrix2(circle2);

    testMatrix2 = std::move(testMatrix1);

    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer0 = testMatrix2[0];
    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer1 = testMatrix2[1];

    BOOST_CHECK(layer0[0] == circle1);
    BOOST_CHECK(layer0[1] == rectangle1);
    BOOST_CHECK(layer1[0] == rectangle2);
    BOOST_REQUIRE_EQUAL(testMatrix1.getLayerNum(), 0);
    BOOST_REQUIRE_EQUAL(testMatrix1.getLayerSize(), 0);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerNum(), 2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(MatrixGetFramerectTest)
  {
    iliintsev::Circle testCircleM2({40.0, 30.0},20.0);
    iliintsev::Circle testCircleM1({-10.0, 0.0},10.0);
    iliintsev::Rectangle testRectangleM1({20.0, 30.0},20.0, 40.0);
    iliintsev::Rectangle testRectangleM2({30.0, 0.0},20.0, 40.0);

    std::shared_ptr<iliintsev::Shape> circle1 = std::make_shared<iliintsev::Circle>(testCircleM1);
    std::shared_ptr<iliintsev::Shape> circle2 = std::make_shared<iliintsev::Circle>(testCircleM2);
    std::shared_ptr<iliintsev::Shape> rectangle1 = std::make_shared<iliintsev::Rectangle>(testRectangleM1);
    std::shared_ptr<iliintsev::Shape> rectangle2 = std::make_shared<iliintsev::Rectangle>(testRectangleM2);

    iliintsev::Matrix testMatrix(circle1);

    testMatrix.add(rectangle1);
    testMatrix.add(rectangle2);
    testMatrix.add(circle2);

    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer0 = testMatrix[0];
    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer1 = testMatrix[1];
    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer2 = testMatrix[2];

    BOOST_CHECK(layer0[0] == circle1);
    BOOST_CHECK(layer0[1] == rectangle1);
    BOOST_CHECK(layer1[0] == rectangle2);
    BOOST_CHECK(layer1[1] == nullptr);
    BOOST_CHECK(layer2[0] == circle2);
    BOOST_CHECK(layer2[1] == nullptr);
    BOOST_CHECK_CLOSE_FRACTION(layer0[0]->getFrameRect().pos.x, -10.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer0[1]->getFrameRect().pos.x, 20.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer1[0]->getFrameRect().pos.x, 30.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer2[0]->getFrameRect().pos.x, 40.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer0[0]->getFrameRect().pos.y, 0.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer0[1]->getFrameRect().pos.y, 30.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer1[0]->getFrameRect().pos.y, 0.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(layer2[0]->getFrameRect().pos.y, 30.0, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(addCs)
  {
    iliintsev::Circle testCircleM2({40.0, 30.0},20.0);
    iliintsev::Circle testCircleM1({-10.0, 0.0},10.0);
    iliintsev::Rectangle testRectangleM1({20.0, 30.0},20.0, 40.0);
    iliintsev::Rectangle testRectangleM2({30.0, 0.0},20.0, 40.0);

    std::shared_ptr<iliintsev::Shape> circle1 = std::make_shared<iliintsev::Circle>(testCircleM1);
    std::shared_ptr<iliintsev::Shape> circle2 = std::make_shared<iliintsev::Circle>(testCircleM2);
    std::shared_ptr<iliintsev::Shape> rectangle1 = std::make_shared<iliintsev::Rectangle>(testRectangleM1);
    std::shared_ptr<iliintsev::Shape> rectangle2 = std::make_shared<iliintsev::Rectangle>(testRectangleM2);

    iliintsev::CompositeShape testCompasiteShape(rectangle1);

    testCompasiteShape.add(rectangle2);
    testCompasiteShape.add(circle2);

    size_t size = testCompasiteShape.getSize();

    std::shared_ptr<iliintsev::CompositeShape> array = std::make_shared<iliintsev::CompositeShape>(testCompasiteShape);
    
    iliintsev::Matrix testMatrix(circle1);

    testMatrix.add(array, size);

    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer0 = testMatrix[0];
    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer1 = testMatrix[1];
    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> layer2 = testMatrix[2];

    BOOST_CHECK(layer0[0] == circle1);
    BOOST_CHECK(layer0[1] == rectangle1);
    BOOST_CHECK(layer1[0] == rectangle2);
    BOOST_CHECK(layer1[1] == nullptr);
    BOOST_CHECK(layer2[0] == circle2);
    BOOST_CHECK(layer2[1] == nullptr);
  }

BOOST_AUTO_TEST_SUITE_END()

