#include "circle.hpp"

#include <iostream>
#include <cmath>
#include <stdexcept>


iliintsev::Circle::Circle(const point_t & set_coordinate, double set_r) :
  center_(set_coordinate),
  r_(set_r)
{
  if (r_ < 0.0)
  {
    throw std::invalid_argument(" error, radius can't be < 0 ");
  }
}

double iliintsev::Circle::getArea() const noexcept
{
  return (M_PI * r_ * r_);
}

iliintsev::rectangle_t iliintsev::Circle::getFrameRect() const noexcept
{
  return {center_, r_ * 2, r_ * 2};
}

void iliintsev::Circle::move(const point_t & new_center) noexcept
{
  center_ = new_center;
}

void iliintsev::Circle::move(const double x_, const double y_) noexcept
{
  center_.x += x_;
  center_.y += y_;
}

void iliintsev::Circle::scale(const double koef)
{
  if (koef < 0.0)
  {
    throw std::invalid_argument(" error, koef can't be < 0 ");
  }
  r_ *= koef;
}

void iliintsev::Circle::rotate(const double /*degrees*/) noexcept
{
}

void iliintsev::Circle::printInfo() const noexcept
{
  std::cout << "Circle:  " << "  Radius - " << r_ << ";"<<std::endl;
  std::cout << "  Center - " << "x = " << center_.x << ", y = " << center_.y << ";"<<std::endl;
}
