#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace iliintsev
{
  class Circle : 
    public Shape
  {
  public:
    Circle(const point_t & set_coord, double set_r);

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t & new_center) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double koef) override;
    void rotate(const double degrees) noexcept override;
    void printInfo() const noexcept override;
  private:
    point_t center_;
    double r_;
  };
}

#endif
