#include "composite-shape.hpp"

#include <stdexcept>
#include <iostream>
#include <new>
#include <cmath>
#include <algorithm>

iliintsev::CompositeShape::CompositeShape():
  size_(0),
  shapes_(nullptr),
  angel_(0.0)
{
}

iliintsev::CompositeShape::CompositeShape(const std::shared_ptr<iliintsev::Shape> & shape):
  size_(0),
  shapes_(nullptr),
  angel_(0.0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("error, we can't add empty shape");
  }
  add(shape);
}

iliintsev::CompositeShape::CompositeShape(const iliintsev::CompositeShape & comp_shape):
  size_(0),
  shapes_(nullptr),
  angel_(0.0)
{
  std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> buff(new std::shared_ptr<iliintsev::Shape>[comp_shape.size_]);
  for(size_t i = 0; i < comp_shape.size_; ++i)
  {
    buff[i] = comp_shape.shapes_[i];  
  }
  size_ = comp_shape.size_;
  angel_ = comp_shape.angel_;
  shapes_ = std::move(buff);
}

iliintsev::CompositeShape::CompositeShape(iliintsev::CompositeShape && comp_shape):
  size_(comp_shape.size_),
  shapes_(std::move(comp_shape.shapes_)),
  angel_(comp_shape.angel_)
{
  comp_shape.angel_ = 0.0;
  comp_shape.shapes_ = (nullptr);
  comp_shape.size_ = 0;
}

iliintsev::CompositeShape::~CompositeShape()
{
  clear();
}

iliintsev::CompositeShape & iliintsev::CompositeShape::operator = (const iliintsev::CompositeShape & comp_shape)
{
  if (this == & comp_shape)
  {
    return * this;
  }
  shapes_.reset(new std::shared_ptr<Shape>[comp_shape.size_] );
  size_ = comp_shape.size_;
  angel_ = comp_shape.angel_;
  for (size_t i = 0; i < size_; ++i)
  {
    shapes_[i] = comp_shape.shapes_[i];
  }
  return * this;
}

iliintsev::CompositeShape & iliintsev::CompositeShape::operator = (iliintsev::CompositeShape && comp_shape)
{
  if (this == & comp_shape)
  {
    return * this;
  }
  size_ = comp_shape.size_;
  shapes_.reset(new std::shared_ptr<Shape>[size_]);
  angel_ = comp_shape.angel_;
  shapes_ = std::move(comp_shape.shapes_);
  comp_shape.shapes_ = (nullptr);
  comp_shape.size_ = 0;
  comp_shape.angel_ = 0.0;
  return * this;
}

std::shared_ptr<iliintsev::Shape> & iliintsev::CompositeShape::operator [](size_t index)
{
  if(size_==0)
  {
    throw std::out_of_range("error, CompositeShape is empty ");
  }
  if(index >= size_)
  {
    throw std::out_of_range("error, index can't be > Size ");
  }
  return shapes_[index];
}



double iliintsev::CompositeShape::getArea() const noexcept
{
  double totalArea = 0.0;
  for(size_t i = 0 ; i< size_ ; i++)
  {
    totalArea += shapes_[i]->getArea();
  }
  return totalArea;
}

iliintsev::rectangle_t iliintsev::CompositeShape::getFrameRect() const noexcept
{
    if (size_ == 0)
  {
    return rectangle_t{{0, 0}, 0, 0 };
  }
  rectangle_t buf_rect = shapes_[0]->getFrameRect();
  double left = buf_rect.pos.x - buf_rect.width / 2;
  double right = buf_rect.pos.x + buf_rect.width / 2;
  double top = buf_rect.pos.y + buf_rect.height / 2;
  double bottom = buf_rect.pos.y - buf_rect.height / 2;
  for (size_t i = 1; i < size_; i++)
  {
    buf_rect = shapes_[i]->getFrameRect();
    double buf_left = buf_rect.pos.x - buf_rect.width / 2;
    double buf_right = buf_rect.pos.x + buf_rect.width / 2;
    double buf_top = buf_rect.pos.y + buf_rect.height / 2;
    double buf_bottom = buf_rect.pos.y - buf_rect.height / 2;

    if (buf_left < left)
    {
      left = buf_left;
    }
    if (buf_right > right)
    {
      right = buf_right;
    }
    if (buf_top > top)
    {
      top = buf_top;
    }
    if (buf_bottom < bottom)
    {
      bottom = buf_bottom;
    }
  }
  const double sine = sin(angel_ * M_PI / 180);
  const double cosine = cos(angel_ * M_PI / 180);
  const double width = (top - bottom) * fabs(sine) + (right - left) * fabs(cosine);
  const double height = (top - bottom) * fabs(cosine) + (right - left) * fabs(sine);
  return {{ ((left + right) / 2.0), ((top + bottom) / 2.0) }, width, height};
  return rectangle_t{ { ((left + right) / 2.0), ((top + bottom) / 2.0) }, (right - left), (top - bottom) };
}

void iliintsev::CompositeShape::move(const iliintsev::point_t & new_center) noexcept
{
  const point_t center = getFrameRect().pos;
  double dx = new_center.x-center.x;
  double dy = new_center.y-center.y;
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx,dy);
  }
}

void iliintsev::CompositeShape::move(double dx, double dy) noexcept
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i]->move(dx, dy);
  }
}

void iliintsev::CompositeShape::scale(const double koef)
{
  if (koef < 0.0)
  {
    throw std::invalid_argument("Koef of scale can not be <= 0");
  }
  iliintsev::point_t comp_center = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    iliintsev::point_t shape_center = shapes_[i]->getFrameRect().pos;
    double dx = shape_center.x-comp_center.x;
    double dy = shape_center.y-comp_center.y;
    iliintsev::point_t new_center = {comp_center.x + koef * dx, comp_center.y + koef * dy};
    shapes_[i]->move(new_center);
    shapes_[i]->scale(koef);
  }
}

void iliintsev::CompositeShape::add(const std::shared_ptr<iliintsev::Shape> & shape)
{
  if (shape == nullptr) 
  {
    throw std::invalid_argument("Empty shape!");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]>buff;
  buff = std::move(shapes_);
  shapes_.reset(new std::shared_ptr<Shape >[size_ + 1]);
  shapes_[size_] = shape;
  size_++;
  for (size_t i = 0; i < size_ - 1; i++)
  {
    shapes_[i] = buff[i];
  }
}

void iliintsev::CompositeShape::remove(size_t index) 
{
  if (index >= size_)
  {
    throw std::out_of_range("error, index can't be > Size ");
  }
  for (size_t i = index; i <= size_ - 2; i++)
  {
    shapes_[i] = shapes_[i + 1];
  }
  shapes_[size_ - 1] = nullptr;
  size_--;
}

void iliintsev::CompositeShape::rotate(const double degrees) noexcept
{
  angel_ += degrees;
  const double sine = sin(degrees * M_PI / 180);
  const double cosine = cos(degrees * M_PI / 180);
  const iliintsev::point_t comp_center = getFrameRect().pos;
  for(size_t i=0;i<size_;++i)
  {
    const iliintsev::point_t shape_center = shapes_[i]->getFrameRect().pos;
    const double dx=(shape_center.x-comp_center.x) * cosine - (shape_center.y-comp_center.y) * sine;
    const double dy=(shape_center.y-comp_center.y) * cosine - (shape_center.x-comp_center.x) * sine;
    shapes_[i]->move(dx,dy);
    shapes_[i]->rotate(degrees);
  }
}

size_t iliintsev::CompositeShape::getSize() const noexcept
{
  return size_;
}

void iliintsev::CompositeShape::clear() noexcept
{
  size_ = 0;
  shapes_ = nullptr;
  angel_ = 0;
}

void iliintsev::CompositeShape::printInfo() const noexcept
{
  std::cout<<"------CompositeShape----------"<<std::endl;
  for(size_t i=0; i<size_;++i)
  {
    shapes_[i]->printInfo();
  }
  std::cout<<"-----------------------------"<<std::endl;
}
