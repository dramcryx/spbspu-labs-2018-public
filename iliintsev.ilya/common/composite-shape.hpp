#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace iliintsev
{
  class CompositeShape : 
    public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & comp_shape);
    CompositeShape(CompositeShape && comp_shape); 
    CompositeShape(const std::shared_ptr<Shape> & shape);
    ~CompositeShape();

    CompositeShape & operator = (const CompositeShape & comp_shape);
    CompositeShape & operator = (CompositeShape && comp_shape);
    std::shared_ptr<Shape> & operator[] (size_t index);

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t & new_center) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double koef) override;
    size_t getSize() const noexcept;
    void rotate(const double degrees) noexcept override;
    void printInfo() const noexcept override;

    void add(const std::shared_ptr< Shape > & shape);
    void remove(size_t index);
    void clear() noexcept;
    
  private:
    size_t size_;
    std::unique_ptr<std::shared_ptr<Shape>[]> shapes_;
    double angel_;
  };
}

#endif
