#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"
#include "composite-shape.hpp"

namespace iliintsev
{
  class Matrix
  {
  public:

    Matrix(const std::shared_ptr<iliintsev::Shape> & shape);
    Matrix(const Matrix & matrix);
    Matrix(Matrix && matrix);
    ~Matrix();

    Matrix & operator = (const Matrix & matrix);
    Matrix & operator = (Matrix && matrix);
    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> operator [](const size_t index) const;

    void add(const std::shared_ptr<iliintsev::Shape> & shape) noexcept;
    void add(const std::shared_ptr<iliintsev::CompositeShape> & shape, size_t size) noexcept;
    size_t getLayerNum() const noexcept;
    size_t getLayerSize() const noexcept;
    void printInfo() const noexcept;
  private:
    std::unique_ptr<std::shared_ptr<iliintsev::Shape>[]> matrix_;
    size_t layerNum_;
    size_t layerSize_;
      
    bool checkOverLapping(size_t index, const std::shared_ptr<iliintsev::Shape> &shape) const noexcept;
  };
}
#endif
