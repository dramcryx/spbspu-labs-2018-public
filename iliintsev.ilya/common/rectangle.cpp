#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>

iliintsev::Rectangle::Rectangle(const point_t & center, double width, double height) :
  center_(center),
  width_(width),
  height_(height),
  angel_(0.0)
{
  if (height_ < 0.0)
  {
    throw std::invalid_argument(" error, height can't be < 0 ");
  }

  if (width_ < 0.0)
  {
    throw std::invalid_argument(" error, weigth can't be < 0 ");
  }
}

double iliintsev::Rectangle::getArea() const noexcept
{
  return width_ * height_;
}

iliintsev::rectangle_t iliintsev::Rectangle::getFrameRect() const noexcept
{
  const double sine = sin(angel_ * M_PI / 180);
  const double cosine = cos(angel_ * M_PI / 180);
  const double width = height_ * fabs(sine) + width_ * fabs(cosine);
  const double height = height_ * fabs(cosine) + width_ * fabs(sine);
  return{center_, width, height};
}

void iliintsev::Rectangle::move(const point_t & new_center) noexcept
{
  center_ = new_center;
}

void iliintsev::Rectangle::move(const double dx, const double dy) noexcept
{
  center_.x += dx;
  center_.y += dy;
}

void iliintsev::Rectangle::scale(const double koef)
{
  if (koef < 0.0)
  {
    throw std::invalid_argument(" error, koef can't be < 0 ");
  }

  width_ *= koef;
  height_ *= koef;
}

void iliintsev::Rectangle::rotate(const double degrees) noexcept
{
  angel_ += degrees;
}

void iliintsev::Rectangle::printInfo() const noexcept
{
  std::cout << "Rectangle:  " << "  Width - " << width_ << ";  Height - " << height_ << ";"<<std::endl;
  std::cout << "  Center - x = " << center_.x << ", y = " << center_.y << ";"<<std::endl;
}
