#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace iliintsev
{
  class Rectangle :
   public Shape
  {
  public:
    Rectangle(const point_t & center, double width, double height);

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t & new_center) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double koef) override;
    void rotate(const double degrees) noexcept override;
    void printInfo() const noexcept override;
  private:
    point_t center_;
    double width_;
    double height_;
    double angel_;
  };
}

#endif
