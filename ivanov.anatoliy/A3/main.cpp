#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

using namespace ivanov;

void printShapeInfo (Shape & shape)
{
  shape.printCurrentInfo();
  std::cout << "Area:" << shape.getArea() << std::endl;
  std::cout << "FrameRect center (x,y): " << shape.getFrameRect().pos.x << ", " << shape.getFrameRect().pos.y << std::endl;
  std::cout << "FrameRect width:" << shape.getFrameRect().width << std::endl;
  std::cout << "FrameRect height:" << shape.getFrameRect().height << std::endl;
}

int main()
{
  try
  {
    Rectangle rect1({4.0,4.0},8.0,3.0);
    Rectangle rect2({11.0,12.0},6.0,1.0);
    Circle circ1({2.0,3.0},4.0);
    Circle circ2({7.0,6.0},3.0);
    Triangle trian1({1.0,3.0},{4.0,1.0},{6.0,4.0});
    Triangle trian2({5.0,1.0},{6.0,5.0},{6.0,2.0});

    std::cout << "ADD 6 SHAPES" << std::endl;
    CompositeShape compShape(&rect1);
    compShape.add(&rect2);
    compShape.add(&circ1);
    compShape.add(&circ2);
    compShape.add(&trian1);
    compShape.add(&trian2);
    compShape.printCurrentInfo();

    std::cout << "\nREMOVE 2 SHAPES" << std::endl;
    compShape.remove(1);
    compShape.remove(3);
    compShape.printCurrentInfo();

    std::cout << "\nMOVE TO POINT" << std::endl;
    compShape.move({10.0,6.0});
    compShape.printCurrentInfo();

    std::cout << "\nMOVE ON dx, dy" << std::endl;
    compShape.move(2.0,-3.0);
    compShape.printCurrentInfo();

    std::cout << "\nSCALE" << std::endl;
    compShape.scale(0.5);
    compShape.printCurrentInfo();

  }
  catch(std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }

  return 0;
}
