#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace ivanov;

int main()
{
  try
  {
    Rectangle testRect({4.0,4.0},8.0,3.0);
    Circle testCirc({2.0,3.0},4.0);
    Triangle testTrian({2.0,3.0},{4.0,1.0},{6.0,5.0});

    std::cout << "ROTATE SHAPES" << std::endl;
    testRect.printCurrentInfo();
    testRect.rotate(90);
    testRect.printCurrentInfo();
    testCirc.printCurrentInfo();
    testCirc.rotate(90);
    testCirc.printCurrentInfo();
    testTrian.printCurrentInfo();
    testTrian.rotate(90);
    testTrian.printCurrentInfo();

    Rectangle rect({4.0,4.0},8.0,3.0);
    Circle circ({2.0,3.0},4.0);
    Triangle trian({1.0,3.0},{4.0,1.0},{6.0,4.0});

    std::cout << "\nROTATE COMPOSITION" << std::endl;
    CompositeShape compShape(&rect);
    compShape.add(&circ);
    compShape.add(&trian);
    compShape.printCurrentInfo();
    compShape.rotate(90);
    compShape.printCurrentInfo();

    Rectangle rect1({2.0,2.0},4.0,1.0);
    Rectangle rect2({10.0,3.0},2.0,2.0);
    Circle circ1({8.0,6.0},3.0);
    Circle circ2({7.0,8.0},2.0);
    Triangle trian1({1.0,3.0},{4.0,1.0},{6.0,4.0});
    Matrix testMatr;

    std::cout << "\nMATRIX" << std::endl;
    testMatr.add(&rect1);
    testMatr.printCurrentInfo();
    testMatr.add(&circ1);
    testMatr.printCurrentInfo();
    testMatr.add(&compShape);
    testMatr.add(&trian1);
    testMatr.printCurrentInfo();
    testMatr.add(&circ2);
    testMatr.add(&rect2);
    testMatr.printCurrentInfo();
  }
  catch(std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }

  return 0;
}
