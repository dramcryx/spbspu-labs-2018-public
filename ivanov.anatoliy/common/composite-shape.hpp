#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace ivanov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(Shape *rhs);
    CompositeShape(const CompositeShape & rhs);
    CompositeShape(CompositeShape && rhs);

    CompositeShape &operator=(CompositeShape & rhs);
    CompositeShape &operator=(CompositeShape && rhs);
    Shape *operator[](size_t index) const;

    size_t getSize() const;
    void add(Shape *newShape);
    void remove(const size_t index);

    point_t getCenter() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & transferPoint) override;
    void move(const double dx, const double dy) override;
    void scale(const double scaleFactor) override;
    void rotate(const double angle) override;
    void printCurrentInfo() const override;

  private:
    std::unique_ptr<Shape *[]> shapes_;
    size_t size_;
  };
}

#endif // COMPOSITESHAPE_HPP
