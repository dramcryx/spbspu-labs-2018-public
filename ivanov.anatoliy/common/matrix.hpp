#ifndef MATRIX_HPP_INCLUDED
#define MATRIX_HPP_INCLUDED
#include <memory>
#include "shape.hpp"

namespace ivanov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix & rhs);
    Matrix(Matrix && rhs);

    Matrix &operator=(Matrix & rhs);
    Matrix &operator=(Matrix && rhs);
    std::unique_ptr<Shape *[]> operator[](size_t index) const;

    bool checkOverlapping(const Shape *shape1,const Shape *shape2);
    void add(Shape *newShape);
    void printCurrentInfo() const;
  private:
    std::unique_ptr<Shape *[]> shapes_;
    size_t line_;
    size_t column_;
  };
}

#endif // MATRIX_HPP_INCLUDED
