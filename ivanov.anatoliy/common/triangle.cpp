#include "triangle.hpp"
#include <iostream>
#include <cmath>
#include <cassert>

using namespace ivanov;

Triangle::Triangle(const point_t & vertexA, const point_t & vertexB, const point_t & vertexC):
  vertexA_(vertexA),
  vertexB_(vertexB),
  vertexC_(vertexC)
{
  center_.x = (vertexA_.x+vertexB_.x+vertexC_.x)/3.0;
  center_.y = (vertexA_.y+vertexB_.y+vertexC_.y)/3.0;
}

point_t Triangle::getCenter() const
{
  return center_;
}

double Triangle::getArea() const
{
  return std::abs((vertexA_.x-vertexC_.x)*(vertexB_.y-vertexC_.y)-(vertexB_.x-vertexC_.x)*(vertexA_.y-vertexC_.y))/2.0;
}

rectangle_t Triangle::getFrameRect() const
{
  double minx(std::min(std::min(vertexA_.x,vertexB_.x),vertexC_.x));
  double maxx(std::max(std::max(vertexA_.x,vertexB_.x),vertexC_.x));
  double miny(std::min(std::min(vertexA_.y,vertexB_.y),vertexC_.y));
  double maxy(std::max(std::max(vertexA_.y,vertexB_.y),vertexC_.y));
  return rectangle_t{{(minx+maxx)/2,(miny+maxy)/2},maxx-minx,maxy-miny};
}

void Triangle::move(const point_t & transferPoint)
{
  vertexA_.x += transferPoint.x - center_.x;
  vertexA_.y += transferPoint.y - center_.y;
  vertexB_.x += transferPoint.x - center_.x;
  vertexB_.y += transferPoint.y - center_.y;
  vertexC_.x += transferPoint.x - center_.x;
  vertexC_.y += transferPoint.y - center_.y;
  center_ = transferPoint;
}

void Triangle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
  vertexA_.x += dx;
  vertexA_.y += dy;
  vertexB_.x += dx;
  vertexB_.y += dy;
  vertexC_.x += dx;
  vertexC_.y += dy;
}

void Triangle::scale(const double scaleFactor)
{
  if (scaleFactor < 0.0)
  {
    throw std::invalid_argument("Invalid scale factor.");
  }
  vertexA_.x = center_.x+scaleFactor*(vertexA_.x-center_.x);
  vertexA_.y = center_.y+scaleFactor*(vertexA_.y-center_.y);
  vertexB_.x = center_.x+scaleFactor*(vertexB_.x-center_.x);
  vertexB_.y = center_.y+scaleFactor*(vertexB_.y-center_.y);
  vertexC_.x = center_.x+scaleFactor*(vertexC_.x-center_.x);
  vertexC_.y = center_.y+scaleFactor*(vertexC_.y-center_.y);
}

void Triangle::rotate(const double angle)
{
  double tmpAngle = (angle*M_PI)/180.0;
  point_t newVertexA;
  point_t newVertexB;
  point_t newVertexC;
  newVertexA.x = center_.x+(vertexA_.x-center_.x)*cos(tmpAngle)-(vertexA_.y-center_.y)*sin(tmpAngle);
  newVertexA.y = center_.y+(vertexA_.x-center_.x)*sin(tmpAngle)+(vertexA_.y-center_.y)*cos(tmpAngle);
  vertexA_ = newVertexA;
  newVertexB.x = center_.x+(vertexB_.x-center_.x)*cos(tmpAngle)-(vertexB_.y-center_.y)*sin(tmpAngle);
  newVertexB.y = center_.y+(vertexB_.x-center_.x)*sin(tmpAngle)+(vertexB_.y-center_.y)*cos(tmpAngle);
  vertexB_ = newVertexB;
  newVertexC.x = center_.x+(vertexC_.x-center_.x)*cos(tmpAngle)-(vertexC_.y-center_.y)*sin(tmpAngle);
  newVertexC.y = center_.y+(vertexC_.x-center_.x)*sin(tmpAngle)+(vertexC_.y-center_.y)*cos(tmpAngle);
  vertexC_ = newVertexC;
}

void Triangle::printCurrentInfo() const
{
  std::cout << "\nTriangle center (x,y): " << center_.x << ", " << center_.y << std::endl;
  std::cout << "Triangle vertex A (x,y): " << vertexA_.x << ", " << vertexA_.y << std::endl;
  std::cout << "Triangle vertex B (x,y): " << vertexB_.x << ", " << vertexB_.y << std::endl;
  std::cout << "Triangle vertex C (x,y): " << vertexC_.x << ", " << vertexC_.y << std::endl;
}
