#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>

using namespace ivanov;

const double EPS = 0.0001;

BOOST_AUTO_TEST_SUITE(RectangleTest)

  BOOST_AUTO_TEST_CASE(Rotate)
  {
    Rectangle rectangle1({0.0, 15.3, {78.6, 12.3}});
    Rectangle rectangle2(rectangle1);
    rectangle2.rotate(59.15);
    BOOST_CHECK_EQUAL(rectangle1.getFrameRect().pos.x, rectangle2.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(rectangle1.getFrameRect().pos.y, rectangle2.getFrameRect().pos.y);
    BOOST_CHECK_EQUAL(rectangle1.getArea(), rectangle2.getArea());
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CircleTest)

  BOOST_AUTO_TEST_CASE(Rotate)
  {
    Circle circle1({22, 12}, 23.1);
    Circle circle2({22, 12}, 23.1);
    circle1.rotate(90.0);
    BOOST_CHECK_EQUAL(circle1.getFrameRect().pos.x, circle2.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(circle1.getFrameRect().pos.y, circle2.getFrameRect().pos.y);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeTest)

  BOOST_AUTO_TEST_CASE(Rotate)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({5.0, 5.0, {0.0, 0.0}}));
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({78.6, 45.1}, 23.1));

    CompositeShape compositeshape1;
    compositeshape1.addShape(rectangle1);
    compositeshape1.addShape(circle1);

    CompositeShape compositeshape2;
    compositeshape2.addShape(rectangle1);
    compositeshape2.addShape(circle1);
    compositeshape2.rotate(180);

    BOOST_CHECK_EQUAL(compositeshape1.getFrameRect().pos.x, compositeshape2.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(compositeshape1.getFrameRect().pos.y, compositeshape2.getFrameRect().pos.y);
    BOOST_CHECK_EQUAL(compositeshape1.getArea(), compositeshape2.getArea());
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Matrix_Tests)

  BOOST_AUTO_TEST_CASE(TestNewLayer)
  {
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({0.2, 1.3}, 11.4));
    std::shared_ptr<Shape> circle2 = std::make_shared<Circle>(Circle({-0.1, 0.3}, 2.9));

    Matrix localMatrix;
    localMatrix.add(circle1);
    localMatrix.add(circle2);

    BOOST_CHECK(localMatrix[0][0] == circle1);
    BOOST_CHECK(localMatrix[1][0] == circle2);

  }

  BOOST_AUTO_TEST_CASE(InvalidAddShape)
  {
    Matrix localMatrix;
    BOOST_CHECK_THROW(localMatrix.add(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(AddShape)
  {
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({12.6, 56.3}, 78.3));
    Matrix localMatrix;
    localMatrix.add(circle1);
    BOOST_CHECK(localMatrix[0][0] == circle1);
  }

  BOOST_AUTO_TEST_CASE(CopyConstructor)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({12.1, 12.2, {1.3, 2.3}}));
    Matrix localMatrix1;
    localMatrix1.add(rect);
    Matrix localMatrix2(localMatrix1);
    BOOST_CHECK(localMatrix1[0][0]== localMatrix2[0][0]);
  }

  BOOST_AUTO_TEST_CASE(CopyOperator)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({2.23, 12.7, {12.1, 12.3}}));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<Rectangle>(Rectangle({2.4, 1.7, {1.1, 1.1}}));
    Matrix localMatrix1;
    localMatrix1.add(rectangle1);
    Matrix localMatrix2;
    localMatrix2.add(rectangle2);
    localMatrix2 = localMatrix1;
    BOOST_CHECK(localMatrix1[0][0] == localMatrix2[0][0]);
  }

  BOOST_AUTO_TEST_CASE(Intersection)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({11.0, 1.0, {0.0, 0.0}}));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<Rectangle>(Rectangle({10.0, 1.0, {1.0, 13.0}}));
    std::shared_ptr<Shape> rectangle3= std::make_shared<Rectangle>(Rectangle({10.0, 1.0, {1.0, 0.0}}));

    Matrix localMatrix;

    BOOST_CHECK(!localMatrix.checkIntersection(rectangle1 , rectangle2));
    BOOST_CHECK(localMatrix.checkIntersection(rectangle1 , rectangle3));
  }

  BOOST_AUTO_TEST_CASE(CheckCompositeShapeSplit)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({11.0, 1.0, {0.0, 0.0}}));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<Rectangle>(Rectangle({10.0, 1.0, {1.0, 13.0}}));
    std::shared_ptr<Shape> rectangle3 = std::make_shared<Rectangle>(Rectangle({10.0, 1.0, {1.0, 0.0}}));

    CompositeShape compositeshape1;

    compositeshape1.addShape(rectangle1);
    compositeshape1.addShape(rectangle2);
    compositeshape1.addShape(rectangle3);

    Matrix localMatrix = compositeshape1.split();

    BOOST_CHECK_EQUAL(localMatrix[0][0], rectangle1);
    BOOST_CHECK_EQUAL(localMatrix[0][1], rectangle2);
    BOOST_CHECK_EQUAL(localMatrix[1][0], rectangle3);
  }

BOOST_AUTO_TEST_SUITE_END()
