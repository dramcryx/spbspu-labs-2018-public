#include "circle.hpp"

#include <iostream>
#include <cmath>
#include <stdexcept>

ivanov::Circle::Circle(const ivanov::point_t & centre, const double radius):
  centre_(centre),
  radius_(radius)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument("Wrong RADIUS!");
  }
}

ivanov::rectangle_t ivanov::Circle::getFrameRect() const noexcept
{
  return ivanov::rectangle_t{(radius_ * 2.0), (radius_ * 2.0), centre_};
}

double ivanov::Circle::getArea() const noexcept
{
  return (M_PI * radius_ * radius_);
}

void ivanov::Circle::move(const ivanov::point_t &newCentrePoint) noexcept
{
  centre_ = newCentrePoint;
}

void ivanov::Circle::move(const double dx, const double dy) noexcept
{
  centre_.x += dx;
  centre_.y += dy;
}

void ivanov::Circle::scale(const double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Wrong RATIO!");
  }
  radius_ *= ratio;
}

void ivanov::Circle::rotate(const double) noexcept
{
}

void ivanov::Circle::printInfo() noexcept
{
  std::cout << "Circle" << std::endl;
  std::cout << "Radius >> " << radius_ << std::endl;
  std::cout << "Position >> " << centre_.x << ", " << centre_.y << std::endl;

  std::cout << "Frame width >> " << getFrameRect().width << std::endl;
  std::cout << "Frame height >> " << getFrameRect().height << std::endl;
  std::cout << "Frame position >> " << getFrameRect().pos.x << ", " << getFrameRect().pos.y << std::endl;
}
