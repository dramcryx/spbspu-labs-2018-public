#ifndef WORK_COMPOSITESHAPE_HPP
#define WORK_COMPOSITESHAPE_HPP

#include <memory>

#include "shape.hpp"
#include "matrix.hpp"

namespace ivanov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape() noexcept;

    CompositeShape(const std::shared_ptr <Shape> & newShape);
    CompositeShape(const CompositeShape & Obj);
    CompositeShape(CompositeShape && Obj) noexcept;

    CompositeShape & operator = (const CompositeShape & Obj);
    CompositeShape & operator = (CompositeShape && Obj) noexcept;
    ~CompositeShape() = default;

    std::shared_ptr<Shape> operator [] (const unsigned int index) const;

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t & newCentrePoint) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double ratio) override;
    void addShape(const std::shared_ptr<Shape> & newShape);
    void deleteShape(const unsigned int index);
    void printInfo() noexcept override;
    size_t getSize() const noexcept;
    void rotate(const double angle) noexcept override;

    Matrix split() const;

  private:
    size_t size_;
    std::unique_ptr<std::shared_ptr<Shape>[]> shapes_;

  };
}

#endif
