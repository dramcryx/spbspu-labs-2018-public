#ifndef WORK_RECTANGLE_HPP
#define WORK_RECTANGLE_HPP

#include "shape.hpp"

namespace ivanov
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const rectangle_t &rectangle);
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t & newCentrePoint) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double ratio) override;
    void rotate(const double angle) noexcept override;
    void printInfo() noexcept override;
  private:
    rectangle_t rectangle_;
    point_t vertices_[4];
  };
}

#endif
