#include <iostream>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using namespace ivanov;

int main ()
{
  try
  {
    Circle circle1{ { -2.0, -2.0 }, 1.0 };
    std::shared_ptr<Shape> circlePtr1 = std::make_shared<Circle>(circle1);
    CompositeShape compositeShape(circlePtr1);
    compositeShape.rotate(90);
    compositeShape.scale(2.0);
    compositeShape.deleteShape(1);
    compositeShape.clear();
    std::cout << std::endl;
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << "\n";
    return 1;
  }
  catch (std::out_of_range & error)
  {
    std::cerr << error.what() << "\n";
    return 1;
  }
  return 0;
}
