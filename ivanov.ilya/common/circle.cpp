#include "circle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

ivanov::Circle::Circle(const point_t & center, double r):
  pos_(center),
  radius_(r)
{
  if (r < 0.0)
  {
    throw std::invalid_argument("Raduis can`t be negative");
  }
}

double ivanov::Circle::getArea() const noexcept
{
  return M_PI*(radius_*radius_);
}

ivanov::rectangle_t ivanov::Circle::getFrameRect() const noexcept
{
  return rectangle_t{pos_, radius_*2, radius_*2};
}

void ivanov::Circle::move (const point_t & movement) noexcept
{
  pos_ = movement;
}

void ivanov::Circle::move (double dx, double dy) noexcept
{
  pos_.x += dx;
  pos_.y += dy;
}

void ivanov::Circle::scale (double koeff)
{
  if (koeff < 0.0)
  {
    throw std::invalid_argument("Negative koefficient");
  }
  radius_ *= koeff;
}

void ivanov::Circle::rotate (double angle) noexcept
{
  angle_ += angle;
}
