#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace ivanov
{
class Rectangle: public Shape
{
public:
  Rectangle(const rectangle_t & rect);
  double getArea() const noexcept override;
  rectangle_t getFrameRect() const noexcept override;
  void move(const point_t & p) noexcept override;
  void move(double dx, double dy) noexcept override;
  void scale(double koeff) override;
  void rotate(double angle) noexcept override;
private:
  rectangle_t bar_;
  double angle_;
};
}
#endif // RECTANGLE_HPP
