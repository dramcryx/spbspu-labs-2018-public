#include "matrix.hpp"
#include <iostream>
#include <memory>
#include <cmath>

ivanov::Matrix::Matrix(const std::shared_ptr<Shape> & Element):
  matrix_(new std::shared_ptr<Shape>[1]),
  rows_(0),
  columns_(0)
{
  if (Element == nullptr)
  {
    matrix_.reset();
    throw std::invalid_argument ("Error");
  }
  matrix_[0] = Element;
  rows_++;
  columns_++;
}

ivanov::Matrix::Matrix(const Matrix & elem):
  matrix_(new std::shared_ptr<Shape>[(elem.rows_ * elem.columns_)]),
  rows_(elem.rows_),
  columns_(elem.columns_)
{
  for (int i = 0; i < (rows_ * columns_); ++i)
  {
    matrix_[i] = elem.matrix_[i];
  }
}

ivanov::Matrix::Matrix(Matrix && elem): //copir
  matrix_(std::move(elem.matrix_)),
  rows_(elem.rows_),
  columns_(elem.columns_)
{
  elem.matrix_.reset();
  elem.rows_ = 0;
  elem.columns_ = 0;
}

ivanov::Matrix & ivanov::Matrix::operator= (const Matrix & elem) //prisva
{
  if (this == &elem)
  {
    return *this;
  }
  matrix_.reset(new std::shared_ptr<Shape>[(elem.rows_ * elem.columns_)]);
  rows_ = elem.rows_;
  columns_ = elem.columns_;
  for (int i = 0; i < (rows_ * columns_); ++i)
  {
    matrix_[i] = elem.matrix_[i];
  }
  return *this;
}

ivanov::Matrix & ivanov::Matrix::operator= (Matrix && elem)
{
  matrix_ = std::move(elem.matrix_);
  rows_ = elem.rows_;
  columns_ = elem.columns_;
  elem.rows_ = 0;
  elem.columns_ = 0;
  return *this;
}

std::unique_ptr<std::shared_ptr<ivanov::Shape>[]>::pointer ivanov::Matrix::operator [](const int index) const //sravn
{
  if ((index < 0) || (index >= rows_))
  {
    throw std::out_of_range ("Invalid index");
  }

  return matrix_.get() + index * columns_;
}

void ivanov::Matrix::addShape(const std::shared_ptr<Shape> & Element)
{
  if (Element == nullptr)
  {
    throw std::invalid_argument ("The object must exist!");
  }
  bool checkingAdd = false;
  for (int i = 0; !checkingAdd; ++i)
  {
    for (int j = 0; j < columns_; ++j)
    {
      if (!matrix_[i * columns_ + j])
      {
        matrix_[i * columns_ + j] = Element;
        checkingAdd = true;
        break;
      }
      else
      {
        if (checkInsertion(matrix_[i * columns_ + j], Element))
        {
          break;
        }
      }

      if (j == (columns_ - 1) && (!checkingAdd))
      {
        std::unique_ptr<std::shared_ptr<ivanov::Shape>[]> newMatrix (
              new std::shared_ptr<ivanov::Shape>[rows_ * (columns_ + 1)]);
        columns_++;
        for (int n = 0; n < rows_; ++n)
        {
          for (int m = 0; m < columns_ - 1; ++m)
          {
            newMatrix[n * columns_ + m] = matrix_[n * (columns_ - 1) + m];
          }
          newMatrix[(n + 1) * columns_ - 1] = nullptr;
        }
        newMatrix[(i + 1) * columns_ - 1] = Element;
        matrix_.swap(newMatrix);
        checkingAdd = true;
        break;
      }
    }
    if ((i == (rows_ - 1)) && (!checkingAdd))
    {
      std::unique_ptr<std::shared_ptr<ivanov::Shape>[]> newMatrix (
            new std::shared_ptr<ivanov::Shape>[(rows_ + 1) * columns_]);
      rows_++;
      for (int n = 0; n < ((rows_ - 1) * columns_); ++n)
      {
        newMatrix[n] = matrix_[n];
      }
      for (int n = ((rows_ - 1) * columns_); n < (rows_ * columns_); ++n)
      {
        newMatrix[n] = nullptr;
      }
      newMatrix[(rows_ - 1) * columns_] = Element;
      matrix_.swap(newMatrix);
      checkingAdd = true;
    }
  }
}

void ivanov::Matrix::print() const
{
  std::cout << "Matrix:" << std::endl;
  std::cout << "Number of rows: " << rows_ << std::endl;
  std::cout << "Number of columns: " << columns_ << std::endl;
}

int ivanov::Matrix::getRows() const //stroki
{
  return rows_;
}

int ivanov::Matrix::getColumns() const //kolonki
{
  return columns_;
}

bool ivanov::Matrix::checkInsertion(const std::shared_ptr<Shape> & shape_1, const std::shared_ptr<Shape> & shape_2) //nalogenie figyr
{
  if ((shape_1 == nullptr) || (shape_2 == nullptr))
  {
    return false;
  }
  ivanov::rectangle_t shapeFrameRect_1 = shape_1 -> getFrameRect();
  ivanov::rectangle_t shapeFrameRect_2 = shape_2 -> getFrameRect();
  bool checkX = (abs(shapeFrameRect_1.pos.x - shapeFrameRect_2.pos.x)
                 < (shapeFrameRect_1.width / 2 + shapeFrameRect_2.width / 2));
  bool checkY = (abs(shapeFrameRect_1.pos.y - shapeFrameRect_2.pos.y)
                 < (shapeFrameRect_1.height / 2 + shapeFrameRect_2.height / 2));
  return ((checkX) && (checkY));
}
