#ifndef MATRIX_HPP
#define MATRIX_HPP
#include "shape.hpp"
#include <memory>

namespace ivanov
{
  class Matrix
  {
  public:
    Matrix(const std::shared_ptr<Shape> & Element);
    Matrix(const Matrix & elem);
    Matrix(Matrix && elem);
    Matrix & operator=(const Matrix & elem);
    Matrix & operator=(Matrix && elem);
    std::unique_ptr<std::shared_ptr<Shape>[]>::pointer operator [](const int index) const;
    void addShape(const std::shared_ptr<Shape> & Element);
    void print() const;
    int getRows() const;
    int getColumns() const;

  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> matrix_;
    int rows_;
    int columns_;
    bool checkInsertion(const std::shared_ptr<Shape> & shape_1, const std::shared_ptr<Shape> & shape_2);
  };
}
#endif // MATRIX_HPP
