#include "rectangle.hpp"
#include <cmath>
#include <stdexcept>

ivanov::Rectangle::Rectangle(const point_t & centr, double height, double width):
  rec_ {centr, height, width},
  angle_(0.0)
{
  if ((height < 0.0) || (width < 0.0))
  {
    throw std::invalid_argument ("ERROR");
  }
  Points_[0] = {rec_.pos.x + rec_.width / 2, rec_.pos.y + rec_.height / 2};
  Points_[1] = {rec_.pos.x - rec_.width / 2, rec_.pos.y + rec_.height / 2};
  Points_[2] = {rec_.pos.x - rec_.width / 2, rec_.pos.y - rec_.height / 2};
  Points_[3] = {rec_.pos.x + rec_.width / 2, rec_.pos.y - rec_.height / 2};
}

ivanov::rectangle_t ivanov::Rectangle::getFrameRect() const
{
  double right = Points_[0].x;
  double left = Points_[0].x;
  double top = Points_[0].y;
  double bottom = Points_[0].y;
  for (int i = 1; i < 4; i++)
  {
    if (Points_[i].x < left)
    {
      left = Points_[i].x;
    }
    if (Points_[i].y < bottom)
    {
      bottom = Points_[i].y;
    }
    if (Points_[i].x > right)
    {
      right = Points_[i].x;
    }
    if (Points_[i].y > top)
    {
      top = Points_[i].y;
    }
  }
  return {{((left + right) / 2), ((bottom + top) / 2)}, (right - left), (top - bottom)};
}

double ivanov::Rectangle::getArea() const
{
  return rec_.height * rec_.width;
}

void ivanov::Rectangle::move(const point_t & pos)
{
  for (int i = 0; i < 4; i++)
  {
    Points_[i].x += pos.x - rec_.pos.x;
    Points_[i].y += pos.y - rec_.pos.y;
  }
  rec_.pos = pos;
}

void ivanov::Rectangle::move(double dx, double dy)
{
  rec_.pos.x += dx;
  rec_.pos.y += dy;
  for (int i = 0; i < 4; i++)
  {
    Points_[i].x += dx;
    Points_[i].y += dy;
  }
}

void ivanov::Rectangle::scale(double k)
{
  if (k < 0.0)
  {
    throw std::invalid_argument ("ERROR");
  }
  rec_.height *= k;
  rec_.width *= k;
  Points_[0] = {rec_.pos.x + rec_.width / 2, rec_.pos.y + rec_.height / 2};
  Points_[1] = {rec_.pos.x - rec_.width / 2, rec_.pos.y + rec_.height / 2};
  Points_[2] = {rec_.pos.x - rec_.width / 2, rec_.pos.y - rec_.height / 2};
  Points_[3] = {rec_.pos.x + rec_.width / 2, rec_.pos.y - rec_.height / 2};
}

void ivanov::Rectangle::rotate(double angle)
{
  double sinVal = sin(angle * M_PI / 180);
  double cosVal = cos(angle * M_PI / 180);
  ivanov::point_t center = getFrameRect().pos;
  for (int i = 0; i < 4; i++)
  {
    double newX = center.x + (Points_[i].x - center.x) * cosVal -
        - (Points_[i].y - center.y) * sinVal;
    double newY = center.y + (Points_[i].x - center.x) * sinVal +
        + (Points_[i].y - center.y) * cosVal;
    Points_[i] = {newX, newY};

  }
}
