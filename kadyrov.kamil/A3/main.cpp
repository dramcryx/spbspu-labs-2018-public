#include "cmath"
#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

int main()
{
  std::cout << "Testing of CompositeShape" << std::endl;
  std::shared_ptr<kadyrov::Shape> testRect =
  std::make_shared<kadyrov::Rectangle>(kadyrov::Rectangle({ 50.0, 75.0, { 105.0,105.0}}));
  std::shared_ptr<kadyrov::Shape> testCirc =
    std::make_shared<kadyrov::Circle>(kadyrov::Circle({ 46.0, 74.5 }, 32.6));
  kadyrov::CompositeShape testComp(testRect);
  testComp.addShape(testCirc);

  std::cout << "Data of Composite Shape: " << std::endl;
  std::cout << testComp.getSize() << " figures here" << std::endl;
  std::cout << "Area of this composite figure: " << testComp.getArea() << std::endl;
  std::cout << "Data of FrameRect: height = " << testComp.getFrameRect().height
    << " width = " << testComp.getFrameRect().width << std::endl;
  std::cout << "Now figure will be changed" << std::endl;
  std::cout << "New position of center is {25.0,100.0}" << std::endl;

  testComp.move({ 25.0,100.0 });
  testComp.move(7.0, 7.0);
  testComp.scale(7.0);
  testComp.deleteShape(0);

  std::cout << "Updated position is {32.0,107.0}" << std::endl;
  std::cout << "Shape is scaled with coefficient = 7.0" << std::endl;
  std::cout << "After update there are" << testComp.getSize() << " figures here" << std::endl;

  return 0;
}
