#include <iostream>
#include "circle.hpp"
#include "matrix.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

int main()
{
  kadyrov::Rectangle obRec({ 1.2, 3.4,{5.6, 7.8} });
  kadyrov::Circle obCirc({ 6.3, 2.7 }, 5.0);
  std::shared_ptr< kadyrov::Shape > circlePtr = std::make_shared<kadyrov::Circle >(obCirc);
  std::shared_ptr< kadyrov::Shape > rectanglePtr = std::make_shared<kadyrov::Rectangle >(obRec);
  kadyrov::CompositeShape obComp;
  obComp.addShape(circlePtr);
  obComp.addShape(rectanglePtr);

  kadyrov::CompositeShape testObComp(obComp);
  std::cout << obComp.getSize() << ' ' << obComp.getFrameRect().pos.x << ' ' <<
      obComp.getFrameRect().pos.y << std::endl;
  std::cout << testObComp.getSize() << ' ' << testObComp.getFrameRect().pos.x << ' ' <<
      testObComp.getFrameRect().pos.y << std::endl;
  testObComp = obComp;
  std::cout << obComp.getSize() << ' ' << obComp.getFrameRect().pos.x << ' ' <<
      obComp.getFrameRect().pos.y << std::endl;
  std::cout << testObComp.getSize() << ' ' << testObComp.getFrameRect().pos.x << ' ' <<
      testObComp.getFrameRect().pos.y << std::endl;

  std::shared_ptr<kadyrov::Shape> ptr1(new kadyrov::Rectangle({ 3.2, 7.5, { 2.4, 6.4 } }));
  std::shared_ptr<kadyrov::Shape> ptr2(new kadyrov::Circle({ 2.4, 6.4 }, 1.7));
  std::cout << "Create matrix with Rectangle" << std::endl;
  kadyrov::Matrix obMatrix(ptr1);
  std::cout << "There are " << obMatrix.getLayers() << " figures inside." << std::endl;
  std::cout << "There are " << obMatrix.getLayerSize() << "layers." << std::endl;
  std::cout << "Add circle in matrix" << std::endl;
  obMatrix.addShape(ptr2);
  std::cout << "There are " << obMatrix.getLayers() << " figures inside." << std::endl;
  std::cout << "There are " << obMatrix.getLayerSize() << "layers." << std::endl;

  std::cout << "Starting area is " << testObComp.getArea() << std::endl;
  std::cout << "Starting position is " << testObComp.getFrameRect().pos.x << ", " <<
      testObComp.getFrameRect().pos.y << std::endl;
  testObComp.scale(1.5);
  std::cout << "Area after scaling: " << testObComp.getArea() << std::endl;
  std::cout << "Position after scaling: " << testObComp.getFrameRect().pos.x << ", " <<
      testObComp.getFrameRect().pos.y << std::endl;

  testObComp.move(2.5, 2.5);
  std::cout << "Move with dx and dy: " << testObComp.getFrameRect().pos.x << ", " <<
      testObComp.getFrameRect().pos.y << std::endl;

  testObComp.move({ 5.2, 5.2 });
  std::cout << "Move to point:  " << testObComp.getFrameRect().pos.x << ", " <<
      testObComp.getFrameRect().pos.y << std::endl;
  return 0;
}
