#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace kadyrov
{
  class Circle : public kadyrov::Shape
  {
  public:
    Circle(const point_t &centerPos, const double radius);
    virtual double getArea() const override;
    virtual rectangle_t getFrameRect() const override;
    virtual void move(const double dx, const double dy) override;
    virtual void move(const point_t &newPos) override;
    virtual void scale(const double coefficient) override;
    virtual void rotate(double angle) override;
  private:
    point_t position_;
    double radius_;
  };
}

#endif
