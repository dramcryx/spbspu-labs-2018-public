#include "composite-shape.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

kadyrov::CompositeShape::CompositeShape() :
  figures_(nullptr),
  size_(0)
{
}

kadyrov::CompositeShape::CompositeShape(const std::shared_ptr<kadyrov::Shape> & obj) :
  figures_(new std::shared_ptr<kadyrov::Shape>[1]),
  size_(0)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  figures_[size_++] = obj;
}

kadyrov::CompositeShape::CompositeShape(const CompositeShape & obj) :
  figures_(new std::shared_ptr<kadyrov::Shape>[obj.size_]),
  size_(obj.size_)
{
  for (size_t i = 0; i < obj.size_; i++)
  {
    figures_[i] = obj.figures_[i];
  }
}

kadyrov::CompositeShape::CompositeShape(CompositeShape && obj) :
  size_(obj.size_)
{
  figures_ = std::move(obj.figures_);
  obj.size_ = 0;
  obj.figures_.reset();
}

kadyrov::CompositeShape & kadyrov::CompositeShape::operator= (const CompositeShape & elem) noexcept
{
  if (this != &elem)
  {
    std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> buf(new std::shared_ptr<kadyrov::Shape>[elem.size_]);
    for (size_t i = 0; i < elem.size_; i++)
    {
      buf[i] = elem.figures_[i];
    }
    figures_.swap(buf);
    size_ = elem.size_;
  }
  return *this;
}

kadyrov::CompositeShape & kadyrov::CompositeShape::operator= (CompositeShape && elem)
{
  figures_ = std::move(elem.figures_);
  size_ = elem.size_;
  elem.size_ = 0;
  return *this;
}

std::shared_ptr<kadyrov::Shape> kadyrov::CompositeShape::operator [] (const size_t index) const
{
  if ((index >= size_))
  {
    throw std::out_of_range("ind is out of range");
  }
  return figures_[index];
}

void kadyrov::CompositeShape::addShape(const std::shared_ptr<kadyrov::Shape> & obj)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> newArray(new std::shared_ptr<kadyrov::Shape>[size_ + 1]);
  for (size_t i = 0; i < size_; i++)
  {
    newArray[i] = figures_[i];
  }
  newArray[size_] = obj;
  size_++;
  figures_.swap(newArray);
}

void kadyrov::CompositeShape::deleteShape(const size_t index)
{
  if (index >= size_)
  {
    throw std::out_of_range("Invalid index");
  }
  if ((size_ == 1) && (index == 0))
  {
    figures_.reset();
    size_ = 0;
    return;
  }
  std::unique_ptr<std::shared_ptr <kadyrov::Shape >[]> newArray(new std::shared_ptr<kadyrov::Shape>[size_ - 1]);
  for (size_t i = 0; i < index; i++)
  {
    newArray[i] = figures_[i];
  }
  for (size_t i = index; i < size_ - 1; ++i)
  {
    newArray[i] = figures_[i + 1];
  }
  figures_.swap(newArray);
  size_--;
}

double kadyrov::CompositeShape::getArea() const
{
  double totalArea = 0.0;
  for (size_t i = 0; i < size_; i++)
  {
    totalArea += figures_[i]->getArea();
  }
  return totalArea;
}

kadyrov::rectangle_t kadyrov::CompositeShape::getFrameRect() const
{
  if (size_ <= 0)
  {
    return { 0.0 , 0.0,{ 0.0 , 0.0 } };
  }
  kadyrov::rectangle_t frameRect = figures_[0]->getFrameRect();
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  for (size_t i = 1; i < size_; i++)
  {
    frameRect = figures_[i]->getFrameRect();
    if ((frameRect.pos.x - frameRect.width / 2) < minX)
    {
      minX = frameRect.pos.x - frameRect.width / 2;
    }
    if ((frameRect.pos.x + frameRect.width / 2) > maxX)
    {
      maxX = frameRect.pos.x + frameRect.width / 2;
    }
    if ((frameRect.pos.y + frameRect.height / 2) > maxY)
    {
      maxY = frameRect.pos.y + frameRect.height / 2;
    }
    if ((frameRect.pos.y - frameRect.height / 2) < minY)
    {
      minY = frameRect.pos.y - frameRect.height / 2;
    }
  }
  return { (maxX - minX), (maxY - minY),{ (minX + (maxX - minX) / 2), (minY + (maxY - minY) / 2) } };
}

void kadyrov::CompositeShape::move(const point_t & newPos)
{
  move(newPos.x - getFrameRect().pos.x, newPos.y - getFrameRect().pos.y);
}

void kadyrov::CompositeShape::move(const double dx, const double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    figures_[i]->move(dx, dy);
  }
}

void kadyrov::CompositeShape::scale(const double coefficient)
{
  if (coefficient < 0.0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  kadyrov::point_t compShapeCenter = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    kadyrov::point_t shapeCenter = figures_[i]->getFrameRect().pos;
    figures_[i]->move((coefficient - 1.0) * (shapeCenter.x - compShapeCenter.x),
        (coefficient - 1.0)* (shapeCenter.y - compShapeCenter.y));
    figures_[i]->scale(coefficient);
  }
}

size_t kadyrov::CompositeShape::getSize() const
{
  return size_;
}

void kadyrov::CompositeShape::rotate(double angle)
{
  double sinAngle = sin(angle * M_PI / 180);
  double cosAngle = cos(angle * M_PI / 180);
  kadyrov::point_t compShapeCenter = getFrameRect().pos;
  for (size_t i = 0; i < size_; i++)
  {
    kadyrov::point_t shapeCenter = figures_[i]->getFrameRect().pos;
    figures_[i]->move ({compShapeCenter.x + cosAngle * (shapeCenter.x - compShapeCenter.x)
        - sinAngle * (shapeCenter.y - compShapeCenter.y),
            compShapeCenter.y + cosAngle * (shapeCenter.y - compShapeCenter.y)
                + sinAngle * (shapeCenter.x - compShapeCenter.x)});
    figures_[i]->rotate(angle);
  }
}
