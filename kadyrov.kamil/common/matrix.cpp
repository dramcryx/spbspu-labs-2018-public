#include "matrix.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <memory>
#include <vector>

kadyrov::Matrix::Matrix():
  figures_(nullptr),
  figuresInLayer_(0),
  layers_(0),
  layerSize_(0)
{
}

kadyrov::Matrix::Matrix(const kadyrov::CompositeShape &obj):
  Matrix()
{
  addCompShape(obj);
}

kadyrov::Matrix::Matrix(const std::shared_ptr<kadyrov::Shape> obj):
  figures_(nullptr),
  figuresInLayer_{0},
  layers_(0),
  layerSize_(0)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Wrong pointer");
  }
  addShape(obj);
}

kadyrov::Matrix::Matrix(const kadyrov::Matrix &obj):
  figures_(new std::shared_ptr<kadyrov::Shape>[obj.layers_ * obj.layerSize_]),
  figuresInLayer_(obj.figuresInLayer_),
  layers_(obj.layers_),
  layerSize_(obj.layerSize_)
{
  for (int i = 0; i < (layers_ * layerSize_); i++)
  {
    figures_[i] = obj.figures_[i];
  }
}

kadyrov::Matrix::Matrix(Matrix &&obj):
  figures_(nullptr),
  figuresInLayer_(obj.figuresInLayer_),
  layers_(obj.layers_),
  layerSize_(obj.layerSize_)
{
  figures_.swap(obj.figures_);
  obj.layers_ = 0;
  obj.layerSize_ = 0;
}

kadyrov::Matrix::~Matrix()
{
  figures_.reset();
  figures_ = nullptr;
  figuresInLayer_.resize(1);
  figuresInLayer_ = {0};
  layers_ = 0;
  layerSize_ = 0;
}

kadyrov::Matrix & kadyrov::Matrix::operator=(const kadyrov::Matrix &obj)
{
  if (this == &obj)
  {
    return *this;
  }
  figures_.reset(new std::shared_ptr<kadyrov::Shape> [obj.layerSize_*obj.layers_] );
  figuresInLayer_ = obj.figuresInLayer_;
  layers_=obj.layers_;
  layerSize_=obj.layerSize_;
  for (int i=0; i < (layers_*layerSize_); ++i)
  {
    figures_[i]=obj.figures_[i];
  }
  return *this;
}

kadyrov::Matrix & kadyrov::Matrix::operator= (kadyrov::Matrix &&obj)
{
  if (this == &obj)
  {
    return *this;
  }
  figures_.reset(new std::shared_ptr<kadyrov::Shape>[obj.layers_* obj.layerSize_]);
  layerSize_=obj.layerSize_;
  layers_=obj.layers_;
  figuresInLayer_ = obj.figuresInLayer_;
  figures_=std::move(obj.figures_);
  obj.figures_=nullptr;
  obj.layers_=0;
  obj.layerSize_=0;
  obj.figuresInLayer_.resize(1);
  figuresInLayer_ = {0};
  return *this;
}

std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> kadyrov::Matrix::operator[](const int index) const
{
  if ((index < 0) || (index >= layers_))
  {
    throw std::out_of_range("Wrong index");
  }
  std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> layer(
      new std::shared_ptr<kadyrov::Shape>[layerSize_]);
  for (int i = 0; i < layerSize_; ++i)
  {
    layer[i] = figures_[index * layerSize_ + i];
  }
  return layer;
}

void kadyrov::Matrix::addShape(const std::shared_ptr<kadyrov::Shape> &obj)
{
  if (obj == nullptr) 
  {
    throw std::invalid_argument("Wrong pointer to shape");
  }
  if ((layers_ == 0) && (layerSize_ == 0))
  {
    std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> copyMatrix (new std::shared_ptr<kadyrov::Shape>[1]);
    figures_.swap(copyMatrix);
    layers_ = layerSize_ = 1;
    figures_[0] = obj;
    figuresInLayer_= {1};
    return;
  }

  int i = 0;
  for (; i < layers_; ++i)
  {
    int j = 0;
    for(; j < layerSize_; ++j)
    {
      if (!figures_[i * layerSize_ + j])
      {
        figures_[i * layerSize_ + j] = obj;
        ++figuresInLayer_[i];
        return;
      }
      if (checkIntersection(figures_[i * layerSize_ + j], obj))
      {
        break;
      }
    }
    if (j == layerSize_)
    {
      std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> newMatrix(
          new std::shared_ptr<kadyrov::Shape>[layers_ * (layerSize_ + 1)]);
      for (int k = 0; k < layers_; ++k)
      {
        for (j = 0; j < layerSize_; ++j)
        {
          newMatrix[k * layerSize_ + j + k] = figures_[k * layerSize_ + j];
        }
      }
      ++layerSize_;
      newMatrix[(i + 1) * layerSize_ - 1] = obj;
      figures_ = std::move(newMatrix);
      ++figuresInLayer_[i];
      return;
    }
  }
  if (i == layers_)
  {
    std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> newMatrix(
        new std::shared_ptr<kadyrov::Shape>[(layers_ + 1) * layerSize_]);
    for (int k = 0; k < layers_ * layerSize_; ++k)
    {
      newMatrix[k] = figures_[k];
    }
    newMatrix[layers_ * layerSize_] = obj;
    ++layers_;
    figuresInLayer_.resize(layers_);
    ++figuresInLayer_[layers_-1];
    figures_ = std::move(newMatrix);
  }
}

void kadyrov::Matrix::addCompShape(const kadyrov::CompositeShape &composite)
{
  for (size_t i = 0; i < composite.getSize(); i++)
  {
    addShape(composite[i]);
  }
}

bool kadyrov::Matrix::checkIntersection(const std::shared_ptr<kadyrov::Shape> &firstShape,
    const std::shared_ptr<kadyrov::Shape> &secondShape) const
{
  if (firstShape == nullptr || secondShape == nullptr)
  {
    return false;
  }
  kadyrov::rectangle_t firstShapeFrameRect = firstShape -> getFrameRect();
  kadyrov::rectangle_t secondShapeFramerect = secondShape -> getFrameRect();
  return ((fabs(firstShapeFrameRect.pos.x - secondShapeFramerect.pos.x)
      < ((firstShapeFrameRect.width / 2) + (secondShapeFramerect.width / 2)))
          && ((fabs(firstShapeFrameRect.pos.y - secondShapeFramerect.pos.y)
              < ((firstShapeFrameRect.height / 2) + (secondShapeFramerect.height / 2)))));
}

int kadyrov::Matrix::getLayers() const noexcept
{
  return layers_;
}

int kadyrov::Matrix::getLayerSize() const noexcept
{
  return layerSize_;
}

int kadyrov::Matrix::getFiguresInLayer(const int index)
{
  return figuresInLayer_[index];
}
