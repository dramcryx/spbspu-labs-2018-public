#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include <vector>
#include "composite-shape.hpp"
#include "shape.hpp"

namespace kadyrov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const kadyrov::CompositeShape &obj);
    Matrix(const std::shared_ptr<kadyrov::Shape> obj);
    Matrix(const Matrix &obj);
    Matrix(Matrix &&obj);
    ~Matrix();

    Matrix & operator=(const Matrix &obj);
    Matrix & operator=(Matrix &&obj);
    std::unique_ptr<std::shared_ptr<kadyrov::Shape>[]> operator[](const int index) const;

    void addShape(const std::shared_ptr<kadyrov::Shape> &obj);
    void addCompShape(const kadyrov::CompositeShape &obj);
    int getLayers() const noexcept;
    int getLayerSize() const noexcept;
    int getFiguresInLayer(const int index);

    bool checkIntersection(const std::shared_ptr<kadyrov::Shape> &firstShape,
        const std::shared_ptr<kadyrov::Shape> &secondShape) const;

  private:
    std::unique_ptr <std::shared_ptr <kadyrov::Shape>[]> figures_;
    std::vector<int> figuresInLayer_;
    int layers_;
    int layerSize_;
  };
};

#endif
