#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>


kadyrov::Rectangle::Rectangle(const kadyrov::rectangle_t &rect) :
  rectangle_(rect),
  angle_(0)
{
  if (rectangle_.width < 0.0 || rectangle_.height < 0.0)
  {
    throw std::invalid_argument("Wrong value of width or height");
  }
}

double kadyrov::Rectangle::getArea() const
{
  return rectangle_.width * rectangle_.height;
}

kadyrov::rectangle_t kadyrov::Rectangle::getFrameRect() const
{
  double sinusAngle = sin(angle_ * M_PI / 180);
  double cosineAngle = cos(angle_ * M_PI / 180);
  double width = fabs(rectangle_.width * cosineAngle ) + fabs(rectangle_.height * sinusAngle);
  double height = fabs(rectangle_.height * cosineAngle) + fabs(rectangle_.width * sinusAngle);
  
  return {width, height, rectangle_.pos};
}

void kadyrov::Rectangle::move(const double dx, const double dy)
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
}

void kadyrov::Rectangle::move(const point_t &newPos)
{
  rectangle_.pos = newPos;
}

void kadyrov::Rectangle::scale(const double coefficient)
{
  if (coefficient < 0.0)
  {
    throw std::invalid_argument("Wrong value of coefficient!");
  }
  else
  {
    rectangle_.height *= coefficient;
    rectangle_.width *= coefficient;
  }
}

void kadyrov::Rectangle::rotate(double angle)
{
  angle_ += angle;
  if (angle_ >= 360 || angle_ <= -360)
  {
    angle_ = fmod (angle_,360);
  }
}

kadyrov::rectangle_t kadyrov::Rectangle::getRectangle() const
{
  return rectangle_;
}
