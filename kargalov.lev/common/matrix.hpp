
#ifndef matrix_hpp
#define matrix_hpp

#include <memory>
#include "shape.hpp"


namespace kargalov
{
    class Matrix
    {
    public:
        
        using element_type = std::shared_ptr<Shape>;
        using matrix_type = std::unique_ptr<element_type []>;
        
        Matrix();
        void addShape(const element_type shape);
        size_t getLayers() const;
        size_t getLayerWidth() const;
        
    private:
        bool overLapping(const element_type shape, const size_t i) const;
        
        matrix_type matrix_;
        size_t layers_;
        size_t layerWidth_;
    };
}
#endif /* matrix_hpp */
