#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

using namespace kargalov;

Rectangle::Rectangle(const rectangle_t& parametrs)
: parametrs_(parametrs),
angle_(0)
{
    if (parametrs_.height < 0.0 || parametrs_.width < 0.0) {
        throw std::invalid_argument("WARNIG: invalid parametr of Rectangle\n");
    }
}

void Rectangle::move(const point_t& point)
{
    parametrs_.pos = point;
}

void Rectangle::move(const double dx, const double dy)
{
    parametrs_.pos.x += dx;
    parametrs_.pos.y += dy;
}

void Rectangle::scale(const double increment)
{
    parametrs_.width *= increment;
    parametrs_.height *= increment;
    
    if (increment < 0.0){
        throw std::invalid_argument("WARNING: invalid coefficient of scaling\n");
    }
}

double Rectangle::getArea() const
{
    return parametrs_.height * parametrs_.width;
}

rectangle_t Rectangle::getFrameRect() const
{
    double width = fabs(cos(angle_ / 180 * M_PI)) * parametrs_.width + fabs(sin(angle_ / 180 * M_PI)) * parametrs_.height;
    
    double height = fabs(sin(angle_ / 180 * M_PI)) * parametrs_.width + fabs(cos(angle_ / 180 * M_PI)) * parametrs_.height;
    
    return {width, height, parametrs_.pos};
    
}

void Rectangle::rotate(const double angle)
{
    angle_ += angle;
}

