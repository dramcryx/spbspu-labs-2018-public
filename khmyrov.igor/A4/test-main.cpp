#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double ACCURACY = 0.01;

BOOST_AUTO_TEST_SUITE(CircleParametersTest)

  BOOST_AUTO_TEST_CASE(MovingToPointTest)
  {
    khmyrov::Circle circleObject({30.0, 30.0}, 10.0);

    const double area_before_moving = circleObject.getArea();
    const khmyrov::rectangle_t rect_before_moving = circleObject.getFrameRect();

    circleObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(area_before_moving, circleObject.getArea());
    BOOST_CHECK_EQUAL(rect_before_moving.width, circleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rect_before_moving.height, circleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(InvalidConstructor)
  {
    BOOST_CHECK_THROW(khmyrov::Circle circleObject({30.0, 30.0}, -5.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingForPointTest)
  {
    khmyrov::Circle circleObject({30.0, 30.0}, 10.0);

    const double area_before_moving = circleObject.getArea();
    const khmyrov::rectangle_t rect_before_moving = circleObject.getFrameRect();

    circleObject.move(20.0, 20.0);

    BOOST_CHECK_EQUAL(area_before_moving, circleObject.getArea());
    BOOST_CHECK_EQUAL(rect_before_moving.width, circleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rect_before_moving.height, circleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    khmyrov::Circle circleObject({30.0, 30.0}, 10.0);

    const double area_before_scaling = circleObject.getArea();
    const double coeff = 3.5;

    circleObject.scale(coeff);

    BOOST_CHECK_CLOSE(coeff * coeff * area_before_scaling, circleObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling)
  {
    khmyrov::Circle circleObject({30.0, 30.0}, 10.0);

    BOOST_CHECK_THROW(circleObject.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(RectangleParametersTest)

  BOOST_AUTO_TEST_CASE(MovingToPointTest)
  {
    khmyrov::Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);

    const double area_before_moving = rectangleObject.getArea();
    const khmyrov::rectangle_t rect_before_moving = rectangleObject.getFrameRect();

    rectangleObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(area_before_moving, rectangleObject.getArea());
    BOOST_CHECK_EQUAL(rect_before_moving.width, rectangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rect_before_moving.height, rectangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(InvalidConstructor)
  {
    BOOST_CHECK_THROW(khmyrov::Rectangle rectangleObject({30.0, 30.0}, -5.0, 10.0), std::invalid_argument);
    BOOST_CHECK_THROW(khmyrov::Rectangle rectangleObject({30.0, 30.0}, 5.0, -10.0), std::invalid_argument);
    BOOST_CHECK_THROW(khmyrov::Rectangle rectangleObject({30.0, 30.0}, -5.0, -10.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingForPointTest)
  {
    khmyrov::Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);

    const double area_before_moving = rectangleObject.getArea();
    const khmyrov::rectangle_t rect_before_moving = rectangleObject.getFrameRect();

    rectangleObject.move(20.0, 20.0);

    BOOST_CHECK_EQUAL(area_before_moving, rectangleObject.getArea());
    BOOST_CHECK_EQUAL(rect_before_moving.width, rectangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rect_before_moving.height, rectangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    khmyrov::Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);

    const double area_before_scaling = rectangleObject.getArea();
    const double coeff = 3.5;

    rectangleObject.scale(coeff);

    BOOST_CHECK_CLOSE(coeff * coeff * area_before_scaling, rectangleObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling)
  {
    khmyrov::Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);

    BOOST_CHECK_THROW(rectangleObject.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TriangleParametersTest)

  BOOST_AUTO_TEST_CASE(MovingToPointTest)
  {
    khmyrov::Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});

    const double area_before_moving = triangleObject.getArea();
    const khmyrov::rectangle_t rect_before_moving = triangleObject.getFrameRect();

    triangleObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(area_before_moving, triangleObject.getArea());
    BOOST_CHECK_EQUAL(rect_before_moving.width, triangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rect_before_moving.height, triangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(InvalidConstructor)
  {
    BOOST_CHECK_THROW(khmyrov::Triangle triangleObject({0.0, 0.0}, {0.0, 0.0}, {0.0, 30.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingForPointTest)
  {
    khmyrov::Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});

    const double area_before_moving = triangleObject.getArea();
    const khmyrov::rectangle_t rect_before_moving = triangleObject.getFrameRect();

    triangleObject.move(20.0, 20.0);

    BOOST_CHECK_EQUAL(area_before_moving, triangleObject.getArea());
    BOOST_CHECK_EQUAL(rect_before_moving.width, triangleObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rect_before_moving.height, triangleObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    khmyrov::Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});

    const double area_before_scaling = triangleObject.getArea();
    const double coeff = 3.5;

    triangleObject.scale(coeff);

    BOOST_CHECK_CLOSE(coeff * coeff * area_before_scaling, triangleObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling)
  {
    khmyrov::Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});

    BOOST_CHECK_THROW(triangleObject.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeParametersTest)

  BOOST_AUTO_TEST_CASE(MovingToPointTest)
  {
    khmyrov::Circle circleObject({30.0, 30.0}, 10.0);
    khmyrov::Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    khmyrov::Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});

    std::shared_ptr <khmyrov::Shape> rectangleObjectPtr = std::make_shared <khmyrov::Rectangle> (rectangleObject);
    std::shared_ptr <khmyrov::Shape> circleObjectPtr = std::make_shared <khmyrov::Circle> (circleObject);
    std::shared_ptr <khmyrov::Shape> triangleObjectPtr = std::make_shared <khmyrov::Triangle> (triangleObject);

    khmyrov::CompositeShape compositeShapeObject({0.0, 0.0});
    compositeShapeObject.addShape(rectangleObjectPtr);
    compositeShapeObject.addShape(circleObjectPtr);
    compositeShapeObject.addShape(triangleObjectPtr);

    const double area_before_moving = compositeShapeObject.getArea();
    const khmyrov::rectangle_t rect_before_moving = compositeShapeObject.getFrameRect();

    compositeShapeObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(area_before_moving, compositeShapeObject.getArea());
    BOOST_CHECK_EQUAL(rect_before_moving.width, compositeShapeObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rect_before_moving.height, compositeShapeObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(InvalidAdding)
  {
    khmyrov::CompositeShape compositeShapeObject({0.0, 0.0});
    BOOST_CHECK_THROW(compositeShapeObject.addShape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidRemoving)
  {
    khmyrov::CompositeShape compositeShapeObject({0.0, 0.0});
    BOOST_CHECK_THROW(compositeShapeObject.removeShape(-1.0), std::invalid_argument);
    BOOST_CHECK_THROW(compositeShapeObject.removeShape(5.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MovingForPointTest)
  {
    khmyrov::Circle circleObject({30.0, 30.0}, 10.0);
    khmyrov::Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    khmyrov::Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});

    std::shared_ptr <khmyrov::Shape> rectangleObjectPtr = std::make_shared <khmyrov::Rectangle> (rectangleObject);
    std::shared_ptr <khmyrov::Shape> circleObjectPtr = std::make_shared <khmyrov::Circle> (circleObject);
    std::shared_ptr <khmyrov::Shape> triangleObjectPtr = std::make_shared <khmyrov::Triangle> (triangleObject);

    khmyrov::CompositeShape compositeShapeObject({0.0, 0.0});
    compositeShapeObject.addShape(rectangleObjectPtr);
    compositeShapeObject.addShape(circleObjectPtr);
    compositeShapeObject.addShape(triangleObjectPtr);

    const double area_before_moving = compositeShapeObject.getArea();
    const khmyrov::rectangle_t rect_before_moving = compositeShapeObject.getFrameRect();

    compositeShapeObject.move({20.0, 20.0});

    BOOST_CHECK_EQUAL(area_before_moving, compositeShapeObject.getArea());
    BOOST_CHECK_EQUAL(rect_before_moving.width, compositeShapeObject.getFrameRect().width);
    BOOST_CHECK_EQUAL(rect_before_moving.height, compositeShapeObject.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(ScalingTest)
  {
    khmyrov::Circle circleObject({30.0, 30.0}, 10);
    khmyrov::Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    khmyrov::Triangle triangleObject({0.0, 0.0}, {90.0, 0.0}, {0.0, 30.0});

    std::shared_ptr <khmyrov::Shape> rectangleObjectPtr = std::make_shared <khmyrov::Rectangle> (rectangleObject);
    std::shared_ptr <khmyrov::Shape> circleObjectPtr = std::make_shared <khmyrov::Circle> (circleObject);
    std::shared_ptr <khmyrov::Shape> triangleObjectPtr = std::make_shared <khmyrov::Triangle> (triangleObject);

    khmyrov::CompositeShape compositeShapeObject({0.0, 0.0});
    compositeShapeObject.addShape(rectangleObjectPtr);
    compositeShapeObject.addShape(circleObjectPtr);
    compositeShapeObject.addShape(triangleObjectPtr);

    const double area_before_scaling = compositeShapeObject.getArea();
    const double coeff = 3.5;

    compositeShapeObject.scale(coeff);

    BOOST_CHECK_CLOSE(coeff * coeff * area_before_scaling, compositeShapeObject.getArea(), ACCURACY);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaling)
  {
    khmyrov::Rectangle rectangleObject({30.0, 30.0}, 10.0, 20.0);
    std::shared_ptr <khmyrov::Shape> rectangleObjectPtr = std::make_shared <khmyrov::Rectangle> (rectangleObject);
    khmyrov::CompositeShape compositeShapeObject({0, 0});
    compositeShapeObject.addShape(rectangleObjectPtr);

    BOOST_CHECK_THROW(compositeShapeObject.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
khmyrov::Matrix matrix;
khmyrov::Rectangle rect3({10.0, 10.0}, 4.0, 5.0);
khmyrov::Circle circ3({0.0, 0.0}, 5.0);
std::shared_ptr <khmyrov::Rectangle> ptrrect3 = std::make_shared <khmyrov::Rectangle> (rect3);
std::shared_ptr <khmyrov::Circle> ptrcirc3 = std::make_shared <khmyrov::Circle> (circ3);

BOOST_AUTO_TEST_SUITE(testMatrix)
BOOST_AUTO_TEST_CASE(test_matrix_add)
{
    BOOST_CHECK_EQUAL(matrix.getRows(), 0);
    BOOST_CHECK_EQUAL(matrix.getColumns(), 0);
}

BOOST_AUTO_TEST_CASE(test_matrix_add2)
{
    matrix.add(ptrrect3);
    BOOST_CHECK_EQUAL(matrix.getRows(), 1);
    BOOST_CHECK_EQUAL(matrix.getColumns(), 1);
}

BOOST_AUTO_TEST_CASE(test_matrix_add3)
{
    matrix.add(ptrcirc3);
    BOOST_CHECK_EQUAL(matrix.getRows(), 1);
    BOOST_CHECK_EQUAL(matrix.getColumns(), 2);
}

BOOST_AUTO_TEST_CASE(test_matrix_exception)
{
    BOOST_CHECK_THROW(matrix.add(nullptr), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(test_figure_rotate)
BOOST_AUTO_TEST_CASE(test_rotate)
{
    rect3.rotate(90);
    BOOST_CHECK_EQUAL(rect3.getFrameRect().width, 4.0);
    BOOST_CHECK_EQUAL(rect3.getFrameRect().height, 5.0);
}

BOOST_AUTO_TEST_CASE(test_rotate2)
{
    rect3.rotate(270);
    BOOST_CHECK_CLOSE(rect3.getFrameRect().width, 4.0, ACCURACY);
    BOOST_CHECK_CLOSE(rect3.getFrameRect().height, 5.0, ACCURACY);
}

BOOST_AUTO_TEST_CASE(test_rotate4)
{
    rect3.rotate(55);
    BOOST_CHECK_EQUAL(rect3.getFrameRect().pos.x, 10.0);
    BOOST_CHECK_EQUAL(rect3.getFrameRect().pos.y, 10.0);
}

BOOST_AUTO_TEST_CASE(test_rotate5)
{
    circ3.rotate(45);
    BOOST_CHECK_EQUAL(circ3.getFrameRect().width / 2, 5.0);
    BOOST_CHECK_EQUAL(circ3.getFrameRect().height / 2, 5.0);
}
BOOST_AUTO_TEST_SUITE_END()
