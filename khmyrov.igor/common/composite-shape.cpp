#include "composite-shape.hpp"
#include <cmath>
#include <stdexcept>
#include <iostream>

khmyrov::CompositeShape::CompositeShape(const point_t & pos):
        Shape(pos),
        numberShapes_(0),
        arrayShapes_(nullptr)
{
}

double khmyrov::CompositeShape::getArea() const
{
  double area = 0.0;
  for (int i = 0; i < numberShapes_; ++i) {
    area += arrayShapes_[i] -> getArea();
  }
  return area;
}

khmyrov::rectangle_t khmyrov::CompositeShape::getFrameRect() const
{
  if (arrayShapes_ == 0) {
    return {0.0, 0.0, {0.0, 0.0}};
  }
  khmyrov::rectangle_t shapeFrame = arrayShapes_[0] -> getFrameRect();
  double minX = shapeFrame.pos.x - shapeFrame.width/2;
  double maxX = shapeFrame.pos.x + shapeFrame.width/2;
  double minY = shapeFrame.pos.y - shapeFrame.height/2;
  double maxY = shapeFrame.pos.y + shapeFrame.height/2;
  for (int i = 0; i < numberShapes_; ++i) {
    shapeFrame = arrayShapes_[i] -> getFrameRect();
    if (shapeFrame.pos.x - shapeFrame.width / 2 < minX) {
      minX = shapeFrame.pos.x - shapeFrame.width / 2;
    }
    if (shapeFrame.pos.x + shapeFrame.width / 2 > maxX ){
      maxX = shapeFrame.pos.x + shapeFrame.width / 2;
    }
    if (shapeFrame.pos.y - shapeFrame.height / 2 < minY) {
      minY = shapeFrame.pos.y - shapeFrame.height / 2;
    }
    if (shapeFrame.pos.y + shapeFrame.height / 2 > maxY) {
      maxY = shapeFrame.pos.y + shapeFrame.height / 2;
    }
  }
  return { maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2} };
}

void khmyrov::CompositeShape::move(const khmyrov::point_t & pos)
{
  for (int i = 0; i < numberShapes_; ++i) {
    arrayShapes_[i]->move((pos.x - getFrameRect().pos.x), (pos.y - getFrameRect().pos.y));
  }
}

void khmyrov::CompositeShape::move(const double dx, const double dy)
{
  for (int i = 0; i < numberShapes_; ++i) {
    arrayShapes_[i]->move(dx, dy);
  }
}

void khmyrov::CompositeShape::scale(const double coeff)
{
  if (coeff <= 0.0) {
    throw std::invalid_argument("Scale coeff is Invalid!");
  }
  point_t currentPosition = getFrameRect().pos;
  for (int i = 0; i < numberShapes_; ++i) {
    arrayShapes_[i]->move((coeff - 1) * (arrayShapes_[i]->getFrameRect().pos.x - currentPosition.x), (coeff - 1)
                                                                                                     * (arrayShapes_[i]->getFrameRect().pos.y - currentPosition.y));
    arrayShapes_[i] -> scale(coeff);
  }
}

void khmyrov::CompositeShape::addShape(const std::shared_ptr< Shape > addedShape)
{
  if (addedShape == nullptr) {
    throw std::invalid_argument("Empty pointer!");
  }
  std::unique_ptr< std::shared_ptr < Shape > [] > tempArray(new std::shared_ptr< Shape > [numberShapes_ + 1]);
  for (int i = 0; i < numberShapes_; ++i) {
    tempArray[i] = arrayShapes_[i];
  }
  tempArray[numberShapes_] = addedShape;
  numberShapes_++;
  arrayShapes_.swap(tempArray);
}

void khmyrov::CompositeShape::removeShape(const int index)
{
  if ((numberShapes_ == 0) || (index >= numberShapes_)) {
    throw std::invalid_argument("Index is out of range!");
  }
  std::unique_ptr< std::shared_ptr < Shape > [] > tempArray(new std::shared_ptr< Shape > [numberShapes_ - 1]);
  for (int i = 0; i < index; ++i) {
    tempArray[i] = arrayShapes_[i];
  }
  for (int i = index; i < numberShapes_ - 1; ++i) {
    tempArray[i] = arrayShapes_[i + 1];
  }
  arrayShapes_.swap(tempArray);
  numberShapes_--;
}

void khmyrov::CompositeShape::rotate(const double degrees)
{
    if (numberShapes_ != 0)
    {
        double radns = degrees * M_PI / 180;
        point_t pos = {0, 0};
        rectangle_t temprect = {0 , 0, pos};
        for (int i = 0; i < numberShapes_; i++)
        {
            temprect = arrayShapes_[i]->getFrameRect();
            pos.x = pos_.x + (temprect.pos.x - pos_.x) * cos(radns) - (temprect.pos.y - pos_.y) * sin(radns);
            pos.y = pos_.y + (temprect.pos.y - pos_.y) * cos(radns) + (temprect.pos.x - pos_.x) * sin(radns);
            arrayShapes_[i]->move(pos);
            arrayShapes_[i]->rotate(degrees);
        }
        pos_.x = (maxXY().x + minXY().x) / 2.0;
        pos_.y = (maxXY().y + minXY().y) / 2.0;
    }
}
khmyrov::point_t khmyrov::CompositeShape::maxXY() const
{
    double maxX = arrayShapes_[0]->getFrameRect().pos.x + ((arrayShapes_[0]->getFrameRect().width) / 2.0);
    double maxY = arrayShapes_[0]->getFrameRect().pos.y + ((arrayShapes_[0]->getFrameRect().height) / 2.0);
    for(int i = 0; i < numberShapes_; i++)
    {
        if ((arrayShapes_[i]->getFrameRect().pos.x + ((arrayShapes_[i]->getFrameRect().width) / 2.0)) > maxX)
        {
            maxX = arrayShapes_[i]->getFrameRect().pos.x + ((arrayShapes_[i]->getFrameRect().width) / 2.0);
        }
        if ((arrayShapes_[i]->getFrameRect().pos.y + ((arrayShapes_[i]->getFrameRect().height) / 2.0)) > maxY)
        {
            maxY = arrayShapes_[i]->getFrameRect().pos.y + ((arrayShapes_[i]->getFrameRect().height) / 2.0);
        }
    }
    return {maxX, maxY};
}

khmyrov::point_t khmyrov::CompositeShape::minXY() const
{
    double minX = arrayShapes_[0]->getFrameRect().pos.x - (arrayShapes_[0]->getFrameRect().width / 2.0);
    double minY = arrayShapes_[0]->getFrameRect().pos.y - (arrayShapes_[0]->getFrameRect().height / 2.0);
    for(int i = 0; i < numberShapes_; i++)
    {
        if ((arrayShapes_[i]->getFrameRect().pos.x - arrayShapes_[i]->getFrameRect().width / 2.0) < minX)
        {
            minX = arrayShapes_[i]->getFrameRect().pos.x - arrayShapes_[i]->getFrameRect().width / 2.0;
        }
        if ((arrayShapes_[i]->getFrameRect().pos.y - arrayShapes_[i]->getFrameRect().height / 2.0) < minY)
        {
            minY = arrayShapes_[i]->getFrameRect().pos.y - arrayShapes_[i]->getFrameRect().height / 2.0;
        }
    }
    return {minX, minY};
}

void khmyrov::CompositeShape::viewName() const
{
    std::cout << "CompositeShape ";
}



