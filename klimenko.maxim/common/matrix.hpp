#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include <memory>

namespace klimenko
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix & rhs);
    Matrix(Matrix && rhs);
    ~Matrix() = default;
    Matrix &operator=(Matrix & rhs);
    Matrix &operator=(Matrix && rhs);
    std::unique_ptr<Shape *[]> operator[](int index) const;

    bool checkOverlapping(const Shape *shape1,const Shape *shape2);
    void add(Shape *newShape);
    void printCurrentInfo() const;

  private:
    std::unique_ptr<Shape *[]> shapes_;
    int line_;
    int column_;
  };
}

#endif
