#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace klimenko
{
  class Triangle : public Shape
  {
  public:
    Triangle(const point_t & point1, const point_t & point2, const point_t & point3);

    double getArea() const noexcept override;
    point_t getPos() noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t & point) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double factor) override;
    void rotate(const double angle) override;

  private:
    point_t point1_;
    point_t point2_;
    point_t point3_;
    point_t centre_;
  };
}
#endif
