#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

int main()
{
  konkin::point_t p1 = {4.0, 4.0};
  konkin::point_t p2 = {2.0, 2.0};
  konkin::Rectangle rect({1.0, 2.0, p1});
  konkin::Circle circle(p2, 1.0);

  konkin::CompositeShape comp_shape;
  konkin::CompositeShape::ptr_type rectPtr = std::make_shared<konkin::Rectangle>(rect);
  konkin::CompositeShape::ptr_type circPtr = std::make_shared<konkin::Circle>(circle);

  std::cout << "add rectangle"<<std::endl;
  comp_shape.addShape(rectPtr);
  std::cout <<"shape0 ="<<comp_shape.size() << std::endl;
  std::cout << comp_shape.getFrameRect().pos.x << ' '<< comp_shape.getFrameRect().pos.y;

  std::cout << "add circle" <<std::endl;
  comp_shape.addShape(circPtr);
  std::cout <<"shape0 ="<< comp_shape.size() << std::endl;

  std::cout << "Area of CompositeShape is = " << comp_shape.getArea() <<std::endl;
  std::cout << "FrameRect of CompositeShape is:" << std::endl;
  std::cout << "X = " <<comp_shape.getFrameRect().pos.x << ' ';
  std::cout << "Y = " <<comp_shape.getFrameRect().pos.y <<std::endl;

  std::cout << "Width of CompositeShape = " <<comp_shape.getFrameRect().width<<std::endl;
  std::cout << "Height of CompositeShape = " <<comp_shape.getFrameRect().height<<std::endl;


  std::cout << "Scaling. Coef = 2" <<std::endl;
  comp_shape.scale(2.0);
  std::cout << "New parameters of CompositeShape" << std::endl;
  std::cout << "New area of CompositeShape is = " << comp_shape.getArea() << std::endl;
  std::cout << "New width of CompositeShape = " <<comp_shape.getFrameRect().width << std::endl;
  std::cout << "New height of CompositeShape = " <<comp_shape.getFrameRect().height << std::endl;


  std::cout << "Moving to point (-3.0 ; -3.0)" << std::endl;

  std::cout << "X1 = " <<comp_shape.getFrameRect().pos.x << ' ';
  std::cout << "Y1 = " <<comp_shape.getFrameRect().pos.y << std::endl;
  comp_shape.move({-3.0, -3.0});
  std::cout << "X1 = " <<comp_shape.getFrameRect().pos.x << ' ';
  std::cout << "Y1 = " <<comp_shape.getFrameRect().pos.y << std::endl;


  std::cout << "Moving by dx = 3, dy = 3" <<std::endl;
  comp_shape.move(3.0, 3.0);
  std::cout<<circle.getFrameRect().pos.x<< ' '<< circle.getFrameRect().pos.y<< std::endl;
  std::cout << "X2 = " <<comp_shape.getFrameRect().pos.x << ' ';
  std::cout << "Y2 = " <<comp_shape.getFrameRect().pos.y <<std::endl;



  std::cout << "remove 1-st shape" <<std::endl;
  comp_shape.removeShape(1);
  std::cout <<"shape0 ="<< comp_shape.size() << std::endl;

  std::cout << "" <<std::endl;

  konkin::Matrix matr;
  matr.addShape(rectPtr);
  matr.addShape(circPtr);
  std::cout << "layers amount = " <<matr.getNumber() << std::endl;
  std::cout << "max size of layer = "<<matr.getSize() << std::endl;

  std::cout << "width[0][0] = "<<matr[0][0]->getFrameRect().width<<std::endl
            << "height[0][0] = "<< matr[0][0]->getFrameRect().height << std::endl;
  std::cout << "rotate matrix[0][0] by 90*" <<std::endl;

  matr[0][0]->rotate(90.0);

  std::cout << "width[0][0] = " << matr[0][0]->getFrameRect().width<<std::endl
            << "height[0][0] = " << matr[0][0]->getFrameRect().height << std::endl;

  std::cout << matr[0][0]->getFrameRect().width<<" "<< matr[0][0]->getFrameRect().height << " "
            << matr[0][0]->getFrameRect().pos.x << " " << matr[0][0]->getFrameRect().pos.y << std::endl;

  konkin::Rectangle rect3({5.0, 6.0, { 5.0,0.0 }});
  konkin::CompositeShape::ptr_type rectPtr3 = std::make_shared<konkin::Rectangle>(rect3);
  konkin::Circle circle3({{ -5.0, -2.0 }, 4.0});
  konkin::CompositeShape::ptr_type circPtr3 = std::make_shared<konkin::Circle>(circle3);

  konkin::CompositeShape comp1;
  comp1.addShape(rectPtr3);
  comp1.addShape(circPtr3);
  comp1.move({0.0,0.0});
  std::cout << rectPtr3->getFrameRect().width << " " << rectPtr3->getFrameRect().height << " "
            << rectPtr3->getFrameRect().pos.x << " " << rectPtr3->getFrameRect().pos.y << std::endl;
  std::cout << comp1.getFrameRect().width << " " << comp1.getFrameRect().height << " "
            << comp1.getFrameRect().pos.x << " " << comp1.getFrameRect().pos.y << std::endl;
  comp1.rotate(180);
  std::cout << rectPtr3->getFrameRect().width << " " << rectPtr3->getFrameRect().height << " "
            << rectPtr3->getFrameRect().pos.x << " " << rectPtr3->getFrameRect().pos.y << std::endl;
  std::cout << comp1.getFrameRect().width << " " << comp1.getFrameRect().height << " "
            << comp1.getFrameRect().pos.x << " " << comp1.getFrameRect().pos.y << std::endl;

  return 0;
}



