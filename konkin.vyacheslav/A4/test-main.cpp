#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double EPS = 0.001;

BOOST_AUTO_TEST_SUITE(Rotate_Tests)

  BOOST_AUTO_TEST_CASE(RectangleTest)
  {
    konkin::Rectangle rectangle({2.0, 3.0, {10.0, 5.0}});
    rectangle.rotate(90.0);
    konkin::rectangle_t shapeFrameRect = rectangle.getFrameRect();
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.width, 2.0, EPS);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.height, 3.0, EPS);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.pos.x, 10.0, EPS);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.pos.y, 5.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeTest)
  {
    konkin::CompositeShape comp_shape;
    std::shared_ptr<konkin::Shape> rectPtr1
        = std::make_shared<konkin::Rectangle>(konkin::Rectangle({2.0, 3.0, {10.0, 5.0}}));
    comp_shape.addShape(rectPtr1);
    std::shared_ptr<konkin::Shape> rectPtr2
        = std::make_shared<konkin::Rectangle>(konkin::Rectangle({2.0, 3.0, {-10.0, -5.0}}));
    comp_shape.addShape(rectPtr2);

    konkin::rectangle_t shapeFRBefore = comp_shape.getFrameRect();
    comp_shape.rotate(90.0);
    konkin::rectangle_t shapeFRAfter = comp_shape.getFrameRect();
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFRBefore.width, shapeFRAfter.height, EPS);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFRBefore.height, shapeFRAfter.width, EPS);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFRBefore.pos.x, shapeFRAfter.pos.x, EPS);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFRBefore.pos.y, shapeFRAfter.pos.y, EPS);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Matrix_Tests)

  BOOST_AUTO_TEST_CASE(AddShapeTest)
  {
    konkin::Matrix matrix_shape;
    std::shared_ptr<konkin::Shape> rectPtr1
        = std::make_shared<konkin::Rectangle>(konkin::Rectangle({2.0, 3.0, {10.0, 5.0}}));
    matrix_shape.addShape(rectPtr1);
    std::shared_ptr<konkin::Shape> rectPtr2
        = std::make_shared<konkin::Rectangle>(konkin::Rectangle({2.0, 3.0, {-10.0, -5.0}}));
    matrix_shape.addShape(rectPtr2);
    std::shared_ptr<konkin::Shape> circlePtr
        = std::make_shared<konkin::Circle>(konkin::Circle( {0.0, 0.0}, 9.0));
    matrix_shape.addShape(circlePtr);
    BOOST_REQUIRE_EQUAL(matrix_shape[0][0], rectPtr1);
    BOOST_REQUIRE_EQUAL(matrix_shape[0][1], rectPtr2);
    BOOST_REQUIRE_EQUAL(matrix_shape[1][0], circlePtr);
  }

  BOOST_AUTO_TEST_CASE(AddCompositeShapeTest)
  {
    konkin::CompositeShape comp_shape;
    konkin::Matrix matrix_shape;
    std::shared_ptr<konkin::Shape> rectPtr1
        = std::make_shared<konkin::Rectangle>(konkin::Rectangle({2.0, 3.0, {10.0, 5.0}}));
    comp_shape.addShape(rectPtr1);
    matrix_shape.addShape(rectPtr1);
    std::shared_ptr<konkin::Shape> rectPtr2
        = std::make_shared<konkin::Rectangle>(konkin::Rectangle({2.0, 3.0, {-10.0, -5.0}}));
    comp_shape.addShape(rectPtr2);
    matrix_shape.addShape(rectPtr2);
    std::shared_ptr<konkin::Shape> compPtr
        = std::make_shared<konkin::CompositeShape>(comp_shape);
    matrix_shape.addShape(compPtr);
    BOOST_REQUIRE_EQUAL(matrix_shape[0][0], rectPtr1);
    BOOST_REQUIRE_EQUAL(matrix_shape[0][1], rectPtr2);
    BOOST_REQUIRE_EQUAL(matrix_shape[0][2], compPtr);
  }


BOOST_AUTO_TEST_SUITE_END()

