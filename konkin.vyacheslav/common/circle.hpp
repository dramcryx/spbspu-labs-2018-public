#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace konkin
{
  class Circle: public Shape
  {
  public:
    Circle(const point_t & centre, const double radius);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(const double dx, const double dy) override;
    void scale(const double coef) override;
    point_t getPos() const;
    double getRadius() const;
    void rotate(const double angle);

  private:
    point_t centre_;
    double radius_;
    double angle_;
  };
}

#endif // CIRCLE_HPP
