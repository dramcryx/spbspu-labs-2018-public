#include "rectangle.hpp"
#include <stdexcept>

#define _USE_MATH_DEFINES
#include <cmath>
using namespace konkin;
Rectangle::Rectangle(const rectangle_t & rect):
  rect_(rect),
  angle_(0.0)
{
  if( (rect_.height < 0.0) || (rect_.width < 0.0) )
  {
    throw std::invalid_argument("Error. incorrect height or width.");
  }
}
double Rectangle::getArea() const
{
  return (rect_.width * rect_.height);
}

rectangle_t Rectangle::getFrameRect() const
{
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  double width = rect_.height * abs(sine) + rect_.width * abs(cosine);
  double height = rect_.height * abs(cosine) + rect_.width * abs(sine);

  return {height, width, rect_.pos };
}

void Rectangle::move(const double dx, const double dy)
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}
void Rectangle::move(const point_t & pos)
{
  rect_.pos = pos;
}
void Rectangle::scale(const double coef)
{
  if (coef < 0.0)
  {
    throw std::invalid_argument("Error. incorrect parametr of scale.");
  }
  rect_.width *= coef;
  rect_.height *= coef;
}

double konkin::Rectangle::getHeight() const noexcept
{
  return rect_.height;
}

double konkin::Rectangle::getWidth() const noexcept
{
  return rect_.width;
}

konkin::point_t konkin::Rectangle::getPos() const
{
  return rect_.pos;
}

void konkin::Rectangle::rotate(const double angle)
{
  angle_ += angle;
  if (angle_ >= 360)
  {
    angle_ = fmod(angle_, 360.0);
  }
}
