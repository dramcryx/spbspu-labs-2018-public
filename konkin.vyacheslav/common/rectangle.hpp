#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace konkin
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(const rectangle_t & rect);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(const double dx, const double dy) override;
    void scale(const double coef) override;
    point_t getPos() const override;
    double getHeight() const noexcept;
    double getWidth() const noexcept;
    void rotate(const double angle) override ;
  private:
    rectangle_t rect_;
    double angle_;
  };
}

#endif // RECTANGLE_HPP
