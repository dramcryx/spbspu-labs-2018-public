#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using namespace korovin;

void printParametrs(const std::shared_ptr<Shape> &object)
{
  std::cout << "Width:" << object->getFrameRect().width << std::endl;
  std::cout << "Height:" << object->getFrameRect().height << std::endl;
  std::cout << "Area:" << object->getArea() << std::endl;
  std::cout << "pos(x):" << object->getFrameRect().pos.x << std::endl;
  std::cout << "pos(y):" << object->getFrameRect().pos.y ;
}

int main()
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{0, 0}, 20, 10);
  std::cout << "rectangle was created" << std::endl << std::endl;

  printParametrs(rectangle);

  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{10, 10}, 10);
  std::cout << std::endl << "circle was created" << std::endl << std::endl;

  printParametrs(circle);

  std::shared_ptr<CompositeShape> composite_shape = std::make_shared<CompositeShape>(rectangle);
  std::cout << std::endl << "composite shape was created" << std::endl << std::endl
    << "rectangle was added to composite shape" << std::endl;

  composite_shape->addShape(circle);
  std::cout << "circle was added to composite shape" << std::endl << std::endl;

  printParametrs(composite_shape);

  return 0;
}
