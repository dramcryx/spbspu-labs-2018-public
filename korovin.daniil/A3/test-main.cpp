#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

using namespace korovin;
const double epsilon = 0.0001;

BOOST_AUTO_TEST_SUITE(testCompositeShape)

BOOST_AUTO_TEST_CASE(move_frame_rect)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{0, 0}, 2, 4);
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{2, -2}, 2);
  
  CompositeShape composite_shape(rectangle);
  
  composite_shape.addShape(circle);
  composite_shape.move(2, 2);
  
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 3.5, epsilon);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 1, epsilon);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 5, epsilon);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 6, epsilon);
}

BOOST_AUTO_TEST_CASE(move_area)
{
  std::shared_ptr<Rectangle> rectangle_1 = std::make_shared<Rectangle>(point_t{1.5, 3}, 3, 1);
  std::shared_ptr<Rectangle> rectangle_2 = std::make_shared<Rectangle>(point_t{2.5, 3}, 3, 2);
  
  CompositeShape composite_shape(rectangle_1);
  
  composite_shape.addShape(rectangle_2);
  composite_shape.move({3, 6});
  
  BOOST_CHECK_CLOSE(composite_shape.getArea(), 9, epsilon);
}

BOOST_AUTO_TEST_CASE(scale_frame_rect)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{3, -2}, 2, 2);
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{0, 2}, 1);
  
  CompositeShape composite_shape(rectangle);
  
  composite_shape.addShape(circle);
  composite_shape.scale(2);

  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 1.5, epsilon);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 0, epsilon);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 10, epsilon);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 12, epsilon);
}

BOOST_AUTO_TEST_CASE(scale_area)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{-1, 0.5}, 4, 1);
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{-2, 3}, 1);
  
  CompositeShape composite_shape(rectangle);
  
  composite_shape.addShape(circle);

  double area = composite_shape.getArea();
  double coeff = 2;

  composite_shape.scale(coeff);

  BOOST_CHECK_CLOSE(composite_shape.getArea(), area * coeff * coeff, epsilon);
}


BOOST_AUTO_TEST_CASE(copy_constructor)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{10, 15}, 30, 20);
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{0, 10}, 15);
  
  CompositeShape composite(circle);
  composite.addShape(rectangle);
  
  CompositeShape composite_1(rectangle);
  composite_1 = composite;

  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite.getFrameRect().height, 30);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().height, 30);
}

BOOST_AUTO_TEST_CASE(move_constructor)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{10, 15}, 30, 20);
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{0, 10}, 15);
  
  CompositeShape composite(rectangle);
  composite.addShape(circle);
  
  CompositeShape composite_1(std::move(composite));

  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().height, 30);
}

BOOST_AUTO_TEST_CASE(move_operator)
{
  std::shared_ptr<Rectangle> rectangle_1 = std::make_shared<Rectangle>(point_t{2, 10}, 8, 4);
  std::shared_ptr<Rectangle> rectangle_2 = std::make_shared<Rectangle>(point_t{-15, 0}, 5, 10);
  
  CompositeShape composite(rectangle_1);
  composite.addShape(rectangle_2);
  
  CompositeShape composite_1 = std::move(composite);

  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.x, -5.75);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.y, 3.5);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().width, 23.5);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().height, 17);
}

BOOST_AUTO_TEST_CASE(invalid_add_shape)
{
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{0, 0}, 3);
  CompositeShape composite_shape(circle);
  
  BOOST_CHECK_THROW(composite_shape.addShape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_scale)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{0, 0}, 5, 5);
  CompositeShape composite_shape(rectangle);

  BOOST_CHECK_THROW(composite_shape.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(add_shape_test)
{
    CompositeShape object;
    object.addShape(std::shared_ptr< Shape >(new Rectangle({ 1, 1 }, 5, 6)));
    object.addShape(std::shared_ptr< Shape >(new Circle({ 2, 2 }, 5)));
    BOOST_REQUIRE_EQUAL(object.getSize(), 2);
}


BOOST_AUTO_TEST_SUITE_END()
