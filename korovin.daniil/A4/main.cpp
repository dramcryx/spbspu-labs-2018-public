#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix-shape.hpp"
#include <iostream>
#include <cmath>

using namespace korovin;

void printParametrs(const std::shared_ptr<Shape> &object)
{
  std::cout << "Width:" << object->getFrameRect().width << std::endl;
  std::cout << "Height:" << object->getFrameRect().height << std::endl;
  std::cout << "Area:" << object->getArea() << std::endl;
  std::cout << "pos(x):" << object->getFrameRect().pos.x << std::endl;
  std::cout << "pos(y):" << object->getFrameRect().pos.y << std::endl;
}

int main()
{
  auto rectangle_1 = std::make_shared<Rectangle>(point_t{-1, -2.5}, 6, 3);
  std::cout << "rectangle_1 was created" << std::endl << std::endl;

  printParametrs(rectangle_1);

  auto circle = std::make_shared<Circle>(point_t{3, 0}, 3);
  std::cout << std::endl << "circle was created" << std::endl << std::endl;

  printParametrs(circle);

  auto rectangle_2 = std::make_shared<Rectangle>(point_t{-2.5, 1}, 3, 2);
  std::cout << std::endl << "rectangle_2 was created" << std::endl << std::endl;

  printParametrs(rectangle_2);

  MatrixShape matrix_shape(rectangle_1);
  std::cout << std::endl << "matrix was created" << std::endl << std::endl;

  matrix_shape.addShape(circle);
  matrix_shape.addShape(rectangle_2);

  matrix_shape.print();
}
