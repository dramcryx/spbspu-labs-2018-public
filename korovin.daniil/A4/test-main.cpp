#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix-shape.hpp"

const double epsilon = 0.0001;

BOOST_AUTO_TEST_SUITE(testRotation)

BOOST_AUTO_TEST_CASE(rectangle)
{
  korovin::Rectangle rectangle({10, 0}, 3, 3);
  rectangle.rotate(-45);

  korovin::rectangle_t frame_rect = rectangle.getFrameRect();
  
  BOOST_CHECK_CLOSE(frame_rect.pos.x, 10, epsilon);
  BOOST_CHECK_CLOSE(frame_rect.pos.y, 0, epsilon);
  BOOST_CHECK_CLOSE(frame_rect.width, 3 * sqrt(2), epsilon);
  BOOST_CHECK_CLOSE(frame_rect.height, 3 * sqrt(2), epsilon);
}

BOOST_AUTO_TEST_CASE(composite_shape)
{
  auto rectangle_1 = std::make_shared<korovin::Rectangle>(korovin::point_t{-3, -5}, 4, 4);
  auto rectangle_2 = std::make_shared<korovin::Rectangle>(korovin::point_t{4, 2}, 4, 4);

  korovin::CompositeShape composite_shape(rectangle_1);
  composite_shape.addShape(rectangle_2);
  composite_shape.rotate(90);

  korovin::rectangle_t frame_rect = composite_shape.getFrameRect();
  
  BOOST_CHECK_CLOSE(frame_rect.pos.x, 0.5, epsilon);
  BOOST_CHECK_CLOSE(frame_rect.pos.y, -1.5, epsilon);
  BOOST_CHECK_CLOSE(frame_rect.width, 11, epsilon);
  BOOST_CHECK_CLOSE(frame_rect.height, 11, epsilon);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(testMatrixShape)

BOOST_AUTO_TEST_CASE(new_rows)
{
  auto rectangle = std::make_shared<korovin::Rectangle>(korovin::point_t{-3, -1}, 4, 4);
  auto circle = std::make_shared<korovin::Circle>(korovin::point_t{-1, 0}, 2);
  
  korovin::MatrixShape matrix_shape(rectangle);
  matrix_shape.addShape(circle);
  
  BOOST_CHECK(matrix_shape[1][0] == circle);
}

BOOST_AUTO_TEST_CASE(new_columns)
{
  auto rectangle_1 = std::make_shared<korovin::Rectangle>(korovin::point_t{-2, 2}, 2, 2);
  auto rectangle_2 = std::make_shared<korovin::Rectangle>(korovin::point_t{0, 0}, 2, 2);
  auto rectangle_3 = std::make_shared<korovin::Rectangle>(korovin::point_t{2, -2}, 2, 2);
  
  korovin::MatrixShape matrix_shape(rectangle_1);
  matrix_shape.addShape(rectangle_2);
  matrix_shape.addShape(rectangle_3);
  
  BOOST_CHECK(matrix_shape[0][2] == rectangle_3);
}

BOOST_AUTO_TEST_CASE(add_one_shape)
{
  auto rectangle = std::make_shared<korovin::Rectangle>(korovin::point_t{-2, 2}, 2, 2);
  korovin::MatrixShape matrix_shape(rectangle);
  
  BOOST_CHECK(matrix_shape[0][0] == rectangle);
}

BOOST_AUTO_TEST_CASE(add_composite_shape)
{
  auto rectangle_1 = std::make_shared<korovin::Rectangle>(korovin::point_t{0, 3}, 4, 2);
  auto rectangle_2 = std::make_shared<korovin::Rectangle>(korovin::point_t{-1, -1}, 2, 2);
  auto circle = std::make_shared<korovin::Circle>(korovin::point_t{0, 2}, 2);

  auto composite_shape_1 = std::make_shared<korovin::CompositeShape>(rectangle_1);
  composite_shape_1->addShape(circle);

  auto composite_shape_2 = std::make_shared<korovin::CompositeShape>(composite_shape_1);

  korovin::MatrixShape matrix_shape(composite_shape_1);
  matrix_shape.addShape(composite_shape_2);

  BOOST_CHECK(matrix_shape[0][0] == composite_shape_1 && matrix_shape[1][0] == composite_shape_2);
}


BOOST_AUTO_TEST_CASE(invalid_add_shape)
{
  auto rectangle = std::make_shared<korovin::Rectangle>(korovin::point_t{-1, -1}, 2, 2);
  korovin::MatrixShape matrix_shape(rectangle);
  
  BOOST_CHECK_THROW(matrix_shape.addShape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
