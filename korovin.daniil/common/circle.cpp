#include "circle.hpp"
#include <cmath>
#include <iostream>

using namespace korovin;
Circle::Circle(const point_t & center,const double radius):
  pos_(center),
  radius_(radius)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument("Invalid circle radius. Radius must be above zero.");
  }
}

bool korovin::operator==(const Circle &circle_1, const Circle &circle_2)
{
  if (circle_1.pos_.x == circle_2.pos_.x &&
    circle_1.pos_.y == circle_2.pos_.y &&
    circle_1.radius_ == circle_2.radius_)
  {
    return true;
  }
  else
  {
    return false;
  }
}

std::string Circle::getName() const
{
  return "circle";
}

double Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

rectangle_t Circle::getFrameRect() const
{
  return rectangle_t{pos_, 2*radius_, 2*radius_};
}

void Circle::move(const point_t & newCenter)
{
  pos_ = newCenter;
}

void Circle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void Circle::scale(const double coefficient)
{
  if (coefficient < 0.0) {
    throw std::invalid_argument("Invalid scale coefficient.");
  }
  else
  {
    radius_ *= coefficient;
  }
}

double Circle::getRadius() const
{
  return radius_;
}

point_t Circle::getPos() const 
{
  return pos_;
}

void Circle::rotate(double /*angle*/)
{
}

