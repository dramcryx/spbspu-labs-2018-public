#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace korovin
{
class Circle: public Shape
{
  public:
    Circle(const point_t &pos, double radius);
    friend bool korovin::operator==(const Circle &, const Circle &);
    std::string getName() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & newCenter) override;
    void move(const double dx, const double dy) override;
    void scale(const double coefficient) override;
    void rotate(double /*angle*/) override;
    double getRadius() const;
    point_t getPos() const  override;

  private:
    point_t pos_;
    double radius_;
  };

  bool operator==(const Circle &, const Circle &);
}

#endif
