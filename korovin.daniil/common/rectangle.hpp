#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace korovin
{
  class Rectangle: public Shape
  {
  public:
    Rectangle(const point_t &pos, double width, double height);
    friend bool korovin::operator==(const Rectangle &, const Rectangle &);
    std::string getName() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const double dx, const double dy) override;
    void move(const point_t & newPoint) override;
    point_t getPos() const override;
    void scale(const double k) override;
    void rotate(double angle) override;

  private:
    point_t corners_[4];
    double width_;
    double height_;
  };

  bool operator==(const Rectangle &, const Rectangle &);
}

#endif
