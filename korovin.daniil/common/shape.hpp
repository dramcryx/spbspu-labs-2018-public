#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"
#include <string>

namespace korovin
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual std::string getName() const = 0;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move (const double dx, const double dy) = 0;
    virtual void move (const point_t & newPoint) = 0;
    virtual point_t getPos() const = 0;
    virtual void scale(const double coefficient) = 0;
    virtual void rotate(double angle) = 0;
  };
}

#endif
