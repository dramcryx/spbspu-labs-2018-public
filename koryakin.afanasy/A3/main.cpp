#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

int main()
{
  using namespace koryakin;

  Rectangle rect(3.0, 5.0, { 14.0, 19.0 } );
  //Circle circ(20.0, { 40.0, 90.0 } );
  std::cout << "rect.getArea: " <<rect.getArea() << std::endl;
 // std::cout << "circ.getArea: " <<circ.getArea() << std::endl;
  std::shared_ptr<Shape> rectPtr1 = std::make_shared<Rectangle>(rect);
  CompositeShape testComp(rectPtr1);
  std::cout << "testComp.getArea  " <<testComp.getArea() << std::endl;
  
  //std::shared_ptr<Shape> circPtr1 = std::make_shared<Circle>(circ);
  //testComp.addShape(circPtr1);
  //std::cout << "testComp.GetArea = circ.getArea() + rect.getArea()  = " << circ.getArea() + rect.getArea() << std::endl;
  std::cout << "testComp.getArea: " <<testComp.getArea() << std::endl;
  
  testComp.removeShape(0);
  std::cout << "removeShape(0)  " << std::endl;
  std::cout << "testComp.getArea() " <<testComp.getArea() << std::endl;
  
  testComp.clear();
  return 0;
} // koryakin 13534/6
