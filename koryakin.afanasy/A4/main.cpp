//KORYAKIN 13534/6
#include <iostream>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using namespace koryakin;

void printMatrix(const Matrix matrix)
{
  for (int i = 0; i < matrix.getLayersNumber(); i++)
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> layer = matrix[i];
    std::cout << "Layer number: " << i + 1 << std::endl;
    for (int j = 0; j < matrix.getLayerSize(); j++)
    {
      if (!layer[j])
      {
        break;
      }
      std::cout << "Shape " << j + 1 << ":" << std::endl;
      layer[j]->print();
    }
  }
}

void printInfo(const Shape & object)
{
  std::cout << "Area: " << object.getArea() << std::endl;
  const rectangle_t frame = object.getFrameRect();
  std::cout << "Frame for object:" << std::endl;
  std::cout << "\tMiddle of frame: " << frame.pos.x << ", " << frame.pos.y << std::endl;
  std::cout << "\tWidth of frame: " << frame.width << std::endl;
  std::cout << "\tHeight of frame: " << frame.height << std::endl;
}

int main()
{
  try
  {
    Circle circle_1{  1.0, { -2.0, -2.0 }};
    std::shared_ptr<Shape> circlePtr1 = std::make_shared<Circle>(circle_1);
    CompositeShape compositeShape(circlePtr1);
    std::cout << "Composite shape:" << std::endl;
    printInfo(compositeShape);
    compositeShape.rotate(90);
    printInfo(compositeShape);
    compositeShape.scale(2.0);
    printInfo(compositeShape);
    compositeShape.clear();
    std::cout << "----------------------------------------------------------" <<std::endl;

    std::shared_ptr<Shape> rect1 = std::make_shared<Rectangle>(Rectangle(10.0, 2.0, { 5.0, 10.0 }));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle(20.0, 40.0, { 10.0, 5.0 }));
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>(Rectangle(10.0, 0.0, { 20.0, 10.0 }));
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle(10.0, { 10.0, 10.0 }));
    std::shared_ptr<Shape> circle2 = std::make_shared<Circle>(Circle(20.0, { 10.0, 10.0 }));

    Matrix matrix;
    matrix.addShape(rect1);
    matrix.addShape(rect2);
    matrix.addShape(rect3);
    matrix.addShape(circle1);
    matrix.addShape(circle2);
    
    printMatrix(matrix);
  }
  catch(std::invalid_argument & e)
  {
    std::cerr << e.what() << std::endl;
  }
  catch(std::out_of_range & e)
  {
    std::cerr << e.what() << std::endl;
  }
  return 0;
}
