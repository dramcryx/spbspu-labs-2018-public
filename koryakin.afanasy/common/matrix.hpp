#ifndef MATRIX_SHAPE_HPP
#define MATRIX_SHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace koryakin
{
  class Matrix
  {
    public:
      Matrix() noexcept;
      Matrix(const Matrix &Matrix);
      Matrix(Matrix &&Matrix);

      std::unique_ptr<std::shared_ptr<Shape>[]> operator[] (const int ind) const;
      Matrix & operator=(const Matrix &rhs);
      Matrix & operator=(Matrix &&rhs);

      void addShape(const std::shared_ptr<Shape> &shape);
      void clear() noexcept;
      int getLayersNumber() const noexcept;
      int getLayerSize() const noexcept;
      void print() const noexcept;
      
      bool checkOverlapping(const std::shared_ptr<Shape> &shapeFirst, const std::shared_ptr<Shape> &shapeSecond);
      
    private:
      std::unique_ptr<std::shared_ptr <Shape>[]> matrix_;
      int layersNumber_;
      int layerSize_;
  };
}

#endif 
