#include "rectangle.hpp"
#include <stdexcept>
#include <iostream>
#include <cmath>

using namespace koryakin;

Rectangle::Rectangle(const double width, const double height, const point_t & center):
  height_(height),
  width_(width),
  center_(center),
  dots_{{center.x - width / 2, center.y - height / 2},
        {center.x + width / 2, center.y - height / 2},
        {center.x - width / 2, center.y + height / 2},
        {center.x + width / 2, center.y + height / 2}}
{
  if ((width < 0.0) || (height < 0.0)) {
    throw std::invalid_argument("The width and height of rectangle must be positive");
  };
};

double Rectangle::getArea() const
{
  return (width_ * height_);
};

rectangle_t Rectangle::getFrameRect() const
{
  double left = center_.x;
  double right = center_.x;
  double top = center_.y;
  double bottom = center_.y;

  for (int i = 0; i < 4; i++)
  {
    double x = dots_[i].x;
    double y = dots_[i].y;

    if (x < left)
    {
      left = x;
    }
    if (x > right)
    {
      right = x;
    }
    if (y < top)
    {
      top = y;
    }
    if (y > bottom)
    {
      bottom = y;
    }
  }

  return { right - left, bottom - top, center_ };
};

void Rectangle::move(const point_t & point) 
{
  double dx = point.x - center_.x;
  double dy = point.y - center_.y;

  center_.x = point.x;
  center_.y = point.y;

  for (int i = 0; i < 4; i++)
  {
    dots_[i].x += dx;
    dots_[i].y += dy;
  }
};

void Rectangle::move(const double dx, const double dy) 
{
  center_.x += dx;
  center_.y += dy;

  for (int i = 0; i < 4; i++)
  {
    dots_[i].x += dx;
    dots_[i].y += dy;
  }
};

void Rectangle::scale(const double factor)
{
  if (factor < 0.0) 
  {
    throw std::invalid_argument("The scaling factor must be non-negative");
  } 

  width_ *= factor;
  height_ *= factor;

  for (int i = 0; i < 4; i++)
  {
    double dx = dots_[i].x - center_.x;
    double dy = dots_[i].y - center_.y;

    dx *= factor;
    dy *= factor;

    dots_[i].x = center_.x + dx;
    dots_[i].y = center_.y + dy;
  }
}

void Rectangle::rotate(const double angle) noexcept
{
  double sin_angle = sin(angle * M_PI / 180);
  double cos_angle = cos(angle * M_PI / 180);

  for (int i = 0; i < 4; i++)
  {
    dots_[i].x = center_.x + (dots_[i].x - center_.x) * cos_angle - (dots_[i].y - center_.y) * sin_angle;
    dots_[i].y = center_.y + (center_.y - dots_[i].y) * cos_angle + (dots_[i].x - center_.x) * sin_angle;
  }
}

void Rectangle::print() const noexcept
{
  rectangle_t rect(getFrameRect());
  std::cout << "Rectangle center: x = " << center_.x << " y = " << center_.y << std::endl;
  std::cout << "                  width = " << width_ << " height " << height_ << std::endl;
  std::cout << "Dots:" << std::endl;
  for (int i = 0; i < 4; i++)
  {
    std::cout << "x = " << dots_[i].x << " y = " << dots_[i].y << std::endl;
  }
  std::cout << std::endl;
}

point_t Rectangle::getPos() const noexcept
{
 return center_;
}
