#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "cmath"

using namespace kostylev;

void printShapeInfo(const kostylev::Shape & figure)
{
  const kostylev::rectangle_t frame = figure.getFrameRect();
  std::cout << "Area = " << figure.getArea() << std::endl;
  std::cout << "Parameters of frame rectangle for this figure:" << std::endl;
  std::cout << "  Center : " << "(" << frame.pos.x << ", " << frame.pos.y << ")" << std::endl;
  std::cout << "  Width = " << frame.width << std::endl;
  std::cout << "  Height = " << frame.height << std::endl;
}

int main()
{
  try
  {
    std::shared_ptr<kostylev::Shape> circle1 = std::make_shared<kostylev::Circle>(kostylev::Circle({3.0, 0.0}, 3.0));
    std::shared_ptr<kostylev::Shape> circle2 = std::make_shared<kostylev::Circle>(kostylev::Circle({-5.0, -5.0}, 5.0));
    std::shared_ptr<kostylev::Shape> rectangle1 =
      std::make_shared<kostylev::Rectangle>(kostylev::Rectangle({0.0, 4.0}, 8.0, 4.0));
    std::shared_ptr<kostylev::Shape> rectangle2 =
      std::make_shared<kostylev::Rectangle>(kostylev::Rectangle({-6.0, 1.0}, 6.0, 6.0));
    std::shared_ptr<kostylev::Shape> rectangle3 =
      std::make_shared<kostylev::Rectangle>(kostylev::Rectangle({0.0, 0.0}, 2.0, 2.0));
      
    kostylev::CompositeShape compositeShape(circle1);
    compositeShape.addShape(circle2);
    compositeShape.addShape(rectangle1);
    compositeShape.addShape(rectangle2);
    compositeShape.addShape(rectangle3);
    std::cout << compositeShape.getFrameRect().pos.x << std::endl;
    std::cout << compositeShape.getFrameRect().pos.y << std::endl;
    
    std::cout << "Move to (5.0, 3.0)" << std::endl;
    compositeShape.move(5.0,3.0);
    
    std::cout << "Scale" << std::endl;
    compositeShape.scale(3.0);
    
    std::cout << "Rotate" << std::endl;
    compositeShape.rotate(-45.0);
    
    std::cout << compositeShape.getFrameRect().pos.x << std::endl;
    std::cout << compositeShape.getFrameRect().pos.y << std::endl;
    std::cout << compositeShape.getFrameRect().width << std::endl;
    std::cout << compositeShape.getFrameRect().height << std::endl;

    kostylev::Matrix test(circle1);
    test.addShape(circle2);
    test.addShape(rectangle1);
    test.addShape(rectangle2);
    test.addShape(rectangle3);
    std::cout << "Quantity of layers" << std::endl;
    std::cout << test.getLayers();
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  return 0;
}
