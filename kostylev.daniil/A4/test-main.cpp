#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double dx = 0.25;

BOOST_AUTO_TEST_SUITE(rotate_tests)

  BOOST_AUTO_TEST_CASE(rotate_rectangle)
  {
    kostylev::Rectangle testRect({0.0, 0.0}, 15.0, 10.0);
    kostylev::rectangle_t testObj = testRect.getFrameRect();
    double tmpArea = testRect.getArea();
    testRect.rotate(180.0);
    BOOST_CHECK_CLOSE(testRect.getArea(), tmpArea, dx);
    BOOST_CHECK_CLOSE(testObj.width, testRect.getFrameRect().width, dx);
    BOOST_CHECK_CLOSE(testObj.height, testRect.getFrameRect().height, dx);
  }

 BOOST_AUTO_TEST_CASE(rotate_circle)
  {
    kostylev::Circle testCirc({0.0, 0.0}, 10.0);
    kostylev::rectangle_t testObj = testCirc.getFrameRect();
    double tmpArea = testCirc.getArea();
    testCirc.rotate(88.0);
    BOOST_CHECK_CLOSE(testCirc.getArea(), tmpArea, dx);
    BOOST_CHECK_CLOSE(testObj.width, testCirc.getFrameRect().width, dx);
    BOOST_CHECK_CLOSE(testObj.height, testCirc.getFrameRect().height, dx);
  }


 BOOST_AUTO_TEST_CASE(rotate_composite_shape)
  {
    std::shared_ptr<kostylev::Shape> testRect1 =
      std::make_shared<kostylev::Rectangle>(kostylev::Rectangle({-15.0, 15.0}, 10.0, 10.0 ));
    std::shared_ptr<kostylev::Shape> testRect2 =
      std::make_shared<kostylev::Rectangle>(kostylev::Rectangle({5.0, -5.0}, 10.0, 10.0 ));
    kostylev::CompositeShape testObj(testRect1);
    testObj.addShape(testRect2);
    double tmpArea = testObj.getArea();
    kostylev::rectangle_t tmpTest = testObj.getFrameRect();
    testObj.rotate(360.0);
    BOOST_CHECK_CLOSE(testObj.getArea(), tmpArea, dx);
    BOOST_CHECK_CLOSE(tmpTest.width, testObj.getFrameRect().width, dx);
    BOOST_CHECK_CLOSE(tmpTest.height, testObj.getFrameRect().height, dx);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTest)

  BOOST_AUTO_TEST_CASE(Check_matrix_layers)
  {
    std::shared_ptr<kostylev::Shape> testCirc1 =
      std::make_shared<kostylev::Circle>(kostylev::Circle({0.0, 0.0}, 3.0));
    std::shared_ptr<kostylev::Shape> testCirc2 =
      std::make_shared<kostylev::Circle>(kostylev::Circle({0.0, 0.0}, 5.0));
    std::shared_ptr<kostylev::Shape> testCirc3 =
      std::make_shared<kostylev::Circle>(kostylev::Circle({30.0, 30.0}, 5.0));
    kostylev::Matrix matrix(testCirc1);
    matrix.addShape(testCirc2);
    matrix.addShape(testCirc3);
    size_t number = 2;
    BOOST_CHECK_EQUAL(number, matrix.getLayers());
  }

  BOOST_AUTO_TEST_CASE(AddShapeTest)
  {
    std::shared_ptr<kostylev::Shape> rectPtr1
         = std::make_shared<kostylev::Rectangle>(kostylev::Rectangle({128.0, 256.0}, 20.0, 48.0));
    kostylev::Matrix matrix_shape(rectPtr1);
    std::shared_ptr<kostylev::Shape> rectPtr2
        = std::make_shared<kostylev::Rectangle>(kostylev::Rectangle({150.0, 256.0}, 24.0, 48.0 ));
    matrix_shape.addShape(rectPtr2);
    std::shared_ptr<kostylev::Shape> circlePtr
        = std::make_shared<kostylev::Circle>(kostylev::Circle({130.0, 256.0}, 10.0));
    matrix_shape.addShape(circlePtr);
    BOOST_REQUIRE_EQUAL(matrix_shape[0][0], rectPtr1);
    BOOST_REQUIRE_EQUAL(matrix_shape[0][1], rectPtr2);
    BOOST_REQUIRE_EQUAL(matrix_shape[1][0], circlePtr);
  }



BOOST_AUTO_TEST_SUITE_END()
