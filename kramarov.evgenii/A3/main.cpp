#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using namespace kramarov;

int main()
{
  std::shared_ptr<Shape> circle = std::make_shared<Circle>(Circle({3.0, 2.0}, 2.0));
  std::shared_ptr<Shape> rectangle = std::make_shared<Rectangle>(Rectangle({10.0, 20.0, {1.0, 10.0}}));
  CompositeShape comp;
  comp.addShape(circle);
  comp.addShape(rectangle);
  std::cout << comp.getArea() << '\n' << comp.getFrameRect().height
  << " " << comp.getFrameRect().width << '\n' << comp.getQtt()
  << '\n';
  comp.move({5.4, 6.0});
  comp.move(2.0, 2.0);
  std::cout << "After move" << '\n' << comp.getFrameRect().pos.x << " "
  << comp.getFrameRect().pos.y
  << '\n';
  comp.scale(2.0);
  std::cout << "After scale" << '\n';
  std::cout << comp.getArea() << '\n' << comp.getFrameRect().height
  << " " << comp.getFrameRect().width << '\n' << comp.getQtt()
  << '\n';
  comp.deleteShape(0);
  std::cout << "After delete" << '\n';
  std::cout << comp.getArea() << '\n' << comp.getFrameRect().height
  << " " << comp.getFrameRect().width << '\n' << comp.getQtt()
  << '\n';

  return 0;
}
