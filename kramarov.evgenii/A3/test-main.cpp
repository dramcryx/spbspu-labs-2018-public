#define BOOST_TEST_MODULE Main
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "shape.hpp"

using namespace kramarov;

const double delta = 0.0001;

BOOST_AUTO_TEST_SUITE(MainCheck)
  BOOST_AUTO_TEST_CASE(ReinitializingMovement)
  {
    Rectangle rectangle(1.5, 2.0, {10.0, 10.0});
    double baseHeight = rectangle.getFrameRect().height;
    double baseWidth = rectangle.getFrameRect().width;
    double baseArea = rectangle.getArea();
    rectangle.move({20.0, 20.0});
    BOOST_CHECK_EQUAL(baseHeight, rectangle.getFrameRect().height);
    BOOST_CHECK_EQUAL(baseWidth, rectangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(baseArea, rectangle.getArea());
    //finished checking rectangle

    Circle circle ({50.0,30.0}, 20.0);
    double baseRadius = circle.getFrameRect().height / 2;
    baseArea = circle.getArea();
    circle.move({35.0, 35.0});
    BOOST_CHECK_EQUAL(baseRadius, circle.getFrameRect().height / 2);
    BOOST_CHECK_EQUAL(baseArea, circle.getArea());
  }

  BOOST_AUTO_TEST_CASE(DifferencialMovement)
  {
    Rectangle rectangle(1.5, 2.0, {10.0, 10.0});
    double baseHeight = rectangle.getFrameRect().height;
    double baseWidth = rectangle.getFrameRect().width;
    double baseArea = rectangle.getArea();
    rectangle.move(20.0, 20.0);
    BOOST_CHECK_EQUAL(baseHeight, rectangle.getFrameRect().height);
    BOOST_CHECK_EQUAL(baseWidth, rectangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(baseArea, rectangle.getArea());
    //finished checking rectangle

    Circle circle ({50.0,30.0}, 20.0);
    double baseRadius = circle.getFrameRect().height / 2;
    baseArea = circle.getArea();
    circle.move(35.0, 35.0);
    BOOST_CHECK_EQUAL(baseRadius, circle.getFrameRect().height / 2);
    BOOST_CHECK_EQUAL(baseArea, circle.getArea());
  }

  BOOST_AUTO_TEST_CASE(Resizing)
  {
    Rectangle rectangle(1.5, 2.0, {10.0, 10.0});
    double baseArea = rectangle.getArea();
    const double scaleRate = 3.35;
    rectangle.scale(scaleRate);
    BOOST_CHECK_CLOSE(baseArea * scaleRate * scaleRate, rectangle.getArea(), delta);
    //finished checking rectangle

    Circle circle ({50.0,30.0}, 20.0);
    baseArea = circle.getArea();
    circle.scale(scaleRate);
    BOOST_CHECK_CLOSE(baseArea * scaleRate * scaleRate, circle.getArea(), delta);
  }

  BOOST_AUTO_TEST_CASE(ConstructorParameters)
  {
    BOOST_CHECK_THROW (Rectangle(-1.5, 2.0, {10.0, 10.0}), std::invalid_argument);
    BOOST_CHECK_THROW (Rectangle(1.5, -2.0, {10.0, 10.0}), std::invalid_argument);
    BOOST_CHECK_THROW (Rectangle(-1.5, -2.0, {10.0, 10.0}), std::invalid_argument);
    BOOST_CHECK_THROW (Circle({50.0, 30.0}, -20.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(ResizingParameters)
  {
    Rectangle rectangle(1.5, 2.0, {10.0, 10.0});
    BOOST_CHECK_THROW (rectangle.scale(-1.5), std::invalid_argument);
    Circle circle({50.0,30.0}, 20.0);
    BOOST_CHECK_THROW (circle.scale(-1.5), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositionCheck)

  BOOST_AUTO_TEST_CASE(AdditionParameters)
  {
    CompositeShape comp;
    BOOST_CHECK_THROW (comp.addShape(nullptr), std::invalid_argument);
    comp.clear();
  }

  BOOST_AUTO_TEST_CASE(DeletionParameters)
  {
    CompositeShape comp;
    BOOST_CHECK_THROW (comp.deleteShape(-3), std::invalid_argument);
    comp.clear();
  }

  BOOST_AUTO_TEST_CASE(DiffMovementCheck)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({5.0, 5.0, {2.0, 3.0}}));
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({0.0, 0.0}, 5.0));
    CompositeShape comp;
    comp.addShape(rectangle1);
    comp.addShape(circle1);
    double tmpHeight = comp.getFrameRect().height;
    double tmpWidth = comp.getFrameRect().width;
    double tmpArea = comp.getArea();
    comp.move(3.0, 2.0);
    BOOST_CHECK_CLOSE(tmpHeight, comp.getFrameRect().height, delta);
    BOOST_CHECK_CLOSE(tmpWidth, comp.getFrameRect().width, delta);
    BOOST_CHECK_CLOSE(tmpArea, comp.getArea(), delta);
  }

  BOOST_AUTO_TEST_CASE(ReinitializingMovementCheck)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({10.0, 10.0, {2.0, 4.0}}));
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({0.0, 0.0}, 5.0));
    CompositeShape comp;
    comp.addShape(rectangle1);
    comp.addShape(circle1);
    double tmpHeight = comp.getFrameRect().height;
    double tmpWidth = comp.getFrameRect().width;
    double tmpArea = comp.getArea();
    comp.move({5.0, 5.0});
    BOOST_CHECK(comp.getFrameRect().pos.x == 5.0 && comp.getFrameRect().pos.y == 5.0);
    BOOST_CHECK_CLOSE(tmpHeight, comp.getFrameRect().height, delta);
    BOOST_CHECK_CLOSE(tmpWidth, comp.getFrameRect().width, delta);
    BOOST_CHECK_CLOSE(tmpArea, comp.getArea(), delta);
  }

  BOOST_AUTO_TEST_CASE(QuantityCheck)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({5.0, 5.0, {1.0, 1.0}}));
    CompositeShape comp;
    comp.addShape(rectangle1);
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({1.0, 1.0}, 5.0));
    comp.addShape(circle1);
    BOOST_CHECK_EQUAL(comp.getQtt(), 2);
  }

  BOOST_AUTO_TEST_CASE(ScalingCheck)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({5.0, 5.0, {1.0, 1.0}}));
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({1.0, 1.0}, 5.0));
    CompositeShape comp;
    comp.addShape(rectangle1);
    comp.addShape(circle1);
    double tmpArea = comp.getArea();
    comp.scale(2.0);
    BOOST_CHECK_CLOSE(2.0 * 2.0 * tmpArea, comp.getArea(), delta);
  }

  BOOST_AUTO_TEST_CASE(GetShapeCheck)
  {
    Rectangle rect0(4.0, 6.0, {0.0, 0.0});
    CompositeShape comp;
    std::shared_ptr<Shape> rectangle0 = std::make_shared<Rectangle>(rect0);
    comp.addShape (rectangle0);
    Rectangle rect1(6.0, 8.0, {1.0, 1.0});
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(rect1);
    comp.addShape (rectangle1);
    BOOST_CHECK_CLOSE (comp[1]->getXY().x, 1.0, delta);
    BOOST_CHECK_CLOSE (comp[1]->getXY().y, 1.0, delta);
    BOOST_CHECK_CLOSE (comp[1]->getFrameRect().width, rect1.getFrameRect().width, delta);
    BOOST_CHECK_CLOSE (comp[1]->getFrameRect().height, rect1.getFrameRect().height, delta);
    comp.clear();
  }

  BOOST_AUTO_TEST_CASE(AreaCheck)
  {
    Rectangle rect0(4.0, 6.0, {0.0, 0.0});
    CompositeShape comp;
    std::shared_ptr<Shape> rectangle0 = std::make_shared<Rectangle>(rect0);
    comp.addShape (rectangle0);
    Rectangle rect1(6.0, 8.0, {1.0, 1.0});
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(rect1);
    comp.addShape (rectangle1);
    BOOST_CHECK_CLOSE (comp.getArea(), 72.0, delta);
    comp.clear();
  }

BOOST_AUTO_TEST_SUITE_END()
