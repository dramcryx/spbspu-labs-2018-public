#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace kramarov;

int main()
{
  try{
  std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({3.0, 2.0}, 2.0));
  std::shared_ptr<Shape> rectangle2 = std::make_shared<Rectangle>(Rectangle({10.0, 20.0, {1.0, 10.0}}));
  CompositeShape comp;
  comp.addShape(circle1);
  comp.addShape(rectangle2);
  std::cout << comp.getArea() << '\n' << comp.getFrameRect().height
  << " " << comp.getFrameRect().width << '\n' << comp.getQtt()
  << '\n';
  comp.move({5.4, 6.0});
  comp.move(2.0, 2.0);
  std::cout << "After move" << '\n' << comp.getFrameRect().pos.x << " "
  << comp.getFrameRect().pos.y
  << '\n';
  comp.scale(2.0);
  comp.rotate(90);
  std::cout << "After scale" << '\n';
  std::cout << comp.getArea() << '\n' << comp.getFrameRect().height
  << " " << comp.getFrameRect().width << '\n' << comp.getQtt()
  << '\n';
  comp.deleteShape(1);

  Matrix matrix;
  matrix.addShape(circle1);
  matrix.addShape(rectangle2);
  std::shared_ptr<Shape> rectangle3 = std::make_shared<Rectangle>(Rectangle({5.0, 1.0, {1.0, 10.0}}));
  matrix.addShape(rectangle3);
  std::shared_ptr<Shape> rectangle4 = std::make_shared<Rectangle>(Rectangle({comp.getFrameRect().width,
  comp.getFrameRect().height, {comp.getFrameRect().pos.x,
  comp.getFrameRect().pos.y}}));
  matrix.addShape(rectangle4);
  matrix.printInfo();

} catch (const std::invalid_argument & err)
  {
     std::cerr << err.what() << '\n';
     return 1;
  }

  return 0;
}
