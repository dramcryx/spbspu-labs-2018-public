#define BOOST_TEST_MODULE Main
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "shape.hpp"
#include "matrix.hpp"

const double delta = 0.0001;

using namespace kramarov;

BOOST_AUTO_TEST_SUITE(MainCheck)
  BOOST_AUTO_TEST_CASE(ReinitializingMovement)
  {
    Rectangle rectangle(1.5, 2.0, {10.0, 10.0});
    double baseHeight = rectangle.getFrameRect().height;
    double baseWidth = rectangle.getFrameRect().width;
    double baseArea = rectangle.getArea();
    rectangle.move({20.0, 20.0});
    BOOST_CHECK_EQUAL(baseHeight, rectangle.getFrameRect().height);
    BOOST_CHECK_EQUAL(baseWidth, rectangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(baseArea, rectangle.getArea());
    //finished checking rectangle

    Circle circle ({50.0,30.0}, 20.0);
    double baseRadius = circle.getFrameRect().height / 2;
    baseArea = circle.getArea();
    circle.move({35.0, 35.0});
    BOOST_CHECK_EQUAL(baseRadius, circle.getFrameRect().height / 2);
    BOOST_CHECK_EQUAL(baseArea, circle.getArea());
  }

  BOOST_AUTO_TEST_CASE(DifferencialMovement)
  {
    Rectangle rectangle(1.5, 2.0, {10.0, 10.0});
    double baseHeight = rectangle.getFrameRect().height;
    double baseWidth = rectangle.getFrameRect().width;
    double baseArea = rectangle.getArea();
    rectangle.move(20.0, 20.0);
    BOOST_CHECK_EQUAL(baseHeight, rectangle.getFrameRect().height);
    BOOST_CHECK_EQUAL(baseWidth, rectangle.getFrameRect().width);
    BOOST_CHECK_EQUAL(baseArea, rectangle.getArea());
    //finished checking rectangle

    Circle circle ({50.0,30.0}, 20.0);
    double baseRadius = circle.getFrameRect().height / 2;
    baseArea = circle.getArea();
    circle.move(35.0, 35.0);
    BOOST_CHECK_EQUAL(baseRadius, circle.getFrameRect().height / 2);
    BOOST_CHECK_EQUAL(baseArea, circle.getArea());
  }

  BOOST_AUTO_TEST_CASE(Resizing)
  {
    Rectangle rectangle(1.5, 2.0, {10.0, 10.0});
    double baseArea = rectangle.getArea();
    const double scaleRate = 3.35;
    rectangle.scale(scaleRate);
    BOOST_CHECK_CLOSE(baseArea * scaleRate * scaleRate, rectangle.getArea(), delta);
    //finished checking rectangle

    Circle circle ({50.0,30.0}, 20.0);
    baseArea = circle.getArea();
    circle.scale(scaleRate);
    BOOST_CHECK_CLOSE(baseArea * scaleRate * scaleRate, circle.getArea(), delta);
  }

  BOOST_AUTO_TEST_CASE(ConstructorParameters)
  {
    BOOST_CHECK_THROW (Rectangle(-1.5, 2.0, {10.0, 10.0}), std::invalid_argument);
    BOOST_CHECK_THROW (Rectangle(1.5, -2.0, {10.0, 10.0}), std::invalid_argument);
    BOOST_CHECK_THROW (Rectangle(-1.5, -2.0, {10.0, 10.0}), std::invalid_argument);
    BOOST_CHECK_THROW (Circle({50.0, 30.0}, -20.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(ResizingParameters)
  {
    Rectangle rectangle(1.5, 2.0, {10.0, 10.0});
    BOOST_CHECK_THROW (rectangle.scale(-1.5), std::invalid_argument);
    Circle circle({50.0,30.0}, 20.0);
    BOOST_CHECK_THROW (circle.scale(-1.5), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositionCheck)

  BOOST_AUTO_TEST_CASE(AdditionParameters)
  {
    CompositeShape comp;
    BOOST_CHECK_THROW (comp.addShape(nullptr), std::invalid_argument);
    comp.clear();
  }

  BOOST_AUTO_TEST_CASE(DeletionParameters)
  {
    CompositeShape comp;
    BOOST_CHECK_THROW (comp.deleteShape(-3), std::invalid_argument);
    comp.clear();
  }

  BOOST_AUTO_TEST_CASE(DiffMovementCheck)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({5.0, 5.0, {2.0, 3.0}}));
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({0.0, 0.0}, 5.0));
    CompositeShape comp;
    comp.addShape(rectangle1);
    comp.addShape(circle1);
    double tmpHeight = comp.getFrameRect().height;
    double tmpWidth = comp.getFrameRect().width;
    double tmpArea = comp.getArea();
    comp.move(3.0, 2.0);
    BOOST_CHECK_CLOSE(tmpHeight, comp.getFrameRect().height, delta);
    BOOST_CHECK_CLOSE(tmpWidth, comp.getFrameRect().width, delta);
    BOOST_CHECK_CLOSE(tmpArea, comp.getArea(), delta);
  }

  BOOST_AUTO_TEST_CASE(ReinitializingMovementCheck)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({10.0, 10.0, {2.0, 4.0}}));
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({0.0, 0.0}, 5.0));
    CompositeShape comp;
    comp.addShape(rectangle1);
    comp.addShape(circle1);
    double tmpHeight = comp.getFrameRect().height;
    double tmpWidth = comp.getFrameRect().width;
    double tmpArea = comp.getArea();
    comp.move({5.0, 5.0});
    BOOST_CHECK(comp.getFrameRect().pos.x == 5.0 && comp.getFrameRect().pos.y == 5.0);
    BOOST_CHECK_CLOSE(tmpHeight, comp.getFrameRect().height, delta);
    BOOST_CHECK_CLOSE(tmpWidth, comp.getFrameRect().width, delta);
    BOOST_CHECK_CLOSE(tmpArea, comp.getArea(), delta);
  }

  BOOST_AUTO_TEST_CASE(QuantityCheck)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({5.0, 5.0, {1.0, 1.0}}));
    CompositeShape comp;
    comp.addShape(rectangle1);
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({1.0, 1.0}, 5.0));
    comp.addShape(circle1);
    BOOST_CHECK_EQUAL(comp.getQtt(), 2);
  }

  BOOST_AUTO_TEST_CASE(ScalingCheck)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({5.0, 5.0, {1.0, 1.0}}));
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({1.0, 1.0}, 5.0));
    CompositeShape comp;
    comp.addShape(rectangle1);
    comp.addShape(circle1);
    double tmpArea = comp.getArea();
    comp.scale(2.0);
    BOOST_CHECK_CLOSE(2.0 * 2.0 * tmpArea, comp.getArea(), delta);
  }

  BOOST_AUTO_TEST_CASE(GetShapeCheck)
  {
    Rectangle rect0(4.0, 6.0, {0.0, 0.0});
    CompositeShape comp;
    std::shared_ptr<Shape> rectangle0 = std::make_shared<Rectangle>(rect0);
    comp.addShape (rectangle0);
    Rectangle rect1(6.0, 8.0, {1.0, 1.0});
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(rect1);
    comp.addShape (rectangle1);
    BOOST_CHECK_CLOSE (comp[1]->getXY().x, 1.0, delta);
    BOOST_CHECK_CLOSE (comp[1]->getXY().y, 1.0, delta);
    BOOST_CHECK_CLOSE (comp[1]->getFrameRect().width, rect1.getFrameRect().width, delta);
    BOOST_CHECK_CLOSE (comp[1]->getFrameRect().height, rect1.getFrameRect().height, delta);
    comp.clear();
  }

  BOOST_AUTO_TEST_CASE(AreaCheck)
  {
    Rectangle rect0(4.0, 6.0, {0.0, 0.0});
    CompositeShape comp;
    std::shared_ptr<Shape> rectangle0 = std::make_shared<Rectangle>(rect0);
    comp.addShape (rectangle0);
    Rectangle rect1(6.0, 8.0, {1.0, 1.0});
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(rect1);
    comp.addShape (rectangle1);
    BOOST_CHECK_CLOSE (comp.getArea(), 72.0, delta);
    comp.clear();
  }

  BOOST_AUTO_TEST_CASE(Rotate_Test)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({25.0, 5.0, {1.0, 1.0}}));
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({0.0, 0.0}, 2.0));

    CompositeShape comp;
    comp.addShape(rectangle1);
    comp.addShape(circle1);

    for (size_t i = -180; i < 180; i++)
    {
      std::shared_ptr<Shape> rectangle2 = std::make_shared<Rectangle>(Rectangle({25.0, 5.0, {1.0, 1.0}}));
      std::shared_ptr<Shape> circle2 = std::make_shared<Circle>(Circle({0.0, 0.0}, 2.0));

      CompositeShape comp2;
      comp2.addShape(rectangle2);
      comp2.addShape(circle2);

      comp2.rotate(i);

      BOOST_CHECK_CLOSE(comp.getFrameRect().pos.x, comp2.getFrameRect().pos.x, delta);
      BOOST_CHECK_CLOSE(comp.getFrameRect().pos.y, comp2.getFrameRect().pos.y, delta);
      BOOST_CHECK_CLOSE(comp.getArea(), comp2.getArea(), delta);
    }
  }

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(Matrix_Tests)

  BOOST_AUTO_TEST_CASE(New_Rows)
  {
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({-10.0, 0.0}, 1.0));
    std::shared_ptr<Shape> circle2 = std::make_shared<Circle>(Circle({0.0, 0.0}, 1.0));
    std::shared_ptr<Shape> circle3 = std::make_shared<Circle>(Circle({10.0, 0.0}, 1.0));

    Matrix matrix;
    matrix.addShape(circle1);
    matrix.addShape(circle2);
    matrix.addShape(circle3);

    BOOST_CHECK(matrix[0][0] == circle1);
    BOOST_CHECK(matrix[0][1] == circle2);
    BOOST_CHECK(matrix[0][2] == circle3);

  }

  BOOST_AUTO_TEST_CASE(New_Columns)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<Rectangle>(Rectangle({10.0, 20.0, {0.0, 0.0}}));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<Rectangle>(Rectangle({10.0, 20.0, {2.0, 1.0}}));

    Matrix matrix;
    matrix.addShape(rectangle1);
    matrix.addShape(rectangle2);

    BOOST_CHECK(matrix[0][0] == rectangle1);
    BOOST_CHECK(matrix[1][0] == rectangle2);

  }

  BOOST_AUTO_TEST_CASE(Copy_Constructor_Test)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({10.0, 20.0, {1.0, 2.0}}));
    Matrix matrix;
    matrix.addShape(rect);
    Matrix matrix2(matrix);
    BOOST_CHECK(matrix[0][0] == matrix2[0][0]);
  }

  BOOST_AUTO_TEST_CASE(Copy_Operator_Test)
  {
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({0.0, 0.0}, 5.0));
    std::shared_ptr<Shape> circle2 = std::make_shared<Circle>(Circle({0.0, 0.0}, 1.0));
    Matrix matrix;
    Matrix matrix2;
    matrix.addShape(circle1);
    matrix2.addShape(circle2);
    matrix2 = matrix;
    BOOST_CHECK(matrix[0][0] == matrix2[0][0]);
  }

BOOST_AUTO_TEST_SUITE_END()
