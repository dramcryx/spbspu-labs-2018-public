#include "composite-shape.hpp"
#include <stdexcept>
#include <cmath>
#include <iostream>

using namespace kramarov;

CompositeShape::CompositeShape():
  shapes_(nullptr),
  qtt_(0)
{
}

CompositeShape::CompositeShape(const CompositeShape &figure):
  qtt_(figure.getQtt())
{
  std::unique_ptr<std::shared_ptr <Shape>[]> clone (new std::shared_ptr<Shape> [figure.getQtt()]);

  for (size_t i = 0; i < figure.getQtt(); i++)
  {
    clone[i] = figure.shapes_[i];
  }
  shapes_.swap(clone);
}

CompositeShape::CompositeShape(CompositeShape &&figure) noexcept:
  qtt_(figure.getQtt())
{
  shapes_.swap(figure.shapes_);
}

double CompositeShape::getArea() const noexcept
{
  double area=0.0;
  for (size_t i=0; i < qtt_; i++)
  {
    area += shapes_[i]->getArea();
  }
  return area;
}

rectangle_t CompositeShape::getFrameRect() const noexcept
{
  if (qtt_ > 0)
  {
    double left = shapes_[0]->getFrameRect().pos.x - shapes_[0]->getFrameRect().width/2;
    double right = shapes_[0]->getFrameRect().pos.x + shapes_[0]->getFrameRect().width/2;
    double top = shapes_[0]->getFrameRect().pos.y + shapes_[0]->getFrameRect().height/2;
    double bottom = shapes_[0]->getFrameRect().pos.y - shapes_[0]->getFrameRect().height/2;

    for (size_t i=1; i < qtt_; i++)
    {
      if ((shapes_[i]->getFrameRect().pos.x - shapes_[i]->getFrameRect().width/2) < left)
      {
        left = shapes_[i]->getFrameRect().pos.x - shapes_[i]->getFrameRect().width/2;
      }
      if ((shapes_[i]->getFrameRect().pos.x + shapes_[i]->getFrameRect().width/2) > right)
      {
        right = shapes_[i]->getFrameRect().pos.x + shapes_[i]->getFrameRect().width/2;
      }
      if ((shapes_[i]->getFrameRect().pos.y - shapes_[i]->getFrameRect().height/2) < bottom)
      {
        bottom = shapes_[i]->getFrameRect().pos.y - shapes_[i]->getFrameRect().height/2;
      }
      if ((shapes_[i]->getFrameRect().pos.y + shapes_[i]->getFrameRect().height/2) > top)
      {
        top = shapes_[i]->getFrameRect().pos.y + shapes_[i]->getFrameRect().height/2;
      }
    }
    return {{(left + (right - left)/2), (bottom + (top - bottom)/2)}, (right - left), (top - bottom)};
  }
  else
  {
    return {{0, 0}, 0, 0};
  }
}

void CompositeShape::move (const point_t & newLocation) noexcept
{
  const point_t curXY = getXY();
  for (size_t i = 0; i < qtt_; i++)
  {
    shapes_[i]->move(newLocation.x - curXY.x, newLocation.y - curXY.y);
  }
}

void CompositeShape::move (const double dx, const double dy) noexcept
{
  for (size_t i=0; i < qtt_; i++)
  {
    shapes_[i]->move (dx,dy);
  }
}

void CompositeShape::scale (const double scaleRate)
{
  if (scaleRate < 0)
  {
    throw std::invalid_argument ("Rate must be above 0");
  }
  const point_t curXY = getXY();
  for (size_t i = 0; i < qtt_; i++)
  {
    const point_t iXY = shapes_[i]->getXY();
    shapes_[i]->move({curXY.x + scaleRate * (iXY.x - curXY.x),
                      curXY.y + scaleRate * (iXY.y - curXY.y)});
    shapes_[i]->scale(scaleRate);
  }
}

void CompositeShape::rotate(double angle) noexcept
{
  double cosA = cos(angle * M_PI / 180);
  double sinA = sin(angle * M_PI / 180);
  point_t center = getXY();
  for (size_t i = 0; i < qtt_; i++)
  {
  point_t shape_center = shapes_[i]->getXY();
  shapes_[i]->move({center.x + cosA * (shape_center.x - center.x) - sinA * (shape_center.y - center.y),
  center.y + cosA * (shape_center.y - center.y) + sinA * (shape_center.x - center.x)});
  shapes_[i]->rotate(angle);
  }
}

size_t CompositeShape::getQtt() const noexcept
{
  return qtt_;
}

point_t CompositeShape::getXY() const noexcept
{
  return getFrameRect().pos;
}

std::string CompositeShape::getName() const noexcept
{
  return "CompositeShape";
}

void CompositeShape::addShape (const std::shared_ptr<Shape> &newShape)
{
  if (!newShape)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  for (size_t i = 0; i < qtt_; ++i)
  {
    if (shapes_[i] == newShape)
    {
      throw std::invalid_argument("Shape exists");
    }
  }
  if (newShape.get() == this)
  {
    throw std::invalid_argument("This shape already exists");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> temp (new std::shared_ptr<Shape>[qtt_+1]);
  for (size_t i = 0; i < qtt_; i++)
  {
    temp[i] = shapes_[i];
  }
  temp[qtt_] = newShape;
  shapes_.swap(temp);
  qtt_++;
}

void CompositeShape::deleteShape (const unsigned int numb)
{
  if (numb >= qtt_)
  {
    throw std::invalid_argument("Number is too large");
  }
  if (qtt_ <= 0)
  {
    throw std::invalid_argument("Shapes list is empty");
  }
  if (qtt_ == 1)
  {
    clear();
    return;
  }
  else
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> temp (new std::shared_ptr<Shape>[qtt_-1]);
    for (size_t i = 0; i < numb; i++)
    {
      temp[i] = shapes_[i];
    }
    for (size_t i = numb; i < qtt_-1; i++)
    {
      temp[i] = shapes_[i+1];
    }
    shapes_.swap(temp);
    qtt_--;
  }
}

void CompositeShape::clear() noexcept
{
  shapes_.reset();
  shapes_ = nullptr;
  qtt_ = 0;
}

CompositeShape &CompositeShape::operator= (const CompositeShape & rhs)
{
  if (this != &rhs)
  {
    qtt_ = rhs.getQtt();
    std::unique_ptr <std::shared_ptr <Shape>[]> clone (new std::shared_ptr <Shape>[rhs.getQtt()]);
    for (size_t i = 0; i < rhs.getQtt(); i++)
    {
      clone[i] = rhs.shapes_[i];
    }
    shapes_.swap(clone);
  }
  return *this;
}

CompositeShape &CompositeShape::operator=(CompositeShape &&rhs) noexcept
{
  if (this != &rhs)
  {
    qtt_ = rhs.qtt_;
    shapes_.swap(rhs.shapes_);
    shapes_.reset();
    rhs.qtt_ = 0;
  }
  return *this;
}

std::shared_ptr<Shape> CompositeShape::operator[](const unsigned int index) const
{
  if (index >= qtt_)
  {
    throw std::out_of_range("Invalid index");
  }
  return shapes_[index];
}
