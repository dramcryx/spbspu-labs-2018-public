#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace kramarov
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape &figure);
    CompositeShape(CompositeShape &&figure) noexcept;
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move (const point_t & newLocation) noexcept override;
    void move (const double dx, const double dy) noexcept override;
    void scale (const double scaleRate) override;
    void rotate(double angle) noexcept override;

    void addShape (const std::shared_ptr<Shape> &newShape);
    void deleteShape (const unsigned int numb);

    void clear() noexcept;

    size_t getQtt() const noexcept;
    point_t getXY() const noexcept;
    std::string getName() const noexcept override;

    CompositeShape &operator= (const CompositeShape & rhs);
    CompositeShape &operator=(CompositeShape &&rhs) noexcept;
    std::shared_ptr<Shape> operator[](const unsigned int index) const;
  private:
    std::unique_ptr <std::shared_ptr <Shape>[]> shapes_;
    size_t qtt_;
  };
}

#endif
