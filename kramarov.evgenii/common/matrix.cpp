#include "matrix.hpp"
#include <stdexcept>
#include <iostream>
#include <memory>
#include <iomanip>

using namespace kramarov;

Matrix::Matrix():
  rows_(0),
  columns_(0),
  groups_(nullptr)
{
}

Matrix::Matrix(const Matrix &figure):
  rows_(figure.rows_),
  columns_(figure.columns_),
  groups_(new std::shared_ptr<Shape> [figure.rows_ * figure.columns_])
{
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    groups_[i] = figure.groups_[i];
  }
}

Matrix::Matrix(Matrix &&figure) noexcept:
  rows_(figure.rows_),
  columns_(figure.columns_),
  groups_(nullptr)
{
  groups_.swap(figure.groups_);
  figure.rows_ = 0;
  figure.columns_ = 0;
}

Matrix &Matrix::operator=(const Matrix &rhs)
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    std::unique_ptr <std::shared_ptr <Shape>[]> clone (new std::shared_ptr <Shape>[rows_ * columns_]);
    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      clone[i] = rhs.groups_[i];
    }
    groups_.swap(clone);
  }
  return *this;
}

Matrix &Matrix::operator=(Matrix &&rhs) noexcept
{
  if (this != &rhs)
  {
    rows_ = rhs.rows_;
    columns_ = rhs.columns_;
    groups_.reset();
    groups_.swap(rhs.groups_);
    rhs.rows_ = 0;
    rhs.columns_ = 0;
  }
  return *this;
}

std::unique_ptr<std::shared_ptr<Shape>[]>::pointer Matrix::operator[](size_t index) const
{
  if (index > rows_)
  {
    throw std::invalid_argument("Index is too big");
  }
  return groups_.get() + index * columns_;
}

void Matrix::addShape(const std::shared_ptr<Shape> &shape)
{
  if (!shape)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  if ((rows_ == 0) && (columns_ == 0))
  {
    rows_ = 1;
    columns_ = 1;
    std::unique_ptr<std::shared_ptr<Shape>[]> copyMatrix (new std::shared_ptr<Shape>[rows_ * columns_]);
    groups_.swap(copyMatrix);
    groups_[0] = shape;
    return;
  }
  size_t i = rows_ * columns_;
  size_t desired_row = 1;
  while (i > 0)
  {
    i --;
    if (overlaying(groups_[i], shape))
    {
      desired_row = i / columns_ + 2;
    }
  }
  size_t rows_temp = rows_;
  size_t columns_temp = columns_;
  size_t free_columns = 0;
  if (desired_row > rows_)
  {
    rows_temp ++;
    free_columns = columns_;
  }
  else
  {
    size_t j = (desired_row - 1) * columns_;
    while (j < (desired_row * columns_))
    {
      if (groups_[j] == nullptr)
      {
        free_columns ++;
      }
      j ++;
    }

    if (free_columns == 0)
    {
      columns_temp ++;
      free_columns = 1;
    }
  }
  std::unique_ptr <std::shared_ptr<Shape>[]> temp(new std::shared_ptr<Shape>[rows_temp * columns_temp]);
  for (size_t i = 0; i < rows_temp; i ++)
  {
    for (size_t j = 0; j < columns_temp; j ++)
    {
      if (i >= rows_ || j >= columns_)
      {
        temp[i * columns_temp + j] = nullptr;
        continue;
      }

      temp[i * columns_temp + j] = groups_[i * columns_ + j];
    }
  }
  temp[desired_row * columns_temp - free_columns] = shape;
  groups_.swap(temp);
  rows_ = rows_temp;
  columns_ = columns_temp;
}

bool Matrix::overlaying(const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> &shape2) noexcept
{
  if (!shape1 || !shape2)
  {
    return false;
  }
  return (abs(shape1->getXY().x - shape2->getXY().x) < ((shape1->getFrameRect().width +
  shape2->getFrameRect().width) / 2) && (abs(shape1->getXY().y - shape2->getXY().y) <
  (shape1->getFrameRect().height + shape2->getFrameRect().height) / 2));
}

void Matrix::printInfo() noexcept
{
  if (rows_ == 0 && columns_ == 0)
  {
    std::cout << "Matrix is empty" << '\n';
  }
  else
  {
  for (size_t i = 0; i < rows_; i++)
  {
    for (size_t j = 0; j < columns_; j++)
    {
      if (groups_[i * columns_ + j])
      {
        std::cout << std::setw(10) << groups_[i * columns_ + j] -> getName();
      }
    }
    std::cout << '\n';
  }
  }
}
