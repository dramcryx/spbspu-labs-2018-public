#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include <memory>

namespace kramarov
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix &figure);
    Matrix(Matrix &&figure) noexcept;
    Matrix &operator=(const Matrix &rhs);
    Matrix &operator=(Matrix &&rhs) noexcept;
    std::unique_ptr<std::shared_ptr<Shape>[]>::pointer operator[](const size_t index) const;
    void addShape(const std::shared_ptr<Shape> &shape);
    void printInfo() noexcept;
  private:
    size_t rows_;
    size_t columns_;
    std::unique_ptr<std::shared_ptr<Shape>[]> groups_;
    bool overlaying(const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> &shape2) noexcept;
  };
}

#endif
