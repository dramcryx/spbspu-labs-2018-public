#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>
#include <iostream>

using namespace kramarov;

Rectangle::Rectangle(const double height, const double width, const point_t & center):
  height_(height),
  width_(width),
  center_(center),
  points_ {{center.x - width_ / 2, center.y + height_ / 2},
  {center.x - width_ / 2, center.y - height_ / 2},
  {center.x + width_ / 2, center.y - height_ / 2},
  {center.x + width_ / 2, center.y + height_ / 2}}
{
  if ((height < 0.0) || (width < 0.0))
  {
    throw std::invalid_argument ("Such rectangle doesn't exist");
  }
}

double Rectangle::getArea() const noexcept
{
  return height_ * width_;
}

rectangle_t Rectangle::getFrameRect() const noexcept
{
  double top = center_.y;
  double bottom = center_.y;
  double right = center_.x;
  double left = center_.x;
  for (size_t i = 0; i < 4; i++)
  {
    if (points_[i].x < left)
    {
      left = points_[i].x;
    }
    if (points_[i].y > top)
    {
      top = points_[i].y;
    }
    if (points_[i].x > right)
    {
      right = points_[i].x;
    }
    if (points_[i].y < bottom)
    {
      bottom = points_[i].y;
    }
  }
  return {center_, top - bottom, right - left};
}

void Rectangle::move (const point_t & newLocation) noexcept
{
  move(newLocation.x - center_.x, newLocation.y - center_.y);
}

void Rectangle::move (const double dx, const double dy) noexcept
{
  center_.x += dx;
  center_.y += dy;
  for (size_t i = 0; i < 4; i++)
  {
    points_[i].x += dx;
    points_[i].y += dy;
  }
}

void Rectangle::scale (const double scaleRate)
{
  if (scaleRate < 0.0)
  {
    throw std::invalid_argument ("Scale rate must be above 0");
  }
  height_ *= scaleRate;
  width_ *= scaleRate;
  for (size_t i = 0; i < 4; i++)
  {
    points_[i] = {center_.x + scaleRate * (points_[i].x - center_.x),
    center_.y + scaleRate * (points_[i].y - center_.y)};
  }
}

point_t kramarov::Rectangle::getXY() const noexcept
{
  return getFrameRect().pos;
}

void Rectangle::rotate(double angle) noexcept
{
  angle = angle * M_PI / 180;
  double cosA = cos(angle);
  double sinA = sin(angle);
  for (size_t i = 0; i < 4; i++)
  {
    points_[i] = {center_.x + cosA * (points_[i].x - center_.x) - sinA * (points_[i].y - center_.y),
    center_.y + cosA * (points_[i].y - center_.y) + sinA * (points_[i].x - center_.x)};
  }
}

std::string Rectangle::getName() const noexcept
{
  return "Rectangle";
}
