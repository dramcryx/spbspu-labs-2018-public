#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace kramarov
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const double height, const double width, const point_t & center);
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move (const point_t & newLocation) noexcept override;
    void move (const double dx, const double dy) noexcept override;
    void scale (const double scaleRate) override;
    point_t getXY() const noexcept override;
    void rotate(double angle) noexcept override;
    std::string getName() const noexcept;
  private:
    double height_;
    double width_;
    point_t center_;
    point_t points_[4];
  };
}

#endif
