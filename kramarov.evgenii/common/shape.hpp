#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <string>
#include "base-types.hpp"

namespace kramarov
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual rectangle_t getFrameRect() const noexcept = 0;
    virtual double getArea() const noexcept = 0;
    virtual void move (const double dx, const double dy) noexcept = 0;
    virtual void move (const point_t & newLocation) noexcept = 0;
    virtual void scale (const double scaleRate) = 0;
    virtual point_t getXY() const noexcept = 0;
    virtual void rotate(double angle) noexcept = 0;
    virtual std::string getName() const noexcept = 0;
  };
}

#endif
