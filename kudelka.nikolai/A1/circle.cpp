#include "circle.hpp"
#include <cassert>
#include <cmath>

Circle::Circle(const point_t & position, const double radius):
  m_pos(position),
  m_radius(radius)
{
  assert(radius >= 0);
}

double Circle::getArea() const
{
  return (M_PI * m_radius * m_radius);
}

rectangle_t Circle::getFrameRect() const
{
  return {m_pos, m_radius * 2, m_radius * 2};
}

void Circle::move(const point_t &newPoint)
{
  m_pos = newPoint;
}

void Circle::move(const double shiftInOx, const double shiftInOy)
{
  m_pos.x += shiftInOx;
  m_pos.y += shiftInOy;
}
