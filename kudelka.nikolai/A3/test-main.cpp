#define BOOST_TEST_MODULE COMPOSITE_SHAPE
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using namespace kudelka;

const double EPSILON = 1e-6;

BOOST_AUTO_TEST_CASE(CopyConstructorTest)
{
  CompositeShape cs2;

  std::shared_ptr< Rectangle > rectangle = std::make_shared< Rectangle >(rectangle_t{ {2.0, 4.0}, 8.0, 16.0 }, 0.0);
  cs2.add(rectangle);

  std::shared_ptr< Circle > circle = std::make_shared< Circle >(point_t{32.0, 64.0}, 12.8, 0.0);
  cs2.add(circle);

  CompositeShape cs1{cs2};

  BOOST_CHECK_EQUAL(cs1.getArea(), cs2.getArea());
  BOOST_CHECK_EQUAL(cs1.getSize(), cs2.getSize());
  rectangle_t rectangle1 = cs1.getFrameRect();
  rectangle_t rectangle2 = cs2.getFrameRect();
  BOOST_CHECK_EQUAL(rectangle1.width, rectangle2.width);
  BOOST_CHECK_EQUAL(rectangle1.height, rectangle2.height);
  BOOST_CHECK_EQUAL(rectangle1.pos.x, rectangle2.pos.x);
  BOOST_CHECK_EQUAL(rectangle1.pos.y, rectangle2.pos.y);
}

BOOST_AUTO_TEST_CASE(MoveConstructorTest)
{
  CompositeShape cs2;
  
  std::shared_ptr< Rectangle > rectangle = std::make_shared< Rectangle >(rectangle_t{ {2.0, 4.0}, 8.0, 16.0}, 0.0 );
  cs2.add(rectangle);

  std::shared_ptr< Circle > circle = std::make_shared< Circle >(point_t{32.0, 64.0}, 12.8, 0.0);
  cs2.add(circle);

  double beforeMovingArea = cs2.getArea();
  size_t beforeMovingSize = cs2.getSize();
  rectangle_t rectangle2 = cs2.getFrameRect();

  CompositeShape cs1{ std::move(cs2) };

  BOOST_CHECK_EQUAL(cs1.getArea(), beforeMovingArea);
  BOOST_CHECK_EQUAL(cs1.getSize(), beforeMovingSize);
  rectangle_t rectangle1 = cs1.getFrameRect();
  BOOST_CHECK_EQUAL(rectangle1.width, rectangle2.width);
  BOOST_CHECK_EQUAL(rectangle1.height, rectangle2.height);
  BOOST_CHECK_EQUAL(rectangle1.pos.x, rectangle2.pos.x);
  BOOST_CHECK_EQUAL(rectangle1.pos.y, rectangle2.pos.y);

  BOOST_CHECK_EQUAL(cs2.getArea(), 0.0);
  BOOST_CHECK_EQUAL(cs2.getSize(), 0.0);
  rectangle2 = cs2.getFrameRect();
  BOOST_CHECK_EQUAL(rectangle2.width, 0.0);
  BOOST_CHECK_EQUAL(rectangle2.height, 0.0);
  BOOST_CHECK_EQUAL(rectangle2.pos.x, 0.0);
  BOOST_CHECK_EQUAL(rectangle2.pos.y, 0.0);
}

BOOST_AUTO_TEST_CASE(AssignmentCopyTest)
{
  CompositeShape cs1, cs2;
  
  std::shared_ptr< Rectangle > rectangle = std::make_shared< Rectangle >(rectangle_t{ {2.0, 4.0}, 8.0, 16.0 }, 0.0);
  cs2.add(rectangle);

  std::shared_ptr< Circle > circle = std::make_shared< Circle >(point_t{32.0, 64.0}, 12.8, 0.0);
  cs2.add(circle);

  cs1 = cs2;

  BOOST_CHECK_EQUAL(cs1.getArea(), cs2.getArea());
  BOOST_CHECK_EQUAL(cs1.getSize(), cs2.getSize());
  rectangle_t rectangle1 = cs1.getFrameRect();
  rectangle_t rectangle2 = cs2.getFrameRect();
  BOOST_CHECK_EQUAL(rectangle1.width, rectangle2.width);
  BOOST_CHECK_EQUAL(rectangle1.height, rectangle2.height);
  BOOST_CHECK_EQUAL(rectangle1.pos.x, rectangle2.pos.x);
  BOOST_CHECK_EQUAL(rectangle1.pos.y, rectangle2.pos.y);
}

BOOST_AUTO_TEST_CASE(AssignmentMoveTest)
{
  CompositeShape cs1, cs2;
  
  std::shared_ptr< Rectangle > rectangle = std::make_shared< Rectangle >(rectangle_t{ {2.0, 4.0}, 8.0, 16.0 }, 0.0);
  cs2.add(rectangle);

  std::shared_ptr< Circle > circle = std::make_shared< Circle >(point_t{32.0, 64.0}, 12.8, 0.0);
  cs2.add(circle);
  
  double beforeMovingArea = cs2.getArea();
  size_t beforeMovingSize = cs2.getSize();
  rectangle_t rectangle2 = cs2.getFrameRect();
  
  cs1 = std::move(cs2);

  BOOST_CHECK_EQUAL(cs1.getArea(), beforeMovingArea);
  BOOST_CHECK_EQUAL(cs1.getSize(), beforeMovingSize);
  rectangle_t rectangle1 = cs1.getFrameRect();
  BOOST_CHECK_EQUAL(rectangle1.width, rectangle2.width);
  BOOST_CHECK_EQUAL(rectangle1.height, rectangle2.height);
  BOOST_CHECK_EQUAL(rectangle1.pos.x, rectangle2.pos.x);
  BOOST_CHECK_EQUAL(rectangle1.pos.y, rectangle2.pos.y);

  BOOST_CHECK_EQUAL(cs2.getArea(), 0.0);
  BOOST_CHECK_EQUAL(cs2.getSize(), 0.0);
  rectangle2 = cs2.getFrameRect();
  BOOST_CHECK_EQUAL(rectangle2.width, 0.0);
  BOOST_CHECK_EQUAL(rectangle2.height, 0.0);
  BOOST_CHECK_EQUAL(rectangle2.pos.x, 0.0);
  BOOST_CHECK_EQUAL(rectangle2.pos.y, 0.0);
}

BOOST_AUTO_TEST_CASE(IndexingOperatorTest)
{
  CompositeShape cs;
  
  const double width = 8.0, height = 16.0;
  std::shared_ptr< Rectangle > rectangle = std::make_shared< Rectangle >(rectangle_t{ {2.0, 4.0}, width, height }, 0.0);
  cs.add(rectangle);

  const double radius = 12.8;
  std::shared_ptr< Circle > circle = std::make_shared< Circle >(point_t{32.0, 64.0}, radius, 0.0);
  cs.add(circle);
  
  BOOST_CHECK_CLOSE(cs[0]->getArea(), width * height, EPSILON);
  BOOST_CHECK_CLOSE(cs[1]->getArea(), M_PI * radius * radius, EPSILON);
}

BOOST_AUTO_TEST_CASE(InvalidIndexindOperatorTest)
{
  CompositeShape cs;
  BOOST_CHECK_THROW(cs[0], std::out_of_range);

  std::shared_ptr< Rectangle > rectangle = std::make_shared< Rectangle >(rectangle_t{ {2.0, 4.0}, 8.0, 16.0 }, 0.0);
  cs.add(rectangle);

  BOOST_CHECK_THROW(cs[1], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(GetAreaTest)
{
  CompositeShape cs;
  
  const double width = 8.0, height = 16.0;
  std::shared_ptr< Rectangle > rectangle = std::make_shared< Rectangle >(rectangle_t{ {10.0, 20.0}, width, height }, 0.0);
  cs.add(rectangle);

  const double radius = 12.8;
  std::shared_ptr< Circle > circle = std::make_shared< Circle >(point_t{32.0, 64.0}, radius, 0.0);
  cs.add(circle);

  BOOST_CHECK_CLOSE(cs.getArea(), width * height + M_PI * radius * radius, EPSILON);
}

BOOST_AUTO_TEST_CASE(GetFrameRectTest)
{
  CompositeShape cs;
  
  std::shared_ptr< Rectangle > rectangle = std::make_shared< Rectangle >(rectangle_t{ {2.0, 4.0}, 8.0, 16.0}, 0.0 );
  cs.add(rectangle);

  rectangle_t frameRect = cs.getFrameRect();

  BOOST_CHECK_CLOSE(frameRect.pos.x, 2.0, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.pos.y, 4.0, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.width, 8.0, EPSILON);
  BOOST_CHECK_CLOSE(frameRect.height, 16.0, EPSILON);
}

BOOST_AUTO_TEST_CASE(RelativeMoveTest)
{
  CompositeShape cs;
  
  const rectangle_t rectanCenter = {{ 10.0, 20.0}, 10.0, 12.0 };
  std::shared_ptr< Rectangle > rectangle = std::make_shared< Rectangle >(rectanCenter, 0.0);
  cs.add(rectangle);

  const point_t circleCenter = {10.0, 20.0};
  const double radius = 5.0;
  std::shared_ptr< Circle > circle = std::make_shared< Circle >(circleCenter, radius, 0.0);
  cs.add(circle);

  const rectangle_t compositeRect = cs.getFrameRect();

  const double shiftInOx = 15.0, shiftInOy = 25.0;
  cs.move(shiftInOx, shiftInOy);

  rectangle_t afterMove = cs.getFrameRect();
  BOOST_CHECK_CLOSE(afterMove.pos.x, compositeRect.pos.x + shiftInOx, EPSILON);
  BOOST_CHECK_CLOSE(afterMove.pos.x, compositeRect.pos.x + shiftInOx, EPSILON);

  afterMove = cs[0]->getFrameRect(); // Rectangle
  BOOST_CHECK_CLOSE(afterMove.pos.x, rectanCenter.pos.x + shiftInOx, EPSILON);
  BOOST_CHECK_CLOSE(afterMove.pos.y, rectanCenter.pos.y + shiftInOy, EPSILON);
  BOOST_CHECK_CLOSE(cs[0]->getArea(), rectanCenter.width * rectanCenter.height, EPSILON);

  afterMove = cs[1]->getFrameRect(); // Circle
  BOOST_CHECK_CLOSE(afterMove.pos.x, circleCenter.x + shiftInOx, EPSILON);
  BOOST_CHECK_CLOSE(afterMove.pos.y, circleCenter.y + shiftInOy, EPSILON);
  BOOST_CHECK_CLOSE(cs[1]->getArea(), M_PI * radius * radius, EPSILON);
}

BOOST_AUTO_TEST_CASE(MovingToPointTest)
{
  std::shared_ptr<Shape> rectangle = std::make_shared<Rectangle>(rectangle_t{{2.0, 4.0}, 8.0, 16.0 }, 0.0);
  std::shared_ptr<Shape> circle = std::make_shared<Circle>(point_t{32.0, 64.0},12.8, 0.0);
  CompositeShape cs;
  cs.add(rectangle);
  cs.add(circle);
  double tmpHeight = cs.getFrameRect().height;
  double tmpWidth = cs.getFrameRect().width;
  double tmpArea = cs.getArea();
  point_t positionAfterMoving = {256.0, 512.0};

  cs.move(positionAfterMoving);

  BOOST_CHECK_CLOSE(cs.getFrameRect().pos.x, positionAfterMoving.x, 1e-6);
  BOOST_CHECK_CLOSE(cs.getFrameRect().pos.y, positionAfterMoving.y, 1e-6);
  BOOST_CHECK_CLOSE(tmpHeight, cs.getFrameRect().height, 1e-6);
  BOOST_CHECK_CLOSE(tmpWidth, cs.getFrameRect().width, 1e-6);
  BOOST_CHECK_CLOSE(tmpArea, cs.getArea(), 1e-6);
}

BOOST_AUTO_TEST_CASE(ScalingTest)
{
  CompositeShape cs;
  
  const rectangle_t rectangle = { {2.0, 4.0}, 8.0, 16.0};
  std::shared_ptr< Rectangle > rect = std::make_shared< Rectangle >(rectangle, 0.0);
  cs.add(rect);

  const point_t circleCenter = {-15.0, 30.0};
  const double radius = 5.0;
  std::shared_ptr< Circle > circle = std::make_shared< Circle >(circleCenter, radius, 0.0);
  cs.add(circle);

  point_t compositeCenter = cs.getFrameRect().pos;

  const double coefficient = 12.0;
  cs.scale(coefficient);

  rectangle_t afterScale = cs.getFrameRect();
  BOOST_CHECK_CLOSE(compositeCenter.x, afterScale.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(compositeCenter.y, afterScale.pos.y, EPSILON);

  afterScale = cs[0]->getFrameRect(); // Rectangle
  BOOST_CHECK_CLOSE(afterScale.pos.x, rectangle.pos.x + (rectangle.pos.x - compositeCenter.x) * (coefficient - 1), EPSILON);
  BOOST_CHECK_CLOSE(afterScale.pos.y, rectangle.pos.y + (rectangle.pos.y - compositeCenter.y) * (coefficient - 1), EPSILON);
  BOOST_CHECK_CLOSE(cs[0]->getArea(), rectangle.width * rectangle.height * coefficient * coefficient, EPSILON);

  afterScale = cs[1]->getFrameRect(); // Circle
  BOOST_CHECK_CLOSE(afterScale.pos.x, circleCenter.x + (circleCenter.x - compositeCenter.x) * (coefficient - 1), EPSILON);
  BOOST_CHECK_CLOSE(afterScale.pos.y, circleCenter.y + (circleCenter.y - compositeCenter.y) * (coefficient - 1), EPSILON);
  BOOST_CHECK_CLOSE(cs[1]->getArea(), M_PI * radius * radius * coefficient * coefficient, EPSILON);
}

BOOST_AUTO_TEST_CASE(InvalidScaleTest)
{
  CompositeShape cs;

  BOOST_CHECK_THROW(cs.scale(-0.5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(Size)
{
  CompositeShape cs;
  BOOST_CHECK_EQUAL(cs.getSize(), 0);
  
  std::shared_ptr< Rectangle > rectangle = std::make_shared< Rectangle >(rectangle_t{ {2.0, 4.0}, 8.0, 16.0 }, 0.0);
  cs.add(rectangle);
  BOOST_CHECK_EQUAL(cs.getSize(), 1);

  std::shared_ptr< Circle > circle = std::make_shared< Circle >(point_t{32.0, 64.0}, 12.8, 0.0);
  cs.add(circle);
  BOOST_CHECK_EQUAL(cs.getSize(), 2);
}

BOOST_AUTO_TEST_CASE(Clear)
{
  CompositeShape cs;
  
  std::shared_ptr< Rectangle > rect = std::make_shared< Rectangle >(rectangle_t{ {2.0, 4.0}, 8.0, 16.0 }, 0.0);
  cs.add(rect);

  std::shared_ptr< Circle > circle = std::make_shared< Circle >(point_t{32.0, 64.0}, 12.8, 0.0);
  cs.add(circle);

  cs.clear();
  BOOST_CHECK_EQUAL(cs.getSize(), 0);
  BOOST_CHECK_THROW(cs[0], std::out_of_range);
}
