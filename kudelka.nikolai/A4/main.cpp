#include <iostream>
#include "circle.hpp"
#include "composite-shape.hpp"
#include "rectangle.hpp"

void outputInfo(const kudelka::Shape & shape);

int main ()
{
  kudelka::CompositeShape compositeShape;
  std::shared_ptr< kudelka::Rectangle > rect =
      std::make_shared< kudelka::Rectangle >(kudelka::rectangle_t{{2.0, 4.0}, 8.0, 16.0 }, 0.0);
  compositeShape.add(rect);
  std::cout << "\tBefore added cicle:" << std::endl;
  outputInfo(compositeShape);
  std::shared_ptr< kudelka::Circle > circle =
      std::make_shared< kudelka::Circle >(kudelka::point_t{2.0, 4.0 }, 8.0, 0.0);
  compositeShape.add(circle);
  std::cout << "\tAfter added cicle:" << std::endl;
  outputInfo(compositeShape);
  std::cout << "\tAfter scaling:" << std::endl;
  compositeShape.scale(6.4);
  outputInfo(compositeShape);
  std::cout << "\tAfter moving:" << std::endl;
  compositeShape.move(12.8, 25.6);
  outputInfo(compositeShape);
  std::cout << "\tAfter rotate" << std::endl;
  compositeShape.rotate(50.0);
  outputInfo(compositeShape);
  return 0;
}

void outputInfo(const kudelka::Shape & shape)
{
  kudelka::rectangle_t frameRect;
  frameRect = shape.getFrameRect();
  std::cout << "\t\tFrameRect:" << std::endl
      << "\t\t\tPosition:\t(" << frameRect.pos.x << ";" << frameRect.pos.y << ")" << std::endl
      << "\t\t\tWidth:\t\t" << frameRect.width << std::endl
      << "\t\t\tHeight:\t\t" << frameRect.height << std::endl
      << "\t\tArea:\t\t\t" << shape.getArea() << std::endl << std::endl;
}
