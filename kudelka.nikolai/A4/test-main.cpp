#define BOOST_TEST_MAIN
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using namespace kudelka;

const double EPSILON = 1e-6;

BOOST_AUTO_TEST_SUITE(RectangleTest)
  BOOST_AUTO_TEST_CASE(Rotate)
  {
    const rectangle_t s_rect = { {2.0, 5.0}, 7.0, 8.0 };
    const double angle = 135.0;
    Rectangle rect{ s_rect, 0.0 };
    
    point_t oldPos ={2.0, 5.0};
    double oldArea;
    oldArea = rect.getArea();

    rect.rotate(angle);

    BOOST_CHECK_CLOSE(oldPos.x, rect.getFrameRect().pos.x, EPSILON);
    BOOST_CHECK_CLOSE(oldPos.y, rect.getFrameRect().pos.y, EPSILON);
    BOOST_CHECK_CLOSE(oldArea, rect.getArea(), EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CircleTest)
  BOOST_AUTO_TEST_CASE(Rotate)
  {
    Circle circle{ {2.0, 3.0}, 5.0, 0.0};

    circle.rotate(34.0);

    BOOST_CHECK_EQUAL(circle.getFrameRect().pos.x, 2.0);
    BOOST_CHECK_EQUAL(circle.getFrameRect().pos.y, 3.0);
    BOOST_CHECK_CLOSE(circle.getFrameRect().width, 10.0, EPSILON);
    BOOST_CHECK_CLOSE(circle.getFrameRect().height, 10.0, EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompostieShapeTest)
  BOOST_AUTO_TEST_CASE(Rotate)
  {
    std::shared_ptr<Shape> rect1 =
      std::make_shared<Rectangle>(Rectangle({ {2.0, 5.0}, 7.0, 8.0 }, 0.0));
    CompositeShape cs;
    cs.add(rect1);
    double oldArea = cs.getArea();
    rectangle_t tmpcs = cs.getFrameRect();
    cs.rotate(180.0);
    BOOST_CHECK_CLOSE_FRACTION(tmpcs.width, cs.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(tmpcs.height, cs.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(cs.getArea(), oldArea, EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()
