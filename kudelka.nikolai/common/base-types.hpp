#ifndef BASE_TYPES_HPP
#define BASE_TYPES_HPP

namespace kudelka
{
  struct point_t
  {
    double x;
    double y;
    void rotate(double angle);
  };

  struct rectangle_t
  {
    point_t pos;
    double height;
    double width;
    bool intersection(const rectangle_t & rhs);
  };
}

#endif
