#include "circle.hpp"
#include <cmath>
#include <stdexcept>

kudelka::Circle::Circle(const point_t & position, const double radius, const double angle):
  m_pos(position),
  m_radius(radius),
  m_angle(angle)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument("Radius must be non-negative");
  }
}

double kudelka::Circle::getArea() const
{
  return (M_PI * m_radius * m_radius);
}

kudelka::rectangle_t kudelka::Circle::getFrameRect() const
{
  return {m_pos, m_radius * 2, m_radius * 2};
}

void kudelka::Circle::move(const point_t &newPoint)
{
  m_pos = newPoint;
}

void kudelka::Circle::move(const double shiftInOx, const double shiftInOy)
{
  m_pos.x += shiftInOx;
  m_pos.y += shiftInOy;
}

void kudelka::Circle::scale(const double coefficient)
{
  if (coefficient < 0.0)
  {
    throw std::invalid_argument("Coefficient must be non-negative");
  }
  m_radius *= coefficient;
}

void kudelka::Circle::rotate(const double angle)
{
  m_angle = std::fmod(m_angle + angle, 360.0);
}
