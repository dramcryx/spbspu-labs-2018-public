#include "composite-shape.hpp"
#include <iostream>
#include <stdexcept>

kudelka::CompositeShape::CompositeShape() noexcept:
  m_array(),
  m_size(0)
{}

kudelka::CompositeShape::CompositeShape(const CompositeShape & rhs):
  m_array(new element_type[rhs.m_size]),
  m_size(rhs.m_size)
{
  for (size_t i = 0; i < rhs.m_size; i++)
  {
    m_array[i] = rhs.m_array[i];
  }
}

kudelka::CompositeShape::CompositeShape(CompositeShape && rhs) noexcept:
  m_array(std::move(rhs.m_array)),
  m_size(rhs.m_size)
{
  rhs.m_size = 0;
}

kudelka::CompositeShape & kudelka::CompositeShape::operator= (const kudelka::CompositeShape & rhs)
{
  if (this == &rhs) {
    return *this;
  }
  m_size = rhs.m_size;
  std::unique_ptr < element_type[] >tempShape(new element_type[rhs.m_size]);
  for (size_t i = 0; i < m_size; i++)
  {
    tempShape[i] = rhs.m_array[i];
  }
  m_array.swap(tempShape);
  return *this;
}

kudelka::CompositeShape & kudelka::CompositeShape::operator= (kudelka::CompositeShape && rhs) noexcept
{
  m_array = std::move(rhs.m_array);
  m_size = rhs.m_size;
  rhs.m_size = 0;
  return *this;
}

kudelka::CompositeShape::element_type & kudelka::CompositeShape::operator[] (const size_t index) const
{
  if (index >= m_size) {
    throw std::out_of_range("The index can not be larger than the array size...");
  }
  return m_array[index];
}

double kudelka::CompositeShape::getArea() const noexcept
{
  double sumArea = 0;
  for (size_t i = 0; i < m_size; i++)
  {
    sumArea += m_array[i]->getArea();
  }
  return sumArea;
}

kudelka::rectangle_t kudelka::CompositeShape::getFrameRect() const noexcept
{
  if (m_size == 0) {
    return {0.0, 0.0, 0.0, 0.0};
  }
  rectangle_t rectangle = m_array[0]->getFrameRect();
  double left = rectangle.pos.x - rectangle.width / 2;
  double right = rectangle.pos.x + rectangle.width / 2;
  double up = rectangle.pos.y + rectangle.height / 2;
  double down = rectangle.pos.y - rectangle.height / 2;


  for (size_t i = 1; i < m_size; i++)
  {
    rectangle = m_array[i]->getFrameRect();
    if ((rectangle.pos.x - rectangle.width / 2) < left)
    {
      left = rectangle.pos.x - rectangle.width / 2;
    }
    if ((rectangle.pos.x + rectangle.width / 2) > right)
    {
      right = rectangle.pos.x + rectangle.width / 2;
    }
    if ((rectangle.pos.y + rectangle.height / 2) > up)
    {
      up = rectangle.pos.y + rectangle.height / 2;
    }
    if ((rectangle.pos.y - rectangle.height / 2) < down)
    {
      down = rectangle.pos.y - rectangle.height / 2;
    }
  }
  return { {left + (right - left) / 2, down + (up - down) / 2}, right - left, up - down };
}

void kudelka::CompositeShape::move(const point_t &newPoint) noexcept
{
  point_t center = getFrameRect().pos;
  move(newPoint.x - center.x, newPoint.y - center.y);
}

void kudelka::CompositeShape::move(const double shiftInOx, const double shiftInOy) noexcept
{
  for (size_t i = 0; i < m_size; i++)
  {
    m_array[i]->move(shiftInOx, shiftInOy);
  };
}

void kudelka::CompositeShape::scale(const double coefficient)
{
  if (coefficient < 0.0) {
    throw std:: invalid_argument("Coefficient must be non-negative...");
  }
  point_t compositeCenter = getFrameRect().pos;
  for (size_t i = 0; i < m_size; i++)
  {
    point_t elementCenter = m_array[i]->getFrameRect().pos;
    m_array[i]->move((coefficient - 1) * (elementCenter.x - compositeCenter.x),
      (coefficient - 1)* (elementCenter.y - compositeCenter.y));
    m_array[i]->scale(coefficient);
  };
}

void kudelka::CompositeShape::rotate(const double angle)
{
  double sinAngle = sin(angle * M_PI / 180);
  double cosAngle = cos(angle * M_PI / 180);
  point_t chCenter = getFrameRect().pos;
  for(size_t i = 0; i < m_size; i++)
  {
    point_t shapePos = m_array[i] -> getFrameRect().pos;
    double newX = chCenter.x + (shapePos.x - chCenter.x) * cosAngle - (shapePos.y - chCenter.y) * sinAngle;
    double newY = chCenter.y + (shapePos.y - chCenter.y) * cosAngle + (shapePos.x - chCenter.x) * sinAngle;
    m_array[i] -> move({newX, newY});
    m_array[i] -> rotate(angle);
  }
}

void kudelka::CompositeShape::add(const element_type & shape)
{
  if (!shape) {
    throw std::invalid_argument("Shape added into CompositeShape must be not-NULL...");
  }
  std::unique_ptr< element_type[] > newShapes(new element_type[m_size + 1]);
  for (size_t i = 0; i < m_size; i++)
  {
    newShapes[i] = m_array[i];
  }
  newShapes[m_size] = shape;
  m_size++;
  m_array.swap(newShapes);
}

void kudelka::CompositeShape::remove(size_t index)
{
  if (index >= m_size) {
    throw std::out_of_range("The index can not be larger than the array size...");
  }
  if ((m_size == 1) && (index == 0))
  {
    m_array.reset();
    m_size = 0;
    return;
  }
  std::unique_ptr< element_type[] > newShapes(new element_type[m_size + 1]);
  for (size_t i = 0; i < index; i++)
  {
    newShapes[i] = m_array[i];
  }
  for (size_t i = index; i < m_size - 1; ++i)
  {
    newShapes[i] = m_array[i + 1];
  }
  m_array.swap(newShapes);
  m_size--;
}

size_t kudelka::CompositeShape::getSize() const noexcept
{
  return m_size;
}

void kudelka::CompositeShape::clear() noexcept
{
  m_array.reset();
  m_size = 0;
}
