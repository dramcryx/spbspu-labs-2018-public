#include "matrix.hpp"
#include <algorithm>
#include <stdexcept>

using namespace kudelka;

Matrix::Matrix():
  m_items(0),
  m_layers(0),
  m_layerSize(0)
{}

Matrix::Matrix(Matrix & rhs):
  m_items(new element_type[rhs.m_layers * rhs.m_layerSize]),
  m_layers(rhs.m_layers),
  m_layerSize(rhs.m_layerSize)
{
  for (std::size_t  i = 0; i < m_layers * m_layerSize; ++i)
  {
    m_items[i] = rhs.m_items[i];
  }
}

Matrix::Matrix(Matrix && rhs):
  m_items(std::move(rhs.m_items)),
  m_layers(rhs.m_layers),
  m_layerSize(rhs.m_layerSize)
{
  rhs.m_layerSize = 0;
  rhs.m_layers = 0;
}

Matrix & Matrix::operator=(const Matrix & rhs)
{
  if(this == &rhs)
  {
    return *this;
  }
  layer_type newShapes(new element_type[rhs.m_layers * rhs.m_layerSize]);
  m_layers = rhs.m_layers;
  m_layerSize = rhs.m_layerSize;
  for (std::size_t  i = 0; i < m_layers * m_layerSize; ++i)
  {
    newShapes[i] = rhs.m_items[i];
  }
  m_items.swap(newShapes);
  return *this;
}

Matrix & Matrix::operator=(Matrix && rhs)
{
  m_items = std::move(rhs.m_items);
  m_layers = rhs.m_layers;
  m_layerSize = rhs.m_layerSize;
  rhs.m_layerSize = 0;
  rhs.m_layers = 0;
  return *this;
}

Matrix::layer_type Matrix::operator[](const size_t index) const
{
  if (index >= m_layers)
  {
    throw std::out_of_range("Out of range Index!!!");
  }
  layer_type layer(new element_type[m_layerSize]);
  for (std::size_t i = 0; i < m_layerSize; ++i)
  {
    layer[i] = m_items[index * m_layerSize + i];
  }
  return layer;
}

void Matrix::add(const element_type & shape)
{
  if (!shape)
  {
    throw std::invalid_argument("Invalid shape!!");
  }
  if ((m_layers == 0) && (m_layerSize == 0))
  {
    layer_type newShapes(new element_type[(m_layers+1) * (m_layerSize)]);
    m_layers = m_layerSize = 1;
    m_items.swap(newShapes);
    m_items[0] = shape;
    return;
  }
  size_t i = 0;
  for (; i < m_layers; ++i)
  {
    size_t j = 0;
    for(; j < m_layerSize; ++j)
    {
      if (!m_items[i * m_layerSize + j])
      {
        m_items[i * m_layerSize + j] = shape;
        return;
      }
      if (intersection(m_items[i * m_layerSize + j], shape))
      {
        break;
      }
    }
    if (j == m_layerSize)
    {
      layer_type newShapes(new element_type[(m_layers+1) * (m_layerSize + 1)]);
      for (size_t k = 0; k < m_layers; ++k)
      {
        for (j = 0; j < m_layerSize; ++j)
        {
          newShapes[k * m_layerSize + j + k] = m_items[k * m_layerSize + j];
        }
      }
      ++m_layerSize;
      newShapes[(i + 1) * m_layerSize - 1] = shape;
      m_items = std::move(newShapes);
      return;
    }
  }
  if (i == m_layers)
  {
    layer_type newShapes(new element_type[(m_layers + 1) * (m_layerSize)]);
    for (size_t k = 0; k < m_layers * m_layerSize; ++k)
    {
      newShapes[k] = m_items[k];
    }
    newShapes[m_layers * m_layerSize] = shape;
    ++m_layers;
    m_items = std::move(newShapes);
  }
}

void Matrix::clean()
{
  m_items.reset();
  m_layers = 0;
  m_layerSize = 0;
}

size_t Matrix::getLayers() const
{
  return m_layers;
}

size_t Matrix::getLayerSize() const
{
  return m_layerSize;
}

bool Matrix::intersection(const element_type & lhs, const element_type & rhs)
{
  return lhs->getFrameRect().intersection(rhs->getFrameRect());
}
