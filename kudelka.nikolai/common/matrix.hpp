#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace kudelka
{
  class Matrix
  {
    public:
      using element_type = std::shared_ptr< Shape >;
      using layer_type = std::unique_ptr< element_type[] >;

      Matrix();
      Matrix(Matrix & rhs);
      Matrix(Matrix && rhs);

      Matrix & operator=(const Matrix & rhs);
      Matrix & operator=(Matrix && rhs);
      layer_type operator[](const size_t index) const;

      void add(const element_type & shape);
      void clean();
      size_t getLayers() const;
      size_t getLayerSize() const;
    private:
      layer_type m_items;
      size_t m_layers;
      size_t m_layerSize;
      static bool intersection(const element_type & lhs, const element_type & rhs);
  };   
}

#endif
