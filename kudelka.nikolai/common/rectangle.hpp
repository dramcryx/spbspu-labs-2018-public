#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include <cmath>
#include "base-types.hpp"
#include "shape.hpp"

namespace kudelka
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t & position, const double heigth, const double width, const double angle);
    Rectangle(const rectangle_t & rectangle, const double angle);
    virtual double getArea() const override;
    virtual rectangle_t getFrameRect() const override;
    virtual void move(const point_t &newPoint) override;
    virtual void move(const double shiftInOx, const double shiftInOy) override;
    virtual void scale(const double coefficient) override;
    virtual void rotate(const double angle) override;
  protected:
    point_t m_pos;
    double m_height;
    double m_width;
    double m_angle;
  };
}

#endif
