#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

namespace kudelka
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const point_t & newPoint) = 0;
    virtual void move(const double shiftInOx, const double shiftInOy) = 0;
    virtual void scale(const double coefficient) = 0;
      virtual void rotate(const double angle) = 0;
  };
}

#endif
