#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

using namespace kudryakov;

int main()
{
  Rectangle rectangle({-2,0}, 2, 2);
  Circle circle({2,0}, 1);
  
  std::shared_ptr <kudryakov::Shape> rectPtr = std::make_shared <kudryakov::Rectangle> (rectangle);
  std::shared_ptr <kudryakov::Shape> circPtr = std::make_shared <kudryakov::Circle> (circle);
  
  kudryakov::CompositeShape compositeShape(circPtr);
  compositeShape.addShape(rectPtr);
  
  std::cout << "Area: " << compositeShape.getArea() << std::endl;
  std::cout << "Pos X: " << compositeShape.getFrameRect().pos.x << std::endl
    << "Pos Y: " << compositeShape.getFrameRect().pos.y << std::endl;
  std::cout << "Width: " << compositeShape.getFrameRect().width << std::endl;
  std::cout << "Heigth: " << compositeShape.getFrameRect().height << std::endl;
  
  compositeShape.scale(2);
  std::cout << "Composite Shape was scaled by 2" << std::endl;
  
  std::cout << "Area: " << compositeShape.getArea() << std::endl;
  std::cout << "Pos X: " << compositeShape.getFrameRect().pos.x << std::endl
    << "Pos Y: " << compositeShape.getFrameRect().pos.y << std::endl;
  std::cout << "Width: " << compositeShape.getFrameRect().width << std::endl;
  std::cout << "Heigth: " << compositeShape.getFrameRect().height << std::endl;
  
  return 0;
}
