#include <iostream>
#include <memory>
#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

void printFrame(const kudryakov::Shape& shape)
{
  const kudryakov::rectangle_t frame = shape.getFrameRect();
  std::cout << "Centre: " << frame.pos.x << ", " << frame.pos.y << std::endl;
  std::cout << "Height: " << frame.height << std::endl;
  std::cout << "Width: " << frame.width << std::endl;
  std::cout << "Angle: " << shape.getAngle() << std::endl;
}



int main()
{
  {
    std::cout << "Rectangle rotate demonstration:" << std::endl;
    kudryakov::Rectangle rectangle({1, 2}, 4, 2);
    printFrame(rectangle);
    std::cout << "Rectangle rotate 30" << std::endl;
    rectangle.rotate(30);
    printFrame(rectangle);
    std::cout << std::endl;
    
    std::cout << "CompositeShape rotate demonstration:" << std::endl;
    std::shared_ptr<kudryakov::Shape> rectanglePtr = std::make_shared<kudryakov::Rectangle>(rectangle);
    kudryakov::Circle circle({0, 0}, 1);
    std::shared_ptr<kudryakov::Shape> circlePtr = std::make_shared<kudryakov::Circle>(circle);
  
    kudryakov::CompositeShape compositeShape(circlePtr);
    compositeShape.addShape(rectanglePtr);
    std::cout << "Whole CompositeShape:" << std::endl;
    printFrame(compositeShape);
    
    std::cout << "CompositeShape[0] (circle):" << std::endl;
    printFrame(*compositeShape[0]);
    std::cout << "CompositeShape[1] (rectangle):" << std::endl;
    printFrame(*compositeShape[1]);
    
    std::cout << "CompositeShape rotate 30" << std::endl;
    compositeShape.rotate(30);
    std::cout << "Whole Composite Shape:" << std::endl;
    printFrame(compositeShape);
  
    std::cout << "CompositeShape[0]:" << std::endl;
    printFrame(*compositeShape[0]);
    std::cout << "CompositeShape[1]:" << std::endl;
    printFrame(*compositeShape[1]);
  }
  
  {
    std::cout << "Matrix demonstration:" << std::endl;
    
    kudryakov::Circle circle({0, 0}, 1);
    kudryakov::Rectangle rectangle({1, 2}, 4, 2);
    kudryakov::Rectangle rectangle2({7, 7}, 4, 2);
    
    std::shared_ptr<kudryakov::Shape> circlePtr = std::make_shared<kudryakov::Circle>(circle);
    std::shared_ptr<kudryakov::Shape> rectanglePtr = std::make_shared<kudryakov::Rectangle>(rectangle);
    std::shared_ptr<kudryakov::Shape> rectangle2Ptr = std::make_shared<kudryakov::Rectangle>(rectangle2);
    
    kudryakov::CompositeShape compositeShape(circlePtr);
    compositeShape.addShape(rectanglePtr);
    
    kudryakov::Matrix matrix(compositeShape);
    matrix.addShape(rectangle2Ptr);
    std::unique_ptr<std::shared_ptr<kudryakov::Shape>[]> new_layer1 = matrix[0];
    std::unique_ptr<std::shared_ptr<kudryakov::Shape>[]> new_layer2 = matrix[1];
    
    if (new_layer1[0] == circlePtr)
    {
      std::cout << "First layer[1] == CIRCLE" << std::endl;
    }
    if (new_layer1[1] == rectangle2Ptr)
    {
      std::cout << "First layer[2] == RECTANGLE #2" << std::endl;
    }
    if (new_layer2[0] == rectanglePtr)
    {
      std::cout << "Second layer[1] == RECTANGLE #1" << std::endl;
    }
  }
  return 0;
}
