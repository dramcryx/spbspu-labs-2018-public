#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include <memory>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double Epsilon = 0.00001;



BOOST_AUTO_TEST_SUITE(RectangleTests)
  
  BOOST_AUTO_TEST_CASE(Rotation)
  {
    kudryakov::Rectangle rect({0,0}, 2, 1);
    rect.rotate(45);
    BOOST_CHECK_CLOSE(rect.getFrameRect().pos.x, 0, Epsilon);
    BOOST_CHECK_CLOSE(rect.getFrameRect().pos.y, 0, Epsilon);
    BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().height , 2.1213, Epsilon);
    BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().width , 2.1213, Epsilon);
  }
  
BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(CompositeShapeTests)
  
  BOOST_AUTO_TEST_CASE(Rotation)
  {
    kudryakov::Circle circ({0, 0}, 1);
    kudryakov::Rectangle rect({4, 0}, 2, 2);
    
    std::shared_ptr <kudryakov::Shape> circPtr = std::make_shared <kudryakov::Circle> (circ);
    std::shared_ptr <kudryakov::Shape> rectPtr = std::make_shared <kudryakov::Rectangle> (rect);
    
    kudryakov::CompositeShape composite_shape(circPtr);
    composite_shape.addShape(rectPtr);
    composite_shape.rotate(270);
    
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 2, Epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 0, Epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 6, Epsilon);
    BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 2, Epsilon);
    
    BOOST_CHECK_CLOSE(composite_shape[0]->getFrameRect().pos.x, 2, Epsilon);
    BOOST_CHECK_CLOSE(composite_shape[0]->getFrameRect().pos.y, 2, Epsilon);
    BOOST_CHECK_CLOSE(composite_shape[1]->getFrameRect().pos.x, 2, Epsilon);
    BOOST_CHECK_CLOSE(composite_shape[1]->getFrameRect().pos.y, -2, Epsilon);
    
    BOOST_CHECK_CLOSE(composite_shape.getArea(), (circ.getArea() + rect.getArea()), Epsilon);
  }
  
BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(MatrixTests)
  
  BOOST_AUTO_TEST_CASE(MatrixConstructorTest)
  {
    kudryakov::Circle new_circle({50,0}, 20);
    kudryakov::Rectangle new_rectangle({10,20}, 40, 80);
    kudryakov::Rectangle new_rectangle2({70,70}, 40, 20);
    
    std::shared_ptr <kudryakov::Shape> new_circlePtr = std::make_shared <kudryakov::Circle> (new_circle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr = std::make_shared <kudryakov::Rectangle> (new_rectangle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr2 = std::make_shared <kudryakov::Rectangle> (new_rectangle2);
    
    kudryakov::Matrix new_matrix(new_circlePtr);
    new_matrix.addShape(new_rectanglePtr);
    new_matrix.addShape(new_rectanglePtr2);
    
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer1 = new_matrix[0];
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer2 = new_matrix[1];
    
    BOOST_CHECK(new_layer1[0] == new_circlePtr);
    BOOST_CHECK(new_layer1[1] == new_rectanglePtr2);
    BOOST_CHECK(new_layer2[0] == new_rectanglePtr);
    BOOST_CHECK(new_layer2[1] == nullptr);
    
    BOOST_CHECK_CLOSE(new_layer1[0]->getFrameRect().pos.x, 50, Epsilon);
    BOOST_CHECK_CLOSE(new_layer1[0]->getFrameRect().pos.y, 0, Epsilon);
    BOOST_CHECK_CLOSE(new_layer1[1]->getFrameRect().pos.x, 70, Epsilon);
    BOOST_CHECK_CLOSE(new_layer1[1]->getFrameRect().pos.y, 70, Epsilon);
    BOOST_CHECK_CLOSE(new_layer2[0]->getFrameRect().pos.x, 10, Epsilon);
    BOOST_CHECK_CLOSE(new_layer2[0]->getFrameRect().pos.y, 20, Epsilon);
    BOOST_CHECK_EQUAL(new_matrix.getLayerSize(1), 1);
  }
  
  BOOST_AUTO_TEST_CASE(MatrixCompositeShapeConstructorTest)
  {
    kudryakov::Circle new_circle({50,0}, 20);
    kudryakov::Rectangle new_rectangle({10,20}, 40, 80);
    kudryakov::Rectangle new_rectangle2({70,70}, 40, 20);
    
    std::shared_ptr <kudryakov::Shape> new_circlePtr = std::make_shared <kudryakov::Circle> (new_circle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr = std::make_shared <kudryakov::Rectangle> (new_rectangle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr2 = std::make_shared <kudryakov::Rectangle> (new_rectangle2);
    
    kudryakov::CompositeShape new_compositeshape(new_circlePtr);
    new_compositeshape.addShape(new_rectanglePtr);
    new_compositeshape.addShape(new_rectanglePtr2);
    
    kudryakov::Matrix new_matrix(new_compositeshape);
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer1 = new_matrix[0];
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer2 = new_matrix[1];
    
    BOOST_CHECK(new_layer1[0] == new_circlePtr);
    BOOST_CHECK(new_layer1[1] == new_rectanglePtr2);
    BOOST_CHECK(new_layer2[0] == new_rectanglePtr);
    BOOST_CHECK(new_layer2[1] == nullptr);
    
    BOOST_CHECK_CLOSE(new_layer1[0]->getFrameRect().pos.x, 50, Epsilon);
    BOOST_CHECK_CLOSE(new_layer1[0]->getFrameRect().pos.y, 0, Epsilon);
    BOOST_CHECK_CLOSE(new_layer1[1]->getFrameRect().pos.x, 70, Epsilon);
    BOOST_CHECK_CLOSE(new_layer1[1]->getFrameRect().pos.y, 70, Epsilon);
    BOOST_CHECK_CLOSE(new_layer2[0]->getFrameRect().pos.x, 10, Epsilon);
    BOOST_CHECK_CLOSE(new_layer2[0]->getFrameRect().pos.y, 20, Epsilon);
  }
  
  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    kudryakov::Circle new_circle({50,0}, 20);
    kudryakov::Rectangle new_rectangle({10,20}, 40, 80);
    kudryakov::Rectangle new_rectangle2({70,70}, 40, 20);
    
    std::shared_ptr <kudryakov::Shape> new_circlePtr = std::make_shared <kudryakov::Circle> (new_circle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr = std::make_shared <kudryakov::Rectangle> (new_rectangle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr2 = std::make_shared <kudryakov::Rectangle> (new_rectangle2);
    
    kudryakov::Matrix new_matrix(new_circlePtr);
    new_matrix.addShape(new_rectanglePtr);
    new_matrix.addShape(new_rectanglePtr2);
    
    kudryakov::Matrix new_matrix1(new_matrix);
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new1_layer1 = new_matrix1[0];
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new1_layer2 = new_matrix1[1];
    
    BOOST_CHECK(new1_layer1[0] == new_circlePtr);
    BOOST_CHECK(new1_layer1[1] == new_rectanglePtr2);
    BOOST_CHECK(new1_layer2[0] == new_rectanglePtr);
    
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer1 = new_matrix[0];
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer2 = new_matrix[1];
    
    BOOST_CHECK(new1_layer1[0] == new_layer1[0]);
    BOOST_CHECK(new1_layer1[1] == new_layer1[1]);
    BOOST_CHECK(new1_layer2[0] == new_layer2[0]);
  }
  
  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    kudryakov::Circle new_circle({50,0}, 20);
    kudryakov::Rectangle new_rectangle({10,20}, 40, 80);
    kudryakov::Rectangle new_rectangle2({70,70}, 40, 20);
    
    std::shared_ptr <kudryakov::Shape> new_circlePtr = std::make_shared <kudryakov::Circle> (new_circle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr = std::make_shared <kudryakov::Rectangle> (new_rectangle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr2 = std::make_shared <kudryakov::Rectangle> (new_rectangle2);
    
    kudryakov::Matrix new_matrix(new_circlePtr);
    new_matrix.addShape(new_rectanglePtr);
    
    kudryakov::Matrix new_matrix1(new_rectanglePtr2);
    new_matrix1 = new_matrix;
    
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new1_layer1 = new_matrix1[0];
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new1_layer2 = new_matrix1[1];
    
    BOOST_CHECK(new1_layer1[0] == new_circlePtr);
    BOOST_CHECK(new1_layer2[0] == new_rectanglePtr);
    
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer1 = new_matrix[0];
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer2 = new_matrix[1];
    
    BOOST_CHECK(new1_layer1[0] == new_layer1[0]);
    BOOST_CHECK(new1_layer2[0] == new_layer2[0]);
  }
  
  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    kudryakov::Circle new_circle({50,0}, 20);
    kudryakov::Rectangle new_rectangle({10,20}, 40, 80);
    kudryakov::Rectangle new_rectangle2({70,70}, 40, 20);
    
    std::shared_ptr <kudryakov::Shape> new_circlePtr = std::make_shared <kudryakov::Circle> (new_circle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr = std::make_shared <kudryakov::Rectangle> (new_rectangle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr2 = std::make_shared <kudryakov::Rectangle> (new_rectangle2);
    
    kudryakov::Matrix new_matrix(new_circlePtr);
    new_matrix.addShape(new_rectanglePtr);
    new_matrix.addShape(new_rectanglePtr2);
    
    kudryakov::Matrix new_matrix1(std::move(new_matrix));
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer1 = new_matrix1[0];
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer2 = new_matrix1[1];
    
    BOOST_CHECK(new_layer1[0] == new_circlePtr);
    BOOST_CHECK(new_layer1[1] == new_rectanglePtr2);
    BOOST_CHECK(new_layer2[0] == new_rectanglePtr);
    BOOST_CHECK_EQUAL(new_matrix.getLayersNumber(), 0);
    BOOST_CHECK_EQUAL(new_matrix.getMaxLayerSize(), 0);
  }
  
  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    kudryakov::Circle new_circle({50,0}, 20);
    kudryakov::Rectangle new_rectangle({10,20}, 40, 80);
    kudryakov::Rectangle new_rectangle2({70,70}, 40, 20);
    
    std::shared_ptr <kudryakov::Shape> new_circlePtr = std::make_shared <kudryakov::Circle> (new_circle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr = std::make_shared <kudryakov::Rectangle> (new_rectangle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr2 = std::make_shared <kudryakov::Rectangle> (new_rectangle2);
    
    kudryakov::Matrix new_matrix(new_circlePtr);
    new_matrix.addShape(new_rectanglePtr);
    
    kudryakov::Matrix new_matrix1(new_rectanglePtr2);
    new_matrix1 = std::move(new_matrix);
    
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer1 = new_matrix1[0];
    std::unique_ptr <std::shared_ptr<kudryakov::Shape>[] > new_layer2 = new_matrix1[1];
    
    BOOST_CHECK(new_layer1[0] == new_circlePtr);
    BOOST_CHECK(new_layer2[0] == new_rectanglePtr);
    BOOST_CHECK_EQUAL(new_matrix.getLayersNumber(), 0);
    BOOST_CHECK_EQUAL(new_matrix.getMaxLayerSize(), 0);
  }
  
  BOOST_AUTO_TEST_CASE(EqualityOperatorTest)
  {
    kudryakov::Circle new_circle({50,0}, 20);
    kudryakov::Rectangle new_rectangle({10,20}, 40, 80);
    
    std::shared_ptr <kudryakov::Shape> new_circlePtr = std::make_shared <kudryakov::Circle> (new_circle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr = std::make_shared <kudryakov::Rectangle> (new_rectangle);
    
    kudryakov::Matrix new_matrix(new_circlePtr);
    new_matrix.addShape(new_rectanglePtr);
    
    kudryakov::Matrix new_matrix1(new_circlePtr);
    new_matrix1.addShape(new_rectanglePtr);
    
    BOOST_CHECK(new_matrix == new_matrix1);
  }
  
  BOOST_AUTO_TEST_CASE(NonEqualityOperatorTest)
  {
    kudryakov::Circle new_circle({50,0}, 20);
    kudryakov::Rectangle new_rectangle({10,20}, 40, 80);
    
    std::shared_ptr <kudryakov::Shape> new_circlePtr = std::make_shared <kudryakov::Circle> (new_circle);
    std::shared_ptr <kudryakov::Shape> new_rectanglePtr = std::make_shared <kudryakov::Rectangle> (new_rectangle);
    
    kudryakov::Matrix new_matrix(new_circlePtr);
    kudryakov::Matrix new_matrix1(new_rectanglePtr);
    
    BOOST_CHECK(new_matrix != new_matrix1);
  }

BOOST_AUTO_TEST_SUITE_END()
