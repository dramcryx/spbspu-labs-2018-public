#ifndef KUDRYAKOV_DMITRY_LABS_COMPOSITESHAPE_HPP
#define KUDRYAKOV_DMITRY_LABS_COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace kudryakov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(std::shared_ptr<Shape> newShape);
    
    CompositeShape(const CompositeShape& other);
    
    CompositeShape(CompositeShape&& other);
  
    CompositeShape& operator= (const CompositeShape& other);
  
    CompositeShape& operator= (CompositeShape&& other);
    
    std::shared_ptr<Shape> operator[] (int index) const;
    
    void addShape(std::shared_ptr<Shape> newShape);
    
    double getArea() const override;
    
    rectangle_t getFrameRect() const override;
    
    void move(const point_t &newCenterPoint) override;
    
    void move(const double dx, const double dy) override;
    
    void scale(const double factor) override;
    
    int getSize() const;
    
    void rotate(const double ang) override;
    
    double getAngle() const override;
    
  private:
    std::unique_ptr< std::shared_ptr<Shape>[] > arr_;
    
    int size_;
  };
}
#endif
