#ifndef KUDRYAKOV_DMITRY_LABS_MATRIX_HPP
#define KUDRYAKOV_DMITRY_LABS_MATRIX_HPP

#include <memory>
#include "shape.hpp"
#include "composite-shape.hpp"

namespace kudryakov
{
  class Matrix
  {
  public:
    Matrix(const std::shared_ptr<Shape> shape);
    
    Matrix(const Matrix& other);
    
    Matrix(Matrix&& other);
    
    Matrix(const CompositeShape& compositeShape);
    
    Matrix& operator=(const Matrix& other);
    
    Matrix& operator=(Matrix&& other);
    
    bool operator==(const Matrix& other) const;
    
    bool operator!=(const Matrix& other) const;
    
    std::unique_ptr<std::shared_ptr<Shape >[]> operator[](const int layerIndex) const;
    
    void addShape(const std::shared_ptr<Shape> new_shape);
    
    int getLayersNumber() const;
    
    int getMaxLayerSize() const;
    
    int getLayerSize(const int layerNumber) const;
    
  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> array_;
    
    int layersNumber_;
    
    int layerSize_;
    
    bool overLappingCheck(const int number, std::shared_ptr<Shape> new_shape) const;
  };
}
#endif //KUDRYAKOV_DMITRY_LABS_MATRIX_HPP
