#ifndef DETAILS_HPP
#define DETAILS_HPP

#include <iostream>
#include <functional>
#include <cstring>

namespace detail
{
  template<typename T>
  struct withBrackets
  {
    size_t begin(T&)
    {
      return 0;
    }
    size_t end(T& contain)
    {
      return contain.size();
    }
    typename T::reference getRef(T& contain, size_t size)
    {
      return contain[size];
    }
  };

  template<typename T>
  struct withAt
  {
    size_t begin(T&)
    {
      return 0;
    }
    size_t end(T& contain)
    {
      return contain.size();
    }
    typename T::reference getRef(T& contain, size_t i)
    {
      return contain.at(i);
    }
  };

  template<typename T>
  struct withIterator
  {
    typename T::iterator begin(T& contain)
    {
      return contain.begin();
    }
    typename T::iterator end(T& contain)
    {
      return contain.end();
    }
    typename T::reference getRef(T&, typename T::iterator i)
    {
      return *i;
    }
  };

  template<typename T, typename A, typename C>
  void sort(T& contain, A access, C compare)
  {
    for(auto i = access.begin(contain); i != access.end(contain); ++i)
    {
      for(auto j = i; j != access.end(contain); ++j)
      {
        if(compare(access.getRef(contain, i), access.getRef(contain, j)))
        {
          std::swap(access.getRef(contain, i), access.getRef(contain, j));
        }
      }
    }
  }

  template<typename T>
  std::function<bool(T,T)> getDirect(char* direction)
  {
    if(strcmp(direction, "ascending") == 0)
    {
      return std::function<bool(T,T)>(std::greater<T>());
    }
    if(strcmp(direction, "descending") == 0)
    {
      return std::function<bool(T,T)>(std::less<T>());
    }
    throw std::invalid_argument("Incorrect way of sorting");
  }

  template<typename T>
  void print(T &container)
  {
    for (typename T::iterator i = container.begin(); i != container.end(); i++)
    {
      std::cout << *i << " ";
    }
    std::cout << std::endl;
  }
}

void task1(char *direction);
void task2(char *name);
void task3();
void task4(char *direction, size_t size);

#endif //DETAILS_HPP
