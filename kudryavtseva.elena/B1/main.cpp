#include "details.hpp"

int main(int argc, char * argv[])
{
  try
  {
    if (argc > 1)
    {
      switch(atoi(argv[1]))
      {
      case 1:
      {
        if (argc != 3)
        {
          throw std::invalid_argument("Invalid number of parameters for part 1");
        }
        task1(argv[2]);
        break;
      }
      case 2:
      {
        if (argc != 3)
        {
          throw std::invalid_argument("Invalid number of parameters for part 2");
        }
        task2(argv[2]);
        break;
      }
      case 3:
      {
        if (argc != 2)
        {
          throw std::invalid_argument("Invalid number of parameters for part 3");
        }
        task3();
        break;
      }
      case 4:
      {
        if (argc != 4)
        {
          throw std::invalid_argument("Invalid number of parameters for part 4");
        }
        task4(argv[2], atoi(argv[3]));
        break;
      }
      default:
      {
        throw std::invalid_argument("Invalid number of part");
      }
      }
    }
    else
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
  }
  catch(std::ios_base::failure &exception)
  {
    std::cerr<<exception.what()<<"\n";
    return 1;
  }

  catch(std::invalid_argument &exception)
  {
    std::cerr<<exception.what()<<"\n";
    return 1;
  }

  return 0;
}
