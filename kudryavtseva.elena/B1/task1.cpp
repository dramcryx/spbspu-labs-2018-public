#include <vector>
#include <forward_list>
#include <iterator>
#include "details.hpp"

void task1 (char *direction)
{
  auto direct = detail::getDirect<int>(direction);
  std::vector<int> vec1(std::istream_iterator<int>(std::cin),
                                        std::istream_iterator<int>());
  if(!std::cin.eof())
  {
    throw std::ios_base::failure("Input error");
  }

  if(vec1.empty())
  {
    return;
  }

  std::vector<int> vec2(vec1);
  std::forward_list<int> list(vec1.begin(), vec1.end());
  detail::sort(vec1, detail::withBrackets<std::vector<int>>(), direct);
  detail::sort(vec2, detail::withAt<std::vector<int>>(), direct);
  detail::sort(list, detail::withIterator<std::forward_list<int>>(), direct);

  detail::print(vec1);
  detail::print(vec2);
  detail::print(list);

}
