#include <vector>
#include <memory>
#include <fstream>
#include "details.hpp"

void task2(char *name)
{
  if (name == nullptr)
  {
    throw std::invalid_argument("File is nullptr");
  }

  std::ifstream file(name, std::ios_base::binary);

  if(file.good() == 0)
  {
    throw std::invalid_argument("Incorrect name of file");
  }

  if (!file)
  {
    throw std::ios_base::failure("Reading file failed");
  }

  file.seekg(0, std::ios_base::end);
  size_t size = file.tellg();
  file.seekg(0, std::ios_base::beg);
  if (size == 0)
  {
    exit(0);
  }

  std::unique_ptr<char[]> data (new char[size]);
  file.read(data.get(), size);
  file.close();
  std::vector<char> vec (data.get(), data.get() + size);
  for (auto i = vec.begin(); i != vec.end(); i++)
  {
    std::cout << *i;
  }
}



