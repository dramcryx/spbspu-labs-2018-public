#include <vector>
#include <iterator>
#include "details.hpp"

void task3()
{
  std::vector<int> vec(std::istream_iterator<int>(std::cin),
                                        std::istream_iterator<int>());
  if(!std::cin.eof())
  {
    throw std::ios_base::failure("Input error");
  }

  if(vec.empty())
  {
    return;
  }

  if(vec.back() != 0)
  {
    throw std::invalid_argument("missing zero");
  }

  vec.pop_back();

  if(vec.empty())
  {
    return;
  }

  if(vec.back() == 1)
  {
    for(auto i = vec.begin(); i != vec.end();)
    {
      if(*i % 2 == 0)
      {
        i = vec.erase(i);
      }
      else
      {
        ++i;
      }
    }
  }
  else if(vec.back() == 2)
  {
    int count = 0;
    for(auto i = vec.begin(); i < vec.end(); ++i)
    {
      if(*i % 3 == 0)
      {
        ++count;
      }
    }

    vec.insert(vec.end(), 3 * count, 0);
    auto i = vec.rbegin();
    auto j = vec.rbegin() + 3 * count;

    while(i != j)
    {
      if(*j % 3 == 0)
      {
        for(int count = 0; count < 3; ++count, ++i)
        {
          *i = 1;
        }
      }
      *(i++) = *(j++);
    }
  }

  detail::print(vec);
}

