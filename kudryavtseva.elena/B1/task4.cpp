#include <random>
#include <vector>
#include "details.hpp"

void fillRand(double *array, int size)
{
  std::random_device random;
  std::mt19937 gen(random());
  std::uniform_real_distribution<double> range (-1.0, 1.0);

  for (int i=0; i < size; i++)
  {
    array[i] = range(gen);
  }
}

void task4 (char *direction, size_t size)
{
  if (size <= 0)
  {
    throw std::invalid_argument("Incorrect number of arguments for task 4");
  }

  auto direct = detail::getDirect<double>(direction);
  std::vector<double> vec(size, 0);
  if (!vec.empty())
  {
    fillRand(vec.data(), vec.size());
    detail::print(vec);
    detail::sort(vec, detail::withIterator<std::vector<double>>(), direct);
    detail::print(vec);
  }
}
