#include <iostream>
#include <stdexcept>

#include "tasksheader.hpp"

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    std::cerr << "Enter the task number from 1 to 4\n";
    return 1;
  }
  try
  {
    if (argc > 1)
    {
      int number = std::atoi(argv[1]);
      switch(number)
      {
      case 1:
        firstTaskExec(argc, argv);
        break;
      case 2:
        secondTaskExec(argc, argv);
        break;
      case 3:
        thirdTaskExec(argc, argv);
        break;
      case 4:
        fourthTaskExec(argc, argv);
        break;
      default:
        std::cerr << "Invalid Parameters";
        return 1;
      }
    }
    else
    {
      std::cerr << "Invalid Parameters";
      return 1;
    }
  }
  catch(const std::exception &error)
  {
    std::cerr << error.what() << std::endl;
    return 2;
  }
  return 0;
}
