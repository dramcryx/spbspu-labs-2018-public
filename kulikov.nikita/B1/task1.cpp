#include <forward_list>
#include <iterator>
#include <stdexcept>
#include <vector>

#include "tasksheader.hpp"

void firstTaskExec(int argc, char *argv[])
{
  if(argc != 3)
  {
    throw std::invalid_argument("Incorrect number of parameters for the first task");
  }

  auto direction = kulikov::getDirection<int>(argv[2]);
  std::vector<int> vector_a(std::istream_iterator<int>(std::cin),
      std::istream_iterator<int>());

  if(!std::cin.eof())
  {
    throw std::ios_base::failure("Incorrect input in first task");
  }

  if(vector_a.empty())
  {
    return;
  }

  std::vector<int> vector_b(vector_a);
  std::forward_list<int> list_c(vector_a.begin(), vector_a.end());

  kulikov::sort(vector_a, kulikov::AccessByOperator<std::vector<int>>(), direction);
  kulikov::sort(vector_b, kulikov::AccessByAt<std::vector<int>>(), direction);
  kulikov::sort(list_c, kulikov::AccessByIterator<std::forward_list<int>>(), direction);

  kulikov::print(vector_a);
  kulikov::print(vector_b);
  kulikov::print(list_c);
}
