#include <fstream>
#include <memory>
#include <stdexcept>
#include <vector>

#include "tasksheader.hpp"

void secondTaskExec(int argc, char *argv[])
{
  if(argc != 3)
  {
    throw std::invalid_argument("Incorrect number of parameters for the second task");
  }

  std::ifstream file(argv[2]);
  if(!file)
  {
    throw std::invalid_argument("Incorrect input file in second task");
  }

  file.seekg(0, file.end);
  const size_t length = file.tellg();
  file.seekg(0, file.beg);

  std::unique_ptr<char[]> arr(new char[length]);
  file.read(arr.get(), length);
  std::vector<char> vector2(arr.get(), arr.get() + length);

  for (auto i = vector2.begin(); i != vector2.end(); i++)
  {
    std::cout << *i;
  }

  file.close();
}
