#include <iostream>
#include <iterator>
#include <vector>

#include "tasksheader.hpp"

void thirdTaskExec(int argc, char*[])
{
  if (argc != 2)
  {
    throw std::invalid_argument("Incorrect number of parameters for the third task");
  }
  std::vector<int> vector_a;
  int size = 0;
  int value = 0;
  while (std::cin) 
  {
    if (std::cin >> value)
    {
      if (value % 3 == 0)
      {
        size++;
      }
      if (value == 0)
      {
        break;
      }
      vector_a.push_back(value);
      
    }
    else
    {
      if (std::cin.eof())
      {
        break;
      }
      else
      {
        throw std::invalid_argument ("Input error");
      }
    }
  }

  if ((!std::cin.eof()) && (value != 0)) 
  {
    throw std::invalid_argument ("Input error");
  }
  
  if (value != 0)
  {
    throw std::invalid_argument ("Missing zero");
  }
  
  if (vector_a.empty())
  {
    return;
  }
 
  switch (vector_a.back()) 
  {
  case 1:
  {
    std::vector<int>::iterator iteratorStart = vector_a.begin();
    while (iteratorStart != vector_a.end())
    {
      if (*iteratorStart % 2 == 0)
      {
        iteratorStart = vector_a.erase(iteratorStart);
      }
      else 
      {
        ++iteratorStart;
      }
    }
    break;
  }
  case 2:
  {
    vector_a.reserve(vector_a.capacity() + 3 * size);
    std::vector<int>::iterator iteratorStart = vector_a.begin();
    while (iteratorStart != vector_a.end())
    {
      if (*iteratorStart % 3 == 0)
      {
        iteratorStart = vector_a.insert(iteratorStart + 1, 3, 1) + 2;
      }
      ++iteratorStart;
    }
    break;
  }
  default:
  {
  }
  }
  
  for(std::vector<int>::iterator i = vector_a.begin(); i < vector_a.end(); ++i)
  {
    std::cout << *i << " ";
  }
}
