#include <iostream>
#include <random>
#include <vector>

#include "tasksheader.hpp"

void fillRandom(double * array, int size)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);

  for(int i = 0; i < size; i++)
  {
    array[i] = distribution(gen);
  }
}

void fourthTaskExec(int argc, char *argv[])
{
  if(argc != 4)
  {
    throw std::invalid_argument("Incorrect number of parameters for the fourth task");
  }

  auto direction = kulikov::getDirection<double>(argv[2]);

  std::vector<double> vector4(std::stoi(argv[3]));
  fillRandom(&vector4[0], vector4.size());
  kulikov::print(vector4);
  kulikov::sort(vector4, kulikov::AccessByIterator<std::vector<double>>(), direction);
  kulikov::print(vector4);
}
