#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include <stdexcept>
#include <shape.hpp>

namespace kulikov
{
  class Circle:
    public Shape
  {
  public:
    Circle(const point_t & p, const double radius);
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t & p) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double koeff) override;
    void rotate(const double angle) noexcept override;
  private:
    point_t pos_;
    double radius_;
    double angle_;
  };
}

#endif // CIRCLE_HPP
