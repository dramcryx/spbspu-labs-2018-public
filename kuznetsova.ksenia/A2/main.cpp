﻿#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"

void PrintArea(const kuznetsova::Shape &shape1)
{
  std::cout << " Area " << shape1.getArea() << std::endl;
}

void PrintFrameRect(const kuznetsova::Shape &shape1)
{
  kuznetsova::rectangle_t framerect = shape1.getFrameRect();
  std::cout << "FrameRect " << std::endl
    << " centre.x " << framerect.pos.x
    << " centre.y " << framerect.pos.y
    << " width " << framerect.width
    << " height" << framerect.height << std::endl;
}

int main()
{
  try
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::point_t newcentre{ -1.0, -1.0 };
    kuznetsova::Circle Circle1(centre, 3.4);
    PrintArea(Circle1);
    Circle1.scale(3.0);
    PrintArea(Circle1);
    Circle1.move(newcentre);
    PrintFrameRect(Circle1);

    std::cout << std::endl;

    kuznetsova::Rectangle Rectangle1(centre, 5.0, 11.0);
    PrintArea(Rectangle1);
    Rectangle1.scale(5.0);
    PrintArea(Rectangle1);
    Rectangle1.move(newcentre);
    PrintFrameRect(Rectangle1);
    Rectangle1.move(-10.0, 1.0);
    PrintFrameRect(Rectangle1);
  }
  catch (std::invalid_argument &error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  return 0;
}
