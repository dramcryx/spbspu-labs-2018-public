#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>

#include "rectangle.hpp"
#include "circle.hpp"

const double EPS = 0.001;

namespace kuznetsova
{
  BOOST_AUTO_TEST_SUITE(RectTest)

    BOOST_AUTO_TEST_CASE(Rect_InvOfParTest)
  {
    kuznetsova::point_t centre{ 2.0, 3.7 };
    kuznetsova::point_t newcentre{ -5.0, -1.9 };
    kuznetsova::Rectangle Rectangle1(centre, 5.0, 11.0);
    double area = Rectangle1.getArea();
    Rectangle1.move(3.0, 12.0);
    BOOST_REQUIRE_EQUAL(Rectangle1.width_, 5.0);
    BOOST_REQUIRE_EQUAL(Rectangle1.height_, 11.0);
    BOOST_REQUIRE_EQUAL(Rectangle1.getArea(), area);
    Rectangle1.move(newcentre);
    BOOST_REQUIRE_EQUAL(Rectangle1.width_, 5.0);
    BOOST_REQUIRE_EQUAL(Rectangle1.height_, 11.0);
    BOOST_REQUIRE_EQUAL(Rectangle1.getArea(), area);
  }

  BOOST_AUTO_TEST_CASE(Rect_ScaleTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::Rectangle Rectangle1(centre, 5.0, 11.0);
    double area = Rectangle1.getArea();
    Rectangle1.scale(5.0);
    BOOST_CHECK_CLOSE(Rectangle1.getArea(), (area * pow(5.0, 2.0)), EPS);
  }

  BOOST_AUTO_TEST_CASE(Rect_InvalidWidthTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    BOOST_CHECK_THROW(kuznetsova::Rectangle Rectangle1(centre, -7.0, 9.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rect_InvalidHeightTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    BOOST_CHECK_THROW(kuznetsova::Rectangle Rectangle1(centre, 7.0, -9.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rect_InvalidScaleFactorTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::Rectangle Rectangle1(centre, 5.0, 11.0);
    BOOST_CHECK_THROW(Rectangle1.scale(-3.0), std::invalid_argument);
  }
  BOOST_AUTO_TEST_SUITE_END()

    BOOST_AUTO_TEST_SUITE(CircleTests)

    BOOST_AUTO_TEST_CASE(Circle_InvOfParTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::point_t newcentre{ -1.0, -1.0 };
    kuznetsova::Circle Circle1(centre, 3.4);
    double area = Circle1.getArea();
    Circle1.move(-9.0, 13.4);
    BOOST_REQUIRE_EQUAL(Circle1.radius_, 3.4);
    BOOST_REQUIRE_EQUAL(Circle1.getArea(), area);
    Circle1.move(newcentre);
    BOOST_REQUIRE_EQUAL(Circle1.radius_, 3.4);
    BOOST_REQUIRE_EQUAL(Circle1.getArea(), area);
  }

  BOOST_AUTO_TEST_CASE(Circle_ScaleTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::Circle Circle1(centre, 3.4);
    double area = Circle1.getArea();
    Circle1.scale(3.0);
    BOOST_REQUIRE_CLOSE_FRACTION(Circle1.getArea(), (area * pow(3.0, 2.0)), EPS);
  }

  BOOST_AUTO_TEST_CASE(Circle_InvalidRadiusTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    BOOST_CHECK_THROW(kuznetsova::Circle Circle1(centre, -8.9), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Circle_InvalidScaleFactorTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::Circle Circle1(centre, 3.4);
    BOOST_CHECK_THROW(Circle1.scale(-5.0), std::invalid_argument);
  }
  BOOST_AUTO_TEST_SUITE_END()
}

