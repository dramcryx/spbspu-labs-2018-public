﻿#include <memory>
#include <iostream>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using namespace std;
using namespace kuznetsova;

void PrintArea(const Shape &shape1)
{
  std::cout << " Area " << shape1.getArea() << std::endl;
}

void PrintFrameRect(Shape &shape1)
{
  rectangle_t framerect = shape1.getFrameRect();
  std::cout << "FrameRect " << std::endl
    << " centre.x " << framerect.pos.x
    << " centre.y " << framerect.pos.y
    << " width " << framerect.width
    << " height" << framerect.height << std::endl;
}

int main()
{
  try
  {
    point_t centre1{ 1.0, 3.0 };  // here we create the centre of the first shape
    shared_ptr <Shape> rect1 = make_shared<Rectangle>(Rectangle(centre1, 5.0, 11.0)); // makes new intellectual pointer and links it 
                                                                                      // to the area of the memory where the shape "rect1" is stored
                                                                                      // i.e. we create shape and immediately initialize it
    point_t centre2{ 5.0, 1.0 }; // here we create the centre of the second shape
    shared_ptr <Shape> rect2 = make_shared<Rectangle>(Rectangle(centre2, 3.0, 7.0)); // makes new intellectual pointer and links it 
                                                                                      // to the area of the memory where the shape "rect2" is stored
                                                                                      // i.e. we create shape and immediately initialize it
    point_t centre3{ 2.7, 13.8 }; // here we create the centre of the third shape
    shared_ptr <Shape> circ1 = make_shared<Circle>(Circle(centre3, 5.6)); // makes new intellectual pointer and links it 
                                                                          // to the area of the memory where the shape "rect2" is stored
                                                                          // i.e. we create shape and immediately initialize it
    CompositeShape comp(rect1); // here we create the instance of CompositeShape class and as a parameter we give the pointer to the first figure that
                                // we created above to it - so, we creare an instance of the CompositeShape (list) and immidiately add the first shape to it
    comp.addShape(circ1); // here we add the second shape to the list
    comp.addShape(rect2); // here we add the third shape to the list

    cout << "Composite Shape" << endl
      << "Area: " << comp.getArea() << endl
      << "Frame: centre (" << comp.getFrameRect().pos.x << ", " << comp.getFrameRect().pos.y << ") " << endl
      << "width = " << comp.getFrameRect().width << ", height = " << comp.getFrameRect().height << endl;

    point_t newcentre{ 3.2, 5.0 };
    comp.moveUsingStruct(newcentre,comp);
    cout << "Moving to {3.2, 5.0} : " << endl
      << "new center (" << comp.getFrameRect().pos.x << ", " << comp.getFrameRect().pos.y << ")" << endl;

    comp.scale(3.0);
    cout << "Scaling with factor - 3.0 : " << endl
      << "new area : " << comp.getArea() << endl;

    cout << "Removing shape no.1" << endl;
    comp.removeShape(1);
    cout << "Composite shape area = " << comp.getArea() << endl;

  }
  catch (invalid_argument &error)
  {
    cerr << "Error : " << error.what() << endl;
    return 1;
  }

  catch (out_of_range &error)
  {
    cerr << "Error : " << error.what() << endl;
    return 1;
  }
  return 0;
}
