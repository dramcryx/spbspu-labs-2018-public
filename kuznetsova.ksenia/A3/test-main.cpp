#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <memory>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double EPS = 0.000001;

namespace kuznetsova
{
  BOOST_AUTO_TEST_SUITE(CompositeShapeTest)

    BOOST_AUTO_TEST_CASE(ConstructorTest)
  {
    std::shared_ptr <kuznetsova::Shape> emptyShape = nullptr;
    BOOST_CHECK_THROW(kuznetsova::CompositeShape aShape(emptyShape), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre2{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre2, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);

    kuznetsova::CompositeShape bShape{ aShape };

    BOOST_CHECK_EQUAL(bShape.getSize(), aShape.getSize());
    BOOST_CHECK_CLOSE(bShape.getArea(), aShape.getArea(), EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().pos.x, aShape.getFrameRect().pos.x, EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().pos.y, aShape.getFrameRect().pos.y, EPS);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre2{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre2, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);

    double aSize = aShape.getSize();
    double aArea = aShape.getArea();

    kuznetsova::rectangle_t aFrameRect = aShape.getFrameRect();
    kuznetsova::CompositeShape bShape( std::move(aShape) );
    kuznetsova::rectangle_t bFrameRect = bShape.getFrameRect();
    double bSize = bShape.getSize();
    double bArea = bShape.getArea();

    BOOST_CHECK_EQUAL(aShape.getSize(), 0);
    BOOST_CHECK_CLOSE(aFrameRect.width, bFrameRect.width, EPS);
    BOOST_CHECK_CLOSE(aFrameRect.height, bFrameRect.height, EPS);
    BOOST_CHECK_CLOSE(aFrameRect.pos.x, bFrameRect.pos.x, EPS);
    BOOST_CHECK_CLOSE(aFrameRect.pos.y, bFrameRect.pos.y, EPS);
    BOOST_CHECK_EQUAL(bSize, aSize);
    BOOST_CHECK_CLOSE(bArea, aArea, EPS);
  }

  BOOST_AUTO_TEST_CASE(CopyAssigmentOperatorTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre3{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre3, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);
    kuznetsova::CompositeShape bShape(rect);

    bShape = aShape;

    BOOST_CHECK_EQUAL(bShape.getSize(), aShape.getSize());
    BOOST_CHECK_CLOSE(bShape.getArea(), aShape.getArea(), EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().pos.x, aShape.getFrameRect().pos.x, EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().pos.y, aShape.getFrameRect().pos.y, EPS);
  }

  BOOST_AUTO_TEST_CASE(MoveAssigmentOperatorTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre3{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre3, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);

    kuznetsova::CompositeShape bShape(rect);
    bShape = std::move(aShape);

    BOOST_CHECK_EQUAL(aShape.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(MovesTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre3{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre3, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);
    kuznetsova::rectangle_t frameRect = aShape.getFrameRect();
    double dx = 2.0;
    double dy = 5.0;
    aShape.move(dx, dy);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.x, frameRect.pos.x + dx, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.y, frameRect.pos.y + dy, EPS);

    kuznetsova::point_t newcentre = { 5.0, 0.0 };
    aShape.moveUsingStruct(newcentre,aShape);

    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.x, newcentre.x, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.y, newcentre.y, EPS);
  }

  BOOST_AUTO_TEST_CASE(ScaleTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre3{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre3, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);

    double aArea = aShape.getArea();
    double aWidth = aShape.getFrameRect().width;
    double aHeight = aShape.getFrameRect().height;
    kuznetsova::point_t apos = aShape.getFrameRect().pos;
    double factor = 3.0;
    aShape.scale(factor);

    BOOST_CHECK_CLOSE(aShape.getFrameRect().width, aWidth*factor, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().height, aHeight*factor, EPS);
    BOOST_CHECK_CLOSE(aShape.getArea(), aArea*factor*factor, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.x, apos.x, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.y, apos.y, EPS);
  }

  BOOST_AUTO_TEST_CASE(AddShapeTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
    std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::CompositeShape aShape(rect);
    BOOST_CHECK_THROW(aShape.addShape(rect), std::invalid_argument);
  }

  BOOST_AUTO_TEST_SUITE_END()
}
