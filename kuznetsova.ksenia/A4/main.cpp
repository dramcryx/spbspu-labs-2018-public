#include <iostream>
#include <memory>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

int main()
{
  try
  {
    kuznetsova::point_t centre{ 3.0, 7.0 };
    kuznetsova::Circle circle(centre, 33.1);
    kuznetsova::point_t centre_rec{ 2.2, 0.0 };
    kuznetsova::Rectangle rectangle(centre_rec, 3.7, 12.0);

    std::cout << "Circle:" << std::endl;
    circle.printInfo();
    circle.move(1.3, 6.1);
    circle.printInfo();
    kuznetsova::point_t newcentre{ 32.3, 47.1 };
    circle.move(newcentre);
    circle.printInfo();
    circle.scale(0.5);
    circle.printInfo();
    double angle = 90.0;
    circle.rotate(angle);
    circle.printInfo();
    std::cout << std::endl;

    std::cout << "Rectangle:" << std::endl;
    rectangle.printInfo();
    rectangle.move(5.30, -33.44);
    rectangle.printInfo();
    kuznetsova::point_t newcentre_{ 2.6, 18.17 };
    rectangle.move(newcentre_);
    rectangle.printInfo();
    rectangle.scale(3.3);
    rectangle.printInfo();
    angle = 45.0;
    rectangle.rotate(angle);
    rectangle.printInfo();
    std::cout << std::endl;

    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect1 = std::make_shared<kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre2{ 5.0, 1.0 };
    std::shared_ptr <kuznetsova::Shape> rect2 = std::make_shared<kuznetsova::Rectangle>(kuznetsova::Rectangle(centre2, 3.0, 7.0));
    kuznetsova::point_t centre3{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ1 = std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre3, 5.6));
    kuznetsova::CompositeShape comp(rect1);
    comp.addShape(circ1);
    comp.addShape(rect2);

    comp.printInfo();

    kuznetsova::point_t newpos{ 3.2, 5.0 };
    comp.moveUsingStruct(newpos, comp);
    std::cout << "Moving to {3.2, 5.0} : " << std::endl
      << "new center (" << comp.getFrameRect().pos.x << ", " << comp.getFrameRect().pos.y << ")" << std::endl;
    comp.printInfo();

    angle = 90.0;
    comp.rotate(angle);
    comp.printInfo();

    comp.scale(2.0);
    comp.printInfo();

    std::cout << "Removing shape no.1" << std::endl;
    comp.removeShape(1);
    comp.printInfo();

    kuznetsova::point_t centre_circ{ 4.8, 1.0 };
    kuznetsova::Circle circleM(centre_circ, 2.5);
    kuznetsova::point_t centre_rect1{ 88.0, 100.0 };
    kuznetsova::Rectangle rectangleM1(centre_rect1, 2.0, 2.0);
    kuznetsova::point_t centre_rect2{ 1.0, 7.0 };
    kuznetsova::Rectangle rectangleM2(centre_rect2, 6.7, 2.1);

    std::shared_ptr<kuznetsova::Shape> rectanglePtr = std::make_shared<kuznetsova::Rectangle>(rectangleM1);
    std::shared_ptr<kuznetsova::Shape> circlePtr = std::make_shared<kuznetsova::Circle>(circleM);
    std::shared_ptr<kuznetsova::Shape> rectPtr = std::make_shared<kuznetsova::Rectangle>(rectangleM2);

    kuznetsova::CompositeShape compShM(rectanglePtr);
    compShM.addShape(circlePtr);
    compShM.addShape(rectPtr);

    kuznetsova::Matrix matrix; // the constructor of the matrix is called here
    matrix.add(circlePtr);
    matrix.add(rectanglePtr);
    matrix.add(rectPtr);
    matrix.addElementsFromCompSh(compShM);
    matrix.printMatrixInfo();
    return 0;
  }

  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }

  catch (std::out_of_range & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
}
