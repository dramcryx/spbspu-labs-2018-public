#define BOOST_TEST_MAIN mytests
#include <boost/test/included/unit_test.hpp>

#include <stdexcept>
#include <cmath>
#include <memory>

#include "composite-shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "matrix.hpp"
#include "shape.hpp"

const double EPS = 0.001;

namespace kuznetsova
{
  BOOST_AUTO_TEST_SUITE(RectTest)

  BOOST_AUTO_TEST_CASE(Rect_InvOfParTest)
  {
    kuznetsova::point_t centre{ 2.0, 3.7 };
    kuznetsova::point_t newcentre{ -5.0, -1.9 };
    kuznetsova::Rectangle Rectangle1(centre, 5.0, 11.0);
    double area = Rectangle1.getArea();
    Rectangle1.move(3.0, 12.0);
    BOOST_REQUIRE_EQUAL(Rectangle1.width_, 5.0);
    BOOST_REQUIRE_EQUAL(Rectangle1.height_, 11.0);
    BOOST_CHECK_CLOSE(Rectangle1.getArea(), area, EPS);
    Rectangle1.move(newcentre);
    BOOST_REQUIRE_EQUAL(Rectangle1.width_, 5.0);
    BOOST_REQUIRE_EQUAL(Rectangle1.height_, 11.0);
    BOOST_CHECK_CLOSE(Rectangle1.getArea(), area, EPS);
  }

  BOOST_AUTO_TEST_CASE(Rect_ScaleTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::Rectangle Rectangle1(centre, 5.0, 11.0);
    double area = Rectangle1.getArea();
    Rectangle1.scale(5.0);
    BOOST_CHECK_CLOSE(Rectangle1.getArea(), (area * pow(5.0, 2.0)), EPS);
  }

  BOOST_AUTO_TEST_CASE(Rect_InvalidWidthTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    BOOST_CHECK_THROW(kuznetsova::Rectangle Rectangle1(centre, -7.0, 9.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rect_InvalidHeightTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    BOOST_CHECK_THROW(kuznetsova::Rectangle Rectangle1(centre, 7.0, -9.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Rect_InvalidScaleFactorTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::Rectangle Rectangle1(centre, 5.0, 11.0);
    BOOST_CHECK_THROW(Rectangle1.scale(-3.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Change_FrameRect_Rotate_90_Test_Rect)
  {
    kuznetsova::point_t centre{ 3.5, -80.2 };
    kuznetsova::Rectangle rectangle(centre, 15.8, 20.4);
    double angle = 90.0;
    rectangle.rotate(angle);
    BOOST_REQUIRE_EQUAL(rectangle.angle_, 90.0);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 20.4, EPS);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 15.8, EPS);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, centre.x, EPS);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, centre.y, EPS);
  }

  BOOST_AUTO_TEST_CASE(Change_FrameRect_Rotate_45_Test_Rect)
  {
    kuznetsova::point_t centre{ 1.0, 1.0 };
    double w = sqrt(2.0);
    double h = sqrt(2.0);
    kuznetsova::Rectangle rectangle(centre, w, h);
    double angle = 45.0;
    rectangle.rotate(angle);
    BOOST_REQUIRE_EQUAL(rectangle.angle_, 45.0);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 2, EPS);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 2, EPS);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, centre.x, EPS);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, centre.y, EPS);
  }
  }
  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(CircleTests)

    BOOST_AUTO_TEST_SUITE(CircleTests)

    BOOST_AUTO_TEST_CASE(Circle_InvOfParTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::point_t newcentre{ -1.0, -1.0 };
    kuznetsova::Circle Circle1(centre, 3.4);
    double area = Circle1.getArea();
    Circle1.move(-9.0, 13.4);
    BOOST_REQUIRE_EQUAL(Circle1.radius_, 3.4);
    BOOST_REQUIRE_EQUAL(Circle1.getArea(), area);
    Circle1.move(newcentre);
    BOOST_REQUIRE_EQUAL(Circle1.radius_, 3.4);
    BOOST_REQUIRE_EQUAL(Circle1.getArea(), area);
  }

  BOOST_AUTO_TEST_CASE(Circle_ScaleTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::Circle Circle1(centre, 3.4);
    double area = Circle1.getArea();
    Circle1.scale(3.0);
    BOOST_REQUIRE_CLOSE_FRACTION(Circle1.getArea(), (area * pow(3.0, 2.0)), EPS);
  }

  BOOST_AUTO_TEST_CASE(Circle_InvalidRadiusTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    BOOST_CHECK_THROW(kuznetsova::Circle Circle1(centre, -8.9), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Circle_InvalidScaleFactorTest)
  {
    kuznetsova::point_t centre{ 1.0, 3.0 };
    kuznetsova::Circle circle1(centre, 3.4);
    BOOST_CHECK_THROW(circle1.scale(-5.0), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(Circle_RotationTest)
  {
    kuznetsova::point_t centre{ 2.5, -7.8 };
    kuznetsova::Circle circle1(centre, 7);
    double angle = 36.0;
    circle1.rotate(angle);
    BOOST_REQUIRE_EQUAL(circle1.angle_, 36.0);
  }

  BOOST_AUTO_TEST_SUITE_END()

  BOOST_AUTO_TEST_SUITE(CompositeShapeTest)

    BOOST_AUTO_TEST_CASE(ConstructorTest)
  {
    std::shared_ptr <kuznetsova::Shape> emptyShape = nullptr;
    BOOST_CHECK_THROW(kuznetsova::CompositeShape aShape(emptyShape), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre2{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre2, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);

    kuznetsova::CompositeShape bShape{ aShape };

    BOOST_CHECK_EQUAL(bShape.getSize(), aShape.getSize());
    BOOST_CHECK_EQUAL(bShape.angle_, aShape.angle_);
    BOOST_CHECK_CLOSE(bShape.getArea(), aShape.getArea(), EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().pos.x, aShape.getFrameRect().pos.x, EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().pos.y, aShape.getFrameRect().pos.y, EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().width, aShape.getFrameRect().width, EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().height, aShape.getFrameRect().height, EPS);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre2{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre2, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);

    double aSize = aShape.getSize();
    double aArea = aShape.getArea();
    double aAngle = aShape.angle_;

    kuznetsova::rectangle_t aFrameRect = aShape.getFrameRect();
    kuznetsova::CompositeShape bShape(std::move(aShape));
    kuznetsova::rectangle_t bFrameRect = bShape.getFrameRect();
    double bSize = bShape.getSize();
    double bArea = bShape.getArea();

    BOOST_CHECK_EQUAL(aShape.getSize(), 0);
    BOOST_CHECK_CLOSE(aFrameRect.width, bFrameRect.width, EPS);
    BOOST_CHECK_CLOSE(aFrameRect.height, bFrameRect.height, EPS);
    BOOST_CHECK_CLOSE(aFrameRect.pos.x, bFrameRect.pos.x, EPS);
    BOOST_CHECK_CLOSE(aFrameRect.pos.y, bFrameRect.pos.y, EPS);
    BOOST_CHECK_EQUAL(bSize, aSize);
    BOOST_CHECK_EQUAL(bShape.angle_, aAngle);
    BOOST_CHECK_CLOSE(bArea, aArea, EPS);
  }

  BOOST_AUTO_TEST_CASE(CopyAssigmentOperatorTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre3{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre3, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);
    kuznetsova::CompositeShape bShape(rect);

    bShape = aShape;

    BOOST_CHECK_EQUAL(bShape.getSize(), aShape.getSize());
    BOOST_CHECK_CLOSE(bShape.getArea(), aShape.getArea(), EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().pos.x, aShape.getFrameRect().pos.x, EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().pos.y, aShape.getFrameRect().pos.y, EPS);
  }

  BOOST_AUTO_TEST_CASE(MoveAssigmentOperatorTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre3{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre3, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);

    kuznetsova::CompositeShape bShape(rect);
    bShape = std::move(aShape);

    BOOST_CHECK_EQUAL(aShape.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(MovesTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre3{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre3, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);
    kuznetsova::rectangle_t frameRect = aShape.getFrameRect();
    double dx = 2.0;
    double dy = 5.0;
    aShape.move(dx, dy);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.x, frameRect.pos.x + dx, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.y, frameRect.pos.y + dy, EPS);

    kuznetsova::point_t newcentre = { 5.0, 0.0 };
    aShape.moveUsingStruct(newcentre, aShape);

    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.x, newcentre.x, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.y, newcentre.y, EPS);
  }

  BOOST_AUTO_TEST_CASE(ScaleTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::point_t centre3{ 2.7, 13.8 };
    std::shared_ptr <kuznetsova::Shape> circ =
      std::make_shared<kuznetsova::Circle>(kuznetsova::Circle(centre3, 5.6));
    kuznetsova::CompositeShape aShape(rect);
    aShape.addShape(circ);

    double aArea = aShape.getArea();
    double aWidth = aShape.getFrameRect().width;
    double aHeight = aShape.getFrameRect().height;
    kuznetsova::point_t apos = aShape.getFrameRect().pos;
    double factor = 3.0;
    aShape.scale(factor);

    BOOST_CHECK_CLOSE(aShape.getFrameRect().width, aWidth*factor, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().height, aHeight*factor, EPS);
    BOOST_CHECK_CLOSE(aShape.getArea(), aArea*factor*factor, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.x, apos.x, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.y, apos.y, EPS);
  }

  BOOST_AUTO_TEST_CASE(AddShapeTest)
  {
    kuznetsova::point_t centre1{ 1.0, 3.0 };
    std::shared_ptr <kuznetsova::Shape> rect =
      std::make_shared <kuznetsova::Rectangle>(kuznetsova::Rectangle(centre1, 5.0, 11.0));
    kuznetsova::CompositeShape aShape(rect);
    BOOST_CHECK_THROW(aShape.addShape(rect), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(RotationOfCSh)
  {
    kuznetsova::point_t centre{ 2.0, 2.0 };
    kuznetsova::point_t centre2{ 4.0, 2.0 };
    kuznetsova::Rectangle rectangle(centre, 2.0, 2.0);
    kuznetsova::Rectangle rectangle2(centre2, 2.0, 2.0);
    std::shared_ptr<kuznetsova::Shape> rect1 = std::make_shared<kuznetsova::Rectangle>(rectangle);
    std::shared_ptr<kuznetsova::Shape> rect2 = std::make_shared<kuznetsova::Rectangle>(rectangle2);
    kuznetsova::CompositeShape aShape(rect1);
    aShape.addShape(rect2);
    double aArea = aShape.getArea();
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.x, 3.0, EPS);
    BOOST_CHECK_CLOSE(aShape.getFrameRect().pos.y, 2.0, EPS);
    kuznetsova::CompositeShape bShape = aShape;
    double angle = 90.0;
    bShape.rotate(angle);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().pos.x, aShape.getFrameRect().pos.x, EPS);
    BOOST_CHECK_CLOSE(bShape.getFrameRect().pos.y, aShape.getFrameRect().pos.y, EPS);
    BOOST_CHECK_CLOSE(aArea, bShape.getArea(), EPS);
  }

  BOOST_AUTO_TEST_SUITE_END()

    BOOST_AUTO_TEST_SUITE(MatrixTests)

    BOOST_AUTO_TEST_CASE(addElementsFromCompSh)
  {
    kuznetsova::point_t centre_r1{ 2.8, 3.0 };
    kuznetsova::Rectangle rectangle1(centre_r1, 5.8, 7.2);
    kuznetsova::point_t centre_c{ 0.0, -4.9 };
    kuznetsova::Circle circle1(centre_c, 2.0);
    kuznetsova::point_t centre_r2{ -8.9, -12.6 };
    kuznetsova::Rectangle rectangle2(centre_r2, 24.03, 14.5);
    std::shared_ptr<kuznetsova::Shape> rectanglePtr = std::make_shared<kuznetsova::Rectangle>(rectangle1);
    std::shared_ptr<kuznetsova::Shape> circlePtr = std::make_shared<kuznetsova::Circle>(circle1);
    std::shared_ptr<kuznetsova::Shape> rectPtr = std::make_shared<kuznetsova::Rectangle>(rectangle2);
    kuznetsova::CompositeShape compSh(rectanglePtr);
    compSh.addShape(circlePtr);
    compSh.addShape(rectPtr);

    kuznetsova::Matrix matrix;
    matrix.addElementsFromCompSh(compSh);
    kuznetsova::rectangle_t FrameRectangle[3];
    FrameRectangle[0] = rectangle1.getFrameRect();
    FrameRectangle[1] = circle1.getFrameRect();
    FrameRectangle[2] = rectangle2.getFrameRect();

    BOOST_CHECK_EQUAL(matrix.getLayerCount(), 2);
    BOOST_CHECK_EQUAL(matrix.getLayerSize(0), 2);
    BOOST_CHECK_EQUAL(matrix.getLayerSize(1), 1);
    for (int i = 0; i < matrix.getLayerCount(); ++i) {
      for (int j = 0; j < matrix.getLayerSize(i); ++j) {
        kuznetsova::rectangle_t rectangle = matrix[i][j]->getFrameRect();
        BOOST_CHECK_CLOSE(rectangle.pos.x, FrameRectangle[i * 2 + j].pos.x, EPS);
        BOOST_CHECK_CLOSE(rectangle.pos.y, FrameRectangle[i * 2 + j].pos.y, EPS);
        BOOST_CHECK_CLOSE(rectangle.width, FrameRectangle[i * 2 + j].width, EPS);
        BOOST_CHECK_CLOSE(rectangle.height, FrameRectangle[i * 2 + j].height, EPS);
      }
    }
  }

  BOOST_AUTO_TEST_CASE(add)
  {
    kuznetsova::point_t centre_r1{ 3.0, -3.0 };
    kuznetsova::Rectangle rectangle1(centre_r1, 4.0, 2.0);
    kuznetsova::point_t centre_c{ 7.0, -3.0 };
    kuznetsova::Circle circle1(centre_c, 1.0);
    std::shared_ptr<kuznetsova::Shape> rectanglePtr = std::make_shared<kuznetsova::Rectangle>(rectangle1);
    std::shared_ptr<kuznetsova::Shape> circlePtr = std::make_shared<kuznetsova::Circle>(circle1);

    kuznetsova::CompositeShape compSh(rectanglePtr);
    compSh.addShape(circlePtr);
    kuznetsova::point_t centre_r2{ 2.0, 1.0 };
    kuznetsova::Rectangle rectangle2(centre_r2, 2.0, 4.0);
    kuznetsova::point_t centre_c2{ 5.0, 3.0 };
    kuznetsova::Circle circle2(centre_c2, 1.0);
    std::shared_ptr<kuznetsova::Shape> circ2 = std::make_shared<kuznetsova::Circle>(circle2);
    std::shared_ptr<kuznetsova::Shape> rect2 = std::make_shared<kuznetsova::Rectangle>(rectangle2);

    kuznetsova::Matrix matrix;
    matrix.addElementsFromCompSh(compSh);
    matrix.add(rect2);
    matrix.add(circ2);
    matrix.addElementsFromCompSh(compSh);

    kuznetsova::rectangle_t FrameRectangle[6];
    FrameRectangle[0] = rectangle1.getFrameRect();
    FrameRectangle[1] = circle1.getFrameRect();
    FrameRectangle[2] = rectangle2.getFrameRect();
    FrameRectangle[3] = circle2.getFrameRect();
    FrameRectangle[4] = rectangle1.getFrameRect();
    FrameRectangle[5] = circle1.getFrameRect();

    BOOST_CHECK_EQUAL(matrix.getLayerCount(), 2);
    BOOST_CHECK_EQUAL(matrix.getLayerSize(0), 4);
    BOOST_CHECK_EQUAL(matrix.getLayerSize(1), 2);
    for (int i = 0; i < matrix.getLayerCount(); ++i) {
      for (int j = 0; j < matrix.getLayerSize(i); ++j) {
        kuznetsova::rectangle_t rectangle = matrix[i][j]->getFrameRect();
        BOOST_CHECK_CLOSE(rectangle.pos.x, FrameRectangle[i * 4 + j].pos.x, EPS);
        BOOST_CHECK_CLOSE(rectangle.pos.y, FrameRectangle[i * 4 + j].pos.y, EPS);
        BOOST_CHECK_CLOSE(rectangle.width, FrameRectangle[i * 4 + j].width, EPS);
        BOOST_CHECK_CLOSE(rectangle.height, FrameRectangle[i * 4 + j].height, EPS);
      }
    }
  }

  BOOST_AUTO_TEST_CASE(Constructor_Copy)
  {
    kuznetsova::point_t centre_r1{ 2.8, 3.0 };
    kuznetsova::Rectangle rectangle1(centre_r1, 5.8, 7.2);
    kuznetsova::point_t centre_c{ 0.0, -4.9 };
    kuznetsova::Circle circle1(centre_c, 2.0);
    std::shared_ptr<kuznetsova::Shape> rectanglePtr = std::make_shared<kuznetsova::Rectangle>(rectangle1);
    std::shared_ptr<kuznetsova::Shape> circlePtr = std::make_shared<kuznetsova::Circle>(circle1);
    kuznetsova::CompositeShape compSh(rectanglePtr);
    compSh.addShape(circlePtr);

    kuznetsova::Matrix matrix;
    kuznetsova::point_t centre_r2{ 0.0, 0.0 };
    kuznetsova::Rectangle rectangle2(centre_r2, 8.1, 3.5);
    kuznetsova::point_t centre_c2{ -6.4, 3.5 };
    kuznetsova::Circle circle2(centre_c2, 5.9);
    std::shared_ptr<kuznetsova::Shape> circ2 = std::make_shared<kuznetsova::Circle>(circle2);
    std::shared_ptr<kuznetsova::Shape> rect2 = std::make_shared<kuznetsova::Rectangle>(rectangle2);
    matrix.add(rect2);
    matrix.add(circ2);
    matrix.addElementsFromCompSh(compSh);

    kuznetsova::Matrix matrix1(matrix);
    BOOST_CHECK_EQUAL(matrix.getLayerCount(), matrix1.getLayerCount());
    for (int i = 0; i < matrix.getLayerCount(); ++i) {
      BOOST_CHECK_EQUAL(matrix.getLayerSize(i), matrix1.getLayerSize(i));
      for (int j = 0; j < matrix.getLayerSize(i); ++j) {
        kuznetsova::rectangle_t rectangle = matrix[i][j]->getFrameRect();
        kuznetsova::rectangle_t rectangle1 = matrix1[i][j]->getFrameRect();
        BOOST_CHECK_CLOSE(rectangle.pos.x, rectangle1.pos.x, EPS);
        BOOST_CHECK_CLOSE(rectangle.pos.y, rectangle1.pos.y, EPS);
        BOOST_CHECK_CLOSE(rectangle.width, rectangle1.width, EPS);
        BOOST_CHECK_CLOSE(rectangle.height, rectangle1.height, EPS);
      }
    }
  }

  BOOST_AUTO_TEST_CASE(MatrixRotate)
  {
    kuznetsova::point_t centre1{ 0.0, 1.0 };
    kuznetsova::point_t centre2{ 3.0, 1.0 };
    kuznetsova::Rectangle rectangle1(centre1, 2.0, 2.0);
    kuznetsova::Rectangle rectangle2(centre2, 2.0, 2.0);
    std::shared_ptr<kuznetsova::Shape> rect1 = std::make_shared<kuznetsova::Rectangle>(rectangle1);
    std::shared_ptr<kuznetsova::Shape> rect2 = std::make_shared<kuznetsova::Rectangle>(rectangle2);
    kuznetsova::CompositeShape compSh(rect1);
    compSh.addShape(rect2);

    kuznetsova::Matrix matrix;
    matrix.addElementsFromCompSh(compSh);

    double angle = 45.0;
    compSh.rotate(angle);
    kuznetsova::Matrix matrix2;
    matrix2.addElementsFromCompSh(compSh);
    BOOST_CHECK_EQUAL(matrix.getLayerSize(0), 2);
    BOOST_CHECK_EQUAL(matrix2.getLayerSize(0), 2);
  }

  BOOST_AUTO_TEST_CASE(Taking_by_index)//operator[]
  {
    kuznetsova::point_t centre_r1{ 3.0, -3.0 };
    kuznetsova::Rectangle rectangle1(centre_r1, 4.0, 2.0);
    kuznetsova::point_t centre_c{ 7.0, -3.0 };
    kuznetsova::Circle circle1(centre_c, 1.0);
    std::shared_ptr<kuznetsova::Shape> rectanglePtr = std::make_shared<kuznetsova::Rectangle>(rectangle1);
    std::shared_ptr<kuznetsova::Shape> circlePtr = std::make_shared<kuznetsova::Circle>(circle1);
    kuznetsova::CompositeShape compSh(rectanglePtr);
    compSh.addShape(circlePtr);

    kuznetsova::Matrix matrix;
    kuznetsova::point_t centre_r2{ 2.0, 1.0 };
    kuznetsova::Rectangle rectangle2(centre_r2, 2.0, 4.0);
    kuznetsova::point_t centre_c2{ 5.0, 3.0 };
    kuznetsova::Circle circle2(centre_c2, 1.0);
    std::shared_ptr<kuznetsova::Shape> circ2 = std::make_shared<kuznetsova::Circle>(circle2);
    std::shared_ptr<kuznetsova::Shape> rect2 = std::make_shared<kuznetsova::Rectangle>(rectangle2);
    matrix.addElementsFromCompSh(compSh);
    matrix.add(rect2);
    matrix.add(circ2);
    matrix.addElementsFromCompSh(compSh);

    BOOST_REQUIRE_EQUAL(matrix[0][2]->getFrameRect().pos.x, rectangle2.getFrameRect().pos.x);
    BOOST_REQUIRE_EQUAL(matrix[0][3]->getFrameRect().pos.y, circle2.getFrameRect().pos.y);
    BOOST_REQUIRE_THROW(matrix[3][0], std::out_of_range);
    BOOST_REQUIRE_THROW(matrix[0][4], std::out_of_range);
  }

  BOOST_AUTO_TEST_SUITE_END();
}
