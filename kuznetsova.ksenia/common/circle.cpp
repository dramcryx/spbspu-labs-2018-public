#include "circle.hpp"

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdexcept>
#include <iostream>


kuznetsova::Circle::Circle(kuznetsova::point_t &centre, double radius) :
  Shape(),
  centre_(centre),
  angle_(0.0)
{
  if (radius < 0.0) 
  {
    radius_ = 0.0;
    throw std::invalid_argument("Radius cannot be negative");
  }
  else
  {
    radius_ = radius;
  }
}

double kuznetsova::Circle::getArea() const
{
  return M_PI * radius_ * radius_;
}

kuznetsova::rectangle_t kuznetsova::Circle::getFrameRect() const
{
  return kuznetsova::rectangle_t{ centre_, 2 * radius_, 2 * radius_ };
}

void kuznetsova::Circle::move(const kuznetsova::point_t &newcentre)
{
  centre_ = newcentre;
}

void kuznetsova::Circle::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void kuznetsova::Circle::scale(const double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Invalid factor");
  }
  radius_ *= factor;
}

void kuznetsova::Circle::rotate(const double angle)
{
  angle_ += angle;
  if (fabs(angle) >= 360.0)
  {
    angle_ = fmod(angle_, 360.0);
  }
}

std::unique_ptr<kuznetsova::Shape> kuznetsova::Circle::getCopy()
{
  return std::unique_ptr<kuznetsova::Shape>(new kuznetsova::Circle(*this));
}

void kuznetsova::Circle::printInfo() const
{
  std::cout << "Circle" << std::endl;
  std::cout << "Centre: " << centre_.x << ", " <<
    centre_.y << ";\n" << "Radius: " << radius_ << ";\n" <<
    "Angle: " << angle_ << ";\n" << "Area: " << getArea() << std::endl;
  const kuznetsova::rectangle_t frame = getFrameRect();
  std::cout << "Framing rectangle for the object:" << std::endl;
  std::cout << "Centre: " << frame.pos.x << ", " << frame.pos.y << std::endl;
  std::cout << "Width: " << frame.width << ";\n" << "Height: " << frame.height << std::endl;
}
