#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include <memory>

#include "shape.hpp"

namespace kuznetsova
{ 
  class Circle :
    public kuznetsova::Shape
  {
  public:

    double radius_;    
    kuznetsova::point_t centre_;
    double angle_;

    Circle(kuznetsova::point_t &centre, double radius);
    
    double getArea() const override;
    kuznetsova::rectangle_t getFrameRect() const override;
    void move(const kuznetsova::point_t &newcentre) override;
    void move(const double dx, const double dy) override;
    void scale(const double factor) override;
    void rotate(const double angle) override;
    std::unique_ptr<kuznetsova::Shape> getCopy() override;
    void printInfo() const override;
  };
};
#endif
