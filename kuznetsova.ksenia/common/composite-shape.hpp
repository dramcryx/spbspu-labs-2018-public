#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace kuznetsova
{
  class CompositeShape :
    public kuznetsova::Shape
  {
  public:
    std::unique_ptr <std::shared_ptr <kuznetsova::Shape>[]> list_;
    int size_;
    double angle_;

    CompositeShape(std::shared_ptr<kuznetsova::Shape> &newShape); 
    CompositeShape(const CompositeShape &other);
    CompositeShape(CompositeShape &&other);

    CompositeShape &operator= (const CompositeShape &other);
    CompositeShape &operator= (CompositeShape &&other);
    std::shared_ptr<kuznetsova::Shape> operator[] (const int j) const;

    double getArea() const override;
    kuznetsova::rectangle_t getFrameRect() const override;
    void move(const kuznetsova::point_t &newcentre) override;
    void move(const double dx, const double dy) override;
    void scale(const double factor) override;
    void rotate(const double angle) override;

    void moveUsingStruct(const kuznetsova::point_t &newcentre, const kuznetsova::CompositeShape &thisShape);
    void addShape(std::shared_ptr <Shape> &newShape);
    void removeShape(const int index);
    int getSize() const;
    std::unique_ptr<kuznetsova::Shape> getCopy() override;
    void printInfo() const;
  };
}

#endif //COMPOSITE_SHAPE_HPP
