#include "matrix.hpp"

#include <memory>
#include <iostream>
#include <cmath>

using namespace kuznetsova;

Matrix::Layer::Layer(const int size, const std::unique_ptr<std::shared_ptr<Shape>[]>::pointer &arrayOfShapes) :
  size_(size),
  arrayOfShapes_(new std::shared_ptr<Shape>[size_])
{
  for (int i = 0; i < size_; ++i) {
    arrayOfShapes_[i] = arrayOfShapes[i];
  }
}

std::shared_ptr<kuznetsova::Shape> Matrix::Layer::operator[](const int i) const
{
  if (i >= size_) {
    throw std::out_of_range("Index out of range");
  }
  return arrayOfShapes_[i];
}

Matrix::Matrix() : // constructor of the matrix
  LayerCount_(0),
  MaxLayerSize_(0),
  LayerSize_(nullptr),
  arrayOfShapes_(nullptr)
{
}

Matrix::Matrix(const Matrix &matrix) : //copy constructor of matrix class
  LayerCount_(matrix.LayerCount_), // number of the layers
  MaxLayerSize_(matrix.MaxLayerSize_) // maximum amount of shapes in the layer
{
  std::unique_ptr<int[]> NumberOfShape_tmp(new int[LayerCount_]); /* here we designate a place in memory for the massive of ints. 
                                                                   The size of massive is equal to the number of the layers.
                                                                   I.e. we create massive of amounts of shapes in each layer*/
  for (int i = 0; i < LayerCount_; ++i) { // this cycle helps us find out how many columns do we need
    NumberOfShape_tmp[i] = matrix.LayerSize_[i]; // each i-cell stores the amount of shapes in the i-layer 
  }
  LayerSize_.swap(NumberOfShape_tmp);

  std::unique_ptr<std::shared_ptr<Shape>[]> arrayOfShapes_tmp(new std::shared_ptr<Shape>[MaxLayerSize_ * LayerCount_]); /* here we designate
                                                                                                                        a place in memory for the two-dimensional massive of
                                                                                                                        intellectual pointers */
  for (int i = 0; i < LayerCount_; ++i) { // till the number of layers - lines
    for (int j = 0; j < LayerSize_[i]; ++j) { // till the number of shapes - columns
      arrayOfShapes_tmp[i * MaxLayerSize_ + j] = matrix.arrayOfShapes_[i * MaxLayerSize_ + j]->getCopy(); /* reminder: my getCopy function returns
                                                                                                          intellectual pointer (unique_ptr) to the instance
                                                                                                          of the particular class*/
    }
  }
  arrayOfShapes_.swap(arrayOfShapes_tmp);
}

Matrix::Matrix(Matrix &&matrix) : // transfering constructor
LayerCount_(matrix.LayerCount_),
MaxLayerSize_(matrix.MaxLayerSize_),
LayerSize_(std::move(matrix.LayerSize_)),
arrayOfShapes_(std::move(matrix.arrayOfShapes_))
{
  matrix.LayerCount_ = 0;
  matrix.MaxLayerSize_ = 0;
  matrix.LayerSize_.reset();
  matrix.arrayOfShapes_.reset();
}

Matrix &Matrix::operator=(const Matrix &matrix) // overloaded assignment operator
{
  if (this != &matrix) {
    LayerCount_ = matrix.LayerCount_;
    MaxLayerSize_ = matrix.MaxLayerSize_;
    std::unique_ptr<int[]> NumberOfShape_tmp(new int[LayerCount_]);
    for (int i = 0; i < LayerCount_; ++i) {
      NumberOfShape_tmp[i] = matrix.LayerSize_[i];
    }
    LayerSize_.swap(NumberOfShape_tmp);

    std::unique_ptr<std::shared_ptr<Shape>[]> arrayOfShapes_tmp(new std::shared_ptr<Shape>[MaxLayerSize_ * LayerCount_]);
    for (int i = 0; i < LayerCount_; ++i) {
      for (int j = 0; j < LayerSize_[i]; ++j) {
        arrayOfShapes_tmp[i * MaxLayerSize_ + j] = matrix.arrayOfShapes_[i * MaxLayerSize_ + j]->getCopy();
      }
    }
    arrayOfShapes_.swap(arrayOfShapes_tmp);
  }
  return *this;
}

Matrix &Matrix::operator=(Matrix &&matrix) // overloaded transfering operator
{
  if (this != &matrix) {
    LayerCount_ = matrix.LayerCount_;
    MaxLayerSize_ = matrix.MaxLayerSize_;
    LayerSize_ = std::move(matrix.LayerSize_);
    arrayOfShapes_ = std::move(matrix.arrayOfShapes_);

    matrix.LayerCount_ = 0;
    matrix.MaxLayerSize_ = 0;
    matrix.LayerSize_.reset();
    matrix.arrayOfShapes_.reset();
  }
  return *this;
}

Matrix::Layer Matrix::operator[](const int j) const
{
  if (j > LayerCount_)
  {
    throw std::out_of_range("Wrong index!");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tmparrayOfShapes(new std::shared_ptr<Shape>[LayerSize_[j]]);
  for (int i = 0; i < LayerSize_[j]; ++i) {
    tmparrayOfShapes[i] = arrayOfShapes_[j * MaxLayerSize_ + i];
  }
  return std::move(Layer(LayerSize_[j], tmparrayOfShapes.get()));
}

void Matrix::addElementsFromCompSh(const CompositeShape &compSh) // here we add elements from the instance of CompositeShape class to the matrix
{
  for (int i = 0; i < compSh.getSize(); i++)
  {
    add(compSh[i]);
  }
}

void Matrix::add(const std::shared_ptr<Shape> shape) // we add a shape to the matrix
{
  int SuitableLayer = LayerCount_; // suitable layer is initialized with current number of layers 
  for (int i = 0; i < LayerCount_; ++i) 
  { // we move from layer 0 to the last layer
    if (IsSuitableLayer(i, shape)) { // if current layer is suitable for given shape
      SuitableLayer = i; // we indicate the suitable number with the number of layer suitable for given shape
      break;
    }
  }

  if (SuitableLayer == LayerCount_) { /* in case if suitable layer is the last layer, we add one more layer.
                                      We need to do this as later we are goint to resize the layer using
                                      overloaded assignment operator and swap function, so we need to have
                                      two massives of equal size to swap them*/
    addLayer(); 
  }

  if (LayerSize_[SuitableLayer] == MaxLayerSize_) { /* if the number of shapes in the suitable layer is full 
                                                    we need to resize the layer - add one more column
                                                    to the matrix*/
    resizeLayer(MaxLayerSize_ + 1); /*ATTENTION as a parameter we give already increased size of the layer*/
  }

  const int PlaceForNewShape = SuitableLayer * MaxLayerSize_ + LayerSize_[SuitableLayer]; // here we define a place for the shape
  arrayOfShapes_[PlaceForNewShape] = shape->getCopy(); // here we place the shape to the place indicated above
  LayerSize_[SuitableLayer]++; // here we show that the number of shapes in this layer has increased
}

int Matrix::getLayerCount() const // returns the number of layers in the matrix
{
  return LayerCount_;
}

int Matrix::getLayerSize(const int i) const // returns the number of shapes in the particular layer
{
  if (i >= LayerCount_) { // we check if given number exceeds the number of layers we have in the matrix
    throw std::out_of_range("Index out of range");
  }
  return LayerSize_[i];
}

bool Matrix::IsSuitableLayer(const int LayerNumber, const std::shared_ptr<Shape> shape) const /* we have certain requirements regarding conditions on which
                                                                                              shapes can be added to the particular layer. So, here we check
                                                                                              if given shape meets this conditions*/
{
  const rectangle_t rect1 = shape->getFrameRect();
  for (int i = 0; i < LayerSize_[LayerNumber]; ++i) {
    rectangle_t rect2 = arrayOfShapes_[LayerNumber * MaxLayerSize_ + i]->getFrameRect();
    if ((rect1.pos.x - rect1.width / 2) < (rect2.pos.x + rect2.width / 2)
      && (rect1.pos.x + rect1.width / 2) > (rect2.pos.x - rect2.width / 2)
      && (rect1.pos.y + rect1.height / 2) >(rect2.pos.y - rect2.height / 2)
      && (rect1.pos.y - rect1.height / 2) < (rect2.pos.y + rect2.height / 2)) {
      return false;
    }
  }
  return true;
}

void Matrix::resizeLayer(const int NewLayerSize)
{
  std::unique_ptr<std::shared_ptr<Shape>[]> tmparrayOfShapes(new std::shared_ptr<Shape>[NewLayerSize * LayerCount_]); /*we create a temporary intelligent pointer 
                                                                                                                      to the massive of shapes - as long as we store all 
                                                                                                                      the matrix data in one block we use multiplication
                                                                                                                      here, not increment to 1*/
  for (int i = 0; i < LayerCount_; ++i) { /* this is how we move around lines in the matrix - 
                                          actualy we use just the incrementation of a variable, 
                                          we compare its value with the number of layers until 
                                          we reach the last layer*/
    for (int j = 0; j < LayerSize_[i]; ++j) { /* this is how we move around columns
                                              we go from the first figure to another and...*/
      tmparrayOfShapes[i * NewLayerSize + j] = arrayOfShapes_[i * MaxLayerSize_ + j]; /* ...and add more place for the shapes
                                                                                      in the new layer using temporary intellectual
                                                                                      pointer to the massiv of shapes*/
    }
  }
  arrayOfShapes_.swap(tmparrayOfShapes);
  MaxLayerSize_ = NewLayerSize;
}

void Matrix::addLayer() // adds a layer to the matrix
{
  std::unique_ptr<int[]> tmp(new int[LayerCount_ + 1]);
  for (int i = 0; i < LayerCount_; ++i) 
  {
    tmp[i] = LayerSize_[i];
  }
  tmp[LayerCount_++] = 0;
  LayerSize_.swap(tmp);

  std::unique_ptr<std::shared_ptr<Shape>[]> tmparrayOfShapes(new std::shared_ptr<Shape>[MaxLayerSize_ * LayerCount_]);
  for (int i = 0; i < LayerCount_; ++i) 
  {
    for (int j = 0; j < LayerSize_[i]; ++j) 
    {
      tmparrayOfShapes[i * MaxLayerSize_ + j] = arrayOfShapes_[i * MaxLayerSize_ + j];
    }
  }
  arrayOfShapes_.swap(tmparrayOfShapes);
}

void Matrix::printMatrixInfo() const
{
  std::cout << "____________Matrix____________" << std::endl;
  for (int i = 0; i < LayerCount_; ++i) {
    std::cout << "Layer " << i << ":" << std::endl;
    for (int j = 0; j < LayerSize_[i]; ++j) {
      arrayOfShapes_[i * MaxLayerSize_ + j]->printInfo();
    }

    if (i == LayerCount_ - 1) {
      std::cout << "______________________________" << std::endl;
    }
    else {
      std::cout << std::endl;
    }

  }
}
