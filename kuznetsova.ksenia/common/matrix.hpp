#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

#include "shape.hpp"
#include "composite-shape.hpp"

namespace kuznetsova
{
  class Matrix
  {
  public:
    class Layer
    {
    public:
      Layer(int size, const std::unique_ptr<std::shared_ptr<kuznetsova::Shape>[]>::pointer &arrayOfShapes);
      //layer constructor declared above
      std::shared_ptr<kuznetsova::Shape> operator[](const int i) const;

    private:
      int size_; //amount of shapes in this particular layer
      std::unique_ptr<std::shared_ptr<kuznetsova::Shape>[]> arrayOfShapes_; //array of shapes in the layer 
    };

    Matrix(); // constructor for matrix
    Matrix(const Matrix &matrix); // copy constructor for matrix
    Matrix(Matrix &&matrix); // move constructor for matrix
    ~Matrix() = default; // matrix destructor

    Matrix &operator=(const Matrix & matrix); // overloading of assignment operator
    Matrix &operator=(Matrix && matrix); // overloading of transfer operator
    Layer operator[](const int j) const;

    void addElementsFromCompSh(const kuznetsova::CompositeShape &compShape); // we need this function to add elements from CompositeShape to the layer
    void add(const std::shared_ptr<kuznetsova::Shape> shape);
    int getLayerCount() const; // returns number of layers
    int getLayerSize(int i) const; // returns number of shapes in the particular layer
    void printMatrixInfo() const;

  private:
    int LayerCount_; // number of layers
    int MaxLayerSize_; // maximum amount of shapes in the layer
    std::unique_ptr<int[]> LayerSize_; // intellectual pointer to the massive containing number of shapes in the layer
    std::unique_ptr<std::shared_ptr<kuznetsova::Shape>[]> arrayOfShapes_; // pointer to the array of shapes in a particular layer

    bool IsSuitableLayer(int LayerNumber, const std::shared_ptr<kuznetsova::Shape> shape) const;
    void resizeLayer(int NewLayerSize);
    void addLayer();
  };
}

#endif // MATRIX_HPP
