#include "rectangle.hpp"

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <stdexcept>

kuznetsova::Rectangle::Rectangle(kuznetsova::point_t &centre, double width, double height) :
  Shape(),
  centre_(centre),
  angle_(0.0)
{
  if (width < 0.0) 
  {
    width_ = 0.0;
    throw std::invalid_argument("Width cannot be negative");
  }
  else 
  {
    width_ = width;
  }
  if (height < 0.0) 
  {
    height_ = 0.0;
    throw std::invalid_argument("Height cannot be negative");
  }
  else
  {
    height_ = height;
  }
}

double kuznetsova::Rectangle::getArea() const
{
  return width_ * height_;
}

kuznetsova::rectangle_t kuznetsova::Rectangle::getFrameRect() const
{
  if (angle_ == 0.0)
  {
    return rectangle_t{ centre_, width_, height_ };
  }
  else
  {
    double sinb = sin(angle_ * M_PI / 180);
    double cosb = cos(angle_ * M_PI / 180);
    double width = height_ * fabs(sinb) + width_ * fabs(cosb);
    double height = height_ * fabs(cosb) + width_ * fabs(sinb);
    return rectangle_t{ centre_, width, height };
  }
}

void kuznetsova::Rectangle::move(const kuznetsova::point_t &newcentre)
{
  centre_ = newcentre;
}

void kuznetsova::Rectangle::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}
void kuznetsova::Rectangle::scale(const double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Invalid factor");
  }
  else
  {
    width_ *= factor;
    height_ *= factor;
  }
}
void kuznetsova::Rectangle::rotate(const double angle)
{
  angle_ += angle;
  if (fabs(angle) >= 360.0)
  {
    angle_ = fmod(angle_, 360.0);
  }
}

std::unique_ptr<kuznetsova::Shape> kuznetsova::Rectangle::getCopy()
{
  return std::unique_ptr<kuznetsova::Shape>(new Rectangle(*this));
}

void kuznetsova::Rectangle::printInfo() const
{
  std::cout << "Rectangle" << std::endl;
  std::cout << "Centre: " << centre_.x << ", " <<
    centre_.y << ";\n" << "Width: " << width_ << ";\n" <<
    "Height: " << height_ << ";'n" << "Angle: " << angle_ << ";\n" << "Area: " 
    << getArea() << std::endl;
  kuznetsova::rectangle_t rectangle = getFrameRect();
  std::cout << "Framing rectangle for the object:" << std::endl;
  std::cout << "Centre: " << rectangle.pos.x << ", " << rectangle.pos.y << std::endl;
  std::cout << "Width: " << rectangle.width << std::endl;
  std::cout << "Height: " << rectangle.height << std::endl;
}
