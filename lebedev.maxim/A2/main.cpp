#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

using namespace lebedev;

void printShapeInfo (Shape & shape)
{
  std::cout << "Area:" << shape.getArea() << std::endl;
  std::cout << "FrameRect center (x,y): " << shape.getFrameRect().pos.x << ", " << shape.getFrameRect().pos.y << std::endl;
  std::cout << "FrameRect width:" << shape.getFrameRect().width << std::endl;
  std::cout << "FrameRect height:" << shape.getFrameRect().height << std::endl;
}

int main()
{
  Rectangle objRectangle({0.0 ,0.0}, 6.0, 4.0);
  Circle objCircle({0.0, 0.0}, 5.0);

  std::cout << "Rectangle:\n";
  printShapeInfo(objRectangle);

  std::cout << "Circle:\n";
  printShapeInfo(objCircle);

  return 0;
}
