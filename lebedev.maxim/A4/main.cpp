#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix-shape.hpp"

using namespace lebedev;

int main()
{
  std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle({100.0, 0.0}, 3.0));
  std::shared_ptr<Shape> rec1 = std::make_shared<Rectangle>(Rectangle({0.0, 0.0}, 1.0, 1.0));
  std::shared_ptr<Shape> tri1 = std::make_shared<Triangle>(Triangle({0.0, 0.0}, {1.0, 6.0}, {20.0, 3.0}));

  MatrixShape ms;
  ms.addShape(circle1);
  ms.addShape(rec1);
  ms.addShape(tri1);


  std::cout << ms << std::endl;

}
