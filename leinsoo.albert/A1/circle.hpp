#ifndef AL_CIRCLE_HPP
#define AL_CIRCLE_HPP
#include "shape.hpp"

class Circle: public Shape
{
public:
  Circle(const point_t &center, const double r);
  ~Circle(); // Пустой деструктор
  virtual double getArea(); // вычисление площади круга
  virtual rectangle_t getFrameRect() ; //  получение ограничивающего прямоугольника круга
  virtual void move(const point_t &toPoint); // перемещение в точку
  virtual void move(const double dx, const double dy); // перемещение по смещению
  virtual void printShape(); // вывод данных
private:
  point_t center_; // центр окружности
  double r_;       // радиус окружности
};

#endif // AL_CIRCLE_HPP
