#include "rectangle.hpp"
#include <iostream>

Rectangle::Rectangle(const rectangle_t &pRectangle): // конструктор
  rectangle_(pRectangle)
{
  if (rectangle_.height <= 0 || rectangle_.width <= 0)
  {
    std::cerr << std::endl << "Init. error: incorrect height or/and width " << std::endl;
  }
}

double Rectangle::getArea() // вычисление площади прямоугольника
{
  return getHeight() * getWidth();
}

rectangle_t Rectangle::getFrameRect() // получение ограничивающего прямоугольника
{
  return (rectangle_);
}

void Rectangle::move(const point_t &toPoint) // перемещение прмоугольника в точку
{
  rectangle_.pos = toPoint;
}

void Rectangle::move(const double dx, const double dy) // перемещение прямоугольника по смещению
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
}

double Rectangle::getWidth() // возвращение ширины
{
  return rectangle_.width;
}

double Rectangle::getHeight() // возвращение высоты
{
  return rectangle_.height;
}

void Rectangle::printShape() // вывод параметров прямоугольника
{
  std::cout << "\t Area = " << getArea() <<  std::endl;
  std::cout << "\t Center X; Y= " << rectangle_.pos.x << "; " <<rectangle_.pos.y <<  std::endl;
  std::cout << "\t Width; Height = " << rectangle_.width << "; " << rectangle_.height << std::endl;
  std::cout << "\t outRectangle:"<<  std::endl;
  std::cout << "\t\t Center X; Y = " << getFrameRect().pos.x << "; " << getFrameRect().pos.y << std::endl;
  std::cout << "\t\t Width  = " << getFrameRect().width << std::endl;
  std::cout << "\t\t Height = " << getFrameRect().height << std::endl;
}

Rectangle::~Rectangle() // деструктор
{

}
