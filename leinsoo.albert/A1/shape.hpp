#ifndef AL_SHAPE
#define AL_SHAPE
#include "base-types.hpp"

class Shape // абстрактный класс геометрической фигуры
{
public:
  virtual ~Shape(); // деструктор
  virtual double getArea() = 0; // вычисление площади
  virtual rectangle_t getFrameRect() = 0; //  получение ограничивающего прямоугольника
  virtual void move(const point_t &ToPoint) = 0; // перемещение в точку
  virtual void move(const double dx, const double dy) = 0; // перемещение по смещению
  virtual void printShape() = 0; // вывод данных
};

#endif // AL_SHAPE
