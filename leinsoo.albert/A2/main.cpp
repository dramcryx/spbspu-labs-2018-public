#include <iostream>
#include <stdexcept>
#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

int main()
{
  Shape * testShape; //��������� �� ������� ����� ������

  //������������ ���������������
  //----------------------------

  //������������ ��������������� �����
  testShape = new Circle({10,10},5);
  std::cout << "Circle initialization test result:" << std::endl;
  testShape->printShape();
  try
  {
    testShape->scale(3);
    std::cout << "Circle scale test result:" << std::endl;
    testShape->printShape();
  }
  catch(std::invalid_argument &err)
  {
    std::cerr << err.what() << std::endl;
  }
  delete(testShape);

  //----------------------------
  //������������ ��������������� ������������
  testShape = new Triangle({5,10},{10,5},{0,0});
  std::cout << "Triangle initialization test result:" << std::endl;
  testShape->printShape();
  try
  {
    testShape->scale(5);
    std::cout << "Triangle scale test result:" << std::endl;
    testShape->printShape();
  }
  catch(std::invalid_argument &err)
  {
    std::cerr << err.what() << std::endl;
  }
  delete(testShape);

  //----------------------------
  //������������ ��������������� ��������������
  testShape = new RectangleC({{5,10},10,5});
  std::cout << "Rectangle initialization test result:" << std::endl;
  testShape->printShape();
  try
  {
    testShape->scale(5);
    std::cout << "Rectangle scale test result:" << std::endl;
    testShape->printShape();
  }
  catch(std::invalid_argument &err)
  {
    std::cerr << err.what() << std::endl;
  }
  delete(testShape);

  return 0;
}
