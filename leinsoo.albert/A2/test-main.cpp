#define BOOST_TEST_MODULE TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "triangle.hpp"
#include "rectangle.hpp"
#define EPSILON 0.001

BOOST_AUTO_TEST_SUITE(circle_tests)

BOOST_AUTO_TEST_CASE(circle_parameters_test)
{
  BOOST_CHECK_THROW(Circle({10,10}, -5), std::invalid_argument);
  Circle testCircle({10,10}, 5);
  BOOST_CHECK_THROW(testCircle.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(circle_move_toPoint)
{
  Circle testCircle({10,10},5);
  const double PrevFrameRectWidth = testCircle.getFrameRect().width;
  const double PrevFrameRectHeight= testCircle.getFrameRect().height;
  const double PrevCircleArea = testCircle.getArea();
  const double PrevRadius = testCircle.getRadius();

  testCircle.move({15,15});
  const double testX = testCircle.getFrameRect().pos.x;
  const double testY = testCircle.getFrameRect().pos.y;

  BOOST_CHECK_CLOSE(PrevFrameRectWidth, testCircle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(PrevFrameRectHeight, testCircle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(PrevCircleArea, testCircle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(PrevRadius, testCircle.getRadius(), EPSILON);
  BOOST_CHECK_CLOSE(15,testX,EPSILON);
  BOOST_CHECK_CLOSE(15,testY,EPSILON);
}

BOOST_AUTO_TEST_CASE(circle_move_dxdy)
{
  Circle testCircle({10,10},5);
  const double PrevFrameRectWidth = testCircle.getFrameRect().width;
  const double PrevFrameRectHeight= testCircle.getFrameRect().height;
  const double PrevCircleArea = testCircle.getArea();
  const double PrevRadius = testCircle.getRadius();

  testCircle.move(15,15);
  const double testX = testCircle.getFrameRect().pos.x;
  const double testY = testCircle.getFrameRect().pos.y;

  BOOST_CHECK_CLOSE(PrevFrameRectWidth, testCircle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(PrevFrameRectHeight, testCircle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(PrevCircleArea, testCircle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(PrevRadius, testCircle.getRadius(), EPSILON);
  BOOST_CHECK_CLOSE(25,testX,EPSILON);
  BOOST_CHECK_CLOSE(25,testY,EPSILON);
}

BOOST_AUTO_TEST_CASE(circle_scale)
{
  Circle testCircle({10,10},5);
  const double PrevFrameRectWidth = testCircle.getFrameRect().width;
  const double PrevFrameRectHeight= testCircle.getFrameRect().height;
  const double PrevCircleArea = testCircle.getArea();
  const double PrevRadius = testCircle.getRadius();
  const double k = 2;
  const double testX = testCircle.getFrameRect().pos.x;
  const double testY = testCircle.getFrameRect().pos.y;

  testCircle.scale(k);

  BOOST_CHECK_CLOSE(PrevFrameRectWidth * k, testCircle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(PrevFrameRectHeight * k, testCircle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(PrevCircleArea * k * k, testCircle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(PrevRadius * k, testCircle.getRadius(), EPSILON);
  BOOST_CHECK_CLOSE(10,testX,EPSILON);
  BOOST_CHECK_CLOSE(10,testY,EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()

//--------------------------------------------------------
BOOST_AUTO_TEST_SUITE(rectangle_tests)

BOOST_AUTO_TEST_CASE(rectangle_parameters_test)
{
  BOOST_CHECK_THROW(RectangleC({{10,10}, 5, -6}), std::invalid_argument);
  RectangleC testRectangle({{10,10}, 5, 6});
  BOOST_CHECK_THROW(testRectangle.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rectangle_move_toPoint)
{
  RectangleC testRect({{10,10}, 5, 6});
  const double PrevFrameRectWidth = testRect.getFrameRect().width;
  const double PrevFrameRectHeight= testRect.getFrameRect().height;
  const double PrevRectArea = testRect.getArea();

  testRect.move({15,15});
  const double testX = testRect.getFrameRect().pos.x;
  const double testY = testRect.getFrameRect().pos.y;

  BOOST_CHECK_CLOSE(PrevFrameRectWidth, testRect.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(PrevFrameRectHeight, testRect.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(PrevRectArea, testRect.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(15,testX,EPSILON);
  BOOST_CHECK_CLOSE(15,testY,EPSILON);
}

BOOST_AUTO_TEST_CASE(rectangle_move_dxdy)
{
  RectangleC testRect({{10,10}, 5, 6});

  const double PrevFrameRectWidth = testRect.getFrameRect().width;
  const double PrevFrameRectHeight= testRect.getFrameRect().height;
  const double PrevRectArea = testRect.getArea();

  testRect.move(15,15);
  const double testX = testRect.getFrameRect().pos.x;
  const double testY = testRect.getFrameRect().pos.y;

  BOOST_CHECK_CLOSE(PrevFrameRectWidth, testRect.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(PrevFrameRectHeight, testRect.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(PrevRectArea, testRect.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(25,testX,EPSILON);
  BOOST_CHECK_CLOSE(25,testY,EPSILON);
}

BOOST_AUTO_TEST_CASE(rectangle_scale)
{
  RectangleC testRect({{10,10}, 5, 6});

  double PrevFrameRectWidth = testRect.getFrameRect().width;
  const double PrevFrameRectHeight= testRect.getFrameRect().height;
  const double PrevRectArea = testRect.getArea();
  const double k = 2;
  const double testX = testRect.getFrameRect().pos.x;
  const double testY = testRect.getFrameRect().pos.y;

  testRect.scale(k);

  BOOST_CHECK_CLOSE(PrevFrameRectWidth * k, testRect.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(PrevFrameRectHeight * k, testRect.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(PrevRectArea * k * k, testRect.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(10,testX,EPSILON);
  BOOST_CHECK_CLOSE(10,testY,EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()

//-------------------------------------
BOOST_AUTO_TEST_SUITE(triangle_tests)

BOOST_AUTO_TEST_CASE(triangle_parameters_test)
{
  BOOST_CHECK_THROW(Triangle({0,10}, {0,20}, {0,30}), std::invalid_argument);
  Triangle testTriangle({0,10}, {10,0}, {0,0});
  BOOST_CHECK_THROW(testTriangle.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(triangle_move_toPoint)
{
  Triangle testTriangle({0,10}, {10,0}, {0,0});
  const double PrevFrameRectWidth = testTriangle.getFrameRect().width;
  const double PrevFrameRectHeight= testTriangle.getFrameRect().height;
  const double PrevTriangleArea = testTriangle.getArea();

  testTriangle.move({15,15});
  const double testX = testTriangle.getMassCenter().x;
  const double testY = testTriangle.getMassCenter().y;

  BOOST_CHECK_CLOSE(PrevFrameRectWidth, testTriangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(PrevFrameRectHeight, testTriangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(PrevTriangleArea, testTriangle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(15,testX,EPSILON);
  BOOST_CHECK_CLOSE(15,testY,EPSILON);
}

BOOST_AUTO_TEST_CASE(circle_move_dxdy)
{
  Triangle testTriangle({0,10}, {10,0}, {0,0});
  const double PrevFrameRectWidth = testTriangle.getFrameRect().width;
  const double PrevFrameRectHeight= testTriangle.getFrameRect().height;
  const double PrevTriangleArea = testTriangle.getArea();

  testTriangle.move(15,15);
  const double testX = testTriangle.getMassCenter().x;
  const double testY = testTriangle.getMassCenter().y;

  BOOST_CHECK_CLOSE(PrevFrameRectWidth, testTriangle.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(PrevFrameRectHeight, testTriangle.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(PrevTriangleArea, testTriangle.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(18.33333333333,testX,EPSILON);
  BOOST_CHECK_CLOSE(18.33333333333,testY,EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
