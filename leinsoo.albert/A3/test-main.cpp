﻿#define BOOST_TEST_MODULE TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "triangle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#define EPSILON 0.001

BOOST_AUTO_TEST_SUITE(composite_shape_tests)

Circle     testShape0Circle({3,0}, 3);
Circle     testShape1Circle({-5,-5}, 5);
RectangleC testShape2Rect({{0,4}, 8, 4});
RectangleC testShape3Rect({{-6,1}, 6, 6});
RectangleC testShape4Rect({{0,0}, 2, 2});
Triangle   testShape5Triangle({2,-7}, {5,-1}, {8,-7});

std::unique_ptr<CompositeShape>testCompositeShape (new CompositeShape());

BOOST_AUTO_TEST_CASE(composite_shape_creating_test)
{
  std::shared_ptr<Shape> testShape0Circle ( new Circle({3,0}, 3));
  testCompositeShape->addShape(testShape0Circle);
  std::shared_ptr<Shape> testShape1Circle ( new Circle({-5,-5}, 5));
  testCompositeShape->addShape(testShape1Circle);
  std::shared_ptr<Shape> testShape2Rect ( new RectangleC({{0,4}, 8, 4}));
  testCompositeShape->addShape(testShape2Rect);
  std::shared_ptr<Shape> testShape3Rect ( new RectangleC({{-6,1}, 6, 6}));
  testCompositeShape->addShape(testShape3Rect);
  std::shared_ptr<Shape> testShape4Rect ( new RectangleC({{0,0}, 2, 2}));
  testCompositeShape->addShape(testShape4Rect);
  std::shared_ptr<Shape> testShape5Triangle ( new Triangle({2,-7}, {5,-1}, {8,-7}));
  testCompositeShape->addShape(testShape5Triangle);

  BOOST_CHECK_CLOSE(testCompositeShape->getArea(),  testShape0Circle->getArea() +
                    testShape1Circle->getArea() +
                    testShape2Rect->getArea() +
                    testShape3Rect->getArea() +
                    testShape4Rect->getArea() +
                    testShape5Triangle->getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(composite_shape_add_invalid_test)
{
  std::shared_ptr<Shape> nullShape;

  BOOST_CHECK_THROW(testCompositeShape->addShape(nullShape), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(composite_shape_move_test)
{
  testShape2Rect.move(2,1);
  rectangle_t prevFrame = testCompositeShape->getFrameRect();
  testCompositeShape->move(2,1);

  BOOST_CHECK_CLOSE(prevFrame.width,testCompositeShape->getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(prevFrame.height,testCompositeShape->getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(prevFrame.pos.x + 2, testCompositeShape->getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(prevFrame.pos.y + 1, testCompositeShape->getFrameRect().pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(composite_shape_scale_test)
{
  const double areaBeforeScale = testCompositeShape->getFrameRect().width *
                                 testCompositeShape->getFrameRect().height;
  const double k = 2;
  testCompositeShape->scale(k);

  const double areaAfterScale = testCompositeShape->getFrameRect().width *
                                testCompositeShape->getFrameRect().height;
  BOOST_CHECK_CLOSE(areaBeforeScale *k *k,areaAfterScale,EPSILON);

  BOOST_CHECK_THROW(testCompositeShape->scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
