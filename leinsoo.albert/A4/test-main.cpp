﻿#define BOOST_TEST_MODULE TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "triangle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#define EPSILON 0.001

BOOST_AUTO_TEST_SUITE(composite_shape_tests)

std::unique_ptr<CompositeShape>testCompositeShape (new CompositeShape());
std::shared_ptr<Shape> testShape0Circle   ( new Circle({3,0}, 3));
std::shared_ptr<Shape> testShape1Circle   ( new Circle({-5,-5}, 5));
std::shared_ptr<Shape> testShape2Rect     ( new RectangleC({{0,4}, 8, 4}));
std::shared_ptr<Shape> testShape3Rect     ( new RectangleC({{-6,1}, 6, 6}));
std::shared_ptr<Shape> testShape4Rect     ( new RectangleC({{0,0}, 2, 2}));
std::shared_ptr<Shape> testShape5Triangle ( new Triangle({2,-7}, {5,-1}, {8,-7}));

BOOST_AUTO_TEST_CASE(composite_shape_creating_test)
{
testCompositeShape->addShape(testShape0Circle);
testCompositeShape->addShape(testShape1Circle);
testCompositeShape->addShape(testShape2Rect);
testCompositeShape->addShape(testShape3Rect);
testCompositeShape->addShape(testShape4Rect);
testCompositeShape->addShape(testShape5Triangle);

  BOOST_CHECK_CLOSE(testCompositeShape->getArea(),
                    testShape0Circle->getArea() +
                    testShape1Circle->getArea() +
                    testShape2Rect->getArea() +
                    testShape3Rect->getArea() +
                    testShape4Rect->getArea() +
                    testShape5Triangle->getArea()
                    , EPSILON);

}

BOOST_AUTO_TEST_CASE(composite_shape_layers_count_test)
{
  testCompositeShape->rebuildLayers();
  const int layersCount =  testCompositeShape->shapeLayers_->getShapeCount();
  BOOST_CHECK_EQUAL(layersCount,3);
}

BOOST_AUTO_TEST_CASE(composite_shape_move_dx_dy_test)
{
  const double areaBeforeMove = testCompositeShape->getArea();
  const point_t  posBeforeMove = testCompositeShape->getFrameRect().pos;
  testCompositeShape->move(2,1);
  const double areaAfterMove = testCompositeShape->getArea();
  const point_t  posAfterMove = testCompositeShape->getFrameRect().pos;

  BOOST_CHECK_CLOSE(areaBeforeMove,areaAfterMove,EPSILON);
  BOOST_CHECK_CLOSE(posBeforeMove.x,posAfterMove.x - 2,EPSILON);
  BOOST_CHECK_CLOSE(posBeforeMove.y,posAfterMove.y - 1,EPSILON);
}
BOOST_AUTO_TEST_CASE(composite_shape_move_test)
{
  const double dXCirleInTestCompShapeBeforeMove = testCompositeShape->getFrameRect().pos.x
      - testShape0Circle->getFrameRect().pos.x;
  const double dYCirleInTestCompShapeBeforeMove = testCompositeShape->getFrameRect().pos.y
      - testShape0Circle->getFrameRect().pos.y;
  testCompositeShape->move({0,0});
  const double dXCirleInTestCompShapeAfterMove = testCompositeShape->getFrameRect().pos.x
      - testShape0Circle->getFrameRect().pos.x;
  const double dYCirleInTestCompShapeAfterMove = testCompositeShape->getFrameRect().pos.y
      - testShape0Circle->getFrameRect().pos.y;

  BOOST_CHECK_CLOSE(dXCirleInTestCompShapeBeforeMove,
                    dXCirleInTestCompShapeAfterMove,EPSILON);
  BOOST_CHECK_CLOSE(dYCirleInTestCompShapeBeforeMove,
                    dYCirleInTestCompShapeAfterMove,EPSILON);
}

BOOST_AUTO_TEST_CASE(composite_shape_rotate_test)
{
  const double areaBeforeRotate = testCompositeShape->getArea();
  testCompositeShape->rotateShape(90);
  testCompositeShape->rotateShape(-90);

  testCompositeShape->rebuildLayers();
  const double areaAfterRotate = testCompositeShape->getArea();
  BOOST_CHECK_CLOSE(areaBeforeRotate,areaAfterRotate,EPSILON);
}
BOOST_AUTO_TEST_CASE(composite_shape_layers_after_rotate_test)
{
  BOOST_CHECK_EQUAL(testCompositeShape->shapeLayers_->getShapeCount(),3);
}
BOOST_AUTO_TEST_SUITE_END()
