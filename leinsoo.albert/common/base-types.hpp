﻿#ifndef AL_BASE_TYPES
#define AL_BASE_TYPES

namespace
{
const double Al_PI = 3.14159265358979323846;
}

struct point_t
{
  double x;
  double y;
};

struct rectangle_t
{
  point_t pos;
  double width, height;
};

#endif // AL_BASE_TYPES
