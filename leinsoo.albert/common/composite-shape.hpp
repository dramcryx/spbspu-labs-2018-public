#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <memory>
#include "shape.hpp"

class CompositeShape : public Shape
{
public:
  CompositeShape();
  CompositeShape (const CompositeShape &compShape);
  CompositeShape & operator = (const CompositeShape &compShape);
  ~CompositeShape();
  virtual double getArea(); //вычисление площади
  virtual rectangle_t getFrameRect(); // ограничивающий прямоугольник
  virtual void move(const point_t &toPoint);//перемещение в точку
  virtual void move(const double dx, const double dy);//перемещение по смещению
  virtual void scale(const double scaleFactor);//масштабирование
  virtual void printShape();// вывод данных
  void addShape(std::shared_ptr<Shape> newShape); //добавление фигуры в массив
  virtual void rotateShape(const double angle); //вращение фигуры
  int getShapeCount();
  virtual void printShapeType(); // Вывести тип фигуры
  void rebuildLayers(); // разбить по слоям
  void printLayers ();  // Вывести слои
  virtual point_t getMassCenter();
  std::unique_ptr<CompositeShape> shapeLayers_; //слои фигур. Слой - это CompositeShape.
private:
  int shapeCount_; // кол-во фигур
  std::unique_ptr<std::shared_ptr<Shape>[]> shapeArray_; // динамический массив фигур
};

#endif // COMPOSITE_SHAPE_HPP
