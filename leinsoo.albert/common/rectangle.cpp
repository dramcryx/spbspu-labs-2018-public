#include "rectangle.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>

RectangleC::RectangleC(const rectangle_t &pRectangle): // конструктор
  rectangle_(pRectangle),
  angle_(0)
{
  if (rectangle_.height <= 0 || rectangle_.width <= 0)
  {
    throw std::invalid_argument("Error: Invalid rectangle parameters.");
  }
}

double RectangleC::getArea() // вычисление площади прямоугольника
{
  return getHeight() * getWidth();
}

rectangle_t RectangleC::getFrameRect() // получение ограничивающего прямоугольника
{
  //Xnew = x1+(x2-x1)*cos(A)-(y2-y1)*sin(A)
  //Ynew = y1+(x2-x1)*sin(A)+(y2-y1)*cos(A)
  double Ax = rectangle_.pos.x +
              ((rectangle_.pos.x - rectangle_.width/2) - rectangle_.pos.x) *
              cos(angle_ * Al_PI / 180) -
              ((rectangle_.pos.y - rectangle_.height/2) - rectangle_.pos.y) *
              sin(angle_ * Al_PI / 180);
  double Ay = rectangle_.pos.y +
              ((rectangle_.pos.x - rectangle_.width/2) - rectangle_.pos.x) *
              sin(angle_ * Al_PI / 180) +
              ((rectangle_.pos.y - rectangle_.height/2) - rectangle_.pos.y) *
              cos(angle_* Al_PI / 180);

  double Bx = rectangle_.pos.x +
              ((rectangle_.pos.x - rectangle_.width/2) - rectangle_.pos.x) *
              cos(angle_ * Al_PI / 180) -
              ((rectangle_.pos.y + rectangle_.height/2) - rectangle_.pos.y) *
              sin(angle_ * Al_PI / 180);
  double By = rectangle_.pos.y +
              ((rectangle_.pos.x - rectangle_.width/2) - rectangle_.pos.x) *
              sin(angle_ * Al_PI / 180) +
              ((rectangle_.pos.y + rectangle_.height/2) - rectangle_.pos.y) *
              cos(angle_* Al_PI / 180);

  double Cx = rectangle_.pos.x +
              ((rectangle_.pos.x + rectangle_.width/2) - rectangle_.pos.x) *
              cos(angle_ * Al_PI / 180) -
              ((rectangle_.pos.y + rectangle_.height/2) - rectangle_.pos.y) *
              sin(angle_ * Al_PI / 180);
  double Cy = rectangle_.pos.y +
              ((rectangle_.pos.x + rectangle_.width/2) - rectangle_.pos.x) *
              sin(angle_ * Al_PI / 180) +
              ((rectangle_.pos.y + rectangle_.height/2) - rectangle_.pos.y) *
              cos(angle_* Al_PI / 180);
  double Dx = rectangle_.pos.x +
              ((rectangle_.pos.x + rectangle_.width/2) - rectangle_.pos.x) *
              cos(angle_ * Al_PI / 180) -
              ((rectangle_.pos.y - rectangle_.height/2) - rectangle_.pos.y) *
              sin(angle_ * Al_PI / 180);
  double Dy = rectangle_.pos.y +
              ((rectangle_.pos.x + rectangle_.width/2) - rectangle_.pos.x) *
              sin(angle_ * Al_PI / 180) +
              ((rectangle_.pos.y - rectangle_.height/2) - rectangle_.pos.y) *
              cos(angle_* Al_PI / 180);

  double minX = std::min(Ax,std::min(Bx,std::min(Cx,Dx)));//minx
  double minY = std::min(Ay,std::min(By,std::min(Cy,Dy)));//miny
  double maxX = std::max(Ax,std::max(Bx,std::max(Cx,Dx)));//maxx
  double maxY = std::max(Ay,std::max(By,std::max(Cy,Dy)));//maxy

  // возвращаем структуру описывающего прамоугольника
  return
  {
    {
      (minX +(maxX-minX)/2),
      (minY +(maxY-minY)/2)
    },
    fabs(maxX-minX),
    fabs(maxY-minY)
  };
}

void RectangleC::move(const point_t &toPoint) // перемещение прмоугольника в точку
{
  rectangle_.pos = toPoint;
}

void RectangleC::move(const double dx, const double dy) // перемещение прямоугольника по смещению
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
}

void RectangleC::scale(const double scaleFactor) // масштабирование фигуры
{
  if (scaleFactor <= 0 )
  {
    throw std::invalid_argument("Error: Invalid rectangle scale argument");
  }
  rectangle_.width *= scaleFactor;
  rectangle_.height*= scaleFactor;
}

double RectangleC::getWidth() // возвращение ширины
{
  return rectangle_.width;
}

double RectangleC::getHeight() // возвращение высоты
{
  return rectangle_.height;
}

void RectangleC::printShape() // вывод параметров прямоугольника
{
  std::cout << "\t Rectangle:" << std::endl;
  std::cout << "\t Area = " << getArea() <<  std::endl;
  std::cout << "\t Center X; Y= " << rectangle_.pos.x << "; " <<rectangle_.pos.y <<  std::endl;
  std::cout << "\t Width  =  "<< rectangle_.width <<" ; Height = "  << rectangle_.height << "; " << std::endl;
  std::cout << "\t outRectangle:"<<  std::endl;
  std::cout << "\t\t Center X = "<< getFrameRect().pos.x <<" ; Y= " << getFrameRect().pos.y<< "; "  << std::endl;
  std::cout << "\t\t Width  = " << getFrameRect().width << std::endl;
  std::cout << "\t\t Height = " << getFrameRect().height << std::endl;
}

void RectangleC::rotateShape(const double angle)
{
  angle_-=angle;
}

void RectangleC::printShapeType() //   вывод параметров окружности
{
  std::cout << "Rectangle";
}

point_t RectangleC::getMassCenter()
{
  return {getFrameRect().pos.x,
          getFrameRect().pos.y};
}

RectangleC::~RectangleC()
{

}
