#ifndef AL_RECTANLE_HPP
#define AL_RECTANLE_HPP
#include "shape.hpp"

class RectangleC: public Shape
{
public:
  RectangleC(const rectangle_t &pRectangle);
  ~RectangleC();
  virtual double getArea(); // вычисление площади
  virtual rectangle_t getFrameRect() ; //  получение ограничивающего прямоугольника
  virtual void move(const point_t &toPoint); // перемещение в точку
  virtual void move(const double dx, const double dy); // перемещение по смещению
  virtual void scale(const double scaleFactor); // масштабирование фигуры
  double getWidth(); // расчет ширины по координатам
  double getHeight(); // расчет высоты по координатам
  virtual void printShape(); // вывод данных
  virtual void printShapeType(); // Вывести тип фигуры
  virtual void rotateShape(const double angle); //вращение фигуры
  virtual point_t getMassCenter(); //центр масс фигуры
private:
  rectangle_t rectangle_;
  double angle_;
};

#endif // AL_RECTANLE_HPP
