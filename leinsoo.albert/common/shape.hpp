#ifndef AL_SHAPE
#define AL_SHAPE
#include "base-types.hpp"

class Shape // абстрактный класс геометрической фигуры
{
public:
  virtual ~Shape(); // деструктор
  virtual double getArea() = 0; // вычисление площади
  virtual rectangle_t getFrameRect() = 0; //  получение ограничивающего прямоугольника
  virtual void move(const point_t &toPoint) = 0; // перемещение в точку
  virtual void move(const double dx, const double dy) = 0; // перемещение по смещению
  virtual void scale(const double scaleFactor) = 0; // масштабирование фигуры
  virtual void printShape() = 0; // вывод данных
  virtual void printShapeType() =0; // Вывести тип фигуры
  virtual void rotateShape(const double angle) = 0; //вращение фигуры
  virtual point_t getMassCenter() = 0; //центр масс фигуры
};

#endif // AL_SHAPE
