#include "triangle.hpp"
#include <cmath> // нужны математические функции
#include <iostream>
#include <stdexcept>

Triangle::Triangle(const point_t &pA, const point_t &pB, const point_t &pC): // конструктор
  pA_(pA),
  pB_(pB),
  pC_(pC)
{
  double a = sqrt((pB_.y - pC_.y) * (pB_.y - pC_.y) + (pB_.x - pC_.x) * (pB_.x - pC_.x));
  double b = sqrt((pA_.y - pC_.y) * (pA_.y - pC_.y) + (pA_.x - pC_.x) * (pA_.x - pC_.x));
  double c = sqrt((pB_.y - pA_.y) * (pB_.y - pA_.y) + (pB_.x - pA_.x) * (pB_.x - pA_.x));
  double cosA = (b*b + c*c - a*a) / (2 * b * c);
  if (trunc(((1 - cosA) * 1000)) == 0 )
  {
    throw std::invalid_argument("Error: Invalid triangle  arguments.");
  }

}

rectangle_t Triangle::getFrameRect() // получение ограничивающего пр¤моугольника
{

  double minX = std::min(pA_.x,std::min(pB_.x,pC_.x));//minx
  double minY = std::min(pA_.y,std::min(pB_.y,pC_.y));//miny
  double maxX = std::max(pA_.x,std::max(pB_.x,pC_.x));//maxx
  double maxY = std::max(pA_.y,std::max(pB_.y,pC_.y));//maxy

  // возвращаем структуру описывающего прамоугольника
  return
  {
    {
      (minX +(maxX-minX)/2),
      (minY +(maxY-minY)/2)
    },
    fabs(maxX-minX),
    fabs(maxY-minY)
  };

}

void Triangle::move(const point_t &toPoint) // перемещение треугольника в точку
// оносительно центра масс
{
  double dx = toPoint.x - getMassCenter().x;
  double dy = toPoint.y - getMassCenter().y;
  move(dx,dy);
}

void Triangle::move(const double dx, const double dy) // перемещение пр¤моугольника по смещению
{
  pA_.x+=dx;
  pB_.x+=dx;
  pC_.x+=dx;
  pA_.y+=dy;
  pB_.y+=dy;
  pC_.y+=dy;
}

void Triangle::scale(const double scaleFactor) // масштабирование фигуры
{
  if (scaleFactor <= 0)
  {
    throw std::invalid_argument ("Error: Invalid triangle scale argument.");
  }
  //масштабирование от центра масс вершины pA_
  double tempLength = sqrt (((pA_.x - getMassCenter().x) * (pA_.x - getMassCenter().x)) +
                            ((pA_.y - getMassCenter().y) * (pA_.y - getMassCenter().y))
                           );

  double tempCos = sqrt (((pA_.x  - getMassCenter().x) * (pA_.x  - getMassCenter().x)))
                   / tempLength;

  point_t tempA {0,0};

  if (pA_.x > getMassCenter().x)
  {
    tempA.x = getMassCenter().x +((tempLength * scaleFactor) * tempCos);
  }
  else
  {
    tempA.x = getMassCenter().x -((tempLength * scaleFactor) * tempCos);
  };
  if (pA_.y > getMassCenter().y)
  {
    tempA.y = getMassCenter().y +((tempLength * scaleFactor) * sqrt(1-(tempCos *tempCos)));
  }
  else
  {
    tempA.y = getMassCenter().y -((tempLength * scaleFactor) * sqrt(1-(tempCos *tempCos)));
  }

  //масштабирование от центра масс вершины pB_
  tempLength = sqrt (((pB_.x - getMassCenter().x) * (pB_.x - getMassCenter().x)) +
                     ((pB_.y - getMassCenter().y) * (pB_.y - getMassCenter().y))
                    );

  tempCos = sqrt (((pB_.x  - getMassCenter().x) * (pB_.x  - getMassCenter().x)))
            / tempLength;

  point_t tempB {0,0};

  if (pB_.x > getMassCenter().x)
  {
    tempB.x = getMassCenter().x +((tempLength * scaleFactor) * tempCos);
  }
  else
  {
    tempB.x = getMassCenter().x -((tempLength * scaleFactor) * tempCos);
  };
  if (pB_.y > getMassCenter().y)
  {
    tempB.y = getMassCenter().y +((tempLength * scaleFactor) * sqrt(1-(tempCos *tempCos)));
  }
  else
  {
    tempB.y = getMassCenter().y -((tempLength * scaleFactor) * sqrt(1-(tempCos *tempCos)));
  }

  //масштабирование от центра масс вершины pC_
  tempLength = sqrt (((pC_.x - getMassCenter().x) * (pC_.x - getMassCenter().x)) +
                     ((pC_.y - getMassCenter().y) * (pC_.y - getMassCenter().y))
                    );

  tempCos = sqrt (((pC_.x  - getMassCenter().x) * (pC_.x  - getMassCenter().x)))
            / tempLength;

  point_t tempC {0,0};

  if (pC_.x > getMassCenter().x)
  {
    tempC.x = getMassCenter().x +((tempLength * scaleFactor) * tempCos);
  }
  else
  {
    tempC.x = getMassCenter().x -((tempLength * scaleFactor) * tempCos);
  };
  if (pC_.y > getMassCenter().y)
  {
    tempC.y = getMassCenter().y +((tempLength * scaleFactor) * sqrt(1-(tempCos *tempCos)));
  }
  else
  {
    tempC.y = getMassCenter().y -((tempLength * scaleFactor) * sqrt(1-(tempCos *tempCos)));
  }

  pA_=tempA;
  pB_=tempB;
  pC_=tempC;
}

double Triangle::getArea() // вычисление площади треугольника по формуле полупериметра
{
  return std::abs( ((pB_.x - pA_.x) * (pC_.y - pA_.y)) - ((pC_.x - pA_.x) * (pB_.y - pA_.y)) ) / 2;
}

void Triangle::printShape() // вывод
{
  std::cout << "\t Triangle:" << std::endl;
  std::cout << "\t pA  X; Y: " << pA_.x << " ; " << pA_.y << std::endl;
  std::cout << "\t pB  X; Y: " << pB_.x << " ; " << pB_.y << std::endl;
  std::cout << "\t pC  X; Y: " << pC_.x << " ; " << pC_.y << std::endl;
  std::cout << "\t Area = " << getArea() <<  std::endl;
  std::cout << "\t Mass center X; Y = " << getMassCenter().x << "; " << getMassCenter().y << std::endl;
  std::cout << "\t outRectangle:"<<  std::endl;
  std::cout << "\t\t Center X; Y= " << getFrameRect().pos.x << "; " << getFrameRect().pos.y << std::endl;
  std::cout << "\t\t Width  = " << getFrameRect().width << std::endl;
  std::cout << "\t\t Height = " << getFrameRect().height << std::endl;
}

point_t Triangle::getMassCenter()  // определение  центра масс
{
  return
  {
    ((pA_.x+pB_.x+pC_.x)/3),
    ((pA_.y+pB_.y+pC_.y)/3)
  };
}

void Triangle::rotateShape(const double angle)
{
  //x' = x0 + (x-x0) cos(A) - (y-y0) sin(A)
//y' = y0 + (x-x0) sin(A) - (y-y0) cos(A)

  double Ax = getMassCenter().x
              + (getMassCenter().x - pA_.x) * cos((360 - angle) * Al_PI / 180)
              - (getMassCenter().y - pA_.y) * sin((360 - angle) * Al_PI / 180);
  double Ay = getMassCenter().y
              + (getMassCenter().x - pA_.x) * sin((360 - angle) * Al_PI / 180)
              - (getMassCenter().y - pA_.y) * cos((360 - angle) * Al_PI / 180);

  double Bx = getMassCenter().x
              + (getMassCenter().x - pB_.x) * cos((360 - angle) * Al_PI / 180)
              - (getMassCenter().y - pB_.y) * sin((360 - angle) * Al_PI / 180);
  double By = getMassCenter().y
              + (getMassCenter().x - pB_.x) * sin((360 - angle) * Al_PI / 180)
              - (getMassCenter().y - pB_.y) * cos((360 - angle) * Al_PI / 180);

  double Cx = getMassCenter().x
              + (getMassCenter().x - pC_.x) * cos((360 - angle) * Al_PI / 180)
              - (getMassCenter().y - pC_.y) * sin((360 - angle) * Al_PI / 180);
  double Cy = getMassCenter().y
              + (getMassCenter().x - pC_.x) * sin((360 - angle) * Al_PI / 180)
              - (getMassCenter().y - pC_.y) * cos((360 - angle) * Al_PI / 180);

  pA_ = {Ax,Ay};
  pB_ = {Bx,By};
  pC_ = {Cx,Cy};
}

void Triangle::printShapeType() //   вывод параметров окружности
{
  std::cout << "Triangle";
}

Triangle::~Triangle()
{

}
