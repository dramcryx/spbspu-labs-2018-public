#include "circle.hpp"

#include <cmath>
#include <stdexcept>

luzhbinin::Circle::Circle(const point_t & center, double radius) :
  center_(center),
  radius_(radius)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument("Invalid circle radius");
  }
}

double luzhbinin::Circle::getArea() const
{
  return (M_PI * radius_ * radius_);
}

luzhbinin::rectangle_t luzhbinin::Circle::getFrameRect() const
{
  return { { center_.x, center_.y}, radius_ * 2, radius_ * 2 };
}

void luzhbinin::Circle::move(const point_t & to)
{
  center_ = to;
}

void luzhbinin::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void luzhbinin::Circle::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Invalid circle ratio");
  }
  radius_ *= ratio;
}
