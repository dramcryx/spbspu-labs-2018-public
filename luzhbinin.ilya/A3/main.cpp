#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

using std::cout;
using std::cerr;
using std::endl;

void ShapeInfo(const luzhbinin::Shape & shape)
{
  cout << "Area = " << shape.getArea() << endl;
  luzhbinin::rectangle_t rectan = shape.getFrameRect();
  cout << "Frame rectangle:" << endl;
  cout << "x = " << rectan.pos.x << endl;
  cout << "y = " << rectan.pos.y << endl;
  cout << "width = " << rectan.width << endl;
  cout << "height = " << rectan.height << endl << endl;
}

int main()
{
  try
  {
    std::shared_ptr<luzhbinin::Shape> rectangle =
      std::make_shared<luzhbinin::Rectangle>(luzhbinin::Rectangle({ { 25.0, 25.0 }, 5.0, 10.0 }));
    std::shared_ptr<luzhbinin::Shape> circle =
      std::make_shared<luzhbinin::Circle>(luzhbinin::Circle({ 50.0, 50.0 }, 10.0));

    luzhbinin::CompositeShape compositeShape;
    compositeShape.addShape(rectangle);
    compositeShape.addShape(circle);
    ShapeInfo(compositeShape);

    compositeShape.move({ 0.0, 0.0 });
    ShapeInfo(compositeShape);

    compositeShape.move(-10.0, -10.0);
    ShapeInfo(compositeShape);

    compositeShape.scale(2.0);
    ShapeInfo(compositeShape);

    compositeShape.delShape(0);
    ShapeInfo(compositeShape);
  }
  catch (std::invalid_argument & error)
  {
    cerr << error.what() << endl;
    return 1;
  }
  catch (std::out_of_range & error)
  {
    cerr << error.what() << endl;
    return 1;
  }
  return 0;
}
