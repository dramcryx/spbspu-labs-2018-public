#include "rectangle.hpp"

#include <stdexcept>

luzhbinin::Rectangle::Rectangle(const rectangle_t & rectan) :
  rectan_(rectan)
{
  if (rectan.height < 0.0)
  {
    throw std::invalid_argument("Invalid rectangle height");
  }
  else if (rectan.width < 0.0)
  {
    throw std::invalid_argument("Invalid rectangle width");
  }
}

double luzhbinin::Rectangle::getArea() const
{
  return rectan_.height * rectan_.width;
}

luzhbinin::rectangle_t luzhbinin::Rectangle::getFrameRect() const
{
  return rectan_;
}

void luzhbinin::Rectangle::move(const point_t & to)
{
  rectan_.pos = to;
}

void luzhbinin::Rectangle::move(double dx, double dy)
{
  rectan_.pos.x += dx;
  rectan_.pos.y += dy;
}

void luzhbinin::Rectangle::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Invalid_rectangle_ratio");
  }
  rectan_.height *= ratio;
  rectan_.width *= ratio;
}
