#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

const double epsilon = 0.001;

BOOST_AUTO_TEST_SUITE(RectangleTest);

  BOOST_AUTO_TEST_CASE(InvarienceOfParameters)
  {
    lyalyuk::Rectangle rectangle({ 4.0, 3.0, { 2.0, 6.0 } });
    double area = rectangle.getArea();
    rectangle.move(20.0, 36.0);
    BOOST_REQUIRE_CLOSE_FRACTION(rectangle.getFrameRect().width, 4.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(rectangle.getFrameRect().height, 3.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(rectangle.getArea(), area, epsilon);
  };

  BOOST_AUTO_TEST_CASE(MoveOnCoord)
  {
    lyalyuk::Rectangle rectangle({ 4.0, 3.0, { 2.0, 6.0 } });
    rectangle.move(20.0, 36.0);
    const auto frameRect = rectangle.getFrameRect();
    BOOST_REQUIRE_CLOSE_FRACTION(frameRect.pos.x, 22.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(frameRect.pos.y, 42.0, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    lyalyuk::Rectangle rectangle({ 4.0, 3.0, { 2.0, 6.0 } });
    const double area = rectangle.getArea();
    rectangle.move({20.0, 36.0});
    const auto frameRect = rectangle.getFrameRect();
    BOOST_REQUIRE_CLOSE_FRACTION(frameRect.pos.x, 20.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(frameRect.pos.y, 36.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(area, rectangle.getArea(), epsilon);
  }

  BOOST_AUTO_TEST_CASE(Scale)
  {
    lyalyuk::Rectangle rectangle({ 4.0, 3.0, { 2.0, 6.0 } });
    double area = rectangle.getArea();
    rectangle.scale(2.0);

    BOOST_REQUIRE_CLOSE_FRACTION(rectangle.getArea(), area * pow(2.0, 2.0), epsilon);
  }

  BOOST_AUTO_TEST_CASE(ArgumentsConstructor)
  {
    BOOST_CHECK_THROW(lyalyuk::Rectangle rectangle({-2.0, -2.0, {4.0, 3.0}}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(ArgumentScale)
  {
    lyalyuk::Rectangle rectangle({ 4.0, 3.0, { 2.0, 6.0 } });
    BOOST_CHECK_THROW(rectangle.scale(-1.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(CircleTests)

  BOOST_AUTO_TEST_CASE(InvarienceOfParameters)
  {
    lyalyuk::Circle circle(4.0, { 8.0, 8.0 });
    double area = circle.getArea();
    circle.move(20.0, 36.0);
    BOOST_REQUIRE_CLOSE_FRACTION(circle.getFrameRect().width, 8.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(circle.getFrameRect().height, 8.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(circle.getArea(), area, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveOnCoord)
  {
    lyalyuk::Circle circle(4.0, { 8.0, 8.0 });
    circle.move(10.0, 13.0);
    const auto frameRect = circle.getFrameRect();
    BOOST_REQUIRE_CLOSE_FRACTION(frameRect.pos.x, 18.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(frameRect.pos.y, 21.0, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    lyalyuk::Circle circle(4.0, { 8.0, 8.0 });
    const double area = circle.getArea();
    circle.move({10.0, 13.0});
    const auto frameRect = circle.getFrameRect();
    BOOST_REQUIRE_CLOSE_FRACTION(frameRect.pos.x, 10.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(frameRect.pos.y, 13.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(area, circle.getArea(), epsilon);
  }

  BOOST_AUTO_TEST_CASE(Scale)
  {
    lyalyuk::Circle circle(4.0, { 8.0, 8.0 });
    double area = circle.getArea();
    circle.scale(3.0);
    BOOST_REQUIRE_CLOSE_FRACTION(circle.getArea(), (area * pow(3.0, 2.0)), epsilon);
  }

  BOOST_AUTO_TEST_CASE(ArgumentsConstructor)
  {
    BOOST_CHECK_THROW(lyalyuk::Circle circle(-2.0, {8.0, 8.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(ArgumentScale)
  {
    lyalyuk::Circle circle(4.0, { 8.0, 8.0 });
    BOOST_CHECK_THROW(circle.scale(-1.0), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(TriangleTests)

  BOOST_AUTO_TEST_CASE(InvarienceOfParameters)
  {
    lyalyuk::Triangle triangle({ 2.0, 4.0 }, { 2.0, 2.0 }, { 5.0, 3.0 });
    double area = triangle.getArea();
    double wBefore = triangle.getFrameRect().width;
    double hBefore = triangle.getFrameRect().height;
    triangle.move(24.0, 40.0);
    BOOST_REQUIRE_CLOSE_FRACTION(triangle.getFrameRect().width, wBefore, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(triangle.getFrameRect().height, hBefore, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(triangle.getArea(), area, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveOnCoord)
  {
    lyalyuk::Triangle triangle({ 2.0, 4.0 }, { 2.0, 2.0 }, { 5.0, 3.0 });
    const auto frameRectBefore = triangle.getFrameRect();
    triangle.move(10.0, 13.0);
    const auto frameRectAfter = triangle.getFrameRect();
    BOOST_REQUIRE_CLOSE_FRACTION(frameRectBefore.pos.x + 10.0, frameRectAfter.pos.x, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(frameRectBefore.pos.y + 13.0, frameRectAfter.pos.y, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveToPoint)
  {
    lyalyuk::Triangle triangle({ 2.0, 4.0 }, { 2.0, 2.0 }, { 5.0, 3.0 });
    const double area = triangle.getArea();
    triangle.move({10.0, 13.0});
    const auto frameRect = triangle.getFrameRect();
    BOOST_REQUIRE_CLOSE_FRACTION(frameRect.pos.x, 10.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(frameRect.pos.y, 13.0, epsilon);
    BOOST_REQUIRE_CLOSE_FRACTION(area, triangle.getArea(), epsilon);
  }

  BOOST_AUTO_TEST_CASE(Scale)
  {
    lyalyuk::Triangle triangle({ 2.0, 4.0 }, { 2.0, 2.0 }, { 5.0, 3.0 });
    double area = triangle.getArea();
    triangle.scale(4.0);
    BOOST_REQUIRE_CLOSE_FRACTION(triangle.getArea(), (area * pow(4.0, 2.0)), epsilon);
  }

  BOOST_AUTO_TEST_CASE(ArgumentsConstructor)
  {
    BOOST_CHECK_THROW(lyalyuk::Triangle triangle({1.0, 1.0}, {1.0, 1.0}, {1.0, 1.0}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(ArgumentScale)
  {
    lyalyuk::Triangle triangle({ 2.0, 4.0 }, { 2.0, 2.0 }, { 5.0, 3.0 });
    BOOST_CHECK_THROW(triangle.scale(-1.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

