#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

using namespace lyalyuk;

const double epsilon = epsilon;

BOOST_AUTO_TEST_SUITE(CompositeShapeTest)

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {0,0}));
    CompositeShape cs;
    cs.add(cir);
    CompositeShape cs1(cs);
    BOOST_CHECK_EQUAL(cs1[0], cs[0]);
    BOOST_CHECK_EQUAL(cs1.getSize(), 1);
  }

  BOOST_AUTO_TEST_CASE(ConstructorTest)
  {
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {0,0}));
    CompositeShape cs;
    cs.add(cir);
    BOOST_CHECK_EQUAL(cs.getSize(), 1);
    BOOST_CHECK_EQUAL(cs[0], cir);
  }

  BOOST_AUTO_TEST_CASE(OverloadOperatorTest)
  {
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {1,1}));
    CompositeShape cs;
    cs.add(cir);
    BOOST_CHECK_EQUAL(cs[0], cir);
  }

  BOOST_AUTO_TEST_CASE(OverloadOperatorWrongParametr1)
  {
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {0,0}));
    CompositeShape cs;
    cs.add(cir);
    BOOST_CHECK_THROW(cs[1013], std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(GetSizeTest)
  {
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {0,0}));
    CompositeShape cs;
    cs.add(cir);
    BOOST_CHECK_EQUAL(cs.getSize(), 1);
  }

  BOOST_AUTO_TEST_CASE(GetAreaTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({9,1, {1,1}}));
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {1,1}));
    CompositeShape cs;
    cs.add(cir);
    cs.add(rect);
    BOOST_CHECK_CLOSE(cs.getArea(),9+M_PI,epsilon);
  }

  BOOST_AUTO_TEST_CASE(GetAreaZeroSizeTest)
  {
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {0,0}));
    CompositeShape cs;
    cs.add(cir);
    cs.remove(0);
    BOOST_CHECK_CLOSE(cs.getArea(), 0, epsilon);
  }

  BOOST_AUTO_TEST_CASE(GetFrameRectTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({9,4, {0,0}}));
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {1,1}));
    CompositeShape cs;
    cs.add(cir);
    cs.add(rect);
    rectangle_t FrameRect = cs.getFrameRect();
    BOOST_CHECK_CLOSE(FrameRect.pos.x, 0, epsilon);
    BOOST_CHECK_CLOSE(FrameRect.pos.y, 0, epsilon);
    BOOST_CHECK_CLOSE(FrameRect.width, 9, epsilon);
    BOOST_CHECK_CLOSE(FrameRect.height,4, epsilon);
  }

  BOOST_AUTO_TEST_CASE(GetFrameRectTestZeroSize)
  {
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1,{1,1}));
    CompositeShape cs;
    cs.add(cir);
    cs.remove(0);
    rectangle_t FrameRect = cs.getFrameRect();
    BOOST_CHECK_CLOSE(FrameRect.pos.x, 0, epsilon);
    BOOST_CHECK_CLOSE(FrameRect.pos.y, 0, epsilon);
    BOOST_CHECK_CLOSE(FrameRect.width, 0, epsilon);
    BOOST_CHECK_CLOSE(FrameRect.height, 0, epsilon);
  }


  BOOST_AUTO_TEST_CASE(addCircle)
  {
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {1,1}));
    std::shared_ptr<Shape> cir2 = std::make_shared<Circle>(Circle(1, {1,1}));
    CompositeShape cs;
    cs.add(cir);
    cs.add(cir2);
    BOOST_CHECK_EQUAL(cs[1], cir2);
    BOOST_CHECK_EQUAL(cs.getSize(), 2);
    BOOST_CHECK_CLOSE(cs[1]->getArea(), M_PI, epsilon);
  }

  BOOST_AUTO_TEST_CASE(addRectangleTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({1,1,{1,1}}));
    CompositeShape cs;
    cs.add(rect);
    BOOST_CHECK_EQUAL(cs.getSize(), 1);
    BOOST_CHECK_CLOSE(cs[0]->getArea(), 1, epsilon);
  }

  BOOST_AUTO_TEST_CASE(removeTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({1,1, {1,1}}));
    CompositeShape cs;
    cs.add(rect);
    cs.remove(0);
    BOOST_CHECK_EQUAL(cs.getSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(removeTest_NULL_array)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({1,1, {1,1}}));
    CompositeShape cs;
    cs.add(rect);
    cs.remove(0);
    BOOST_CHECK_THROW(cs.remove(0), std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(removeTest_WrongParametr)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({1,1, {1,1}}));
    CompositeShape cs;
    cs.add(rect);
    BOOST_CHECK_THROW(cs.remove(212), std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(MoveToPointTest)
  {
    point_t p={10,10};
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({1,1,{0,0}}));
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {0,0}));
    CompositeShape cs;
    cs.add(cir);
    cs.add(rect);
    cs.move(p);
    rectangle_t rect0 = cs.getFrameRect();
    rectangle_t rect00 = cs.getFrameRect();
    BOOST_CHECK_CLOSE(rect0.pos.x, 10, epsilon);
    BOOST_CHECK_CLOSE(rect00.pos.x, 10, epsilon);
    BOOST_CHECK_CLOSE(rect0.pos.y, 10, epsilon);
    BOOST_CHECK_CLOSE(rect00.pos.y, 10, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveToPointAreaConst)
  {
    point_t p={10,10};
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({1,1,{0,0}}));
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1,{0,0}));
    CompositeShape cs;
    cs.add(cir);
    cs.add(rect);
    double S = cs.getArea();
    cs.move(p);
    double S1 = cs.getArea();
    BOOST_CHECK_CLOSE(S,S1,epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveXY_Test)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({1,1,{0,0}}));
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1,{0,0}));
    CompositeShape cs;
    cs.add(cir);
    cs.add(rect);
    cs.move(10,10);
    rectangle_t rect0 = cs.getFrameRect();
    rectangle_t rect00 = cs.getFrameRect();
    BOOST_CHECK_CLOSE(rect0.pos.x, 10, epsilon);
    BOOST_CHECK_CLOSE(rect00.pos.x, 10, epsilon);
    BOOST_CHECK_CLOSE(rect0.pos.y, 10, epsilon);
    BOOST_CHECK_CLOSE(rect00.pos.y, 10, epsilon);
  }

  BOOST_AUTO_TEST_CASE(MoveXY_AreaConst)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({1,1,{0,0}}));
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {0,0}));
    CompositeShape cs;
    cs.add(cir);
    cs.add(rect);
    double S = cs.getArea();
    cs.move(21,44);
    double S1 = cs.getArea();
    BOOST_CHECK_CLOSE(S,S1,epsilon);
  }

  BOOST_AUTO_TEST_CASE(ScaleTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({1,1,{0,0}}));
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {0,0}));
    CompositeShape cs;
    cs.add(rect);
    cs.add(cir);
    cs.scale(2);
    BOOST_CHECK_CLOSE(cs.getArea(),4+4*M_PI,0.01);
  }

  BOOST_AUTO_TEST_CASE(ScaleWrongParametr)
  {
    std::shared_ptr<Shape> cir = std::make_shared<Circle>(Circle(1, {0,0}));
    CompositeShape cs;
    cs.add(cir);
    BOOST_CHECK_THROW(cs.scale(-22),std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    CompositeShape cs;
    cs.add(rect);
    CompositeShape cs2(std::move(cs));
    BOOST_CHECK_THROW(cs[0], std::out_of_range);
    BOOST_CHECK_EQUAL(cs2[0], rect);
    BOOST_CHECK_EQUAL(rect.use_count(), 2);
  }

  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({2, 4, {11, 1}}));
    CompositeShape cs;
    cs.add(rect);
    CompositeShape cs2;
    cs2.add(rect2);
    cs2 = std::move(cs);
    BOOST_CHECK_THROW(cs[0], std::out_of_range);
    BOOST_CHECK_EQUAL(cs2[0], rect);
    BOOST_CHECK_EQUAL(rect2.use_count(), 1);
  }
  

BOOST_AUTO_TEST_SUITE_END()
