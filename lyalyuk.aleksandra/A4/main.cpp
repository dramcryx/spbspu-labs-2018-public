#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrixshape.hpp"

int main()
{
  using namespace lyalyuk;
  try
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    std::shared_ptr<Shape> rect1 = std::make_shared<Rectangle>(Rectangle({2, 1, {3, 4}}));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({2, 2, {8, 3}}));
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>(Rectangle({2, 2, {4.5, 5.5}}));
    std::shared_ptr<Shape> rect4 = std::make_shared<Rectangle>(Rectangle({1.3, 1.3, {5.1, 6.2}}));
    std::shared_ptr<Shape> rect5 = std::make_shared<Rectangle>(Rectangle({1, 1, {4.5, 1.5}}));
    std::shared_ptr<Shape> cir1 = std::make_shared<Circle>(Circle(1, {4.5, 1.5}));
    CompositeShape cs;
    cs.add(rect);
    cs.add(rect1);
    cs.add(rect2);
    cs.add(rect3);
    cs.add(rect4);
    cs.add(rect5);
    cs.add(cir1);
    cs.printInfo();
    cs.rotate(45);
    cs.printInfo();
    MatrixShape ms = cs.getSlayers();
    ms.printInfo();
  }
  catch (std::invalid_argument exception)
  {
    std::cout << exception.what();
  }
  catch (std::out_of_range exception)
  {
    std::cout << exception.what();
  }
  catch (...)
  {
    std::cout << "exception";
  }
  return 0;
}
