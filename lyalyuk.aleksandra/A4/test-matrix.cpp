#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "triangle.hpp"
#include "matrixshape.hpp"

using namespace lyalyuk;

BOOST_AUTO_TEST_SUITE(MatrixShapeTest)

  BOOST_AUTO_TEST_CASE(ConstructorWrongParameterTest)
  {
    std::shared_ptr<Shape> rect = nullptr;
    BOOST_CHECK_THROW(MatrixShape ms(rect), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    MatrixShape ms(rect);
    MatrixShape ms2(ms);
    BOOST_CHECK(ms(0,0) == ms2(0,0));
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    MatrixShape ms(rect);
    MatrixShape ms2(std::move(ms));
    BOOST_CHECK_THROW(ms(0,0), std::out_of_range);
    BOOST_CHECK(ms2(0,0) == rect);
    BOOST_CHECK(rect.use_count() == 2);
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({2, 4, {11, 1}}));
    MatrixShape ms(rect);
    MatrixShape ms2(rect2);
    ms2 = ms;
    BOOST_CHECK(ms(0,0) == ms2(0,0));
  }

  BOOST_AUTO_TEST_CASE(BracketsTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({2, 4, {11, 1}}));
    MatrixShape ms(rect);
    ms.addToMatrix(rect2);
    BOOST_CHECK(ms(0,0) == rect);
    BOOST_CHECK(ms(0,1) == rect2);
  }

  BOOST_AUTO_TEST_CASE(BracketsWrongParameterTest)
  {
    MatrixShape ms;
    BOOST_CHECK_THROW(ms(0,0), std::out_of_range);
  }


  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({2, 4, {11, 1}}));
    MatrixShape ms(rect);
    MatrixShape ms2(rect2);
    ms2 = std::move(ms);
    BOOST_CHECK_THROW(ms(0,0), std::out_of_range);
    BOOST_CHECK(ms2(0,0) == rect);
    BOOST_CHECK(rect2.use_count() == 1);
  }

  BOOST_AUTO_TEST_CASE(AddToMatrixTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({2, 4, {10, 1}}));
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>(Rectangle({1, 4, {10, 1}}));
    MatrixShape ms(rect);
    ms.addToMatrix(rect2);
    ms.addToMatrix(rect3);
    BOOST_CHECK(ms(0,1) == rect2);
    BOOST_CHECK(ms(1,0) == rect3);
  }

  BOOST_AUTO_TEST_CASE(AddToMatrixTestWrongParameter)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({2, 4, {10, 1}}));
    std::shared_ptr<Shape> rect3 = nullptr;
    MatrixShape ms(rect);
    BOOST_CHECK_THROW(ms.addToMatrix(rect3), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(AddToMatrixTestWrongParameter2)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    MatrixShape ms(rect);
    BOOST_CHECK_THROW(ms.addToMatrix(rect), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CheckInsertionTest)
  {
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(Rectangle({2, 4, {1, 1}}));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({2, 4, {0, 1}}));
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>(Rectangle({2, 4, {10, 1}}));
    MatrixShape ms;
    BOOST_CHECK(ms.checkIntersection(rect , rect2));
    BOOST_CHECK(!ms.checkIntersection(rect , rect3));
  }

  BOOST_AUTO_TEST_CASE(CheckInsertionWrongParametrTest)
  {
    std::shared_ptr<Shape> rect = nullptr;
    MatrixShape ms;
    BOOST_CHECK_THROW(ms.checkIntersection(rect , rect), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Rotate)

  BOOST_AUTO_TEST_CASE(rectangleRotate)
  {
    Rectangle rectangle({2, 1, { 0, 0 }});
    rectangle.rotate(M_PI / 180 * 30);
    const rectangle_t frame = rectangle.getFrameRect();
    BOOST_CHECK_CLOSE_FRACTION(frame.width, sqrt(3) + 0.5, 0.00001);
    BOOST_CHECK_CLOSE_FRACTION(frame.height, 1 + sqrt(0.75), 0.00001);
  }

  BOOST_AUTO_TEST_CASE(triangleRotate)
  {
    Triangle triangle({ { 0, 0 },{ 0, 1 },{ 1, 0 } });
    triangle.rotate(M_PI / 180 * 30);
    const rectangle_t frame = triangle.getFrameRect();
    BOOST_CHECK_CLOSE_FRACTION(frame.width, 1.36602, 0.0001);
    BOOST_CHECK_CLOSE_FRACTION(frame.height, 0.616, 0.001);
  }

  BOOST_AUTO_TEST_CASE(CSRotate)
  {
    CompositeShape composite;
    composite.add(std::make_shared<Circle>(Circle(3, { 1, 0 })));
    composite.add(std::make_shared<Rectangle>(Rectangle({3, 1, { -3, -7 }})));
    composite.add(std::make_shared<Triangle>(Triangle({ { 0, 0 },{ 0, 1 },{ 1, 0 } })));
    const double area = composite.getArea();
    composite.rotate(M_PI / 180 * 30);
    BOOST_CHECK_CLOSE_FRACTION(area, composite.getArea(), 0.01);
  }


BOOST_AUTO_TEST_SUITE_END()

