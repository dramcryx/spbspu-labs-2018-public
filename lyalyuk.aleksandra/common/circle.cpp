#include "circle.hpp"
#include <iostream>
#include <cmath>

lyalyuk::Circle::Circle(double radius, const point_t &position) :
  rad_(radius),
  pos_(position)
{
  if (rad_ < 0.0) {
    throw std::invalid_argument("incorrect radius");
  }
}

double lyalyuk::Circle::getArea() const noexcept
{
  return (M_PI * rad_ * rad_);
}

lyalyuk::rectangle_t lyalyuk::Circle::getFrameRect() const noexcept
{
  return rectangle_t{2 * rad_, 2 * rad_, pos_};
}

void lyalyuk::Circle::move(const point_t &p) noexcept
{
  pos_ = p;
}

void lyalyuk::Circle::move(double dx, double dy) noexcept
{
  pos_.x += dx;
  pos_.y += dy;
}

void lyalyuk::Circle::scale(double coefficient)
{
  if (coefficient < 0.0) {
    throw std::invalid_argument("incorrect parameter of scaling");
  }
  rad_ *= coefficient;
}

void lyalyuk::Circle::printInfo() const noexcept
{
  const rectangle_t rect = getFrameRect();
  std::cout << "circle:\ncenter=(" << pos_.x << ";" << pos_.y << ") \nradius=" << rad_ << "\n"
            << "CIRCLE S=" << getArea() << "\n" << "FrameRect: width=" << rect.width
            << ",heigth=" << rect.height << ",pos=(" << rect.pos.x << ";"
            << rect.pos.y << ")\n";
}

void lyalyuk::Circle::rotate(double alpha)
{
  (void)alpha;
}

