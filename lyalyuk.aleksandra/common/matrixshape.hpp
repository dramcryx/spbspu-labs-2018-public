#ifndef MATRIXSHAPE_HPP
#define MATRIXSHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace lyalyuk {
  class MatrixShape
  {
  public:
    MatrixShape();
    MatrixShape(const MatrixShape& matrixShape);
    MatrixShape(MatrixShape&& matrixShape);
    MatrixShape(const std::shared_ptr<Shape> &shape);

    const std::shared_ptr<Shape> operator ()(size_t index, size_t index2) const;
    MatrixShape & operator=(const MatrixShape& matrixShape);
    MatrixShape & operator=(MatrixShape&& matrixShape);

    void addToMatrix(const std::shared_ptr<Shape> &shape);
    bool checkIntersection(const std::shared_ptr<Shape> &shape1, const std::shared_ptr<Shape> &shape2);

    size_t getNumberOfColumns();
    size_t getNumberOfRows();

    void printInfo() noexcept;
  private:
    using array_type = std::unique_ptr<std::shared_ptr<Shape>[]>;
    array_type matrix_;

    size_t numberOfColumns_;
    size_t numberOfRows_;
  };

}

#endif
