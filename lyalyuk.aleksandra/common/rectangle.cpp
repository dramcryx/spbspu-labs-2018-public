#include "rectangle.hpp"
#include <iostream>
#include <cmath>

lyalyuk::Rectangle::Rectangle(const rectangle_t &rect) :
  alpha_(0),
  rect_(rect)
{
  if ((rect_.height < 0.0) || (rect_.width < 0.0)) {
    throw std::invalid_argument("incorrect height or width of rectangle");
  }
}

double lyalyuk::Rectangle::getArea() const noexcept
{
  return (rect_.width * rect_.height);
}

lyalyuk::rectangle_t lyalyuk::Rectangle::getFrameRect() const noexcept
{
  const double cosAlpha = cos(alpha_), sinAlpha = sin(alpha_);
  return {rect_.width * cosAlpha + rect_.height * sinAlpha,
          rect_.width * sinAlpha + rect_.height * cosAlpha,
          rect_.pos};
}

void lyalyuk::Rectangle::move(const point_t &p) noexcept
{
  rect_.pos = p;
}

void lyalyuk::Rectangle::move(double dx, double dy) noexcept
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void lyalyuk::Rectangle::scale(double coefficient)
{
  if (coefficient < 0.0) {
    throw std::invalid_argument("incorrect parameter of scaling");
  }
  rect_.width *= coefficient;
  rect_.height *= coefficient;
}

void lyalyuk::Rectangle::printInfo() const noexcept
{
  std::cout << "rectangle:\ncenter=(" << rect_.pos.x << ";" << rect_.pos.y << ")\nwidth=" << rect_.width
            << "\nrotation angle=" << alpha_ << "\nheight=" << rect_.height << "\nArea=" << getArea();
}

void lyalyuk::Rectangle::rotate(double alpha)
{
  alpha_ += alpha;
}
