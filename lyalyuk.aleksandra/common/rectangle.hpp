#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "shape.hpp"
#include "base-types.hpp"

namespace lyalyuk
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const rectangle_t &rect);

    double getArea() const noexcept override;

    rectangle_t getFrameRect() const noexcept override;

    void move(double dx, double dy) noexcept override;

    void move(const point_t &p) noexcept override;

    void scale(double coefficient) override;

    void printInfo() const noexcept override;

    void rotate(double alpha) override;

  private:
    double alpha_;
    rectangle_t rect_;
  };
}

#endif

