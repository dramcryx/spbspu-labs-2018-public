#include "triangle.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <assert.h>

lyalyuk::Triangle::Triangle(const point_t &firstPt, const point_t &secondPt, const point_t &thirdPt) :
  firstPt_(firstPt),
  secondPt_(secondPt),
  thirdPt_(thirdPt)
{
  if (getArea() <= 0) {
    throw std::invalid_argument("incorrect triangle parameters");
  }
}

double lyalyuk::Triangle::getArea() const noexcept
{
  double firstSide = sqrt(pow(secondPt_.x - firstPt_.x, 2)
                          + pow(secondPt_.y - firstPt_.y, 2));
  double secondSide = sqrt(pow(thirdPt_.x - firstPt_.x, 2)
                           + pow(thirdPt_.y - firstPt_.y, 2));
  double thirdSide = sqrt(pow(thirdPt_.x - secondPt_.x, 2)
                          + pow(thirdPt_.y - secondPt_.y, 2));
  double halfPer = (firstSide + secondSide + thirdSide) / 2;
  return sqrt(halfPer * (halfPer - firstSide) * (halfPer - secondSide) * (halfPer - thirdSide));
}

lyalyuk::rectangle_t lyalyuk::Triangle::getFrameRect() const noexcept
{
  double maxY = std::max(std::max(firstPt_.y, secondPt_.y),
                         thirdPt_.y);
  double maxX = std::max(std::max(firstPt_.x, secondPt_.x),
                         thirdPt_.x);
  double minY = std::min(std::min(firstPt_.y, secondPt_.y),
                         thirdPt_.y);
  double minX = std::min(std::min(firstPt_.x, secondPt_.x),
                         thirdPt_.x);
  return {sqrt(pow(maxX - minX, 2)), sqrt(pow(maxY - minY, 2)),
          {(minX + maxX) / 2, (minY + maxY) / 2}};
}

void lyalyuk::Triangle::move(const point_t &p) noexcept
{
  const point_t center = getFrameRect().pos;
  move(p.x - center.x, p.y - center.y);
}

void lyalyuk::Triangle::move(double dx, double dy) noexcept
{
  firstPt_ = {firstPt_.x + dx,
              firstPt_.y + dy};
  secondPt_ = {secondPt_.x + dx,
               secondPt_.y + dy};
  thirdPt_ = {thirdPt_.x + dx,
              thirdPt_.y + dy};
}

void lyalyuk::Triangle::scale(double coefficient)
{
  if (coefficient <= 0) {
    throw std::invalid_argument("Invalid scaling coefficient");
  }
  point_t pos = getFrameRect().pos;
  firstPt_ = {(firstPt_.x - pos.x) * coefficient + pos.x,
              (firstPt_.y - pos.y) * coefficient + pos.y};
  secondPt_ = {(secondPt_.x - pos.x) * coefficient + pos.x,
               (secondPt_.y - pos.y) * coefficient + pos.y};
  thirdPt_ = {(thirdPt_.x - pos.x) * coefficient + pos.x,
              (thirdPt_.y - pos.y) * coefficient + pos.y};
}

lyalyuk::point_t lyalyuk::Triangle::getCenter() const noexcept
{
  return point_t{((firstPt_.x + secondPt_.x + thirdPt_.x) / 3), ((firstPt_.y + secondPt_.y + thirdPt_.y) / 3)};
}

void lyalyuk::Triangle::printInfo() const noexcept
{
  std::cout << "A: " << firstPt_.x << ' ' << firstPt_.y
            << "B: " << secondPt_.x << ' ' << secondPt_.y
            << "C: " << thirdPt_.x << ' ' << thirdPt_.y << '\n';
}

void lyalyuk::Triangle::rotate(const double alpha) noexcept
{
  const point_t center = getCenter();
  const double sin_of_angle = sin(alpha);
  const double cos_of_angle = cos(alpha);
  firstPt_.x = (firstPt_.x - center.x) * cos_of_angle - (firstPt_.y - center.y) * sin_of_angle;
  firstPt_.y = (firstPt_.x - center.x) * sin_of_angle + (firstPt_.y - center.x) * cos_of_angle;
  secondPt_.x = (secondPt_.x - center.x) * cos_of_angle - (secondPt_.y - center.y) * sin_of_angle;
  secondPt_.y = (secondPt_.x - center.x) * sin_of_angle + (secondPt_.y - center.x) * cos_of_angle;
  thirdPt_.x = (thirdPt_.x - center.x) * cos_of_angle - (thirdPt_.y - center.y) * sin_of_angle;
  thirdPt_.y = (thirdPt_.x - center.x) * sin_of_angle + (thirdPt_.y - center.x) * cos_of_angle;
}


    
