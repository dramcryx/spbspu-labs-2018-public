#include <iostream>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace lysenko;

int main()
{
  std::shared_ptr<Rectangle> rectangle1 = std::make_shared<Rectangle>(Rectangle({ 3,3 }, 2, 2));
  std::shared_ptr<Rectangle> rectangle2 = std::make_shared<Rectangle>(Rectangle({ 3,3 }, 2, 2));
  std::shared_ptr<Circle> circle1 = std::make_shared<Circle>(Circle({10,10}, 2));
  std::shared_ptr<Circle> circle2 = std::make_shared<Circle>(Circle({5,5}, 10));
  std::shared_ptr<CompositeShape> compositeshape = std::make_shared<CompositeShape>(CompositeShape(circle2));
  compositeshape->addShape(rectangle1);
  Matrix matrix;
  matrix.addShape(rectangle2);
  matrix.addShape(circle1);
  matrix.addFromComposite(compositeshape);
  matrix.print(matrix);
  std::cout << "Rotate Tests: " << std::endl;
  std::cout << "size: " <<compositeshape->getSize() <<std::endl;
  std::cout << "x: " << compositeshape->getFrameRect().pos.x << "  y: " << compositeshape->getFrameRect().pos.y<< std::endl;
  compositeshape->rotate(45);
  std::cout << "size: " <<compositeshape->getSize() <<std::endl;
  std::cout << "x: " << compositeshape->getFrameRect().pos.x << "  y: " << compositeshape->getFrameRect().pos.y<< std::endl;
  std::shared_ptr<Rectangle> rectangle3 = std::make_shared<Rectangle>(Rectangle({ 3,3 }, 2, 2));
  compositeshape->addShape(rectangle3);
  compositeshape->rotate(90);
  std::cout << "size: " <<compositeshape->getSize() <<std::endl;
  std::cout << "x: " << compositeshape->getFrameRect().pos.x << "  y: " << compositeshape->getFrameRect().pos.y << std::endl;
  return 0;
}
