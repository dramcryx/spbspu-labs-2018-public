#define BOOST_TEST_MAIN

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace lysenko;

const double ACCURACY = 0.00001;

BOOST_AUTO_TEST_SUITE(RotateTest)

  BOOST_AUTO_TEST_CASE(RotationRectangleTest)
  {
    lysenko::Rectangle rectangle({0,0}, 3, 2);
    rectangle.rotate(45);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, 0, ACCURACY);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, 0, ACCURACY);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 3.5355339, ACCURACY);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 3.5355339, ACCURACY);
    rectangle.rotate(60);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.x, 0, ACCURACY);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().pos.y, 0, ACCURACY);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 3.4154155, ACCURACY);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 2.7083087, ACCURACY);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(RotateCompositeShapeTest)

  BOOST_AUTO_TEST_CASE(RotationCompositeShapeTest)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 3, 2));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 4, 3));
    std::shared_ptr<Shape> circle = std::make_shared<Circle>(Circle({ 0,0 }, 1));
    CompositeShape compositeshape(rectangle1);
    compositeshape.addShape(rectangle2);
    compositeshape.addShape(circle);
    double get_area = compositeshape.getArea();
    compositeshape.rotate(90);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().pos.x, 0, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().pos.x, 0, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().width, 4, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().height, 3, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getArea(), get_area, ACCURACY);
    compositeshape.rotate(45);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().pos.x, 0, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().pos.x, 0, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().width, 4.9497474, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().height, 4.9497474, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getArea(), get_area, ACCURACY);
    compositeshape.rotate(30);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().pos.x, 0, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().pos.x, 0, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().width, 3.9330536, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getFrameRect().height, 4.6401604, ACCURACY);
    BOOST_CHECK_CLOSE(compositeshape.getArea(), get_area, ACCURACY);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTest)

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 1, 1));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 2, 2));
    std::shared_ptr<Shape> rectangle3 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 3, 3));
    Matrix Matrix1;
    Matrix1.addShape(rectangle1);
    Matrix1.addShape(rectangle2);
    Matrix1.addShape(rectangle3);
    Matrix Matrix2(Matrix1);
    BOOST_CHECK_EQUAL(Matrix2.getNumOfLayers(), Matrix1.getNumOfLayers());
    BOOST_CHECK_EQUAL(Matrix2.getNumOfShape(), Matrix1.getNumOfShape());
    BOOST_CHECK_EQUAL(Matrix2.getShape(0), Matrix2.getShape(0));
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 1, 1));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 2, 2));
    std::shared_ptr<Shape> rectangle3 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 3, 3));
    Matrix Matrix1;
    Matrix1.addShape(rectangle1);
    Matrix1.addShape(rectangle2);
    Matrix1.addShape(rectangle3);
    Matrix Matrix2(std::move(Matrix1));
    BOOST_CHECK_EQUAL(Matrix2.getNumOfLayers(), 3);
    BOOST_CHECK_EQUAL(Matrix2.getNumOfShape(), 3);
    BOOST_CHECK_EQUAL(Matrix2.getShape(0), rectangle1);
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 1, 1));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 2, 2));
    std::shared_ptr<Shape> rectangle3 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 3, 3));
    Matrix Matrix1;
    Matrix1.addShape(rectangle1);
    Matrix1.addShape(rectangle2);
    Matrix1.addShape(rectangle3);
    Matrix Matrix2;
    Matrix2 = Matrix1;
    BOOST_CHECK_EQUAL(Matrix2.getNumOfLayers(), Matrix1.getNumOfLayers());
    BOOST_CHECK_EQUAL(Matrix2.getNumOfShape(), Matrix1.getNumOfShape());
    BOOST_CHECK_EQUAL(Matrix2.getShape(0), Matrix2.getShape(0));
  }

  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 1, 1));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 2, 2));
    std::shared_ptr<Shape> rectangle3 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 3, 3));
    Matrix Matrix1;
    Matrix1.addShape(rectangle1);
    Matrix1.addShape(rectangle2);
    Matrix1.addShape(rectangle3);
    Matrix Matrix2;
    Matrix2 = std::move(Matrix1);
    BOOST_CHECK_EQUAL(Matrix2.getNumOfLayers(), 3);
    BOOST_CHECK_EQUAL(Matrix2.getNumOfShape(), 3);
    BOOST_CHECK_EQUAL(Matrix2.getShape(0), rectangle1);
  }

  BOOST_AUTO_TEST_CASE(GetShapesTest)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 1, 1));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 2, 2));
    std::shared_ptr<Shape> rectangle3 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 3, 3));
    Matrix Matrix1;
    Matrix1.addShape(rectangle1);
    Matrix1.addShape(rectangle2);
    Matrix1.addShape(rectangle3);
    BOOST_CHECK_EQUAL(Matrix1.getShape(0), rectangle1);
    BOOST_CHECK_EQUAL(Matrix1.getShape(1), rectangle2);
    BOOST_CHECK_EQUAL(Matrix1.getShape(2), rectangle3);
  }

  BOOST_AUTO_TEST_CASE(GetFromCompositeTest)
  {
    std::shared_ptr<Shape> rectangle1 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 1, 1));
    std::shared_ptr<Shape> rectangle2 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 2, 2));
    std::shared_ptr<Shape> rectangle3 = std::make_shared<lysenko::Rectangle>(lysenko::Rectangle({ 0,0 }, 3, 3));
    std::shared_ptr<CompositeShape> compositeshape = std::make_shared<CompositeShape>(CompositeShape(rectangle1));
    compositeshape->addShape(rectangle2);
    Matrix Matrix1;
    Matrix1.addFromComposite(compositeshape);
    Matrix1.addShape(rectangle3);
    BOOST_CHECK_EQUAL(Matrix1.getNumOfLayers(), 3);
  }

BOOST_AUTO_TEST_SUITE_END()
