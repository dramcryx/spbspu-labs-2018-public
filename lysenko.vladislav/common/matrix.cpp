#include <iostream>
#include <cmath>
#include "matrix.hpp"

using namespace lysenko;

Matrix::Matrix():
  numOfShapes_(0),
  numOfLayers_(0),
  array_(nullptr),
  sizeOfLayer_(nullptr)
{

}

Matrix::Matrix(const Matrix & copyShape):
  numOfShapes_(copyShape.numOfShapes_),
  numOfLayers_(copyShape.numOfLayers_),
  array_(new std::shared_ptr<Shape>[numOfShapes_]),
  sizeOfLayer_(new int[numOfLayers_])
{
  for(int i = 0; i < numOfShapes_; i++)
  {
    array_[i] = copyShape.array_[i];
  }
  for(int i = 0; i < numOfLayers_; i++)
  {
    sizeOfLayer_[i] = copyShape.sizeOfLayer_[i];
  }
}

Matrix::Matrix(Matrix && moveShape):
  numOfShapes_(moveShape.numOfShapes_),
  numOfLayers_(moveShape.numOfLayers_)
{
  array_.swap(moveShape.array_);
  sizeOfLayer_.swap(moveShape.sizeOfLayer_);
  moveShape.array_.reset();
  moveShape.sizeOfLayer_.reset();
  moveShape.numOfLayers_ = 0;
  moveShape.numOfShapes_ = 0;
}

Matrix & Matrix::operator=(const Matrix & copyShape)
{
  numOfLayers_ = copyShape.numOfLayers_;
  numOfShapes_ = copyShape.numOfShapes_;
  array_ = std::unique_ptr<std::shared_ptr<Shape>[]>(new std::shared_ptr<Shape>[numOfShapes_]);
  sizeOfLayer_ = std::unique_ptr<int[]>(new int[numOfLayers_]);
  for(int i = 0; i < numOfLayers_; i++)
  {
    sizeOfLayer_[i] = copyShape.sizeOfLayer_[i];
  }
  for(int i = 0; i < numOfShapes_; i++)
  {
    array_[i] = copyShape.array_[i];
  }
  return *this;
}

Matrix &Matrix::operator=(Matrix && moveShape)
{
  if(this != &moveShape)
  {
    sizeOfLayer_.swap(moveShape.sizeOfLayer_);
    array_.swap(moveShape.array_);
    numOfLayers_ = moveShape.numOfLayers_;
    numOfShapes_ = moveShape.numOfShapes_;
    moveShape.numOfLayers_ = 0;
    moveShape.numOfShapes_ = 0;
    moveShape.sizeOfLayer_.reset();
    moveShape.array_.reset();
  }
  else
  {
    throw std::invalid_argument("Error! Moving same object");
  }
  return *this;
}

int Matrix::getNumOfLayers()
{
  return numOfLayers_;
}

int Matrix::getNumOfShape()
{
  return numOfShapes_;
}

void Matrix::addFromComposite(const std::shared_ptr<CompositeShape> &shape)
{
  if(shape->getSize() == 0)
  {
    throw std::invalid_argument("Error! Composite shape is empty");
  }
  for(int i = 0; i < shape->getSize(); i++)
  {
    addShape((*shape)[i]);
  }
}

void Matrix::addShape(const std::shared_ptr<Shape> shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Shape has a nullptr value");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tempArray(new std::shared_ptr<Shape>[numOfShapes_ + 1]);
  bool imposition = false;
  int  offset = 0;
  for (int  i = 0; i < numOfLayers_; i++)
  {
    imposition = true;
    for (int  j = 0; j < sizeOfLayer_[i]; j++)
    {
      rectangle_t rect1 = array_[offset + j]->getFrameRect();
      rectangle_t rect2 = shape->getFrameRect();
      if (fabs(rect1.pos.x - rect2.pos.x) <= rect1.width / 2 + rect2.width / 2 &&
          fabs(rect1.pos.y - rect2.pos.y) <= rect1.height / 2 + rect2.height / 2)
      {
        imposition = false;
      }
      tempArray[offset + j] = array_[offset + j];
    }
    offset += sizeOfLayer_[i];

    if (imposition)
    {
      tempArray[offset] = shape;
      for (int  j = offset; j < numOfShapes_; j++)
      {
        tempArray[j + 1] = array_[j];
      }
      sizeOfLayer_[i]++;
      numOfShapes_++;
      break;
    }
  }

  if(!imposition)
  {
    tempArray[numOfShapes_++] = shape;
    std::unique_ptr<int []> sizesTmp(new int [numOfLayers_+ 1]);
    for (int  i = 0; i < numOfLayers_; i++)
    {
      sizesTmp[i] = sizeOfLayer_[i];
    }
    sizesTmp[numOfLayers_++] = 1;
    sizeOfLayer_.swap(sizesTmp);
  }
  array_.swap(tempArray);
}

std::shared_ptr<Shape> Matrix::getShape(int number)const
{
  return array_[number];
}

int Matrix::getLayerSize(int numb)
{
  if(numb > numOfLayers_)
  {
    throw std::invalid_argument("Error! This matrix does not exist this layer");
  }
  return sizeOfLayer_[numb];
}

void Matrix::print(Matrix &matrix)
{
  std::cout << "Count of layers: " <<  matrix.getNumOfLayers() << std::endl;
  std::cout << "Count of shapes: " << matrix.getNumOfShape() << std::endl;
  std::cout << std::endl;
  for(int i = 0; i < matrix.getNumOfLayers(); i++)
  {
    std::cout << "Layer number: " << i + 1 << std::endl;
    std::cout << "Count shapes in layer: " << matrix.getLayerSize(i) << std::endl;
  }
}
