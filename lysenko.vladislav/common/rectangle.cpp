#include <cmath>
#include <stdexcept>
#include "rectangle.hpp"

using namespace lysenko;

Rectangle::Rectangle(const point_t &center, const double width, const double height):
  center_(center),
  width_(width),
  height_(height),
  angle_(0.0)
{
  if ((width < 0.0) || (height < 0.0))
  {
    throw::std::invalid_argument("Error! Width and heigth must be >= 0.0");
  }
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  const double sint = sin(angle_ * M_PI / 180);
  const double cost = cos(angle_ * M_PI / 180);
  const double width = fabs(width_ * cost) + fabs(height_ * sint);
  const double height =  fabs(height_ * cost) + fabs(width_ * sint);
  return {center_, width, height};
}


void Rectangle::move(const point_t & center)
{
  center_ = center;
}

void Rectangle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Rectangle::scale(const double ScaleCoeff)
{
  if (ScaleCoeff < 0.0)
  {
    throw std::invalid_argument("Error! ScaleCoeff must be >= 0.0");
  }
  width_ *= ScaleCoeff;
  height_ *= ScaleCoeff;
}

void Rectangle::rotate(const double angle)
{
  angle_ += angle;
  if (angle_ > 360.0)
  {
    angle_ = fmod(angle_, 360.0);
  }
}
