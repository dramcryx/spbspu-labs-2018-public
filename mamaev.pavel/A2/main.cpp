#include "circle.hpp"
#include "rectangle.hpp"

#include <iostream>

using namespace mamaev;

int main()
{
  try
  {
    double coeff;

    Rectangle rect(5, 10, {0, 0});  
    std::cout << "Area of circle before scaling: " << rect.getArea() << std::endl;
    coeff = 4.5;
    rect.scale(coeff);
    std::cout << "Area of circle after scaling: " << rect.getArea() << std::endl
              << "Coefficient: " << coeff << std::endl << std::endl;
    
    Circle circ(3, {1.6, 0.2});
    std::cout << "Area of circle before scaling: " << circ.getArea() << std::endl;
    coeff = 10;
    circ.scale(coeff);
    std::cout << "Area of circle after scaling: " << circ.getArea() << std::endl
              << "Coefficient: " << coeff << std::endl;
  }
  catch(std::invalid_argument & ia)
  {
    std::cerr << ia.what() << std::endl;
    return 1;
  }
  return 0;
}
