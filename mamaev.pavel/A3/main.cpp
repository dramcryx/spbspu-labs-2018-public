#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

#include <iostream>
#include <memory>

using namespace mamaev;

void printShapeInfo(const Shape & shape)
{
  std::cout << "Area = " <<shape.getArea() <<std::endl;
  const rectangle_t frame = shape.getFrameRect();
  std::cout << "FrameRectangle: " <<std::endl;
  std::cout << " Pos: (" <<frame.pos.x << ", " << frame.pos.y << ")" << std::endl;
  std::cout << " width = " << frame.width << std::endl;
  std::cout << " Height = " << frame.height << std::endl << std::endl;
}
 
int main()
{
  try
  {
    std::cout << "Creating the Rectangle(5, 10, {0, 0})..." <<std::endl;
    Rectangle rect(5, 10, {0, 0});  
    std::cout << "Rectangle info: " << std::endl;
    printShapeInfo(rect);
    
    std::cout << "Creating the Circle(3, {1.6, 0.2})..." << std::endl;
    Circle circ(3, {1.6, 0.2});
    std::cout << "Circle info: " << std::endl;
    printShapeInfo(circ);
    
    std::cout << "Creating the CompositeShape(Circle)..." <<std::endl;
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle>(circ);
    CompositeShape compShp(circPtr);
    std::cout << "CompositeShape size: " << compShp.getSize() << std::endl << std::endl;
    
    std::cout << "Adding the Rectangle to the CompositeShape..." << std::endl;
    std::shared_ptr <Shape> rectPtr = std::make_shared <Rectangle>(rect);
    compShp.addShape(rectPtr);
    std::cout << "CompositeShape size: " << compShp.getSize() << std::endl <<std::endl;
    
    std::cout << "CompositeShape pos: {" << compShp.getPos().x
      << ", " << compShp.getPos().y << "}" << std::endl
      << "Moving the CompositeShape..." << std::endl;
    compShp.move(5.0, 10.0);
    std::cout << "CompositeShape pos: {" << compShp.getPos().x
      << ", " << compShp.getPos().y << "}" << std::endl << std::endl;
	
    std::cout << "Scaling the CompositeShape..." << std::endl;
    compShp.scale(2.0);
    std::cout << "CompositeShape info: " << std::endl;
    printShapeInfo(compShp);
	
    std::cout << "Removing the Circle from the CompositeShape..." << std::endl;
    compShp.removeShape(1);
    std::cout << "CompositeShape size: " << compShp.getSize() <<std::endl << std::endl;
	
    std::cout << "Clearing the CompositeShape..." << std::endl;
    compShp.clear();
    std::cout << "CompositeShape size: " << compShp.getSize() << std::endl;
  }
  catch(std::invalid_argument & ia)
  {
    std::cerr << ia.what() << std::endl;
    return 1;
  }
  return 0;
}
