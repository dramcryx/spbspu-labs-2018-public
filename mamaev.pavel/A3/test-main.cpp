#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

using namespace mamaev;

const double EPS = 0.00001;

BOOST_AUTO_TEST_SUITE(CompositeShape_Test)
  BOOST_AUTO_TEST_CASE(Constructor_Copy)
  {
  Circle circ1(3.33, {1, 3});
  Rectangle rect1(1, 2, {3, 4});
  Circle circ2(9, {8, 7});
  CompositeShape compShp1(std::make_shared <Circle>(circ1));
  compShp1.addShape(std::make_shared <Rectangle>(rect1));
  compShp1.addShape(std::make_shared <Circle>(circ2));
    
  CompositeShape compShp2(compShp1);
  for (unsigned int i = 0; i < compShp1.getSize(); ++i)
    {
    rectangle_t frame1 = compShp1.getShape(i)->getFrameRect();
    rectangle_t frame2 = compShp2.getShape(i)->getFrameRect();
    BOOST_CHECK_CLOSE_FRACTION(frame1.height, frame2.height, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame1.width, frame2.width, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame1.pos.x, frame2.pos.x, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame1.pos.y, frame2.pos.y, EPS);
    }
  }
  
  BOOST_AUTO_TEST_CASE(Constructor_Move)
  {
    Circle circ1(3.33, {1, 3});
    Rectangle rect1(1, 2, {3, 4});
    Circle circ2(9, {8, 7});
    CompositeShape compShp0(std::make_shared <Circle>(circ1));
    compShp0.addShape(std::make_shared <Rectangle>(rect1));
    compShp0.addShape(std::make_shared <Circle>(circ2));
    CompositeShape compShp1(compShp0);
    
    CompositeShape compShp2(std::move(compShp1));
    for (unsigned int i = 0; i < compShp1.getSize(); ++i)
    {
      rectangle_t frame0 = compShp0.getShape(i)->getFrameRect();
      rectangle_t frame2 = compShp2.getShape(i)->getFrameRect();
      BOOST_CHECK_CLOSE_FRACTION(frame0.height, frame2.height, EPS);
      BOOST_CHECK_CLOSE_FRACTION(frame0.width, frame2.width, EPS);
      BOOST_CHECK_CLOSE_FRACTION(frame0.pos.x, frame2.pos.x, EPS);
      BOOST_CHECK_CLOSE_FRACTION(frame0.pos.y, frame2.pos.y, EPS);
    }
  }
  
  BOOST_AUTO_TEST_CASE(Operator_Copy)
  {
    Circle circ1(3.33, {1, 3});
    Rectangle rect1(1, 2, {3, 4});
    Circle circ2(9, {8, 7});
    CompositeShape compShp1(std::make_shared <Circle>(circ1));
    compShp1.addShape(std::make_shared <Rectangle>(rect1));
    CompositeShape compShp2(std::make_shared <Circle>(circ2));
    
    compShp2 = compShp1;
    for (unsigned int i = 0; i < compShp1.getSize(); ++i)
    {
      rectangle_t frame1 = compShp1.getShape(i)->getFrameRect();
      rectangle_t frame2 = compShp2.getShape(i)->getFrameRect();
      BOOST_CHECK_CLOSE_FRACTION(frame1.height, frame2.height, EPS);
      BOOST_CHECK_CLOSE_FRACTION(frame1.width, frame2.width, EPS);
      BOOST_CHECK_CLOSE_FRACTION(frame1.pos.x, frame2.pos.x, EPS);
      BOOST_CHECK_CLOSE_FRACTION(frame1.pos.y, frame2.pos.y, EPS);
    }
  }  
    
  BOOST_AUTO_TEST_CASE(Operator_Move)
  {
    Circle circ1(3.33, {1, 3});
    Rectangle rect1(1, 2, {3, 4});
    Circle circ2(9, {8, 7});
    CompositeShape compShp0(std::make_shared <Circle>(circ1));
    compShp0.addShape(std::make_shared <Rectangle>(rect1));
    CompositeShape compShp1(std::make_shared <Circle>(circ2));
    compShp1(compShp0);
    
    CompositeShape compShp2 = std::move(compShp1);
    for (unsigned int i = 0; i < compShp1.getSize(); ++i)
    {
      rectangle_t frame1 = compShp1.getShape(i)->getFrameRect();
      rectangle_t frame2 = compShp2.getShape(i)->getFrameRect();
      BOOST_CHECK_CLOSE_FRACTION(frame1.height, frame2.height, EPS);
      BOOST_CHECK_CLOSE_FRACTION(frame1.width, frame2.width, EPS);
      BOOST_CHECK_CLOSE_FRACTION(frame1.pos.x, frame2.pos.x, EPS);
      BOOST_CHECK_CLOSE_FRACTION(frame1.pos.y, frame2.pos.y, EPS);
	}
  }
    
  BOOST_AUTO_TEST_CASE(Get_Area)
  {
    Circle circ1(3.33, {1, 3});
    Rectangle rect1(1, 2, {3, 4});
    Circle circ2(9, {8, 7});
    CompositeShape compShp(std::make_shared <Circle>(circ1));
    compShp.addShape(std::make_shared <Rectangle>(rect1));
    compShp.addShape(std::make_shared <Circle>(circ2));
    
    BOOST_CHECK_CLOSE_FRACTION(compShp.getArea(), (circ1.getArea() + circ2.getArea() + rect1.getArea()), EPS);
  }  
    
  BOOST_AUTO_TEST_CASE(Get_Frame_Rectangle_And_Get_Pos)
  {
    Circle circ1(3.33, {1, 3});
    Rectangle rect1(1, 2, {3, 4});
    Circle circ2(9, {8, 7});
    CompositeShape compShp(std::make_shared <Circle>(circ1));
    compShp.addShape(std::make_shared <Rectangle>(rect1));
    compShp.addShape(std::make_shared <Circle>(circ2));    
    
    rectangle_t frame = compShp.getFrameRect();
    BOOST_CHECK_CLOSE_FRACTION(frame.pos.x, 7.335, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame.pos.y, 7, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame.width, 19.33, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame.height, 18, EPS);
    
    point_t pos = compShp.getPos();
    BOOST_CHECK_CLOSE_FRACTION(pos.x, 7.335, EPS);
    BOOST_CHECK_CLOSE_FRACTION(pos.y, 7, EPS);
  }
  
  BOOST_AUTO_TEST_CASE(Delta_And_Absolute_Move)
  {
    Circle circ1(3.33, {1, 3});
    Rectangle rect1(1, 2, {3, 4});
    Circle circ2(9, {8, 7});
    CompositeShape compShp(std::make_shared <Circle>(circ1));
    compShp.addShape(std::make_shared <Rectangle>(rect1));
    compShp.addShape(std::make_shared <Circle>(circ2));    
    
    rectangle_t frame1 = compShp.getFrameRect();
    compShp.move(5, 10);
    rectangle_t frame2 = compShp.getFrameRect();
    BOOST_CHECK_CLOSE_FRACTION((frame1.pos.x + 5), (frame2.pos.x), EPS);
    BOOST_CHECK_CLOSE_FRACTION((frame1.pos.y + 10), (frame2.pos.y), EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame1.width, frame2.width, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame1.height, frame2.height, EPS);
    
    compShp.move({5, 10});
    rectangle_t frame3 = compShp.getFrameRect();
    BOOST_CHECK_CLOSE_FRACTION(frame3.pos.x, 5, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame3.pos.y, 10, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame1.width, frame3.width, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame1.height, frame3.height, EPS);
  }
  
  BOOST_AUTO_TEST_CASE(Scaling)
  {
    Circle circ1(3.33, {1, 3});
    Rectangle rect1(1, 2, {3, 4});
    Circle circ2(9, {8, 7});
    CompositeShape compShp(std::make_shared <Circle>(circ1));
    compShp.addShape(std::make_shared <Rectangle>(rect1));
    compShp.addShape(std::make_shared <Circle>(circ2));
    
    rectangle_t frame1 = compShp.getFrameRect();
    double area1 = compShp.getArea();
    compShp.scale(2);
    rectangle_t frame2 = compShp.getFrameRect();
    double area2 = compShp.getArea();
    BOOST_CHECK_CLOSE_FRACTION(frame1.pos.x, frame2.pos.x, EPS);
    BOOST_CHECK_CLOSE_FRACTION(frame1.pos.y, frame2.pos.y, EPS);
    BOOST_CHECK_CLOSE_FRACTION((frame1.width * 2), frame2.width, EPS);
    BOOST_CHECK_CLOSE_FRACTION((frame1.height * 2), frame2.height, EPS);
    BOOST_CHECK_CLOSE_FRACTION((area1 * 2 * 2), area2, EPS);
  }  
    
  BOOST_AUTO_TEST_CASE(Removing_Shape)
  {
    Circle circ1(3.33, {1, 3});
    Rectangle rect1(1, 2, {3, 4});
    Circle circ2(9, {8, 7});
    CompositeShape compShp(std::make_shared <Circle>(circ1));
    compShp.addShape(std::make_shared <Rectangle>(rect1));
    compShp.addShape(std::make_shared <Circle>(circ2));
    
    unsigned int size1 = compShp.getSize();
    double area1 = compShp.getArea();
    double circ2Area = circ2.getArea();
    compShp.removeShape(3);
    unsigned int size2 = compShp.getSize();
    double area2 = compShp.getArea();
    BOOST_CHECK_EQUAL(size1, (size2 + 1));
    BOOST_CHECK_CLOSE_FRACTION(area1, (area2 + circ2Area), EPS);
  }
    
  BOOST_AUTO_TEST_CASE(Clearing)
  {
    Circle circ1(3.33, {1, 3});
    Rectangle rect1(1, 2, {3, 4});
    Circle circ2(9, {8, 7});
    CompositeShape compShp(std::make_shared <Circle>(circ1));
    compShp.addShape(std::make_shared <Rectangle>(rect1));
    compShp.addShape(std::make_shared <Circle>(circ2));
    
    compShp.clear();
    BOOST_CHECK_EQUAL(compShp.getSize(), 0);
    BOOST_CHECK_CLOSE_FRACTION(compShp.getArea(), 0, EPS);
  }
   
  BOOST_AUTO_TEST_CASE(Invalid_Argument)
  {
    BOOST_CHECK_THROW(CompositeShape(nullptr), std::invalid_argument);
    
    Circle circ(3.33, {1, 3});
    std::shared_ptr <Shape> circPtr = std::make_shared <Circle>(circ);
    CompositeShape compShp(circPtr);
    BOOST_CHECK_THROW(compShp.scale(-1), std::invalid_argument);
    
    BOOST_CHECK_THROW(compShp.addShape(nullptr), std::invalid_argument);
    BOOST_CHECK_THROW(compShp.addShape(circPtr), std::invalid_argument);
    
    BOOST_CHECK_THROW(compShp.removeShape(2), std::invalid_argument);
    compShp.clear();
    BOOST_CHECK_THROW(compShp.removeShape(2), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()
