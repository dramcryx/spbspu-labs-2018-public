#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace mamaev
{
  class Circle: public Shape
  {
  public:
    Circle(const double radius_, const point_t & pos);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    point_t getPos() const override;
    void move(const point_t & pos) override;
    void move(const double dx, const double dy) override;
    void scale(const double coefficient) override;

    double getRadius() const;
  private:
    point_t pos_;
    double radius_;
  };
}

#endif
