#include <stdexcept>
#include <memory>
#include <cmath>

#include "composite-shape.hpp"

mamaev::CompositeShape::CompositeShape(std::shared_ptr <mamaev::Shape> shape):
  list_(nullptr),
  size_(0)
{
  if (!shape)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  
  addShape(shape);
}

mamaev::CompositeShape::CompositeShape(const mamaev::CompositeShape & shapeCopy):
  size_(shapeCopy.size_)
{
  std::unique_ptr<std::shared_ptr<Shape>[]> CopyList (new std::shared_ptr<Shape>[size_]);
    for (unsigned int i = 0; i < size_; ++i)
    {
      CopyList[i] = shapeCopy.list_[i];
    }
    list_.swap(CopyList);
}

mamaev::CompositeShape::CompositeShape(mamaev::CompositeShape && shapeMove):
  list_(std::move(shapeMove.list_)),
  size_(shapeMove.size_)
{
  shapeMove.list_.reset();
  shapeMove.size_ = 0;
}

mamaev::CompositeShape & mamaev::CompositeShape::operator=(const mamaev::CompositeShape & shapeCopy)
{
  if (this == & shapeCopy)
  {
    return *this;
  }
  size_ = shapeCopy.size_;
  std::unique_ptr<std::shared_ptr<mamaev::Shape>[]> new_list_(new std::shared_ptr<mamaev::Shape>[size_]);
  for (unsigned int i = 0; i < size_; ++i)
  {
    new_list_[i] = shapeCopy.list_[i];
  }
  list_.swap(new_list_);
  
  return *this;
}

mamaev::CompositeShape & mamaev::CompositeShape::operator=(mamaev::CompositeShape && shapeMove)
{
  if (this != & shapeMove)
  {
    list_.swap(shapeMove.list_);
    size_=shapeMove.size_;
    shapeMove.list_.reset();
    shapeMove.size_ = 0;
  }
  return *this;
}

double mamaev:: CompositeShape::getArea() const
{
  double area = 0;
  for (unsigned int i = 0; i < size_; ++i)
  {
    area += list_[i]->getArea();
  }
  return area;
}

mamaev::rectangle_t mamaev::CompositeShape::getFrameRect() const
{
  double width;
  double height;
  mamaev::point_t pos;
  if (size_ != 0)
  {
    mamaev::rectangle_t frameRect = list_[0]->getFrameRect();
    double left = frameRect.pos.x - (frameRect.width / 2);
    double right = frameRect.pos.x + (frameRect.width / 2);
    double bottom = frameRect.pos.y - (frameRect.height / 2);
    double top = frameRect.pos.y + (frameRect.height / 2);
    for (unsigned int i = 1; i < size_; ++i)
    {
    frameRect = list_[i]->getFrameRect();
    double left_i = frameRect.pos.x - (frameRect.width / 2);
    double right_i = frameRect.pos.x + (frameRect.width / 2);
    double bottom_i = frameRect.pos.y - (frameRect.height / 2);
    double top_i = frameRect.pos.y + (frameRect.height / 2);
    
    if (left_i < left)
    {
      left = left_i;
    }
    if (right_i > right)
    {
      right = right_i;
    }
    if (bottom_i < bottom)
    {
      bottom = bottom_i;
    }
    if (top_i > top)
    {
      top = top_i;
    }
  }
  width = right - left;
  height = top - bottom;
  pos = {((left + right) / 2), ((top + bottom) / 2)};
  }
  else
  {
    width = 0;
    height = 0;
    pos = {0, 0};
  }
  return{width, height, pos};
}

mamaev::point_t mamaev::CompositeShape::getPos() const
{
  double x = getFrameRect().pos.x;
  double y = getFrameRect().pos.y;
  return{x, y};
}

void mamaev::CompositeShape::move(const double dx, const double dy)
{
  for (unsigned int i = 0; i < size_; ++i)
  {
    list_[i]->move(dx, dy);
  }
}

void mamaev::CompositeShape::move(const mamaev::point_t & Pos)
{
  const mamaev::point_t curPos = getPos();
  double dx = (Pos.x - curPos.x);
  double dy = (Pos.y - curPos.y);
  move(dx, dy);
}

void mamaev::CompositeShape::scale(const double coefficient)
{
  if (coefficient < 0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  
  const mamaev::point_t curPos = getPos();
  for (unsigned int i = 0; i < size_; ++i)
  {
    const mamaev::point_t shapePos = list_[i]->getPos();
    list_[i]->move((coefficient - 1) * (shapePos.x - curPos.x), (coefficient - 1) * (shapePos.y - curPos.y));
    list_[i]->scale(coefficient);
  }
}

void mamaev::CompositeShape::addShape(const std::shared_ptr <mamaev::Shape> newShape)
{
  if (!newShape)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  
  for (unsigned int i = 0; i < size_; ++i)
  {
    if (newShape == list_[i])
    {
    throw std::invalid_argument("This shape is already a part of this CompositeShape");
    }
  }
  
  std::unique_ptr <std::shared_ptr <mamaev::Shape>[]> newList (new std::shared_ptr <mamaev::Shape>[size_ + 1]);
  for (unsigned int i = 0; i < size_; ++i)
  {
    newList[i] = list_[i];
  }
  newList[size_++] = newShape;
  list_.swap(newList);
}

std::shared_ptr <mamaev::Shape> mamaev::CompositeShape::getShape(unsigned int const index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Invalid index");
  }
  return list_[index];
}

void mamaev::CompositeShape::removeShape(const unsigned int index)
{
  if (size_ ==0)
  {
    throw std::invalid_argument("Empty CompositeShape");
  }
  else
  {
    if ((index <= 0) || (index > size_))
    {
    throw std::invalid_argument("Invalid index");
    }
  }
  
  if (size_== 1)
  {
    clear();
  }
  else
  {
  std::unique_ptr <std::shared_ptr <mamaev::Shape>[]> newParts(new std::shared_ptr <mamaev::Shape>[size_ - 1]);
  for (unsigned int i = 0; i < (index - 1); ++i)
    {
    newParts[i] = list_[i];
    }
  for (unsigned int i = index; i < size_; ++i)
    {
    newParts[i - 1] = list_[i];
    }
  list_.swap(newParts);
  --size_;
  }
}

void mamaev::CompositeShape::clear()
{
  list_.reset();
  list_ = nullptr;
  size_ = 0;
}

unsigned int mamaev::CompositeShape::getSize() const
{
  return size_;
}
