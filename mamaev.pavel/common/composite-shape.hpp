#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace mamaev
{
  class CompositeShape:
    public Shape
  {
  public:
    CompositeShape(const std::shared_ptr <Shape> shape);
    CompositeShape(const CompositeShape & shapeCopy);
    CompositeShape(CompositeShape && shapeMove);
    
    CompositeShape & operator=(const CompositeShape & shapeCopy);
    CompositeShape & operator=(CompositeShape && shapeMove);
    
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    point_t getPos() const override;
    void move(const double dx, const double dy) override;
    void move(const point_t & Pos) override;
    void scale(const double coefficient) override;
    
    void addShape(std::shared_ptr <Shape> shape);
    std::shared_ptr <Shape> getShape(unsigned int const index) const;
    void removeShape(const unsigned int index);
    void clear();
    unsigned int getSize() const;
  private:
    std::unique_ptr <std::shared_ptr <Shape>[]> list_;
    unsigned int size_;
  };
}

#endif
 
