#include "rectangle.hpp"

#include <stdexcept>
#include <cmath>
#include <iostream>

mamaev::Rectangle::Rectangle(const double width, const double height, const mamaev::point_t & pos):
  pos_(pos),
  width_(width),
  height_(height)
{
  if ((width_ < 0.0) || (height_ < 0.0))
  {
    throw std::invalid_argument("Invalid width or/and height");
  }
}

double mamaev::Rectangle::getArea() const
{
  return width_ * height_;
}

mamaev::rectangle_t mamaev::Rectangle::getFrameRect() const
{
  return {width_, height_, pos_};
}

mamaev::point_t mamaev::Rectangle::getPos() const
{
  return pos_;
}

void mamaev::Rectangle::move(const point_t & pos)
{
  pos_ = pos;
}

void mamaev::Rectangle::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;
}

void mamaev::Rectangle::scale(const double coefficient)
{
  if (coefficient < 0.0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  width_ *= coefficient;
  height_ *= coefficient;
}

double mamaev::Rectangle::getWidth() const
{
  return width_;
}

double mamaev::Rectangle::getHeight() const
{
  return height_;
}
