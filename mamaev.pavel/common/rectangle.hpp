#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace mamaev
{
 class Rectangle : public Shape
 {
 public:
  Rectangle(const double width, const double height, const point_t & pos);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  point_t getPos() const override;
  void move(const point_t & pos) override;
  void move(const double dx, const double dy) override;
  void scale(const double coefficient) override;

  double getHeight() const;
  double getWidth() const;
 private:
  point_t pos_;
  double width_;
  double height_;
 };
}

#endif
