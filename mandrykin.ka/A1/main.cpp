#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "base-types.hpp"


int main()
{
  Rectangle r({ 24, 11 }, 2, 7);
  r.move(6, 13);
  r.move({ 31.54, 32.21 });
  r.getFrameRect();
  r.getArea();

  Circle c({ 15, 11 }, 5);
  c.move({ 14.25, 7 });
  c.move(6.3, 26.3);
  c.getFrameRect();
  c.getArea();

  Triangle t({ 20, 22 }, { 25, 19 }, { 19, 15 });
  t.move(0.6, 3);
  t.move({ 23, 101 });
  t.getFrameRect();
  t.getArea();

  return 0;
}
