#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

int main()
{
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(4.0, 2.0, {3.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Circle({9.0, 1.0}, 3.0));
  std::shared_ptr< marchenko::Shape > ptr3(new marchenko::Circle({8.0, 7.5}, 105.0));
  marchenko::Matrix testMatrix(ptr1);
  testMatrix.add(ptr2);
  std::cout<<"This matrix have rectangle and circle. "<< std::endl;
  std::cout<<"In this matrix there are "<<testMatrix.getLayersNum()<<" layers"<< std::endl;
  testMatrix.add(ptr3);
  std::cout<<"New circle is added to matrix: "<< std::endl;
  std::cout<<"In this matrix there are "<<testMatrix.getLayersNum()<<" layers"<< std::endl;
  return 0;
}
