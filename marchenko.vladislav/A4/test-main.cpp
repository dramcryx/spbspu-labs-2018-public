#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(testingMatrix)

BOOST_AUTO_TEST_CASE(Matrix_Test)
{
  marchenko::Circle testCircle( { -2.0, -2.0 }, 2.0 );
  marchenko::Rectangle testRect1(2.0, 2.0, { -2.0, 0.0 });
  marchenko::Rectangle testRect2(2.0, 4.0, { 3.0, 1.0 });
  std::shared_ptr<marchenko::Shape> circlePtr = std::make_shared<marchenko::Circle>(testCircle);
  std::shared_ptr<marchenko::Shape> rectanglePtr1 = std::make_shared<marchenko::Rectangle>(testRect1);
  std::shared_ptr<marchenko::Shape> rectanglePtr2 = std::make_shared<marchenko::Rectangle>(testRect2);
  marchenko::Matrix matrix(circlePtr);
  matrix.add(rectanglePtr1);
  matrix.add(rectanglePtr2);
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> layer1 = matrix[0];
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> layer2 = matrix[1];
  BOOST_CHECK(layer1[0] == circlePtr);
  BOOST_CHECK(layer1[1] == rectanglePtr2);
  BOOST_CHECK(layer2[0] == rectanglePtr1);
}

BOOST_AUTO_TEST_CASE(CheckLayers)
{
  std::shared_ptr<marchenko::Shape> testCircle1 =
    std::make_shared<marchenko::Circle>(marchenko::Circle({3.0, 0.0}, 3.0));
  std::shared_ptr<marchenko::Shape> testCircle2 =
    std::make_shared<marchenko::Circle>(marchenko::Circle({-5.0, -5.0}, 5.0));
  std::shared_ptr<marchenko::Shape> testRect1 =
    std::make_shared<marchenko::Rectangle>(marchenko::Rectangle(8.0, 4.0, {0.0, 4.0}));
  std::shared_ptr<marchenko::Shape> testRect2 =
    std::make_shared<marchenko::Rectangle>(marchenko::Rectangle(6.0, 6.0, {-6.0, 1.0}));
  std::shared_ptr<marchenko::Shape> testRect3 =
    std::make_shared<marchenko::Rectangle>(marchenko::Rectangle(2.0, 2.0, {0.0, 0.0}));
  marchenko::Matrix matrix(testCircle1);
  matrix.add(testCircle2);
  matrix.add(testRect1);
  matrix.add(testRect2);
  matrix.add(testRect3);
  size_t quant = 4;
  BOOST_CHECK_EQUAL(quant, matrix.getLayersNum());
}

BOOST_AUTO_TEST_CASE(CompToMatrix)
{
  marchenko::CompositeShape testComp;
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(2.0, 3.0, {1.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Rectangle(1.0, 1.0, {2.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr3(new marchenko::Circle({10.0, 10.0}, 1.0));
  testComp.addShape(ptr1);
  testComp.addShape(ptr2);
  testComp.addShape(ptr3);
  marchenko::Matrix testMatrix;
  testMatrix.add(testComp);
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> layer1 = testMatrix[0];
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> layer2 = testMatrix[1];
  BOOST_CHECK(layer1[0] == ptr1);
  BOOST_CHECK(layer1[1] == ptr3);
  BOOST_CHECK(layer2[0] == ptr2);  
}

BOOST_AUTO_TEST_CASE(copyConstructor)
{
  marchenko::Matrix testMatrix;
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(2.0, 3.0, {1.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Rectangle(1.0, 1.0, {2.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr3(new marchenko::Circle({10.0, 10.0}, 1.0));     
  testMatrix.add(ptr1);
  testMatrix.add(ptr2);
  testMatrix.add(ptr3);
  marchenko::Matrix newMatrix(testMatrix);
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> scdLayer1 = newMatrix[0];
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> scdLayer2 = newMatrix[1];
  BOOST_CHECK(scdLayer1[0] == ptr1);
  BOOST_CHECK(scdLayer1[1] == ptr3);
  BOOST_CHECK(scdLayer2[0] == ptr2);
}

BOOST_AUTO_TEST_CASE(moveConstructor)
{
  marchenko::Matrix testMatrix;
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(2.0, 3.0, {1.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Rectangle(1.0, 1.0, {2.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr3(new marchenko::Circle({10.0, 10.0}, 1.0));     
  testMatrix.add(ptr1);
  testMatrix.add(ptr2);
  testMatrix.add(ptr3);
  marchenko::Matrix newMatrix(std::move(testMatrix));
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> scdLayer1 = newMatrix[0];
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> scdLayer2 = newMatrix[1];
  BOOST_CHECK(scdLayer1[0] == ptr1);
  BOOST_CHECK(scdLayer1[1] == ptr3);
  BOOST_CHECK(scdLayer2[0] == ptr2);  
}

BOOST_AUTO_TEST_CASE(copyOperator)
{
  marchenko::Matrix testMatrix;
  marchenko::Matrix newMatrix;
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(2.0, 3.0, {1.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Rectangle(1.0, 1.0, {2.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr3(new marchenko::Circle({10.0, 10.0}, 1.0));  
  std::shared_ptr< marchenko::Shape > ptr4(new marchenko::Rectangle(2.0, 3.0, {2.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr5(new marchenko::Rectangle(1.0, 1.0, {3.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr6(new marchenko::Circle({100.0, 100.0}, 2.0));  
  testMatrix.add(ptr1);
  testMatrix.add(ptr2);
  testMatrix.add(ptr3);
  newMatrix.add(ptr4);
  newMatrix.add(ptr5);
  newMatrix.add(ptr6);
  newMatrix = testMatrix;
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> scdLayer1 = newMatrix[0];
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> scdLayer2 = newMatrix[1];  
  BOOST_CHECK(scdLayer1[0] == ptr1);
  BOOST_CHECK(scdLayer1[1] == ptr3);
  BOOST_CHECK(scdLayer2[0] == ptr2);  
}

BOOST_AUTO_TEST_CASE(moveOperator)
{
  marchenko::Matrix testMatrix;
  marchenko::Matrix newMatrix;
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(2.0, 3.0, {1.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Rectangle(1.0, 1.0, {2.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr3(new marchenko::Circle({10.0, 10.0}, 1.0));  
  std::shared_ptr< marchenko::Shape > ptr4(new marchenko::Rectangle(2.0, 3.0, {2.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr5(new marchenko::Rectangle(1.0, 1.0, {3.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr6(new marchenko::Circle({100.0, 100.0}, 2.0));  
  testMatrix.add(ptr1);
  testMatrix.add(ptr2);
  testMatrix.add(ptr3);
  newMatrix.add(ptr4);
  newMatrix.add(ptr5);
  newMatrix.add(ptr6);
  newMatrix = std::move(testMatrix);
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> scdLayer1 = newMatrix[0];
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> scdLayer2 = newMatrix[1];  
  BOOST_CHECK(scdLayer1[0] == ptr1);
  BOOST_CHECK(scdLayer1[1] == ptr3);
  BOOST_CHECK(scdLayer2[0] == ptr2); 
}

BOOST_AUTO_TEST_CASE(equalityOperator)
{
  marchenko::Matrix testMatrix;
  marchenko::Matrix newMatrix;
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(2.0, 3.0, {1.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Rectangle(1.0, 1.0, {2.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr3(new marchenko::Circle({10.0, 10.0}, 1.0));
  testMatrix.add(ptr1);
  testMatrix.add(ptr2);
  testMatrix.add(ptr3);
  newMatrix.add(ptr1);
  newMatrix.add(ptr2);
  newMatrix.add(ptr3);
  BOOST_CHECK_EQUAL(newMatrix == testMatrix , true);
}

BOOST_AUTO_TEST_CASE(unequalityOperator)
{
  marchenko::Matrix testMatrix;
  marchenko::Matrix newMatrix;
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(2.0, 3.0, {1.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Rectangle(1.0, 1.0, {2.0, 1.0}));
  std::shared_ptr< marchenko::Shape > ptr3(new marchenko::Circle({10.0, 10.0}, 1.0));
  testMatrix.add(ptr1);
  testMatrix.add(ptr2);
  newMatrix.add(ptr1);
  newMatrix.add(ptr2);
  newMatrix.add(ptr3);
  BOOST_CHECK_EQUAL(newMatrix != testMatrix , true);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(testRectangle)

BOOST_AUTO_TEST_CASE(moveByDxAndDy)
{
  marchenko::Rectangle testRect(5.0, 3.0, {1.0, 1.0});
  marchenko::point_t testVert[4];
  for ( size_t i = 0; i < 4; i++)
  {
    testVert[i] = testRect.getVertice(i);
    testVert[i].x += 2.0;
    testVert[i].y += 3.0;
  }
  testRect.move(2.0,3.0);
  BOOST_CHECK_EQUAL(testVert[0].x, testRect.getVertice(0).x);
  BOOST_CHECK_EQUAL(testVert[1].x, testRect.getVertice(1).x);
  BOOST_CHECK_EQUAL(testVert[2].x, testRect.getVertice(2).x);
  BOOST_CHECK_EQUAL(testVert[3].x, testRect.getVertice(3).x);
  BOOST_CHECK_EQUAL(testVert[0].y, testRect.getVertice(0).y);
  BOOST_CHECK_EQUAL(testVert[1].y, testRect.getVertice(1).y);
  BOOST_CHECK_EQUAL(testVert[2].y, testRect.getVertice(2).y);
  BOOST_CHECK_EQUAL(testVert[3].y, testRect.getVertice(3).y);
}

BOOST_AUTO_TEST_CASE(moveToNewPosition)
{
  marchenko::Rectangle testRect(5.0, 3.0, {1.0, 1.0});
  marchenko::point_t newPos = {2.0,2.0};
  testRect.move(newPos);
  BOOST_CHECK_EQUAL(-0.5, testRect.getVertice(0).x);
  BOOST_CHECK_EQUAL(-0.5, testRect.getVertice(1).x);
  BOOST_CHECK_EQUAL(4.5, testRect.getVertice(2).x);
  BOOST_CHECK_EQUAL(4.5, testRect.getVertice(3).x);
  BOOST_CHECK_EQUAL(0.5, testRect.getVertice(0).y);
  BOOST_CHECK_EQUAL(3.5, testRect.getVertice(1).y);
  BOOST_CHECK_EQUAL(3.5, testRect.getVertice(2).y);
  BOOST_CHECK_EQUAL(0.5, testRect.getVertice(3).y);
}

BOOST_AUTO_TEST_CASE(scaling)
{
  marchenko::Rectangle testRect(4.0, 2.0, {1.0, 1.0});
  testRect.scale(2.0);
  BOOST_CHECK_EQUAL(-3.0, testRect.getVertice(0).x);
  BOOST_CHECK_EQUAL(-3.0, testRect.getVertice(1).x);
  BOOST_CHECK_EQUAL(5.0, testRect.getVertice(2).x);
  BOOST_CHECK_EQUAL(5.0, testRect.getVertice(3).x);
  BOOST_CHECK_EQUAL(-1.0, testRect.getVertice(0).y);
  BOOST_CHECK_EQUAL(3.0, testRect.getVertice(1).y);
  BOOST_CHECK_EQUAL(3.0, testRect.getVertice(2).y);
  BOOST_CHECK_EQUAL(-1.0, testRect.getVertice(3).y);
}

BOOST_AUTO_TEST_CASE(vertAfterRotate)
{
  marchenko::Rectangle testRect(4.0, 6.0, {1.0, 1.0});
  testRect.rotate(60);
  BOOST_CHECK_CLOSE(testRect.getVertice(0).x, 2.59807621, EPSILON);
  BOOST_CHECK_CLOSE(testRect.getVertice(1).x, -2.59807621, EPSILON);
  BOOST_CHECK_CLOSE(testRect.getVertice(2).x, -0.59807621, EPSILON);
  BOOST_CHECK_CLOSE(testRect.getVertice(3).x,4.59807621, EPSILON);
  BOOST_CHECK_CLOSE(testRect.getVertice(0).y, 0.88397459, EPSILON);
  BOOST_CHECK_CLOSE(testRect.getVertice(1).y, -0.61602540 , EPSILON);
  BOOST_CHECK_CLOSE(testRect.getVertice(2).y,1.116025403, EPSILON);
  BOOST_CHECK_CLOSE(testRect.getVertice(3).y, 2.6160 , EPSILON);
}

BOOST_AUTO_TEST_CASE(frameRectAfterRotate)
{
  marchenko::Rectangle testRect(6.0, 4.0, {1.0, 1.0});
  testRect.rotate(60);
  BOOST_CHECK_CLOSE(testRect.getFrameRect().pos.x, 1.0 , EPSILON);
  BOOST_CHECK_CLOSE(testRect.getFrameRect().pos.y, 1.0 , EPSILON);
  BOOST_CHECK_CLOSE(testRect.getFrameRect().width,3.59807621, EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(testsOfCompShape)

BOOST_AUTO_TEST_CASE(invalidConstructor)
{
  std::shared_ptr<marchenko::Shape> testRect = nullptr;
  BOOST_CHECK_THROW(marchenko::CompositeShape testComp(testRect), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(copyConstructor)
{
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(5.0, 15.0, {35.0, 35.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Circle({3.0, 2.5}, 105));
  marchenko::CompositeShape testComp;
  testComp.addShape(ptr1);
  testComp.addShape(ptr2);
  marchenko::CompositeShape newComp(testComp);
  BOOST_CHECK_EQUAL(testComp[0], newComp[0]);
  BOOST_CHECK_EQUAL(testComp[1], newComp[1]);
}

BOOST_AUTO_TEST_CASE(moveConstructor)
{
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(5.0, 15.0, {35.0, 35.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Circle({3.0, 2.5}, 105));
  marchenko::CompositeShape testComp;
  testComp.addShape(ptr1);
  testComp.addShape(ptr2);
  marchenko::CompositeShape newComp(std::move(testComp));
  BOOST_CHECK_EQUAL(newComp[0], ptr1);
  BOOST_CHECK_EQUAL(newComp[1], ptr2);
  BOOST_CHECK_EQUAL(newComp.getSize(), 2);
}

BOOST_AUTO_TEST_CASE(copyOperator)
{
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(5.0, 15.0, {35.0, 35.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Circle({3.0, 2.5}, 105));
  std::shared_ptr< marchenko::Shape > ptr3(new marchenko::Rectangle(6.0, 16.0, {36.0, 36.0}));
  std::shared_ptr< marchenko::Shape > ptr4(new marchenko::Circle({3.6, 2.6}, 106));
  marchenko::CompositeShape testComp;
  testComp.addShape(ptr1);
  testComp.addShape(ptr2);
  marchenko::CompositeShape newComp;
  newComp.addShape(ptr3);
  newComp.addShape(ptr4);
  newComp = testComp;
  BOOST_CHECK_EQUAL(newComp[0],testComp[0]);
  BOOST_CHECK_EQUAL(newComp[1],testComp[1]);
}

BOOST_AUTO_TEST_CASE(moveOperator)
{
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(5.0, 15.0, {35.0, 35.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Circle({3.0, 2.5}, 105));
  std::shared_ptr< marchenko::Shape > ptr3(new marchenko::Rectangle(6.0, 16.0, {36.0, 36.0}));
  std::shared_ptr< marchenko::Shape > ptr4(new marchenko::Circle({3.6, 2.6}, 106));
  marchenko::CompositeShape testComp;
  testComp.addShape(ptr1);
  testComp.addShape(ptr2);
  marchenko::CompositeShape newComp;
  newComp.addShape(ptr3);
  newComp.addShape(ptr4);
  newComp = std::move(testComp);
  BOOST_CHECK_EQUAL(newComp[0],ptr1);
  BOOST_CHECK_EQUAL(newComp[1],ptr2);
}

BOOST_AUTO_TEST_CASE(GeneralPosition)
{
  marchenko::point_t position = {2.0, 4.0};
  double width = 10.0, height = 10.0;
  std::shared_ptr<marchenko::Shape> testRect =
    std::make_shared<marchenko::Rectangle>(marchenko::Rectangle(width, height, position));
  marchenko::CompositeShape testComp(testRect);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.x, position.x, EPSILON);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.y, position.y, EPSILON);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().width, width, EPSILON);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().height, height, EPSILON);
}

BOOST_AUTO_TEST_CASE(moveToNewPosition)
{
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(5.0, 15.0, {35.0, 35.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Circle({3.0, 2.5}, 105));
  marchenko::CompositeShape testComp;
  testComp.addShape(ptr1);
  testComp.addShape(ptr2);
  double fstHeight = testComp.getFrameRect().height;
  double fstWidth = testComp.getFrameRect().width;
  double fstArea = testComp.getArea();
  marchenko::point_t positionAfterMoving = {5.0, 5.0};
  testComp.move(positionAfterMoving);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.x, positionAfterMoving.x, EPSILON);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.y, positionAfterMoving.y, EPSILON);
  BOOST_CHECK_EQUAL(fstHeight, testComp.getFrameRect().height);
  BOOST_CHECK_EQUAL(fstWidth, testComp.getFrameRect().width);
  BOOST_CHECK_EQUAL(fstArea, testComp.getArea());
}

BOOST_AUTO_TEST_CASE(moveByDxAndDy)
{
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(5.0, 15.0, {35.0, 35.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Circle({3.0, 2.5}, 105));
  marchenko::CompositeShape testComp;
  testComp.addShape(ptr1);
  testComp.addShape(ptr2);
  double fstHeight = testComp.getFrameRect().height;
  double fstWidth = testComp.getFrameRect().width;
  double fstArea = testComp.getArea();
  marchenko::point_t positionAfterMoving = {testComp.getFrameRect().pos.x + 3.0,testComp.getFrameRect().pos.y + 5.0};
  testComp.move(3.0,5.0);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.x, positionAfterMoving.x, EPSILON);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.y, positionAfterMoving.y, EPSILON);
  BOOST_CHECK_EQUAL(fstHeight, testComp.getFrameRect().height);
  BOOST_CHECK_EQUAL(fstWidth, testComp.getFrameRect().width);
  BOOST_CHECK_EQUAL(fstArea, testComp.getArea());
}

BOOST_AUTO_TEST_CASE(scaling)
{
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(5.0, 15.0, {35.0, 35.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Circle({3.0, 2.5}, 105));
  marchenko::CompositeShape testComp;
  testComp.addShape(ptr1);
  testComp.addShape(ptr2);
  double fstPosX = testComp.getFrameRect().pos.x;
  double fstPosY = testComp.getFrameRect().pos.y;
  marchenko::point_t fstPosRect =  ptr1 ->getFrameRect().pos;
  marchenko::point_t fstPosCircle = ptr2 ->getFrameRect().pos;
  double fstHeight = testComp.getFrameRect().height;
  double fstWidth = testComp.getFrameRect().width;
  double fstArea = testComp.getArea();
  testComp.scale(2.0);
  BOOST_CHECK_CLOSE(2.0 * 2.0 * fstArea, testComp.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(2.0 * fstHeight, testComp.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(2.0 * fstWidth, testComp.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(fstPosX, testComp.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(fstPosY, testComp.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(fstPosRect.x + (2.0-1.0) * (fstPosRect.x - fstPosX ) , ptr1 ->getFrameRect().pos.x , EPSILON);
  BOOST_CHECK_CLOSE(fstPosRect.y + (2.0-1.0) * (fstPosRect.y - fstPosY ) , ptr1 ->getFrameRect().pos.y , EPSILON);
  BOOST_CHECK_CLOSE(fstPosCircle.x + (2.0-1.0) * (fstPosCircle.x - fstPosX ) , ptr2 ->getFrameRect().pos.x , EPSILON);
  BOOST_CHECK_CLOSE(fstPosCircle.y + (2.0-1.0) * (fstPosCircle.y - fstPosY ) , ptr2 ->getFrameRect().pos.y , EPSILON);
}

BOOST_AUTO_TEST_CASE(invalidScaling)
{
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(5.0, 15.0, {35.0, 35.0}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Circle({3.0, 2.5}, 105));
  marchenko::CompositeShape testComp;
  testComp.addShape(ptr1);
  testComp.addShape(ptr2);
  BOOST_CHECK_THROW(testComp.scale(-2.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rotate)
{
  std::shared_ptr< marchenko::Shape > ptr1(new marchenko::Rectangle(3.0, 3.0, {4.5, 4.5}));
  std::shared_ptr< marchenko::Shape > ptr2(new marchenko::Circle({1.0, 1.0}, 1.0));
  marchenko::CompositeShape testComp;
  testComp.addShape(ptr1);
  testComp.addShape(ptr2);
  testComp.rotate(60);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().height, 7.3082, EPSILON);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().width, 3.807746825, EPSILON);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.x, 2.828177394, EPSILON);
  BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.y, 2.9220754043, EPSILON);
  BOOST_CHECK_CLOSE(ptr1 ->getFrameRect().pos.x , 2.4509618943, EPSILON);
  BOOST_CHECK_CLOSE(ptr1 ->getFrameRect().pos.y , 5.0490, EPSILON);
  BOOST_CHECK_CLOSE(ptr2 ->getFrameRect().pos.x , 3.73205080, EPSILON);
  BOOST_CHECK_CLOSE(ptr2 ->getFrameRect().pos.y , 0.267949192, EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()
