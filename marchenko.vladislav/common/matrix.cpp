#include <memory>
#include <stdexcept>

#include "matrix.hpp"

marchenko::Matrix::Matrix() :
  matrix_(nullptr),
  layers_(0),
  numberOfShapes_(0)
{
}

marchenko::Matrix::Matrix(const std::shared_ptr<marchenko::Shape> object):
  matrix_(nullptr),
  layers_(0),
  numberOfShapes_(0)
{
  if (object == nullptr)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  add(object);
}

marchenko::Matrix::Matrix(const CompositeShape & comp) :
  Matrix()
{
  for (size_t i = 0; i < comp.getSize(); i++)
  {
    add(comp[i]);
  }
}

marchenko::Matrix::Matrix(const marchenko::Matrix & object):
  matrix_(new std::shared_ptr<Shape>[object.layers_ * object.numberOfShapes_]),
  layers_(object.layers_),
  numberOfShapes_(object.numberOfShapes_)
{
  for (int i = 0; i < layers_ * numberOfShapes_; ++i)
  {
    matrix_[i] = object.matrix_[i];
  }
}

marchenko::Matrix::Matrix(marchenko::Matrix && object):
  matrix_(nullptr),
  layers_(object.layers_),
  numberOfShapes_(object.numberOfShapes_)
{
  matrix_.swap(object.matrix_);
  object.numberOfShapes_ = 0;
  object.layers_ = 0;
}

marchenko::Matrix::~Matrix()
{
  matrix_.reset();
  matrix_ = nullptr;
  numberOfShapes_ = 0;
  layers_ = 0;
}

marchenko::Matrix & marchenko::Matrix::operator=(const marchenko::Matrix & object)
{
  if (this != & object)
  {
    std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> newMatrix(
      new std::shared_ptr<marchenko::Shape>[object.layers_ * object.numberOfShapes_]);
    layers_ = object.layers_;
    numberOfShapes_ = object.numberOfShapes_;
    for (int i = 0; i < layers_ * numberOfShapes_; ++i)
    {
      newMatrix[i] = object.matrix_[i];
    }
    matrix_.swap(newMatrix);
  }
  return *this;
}

bool marchenko::Matrix::operator== (const Matrix & object) const
{
  if ((layers_ == object.layers_) && (numberOfShapes_ == object.numberOfShapes_))
  {
    bool equality = true;
    for (int i = 0; i < layers_ * numberOfShapes_; ++i)
    {
      if (!(matrix_[i] == object.matrix_[i]))
      {
        equality = false;
      }
    }
    if (equality)
    {
      return true;
    }
  }
  return false;
}

bool marchenko::Matrix::operator!= (const Matrix & object) const
{
  return !(object == *this);
}

marchenko::Matrix & marchenko::Matrix::operator=(marchenko::Matrix && object)
{
  if (this != & object)
  {
    layers_ = object.layers_;
    numberOfShapes_ = object.numberOfShapes_;
    matrix_.reset();
    matrix_.swap(object.matrix_);
    object.layers_ = 0;
    object.numberOfShapes_ = 0;
  }
  return *this;
}

std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> marchenko::Matrix::operator[](const int index) const
{
  if ((index < 0) || (index >= layers_))
  {
    throw std::out_of_range("Invalid index");
  }
  std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> layer(
    new std::shared_ptr<marchenko::Shape>[numberOfShapes_]);
  for (int i = 0; i < numberOfShapes_; ++i)
  {
    layer[i] = matrix_[index * numberOfShapes_ + i];
  }
  return layer;
}

size_t marchenko::Matrix::getLayersNum() const noexcept
{
  return layers_;
}

void marchenko::Matrix::add(const std::shared_ptr<marchenko::Shape> object) noexcept
{
  if (layers_ == 0)
  {
    std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> newMatrix(
      new std::shared_ptr<marchenko::Shape>[(layers_ + 1) * (numberOfShapes_ + 1)]);
    numberOfShapes_++;
    layers_++;
    matrix_.swap(newMatrix);
    matrix_[0] = object;
  }
  else
  {
    bool addedShape = false;
    for (int i = 0; !addedShape ; ++i)
    {
      for (int j = 0; j < numberOfShapes_; ++j)
      {
        if (!matrix_[i * numberOfShapes_ + j])
        {
          matrix_[i * numberOfShapes_ + j] = object;
          addedShape = true;
          break;
        }
        else
        {
          if (checkOverlap(i * numberOfShapes_ + j, object))
          {
            break;
          }
        }

        if (j == (numberOfShapes_ - 1))
        {
          std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> newMatrix(
            new std::shared_ptr<marchenko::Shape>[layers_ * (numberOfShapes_ + 1)]);
          numberOfShapes_++;
          for (int n = 0; n < layers_; ++n)
          {
            for (int m = 0; m < numberOfShapes_ - 1; ++m)
            {
              newMatrix[n * numberOfShapes_ + m] = matrix_[n * (numberOfShapes_ - 1) + m];
            }
            newMatrix[(n + 1) * numberOfShapes_ - 1] = nullptr;
          }
          newMatrix[(i + 1) * numberOfShapes_ - 1] = object;
          matrix_.swap(newMatrix);
          addedShape = true;
          break;
        }
      }
      if ((i == (layers_ - 1)) && !addedShape)
      {
        std::unique_ptr<std::shared_ptr<marchenko::Shape>[]> newMatrix(
          new std::shared_ptr<marchenko::Shape>[(layers_ + 1) * numberOfShapes_]);
        layers_++;
        for (int n = 0; n < ((layers_ - 1) * numberOfShapes_); ++n)
        {
          newMatrix[n] = matrix_[n];
        }
        for (int n = ((layers_ - 1) * numberOfShapes_) ; n < (layers_ * numberOfShapes_); ++n)
        {
          newMatrix[n] = nullptr;
        }
        newMatrix[(layers_ - 1) * numberOfShapes_ ] = object;
        matrix_.swap(newMatrix);
        addedShape = true;
      }
    }
  }
}

void marchenko::Matrix::add(const CompositeShape & object)
{
  for (size_t i = 0; i < object.getSize(); i++)
  {
    add(object[i]);
  }
}

bool marchenko::Matrix::checkOverlap(const int index,
  std::shared_ptr<marchenko::Shape> object) const noexcept
{
  marchenko::rectangle_t nShapeFrameRect = object->getFrameRect();
  marchenko::rectangle_t mShapeFrameRect = matrix_[index]->getFrameRect();
  marchenko::point_t newPoints[4] = 
  {
    {nShapeFrameRect.pos.x - nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y + nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x + nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y + nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x + nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y - nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x - nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y - nShapeFrameRect.height / 2.0}
  };

  marchenko::point_t matrixPoints[4] = 
  {
    {mShapeFrameRect.pos.x - mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y + mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x + mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y + mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x + mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y - mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x - mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y - mShapeFrameRect.height / 2.0}
  };

  for (int i = 0; i < 4; ++i)
  {
    if (((newPoints[i].x >= matrixPoints[0].x) && (newPoints[i].x <= matrixPoints[2].x)
        && (newPoints[i].y >= matrixPoints[3].y) && (newPoints[i].y <= matrixPoints[1].y))
            || ((matrixPoints[i].x >= newPoints[0].x) && (matrixPoints[i].x <= newPoints[2].x)
                && (matrixPoints[i].y >= newPoints[3].y) && (matrixPoints[i].y <= newPoints[1].y)))
    {
      return true;
    }
  }
  return false;
}
