#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

#include "shape.hpp"
#include "composite-shape.hpp"

namespace marchenko
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const std::shared_ptr<Shape> object);
    Matrix(const CompositeShape& comp);
    Matrix(const Matrix & object);
    Matrix(Matrix && object);
    ~Matrix();
    Matrix & operator=(const Matrix & object);
    Matrix & operator=(Matrix && object);
    std::unique_ptr<std::shared_ptr<Shape>[]> operator[](const int index) const;
    bool operator==(const Matrix & object) const;
    bool operator!=(const Matrix & object) const;
    size_t getLayersNum() const noexcept;
    void add(const std::shared_ptr<Shape> shape) noexcept;
    void add(const CompositeShape& object);  
  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> matrix_;
    int layers_;
    int numberOfShapes_;
    bool checkOverlap(const int index, std::shared_ptr<Shape> object) const noexcept;
  };
}

#endif

