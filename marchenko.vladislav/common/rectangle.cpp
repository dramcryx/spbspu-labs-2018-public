#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>
#include <stdexcept>

#include "rectangle.hpp"

marchenko::Rectangle::Rectangle(const double width, const double height, const point_t &centerPos) :
  width_(width),
  height_(height),
  center_(centerPos)
{
  if (width_ < 0.0)
  {
    throw std::invalid_argument("Invalid value of width. It must be >=0.");
  }
  if (height_ < 0.0)
  {
    throw std::invalid_argument("Invalid value of height. It must be >=0.");
  }
  vertices_[0] = {centerPos.x - width / 2 , centerPos.y - height / 2};
  vertices_[1] = {centerPos.x - width / 2 , centerPos.y + height / 2};
  vertices_[2] = {centerPos.x + width / 2 , centerPos.y + height / 2};
  vertices_[3] = {centerPos.x + width / 2 , centerPos.y - height / 2};
}

double marchenko::Rectangle::getArea() const
{
  return width_ * height_;
}

marchenko::rectangle_t marchenko::Rectangle::getFrameRect() const
{
  double lVert = vertices_[0].x;
  double rVert = vertices_[0].x;
  double tVert = vertices_[0].y;
  double dVert = vertices_[0].y;
  for (size_t i = 0; i < 4 ; i++)
  {
    if (vertices_[i].x < lVert)
    {
      lVert = vertices_[i].x;
    }
    if (vertices_[i].x > rVert)
    {
      rVert = vertices_[i].x;
    }
    if (vertices_[i].y < dVert)
    {
      dVert = vertices_[i].y;
    }
    if (vertices_[i].y > tVert)
    {
      tVert = vertices_[i].y;
    }
  }
  return { tVert - dVert, rVert - lVert, {(rVert + lVert)/2 , (tVert + dVert) / 2} };
}

void marchenko::Rectangle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
  for ( size_t i = 0; i < 4; i++)
  {
    vertices_[i].x += dx;
    vertices_[i].y += dy;
  }
}

void marchenko::Rectangle::move(const point_t &newPos)
{
  vertices_[0] = {newPos.x - width_ / 2 , newPos.y - height_/2};
  vertices_[1] = {newPos.x - width_ / 2 , newPos.y + height_/2};
  vertices_[2] = {newPos.x + width_ / 2 , newPos.y + height_/2};
  vertices_[3] = {newPos.x + width_ / 2 , newPos.y - height_/2};
  center_ = newPos;
}

void marchenko::Rectangle::scale(const double coefficient)
{
  if (coefficient < 0.0)
  {
    throw std::invalid_argument("Invalid coefficient of scale. It must be >=0.");
  }
  else
  {
    width_ *= coefficient;
    height_ *= coefficient;
    point_t cent = getFrameRect().pos;
    for (size_t i = 0; i < 4; i++)
    {
      vertices_[i].x = cent.x + coefficient * (vertices_[i].x - cent.x);
      vertices_[i].y = cent.y + coefficient * (vertices_[i].y - cent.y);
    }
  }
}

void marchenko::Rectangle::rotate(double angle)
{
  double sinAngle = sin(angle * M_PI / 180);
  double cosAngle = cos(angle * M_PI / 180);
  marchenko::point_t center = getFrameRect().pos;
  for (size_t i = 0; i < 4; i++)
  {
    vertices_[i].x = center.x + (vertices_[i].x - center.x) * cosAngle - (vertices_[i].y - center.y) * sinAngle;
    vertices_[i].y = center.y + (vertices_[i].y - center.y) * cosAngle + (vertices_[i].x - center.x) * sinAngle;
  }
}

marchenko::point_t marchenko::Rectangle::getVertice(const int index) const
{
  return vertices_[index];
}
