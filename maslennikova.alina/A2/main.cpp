#include <iostream>

#include "../common/rectangle.hpp"
#include "../common/circle.hpp"

using namespace maslennikova;

int main()
{
    maslennikova::Rectangle rectangle({10.0, 8.0, {6.0, 14.0}});
    maslennikova::Shape *shape = &rectangle;
    std::cout << shape -> getArea() << std::endl;
    shape -> scale(2.0);
    std::cout << shape -> getArea() << std::endl;
    
    maslennikova::Circle circle(8.0, {16.0, 16.0});
    shape = &circle;
    std::cout << shape -> getArea() << std::endl;
    shape -> scale(3.0);
    std::cout << shape -> getArea() << std::endl;

    return 0;
}


