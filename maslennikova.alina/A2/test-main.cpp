#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"
#include "circle.hpp"

double Eps = 0.0001;

BOOST_AUTO_TEST_SUITE(RectangleTests)

BOOST_AUTO_TEST_CASE(InvarienceOfParameters)
{
    maslennikova::Rectangle rectangle({10.0, 8.0, {6.0, 14.0}});
    double area = rectangle.getArea();
    
    rectangle.move(12.0, 20.0);
    
    BOOST_REQUIRE_EQUAL(rectangle.getFrameRect().width, 10.0);
    BOOST_REQUIRE_EQUAL(rectangle.getFrameRect().height, 8.0);
    BOOST_REQUIRE_EQUAL(rectangle.getArea(), area);
}

BOOST_AUTO_TEST_CASE(Scale)
{
    maslennikova::Rectangle rectangle({10.0, 8.0, {6.0, 14.0}});
    double area = rectangle.getArea();
    
    rectangle.scale(2.0);
    
    BOOST_REQUIRE_CLOSE_FRACTION(rectangle.getArea(), area * pow(2.0, 2.0), Eps);
}

BOOST_AUTO_TEST_CASE(ArgumentsConstructor)
{
    BOOST_CHECK_THROW(maslennikova::Rectangle rectangle({-2.0, -2.0, {10.0, 8.0}}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ArgumentScale)
{
    maslennikova::Rectangle rectangle({5.0, 4.0, {3.0, 7.0}});
    BOOST_CHECK_THROW(rectangle.scale(-1.0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CircleTests)

BOOST_AUTO_TEST_CASE(InvarienceOfParameters)
{
    maslennikova::Circle circle(8.0, {16.0, 16.0});
    double area = circle.getArea();
    
    circle.move(12.0, 20.0);
    
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().width, 16.0);
    BOOST_REQUIRE_EQUAL(circle.getFrameRect().height, 16.0);
    BOOST_REQUIRE_EQUAL(circle.getArea(), area);
}

BOOST_AUTO_TEST_CASE(Scale)
{
    maslennikova::Circle circle(8.0, {16.0, 16.0});
    double area = circle.getArea();
    
    circle.scale(3.0);
    
    BOOST_REQUIRE_CLOSE_FRACTION(circle.getArea(), (area * pow(3.0, 2.0)), Eps);
}

BOOST_AUTO_TEST_CASE(ArgumentsConstructor)
{
    BOOST_CHECK_THROW(maslennikova::Circle circle(-2.0, {16.0, 16.0}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ArgumentScale)
{
    maslennikova::Circle circle(8.0, {16.0, 16.0});
    BOOST_CHECK_THROW(circle.scale(-1.0), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()



