#include <iostream>

#include "../common/rectangle.hpp"
#include "../common/circle.hpp"
#include "../common/composite-shape.hpp"

int main()
{
    
    std::shared_ptr<maslennikova::Shape> rectPtr
        = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({5.0, 10.0, {50.0, 100.0}}));
    maslennikova::CompositeShape shape(rectPtr);

    std::shared_ptr<maslennikova::Shape> circlePtr
        = std::make_shared<maslennikova::Circle>(maslennikova::Circle(6.0, {12.0, 12.0}));
    shape.addShape(circlePtr);
        
    std::cout<< shape.getArea() << "\n";
    
    shape.move({100.0, 50.0});
    std::cout<< shape.getArea() << "\n";
    
    shape.move(30.0, 40.0);
    shape.scale(2.0);
    std::cout<< shape.getArea() << "\n";
    
    shape.delShape(0);
    
    return 0;
}
