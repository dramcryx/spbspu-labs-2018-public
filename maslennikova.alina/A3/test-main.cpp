#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)

  BOOST_AUTO_TEST_CASE(InvarienceOfArea)
  {
    std::shared_ptr<maslennikova::Shape> rectPtr
      = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    maslennikova::CompositeShape shape(rectPtr);
    
    std::shared_ptr<maslennikova::Shape> circlePtr
      = std::make_shared<maslennikova::Circle>(maslennikova::Circle(5.0, {10.0, 10.0}));
    shape.addShape(circlePtr);
  }

  BOOST_AUTO_TEST_CASE(InvarienceOfCoords)
  {
    double EPSILON = 0.1;
    
    std::shared_ptr<maslennikova::Shape> rectPtr
      = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    maslennikova::CompositeShape shape(rectPtr);
    
    std::shared_ptr<maslennikova::Shape> circlePtr
      = std::make_shared<maslennikova::Circle>(maslennikova::Circle(5.0, {10.0, 10.0}));
    shape.addShape(circlePtr);
        
    double posX = shape.getFrameRect().pos.x;
    double posY = shape.getFrameRect().pos.y;
    shape.scale(2.0);
    
    BOOST_REQUIRE_CLOSE_FRACTION(shape.getFrameRect().pos.x, posX, EPSILON);
    BOOST_REQUIRE_CLOSE_FRACTION(shape.getFrameRect().pos.y, posY, EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Scale)
  {
    double EPSILON = 0.0001;
    std::shared_ptr<maslennikova::Shape> rectPtr
      = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    maslennikova::CompositeShape shape(rectPtr);
    
    std::shared_ptr<maslennikova::Shape> circlePtr
      = std::make_shared<maslennikova::Circle>(maslennikova::Circle(5.0, {10.0, 10.0}));
    shape.addShape(circlePtr);
        
    double area = shape.getArea();
    shape.scale(2.0);
    BOOST_REQUIRE_CLOSE_FRACTION(shape.getArea(), area * pow(2.0, 2.0), EPSILON);

  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentConstructor)
  {
    
    BOOST_REQUIRE_THROW(maslennikova::CompositeShape shape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentAddShape)
  {
    std::shared_ptr<maslennikova::Shape> rectPtr
      = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    maslennikova::CompositeShape shape(rectPtr);
    
    BOOST_REQUIRE_THROW(shape.addShape(nullptr), std::invalid_argument);

  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentScale)
  {
    std::shared_ptr<maslennikova::Shape> rectPtr
      = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    maslennikova::CompositeShape shape(rectPtr);
    
    std::shared_ptr<maslennikova::Shape> circlePtr
      = std::make_shared<maslennikova::Circle>(maslennikova::Circle(5.0, {10.0, 10.0}));
    shape.addShape(circlePtr);
        
    BOOST_CHECK_THROW(shape.scale(-1.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(GeneralAccordance)
  {
    double EPSILON = 0.0001;
    std::shared_ptr<maslennikova::Shape> rectPtr
      = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {128.0, 256.0}}));
    maslennikova::CompositeShape shape(rectPtr);
    maslennikova::rectangle_t shapeFrameRect = shape.getFrameRect();
    maslennikova::rectangle_t rectFrameRect = rectPtr -> getFrameRect();
    
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.pos.x, rectFrameRect.pos.x, EPSILON);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.pos.y, rectFrameRect.pos.y, EPSILON);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.width, rectFrameRect.width, EPSILON);
    BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.height, rectFrameRect.height, EPSILON);
      
  }

BOOST_AUTO_TEST_SUITE_END()

