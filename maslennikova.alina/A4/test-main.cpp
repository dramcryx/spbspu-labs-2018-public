#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double epsilon = 0.0001;

BOOST_AUTO_TEST_SUITE(RotateTest)

    BOOST_AUTO_TEST_CASE(RectangleTest)
    {
      maslennikova::Rectangle rectangle({24.0, 48.0, {128.0, 256.0}});
      rectangle.rotate(90.0);
      maslennikova::rectangle_t shapeFrameRect = rectangle.getFrameRect();
  
      BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.width, 24.0, epsilon);
      BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.height, 48.0, epsilon);
      BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.pos.x, 128.0, epsilon);
      BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.pos.y, 256.0, epsilon);
    }

  BOOST_AUTO_TEST_CASE(CompositeShapeTest)
    {
      std::shared_ptr<maslennikova::Shape> rectPtr1
        = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({20.0, 48.0, {128.0, 256.0}}));
      maslennikova::CompositeShape shape(rectPtr1);
  
      std::shared_ptr<maslennikova::Shape> rectPtr2
        = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {200.0, 256.0}}));
      shape.addShape(rectPtr2);
  
      shape.rotate(90.0);
      maslennikova::rectangle_t shapeFrameRect = shape.getFrameRect();
  
      BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.width, 24.0, epsilon);
      BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.height, 120.0, epsilon);
      BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.pos.x, 165.0, epsilon);
      BOOST_REQUIRE_CLOSE_FRACTION(shapeFrameRect.pos.y, 255.0, epsilon);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTest)

  BOOST_AUTO_TEST_CASE(AddShapeTest)
  {
      std::shared_ptr<maslennikova::Shape> rectPtr1
        = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({20.0, 48.0, {128.0, 256.0}}));
      maslennikova::MatrixShape matrix_shape(rectPtr1);
  
      std::shared_ptr<maslennikova::Shape> rectPtr2
        = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {150.0, 256.0}}));
      matrix_shape.addShape(rectPtr2);
  
      std::shared_ptr<maslennikova::Shape> circlePtr
        = std::make_shared<maslennikova::Circle>(maslennikova::Circle(10.0, {130.0, 256.0}));
      matrix_shape.addShape(circlePtr);
  
      BOOST_REQUIRE_EQUAL(matrix_shape[0][0], rectPtr1);
      BOOST_REQUIRE_EQUAL(matrix_shape[0][1], rectPtr2);
      BOOST_REQUIRE_EQUAL(matrix_shape[1][0], circlePtr);
  }

  BOOST_AUTO_TEST_CASE(AddCompositeShapeTest)
  {
      std::shared_ptr<maslennikova::Shape> rectPtr1
        = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({20.0, 48.0, {128.0, 256.0}}));
      maslennikova::CompositeShape comp_shape(rectPtr1);
      maslennikova::MatrixShape matrix_shape(rectPtr1);
  
      std::shared_ptr<maslennikova::Shape> rectPtr2
        = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({24.0, 48.0, {200.0, 256.0}}));
      comp_shape.addShape(rectPtr2);
      matrix_shape.addShape(rectPtr2);
  
      std::shared_ptr<maslennikova::Shape> compPtr
        = std::make_shared<maslennikova::CompositeShape>(comp_shape);
      matrix_shape.addShape(compPtr);
  
      BOOST_REQUIRE_EQUAL(matrix_shape[0][0], rectPtr1);
      BOOST_REQUIRE_EQUAL(matrix_shape[0][1], rectPtr2);
      BOOST_REQUIRE_EQUAL(matrix_shape[1][0], compPtr);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentConstructorTest)
  {
      BOOST_REQUIRE_THROW(maslennikova::MatrixShape matrix_shape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentAddShapeTest)
  {
      std::shared_ptr<maslennikova::Shape> rectPtr1
        = std::make_shared<maslennikova::Rectangle>(maslennikova::Rectangle({20.0, 48.0, {128.0, 256.0}}));
      maslennikova::MatrixShape matrix_shape(rectPtr1);
  
      BOOST_REQUIRE_THROW(matrix_shape.addShape(nullptr), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
