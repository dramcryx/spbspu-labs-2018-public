#include <iostream>
#include <cmath>

#include "circle.hpp"
#include "base-types.hpp"

maslennikova::Circle::Circle(double radius, const point_t & position):
  r_(radius),
  p_(position),
  angle_(0.0)
{
  if (r_ < 0.0)
  {
    throw std::invalid_argument("it is not right radius");
  }
}

double maslennikova::Circle::getArea() const noexcept
{
  return (M_PI * r_ * r_);
}

maslennikova::rectangle_t maslennikova::Circle::getFrameRect() const noexcept
{
  return rectangle_t{2 * r_, 2 * r_, p_};
}

void maslennikova::Circle::move(const point_t & p) noexcept
{
  p_ = p;
}

void maslennikova::Circle::move(double dx, double dy) noexcept
{
  p_.x += dx;
  p_.y += dy;
}

void maslennikova::Circle::scale(double k)
{
    if (k < 0.0)
    {
        throw std::invalid_argument("it is not right parameter of scaling");
    }
    r_ *= k;
}

void maslennikova::Circle::rotate(double a)
{
    angle_ += a;
    if (angle_ >= 360.0)
    {
        angle_ = fmod(angle_, 360.0);
    }
}

