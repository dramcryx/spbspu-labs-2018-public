#include <iostream>
#include <iomanip>
#include <cmath>

#include "matrix.hpp"

maslennikova::MatrixShape::MatrixShape(const std::shared_ptr<Shape> & shapeElement):
  matrix_(new std::shared_ptr<Shape>[1]),
  row_(0),
  column_(0)
{
  if (!shapeElement)
  {
    throw std::invalid_argument("Error. Object does not exist.");
  }
  matrix_[0] = shapeElement;
  ++row_;
  ++column_;
}

maslennikova::MatrixShape::MatrixShape(const MatrixShape & obj):
  matrix_(new std::shared_ptr<Shape>[obj.row_ * obj.column_]),
  row_(obj.row_),
  column_(obj.column_)
{
  for (size_t i = 0; i < (row_ * column_); ++i)
  {
    matrix_[i] = obj.matrix_[i];
  }
}

maslennikova::MatrixShape::MatrixShape(MatrixShape && obj):
  matrix_(std::move(obj.matrix_)),
  row_(std::move(obj.row_)),
  column_(std::move(obj.column_))
{
  obj.matrix_.reset();
  obj.row_ = 0;
  obj.column_ = 0;
}

maslennikova::MatrixShape & maslennikova::MatrixShape::operator =(const MatrixShape & obj)
{
  if (this != &obj)
  {
     row_ = obj.row_;
     column_ = obj.column_;
     m_type new_arr(new std::shared_ptr<Shape>[row_ * column_]);
     for (size_t i = 0; i < (row_ * column_); ++i)
     {
       new_arr[i] = obj.matrix_[i];
     }
     matrix_.swap(new_arr);
  }
  return *this;
}

maslennikova::MatrixShape & maslennikova::MatrixShape::operator =(MatrixShape && obj)
{
  if (this != &obj)
  {
    matrix_ = std::move(obj.matrix_);
    row_ = std::move(obj.row_);
    column_ = std::move(obj.column_);
    obj.matrix_.reset();
    obj.row_ = 0;
    obj.column_ = 0;
  }
  return *this;
}

std::unique_ptr<std::shared_ptr<maslennikova::Shape>[]>::pointer maslennikova::MatrixShape::operator [](const size_t index)
{
  if (index > row_)
  {
    throw std::out_of_range("index is out of range");
  }
  return (matrix_.get() + index * column_);
}

void maslennikova::MatrixShape::addShape(const std::shared_ptr<Shape> & shapeElement)
{
  if (!shapeElement)
  {
    throw std::invalid_argument("this object doesn't exist");
  }
  size_t i = row_ * column_;
  size_t desired_row = 1;
  while (i > 0)
  {
    --i;
    if (checkOverlap(matrix_[i], shapeElement))
    {
      desired_row = i / column_ + 2;
    }
  }
  size_t rows_temp = row_;
  size_t columns_temp = column_;
  size_t free_columns = 0;
  if (desired_row > row_)
  {
    ++rows_temp;
    free_columns = column_;
  }
  else
  {
    size_t j = (desired_row - 1) * column_;
    while (j < (desired_row * column_))
    {
      if (!matrix_[j])
      {
        ++free_columns;
      }
        ++j;
    }
    if (free_columns == 0)
    {
      ++columns_temp;
      free_columns = 1;
    }
  }
  m_type new_arr(new std::shared_ptr<Shape>[rows_temp * columns_temp]);
  for (size_t i = 0; i < rows_temp; ++i)
  {
    for (size_t j = 0; j < columns_temp; ++j)
    {
      if ((i >= row_) || (j >= column_))
      {
        new_arr[i * columns_temp + j].reset();
        continue;
      }
      new_arr[i * columns_temp + j] = matrix_[i * column_ + j];
    }
  }
  new_arr[desired_row * columns_temp - free_columns] = shapeElement;
  matrix_.swap(new_arr);
  row_ = rows_temp;
  column_ = columns_temp;
}

bool maslennikova::MatrixShape::checkOverlap(const std::shared_ptr<Shape> & shape1, const std::shared_ptr<Shape> & shape2) const
{
  if ((!shape1) || (!shape2))
  {
    return false;
  }
  rectangle_t shapeFrameRect1 = shape1 -> getFrameRect();
  rectangle_t shapeFrameRect2 = shape2 -> getFrameRect();
  return ((abs(shapeFrameRect1.pos.x - shapeFrameRect2.pos.x)
             < ((shapeFrameRect1.width / 2) + (shapeFrameRect2.width / 2)))
            && ((abs(shapeFrameRect1.pos.y - shapeFrameRect2.pos.y)
                 < ((shapeFrameRect1.height / 2) + (shapeFrameRect2.height / 2)))));
}
