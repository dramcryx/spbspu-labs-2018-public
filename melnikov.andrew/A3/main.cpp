#include <iostream>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

using namespace melnikov;

void testShape(Shape & shapeObject) {
  std::cout << "Initial values:" << std::endl << "Area: " << shapeObject.getArea() << std::endl;
  shapeObject.getFrameRect();

  std::cout << "After moving in (2.0, 5.0):" << std::endl;
  shapeObject.move(2.0, 5.0);
  std::cout << "Area: " << shapeObject.getArea() << std::endl;
  shapeObject.getFrameRect();
  
  std::cout << "After moving to ({50, 100}):" << std:: endl;
  shapeObject.move({50, 100});
  std::cout << "Area: " << shapeObject.getArea() << std::endl << std::endl;
  shapeObject.getFrameRect();

  std::cout << "Increase scale in (3.0):" << std::endl;
  shapeObject.scale(3.0);
  std::cout << "Area: " << shapeObject.getArea() << std::endl;
}

int main() {
  try {
    Circle circleObject({30, 30}, 10);
    Rectangle rectangleObject({30, 30}, 10, 20);
    Triangle triangleObject({0, 0}, {90, 0}, {0, 30});

    std::shared_ptr <Shape> circleObjectPtr = std::make_shared <Circle> (circleObject);
    std::shared_ptr <Shape> rectangleObjectPtr = std::make_shared <Rectangle> (rectangleObject);
    std::shared_ptr <Shape> triangleObjectPtr = std::make_shared <Triangle> (triangleObject);

    testShape(rectangleObject);
    testShape(circleObject);
    testShape(triangleObject);

    CompositeShape compositeObject({0, 0});
    compositeObject.addElement(circleObjectPtr);
    compositeObject.addElement(rectangleObjectPtr);
    compositeObject.addElement(triangleObjectPtr);

    testShape(compositeObject);
  }
  catch(std::invalid_argument & invArg) {
    std::cerr << invArg.what() << std::endl;
    return 1;
  }
  return 0;
}
