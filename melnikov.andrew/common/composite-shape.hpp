#ifndef COMPOSITE_SHAPE_HPP_
#define COMPOSITE_SHAPE_HPP_

#include <memory>
#include "shape.hpp"

namespace melnikov {
  class CompositeShape:
    public Shape {
  public:
    CompositeShape(const point_t & pos);
    ~CompositeShape();
    
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(const double dx, const double dy) override;
    void scale(const double coef) override;
    void rotate(const double angle) override;
    
    void addElement(const std::shared_ptr <Shape> addedShape);
    void removeElement(const int index);
    
  private:
    int size_;
    double angle_;
    std::unique_ptr <std::shared_ptr <Shape> []> array_;
  };
}

#endif //COMPOSITE_SHAPE_HPP_
