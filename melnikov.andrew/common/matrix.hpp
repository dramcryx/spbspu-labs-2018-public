#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <iostream>
#include <memory>
#include "shape.hpp"

namespace  melnikov {
  class Matrix {
  public:
    Matrix(const std::shared_ptr <Shape> shape);
    Matrix(const Matrix & matrix);
    Matrix();
    ~Matrix();

    void addShape(const std::shared_ptr <Shape> addedShape);
    int getLayersNumber();
    int getShapesNumber();
    bool crossing(const std::shared_ptr <Shape> currentShape, const std::shared_ptr <Shape> newShape);
  private:
    int layerSize_;
    int shapeSize_;
    std::unique_ptr <std::shared_ptr <Shape> []> shapeArr_;
  };
}

#endif //MATRIX_HPP
