#include <math.h>
#include <iostream>
#include "rectangle.hpp"

using namespace melnikov;

Rectangle::Rectangle(const point_t & pos, const double width, const double height):
  Shape(pos) {
  if (width < 0.0) {
    throw std::invalid_argument("Width is invalid!");
  } else if (height < 0.0) {
    throw std::invalid_argument("Height is invalid!");
  }
  width_ = width;
  height_ = height;
  angle_ = 0.0;
}

double Rectangle::getArea() const {
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const {
  const double width = height_ * std::fabs(sin(angle_ * M_PI / 180)) + width_ * std::fabs(cos(angle_ * M_PI / 180));
  const double height = height_ * std::fabs(cos(angle_ * M_PI / 180)) + width_ * std::fabs(sin(angle_ * M_PI / 180));
  return rectangle_t{width, height, pos_};
}

void Rectangle::move(const double dx, const double dy) {
  pos_.x += dx;
  pos_.y += dy;
}

void Rectangle::move(const point_t & pos) {
  pos_ = pos;
};

void Rectangle::scale(const double coef) {
  if (coef >= 0.0) {
    width_ *= coef;
    height_ *= coef;
  } else {
    throw std::invalid_argument("Scaling coefficient is Invalid!");
  }
}

void Rectangle::rotate(const double angle) {
  angle_ += angle;
}
