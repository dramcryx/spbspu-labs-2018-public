#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace melnikov {
  class Rectangle:
    public Shape {
  public:
    Rectangle(const point_t & pos, const double width, const double height);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(const double dx, const double dy) override;
    void scale(const double coef) override;
    void rotate(const double angle) override;
  private:
    double width_;
    double height_;
    double angle_;
  };
}

#endif //RECTANGLE_HPP
