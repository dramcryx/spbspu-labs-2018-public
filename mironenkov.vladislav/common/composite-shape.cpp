#include "composite-shape.hpp"
#include <cmath>
#include <stdexcept>
#include <iostream>

mironenkov::CompositeShape::CompositeShape(const point_t & pos):
  Shape(pos),
  numberShapes_(0),
  arrayShapes_(nullptr)
{
}

double mironenkov::CompositeShape::getArea() const
{
  double area = 0.0;
  for (int i = 0; i < numberShapes_; ++i) {
    area += arrayShapes_[i] -> getArea();
  }
  return area;
}

mironenkov::rectangle_t mironenkov::CompositeShape::getFrameRect() const
{
  if (arrayShapes_ == 0) {
    return {0.0, 0.0, {0.0, 0.0}};
  }
  mironenkov::rectangle_t shapeFrame = arrayShapes_[0] -> getFrameRect();
  double minX = shapeFrame.pos.x - shapeFrame.width/2;
  double maxX = shapeFrame.pos.x + shapeFrame.width/2;
  double minY = shapeFrame.pos.y - shapeFrame.height/2;
  double maxY = shapeFrame.pos.y + shapeFrame.height/2;
  for (int i = 0; i < numberShapes_; ++i) {
    shapeFrame = arrayShapes_[i] -> getFrameRect();
    if (shapeFrame.pos.x - shapeFrame.width / 2 < minX) {
      minX = shapeFrame.pos.x - shapeFrame.width / 2;
    }
    if (shapeFrame.pos.x + shapeFrame.width / 2 > maxX ){
      maxX = shapeFrame.pos.x + shapeFrame.width / 2;
    }
    if (shapeFrame.pos.y - shapeFrame.height / 2 < minY) {
      minY = shapeFrame.pos.y - shapeFrame.height / 2;
    }
    if (shapeFrame.pos.y + shapeFrame.height / 2 > maxY) {
      maxY = shapeFrame.pos.y + shapeFrame.height / 2;
    }
  }
  return { maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2} };
}

void mironenkov::CompositeShape::move(const mironenkov::point_t & pos)
{
  for (int i = 0; i < numberShapes_; ++i) {
    arrayShapes_[i]->move((pos.x - getFrameRect().pos.x), (pos.y - getFrameRect().pos.y));
  }
}

void mironenkov::CompositeShape::move(const double dx, const double dy)
{
  for (int i = 0; i < numberShapes_; ++i) {
    arrayShapes_[i]->move(dx, dy);
  }
}

void mironenkov::CompositeShape::scale(const double coeff)
{
  if (coeff <= 0.0) {
    throw std::invalid_argument("Scale coeff is Invalid!");
  }
  point_t currentPosition = getFrameRect().pos;
  for (int i = 0; i < numberShapes_; ++i) {
    arrayShapes_[i]->move((coeff - 1) * (arrayShapes_[i]->getFrameRect().pos.x - currentPosition.x), (coeff - 1)
      * (arrayShapes_[i]->getFrameRect().pos.y - currentPosition.y));
    arrayShapes_[i] -> scale(coeff);
  }
}

void mironenkov::CompositeShape::addShape(const std::shared_ptr< Shape > addedShape)
{
  if (addedShape == nullptr) {
    throw std::invalid_argument("Empty pointer!");
  }
  std::unique_ptr< std::shared_ptr < Shape > [] > tempArray(new std::shared_ptr< Shape > [numberShapes_ + 1]);
  for (int i = 0; i < numberShapes_; ++i) {
    tempArray[i] = arrayShapes_[i];
  }
  tempArray[numberShapes_] = addedShape;
  numberShapes_++;
  arrayShapes_.swap(tempArray);
}

void mironenkov::CompositeShape::removeShape(const int index)
{
  if ((numberShapes_ == 0) || (index >= numberShapes_)) {
    throw std::invalid_argument("Index is out of range!");
  }
  std::unique_ptr< std::shared_ptr < Shape > [] > tempArray(new std::shared_ptr< Shape > [numberShapes_ - 1]);
  for (int i = 0; i < index; ++i) {
    tempArray[i] = arrayShapes_[i];
  }
  for (int i = index; i < numberShapes_ - 1; ++i) {
    tempArray[i] = arrayShapes_[i + 1];
  }
  arrayShapes_.swap(tempArray);
  numberShapes_--;
}
void mironenkov::CompositeShape::rotate(const double degrees)
{
    if (numberShapes_ != 0)
    {
        double radns = degrees * M_PI / 180;
        point_t pos = {0, 0};
        rectangle_t temprect = {0 , 0, pos};
        for (int i = 0; i < numberShapes_; i++)
        {
            temprect = arrayShapes_[i]->getFrameRect();
            pos.x = pos_.x + (temprect.pos.x - pos_.x) * cos(radns) - (temprect.pos.y - pos_.y) * sin(radns);
            pos.y = pos_.y + (temprect.pos.y - pos_.y) * cos(radns) + (temprect.pos.x - pos_.x) * sin(radns);
            arrayShapes_[i]->move(pos);
            arrayShapes_[i]->rotate(degrees);
        }
        pos_.x = (maxXY().x + minXY().x) / 2.0;
        pos_.y = (maxXY().y + minXY().y) / 2.0;
    }
}
mironenkov::point_t mironenkov::CompositeShape::maxXY() const
{
    double maxX = arrayShapes_[0]->getFrameRect().pos.x + ((arrayShapes_[0]->getFrameRect().width) / 2.0);
    double maxY = arrayShapes_[0]->getFrameRect().pos.y + ((arrayShapes_[0]->getFrameRect().height) / 2.0);
    for(int i = 0; i < numberShapes_; i++)
    {
        if ((arrayShapes_[i]->getFrameRect().pos.x + ((arrayShapes_[i]->getFrameRect().width) / 2.0)) > maxX)
        {
            maxX = arrayShapes_[i]->getFrameRect().pos.x + ((arrayShapes_[i]->getFrameRect().width) / 2.0);
        }
        if ((arrayShapes_[i]->getFrameRect().pos.y + ((arrayShapes_[i]->getFrameRect().height) / 2.0)) > maxY)
        {
            maxY = arrayShapes_[i]->getFrameRect().pos.y + ((arrayShapes_[i]->getFrameRect().height) / 2.0);
        }
    }
    return {maxX, maxY};
}

mironenkov::point_t mironenkov::CompositeShape::minXY() const
{
    double minX = arrayShapes_[0]->getFrameRect().pos.x - (arrayShapes_[0]->getFrameRect().width / 2.0);
    double minY = arrayShapes_[0]->getFrameRect().pos.y - (arrayShapes_[0]->getFrameRect().height / 2.0);
    for(int i = 0; i < numberShapes_; i++)
    {
        if ((arrayShapes_[i]->getFrameRect().pos.x - arrayShapes_[i]->getFrameRect().width / 2.0) < minX)
        {
            minX = arrayShapes_[i]->getFrameRect().pos.x - arrayShapes_[i]->getFrameRect().width / 2.0;
        }
        if ((arrayShapes_[i]->getFrameRect().pos.y - arrayShapes_[i]->getFrameRect().height / 2.0) < minY)
        {
            minY = arrayShapes_[i]->getFrameRect().pos.y - arrayShapes_[i]->getFrameRect().height / 2.0;
        }
    }
    return {minX, minY};
}

void mironenkov::CompositeShape::viewName() const
{
    std::cout << "CompositeShape ";
}


