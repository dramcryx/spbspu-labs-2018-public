#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace mironenkov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape(const point_t & pos);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & pos) override;
    void move(const double dx, const double dy) override;
    void scale(const double coeff) override;
    void addShape (const std::shared_ptr< Shape > addedShape);
    void removeShape (const int index);
    void rotate(const double degrees) override;
    void viewName() const override;
  private:
    int numberShapes_;
    std::unique_ptr< std::shared_ptr< Shape > [] > arrayShapes_;
    inline point_t maxXY() const;
    inline point_t minXY() const;
  };
}

#endif

