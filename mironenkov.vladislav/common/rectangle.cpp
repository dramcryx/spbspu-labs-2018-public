#include "rectangle.hpp"
#include <iostream>
#include <cmath>

mironenkov::Rectangle::Rectangle(const point_t & pos, const double width, const double height):
  Shape(pos)
{
  if (width < 0.0)
  {
    throw std::invalid_argument("Width is invalid!");
  }
  else if (height < 0.0)
  {
    throw std::invalid_argument("Height is invalid!");
  }
  width_ = width;
  height_ = height;
};

double mironenkov::Rectangle::getArea() const
{
  return width_ * height_;
};

mironenkov::rectangle_t mironenkov::Rectangle::getFrameRect() const
{
  return rectangle_t{width_, height_, pos_};
};

void mironenkov::Rectangle::move(const double dx, const double dy)
{
    pos_.x += dx;
    pos_.y += dy;
};

void mironenkov::Rectangle::move(const point_t & pos)
{
  pos_ = pos;
};

void mironenkov::Rectangle::scale(const double coeff)
{
  if (coeff <= 0.0)
  {
    throw std::invalid_argument("Scale coeff is invalid!");
  }
  width_ *= coeff;
  height_ *= coeff;
};
void mironenkov::Rectangle::rotate(const double degrees)
{
    angle_ += degrees;
    point_t rtup = {width_ / 2, height_ / 2};
    point_t rtdn = {width_ / 2, - height_ / 2};
    double radns = angle_ * M_PI / 180;
    point_t edge[2] = {{0, 0}, {0, 0}};
    edge[0].x = fabs(rtup.x * cos(radns) - rtup.y * sin(radns));
    edge[0].y = fabs(rtup.y * cos(radns) + rtup.x * sin(radns));
    edge[1].x = fabs(rtdn.x * cos(radns) - rtdn.y * sin(radns));
    edge[1].y = fabs(rtdn.y * cos(radns) + rtdn.x * sin(radns));
    if (edge[0].x < edge[1].x)
    {
        rectw_ = edge[1].x * 2;
    }
    else
    {
        rectw_ = edge[0].x * 2;
    }
    if (edge[0].y < edge[1].y)
    {
        recth_ = edge[1].y * 2;
    }
    else
    {
        recth_ = edge[0].y * 2;
    }
}

void mironenkov::Rectangle::viewName() const
{
    std::cout << "Rectangle ";
}

