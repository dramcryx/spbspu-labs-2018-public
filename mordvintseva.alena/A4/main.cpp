#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace mordvintseva;
using std::cout;
using std::endl;
using std::cerr;

void printMatrix(const Matrix matrix)
{
  for (int i = 0; i < matrix.getLayersNumber(); i++)
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> layer = matrix[i];
    cout << "Layer number: " << i + 1 << endl;
    for (int j = 0; j < matrix.getLayerSize(); j++)
    {
      if (!layer[j])
      {
        break;
      }
      cout << "Shape " << j + 1 << ":" << endl;
      layer[j]->print();
    }
  }
}

int main()
{
  try 
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<Rectangle>(Rectangle(10.0, 2.0, { 5.0, 10.0 }));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle(20.0, 40.0, { 10.0, 5.0 }));
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>(Rectangle(10.0, 0.0, { 20.0, 10.0 }));
    std::shared_ptr<Shape> circle1 = std::make_shared<Circle>(Circle(10.0, { 10.0, 10.0 }));
    std::shared_ptr<Shape> circle2 = std::make_shared<Circle>(Circle(20.0, { 10.0, 10.0 }));

    Matrix matrix;
    matrix.add(rect1);
    matrix.add(rect2);
    matrix.add(rect3);
    matrix.add(circle1);
    matrix.add(circle2);
    
    printMatrix(matrix);
    cout << endl;
    
    return 0;
  } catch (std::invalid_argument& e) {
    cerr << e.what() << endl;
  } catch (std::out_of_range& e) {
    cerr << e.what() << endl;
  }
}
