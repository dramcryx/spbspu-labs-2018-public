#ifndef COMPOSITESHAPE_HPP
#define COMPOSITEsHAPE_HPP

#include <memory>
#include "shape.hpp"
#include "matrix.hpp"

namespace mordvintseva
{
  class CompositeShape : public Shape
  {
    public:
      CompositeShape(const std::shared_ptr<Shape> &shape);
      CompositeShape(const CompositeShape & shape);
      CompositeShape(CompositeShape && shape);
      ~CompositeShape();

      double getArea() const noexcept override;
      rectangle_t getFrameRect() const noexcept override;
      void move(const point_t &point) noexcept override;
      void move(const double dx, const double dy) noexcept override;
      void scale(const double factor) override;
      void rotate(const double angle) noexcept override;
      void print() const noexcept override;

      int getCount() const noexcept;
      void add(const std::shared_ptr<Shape> shape);
      void remove(const int index);
      void clear();
      Matrix split();

      CompositeShape &operator=(CompositeShape &rhs);
      CompositeShape &operator=(CompositeShape &&rhs);
      std::shared_ptr<Shape> operator[](const int index) const;

    private:
      std::unique_ptr<std::shared_ptr<Shape>[]> list_;
      int count_;
  };
}

#endif //COMPOSITESHAPE_HPP
