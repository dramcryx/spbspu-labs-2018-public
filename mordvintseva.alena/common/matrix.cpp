#include "matrix.hpp"

#include <iostream>
#include <stdexcept>
#include <memory>

using namespace mordvintseva;

Matrix::Matrix() noexcept:
  matrix_(),
  layersNumber_(0),
  layerSize_(0)
{

}

Matrix::Matrix(const Matrix &matrix):
  matrix_(new std::shared_ptr<Shape>[matrix.layersNumber_ * matrix.layerSize_]),
  layersNumber_(matrix.layersNumber_),
  layerSize_(matrix.layerSize_)
{
  for (int i = 0; i < layerSize_ * layersNumber_; i++)
  {
    matrix_[i] = matrix.matrix_[i];
  }
}

Matrix::Matrix(Matrix &&matrix):
  matrix_(nullptr),
  layersNumber_(matrix.layersNumber_),
  layerSize_(matrix.layerSize_)
{
  matrix_.swap(matrix.matrix_);
  matrix.layersNumber_ = 0;
  matrix.layerSize_ = 0;
}

std::unique_ptr<std::shared_ptr<Shape>[]> Matrix::operator[] (const int ind) const
{
  if (ind >= layersNumber_)
  {
    throw std::out_of_range("Index out of range");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> layer(new std::shared_ptr<Shape>[layerSize_]);
  std::copy(matrix_.get() + ind * layerSize_, matrix_.get() + (ind + 1) * layerSize_, layer.get());
  return layer;
}

Matrix & Matrix::operator=(const Matrix &rhs)
{
  if (this != &rhs)
  {
    layersNumber_ = rhs.getLayersNumber();
    layerSize_ = rhs.getLayerSize();
    std::unique_ptr<std::shared_ptr<Shape>[]> matrixCopy(new std::shared_ptr<Shape>[rhs.getLayersNumber()]);
    for (int i = 0; i < rhs.getLayersNumber(); i++)
    {
      matrixCopy[i] = rhs.matrix_[i];
    }
    matrix_.swap(matrixCopy);
  }

  return *this;
}

Matrix & Matrix::operator=(Matrix &&rhs)
{
  if (this != &rhs)
  {
    layersNumber_ = rhs.layersNumber_;
    layerSize_ = rhs.layerSize_;
    matrix_.swap(rhs.matrix_);
    rhs.matrix_ = nullptr;
    rhs.layersNumber_ = 0;
    rhs.layerSize_ = 0;
  }

  return *this;
}

void Matrix::add(const std::shared_ptr<Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("invalid_argument");
  }

  if ((layersNumber_ == 0) && (layerSize_ == 0))
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> matrixCopy(new std::shared_ptr<Shape>[1]);
    matrix_.swap(matrixCopy);
    layersNumber_ = layerSize_ = 1;
    matrix_[0] = shape;
    return;
  }

  int i = 0;
  for (; i < layersNumber_; ++i)
  {
    int j = 0;
    for (; j < layerSize_; ++j)
    {
      if (!matrix_[i * layerSize_ + j])
      {
        matrix_[i * layerSize_ + j] = shape;
        return;
      }
      if (checkOverlapping(matrix_[i * layerSize_ + j], shape))
      {
        break;
      }
    }

    if (j == layerSize_)
    {
      std::unique_ptr<std::shared_ptr<Shape>[]> matrix(new std::shared_ptr<Shape>[layersNumber_ * (layerSize_ + 1)]);
      for (int k = 0; k < layersNumber_; ++k)
      {
        for (j = 0; j < layerSize_; ++j)
        {
          matrix[k * layerSize_ + j + k] = matrix_[k * layerSize_ + j];
        }
      }
      layerSize_++;
      matrix[(i + 1) * layerSize_ - 1] = shape;
      matrix_ = std::move(matrix);
      return;
    }
  }

  if (i == layersNumber_)
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> matrix(new std::shared_ptr<Shape>[(layersNumber_ + 1) * layerSize_]);
    for (int k = 0; k < layersNumber_ * layerSize_; k++)
    {
      matrix[k] = matrix_[k];
    }
    matrix[layersNumber_ * layerSize_] = shape;
    layersNumber_++;
    matrix_ = std::move(matrix);
  }
}

void Matrix::clear() noexcept
{
  layersNumber_ = 0;
  layerSize_ = 0;
  matrix_.reset();
}

int Matrix::getLayersNumber() const noexcept
{
  return layersNumber_;
}

int Matrix::getLayerSize() const noexcept
{
  return layerSize_;
}

bool Matrix::checkOverlapping(const std::shared_ptr<Shape> &shapeFirst, const std::shared_ptr<Shape> &shapeSecond)
{
  rectangle_t frameFirst = shapeFirst->getFrameRect();
  rectangle_t frameSecond = shapeSecond->getFrameRect();

  double frameFirstLeft = frameFirst.pos.x - frameFirst.width / 2;
  double frameFirstRight = frameFirst.pos.x + frameFirst.width / 2;
  double frameFirstTop = frameFirst.pos.y + frameFirst.height / 2;
  double frameFirstBottom = frameFirst.pos.y - frameFirst.height / 2;

  double frameSecondLeft = frameSecond.pos.x - frameSecond.width / 2;
  double frameSecondRight = frameSecond.pos.x + frameSecond.width / 2;
  double frameSecondTop = frameSecond.pos.y + frameSecond.height / 2;
  double frameSecondBottom = frameSecond.pos.y - frameSecond.height / 2;

  if ((frameFirstLeft > frameSecondRight) || (frameFirstRight < frameSecondLeft)
    || (frameFirstTop < frameSecondBottom) || (frameFirstBottom > frameSecondTop))
  {
    return false;
  }
  else
  {
    return true;
  }
}
