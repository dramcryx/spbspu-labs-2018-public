#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>
#include "circle.hpp"

Circle::Circle(const point_t &center, double radius) :
  pos_(center)
{
  if (radius > 0.0)
  {
    radius_ = radius;
  }
  else
  {
    std::cerr << "Circle does not exist\n";
  }
}

double Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

rectangle_t Circle::getFrameRect() const
{
  return{ 2 * radius_, 2 * radius_, pos_ };
}

void Circle::move(const point_t &goal)
{
  pos_ = goal;
}

void Circle::move(double dx, double dy)
{
  pos_.x = pos_.x + dx;
  pos_.y = pos_.y + dy;
}
