#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

void example(Shape & Shape2)
{
  std::cout << "Area " << Shape2.getArea() << std::endl;
  std::cout << "Width " << Shape2.getFrameRect().width << ", height " << Shape2.getFrameRect().height << std::endl;

  Shape2.move(10.11, 10.11);
  std::cout << "Position " << Shape2.getFrameRect().pos.x << ", " << Shape2.getFrameRect().pos.y << std::endl;

  Shape2.move({ 20.12,20.12 });
  std::cout << "Position " << Shape2.getFrameRect().pos.x << ", " << Shape2.getFrameRect().pos.y << std::endl;

}

int main()
{
  Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  Circle ci_le({ 70.0,70.0 }, 40.0);
  Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 10.0 });

  example(re_le);
  example(ci_le);
  example(tr_le);

  return 0;
}
