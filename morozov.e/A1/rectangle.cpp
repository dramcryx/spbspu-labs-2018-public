#include <iostream>
#include "rectangle.hpp"

Rectangle::Rectangle(const point_t &center, double width, double height) :
  pos_(center)
{
  if ((width > 0.0) && (height > 0.0))
  {
    width_ = width;
    height_ = height;
  }
  else
  {
    std::cerr << "Rectanhle does not exist\n";
  }
}

double Rectangle::getArea() const
{
  return height_ * width_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return{ width_, height_, pos_ };
}

void Rectangle::move(const point_t &goal)
{
  pos_ = goal;
}

void Rectangle::move(double dx, double dy)
{
  pos_.x = pos_.x + dx;
  pos_.y = pos_.y + dy;
}
