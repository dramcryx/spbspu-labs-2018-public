#include <iostream>
#include "triangle.hpp"

Triangle::Triangle(const point_t &side1, const point_t &side2, const point_t &side3) :
  pos_({ (side1.x + side2.x + side3.x) / 3, (side1.y + side2.y + side3.y) / 3 })
{
  side1_ = side1;
  side2_ = side2;
  side3_ = side3;

  if (getArea() == 0.0)
  {
    std::cerr << "Degenerate triangle\n";
  }
}

double Triangle::getArea() const
{
  return 0.5 * ((side1_.x - side3_.x) * (side2_.y - side3_.y) - (side2_.x - side3_.x) * (side1_.y - side3_.y));
}

rectangle_t Triangle::getFrameRect() const
{
  double max_x = side1_.x;
  double max_y = side1_.y;

  double min_x = max_x;
  double min_y = max_y;

  if (side2_.x < side3_.x)
  {
    if (side3_.x > max_x)
    {
      max_x = side3_.x;
    }

    if (side2_.x < min_x)
    {
      min_x = side2_.x;
    }
  }
  else
  {
    if (side2_.x > max_x)
    {
      max_x = side2_.x;
    }

    if (side3_.x < min_x)
    {
      min_x = side3_.x;
    }
  }

  if (side2_.y < side3_.y)
  {
    if (side3_.y > max_y)
    {
      max_y = side3_.y;
    }

    if (side2_.y < min_y)
    {
      min_y = side2_.y;
    }
  }
  else
  {
    if (side2_.y > max_y)
    {
      max_y = side2_.y;
    }

    if (side3_.y < min_y)
    {
      min_y = side3_.y;
    }
  }

  return{ max_x - min_x, max_y - min_y,{ min_x, min_y } };
}

void Triangle::move(const point_t &purpose)
{
  double dx = purpose.x - pos_.x;
  double dy = purpose.y - pos_.y;

  pos_ = purpose;

  side1_.x = side1_.x + dx;
  side1_.y = side1_.y + dy;

  side2_.x = side2_.x + dx;
  side2_.y = side2_.y + dy;

  side3_.x = side3_.x + dx;
  side3_.y = side3_.y + dy;
}

void Triangle::move(double dx, double dy)
{
  pos_.x = pos_.x + dx;
  pos_.y = pos_.y + dy;

  side1_.x = side1_.x + dx;
  side1_.y = side1_.y + dy;

  side2_.x = side2_.x + dx;
  side2_.y = side2_.y + dy;

  side3_.x = side3_.x + dx;
  side3_.y = side3_.y + dy;
}
