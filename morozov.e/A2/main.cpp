#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include <iostream>

void example(morozov::Shape & Shape2)
{
  std::cout << "Area " << Shape2.getArea() << std::endl;
  std::cout << "Width " << Shape2.getFrameRect().width << ", height " << Shape2.getFrameRect().height << std::endl;

  Shape2.move({ 0.0,0.0 });
  std::cout << "Position " << Shape2.getFrameRect().pos.x << ", " << Shape2.getFrameRect().pos.y << std::endl;

  Shape2.move({ 20.12,20.12 });
  std::cout << "Position " << Shape2.getFrameRect().pos.x << ", " << Shape2.getFrameRect().pos.y << std::endl;

  Shape2.scale(13.0);
  std::cout << "Area " << Shape2.getArea() << std::endl;
  std::cout << "Width " << Shape2.getFrameRect().width << ", height " << Shape2.getFrameRect().height << std::endl << std::endl;
}

int main()
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  example(re_le);
  example(ci_le);
  example(tr_le);

  return 0;
}
