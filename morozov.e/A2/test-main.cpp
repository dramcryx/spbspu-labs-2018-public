//#define BOOST_TEST_MODULE boost_test
#define _USE_MATH_DEFINES
#define BOOST_TEST_MODULE boost_test
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

const double EPSILON = 0.00001;

BOOST_AUTO_TEST_SUITE(TestMoveToPoint)
BOOST_AUTO_TEST_CASE(MoveRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  re_le.move({ 0.0,0.0 });
  BOOST_CHECK_CLOSE(re_le.getArea(), 10.0 * 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getWidth(), 10.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getHeight(), 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().y, 0.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  ci_le.move({ 0.0,0.0 });
  BOOST_CHECK_CLOSE(ci_le.getArea(), M_PI * 1600.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getRadius(), 40.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().y, 0.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveTriangle)
{
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });
  tr_le.move({ 0.0,0.0 });
  BOOST_CHECK_CLOSE(tr_le.getArea(), 9, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide1(), sqrt(18), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide2(), 6, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide3(), sqrt(18), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().y, 0.0, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestMovingAxial)
BOOST_AUTO_TEST_CASE(MoveRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  re_le.move(2.0, 0.0);
  BOOST_CHECK_CLOSE(re_le.getArea(), 10.0 * 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getWidth(), 10.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getHeight(), 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().x, 72.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().y, 70.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  ci_le.move(2.0, 0.0);;
  BOOST_CHECK_CLOSE(ci_le.getArea(), M_PI * 1600.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getRadius(), 40.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().x, 72.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().y, 70.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveTriangle)
{
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });
  double x_before, y_before;
  x_before = tr_le.getPos().x;
  y_before = tr_le.getPos().y;
  tr_le.move(2.0, 0.0);
  BOOST_CHECK_CLOSE(tr_le.getArea(), std::abs(0.5 * (-8.0 * (-12.0 - 4.0) + 4.0 * (4.0 + 3.0) + 8.0 * (-3.0 + 12.0))), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide1(), sqrt(144.0 + 81.0), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide2(), sqrt((256.0) + (49.0)), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide3(), sqrt((16.0) + (256.0)), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().x, (x_before + 2), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().y, y_before, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(ScaleArea)
BOOST_AUTO_TEST_CASE(AreaOfRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  double ar = re_le.getArea();
  re_le.scale(2.0);
  BOOST_CHECK_CLOSE(re_le.getArea(), ar * 4.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(AreaOfCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  double ar = ci_le.getArea();
  ci_le.scale(3.0);
  BOOST_CHECK_CLOSE(ci_le.getArea(), ar * 9.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(AreaOfTriangle)
{
  morozov::Triangle tr_le({ 1.0, 2.0 }, { 4.0, 2.0 }, { 8.0, 4.0 });
  double ar = tr_le.getArea();
  tr_le.scale(4.0);
  BOOST_CHECK_CLOSE(tr_le.getArea(), ar * 16.0, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(SomethingWrong)
BOOST_AUTO_TEST_CASE(withRectangle)
{
  BOOST_CHECK_THROW(morozov::Rectangle re_le({ 10.0,20.0 }, -1.0, -1.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(withCircle)
{
  BOOST_CHECK_THROW(morozov::Circle ce_le({ -4.0,1.0 }, -5.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(withTriangle)
{
  BOOST_CHECK_THROW(morozov::Triangle tr_le({ 0.0, 0.0 }, { 0.0, 0.0 }, { 0.0, 0.0 }), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scaleForRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  BOOST_CHECK_THROW(re_le.scale(-10.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scaleForCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  BOOST_CHECK_THROW(ci_le.scale(-20.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scaleForTriangle)
{
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });
  BOOST_CHECK_THROW(tr_le.scale(0.0), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()
