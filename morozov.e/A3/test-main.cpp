//#define BOOST_TEST_MODULE boost_test
#define _USE_MATH_DEFINES
#define BOOST_TEST_MODULE boost_test
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

const double EPSILON = 0.00001;

BOOST_AUTO_TEST_SUITE(TestMoveToPoint)
BOOST_AUTO_TEST_CASE(MoveRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  re_le.move({ 0.0,0.0 });
  BOOST_CHECK_CLOSE(re_le.getArea(), 10.0 * 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getWidth(), 10.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getHeight(), 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().y, 0.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  ci_le.move({ 0.0,0.0 });
  BOOST_CHECK_CLOSE(ci_le.getArea(), M_PI * 1600.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getRadius(), 40.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().y, 0.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveTriangle)
{
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });
  tr_le.move({ 0.0,0.0 });
  BOOST_CHECK_CLOSE(tr_le.getArea(), 9, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide1(), sqrt(18), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide2(), 6, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide3(), sqrt(18), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().y, 0.0, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestMovingAxial)
BOOST_AUTO_TEST_CASE(MoveRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  re_le.move(2.0, 0.0);
  BOOST_CHECK_CLOSE(re_le.getArea(), 10.0 * 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getWidth(), 10.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getHeight(), 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().x, 72.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().y, 70.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  ci_le.move(2.0, 0.0);;
  BOOST_CHECK_CLOSE(ci_le.getArea(), M_PI * 1600.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getRadius(), 40.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().x, 72.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().y, 70.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveTriangle)
{
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });
  double x_before, y_before;
  x_before = tr_le.getPos().x;
  y_before = tr_le.getPos().y;
  tr_le.move(2.0, 0.0);
  BOOST_CHECK_CLOSE(tr_le.getArea(), std::abs(0.5 * (-8.0 * (-12.0 - 4.0) + 4.0 * (4.0 + 3.0) + 8.0 * (-3.0 + 12.0))), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide1(), sqrt(144.0 + 81.0), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide2(), sqrt((256.0) + (49.0)), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide3(), sqrt((16.0) + (256.0)), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().x, (x_before + 2), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().y, y_before, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(ScaleArea)
BOOST_AUTO_TEST_CASE(AreaOfRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  double ar = re_le.getArea();
  re_le.scale(2.0);
  BOOST_CHECK_CLOSE(re_le.getArea(), ar * 4.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(AreaOfCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  double ar = ci_le.getArea();
  ci_le.scale(3.0);
  BOOST_CHECK_CLOSE(ci_le.getArea(), ar * 9.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(AreaOfTriangle)
{
  morozov::Triangle tr_le({ 1.0, 2.0 }, { 4.0, 2.0 }, { 8.0, 4.0 });
  double ar = tr_le.getArea();
  tr_le.scale(4.0);
  BOOST_CHECK_CLOSE(tr_le.getArea(), ar * 16.0, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(SomethingWrong)
BOOST_AUTO_TEST_CASE(withRectangle)
{
  BOOST_CHECK_THROW(morozov::Rectangle re_le({ 10.0,20.0 }, -1.0, -1.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(withCircle)
{
  BOOST_CHECK_THROW(morozov::Circle ce_le({ -4.0,1.0 }, -5.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(withTriangle)
{
  BOOST_CHECK_THROW(morozov::Triangle tr_le({ 0.0, 0.0 }, { 0.0, 0.0 }, { 0.0, 0.0 }), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scaleForRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  BOOST_CHECK_THROW(re_le.scale(-10.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scaleForCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  BOOST_CHECK_THROW(ci_le.scale(-20.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scaleForTriangle)
{
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });
  BOOST_CHECK_THROW(tr_le.scale(0.0), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeTest)

BOOST_AUTO_TEST_CASE(ConstructorCompSh)
{
  morozov::CompositeShape compShape;
  BOOST_CHECK_EQUAL(compShape.getAmount(), size_t(0));
  BOOST_CHECK_CLOSE(compShape.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getPos().y, 0.0, EPSILON);
}

BOOST_AUTO_TEST_CASE(CopyConstrCompSh)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 1.0, 1.0 }, 2);
  morozov::Rectangle re_le({ 10.0, 10.0 }, 10.0, 15.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::CompositeShape compShape(Shape1);

  BOOST_CHECK_EQUAL(compShape.getAmount(), Shape1.getAmount());
  BOOST_CHECK_CLOSE(compShape.getPos().x, Shape1.getPos().x, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getPos().y, Shape1.getPos().y, EPSILON);
  BOOST_CHECK_EQUAL(compShape[0], Shape1[0]);
  BOOST_CHECK_EQUAL(compShape[1], Shape1[1]);
  BOOST_CHECK_EQUAL(compShape[2], Shape1[2]);
}

BOOST_AUTO_TEST_CASE(MoveConstrCompSh)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 1.0, 1.0 }, 5);
  morozov::Rectangle re_le({ 10.0, 10.0 }, 10.0, 15.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::point_t testPos = Shape1.getPos();

  morozov::CompositeShape compShape(std::move(Shape1));

  BOOST_CHECK_EQUAL(compShape.getAmount(), size_t(3));
  BOOST_CHECK_EQUAL(compShape.getPos().x, testPos.x);
  BOOST_CHECK_EQUAL(compShape.getPos().y, testPos.y);
  BOOST_CHECK_EQUAL(compShape[0], &ci_le);
  BOOST_CHECK_EQUAL(compShape[1], &re_le);
  BOOST_CHECK_EQUAL(compShape[2], &tr_le);

  BOOST_CHECK_EQUAL(Shape1.getAmount(), size_t(0));
  BOOST_CHECK_CLOSE(Shape1.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, 0.0, EPSILON);
}

BOOST_AUTO_TEST_CASE(OperatorSquareBrackets)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  BOOST_CHECK_EQUAL(&ci_le, Shape1[0]);
  BOOST_CHECK_EQUAL(&re_le, Shape1[1]);
  BOOST_CHECK_EQUAL(&tr_le, Shape1[2]);
  BOOST_CHECK_THROW(Shape1[3], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(OperatorCopy)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 1.0, 1.0 }, 5.0);
  morozov::Rectangle re_le({ 10.0, 10.0 }, 10.0, 15.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::CompositeShape compShape;

  compShape = Shape1;

  BOOST_CHECK_EQUAL(compShape.getAmount(), Shape1.getAmount());
  BOOST_CHECK_CLOSE(compShape.getPos().x, Shape1.getPos().x, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getPos().y, Shape1.getPos().y, EPSILON);
  BOOST_CHECK_EQUAL(compShape[0], Shape1[0]);
  BOOST_CHECK_EQUAL(compShape[1], Shape1[1]);
  BOOST_CHECK_EQUAL(compShape[2], Shape1[2]);
}

BOOST_AUTO_TEST_CASE(OperatorMove)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::point_t testPos = Shape1.getPos();
  morozov::CompositeShape compShape;

  compShape = std::move(Shape1);

  BOOST_CHECK_EQUAL(compShape.getAmount(), size_t(3));
  BOOST_CHECK_CLOSE(compShape.getPos().x, testPos.x, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getPos().y, testPos.y, EPSILON);
  BOOST_CHECK_EQUAL(compShape[0], &ci_le);
  BOOST_CHECK_EQUAL(compShape[1], &re_le);
  BOOST_CHECK_EQUAL(compShape[2], &tr_le);

  BOOST_CHECK_EQUAL(Shape1.getAmount(), size_t(0));
  BOOST_CHECK_CLOSE(Shape1.getPos().x, -6, EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, -8, EPSILON);
}

BOOST_AUTO_TEST_CASE(pushNewCompSh)
{
  morozov::CompositeShape Shape1;
  morozov::Shape * Shape2 = nullptr;

  BOOST_CHECK_THROW(Shape1.pushNew(Shape2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestGetInfo)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  BOOST_CHECK_CLOSE(Shape1.getPos().x, -6, EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, -8, EPSILON);
  BOOST_CHECK_EQUAL(Shape1.getAmount(), size_t(3));
}

BOOST_AUTO_TEST_CASE(MoveTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::point_t posBefore = Shape1.getPos();
  const double areaBefore = Shape1.getArea();
  const double frameWBefore = Shape1.getFrameRect().width;
  const double frameHBefore = Shape1.getFrameRect().height;

  Shape1.move(11.0, 11.0);

  BOOST_CHECK_CLOSE(Shape1.getPos().x, (posBefore.x + 11.0), EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, (posBefore.y + 11.0), EPSILON);

  double area = Shape1.getArea();
  double frameW = Shape1.getFrameRect().width;
  double frameH = Shape1.getFrameRect().height;

  BOOST_CHECK_CLOSE(frameWBefore, frameW, EPSILON);
  BOOST_CHECK_CLOSE(frameHBefore, frameH, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, area, EPSILON);

  Shape1.move(-11.0, -11.0);

  BOOST_CHECK_CLOSE(Shape1.getPos().x, posBefore.x, EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, posBefore.y, EPSILON);

  frameW = Shape1.getFrameRect().width;
  frameH = Shape1.getFrameRect().height;
  area = Shape1.getArea();

  BOOST_CHECK_CLOSE(frameWBefore, frameW, EPSILON);
  BOOST_CHECK_CLOSE(frameHBefore, frameH, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, area, EPSILON);
}

BOOST_AUTO_TEST_CASE(MoveToPointTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::point_t posBefore = Shape1.getPos();
  const double frameWBefore = Shape1.getFrameRect().width;
  const double frameHBefore = Shape1.getFrameRect().height;
  const double areaBefore = Shape1.getArea();

  morozov::point_t newPos{ 6.0, 8.0 };
  Shape1.move(newPos);

  const double dx = newPos.x - posBefore.x;
  const double dy = newPos.y - posBefore.y;
  BOOST_CHECK_CLOSE(Shape1.getPos().x, (posBefore.x + dx), EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, (posBefore.y + dy), EPSILON);

  double frameW = Shape1.getFrameRect().width;
  double frameH = Shape1.getFrameRect().height;
  double area = Shape1.getArea();

  BOOST_CHECK_CLOSE(frameWBefore, frameW, EPSILON);
  BOOST_CHECK_CLOSE(frameHBefore, frameH, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, area, EPSILON);

  Shape1.move(posBefore);

  BOOST_CHECK_CLOSE(Shape1.getPos().x, posBefore.x, EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, posBefore.y, EPSILON);

  frameW = Shape1.getFrameRect().width;
  frameH = Shape1.getFrameRect().height;
  area = Shape1.getArea();

  BOOST_CHECK_CLOSE(frameWBefore, frameW, EPSILON);
  BOOST_CHECK_CLOSE(frameHBefore, frameH, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, area, EPSILON);
}

BOOST_AUTO_TEST_CASE(scaleInvalidArgumentTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 1.0, 1.0 }, 5.0);
  morozov::Rectangle re_le({ 10.0, 10.0 }, 10.0, 15.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  BOOST_CHECK_THROW(Shape1.scale(-10.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(PositionAfterScalingTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  Shape1.scale(2.0);

  BOOST_CHECK_CLOSE(Shape1[0]->getPos().x, -6.0, EPSILON);
  BOOST_CHECK_CLOSE(Shape1[0]->getPos().y, 2.25, EPSILON);

  BOOST_CHECK_CLOSE(Shape1[1]->getPos().x, 4.0, EPSILON);
  BOOST_CHECK_CLOSE(Shape1[1]->getPos().y, 2.25, EPSILON);

  BOOST_CHECK_CLOSE(Shape1[2]->getPos().x, 8, EPSILON);
  BOOST_CHECK_CLOSE(Shape1[2]->getPos().y, 2.25, EPSILON);
}

BOOST_AUTO_TEST_CASE(getAreaTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 0.0, 0.0 }, 1.0);
  morozov::Rectangle re_le({ 0.0, 0.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  const double area = M_PI + 25;
  BOOST_CHECK_CLOSE(Shape1.getArea(), area, EPSILON);
}

BOOST_AUTO_TEST_CASE(AreaAfterAcalingTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 1.0, 1.0 }, 5.0);
  morozov::Rectangle re_le({ 10.0, 10.0 }, 10.0, 15.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  const double areaBefore = Shape1.getArea();
  const double k = 2;
  Shape1.scale(k);
  const double area = Shape1.getArea();

  BOOST_CHECK_CLOSE(areaBefore*k*k, area, EPSILON);
}

BOOST_AUTO_TEST_CASE(ProportionAfterScalingTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::point_t posBefore = Shape1.getPos();
  const double widthBefore = Shape1.getFrameRect().width;
  const double heightBefore = Shape1.getFrameRect().height;
  const double k = 7;

  Shape1.scale(k);

  morozov::point_t pos = Shape1.getPos();
  const double width = Shape1.getFrameRect().width;
  const double height = Shape1.getFrameRect().height;

  BOOST_CHECK_CLOSE(widthBefore, width / k, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, height / k, EPSILON);
  BOOST_CHECK_CLOSE(posBefore.x, pos.x, EPSILON);
  BOOST_CHECK_CLOSE(posBefore.y, pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(EmptyFrameTest)
{
  morozov::CompositeShape Shape1;
  BOOST_CHECK_THROW(Shape1.getFrameRect(), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
