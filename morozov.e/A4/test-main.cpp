#define BOOST_TEST_MODULE boost_test
#define _USE_MATH_DEFINES
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "polygon.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

const double EPSILON = 0.00001;

BOOST_AUTO_TEST_SUITE(TestMoveToPoint)
BOOST_AUTO_TEST_CASE(MoveRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  re_le.move({ 0.0,0.0 });
  BOOST_CHECK_CLOSE(re_le.getArea(), 10.0 * 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getWidth(), 10.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getHeight(), 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().y, 0.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  ci_le.move({ 0.0,0.0 });
  BOOST_CHECK_CLOSE(ci_le.getArea(), M_PI * 1600.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getRadius(), 40.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().y, 0.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveTriangle)
{
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });
  tr_le.move({ 0.0,0.0 });
  BOOST_CHECK_CLOSE(tr_le.getArea(), 9, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide1(), sqrt(18), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide2(), 6, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide3(), sqrt(18), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().y, 0.0, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TestMovingAxial)
BOOST_AUTO_TEST_CASE(MoveRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  re_le.move(2.0, 0.0);
  BOOST_CHECK_CLOSE(re_le.getArea(), 10.0 * 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getWidth(), 10.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getHeight(), 30.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().x, 72.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getPos().y, 70.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  ci_le.move(2.0, 0.0);;
  BOOST_CHECK_CLOSE(ci_le.getArea(), M_PI * 1600.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getRadius(), 40.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().x, 72.0, EPSILON);
  BOOST_CHECK_CLOSE(ci_le.getPos().y, 70.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(MoveTriangle)
{
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });
  double x_before, y_before;
  x_before = tr_le.getPos().x;
  y_before = tr_le.getPos().y;
  tr_le.move(2.0, 0.0);
  BOOST_CHECK_CLOSE(tr_le.getArea(), std::abs(0.5 * (-8.0 * (-12.0 - 4.0) + 4.0 * (4.0 + 3.0) + 8.0 * (-3.0 + 12.0))), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide1(), sqrt(144.0 + 81.0), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide2(), sqrt((256.0) + (49.0)), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getSide3(), sqrt((16.0) + (256.0)), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().x, (x_before + 2), EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getPos().y, y_before, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(ScaleArea)
BOOST_AUTO_TEST_CASE(AreaOfRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  double ar = re_le.getArea();
  re_le.scale(2.0);
  BOOST_CHECK_CLOSE(re_le.getArea(), ar * 4.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(AreaOfCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  double ar = ci_le.getArea();
  ci_le.scale(3.0);
  BOOST_CHECK_CLOSE(ci_le.getArea(), ar * 9.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(AreaOfTriangle)
{
  morozov::Triangle tr_le({ 1.0, 2.0 }, { 4.0, 2.0 }, { 8.0, 4.0 });
  double ar = tr_le.getArea();
  tr_le.scale(4.0);
  BOOST_CHECK_CLOSE(tr_le.getArea(), ar * 16.0, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(SomethingWrong)
BOOST_AUTO_TEST_CASE(withRectangle)
{
  BOOST_CHECK_THROW(morozov::Rectangle re_le({ 10.0,20.0 }, -1.0, -1.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(withCircle)
{
  BOOST_CHECK_THROW(morozov::Circle ce_le({ -4.0,1.0 }, -5.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(withTriangle)
{
  BOOST_CHECK_THROW(morozov::Triangle tr_le({ 0.0, 0.0 }, { 0.0, 0.0 }, { 0.0, 0.0 }), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scaleForRectangle)
{
  morozov::Rectangle re_le({ 70.0,70.0 }, 10.0, 30.0);
  BOOST_CHECK_THROW(re_le.scale(-10.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scaleForCircle)
{
  morozov::Circle ci_le({ 70.0,70.0 }, 40.0);
  BOOST_CHECK_THROW(ci_le.scale(-20.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(scaleForTriangle)
{
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });
  BOOST_CHECK_THROW(tr_le.scale(0.0), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeTest)

BOOST_AUTO_TEST_CASE(ConstructorCompSh)
{
  morozov::CompositeShape compShape;
  BOOST_CHECK_EQUAL(compShape.getAmount(), size_t(0));
  BOOST_CHECK_CLOSE(compShape.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getPos().y, 0.0, EPSILON);
}

BOOST_AUTO_TEST_CASE(CopyConstrCompSh)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 1.0, 1.0 }, 2);
  morozov::Rectangle re_le({ 10.0, 10.0 }, 10.0, 15.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::CompositeShape compShape(Shape1);

  BOOST_CHECK_EQUAL(compShape.getAmount(), Shape1.getAmount());
  BOOST_CHECK_CLOSE(compShape.getPos().x, Shape1.getPos().x, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getPos().y, Shape1.getPos().y, EPSILON);
  BOOST_CHECK_EQUAL(compShape[0], Shape1[0]);
  BOOST_CHECK_EQUAL(compShape[1], Shape1[1]);
  BOOST_CHECK_EQUAL(compShape[2], Shape1[2]);
}

BOOST_AUTO_TEST_CASE(MoveConstrCompSh)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 1.0, 1.0 }, 5);
  morozov::Rectangle re_le({ 10.0, 10.0 }, 10.0, 15.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::point_t testPos = Shape1.getPos();

  morozov::CompositeShape compShape(std::move(Shape1));

  BOOST_CHECK_EQUAL(compShape.getAmount(), size_t(3));
  BOOST_CHECK_EQUAL(compShape.getPos().x, testPos.x);
  BOOST_CHECK_EQUAL(compShape.getPos().y, testPos.y);
  BOOST_CHECK_EQUAL(compShape[0], &ci_le);
  BOOST_CHECK_EQUAL(compShape[1], &re_le);
  BOOST_CHECK_EQUAL(compShape[2], &tr_le);

  BOOST_CHECK_EQUAL(Shape1.getAmount(), size_t(0));
  BOOST_CHECK_CLOSE(Shape1.getPos().x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, 0.0, EPSILON);
}

BOOST_AUTO_TEST_CASE(OperatorSquareBrackets)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  BOOST_CHECK_EQUAL(&ci_le, Shape1[0]);
  BOOST_CHECK_EQUAL(&re_le, Shape1[1]);
  BOOST_CHECK_EQUAL(&tr_le, Shape1[2]);
  BOOST_CHECK_THROW(Shape1[3], std::out_of_range);
}

BOOST_AUTO_TEST_CASE(OperatorCopy)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 1.0, 1.0 }, 5.0);
  morozov::Rectangle re_le({ 10.0, 10.0 }, 10.0, 15.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::CompositeShape compShape;

  compShape = Shape1;

  BOOST_CHECK_EQUAL(compShape.getAmount(), Shape1.getAmount());
  BOOST_CHECK_CLOSE(compShape.getPos().x, Shape1.getPos().x, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getPos().y, Shape1.getPos().y, EPSILON);
  BOOST_CHECK_EQUAL(compShape[0], Shape1[0]);
  BOOST_CHECK_EQUAL(compShape[1], Shape1[1]);
  BOOST_CHECK_EQUAL(compShape[2], Shape1[2]);
}

BOOST_AUTO_TEST_CASE(OperatorMove)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::point_t testPos = Shape1.getPos();
  morozov::CompositeShape compShape;

  compShape = std::move(Shape1);

  BOOST_CHECK_EQUAL(compShape.getAmount(), size_t(3));
  BOOST_CHECK_CLOSE(compShape.getPos().x, testPos.x, EPSILON);
  BOOST_CHECK_CLOSE(compShape.getPos().y, testPos.y, EPSILON);
  BOOST_CHECK_EQUAL(compShape[0], &ci_le);
  BOOST_CHECK_EQUAL(compShape[1], &re_le);
  BOOST_CHECK_EQUAL(compShape[2], &tr_le);

  BOOST_CHECK_EQUAL(Shape1.getAmount(), size_t(0));
  BOOST_CHECK_CLOSE(Shape1.getPos().x, -6, EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, -8, EPSILON);
}

BOOST_AUTO_TEST_CASE(pushNewCompSh)
{
  morozov::CompositeShape Shape1;
  morozov::Shape * Shape2 = nullptr;

  BOOST_CHECK_THROW(Shape1.pushNew(Shape2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(TestGetInfo)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  BOOST_CHECK_CLOSE(Shape1.getPos().x, -6, EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, -8, EPSILON);
  BOOST_CHECK_EQUAL(Shape1.getAmount(), size_t(3));
}

BOOST_AUTO_TEST_CASE(MoveTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::point_t posBefore = Shape1.getPos();
  const double areaBefore = Shape1.getArea();
  const double frameWBefore = Shape1.getFrameRect().width;
  const double frameHBefore = Shape1.getFrameRect().height;

  Shape1.move(11.0, 11.0);

  BOOST_CHECK_CLOSE(Shape1.getPos().x, (posBefore.x + 11.0), EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, (posBefore.y + 11.0), EPSILON);

  double area = Shape1.getArea();
  double frameW = Shape1.getFrameRect().width;
  double frameH = Shape1.getFrameRect().height;

  BOOST_CHECK_CLOSE(frameWBefore, frameW, EPSILON);
  BOOST_CHECK_CLOSE(frameHBefore, frameH, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, area, EPSILON);

  Shape1.move(-11.0, -11.0);

  BOOST_CHECK_CLOSE(Shape1.getPos().x, posBefore.x, EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, posBefore.y, EPSILON);

  frameW = Shape1.getFrameRect().width;
  frameH = Shape1.getFrameRect().height;
  area = Shape1.getArea();

  BOOST_CHECK_CLOSE(frameWBefore, frameW, EPSILON);
  BOOST_CHECK_CLOSE(frameHBefore, frameH, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, area, EPSILON);
}

BOOST_AUTO_TEST_CASE(MoveToPointTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::point_t posBefore = Shape1.getPos();
  const double frameWBefore = Shape1.getFrameRect().width;
  const double frameHBefore = Shape1.getFrameRect().height;
  const double areaBefore = Shape1.getArea();

  morozov::point_t newPos{ 6.0, 8.0 };
  Shape1.move(newPos);

  const double dx = newPos.x - posBefore.x;
  const double dy = newPos.y - posBefore.y;
  BOOST_CHECK_CLOSE(Shape1.getPos().x, (posBefore.x + dx), EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, (posBefore.y + dy), EPSILON);

  double frameW = Shape1.getFrameRect().width;
  double frameH = Shape1.getFrameRect().height;
  double area = Shape1.getArea();

  BOOST_CHECK_CLOSE(frameWBefore, frameW, EPSILON);
  BOOST_CHECK_CLOSE(frameHBefore, frameH, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, area, EPSILON);

  Shape1.move(posBefore);

  BOOST_CHECK_CLOSE(Shape1.getPos().x, posBefore.x, EPSILON);
  BOOST_CHECK_CLOSE(Shape1.getPos().y, posBefore.y, EPSILON);

  frameW = Shape1.getFrameRect().width;
  frameH = Shape1.getFrameRect().height;
  area = Shape1.getArea();

  BOOST_CHECK_CLOSE(frameWBefore, frameW, EPSILON);
  BOOST_CHECK_CLOSE(frameHBefore, frameH, EPSILON);
  BOOST_CHECK_CLOSE(areaBefore, area, EPSILON);
}

BOOST_AUTO_TEST_CASE(scaleInvalidArgumentTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 1.0, 1.0 }, 5.0);
  morozov::Rectangle re_le({ 10.0, 10.0 }, 10.0, 15.0);
  morozov::Triangle tr_le({ -8.0, -3.0 }, { 4.0, -12.0 }, { 8.0, 4.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  BOOST_CHECK_THROW(Shape1.scale(-10.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(PositionAfterScalingTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  Shape1.scale(2.0);

  BOOST_CHECK_CLOSE(Shape1[0]->getPos().x, -6.0, EPSILON);
  BOOST_CHECK_CLOSE(Shape1[0]->getPos().y, 2.25, EPSILON);

  BOOST_CHECK_CLOSE(Shape1[1]->getPos().x, 4.0, EPSILON);
  BOOST_CHECK_CLOSE(Shape1[1]->getPos().y, 2.25, EPSILON);

  BOOST_CHECK_CLOSE(Shape1[2]->getPos().x, 8, EPSILON);
  BOOST_CHECK_CLOSE(Shape1[2]->getPos().y, 2.25, EPSILON);
}

BOOST_AUTO_TEST_CASE(getAreaTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 0.0, 0.0 }, 1.0);
  morozov::Rectangle re_le({ 0.0, 0.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  const double area = M_PI + 25;
  BOOST_CHECK_CLOSE(Shape1.getArea(), area, EPSILON);
}

BOOST_AUTO_TEST_CASE(AreaAfterAcalingTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ 1.0, 1.0 }, 5.0);
  morozov::Rectangle re_le({ 10.0, 10.0 }, 10.0, 15.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  const double areaBefore = Shape1.getArea();
  const double k = 2;
  Shape1.scale(k);
  const double area = Shape1.getArea();

  BOOST_CHECK_CLOSE(areaBefore*k*k, area, EPSILON);
}

BOOST_AUTO_TEST_CASE(ProportionAfterScalingTest)
{
  morozov::CompositeShape Shape1;

  morozov::Circle ci_le({ -3.0, 2.0 }, 1.0);
  morozov::Rectangle re_le({ 2.0, 2.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  Shape1.pushNew(&ci_le);
  Shape1.pushNew(&re_le);
  Shape1.pushNew(&tr_le);

  morozov::point_t posBefore = Shape1.getPos();
  const double widthBefore = Shape1.getFrameRect().width;
  const double heightBefore = Shape1.getFrameRect().height;
  const double k = 7;

  Shape1.scale(k);

  morozov::point_t pos = Shape1.getPos();
  const double width = Shape1.getFrameRect().width;
  const double height = Shape1.getFrameRect().height;

  BOOST_CHECK_CLOSE(widthBefore, width / k, EPSILON);
  BOOST_CHECK_CLOSE(heightBefore, height / k, EPSILON);
  BOOST_CHECK_CLOSE(posBefore.x, pos.x, EPSILON);
  BOOST_CHECK_CLOSE(posBefore.y, pos.y, EPSILON);
}

BOOST_AUTO_TEST_CASE(EmptyFrameTest)
{
  morozov::CompositeShape Shape1;
  BOOST_CHECK_THROW(Shape1.getFrameRect(), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTest)

BOOST_AUTO_TEST_CASE(TestMatrixOverlapping)
{
  morozov::Circle ci_le1({ 0.0, 0.0 }, 2.0);
  morozov::Circle ci_le2({ 30.0, 30.0 }, 3.0);

  morozov::Rectangle re_le1({ 15.0, 15.0 }, 4.0, 5.0);
  morozov::Rectangle re_le2({ 0.0, 0.0 }, 4.0, 5.0);

  morozov::Triangle tr_le1({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });
  morozov::Triangle tr_le2({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  morozov::CompositeShape compShape;
  compShape.pushNew(&ci_le1);
  compShape.pushNew(&re_le1);
  compShape.pushNew(&tr_le1);

  morozov::Matrix mat_ix{ &ci_le1, &re_le1, &tr_le1 };
  size_t size = 3;

  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 0), &ci_le1);
  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 1), &re_le1);

  mat_ix.addShape(&compShape);
  BOOST_CHECK_EQUAL(mat_ix.getShape(2, 0), &compShape);

  mat_ix.addShape(&re_le2);
  BOOST_CHECK_EQUAL(mat_ix.getShape(2, 1), &re_le2);

  mat_ix.addShape(&ci_le2);
  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 2), &ci_le2);

  mat_ix.addShape(&tr_le2);
  BOOST_CHECK_EQUAL(mat_ix.getShape(3, 1), &tr_le2);

  BOOST_CHECK_EQUAL(mat_ix.getLayerSize(0), size);
}

BOOST_AUTO_TEST_CASE(TestCreateMatrixOverlapping)
{ 
  morozov::Rectangle re_le1({ 15.0, 15.0 }, 4.0, 5.0);
  morozov::Rectangle re_le2({ 0.0, 0.0 }, 4.0, 5.0);

  morozov::Circle ci_le1({ 0.0, 0.0 }, 2.0);
  morozov::Circle ci_le2({ 30.0, 30.0 }, 3.0);

  morozov::Triangle tr_le1({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });
  morozov::Triangle tr_le2({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  morozov::CompositeShape compShape;

  compShape.pushNew(&ci_le1);
  compShape.pushNew(&re_le1);
  compShape.pushNew(&re_le2);
  compShape.pushNew(&ci_le2);
  compShape.pushNew(&tr_le1);
  compShape.pushNew(&tr_le2);

  morozov::Matrix mat_ix = mat_ix.createMatrix(compShape);
  morozov::Matrix mat_ix1{ &ci_le1, &re_le1, &re_le2, &ci_le2, &tr_le1, &tr_le2 };

  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 0), mat_ix1.getShape(0, 0));
  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 1), mat_ix1.getShape(0, 1));
  BOOST_CHECK_EQUAL(mat_ix.getShape(1, 0), mat_ix1.getShape(1, 0));
  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 2), mat_ix1.getShape(0, 2));
  BOOST_CHECK_EQUAL(mat_ix.getShape(2, 0), mat_ix1.getShape(2, 0));
  BOOST_CHECK_EQUAL(mat_ix.getShape(2, 1), mat_ix1.getShape(2, 1));
}

BOOST_AUTO_TEST_CASE(TestCreateMatrixAfterRotation)
{
  morozov::Circle ci_le({ 1.0, 1.0 }, 2.0);
  morozov::Rectangle re_le({ 4.0, 4.0 }, 4.0, 2.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  morozov::CompositeShape compShape;
  compShape.pushNew(&ci_le);
  compShape.pushNew(&re_le);
  compShape.pushNew(&tr_le);

  size_t size = 2;

  morozov::Matrix mat_ix = mat_ix.createMatrix(compShape);

  BOOST_CHECK_EQUAL(mat_ix.getLayerNum(), size);

  compShape.rotate(45.0);
  mat_ix = mat_ix.createMatrix(compShape);

  BOOST_CHECK_EQUAL(mat_ix.getLayerNum(), size);
}

BOOST_AUTO_TEST_CASE(TestMatrixOperatorCopy)
{
  morozov::Circle ci_le({ 0.0, 0.0 }, 2.0);
  morozov::Rectangle re_le({ 15.0, 15.0 }, 4.0, 5.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  morozov::Matrix mat_ix;
  morozov::Matrix mat_ix1{ &ci_le,&re_le, &tr_le };
  mat_ix = mat_ix1;

  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 0), mat_ix1.getShape(0, 0));
  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 0), &ci_le);

  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 1), mat_ix1.getShape(0, 1));
  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 1), &re_le);

  BOOST_CHECK_EQUAL(mat_ix.getShape(1, 0), mat_ix1.getShape(1, 0));
  BOOST_CHECK_EQUAL(mat_ix.getShape(1, 0), &tr_le);
}

BOOST_AUTO_TEST_CASE(TestCopyConstructor)
{
  morozov::Circle ci_le({ 0.0, 0.0 }, 2.0);
  morozov::Rectangle re_le({ 15.0, 15.0 }, 4.0, 5.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });

  morozov::Matrix mat_ix{ &ci_le,&re_le, &tr_le };
  morozov::Matrix mat_ix1(mat_ix);

  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 0), mat_ix1.getShape(0, 0));
  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 0), &ci_le);

  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 1), mat_ix1.getShape(0, 1));
  BOOST_CHECK_EQUAL(mat_ix.getShape(0, 1), &re_le);

  BOOST_CHECK_EQUAL(mat_ix.getShape(1, 0), mat_ix1.getShape(1, 0));
  BOOST_CHECK_EQUAL(mat_ix.getShape(1, 0), &tr_le);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(PolygonTests)

BOOST_AUTO_TEST_CASE(ConstructorThrowArea)
{
  const size_t size = 4;
  morozov::point_t ver[size] = { { 0, 0 },  { 0, 0 },  { 0, 0 },  { 0, 0 } };

  std::unique_ptr<morozov::point_t[]> array(new morozov::point_t[size]);
  for (int i = 0; i < 4; ++i) 
    array[i] = ver[i];
  
  BOOST_CHECK_THROW(morozov::Polygon pol(array, size), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorThrowSize)
{
  const size_t size = 3;
  morozov::point_t ver[size] = { { 0, 1 },  { 1, 0 },  { 3, 3 } };

  std::unique_ptr<morozov::point_t[]> array(new morozov::point_t[size]);
  for (int i = 0; i < 3; ++i)
    array[i] = ver[i];
  
  BOOST_CHECK_THROW(morozov::Polygon pol(array, size), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorThrowConvex)
{
  const size_t size = 4;
  morozov::point_t ver[size] = { { -2, 1 },  { -5, 1 },  { -3, 2 },  { -2, 4 } };

  std::unique_ptr<morozov::point_t[]> array(new morozov::point_t[size]);
  for (int i = 0; i < 4; ++i) 
    array[i] = ver[i];
  
  BOOST_CHECK_THROW(morozov::Polygon pol(array, size), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(MoveToPointTest)
{
  const size_t size = 4;
  morozov::point_t ver[size] = { { 0, 0 },  { 2, 2 },  { 4, 2 },  { 2, 0 } };

  std::unique_ptr<morozov::point_t[]> array(new morozov::point_t[size]);
  for (int i = 0; i < 4; ++i)
    array[i] = ver[i];
  
  morozov::Polygon pol(array, size);
  pol.move({ 4.0, 5.0 });

  BOOST_CHECK_CLOSE(pol.getFrameRect().width, 4, EPSILON);
  BOOST_CHECK_CLOSE(pol.getFrameRect().height, 2, EPSILON);
  BOOST_CHECK_CLOSE(pol.getFrameRect().pos.x, 4, EPSILON);
  BOOST_CHECK_CLOSE(pol.getFrameRect().pos.y, 5, EPSILON);
  BOOST_CHECK_CLOSE(pol.getArea(), 4, EPSILON);
}

BOOST_AUTO_TEST_CASE(MoveDxTest)
{
  const size_t size = 4;
  morozov::point_t ver[size] = { { 0, 0 },  { 2, 2 },  { 4, 2 },  { 2, 0 } };

  std::unique_ptr<morozov::point_t[]> array(new morozov::point_t[size]);
  for (int i = 0; i < 4; ++i) 
    array[i] = ver[i];
  
  morozov::Polygon pol(array, size);
  pol.move(4.0, 5.0);

  BOOST_CHECK_CLOSE(pol.getFrameRect().width, 4, EPSILON);
  BOOST_CHECK_CLOSE(pol.getFrameRect().height, 2, EPSILON);
  BOOST_CHECK_CLOSE(pol.getFrameRect().pos.x, 6, EPSILON);
  BOOST_CHECK_CLOSE(pol.getFrameRect().pos.y, 6, EPSILON);
  BOOST_CHECK_CLOSE(pol.getArea(), 4, EPSILON);
}

BOOST_AUTO_TEST_CASE(getAreaTest)
{
  const size_t size = 4;
  morozov::point_t ver[size] = { { 0, 0 },  { 2, 2 },  { 4, 2 },  { 2, 0 } };

  std::unique_ptr<morozov::point_t[]> array(new morozov::point_t[size]);
  for (int i = 0; i < 4; ++i)
    array[i] = ver[i];
  
  morozov::Polygon pol(array, size);
  BOOST_CHECK_CLOSE(pol.getArea(), 4, EPSILON);
}

BOOST_AUTO_TEST_CASE(scaleTest)
{
  const size_t size = 4;
  morozov::point_t ver[size] = { { 0, 0 },  { 2, 2 },  { 4, 2 },  { 2, 0 } };

  std::unique_ptr<morozov::point_t[]> array(new morozov::point_t[size]);
  for (int i = 0; i < 4; ++i)
    array[i] = ver[i];
  
  morozov::Polygon pol(array, size);
  pol.scale(2);

  BOOST_CHECK_CLOSE(pol.getArea(), 16, EPSILON);
}

BOOST_AUTO_TEST_CASE(rotateTest)
{
  const size_t size = 4;
  morozov::point_t ver[size] = { { 0, 0 },  { 2, 2 },  { 4, 2 },  { 2, 0 } };

  std::unique_ptr<morozov::point_t[]> array(new morozov::point_t[size]);
  for (int i = 0; i < 4; ++i)
    array[i] = ver[i];
  
  morozov::Polygon pol1(array, size);
  morozov::Polygon pol2(array, size);

  pol1.rotate(45);
  BOOST_CHECK_CLOSE(pol1.getFrameRect().pos.x, pol2.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(pol1.getFrameRect().pos.y, pol2.getFrameRect().pos.y, EPSILON);
  BOOST_CHECK_CLOSE(pol1.getArea(), pol2.getArea(), EPSILON);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(rotateTest)

BOOST_AUTO_TEST_CASE(rotateTriangle)
{
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });
  tr_le.rotate(90.0);
  BOOST_CHECK_CLOSE(tr_le.getFrameRect().pos.x, 2.0, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getFrameRect().pos.y, -1.0, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getFrameRect().height, 6.0, EPSILON);
  BOOST_CHECK_CLOSE(tr_le.getFrameRect().width, 3.0, EPSILON); 
}
BOOST_AUTO_TEST_CASE(rotateRectangle)
{
  morozov::Rectangle re_le({ 0.0, 0.0 }, 4.0, 8.0);//width 4 ; height 8
  re_le.rotate(90.0);
  BOOST_CHECK_CLOSE(re_le.getFrameRect().pos.x, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getFrameRect().pos.y, 0.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getFrameRect().height, 4.0, EPSILON);
  BOOST_CHECK_CLOSE(re_le.getFrameRect().width, 8.0, EPSILON);
}
BOOST_AUTO_TEST_CASE(rotateCompositeShape)
{
  morozov::Circle ci_le({ 0.0, 0.0 }, 1.0);
  morozov::Rectangle re_le({ 0.0, 0.0 }, 4.0, 4.0);
  morozov::Triangle tr_le({ 1.0, 1.0 }, { 4.0, 4.0 }, { 7.0, 1.0 });
  morozov::CompositeShape co_te;

  co_te.pushNew(&re_le);
  co_te.pushNew(&ci_le);
  co_te.pushNew(&tr_le);
  
  co_te.rotate(30.0);

  double x_before = co_te.getFrameRect().pos.x;
  double y_before = co_te.getFrameRect().pos.y;
  double height_before = co_te.getFrameRect().height;
  double width_before = co_te.getFrameRect().width;

  BOOST_CHECK_CLOSE(co_te.getFrameRect().pos.x, x_before, EPSILON);
  BOOST_CHECK_CLOSE(co_te.getFrameRect().pos.y, y_before, EPSILON);
  BOOST_CHECK_CLOSE(co_te.getFrameRect().height, height_before, EPSILON);
  BOOST_CHECK_CLOSE(co_te.getFrameRect().width, width_before, EPSILON);
}
BOOST_AUTO_TEST_SUITE_END()
