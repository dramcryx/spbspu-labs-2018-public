#define _USE_MATH_DEFINES
#include "circle.hpp"
#include <iostream>
#include <cmath>

morozov::Circle::Circle(const point_t &center, double radius) :pos_(center)
{
  if (radius >= 0.0)
    radius_ = radius;
  else
    throw std::invalid_argument("radius must be > 0\n");
}

double morozov::Circle::getArea() const
{
  return radius_ * radius_ * M_PI;
}

morozov::rectangle_t morozov::Circle::getFrameRect() const
{
  return{ 2 * radius_, 2 * radius_, pos_ };
}

void morozov::Circle::move(const point_t &goal)
{
  pos_ = goal;
}

void morozov::Circle::move(double dx, double dy)
{
  pos_.x = pos_.x + dx;
  pos_.y = pos_.y + dy;
}

void morozov::Circle::scale(double k)
{
  if (k <= 0.0)
  {
    throw std::invalid_argument("Scale coefficient must be > 0");
  }
  radius_ *= k;
}

double morozov::Circle::getRadius()
{
  return radius_;
}

morozov::point_t morozov::Circle::getPos() const
{
  return pos_;
}

void morozov::Circle::rotate(double)
{
}
