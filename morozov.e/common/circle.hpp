#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace morozov {
  class Circle : public Shape
  {
  public:
    Circle(const point_t &center, double radius);
    double getArea() const override;
    point_t getPos() const override;
    double getRadius();
    rectangle_t getFrameRect() const override;
    void move(const point_t &goal) override;
    void move(double dx, double dy) override;
    virtual void scale(double coefficient) override;
    void rotate(double phi) override;

  private:
    point_t pos_;
    double radius_;
  };
}

#endif
