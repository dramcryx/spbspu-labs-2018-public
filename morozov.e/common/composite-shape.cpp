#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <stdexcept>
#include <cmath>

morozov::CompositeShape::CompositeShape() :amount_(0), compShape_(nullptr), pos_({ 0.0, 0.0 })
{}

morozov::CompositeShape::CompositeShape(const CompositeShape & newShape) :
  amount_(newShape.getAmount()),
  compShape_(new Shape*[amount_]),
  pos_(newShape.getFrameRect().pos)
{
  for (size_t i = 0; i < amount_; i++)
  {
    compShape_[i] = newShape[i];
  }
}

morozov::CompositeShape::CompositeShape(CompositeShape && newShape) :
  amount_(newShape.getAmount()),
  compShape_(newShape.compShape_),
  pos_(newShape.getFrameRect().pos)
{
  newShape.amount_ = 0;
  newShape.compShape_ = nullptr;
  newShape.pos_ = { 0.0, 0.0 };
}

morozov::CompositeShape::~CompositeShape()
{
  delete[] compShape_;
}

morozov::Shape * morozov::CompositeShape::operator[](size_t ind) const
{
  if (ind >= amount_)
    throw std::out_of_range("number > amount of array");

  return compShape_[ind];
}

morozov::CompositeShape & morozov::CompositeShape::operator=(const CompositeShape & newShape)
{
  if (this != &newShape)
  {
    pos_ = newShape.getFrameRect().pos;
    Shape ** tmpSh = new Shape*[newShape.amount_];
    amount_ = newShape.amount_;
    delete[] compShape_;
    compShape_ = tmpSh;

    for (size_t i = 0; i < newShape.getAmount(); i++)
    {
      compShape_[i] = newShape[i];
    }
  }

  return *this;
}

morozov::CompositeShape & morozov::CompositeShape::operator=(CompositeShape && newShape)
{
  if (this != &newShape)
  {
    pos_ = newShape.getFrameRect().pos;
    delete[] compShape_;
    compShape_ = newShape.compShape_;
    amount_ = newShape.amount_;
    newShape.amount_ = 0;
    newShape.compShape_ = nullptr;
  }

  return *this;
}

double morozov::CompositeShape::getArea() const
{
  double Area_ = 0.0;

  for (size_t i = 0; i < amount_; i++)
  {
    Area_ += compShape_[i]->getArea();
  }

  return Area_;
}

morozov::rectangle_t morozov::CompositeShape::getFrameRect() const
{
  if (amount_ == size_t(0))
    throw std::invalid_argument("composite shape has not shapes");

  rectangle_t re_le = compShape_[0]->getFrameRect();
  double minX = re_le.pos.x - re_le.width / 2;
  double maxX = re_le.pos.x + re_le.width / 2;
  double minY = re_le.pos.y - re_le.height / 2;
  double maxY = re_le.pos.y + re_le.height / 2;

  for (size_t i = 1; i < amount_; i++)
  {
    re_le = compShape_[i]->getFrameRect();

    if (minX > re_le.pos.x - re_le.width / 2)
      minX = re_le.pos.x - re_le.width / 2;

    if (maxX < re_le.pos.x + re_le.width / 2)
      maxX = re_le.pos.x + re_le.width / 2;

    if (minY > re_le.pos.y - re_le.height / 2)
      minY = re_le.pos.y - re_le.height / 2;

    if (maxY < re_le.pos.y + re_le.height / 2)
      maxY = re_le.pos.y + re_le.height / 2;
  }

  re_le.width = maxX - minX;
  re_le.height = maxY - minY;
  re_le.pos = { (minX + re_le.width / 2), (maxY - re_le.height / 2) };

  return re_le;
}

void morozov::CompositeShape::move(const point_t & pos2)
{
  const double dx = pos2.x - pos_.x;
  const double dy = pos2.y - pos_.y;

  for (size_t i = 0; i < amount_; i++)
  {
    compShape_[i]->move(dx, dy);
  }
  pos_ = pos2;
}

void morozov::CompositeShape::move(const double dx, const double dy)
{
  pos_.x += dx;
  pos_.y += dy;

  for (size_t i = 0; i < amount_; i++)
  {
    compShape_[i]->move(dx, dy);
  }
}

void morozov::CompositeShape::scale(const double coefficient)
{
  if (coefficient <= 0)
    throw std::invalid_argument("k must be > 0");

  for (size_t i = 0; i < amount_; i++)
  {
    double dx = (compShape_[i]->getPos().x - pos_.x)*(coefficient - 1);
    double dy = (compShape_[i]->getPos().y - pos_.y)*(coefficient - 1);

    compShape_[i]->scale(coefficient);
    compShape_[i]->move(dx, dy);
  }
}

size_t morozov::CompositeShape::getAmount() const
{
  return amount_;
}

morozov::point_t morozov::CompositeShape::getPos() const
{
  return pos_;
}

void morozov::CompositeShape::pushNew(Shape * newShape)
{
  if (newShape == nullptr)
    throw std::invalid_argument("no shape to add");

  Shape ** tmpSh = new Shape*[amount_ + 1];

  if (amount_>0)
  {
    for (size_t i = 0; i < amount_; i++)
    {
      tmpSh[i] = compShape_[i];
    }
  }

  tmpSh[amount_] = newShape;
  delete[] compShape_;
  compShape_ = tmpSh;
  amount_++;
  pos_ = getFrameRect().pos;
}

void morozov::CompositeShape::removeSh(Shape * Shape1)
{
  if (amount_ <= 0)
    throw std::invalid_argument("no shapes in the composition");

  for (size_t i = 0; i < amount_; ++i)
  {
    if (compShape_[i] == Shape1)
    {
      size_t index = i;
      Shape ** tmpSh = new Shape*[amount_ - 1];
      for (size_t i = 0; i < index; ++i)
      {
        tmpSh[i] = compShape_[i];
      }
      for (size_t i = index; i < amount_ - 1; ++i)
      {
        tmpSh[i] = compShape_[i + 1];
      }
      delete[] compShape_;
      compShape_ = tmpSh;
      amount_--;
      pos_ = getFrameRect().pos;
    }
    else throw std::invalid_argument("this shape is not a part of composite shape");
  }
}

void morozov::CompositeShape::rotate(double phi)
{
  point_t curPos = getPos();
  double radAngle = (phi*M_PI) / 180.0;

  for (size_t i = 0; i < amount_; i++)
  {
    compShape_[i]->move({
      curPos.x + (compShape_[i]->getPos().x - curPos.x) * cos(radAngle) - (compShape_[i]->getPos().y - curPos.y) * sin(radAngle),
      curPos.y + (compShape_[i]->getPos().y - curPos.y) * cos(radAngle) + (compShape_[i]->getPos().x - curPos.x) * sin(radAngle)
    });

    compShape_[i]->rotate(radAngle);
  }

}
