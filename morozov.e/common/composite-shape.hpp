#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"
#include <cstring>

namespace morozov {
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const CompositeShape & newShape);
    CompositeShape(CompositeShape && newShape);
    ~CompositeShape();

    Shape * operator[](size_t ind) const;
    CompositeShape & operator= (const CompositeShape & newShape);
    CompositeShape & operator= (CompositeShape && newShape);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &pos2) override;
    void move(const double dx, const double dy) override;
    virtual void scale(double coefficient) override;
    void rotate(double phi) override;

    size_t getAmount() const;
    void pushNew(Shape * newShape);
    void removeSh(Shape * Shape1);
    point_t getPos() const;
  private:
    size_t amount_;
    Shape** compShape_;
    point_t pos_;
  };
}

#endif
