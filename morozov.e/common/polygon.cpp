#define _USE_MATH_DEFINES
#include "base-types.hpp"
#include "polygon.hpp"
#include <stdexcept>
#include <cmath>
#include <iostream>

morozov::Polygon::Polygon(const std::unique_ptr<morozov::point_t[]> &array, size_t size) :size_(size), ver_(new morozov::point_t[size])
{
  if (size_ < 4)
    throw std::invalid_argument("size must be > 3");

  for (size_t i = 0; i < size_; ++i)
    ver_[i] = array[i];
  
  if (!isConvex()) 
    throw std::invalid_argument("polygon is  not convex");
  
  if (getArea() == 0) 
    throw std::invalid_argument("polygon area = 0");

  pos_ = getPos();
}

morozov::point_t &morozov::Polygon::operator[](unsigned int ind) const
{
  if (ind >= size_) 
    throw std::out_of_range("index out of range");
  
  return ver_[ind];
}

morozov::point_t morozov::Polygon::getPos() const
{
  double x_cr = 0;
  double y_cr = 0;

  double p = 0;
  double l = 0;

  for (size_t i = 0; i < size_; ++i) {
    if (i != size_ - 1) 
    {
      l = sqrt(pow(ver_[i].x - ver_[i + 1].x, 2) + pow(ver_[i].y - ver_[i + 1].y, 2));
      x_cr += l * ((ver_[i].x + ver_[i + 1].x) / 2);
      y_cr += l * ((ver_[i].y + ver_[i + 1].y) / 2);
      p += l;
    }
    else {
      l = sqrt(pow(ver_[i].x - ver_[0].x, 2) + pow(ver_[i].y - ver_[0].y, 2));
      x_cr += l * ((ver_[i].x + ver_[0].x) / 2);
      y_cr += l * ((ver_[i].y + ver_[0].y) / 2);
      p += l;
    }
  }

  return{ x_cr / p, y_cr / p };
}

double morozov::Polygon::getArea() const
{
  double area = 0;

  for (size_t i = 0; i < size_ - 1; i++) 
    area += ((ver_[i].x + ver_[i + 1].x) * (ver_[i].y - ver_[i + 1].y));
  
  area += ((ver_[size_ - 1].x + ver_[0].x) * (ver_[size_ - 1].y - ver_[0].y));

  return fabs(area) / 2;
}

morozov::rectangle_t morozov::Polygon::getFrameRect() const
{
  double maxX = ver_[0].x > ver_[1].x ? ver_[0].x : ver_[1].x;
  double maxY = ver_[0].y > ver_[1].y ? ver_[0].y : ver_[1].y;

  double minX = ver_[0].x < ver_[1].x ? ver_[0].x : ver_[1].x;
  double minY = ver_[0].y < ver_[1].y ? ver_[0].y : ver_[1].y;

  for (size_t i = 2; i < size_; ++i) 
  {
    if (ver_[i].x > maxX)
      maxX = ver_[i].x;
    else if (ver_[i].x < minX)
      minX = ver_[i].x;

    if (ver_[i].y > maxY)
      maxY = ver_[i].y;
    else if (ver_[i].y < minY)
      minY = ver_[i].y;
  }

  return{ maxX - minX, maxY - minY,{ minX + (maxX - minX) / 2, minY + (maxY - minY) / 2 } };
}

void morozov::Polygon::move(const morozov::point_t &pos)
{
  move(pos.x - getPos().x, pos.y - getPos().y);
}

void morozov::Polygon::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; ++i) 
  {
    ver_[i].x += dx;
    ver_[i].y += dy;
  }

  pos_.x += dx;
  pos_.y += dy;
}

void morozov::Polygon::printInfo()
{
  for (size_t i = 0; i < size_; ++i) 
    std::cout << "ver[" << i << "] : {" << ver_[i].x << " , " << ver_[i].y << "}" << std::endl;
  
  std::cout << "center : {" << pos_.x << " , " << pos_.y << "}" << std::endl;
}

void morozov::Polygon::scale(double coefficient)
{
  for (size_t i = 0; i < size_; ++i) 
  {
    ver_[i].x = pos_.x + coefficient * (ver_[i].x - pos_.x);
    ver_[i].y = pos_.y + coefficient * (ver_[i].y - pos_.y);
  }
}

void morozov::Polygon::rotate(double phi)
{
  phi = phi * M_PI / 180;

  double r = 0;
  double phi2 = 0;

  point_t center = getFrameRect().pos;

  for (size_t i = 0; i < size_; i++) 
  {
    r = sqrt(pow((center.x - ver_[i].x), 2) + pow((center.y - ver_[i].y), 2));

    if (ver_[i].y > center.y) 
      phi2 = acos((ver_[i].x - center.x) / r);
    else 
      phi2 = 2 * M_PI - acos((ver_[i].x - center.x) / r);
    
    ver_[i].x += (cos(phi2 + phi) - cos(phi2)) * r;
    ver_[i].y += (sin(phi2 + phi) - sin(phi2)) * r;
  }

}

bool morozov::Polygon::isConvex() const
{
  for (size_t i = 0; i < size_ - 1; i++) 
  {
    double line_pos1 = (ver_[i + 1].y - ver_[i].y) * (ver_[0].x - ver_[i].x) - (ver_[i + 1].x - ver_[i].x) * (ver_[0].y - ver_[i].y);
    double line_pos2 = 0;

    for (size_t j = 1; j < size_; j++) 
    {
      line_pos2 = (ver_[i + 1].y - ver_[i].y) * (ver_[j].x - ver_[i].x) - (ver_[i + 1].x - ver_[i].x) * (ver_[j].y - ver_[i].y);

      if (line_pos2 * line_pos1 >= 0) 
        line_pos1 = line_pos2;
      else 
        return false;
    }
  }
  return true;

}
