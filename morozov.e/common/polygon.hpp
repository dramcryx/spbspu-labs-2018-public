#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "shape.hpp"
#include <memory>

namespace morozov
{
  class Polygon : public morozov::Shape
  {
  public:
    explicit Polygon(const std::unique_ptr<morozov::point_t[]> & array, size_t size);

    morozov::point_t &operator[](unsigned int ind) const;

    point_t getPos() const override;
    double getArea() const override;
    rectangle_t getFrameRect() const override;

    void move(const point_t &pos) override;
    void move(double dx, double dy) override;
    void printInfo();
    void scale(double coefficient) override;
    void rotate(double phi) override;
    bool isConvex() const;
    
  private:
    size_t size_;
    std::unique_ptr<morozov::point_t[]> ver_;
    point_t pos_;
  };
}
#endif
