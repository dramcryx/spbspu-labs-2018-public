#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace morozov {
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t &center, double width, double height);
    double getArea() const override;
    point_t getPos() const override;
    double getHeight();
    double getWidth();
    rectangle_t getFrameRect() const override;
    void move(const point_t &goal) override;
    void move(double dx, double dy) override;
    virtual void scale(double coefficient) override;
    void rotate(double phi) override;

  private:
    point_t pos_;
    double width_, height_;
    double angle_;
  };
}

#endif
