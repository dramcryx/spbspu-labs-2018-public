#include "triangle.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "shape.hpp"

int main()
{
  Rectangle rectangle({5.0, 7.0, { 2.0, 5.0 }});
  Shape *shape = &rectangle;
  shape->move(5.0, 4.0);
  shape->move({6.0,4.0});

  Circle circle(7.0, { 4.0, 4.0 });
  shape = &circle;
  shape->move(1.0, 1.0);
  shape->move({10.0, 6.0});

  Triangle triangle({ 4.0, 5.0 }, { 6.0, 8.0 }, { 7.0, 5.0 });
  shape = &triangle;
  shape->move(6.0,9.0);
  shape->move({2.0,2.0});
  return 0;
}
