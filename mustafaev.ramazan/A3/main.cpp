#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
    
    std::shared_ptr<mustafaev::Shape> rectPtr
        = std::make_shared<mustafaev::Rectangle>(mustafaev::Rectangle({5.0, 10.0, {50.0, 100.0}}));
    mustafaev::CompositeShape shape(rectPtr);

    std::shared_ptr<mustafaev::Shape> circlePtr
        = std::make_shared<mustafaev::Circle>(mustafaev::Circle(6.0, {12.0, 12.0}));
    shape.addShape(circlePtr);
    
    std::shared_ptr<mustafaev::Shape> trianglePtr
        = std::make_shared<mustafaev::Triangle>(mustafaev::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);
    
    std::cout<< shape.getArea() << "\n";
    
    shape.move({100.0, 50.0});
    std::cout<< shape.getArea() << "\n";
    
    shape.move(30.0, 40.0);
    shape.scale(2.0);
    std::cout<< shape.getArea() << "\n";
    
    shape.delShape(0);
    
    return 0;
}

