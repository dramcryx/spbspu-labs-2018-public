#include <iostream>
#include <cmath>

#include "circle.hpp"
#include "base-types.hpp"

mustafaev::Circle::Circle(double radius, const point_t & position):
  r_(radius),
  p_(position),
  angle_(0.0)
{
  if (r_ < 0.0)
  {
    throw std::invalid_argument("it is not right radius");
  }
}

double mustafaev::Circle::getArea() const noexcept
{
  return (M_PI * r_ * r_);
}

mustafaev::rectangle_t mustafaev::Circle::getFrameRect() const noexcept
{
  return rectangle_t{2 * r_, 2 * r_, p_};
}

void mustafaev::Circle::move(const point_t & p) noexcept
{
  p_ = p;
}

void mustafaev::Circle::move(double dx, double dy) noexcept
{
  p_.x += dx;
  p_.y += dy;
}

void mustafaev::Circle::scale(double k)
{
  if (k < 0.0)
    {
        throw std::invalid_argument("it is not right parameter of scaling");
    }
  r_ *= k;
}

void mustafaev::Circle::rotate(double a)
{
  angle_ += a;
  if (angle_ >= 360.0)
    {
        angle_ = fmod(angle_, 360.0);
    }
}
