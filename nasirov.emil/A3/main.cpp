#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

using namespace nasirov;

int main()
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{0, 0}, 20, 10);
  std::cout << "rectangle was created" << std::endl << std::endl;

  rectangle->printData();

  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{10, 10}, 10);
  std::cout << std::endl << "circle was created" << std::endl << std::endl;

  circle->printData();

  CompositeShape composite_shape(rectangle);
  std::cout << std::endl << "composite shape was created" << std::endl << std::endl
    << "rectangle was added to composite shape" << std::endl;

  composite_shape.addShape(circle);
  std::cout << "circle was added to composite shape" << std::endl << std::endl;

  (&composite_shape)->printData();

  return 0;
}
