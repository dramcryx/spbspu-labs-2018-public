#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

using namespace nasirov;
const double TOLERANCE = 0.0001;

BOOST_AUTO_TEST_SUITE(testCompositeShape)

BOOST_AUTO_TEST_CASE(move)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{0, 0}, 2, 4);
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{2, -2}, 2);
  
  CompositeShape composite_shape(rectangle);
  composite_shape.addShape(circle);

  double area = composite_shape.getArea();

  composite_shape.move(2, 2);
  
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 3.5, TOLERANCE);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 1, TOLERANCE);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 5, TOLERANCE);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 6, TOLERANCE);
  BOOST_CHECK_CLOSE(composite_shape.getArea(), area, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(scale)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{3, -2}, 2, 2);
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{0, 2}, 1);
  
  CompositeShape composite_shape(rectangle);
  composite_shape.addShape(circle);

  double area = composite_shape.getArea();
  
  composite_shape.scale(2);

  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.x, 1.5, TOLERANCE);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().pos.y, 0, TOLERANCE);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().width, 10, TOLERANCE);
  BOOST_CHECK_CLOSE(composite_shape.getFrameRect().height, 12, TOLERANCE);
  BOOST_CHECK_CLOSE(composite_shape.getArea(), area * 4, TOLERANCE);
}

BOOST_AUTO_TEST_CASE(copy_constructor)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{10, 15}, 30, 20);
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{0, 10}, 15);
  
  CompositeShape composite(circle);
  composite.addShape(rectangle);
  
  CompositeShape composite_1(composite);

  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite.getFrameRect().height, 30);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().height, 30);
}

BOOST_AUTO_TEST_CASE(copy_operator)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{10, 15}, 30, 20);
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{0, 10}, 15);
  
  CompositeShape composite(circle);
  composite.addShape(rectangle);
  
  CompositeShape composite_1 = composite;

  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite.getFrameRect().height, 30);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().height, 30);
}

BOOST_AUTO_TEST_CASE(move_constructor)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{10, 15}, 30, 20);
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{0, 10}, 15);
  
  CompositeShape composite(rectangle);
  composite.addShape(circle);
  
  CompositeShape composite_1(std::move(composite));

  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.x, 5);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.y, 10);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().width, 40);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().height, 30);
}

BOOST_AUTO_TEST_CASE(move_operator)
{
  std::shared_ptr<Rectangle> rectangle_1 = std::make_shared<Rectangle>(point_t{2, 10}, 8, 4);
  std::shared_ptr<Rectangle> rectangle_2 = std::make_shared<Rectangle>(point_t{-15, 0}, 5, 10);
  
  CompositeShape composite(rectangle_1);
  composite.addShape(rectangle_2);
  
  CompositeShape composite_1 = std::move(composite);

  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.x, -5.75);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().pos.y, 3.5);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().width, 23.5);
  BOOST_CHECK_EQUAL(composite_1.getFrameRect().height, 17);
}

BOOST_AUTO_TEST_CASE(invalid_constructor)
{ 
  BOOST_CHECK_THROW(CompositeShape composite_shape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_add_shape)
{
  std::shared_ptr<Circle> circle = std::make_shared<Circle>(point_t{0, 0}, 3);
  CompositeShape composite_shape(circle);
  
  BOOST_CHECK_THROW(composite_shape.addShape(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(invalid_scale)
{
  std::shared_ptr<Rectangle> rectangle = std::make_shared<Rectangle>(point_t{0, 0}, 5, 5);
  CompositeShape composite_shape(rectangle);

  BOOST_CHECK_THROW(composite_shape.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
