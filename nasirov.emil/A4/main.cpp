#include <iostream>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix-shape.hpp"

using namespace nasirov;

int main()
{
  std::shared_ptr<nasirov::Rectangle> rectangle_1 = std::make_shared<Rectangle>(point_t{-1, -2.5}, 6, 3);
  std::cout << "rectangle_1 was created" << std::endl << std::endl;;

  rectangle_1->printData();

  std::shared_ptr<nasirov::Circle> circle = std::make_shared<Circle>(point_t{3, 0}, 3);
  std::cout << std::endl << "circle was created" << std::endl << std::endl;;

  circle->printData();

  std::shared_ptr<nasirov::Rectangle> rectangle_2 = std::make_shared<Rectangle>(point_t{-2.5, 1}, 3, 2);
  std::cout << std::endl << "rectangle_2 was created" << std::endl << std::endl;;

  rectangle_2->printData();

  MatrixShape matrix_shape(rectangle_1);
  std::cout << std::endl << "matrix was created" << std::endl << std::endl;;

  matrix_shape.addShape(circle);
  matrix_shape.addShape(rectangle_2);

  matrix_shape.print();
}
