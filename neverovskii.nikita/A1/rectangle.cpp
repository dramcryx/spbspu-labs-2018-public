#include <iostream>
#include "rectangle.hpp"

Rectangle::Rectangle(const double width, const double height, const point_t &center) :center_(center)
{
  if (width <= 0.0 || height <= 0.0) {
    std::cerr << "Width and height can't be less then zero" << std::endl;
  }
  width_ = width;
  height_ = height;
  center_ = center;
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {height_, width_, {center_.x, center_.y}};
}

void Rectangle::move(const point_t &center)
{
  center_ = center;
}

void Rectangle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void Rectangle::show() const
{
  std::cout << "Center of the rectangle ( " << center_.x << " ; " << center_.y << " )" << std::endl;
  std::cout << "Width of the rectangle: " << getFrameRect().width << std::endl;
  std::cout << "Height of the rectangle: " << getFrameRect().height << std::endl;
  std::cout << "Area of the rectangle: " << getArea() << std::endl;
  std::cout << " " << std::endl;
}
