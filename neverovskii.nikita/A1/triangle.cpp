#include <cmath>
#include <iostream>
#include <iomanip>
#include "triangle.hpp"

Triangle::Triangle(const point_t &A, const point_t &B, const point_t &C) :
  A_(A),
  B_(B),
  C_(C)
{
  A_ = A;
  B_ = B;
  C_ = C;
  center_ = {(A_.x + B_.x + C_.x) / 3, (A_.y + B_.y + C_.y) / 3};
}

rectangle_t Triangle::getFrameRect() const
{
  double maxX = A_.x > B_.x ? (A_.x > C_.x ? A_.x : C_.x) : (B_.x > C_.x ? B_.x : C_.x);
  double minX = A_.x < B_.x ? (A_.x < C_.x ? A_.x : C_.x) : (B_.x < C_.x ? B_.x : C_.x);
  double maxY = A_.y > B_.y ? (A_.y > C_.y ? A_.y : C_.y) : (B_.y > C_.y ? B_.y : C_.y);
  double minY = A_.y < B_.y ? (A_.y < C_.y ? A_.y : C_.y) : (B_.y < C_.y ? B_.y : C_.y);
  return {maxX - minX, maxY - minY, {minX + (maxX - minX) / 2, minY + (maxY - minY) / 2}};
}

double Triangle::getArea() const
{
  double s = (0.5) * (A_.x * (B_.y - C_.y) + B_.x * (C_.y - A_.y) + C_.x * (A_.y - B_.y));
  if (s < 0) {
    s *= -1;
  }
  return s;
}

void Triangle::move(const point_t &center)
{
  move(center.x - (A_.x + B_.x + C_.x) / 3, center.y - (A_.y + B_.y + C_.y) / 3);
  center_ = center;
}

void Triangle::move(double dx, double dy)
{
  A_ = {A_.x + dx, A_.y + dy};
  B_ = {B_.x + dx, B_.y + dy};
  C_ = {C_.x + dx, C_.y + dy};
  center_.x += dx;
  center_.y += dy;
}

void Triangle::show() const
{
  std::cout << "Point A ( " << std::setprecision(2) << A_.x << " ; " << A_.y << " )" << std::endl;
  std::cout << "Point B ( " << std::setprecision(2) << B_.x << " ; " << B_.y << " )" << std::endl;
  std::cout << "Point C ( " << std::setprecision(2) << C_.x << " ; " << C_.y << " )" << std::endl;
  std::cout << "Center of the triangle ( " << std::setprecision(2) << center_.x << " ; " << center_.y << " )" << std::endl;
  std::cout << "Width of the triangle: " << std::setprecision(2) << getFrameRect().width << std::endl;
  std::cout << "Height of the triangle: " << std::setprecision(2) << getFrameRect().height << std::endl;
  std::cout << "Area of the triangle: " << std::setprecision(2) << getArea() << std::endl;
  std::cout << " " << std::endl;
}
