#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

using namespace neverovskii;
void ShowShape(const Shape &shape)
{
  shape.show();
}

int main()
{
  Rectangle rectangle(6.0, 4.0, {4.0, 1.0});
  std::cout << " " << std::endl;
  std::cout << "Rectangle:" << std::endl;
  std::cout << " " << std::endl;
  ShowShape(rectangle);
  std::cout << " " << std::endl;
  std::cout << "Move to the certain point: " << std::endl;
  std::cout << " " << std::endl;
  rectangle.move({4.1, 5.2});
  ShowShape(rectangle);
  std::cout << " " << std::endl;
  std::cout << "Move by X and Y: " << std::endl;
  std::cout << " " << std::endl;
  rectangle.move(1.8, 6.0);
  ShowShape(rectangle);
  std::cout << " " << std::endl;
  std::cout << "Scale:" << std::endl;
  std::cout << " " << std::endl;
  rectangle.scale(5.0);
  ShowShape(rectangle);

  Circle circle(2.0, {4.0, 5.0});
  std::cout << " " << std::endl;
  std::cout << "Circle:" << std::endl;
  std::cout << " " << std::endl;
  ShowShape(circle);
  std::cout << " " << std::endl;
  std::cout << "Move to the certain point: " << std::endl;
  std::cout << " " << std::endl;
  rectangle.move({3.5, 8.0});
  ShowShape(circle);
  std::cout << " " << std::endl;
  std::cout << "Move by X and Y: " << std::endl;
  std::cout << " " << std::endl;
  rectangle.move(7.3, 7.4);
  ShowShape(circle);
  std::cout << " " << std::endl;
  std::cout << "Scale:" << std::endl;
  std::cout << " " << std::endl;
  circle.scale(5.0);
  ShowShape(circle);

  Triangle triangle({1.0, 1.0}, {2.0, 2.0}, {2.0, 0.0});
  std::cout << " " << std::endl;
  std::cout << "Triangle:" << std::endl;
  std::cout << " " << std::endl;
  ShowShape(triangle);
  std::cout << " " << std::endl;
  std::cout << "Move to the certain point: " << std::endl;
  std::cout << " " << std::endl;
  triangle.move({2.0, 9.0});
  ShowShape(triangle);
  std::cout << " " << std::endl;
  std::cout << "Move by X and Y: " << std::endl;
  std::cout << " " << std::endl;
  triangle.move(14.1, 5.5);
  ShowShape(triangle);
  std::cout << " " << std::endl;
  std::cout << "Scale:" << std::endl;
  std::cout << " " << std::endl;
  triangle.scale(2.0);
  ShowShape(triangle);
  return 0;
}
