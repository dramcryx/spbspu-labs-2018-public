#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <cmath>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include <boost/test/included/unit_test.hpp>

using namespace neverovskii;
const double epsilon=0.00001;
//Testing Circle
BOOST_AUTO_TEST_SUITE(CircleTest)
  BOOST_AUTO_TEST_CASE(MoveToThePoint)
  {
    Circle testCircle{10.0,{2,2}};
    testCircle.move({5,5});
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().height, 20.0, epsilon);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().width, 20.0, epsilon);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().pos.x, 5.0, epsilon);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().pos.y, 5.0, epsilon);
    BOOST_CHECK_CLOSE(testCircle.getArea(), 100*M_PI, epsilon);
  }
  BOOST_AUTO_TEST_CASE(MovebyXandY)
  {
    Circle testCircle{10.0,{2,2}};
    testCircle.move(5,5);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().height, 20.0, epsilon);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().width, 20.0, epsilon);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().pos.x, 7.0, epsilon);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().pos.y, 7.0, epsilon);
    BOOST_CHECK_CLOSE(testCircle.getArea(), 100*M_PI, epsilon);
  }
  BOOST_AUTO_TEST_CASE(Scale)
  {
    Circle testCircle{10.0,{2,2}};
    testCircle.scale(2);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().height, 40, epsilon);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().width, 40, epsilon);
    BOOST_CHECK_CLOSE(testCircle.getArea(), 400*M_PI, epsilon);
    BOOST_CHECK_THROW(testCircle.scale(-2), std::invalid_argument);
  }
  BOOST_AUTO_TEST_SUITE_END()
//Testing Rectangle
BOOST_AUTO_TEST_SUITE(RectangleTest)
  BOOST_AUTO_TEST_CASE(MoveToThePoint)
  {
    Rectangle testRectangle{1.0,1.0,{1,1}};
    testRectangle.move({2,2});
    BOOST_CHECK_CLOSE(testRectangle.getFrameRect().height, 1.0, epsilon);
    BOOST_CHECK_CLOSE(testRectangle.getFrameRect().width, 1.0, epsilon);
    BOOST_CHECK_CLOSE(testRectangle.getFrameRect().pos.x, 2.0, epsilon);
    BOOST_CHECK_CLOSE(testRectangle.getFrameRect().pos.y, 2.0, epsilon);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), 1, epsilon);
  }
  BOOST_AUTO_TEST_CASE(MovebyXandY)
  {
    Rectangle testRectangle{1.0,1.0,{1,1}};
    testRectangle.move(5,5);
    BOOST_CHECK_CLOSE(testRectangle.getFrameRect().height, 1.0, epsilon);
    BOOST_CHECK_CLOSE(testRectangle.getFrameRect().width, 1.0, epsilon);
    BOOST_CHECK_CLOSE(testRectangle.getFrameRect().pos.x, 6.0, epsilon);
    BOOST_CHECK_CLOSE(testRectangle.getFrameRect().pos.y, 6.0, epsilon);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), 1, epsilon);
  }
  BOOST_AUTO_TEST_CASE(Scale)
  {
    Rectangle testRectangle{1.0,1.0,{1,1}};
    testRectangle.scale(2);
    BOOST_CHECK_CLOSE(testRectangle.getFrameRect().height, 2, epsilon);
    BOOST_CHECK_CLOSE(testRectangle.getFrameRect().width, 2, epsilon);
    BOOST_CHECK_CLOSE(testRectangle.getArea(), 4, epsilon);
    BOOST_CHECK_THROW(testRectangle.scale(-2), std::invalid_argument);
  }
  BOOST_AUTO_TEST_SUITE_END()
//Testing Triangle
BOOST_AUTO_TEST_SUITE(TriangleTest)
  BOOST_AUTO_TEST_CASE(MoveToThePoint)
  {
    Triangle testTriangle{{1.0, 1.0}, {2.0, 2.0}, {2.0, 0.0}};
    testTriangle.move({2.0,2.0});
    BOOST_CHECK_CLOSE(testTriangle.getFrameRect().height, 2.0, epsilon);
    BOOST_CHECK_CLOSE(testTriangle.getFrameRect().width, 1.0, epsilon);
    BOOST_CHECK_CLOSE(testTriangle.getFrameRect().pos.x, 1.833333333, epsilon);
    BOOST_CHECK_CLOSE(testTriangle.getFrameRect().pos.y, 2.0, epsilon);
    BOOST_CHECK_CLOSE(testTriangle.getArea(), 1, epsilon);
  }
  BOOST_AUTO_TEST_CASE(MovebyXandY)
  {
    Triangle testTriangle{{1.0, 1.0}, {2.0, 2.0}, {2.0, 0.0}};
    testTriangle.move(5.0,5.0);
    BOOST_CHECK_CLOSE(testTriangle.getFrameRect().height, 2.0, epsilon);
    BOOST_CHECK_CLOSE(testTriangle.getFrameRect().width, 1.0, epsilon);
    BOOST_CHECK_CLOSE(testTriangle.getFrameRect().pos.x, 6.5, epsilon);
    BOOST_CHECK_CLOSE(testTriangle.getFrameRect().pos.y, 6.0, epsilon);
    BOOST_CHECK_CLOSE(testTriangle.getArea(), 1, epsilon);
  }
  BOOST_AUTO_TEST_CASE(Scale)
  {
    Triangle testTriangle{{1.0, 1.0}, {2.0, 2.0}, {2.0, 0.0}};
    testTriangle.scale(2);
    BOOST_CHECK_CLOSE(testTriangle.getFrameRect().height, 4, epsilon);
    BOOST_CHECK_CLOSE(testTriangle.getFrameRect().width, 2, epsilon);
    BOOST_CHECK_CLOSE(testTriangle.getArea(), 4, epsilon);
    BOOST_CHECK_THROW(testTriangle.scale(-2), std::invalid_argument);
  }
  BOOST_AUTO_TEST_SUITE_END()
