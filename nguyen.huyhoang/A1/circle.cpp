#include "circle.hpp"
#include <cstdlib>
#include <iostream>
#include <cmath>

Circle::Circle(point_t pos, double rad):
  cen_(pos),
  r_(rad)
{
  if (r_ <= 0)
  {
    throw "Invalid Circle";
  }
}

double Circle::getArea() const
{
  return r_*r_*M_PI;
}

double Circle::getRadius() const
{
  return r_;
}

point_t Circle::getCenter() const
{
  return cen_;
}

rectangle_t Circle::getFrameRect() const
{
  return {cen_, 2*r_, 2*r_};
}

void Circle::getShapeName()
{
  std::cout << "This shape is a Circle:" << std::endl;
}

void Circle::move(const point_t& c)
{
  cen_ = c;
}

void Circle::move(const double& dx, const double& dy)
{
  cen_.x += dx;
  cen_.y += dy;
}
