#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

class Circle: public Shape
{
  public:
    Circle(point_t pos, double rad);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& c) override;
    void move(const double& dx, const double& dy) override;
    void getShapeName() override;
    point_t getCenter() const override;
    double getRadius() const;
  private:
    point_t cen_;
    double r_;
};
#endif
