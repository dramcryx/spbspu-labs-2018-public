#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include <iostream>

void shorten(Shape& obj) //to shorten the code
{
  obj.getShapeName();
  std::cout << "The center of the shape is: (" << obj.getCenter().x
      << ";" << obj.getCenter().y << ")" << std::endl;
  obj.getInfoShape();
  point_t mc;
  mc.x = 100;
  mc.y = 120;
  obj.move(mc);
  std::cout << "The center of the shape is: (" << obj.getCenter().x
      << ";" << obj.getCenter().y << ")" << std::endl;
  obj.getInfoShape();
  obj.move(20,20);
  std::cout << "The center of the shape is: (" << obj.getCenter().x
      << ";" << obj.getCenter().y << ")" << std::endl;
  obj.getInfoShape();
}

int main()
{
  try
  {
    Rectangle rec({5,5},20,30);
    shorten(rec);
    std::cout << std::endl;
    
    Circle circ({10,12},11);
    shorten(circ);
    std::cout << std::endl;
    
    Triangle tria({1,2},{20,30},{3,45});
    shorten(tria);
    tria.getLengths();
  }
  catch (const char* msg) 
  {
    std::cerr << msg << std::endl;
    return 1;
  }
  return 0;
}
