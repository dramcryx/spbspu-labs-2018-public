#include "rectangle.hpp"
#include <cstdlib>
#include <iostream>

Rectangle::Rectangle(point_t pos, double width, double lenght):
  wid_(width),
  hei_(lenght),
  cen_(pos)        
{
  if (wid_ <= 0) 
  {
    throw "Invalid rectangle width";
  }
  if (hei_ <= 0) 
  {
    throw "Invalid rectangle height";
  }
}

double Rectangle::getArea() const
{
  return wid_*hei_;
}

point_t Rectangle::getCenter() const
{
  return cen_;
}

double Rectangle::getHeight() const
{
  return hei_;
}

double Rectangle::getWidth() const
{
  return wid_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {cen_, wid_, hei_};
}

void Rectangle::getShapeName()
{
  std::cout << "This shape is Rectangle:" << std::endl;
}

void Rectangle::move(const point_t& c)
{
  cen_ = c;
}

void Rectangle::move(const double& dx, const double& dy)
{
  cen_.x += dx;
  cen_.y += dy;
}
