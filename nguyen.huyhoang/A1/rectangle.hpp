#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

class Rectangle: public Shape
{
  public:
    Rectangle(point_t pos, double width, double lenght);
    double getArea() const override;
    void move(const point_t& c) override;
    void move(const double& dx, const double& dy) override;
    rectangle_t getFrameRect() const override;
    point_t getCenter() const override;
    void getShapeName() override;
    double getHeight() const;
    double getWidth() const;
  private:
    double wid_;
    double hei_;
    point_t cen_;
};
#endif
