#ifndef SHAPE_HPP
#define SHAPE_HPP
#include "base-types.hpp"
#include <iostream>

class Shape
{
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    //Move to certain point
    virtual void move(const point_t& c) = 0;
    //Move the distance dx,dy
    virtual void move(const double& dx, const double& dy) = 0;
    virtual void getShapeName() = 0;
    virtual point_t getCenter() const = 0;
    void getInfoShape()
    {
      double q = this->getArea();
      std::cout << "The area of this shape is: " << q << std::endl;
      rectangle_t obbj = this->getFrameRect();
      std::cout << "The frame of this shape is: Center:(" << obbj.pos.x
          << ";" << obbj.pos.y << ") with Width:" << obbj.width
              << " and Height:" << obbj.height << std::endl;
    }
};
#endif
