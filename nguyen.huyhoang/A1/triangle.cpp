#include "triangle.hpp"
#include <cstdlib>
#include <iostream>
#include <cmath>

Triangle::Triangle(point_t poA, point_t poB, point_t poC):
  a_(poA),
  b_(poB),
  c_(poC),
  g_({(poA.x+poB.x+poC.x)/3,(poA.y+poB.y+poC.y)/3}),
  ab_(sqrt((poA.x-poB.x)*(poA.x-poB.x)+(poA.y-poB.y)*(poA.y-poB.y))),
  bc_(sqrt((poB.x-poC.x)*(poB.x-poC.x)+(poB.y-poC.y)*(poB.y-poC.y))),
  ca_(sqrt((poC.x-poA.x)*(poC.x-poA.x)+(poC.y-poA.y)*(poC.y-poA.y)))
{}
  
point_t Triangle::getCenter() const
{
  return g_;
}

void Triangle::move(const point_t& c)
{
  g_ = c;
  double p = c.x - g_.x;
  double q = c.y - g_.y;
  a_.x += p;
  b_.x += p;
  c_.x += p;
  a_.y += q;
  b_.y += q;
  c_.y += q;
}

void Triangle::move(const double& dx, const double& dy)
{
  g_.x += dx;
  g_.y += dy;
  a_.x += dx;
  b_.x += dx;
  c_.x += dx;
  a_.y += dy;
  b_.y += dy;
  c_.y += dy;
}

void Triangle::getLengths() const
{
  std::cout << "3 parameters of the triangle are:"
      << ab_ << "," << bc_ << "," << ca_ << std::endl;
}

double Triangle::getArea() const
{
  double s = sqrt((ab_+bc_+ca_)*(ab_+bc_-ca_)*(ab_-bc_+ca_)*(bc_+ca_-ab_))/4;
  return s;
}

rectangle_t Triangle::getFrameRect() const
{
  double a[3] = {a_.x, b_.x, c_.x};
  double left = a[0];
  for (int i=1; i<2; i++)
  {
    if (a[i] < left)
    {
      left = a[i];
    }
  }
  double right = a[0];
  for (int i=1; i<2; i++)
  {
    if (a[i] > right)
    {
      right = a[i];
    }
  }
  double b[3] = {a_.y, b_.y, c_.y};
  double under = b[0];
  for (int i=1; i<2; i++)
  {
    if (b[i] < under)
    {
      under = b[i];
    }
  }
  double beneath = b[0];
  for (int i=1; i<2; i++)
  {
    if (b[i] > beneath)
    {
      beneath = b[i];
    }
  }
  double widthR;
  widthR = right - left;
  double heightR;
  heightR = beneath - under;
  point_t cenR;
  cenR.x = (right + left)/2;
  cenR.y = (under + beneath)/2;
  return {cenR, widthR, heightR};
}

void Triangle::getShapeName()
{
  std::cout << "This shape is Triangle:" << std::endl;
}
