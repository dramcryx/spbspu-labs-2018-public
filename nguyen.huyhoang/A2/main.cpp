#include "../common/circle.hpp"
#include "../common/rectangle.hpp"
#include "../common/triangle.hpp"
#include <iostream>
#include <stdexcept>

namespace nguyen
{
  void shorten(Shape& obj) //to shorten the code
  {
    obj.getInfoShape();
    obj.move(20,20);
    obj.getInfoShape();
    obj.scale(2);
    obj.getInfoShape();
  }
}

int main()
{
  try
  {
    nguyen::Rectangle rec({5,5},20,30);
    shorten(rec);
    std::cout << std::endl;
    
    nguyen::Circle circ({10,12},11);
    shorten(circ);
    std::cout << std::endl;
    
    nguyen::Triangle tria({1,2},{20,30},{3,45});
    shorten(tria);
  }
  catch (const std::exception& e)
  {
    std::cout << e.what() << std::endl;
    return 1;
  }
  return 0;
}
