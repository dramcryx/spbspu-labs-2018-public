#define BOOST_TEST_MODULE MyTestMain
#include "../common/circle.hpp"
#include "../common/rectangle.hpp"
#include "../common/triangle.hpp"
#include <stdexcept>
#include <cmath>
#include <boost/test/included/unit_test.hpp>

const double epsilon = 0.001;

BOOST_AUTO_TEST_SUITE(Check_Rectangle)

  BOOST_AUTO_TEST_CASE(Action_Move_dxdy)
  {
    nguyen::Rectangle rec({5,5},20,30);
    rec.move(20,20);
    BOOST_CHECK_CLOSE(20,rec.getWidth(),epsilon);
    BOOST_CHECK_CLOSE(30,rec.getHeight(),epsilon);
    BOOST_CHECK_CLOSE(20*30,rec.getArea(),epsilon);
  }

  BOOST_AUTO_TEST_CASE(Area_Scale)
  {
    nguyen::Rectangle rec({5,5},20,30);
    rec.scale(3);
    BOOST_CHECK_CLOSE(20*30*3*3,rec.getArea(),epsilon);
    BOOST_CHECK_CLOSE(20*3,rec.getWidth(),epsilon);
    BOOST_CHECK_CLOSE(30*3,rec.getHeight(),epsilon);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Constructor)
  {
    BOOST_CHECK_THROW(nguyen::Rectangle rec({5,5},-20,30),std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Scale)
  {
    nguyen::Rectangle rec({5,5},20,30);
    BOOST_CHECK_THROW(rec.scale(-2),std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
        
BOOST_AUTO_TEST_SUITE(Check_Circle)
        
  BOOST_AUTO_TEST_CASE(Action_Move_Point)
  {
    nguyen::Circle circ({10,12},11);
    nguyen::point_t poi;
    poi.x = 100;
    poi.y = 120;
    circ.move(poi);
    BOOST_CHECK_CLOSE(11,circ.getRadius(),epsilon);
    BOOST_CHECK_CLOSE(11*11*M_PI,circ.getArea(),epsilon);
  }

  BOOST_AUTO_TEST_CASE(Action_Scale)
  {
    nguyen::Circle circ({10,12},11);
    circ.scale(4);
    BOOST_CHECK_CLOSE(11*11*M_PI*4*4,circ.getArea(),epsilon);
    BOOST_REQUIRE_EQUAL(11*4,circ.getRadius());
  }

  BOOST_AUTO_TEST_CASE(Invalid_Scale)
  {
    nguyen::Circle circ({10,12},11);
    BOOST_CHECK_THROW(circ.scale(-4),std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Constructor)
  {
    BOOST_CHECK_THROW(nguyen::Circle circ({10,12},-11),std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
        
BOOST_AUTO_TEST_SUITE(Check_Triangle)
        
  BOOST_AUTO_TEST_CASE(Action_Move_dxdy)
  {
    nguyen::Triangle tria({1,2},{20,30},{3,45});
    const double squ = tria.getArea();
    tria.move(20,20);
    BOOST_CHECK_CLOSE(squ,tria.getArea(),epsilon);
  }

  BOOST_AUTO_TEST_CASE(Action_Scale)
  {
    nguyen::Triangle tria({1,2},{20,30},{3,45});
    const double squ = tria.getArea();
    tria.scale(5);
    BOOST_CHECK_CLOSE(squ*5*5,tria.getArea(),epsilon);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Scale)
  {
    nguyen::Triangle tria({1,2},{20,30},{3,45});
    BOOST_CHECK_THROW(tria.scale(-2),std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
