#include "../common/circle.hpp"
#include "../common/rectangle.hpp"
#include "../common/triangle.hpp"
#include "../common/composite-shape.hpp"
#include <memory>
#include <iostream>
#include <stdexcept>

namespace nguyen
{
  void shorten(Shape& obj) //to shorten the code
  {
    obj.getInfoShape();
    obj.move(20,20);
    obj.getInfoShape();
    obj.scale(2);
    obj.getInfoShape();
  }
}

int main()
{
  try
  {
    nguyen::Rectangle rec({5,5},20,30);
    nguyen::Circle circ({10,12},11);
    nguyen::Triangle tria({1,2},{20,30},{3,45});
    std::shared_ptr<nguyen::Shape> objCirc = std::make_shared<nguyen::Circle>(circ);
    std::shared_ptr<nguyen::Shape> objTria = std::make_shared<nguyen::Triangle>(tria);
    std::shared_ptr<nguyen::Shape> objRec = std::make_shared<nguyen::Rectangle>(rec);
    
    nguyen::CompositeShape myShapes(objCirc);
    shorten(myShapes);
    std::cout << std::endl;

    myShapes.addElem(objTria);
    myShapes.addElem(objRec);
    shorten(myShapes);
    std::cout << std::endl;
    myShapes.delElem(1);
    shorten(myShapes);
  }
  catch(const std::invalid_argument & e)
  {
    std::cerr<< e.what() << std::endl;
    return 1;
  }
  catch(const std::out_of_range & e)
  {
    std::cerr<< e.what() << std::endl;
    return 1;
  }
  return 0;
}
