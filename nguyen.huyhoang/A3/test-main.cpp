#define BOOST_TEST_MODULE TestCompositeShape
#include "../common/circle.hpp"
#include "../common/rectangle.hpp"
#include "../common/triangle.hpp"
#include "../common/composite-shape.hpp"
#include <boost/test/included/unit_test.hpp>
#include <cmath>
#include <memory>
#include <stdexcept>

const double epsilon = 0.001;

BOOST_AUTO_TEST_SUITE(CompositeShape_Suite)
       
  BOOST_AUTO_TEST_CASE(Area_Move_Check)
  {
    nguyen::Rectangle rec({5,5},20,30);
    nguyen::Circle circ({10,12},11);
    nguyen::Triangle tria({1,2},{20,30},{3,45});
    std::shared_ptr<nguyen::Shape> objCirc = std::make_shared<nguyen::Circle>(circ);
    std::shared_ptr<nguyen::Shape> objTria = std::make_shared<nguyen::Triangle>(tria);
    std::shared_ptr<nguyen::Shape> objRec = std::make_shared<nguyen::Rectangle>(rec);
    nguyen::CompositeShape myShapes(objCirc);
    myShapes.addElem(objRec);
    myShapes.addElem(objTria);
    myShapes.move(10,12);
    BOOST_CHECK_CLOSE(myShapes.getArea(), circ.getArea() + tria.getArea() + rec.getArea(), epsilon);
  }

  BOOST_AUTO_TEST_CASE(Move_Center_Check)
  {
    nguyen::Rectangle rec({5,5},20,30);
    nguyen::Circle circ({10,12},11);
    std::shared_ptr<nguyen::Shape> objCirc = std::make_shared<nguyen::Circle>(circ);
    std::shared_ptr<nguyen::Shape> objRec = std::make_shared<nguyen::Rectangle>(rec);
    nguyen::CompositeShape myShapes(objCirc);
    myShapes.addElem(objRec);
    const nguyen::point_t constcen = myShapes.getCenter();
    myShapes.move({20,30});
    myShapes.delElem(0);
    const nguyen::point_t currcen = myShapes.getCenter();
    BOOST_CHECK_CLOSE(currcen.x, 5+20-constcen.x, epsilon);
    BOOST_CHECK_CLOSE(currcen.y, 5+30-constcen.y, epsilon);
  }

  BOOST_AUTO_TEST_CASE(Scale_Check)
  {
    nguyen::Rectangle rec({5,5},20,30);
    nguyen::Circle circ({10,12},11);
    std::shared_ptr<nguyen::Shape> objCirc = std::make_shared<nguyen::Circle>(circ);
    std::shared_ptr<nguyen::Shape> objRec = std::make_shared<nguyen::Rectangle>(rec);    
    nguyen::CompositeShape myShapes(objCirc);
    myShapes.addElem(objRec);
    const nguyen::point_t constcen = myShapes.getCenter();
    const nguyen::rectangle_t pastframe = myShapes.getFrameRect();
    myShapes.scale(4);
    const nguyen::rectangle_t currframe = myShapes.getFrameRect();
    BOOST_CHECK_CLOSE(myShapes.getArea(), (circ.getArea() + rec.getArea())*4*4, epsilon);
    BOOST_CHECK_CLOSE(4*pastframe.height, currframe.height, epsilon);
    BOOST_CHECK_CLOSE(4*pastframe.width, currframe.width, epsilon);
    myShapes.delElem(0);
    const nguyen::point_t currcen = myShapes.getCenter();
    BOOST_CHECK_CLOSE(currcen.x, constcen.x+4*(5-constcen.x), epsilon);
    BOOST_CHECK_CLOSE(currcen.y, constcen.y+4*(5-constcen.y), epsilon);
  }
  
  BOOST_AUTO_TEST_CASE(Delete_Shapes_Check)
  {
    nguyen::Rectangle rec({5,5},20,30);
    nguyen::Circle circ({10,12},11);
    nguyen::Triangle tria({1,2},{20,30},{3,45});
    std::shared_ptr<nguyen::Shape> objCirc = std::make_shared<nguyen::Circle>(circ);
    std::shared_ptr<nguyen::Shape> objTria = std::make_shared<nguyen::Triangle>(tria);
    std::shared_ptr<nguyen::Shape> objRec = std::make_shared<nguyen::Rectangle>(rec);    
    nguyen::CompositeShape myShapes(objCirc);
    myShapes.addElem(objRec);
    myShapes.addElem(objTria); 
    myShapes.delElem(1);
    BOOST_REQUIRE_EQUAL(myShapes.getArea(), circ.getArea() + tria.getArea());
    BOOST_REQUIRE_EQUAL(myShapes.getSize(), 2);
  }
  
  BOOST_AUTO_TEST_CASE(Invalid_Scale_Check)
  {
    nguyen::Circle circ({10,12},11);
    std::shared_ptr<nguyen::Shape> objCirc = std::make_shared<nguyen::Circle>(circ);  
    nguyen::CompositeShape myShapes(objCirc);  
    BOOST_CHECK_THROW(myShapes.scale(-2), std::invalid_argument);
  }
  
  BOOST_AUTO_TEST_CASE(Invalid_Delete_Check)
  {
    nguyen::Circle circ({10,12},11);
    nguyen::Rectangle rec({5,5},20,30);
    std::shared_ptr<nguyen::Shape> objCirc = std::make_shared<nguyen::Circle>(circ);  
    std::shared_ptr<nguyen::Shape> objRec = std::make_shared<nguyen::Rectangle>(rec);
    nguyen::CompositeShape myShapes(objCirc);
    myShapes.addElem(objRec);
    BOOST_CHECK_THROW(myShapes.delElem(5), std::out_of_range);
  }
  
BOOST_AUTO_TEST_SUITE_END()
