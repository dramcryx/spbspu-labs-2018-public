#include "circle.hpp"
#include <iostream>
#include <cmath>

nguyen::Circle::Circle(point_t pos, double rad):
  cen_(pos),
  r_(rad),
  angle_(0)
{
  if (r_ <= 0) 
  {
    throw std::invalid_argument("Invalid Circle");
  }    
}

double nguyen::Circle::getArea() const
{
  return r_*r_*M_PI;
}

double nguyen::Circle::getRadius() const
{
  return r_;
}

nguyen::point_t nguyen::Circle::getCenter() const
{
  return cen_;
}

nguyen::rectangle_t nguyen::Circle::getFrameRect() const
{
  return {cen_, 2*r_, 2*r_};
}

void nguyen::Circle::move(const point_t& c)
{
  cen_ = c;
}

void nguyen::Circle::move(const double& dx, const double& dy)
{
  cen_.x += dx;
  cen_.y += dy;
}

void nguyen::Circle::scale(double k)
{
  if (k <= 0)
  {
    throw std::invalid_argument("Invalid scale of the circle");
  }
  r_ *= k;
}

void nguyen::Circle::rotate(double deg)
{
  angle_ += deg;
  angle_ = fmod(angle_, 360);
}

void nguyen::Circle::getInfoShape()
{
  std::cout << "This shape is Circle:" << std::endl;
  std::cout << "The center of the circle is: (" << cen_.x << ";"
      << cen_.y << ")" << std::endl;
  std::cout << "The radius of the circle is:" << r_ << std::endl;
  std::cout << "The area of the circle is:" << this->getArea() << std::endl;
  rectangle_t rect = this->getFrameRect();
  std::cout << "The frame of the circle is: Center (" << rect.pos.x 
      << ";" << rect.pos.y << ") and Width:" << rect.width
          << " and Height:" << rect.height << std::endl;
  std::cout << "The current angle is:" << angle_ << std::endl;
}
