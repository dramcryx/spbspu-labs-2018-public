#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace nguyen
{
class Circle: public Shape
{
  public:
    Circle(point_t pos, double rad);
    double getArea() const override;
    double getRadius() const;
    point_t getCenter() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t& c) override;
    void move(const double& dx, const double& dy) override;
    void getInfoShape() override;
    void scale(double k) override;
    void rotate(double deg) override;
  private:
    point_t cen_;
    double r_;
    double angle_;
};
}
#endif
