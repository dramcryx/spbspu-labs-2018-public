#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP
#include "shape.hpp"
#include <memory>

namespace nguyen
{
  class CompositeShape: public Shape
  {
    public:
      CompositeShape(const std::shared_ptr<Shape>& obj);
      CompositeShape(const CompositeShape & arobj);
      ~CompositeShape();
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      void move(const point_t& c) override;
      void move(const double& dx, const double& dy) override;
      void scale(double k) override;
      void addElem(const std::shared_ptr<Shape>& obj);
      void delElem(const int& index);
      int getSize() const;
      point_t getCenter() const override;
      void rotate(double deg) override;
      void getInfoShape() override;
    private:
      std::unique_ptr<std::shared_ptr<Shape> []> arr_;
      int size_;
      double angle_;
  };
}
#endif
