#include "matrix.hpp"
#include <cstdlib>
#include <memory>
#include <cmath>
#include <iostream>
#include <stdexcept>

nguyen::Matrix::Matrix(const std::shared_ptr<Shape>& obj):
  matrix_(new std::shared_ptr<Shape> [1]),
  columns_(1),
  rows_(1)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Invalid object");
  }
  matrix_[0]=obj;
}

nguyen::Matrix::Matrix(const Matrix& matr):
  matrix_(nullptr),
  columns_(0),
  rows_(0)
  
{
  *this = matr;
}

nguyen::Matrix::~Matrix()
{
  matrix_.reset();
  columns_ = 0;
  rows_ = 0;
  matrix_ = nullptr;
}

nguyen::Matrix & nguyen::Matrix::operator =(const Matrix& matr)
{
  if (this != &matr)
  {
    this->columns_ = matr.columns_;
    this->rows_ = matr.rows_;
    std::unique_ptr<std::shared_ptr<Shape> []> tempShape(new std::shared_ptr<Shape> [matr.rows_*matr.columns_]);
    for (int i=0; i<(columns_*rows_); ++i)
    {
      tempShape[i]= matr.matrix_[i];
    }
    this->matrix_.swap(tempShape);
  }
  return *this;
}

bool nguyen::Matrix::operator ==(const Matrix& matr) const
{
  if ((rows_ == matr.rows_) && (columns_ == matr.columns_))
  {
    bool equal = true;  
    for (int i = 0; i < (rows_*columns_); ++i)
    {
      if (matrix_[i] != matr.matrix_[i])
      {
        equal = false;
        break;
      }
    }
    if (equal)
    {
      return true;
    }
  }
  return false;
}

bool nguyen::Matrix::operator !=(const Matrix& matr) const
{
  if ((rows_ != matr.rows_) || (columns_ != matr.columns_))   
  {
    return true;
  }
  else
  {
    bool equal = true;
    for (int i=0; i < (rows_*columns_); ++i)
    {
      if (matrix_[i] != matr.matrix_[i])
      {
        equal = false;    
        break;
      }
    }
    if (!(equal))
    {
      return true;
    }
  }
  return false;
}

std::unique_ptr<std::shared_ptr<nguyen::Shape> []> nguyen::Matrix::operator[](const int index) const
{
  if ((index < 0) || (index >= rows_))
  {
    throw std::out_of_range("Invalid index");  
  }
  std::unique_ptr<std::shared_ptr<nguyen::Shape> []> arow(new std::shared_ptr<nguyen::Shape> [columns_]);
  for (int i=0; i<columns_; ++i)
  {
    arow[i] = matrix_[index*columns_+i];
  }
  return arow;
}

void nguyen::Matrix::addShape(const std::shared_ptr<Shape>& obj)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("Invalid object");
  }
  int newRow = 1;
  for (int i=0; i<(rows_*columns_); ++i)
  {
    if (checkIntersect(matrix_[i], obj) == true)
    {
      newRow = i/columns_ +2;
    }
  }
  int tempR = rows_;
  int tempC = columns_;
  int desireC = 0;
  if (newRow > rows_)
  {
    tempR++;
    desireC = columns_;    
  }
  else
  {
    for (int j=(newRow-1)*columns_; j<(newRow*columns_); ++j)
    {
      if (matrix_[j] == nullptr)
      {
        desireC++;
      }
    }
    if (desireC == 0)
    {
      tempC++;
      desireC = 1;
    }
  }
  std::unique_ptr<std::shared_ptr<Shape> []> newMatrix(new std::shared_ptr<Shape> [tempR*tempC]);
  for (int i=0; i<tempR; ++i)
  {
    for (int j=0; j<tempC; ++j)
    {
      if (i>= rows_ || j>=columns_)
      {
        newMatrix[i*tempC +j] = nullptr;
        continue;
      }
      newMatrix[i*tempC+j] = matrix_[i*columns_+j];
    }
  }
  newMatrix[newRow*tempC-desireC]=obj;
  matrix_.swap(newMatrix);
  columns_ = tempC;
  rows_ = tempR;
}

bool nguyen::Matrix::checkIntersect(const std::shared_ptr<Shape>& obj1, const std::shared_ptr<Shape>& obj2)
{
  if (obj1 == nullptr)
  {
    return false;
  }
  bool checkwid = false;
  rectangle_t obb1 = obj1->getFrameRect();
  rectangle_t obb2 = obj2->getFrameRect();
  double x1 = obb1.pos.x;
  double x2 = obb2.pos.x;
  double wid1 = obb1.width;
  double wid2 = obb2.width;
  double c = std::max(x1,x2) - std::min(x1,x2);
  if (c < (wid1+wid2)/2)
  {
    checkwid = true;
  }
  bool checkhei = false;
  double y1 = obb1.pos.y;
  double y2 = obb2.pos.y;
  double hei1 = obb1.height;
  double hei2 = obb2.height;
  double d = std::max(y1,y2) - std::min(y1,y2);
  if (d < (hei1+hei2)/2)
  {
    checkhei = true;
  }
  bool checksum = false;
  if ((checkhei == true) && (checkwid == true))
  {
    checksum = true;
  }
  return checksum;
}

int nguyen::Matrix::getRows() const
{
  return rows_;
}

int nguyen::Matrix::getColumns() const
{ 
  return columns_;
}
