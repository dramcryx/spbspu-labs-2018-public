#ifndef MATRIX_HPP
#define MATRIX_HPP
#include "shape.hpp"
#include <memory>

namespace nguyen
{
  class Matrix
  {
    public:
      Matrix(const std::shared_ptr<Shape>& obj);
      Matrix(const Matrix & matr);
      ~Matrix();
      Matrix & operator=(const Matrix & matr);
      bool operator==(const Matrix & matr) const;
      bool operator!=(const Matrix & matr) const;
      std::unique_ptr<std::shared_ptr<Shape> []> operator[](const int index) const;
      void addShape(const std::shared_ptr<Shape>& obj);
      int getRows() const;
      int getColumns() const;
    private:
      std::unique_ptr<std::shared_ptr<Shape> []> matrix_;
      int columns_;
      int rows_;
      bool checkIntersect(const std::shared_ptr<Shape>& obj1, const std::shared_ptr<Shape>& obj2);
  };
}
#endif
