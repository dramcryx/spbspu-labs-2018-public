#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"

namespace nguyen
{
class Rectangle: public Shape
{
  public:
    Rectangle(point_t pos, double width, double lenght);
    double getArea() const override;
    point_t getCenter() const override;
    double getHeight() const;
    double getWidth() const;
    rectangle_t getFrameRect() const override;
    void move(const point_t& c) override;
    void move(const double& dx, const double& dy) override;
    void scale(double k) override;
    void rotate(double deg) override;
    void getInfoShape() override;
  private:
    double wid_;
    double hei_;
    point_t cen_;
    double angle_;
};
}
#endif
