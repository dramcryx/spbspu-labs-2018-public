#include "triangle.hpp"
#include <iostream>
#include <cmath>

nguyen::Triangle::Triangle(point_t poA, point_t poB, point_t poC):
  a_(poA),
  b_(poB),
  c_(poC),
  g_({(poA.x+poB.x+poC.x)/3,(poA.y+poB.y+poC.y)/3}),
  ab_(sqrt((poA.x-poB.x)*(poA.x-poB.x)+(poA.y-poB.y)*(poA.y-poB.y))),
  bc_(sqrt((poB.x-poC.x)*(poB.x-poC.x)+(poB.y-poC.y)*(poB.y-poC.y))),
  ca_(sqrt((poC.x-poA.x)*(poC.x-poA.x)+(poC.y-poA.y)*(poC.y-poA.y))),
  angle_(0)
{}
  
nguyen::point_t nguyen::Triangle::getCenter() const
{   
  point_t poi = this->getFrameRect().pos;
  return poi;
}

nguyen::point_t nguyen::Triangle::getCenterG() const
{
  return g_;  
}

void nguyen::Triangle::move(const point_t& c)
{
  const point_t cen = this->getCenter();
  double p = c.x - cen.x;
  double q = c.y - cen.y;
  a_.x += p;
  b_.x += p;
  c_.x += p;
  g_.x += p;
  a_.y += q;
  b_.y += q;
  c_.y += q;
  g_.y += q;
}

void nguyen::Triangle::move(const double& dx, const double& dy)
{
  g_.x += dx;
  g_.y += dy;
  a_.x += dx;
  b_.x += dx;
  c_.x += dx;
  a_.y += dy;
  b_.y += dy;
  c_.y += dy;
}

void nguyen::Triangle::scale(double k)
{
  if (k <= 0) 
  {
    throw std::invalid_argument("Invalid scale of the Triangle");
  } 
  ab_ *= k;
  bc_ *= k;
  ca_ *= k;
  point_t cc;
  cc = this->getFrameRect().pos;
  a_.x = cc.x + k*(a_.x - cc.x);
  a_.y = cc.y + k*(a_.y - cc.y);
  b_.x = cc.x + k*(b_.x - cc.x);
  b_.y = cc.y + k*(b_.y - cc.y);
  c_.x = cc.x + k*(c_.x - cc.x);
  c_.y = cc.y + k*(c_.y - cc.y);
  g_.x = cc.x + k*(g_.x - cc.x);
  g_.y = cc.y + k*(g_.y - cc.y);
}

void nguyen::Triangle::getLengths() const
{
  std::cout << "3 parameters of the triangle are:" << ab_ 
      << "," << bc_ << "," << ca_ << std::endl;
}

double nguyen::Triangle::getArea() const
{
  double s;
  s = sqrt((ab_+bc_+ca_)*(ab_+bc_-ca_)*(ab_-bc_+ca_)*(bc_+ca_-ab_))/4;
  return s;
}

nguyen::rectangle_t nguyen::Triangle::getFrameRect() const
{
  double a[3] = {a_.x, b_.x, c_.x};
  double left = a[0];
  for (int i=1; i<2; i++)
  {
    if (a[i] < left)
    {
      left = a[i];
    }
  }
  double right = a[0];
  for (int i=1; i<2; i++)
  {
    if (a[i] > right)
    {
      right = a[i];
    }
  }
  double b[3] = {a_.y, b_.y, c_.y};
  double under = b[0];
  for (int i=1; i<2; i++)
  {
    if (b[i] < under)
    {
      under = b[i];
    }
  }
  double beneath = b[0];
  for (int i=1; i<2; i++)
  {
    if (b[i] > beneath)
    {
      beneath = b[i];
    }
  }
  double widthR;
  widthR = right - left;
  double heightR;
  heightR = beneath - under;
  point_t cenR;
  cenR.x = (right + left)/2;
  cenR.y = (under + beneath)/2;
  return {cenR, widthR, heightR};
}

void nguyen::Triangle::rotate(double deg)
{
  angle_ += deg;
  angle_ = fmod(angle_, 360); 
  const double sindeg = sin(deg*M_PI/180);
  const double cosdeg = cos(deg*M_PI/180);
  point_t cc;
  cc = this->getFrameRect().pos;
  a_ = {cc.x + (a_.x - cc.x)*cosdeg - (a_.y - cc.y)*sindeg, cc.y 
      + (a_.y - cc.y)*cosdeg + (a_.x - cc.x)*sindeg};
  b_ = {cc.x + (b_.x - cc.x)*cosdeg - (b_.y - cc.y)*sindeg, cc.y 
      + (b_.y - cc.y)*cosdeg + (b_.x - cc.x)*sindeg};
  c_ = {cc.x + (c_.x - cc.x)*cosdeg - (c_.y - cc.y)*sindeg, cc.y 
      + (c_.y - cc.y)*cosdeg + (c_.x - cc.x)*sindeg};
  g_ = {cc.x + (g_.x - cc.x)*cosdeg - (g_.y - cc.y)*sindeg, cc.y 
      + (g_.y - cc.y)*cosdeg + (g_.x - cc.x)*sindeg};
}

void nguyen::Triangle::getInfoShape()
{
  std::cout << "This shape is Triangle:" << std::endl;
  this->getLengths();
  std::cout << "The center(G) of the triangle is: (" << g_.x
      << ";" << g_.y << ")" << std::endl;
  std::cout << "The area of the triangle is " << this->getArea() << std::endl;
  rectangle_t fram = this->getFrameRect();
  std::cout << "The frame of the rectangle is: Center (" << fram.pos.x
      << ";" << fram.pos.y << ") and Width " << fram.width
          << " and Height " << fram.height << std::endl;
  std::cout << "The current angle is:" << angle_ << std::endl;
}
