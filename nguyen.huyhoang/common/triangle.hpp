#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace nguyen
{
class Triangle: public Shape
{
  public:
    Triangle(point_t poA, point_t poB, point_t poC);
    point_t getCenter() const override;
    point_t getCenterG() const;
    void move(const point_t& c) override;
    void move(const double& dx, const double& dy) override;
    void scale(double k) override;
    void getLengths () const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void rotate(double deg) override;
    void getInfoShape() override;
  private:
    point_t a_;
    point_t b_;
    point_t c_;
    point_t g_;
    double ab_;
    double bc_;
    double ca_;
    double angle_;
};
}
#endif
