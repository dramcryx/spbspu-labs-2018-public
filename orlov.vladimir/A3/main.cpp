#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

using namespace orlov;
using namespace std;

int getRectInfo(Rectangle objrect)
{
  cout << "Rectangle height: " << objrect.getFrameRect().height << ", ";
  cout << "Width: " << objrect.getFrameRect().width;
  cout << " and center: (" << objrect.getPos().x << ", " << objrect.getPos().y << ")" << endl;
  cout << "Rectangle's area: " << objrect.getArea() << endl;
  return 0;
}

int getCircInfo(Circle objcircle)
{
  cout << "Circle limit for height: " << objcircle.getFrameRect().height << ", ";
  cout << "limit for width: " << objcircle.getFrameRect().width;
  cout << " and center: (" << objcircle.getPos().x << ", " << objcircle.getPos().y << ")" << endl;
  cout << "Circle's area: " << objcircle.getArea() << endl;
  return 0;
}

int getTriangleInfo(Triangle objtriangle)
{
  cout << "Triangle limit for height: " << objtriangle.getFrameRect().height << ", ";
  cout << "limit for width: " << objtriangle.getFrameRect().width;
  cout << " and center: (" << objtriangle.getPos().x << ", " << objtriangle.getPos().y << ")" << endl;
  cout << "Triangle's area: " << objtriangle.getArea() << endl;
  return 0;
}

int main()
{
  try
  {
    Rectangle objrect({5, 11, {3, 6}});
    getRectInfo(objrect);
    Circle objcircle(3, {1, 1});
    getCircInfo(objcircle);
    Triangle objtriangle({1, 1}, {3, 3}, {6, 2});
    getTriangleInfo(objtriangle);

    shared_ptr<Shape> rectPtr = make_shared<Rectangle>(objrect);
    shared_ptr<Shape> circPtr = make_shared<Circle>(objcircle);
    shared_ptr<Shape> trianPtr = make_shared<Triangle>(objtriangle);

    CompositeShape compShape(rectPtr);
    cout << "\tArea of composite shape after adding rectangle = " << compShape.getArea() << endl;

    compShape.addShape(circPtr);
    cout << "\tArea of composite shape after adding circle = " << compShape.getArea() << endl;

    compShape.addShape(trianPtr);
    cout << "\tArea of composite shape after adding triangle = " << compShape.getArea() << endl;

    compShape.scale(2);
    cout << "\tArea of composite shape after scale with k = 2 = " << compShape.getArea() << endl;
  }
  catch(std::invalid_argument & error)
  {
    cerr << error.what() << endl;
    return 1;
  }

  return 0;
}
