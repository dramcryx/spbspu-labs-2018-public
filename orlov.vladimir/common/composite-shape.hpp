#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>


namespace orlov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape(std::shared_ptr<Shape> newShape);
    CompositeShape();
    void addShape(const std::shared_ptr<Shape> newShape);
    void removeShape(const int index);

    point_t getPos() noexcept override;
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t & point) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double factor) override;
    void rotate(const double angle) override;
    int getCount();

  private:
    int count_;
    std::unique_ptr<std::shared_ptr<Shape>[]> mass_;
  };
}

#endif
