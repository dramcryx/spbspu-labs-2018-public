#include "rectangle.hpp"
#include <stdexcept>
#include <cmath>

using namespace orlov;

orlov::Rectangle::Rectangle(const rectangle_t & rctng):
rec_(rctng),
currentAngle_(0.0)
{
  if (rctng.height <= 0.0)
  {
    throw std::invalid_argument("Height must be > 0");
  }
  if (rctng.width <= 0.0)
  {
    throw std::invalid_argument("Width must be > 0");
  }
}

double orlov::Rectangle::getArea() const noexcept
{
  return (rec_.width * rec_.height);
}

point_t orlov::Rectangle::getPos() noexcept
{
  return rec_.pos;
}

rectangle_t orlov::Rectangle::getFrameRect() const noexcept
{
  return rec_;
}

void orlov::Rectangle::move(const point_t & point) noexcept
{
  rec_.pos = point;
}

void orlov::Rectangle::move(const double dx, const double dy) noexcept
{
  rec_.pos.x += dx;
  rec_.pos.y += dy;
}

void orlov::Rectangle::scale(const double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Factor must be > 0");
  }

  rec_.height *= factor;
  rec_.width *= factor;
}

void orlov::Rectangle::rotate(const double angle)
{
  currentAngle_ = std::fmod(currentAngle_ + angle, 360.0);
}

double orlov::Rectangle::getAngle()
{
  return currentAngle_;
}
