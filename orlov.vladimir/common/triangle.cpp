#include "triangle.hpp"
#include <stdexcept>
#include <math.h>
#include <algorithm>

using namespace orlov;

orlov::Triangle::Triangle(const point_t & A, const point_t & B, const point_t & C):
A_(A),
B_(B),
C_(C),
centre_({ ((A.x + B.x + C.x) / 3.0), ((A.y + B.y + C.y) / 3.0) })
{
  double AB = sqrt(pow(B.x - A.x, 2) + pow(B.y - A.y, 2));
  double BC = sqrt(pow(C.x - B.x, 2) + pow(C.y - B.y, 2));
  double AC = sqrt(pow(C.x - A.x, 2) + pow(C.y - A.y, 2));

  if ((AB + BC) == AC)
  {
    throw std::invalid_argument("(AB + BC) mustn't be = AC");
  }
  else if ((AC + BC) == AB)
  {
    throw std::invalid_argument("(AC + BC) mustn't be = AB");
  }
  else if ((AB + AC) == BC)
  {
    throw std::invalid_argument("(AB + AC) mustn't be = BC");
  }
}

double orlov::Triangle::getArea() const noexcept
{
  return (abs((((A_.x - C_.x) * (B_.y - C_.y))
    - ((A_.y - C_.y) * (B_.x - C_.x))) / 2.0));
}

point_t orlov::Triangle::getPos() noexcept
{
  return centre_;
}

rectangle_t orlov::Triangle::getFrameRect() const noexcept
{
  double width = std::max(std::max(A_.x, B_.x), C_.x)
    - std::min(std::min(A_.x, B_.x), C_.x);
  double height = std::max(std::max(A_.y, B_.y), C_.y)
    - std::min(std::min(A_.y, B_.y), C_.y);

  double shiftX = width / 2.0 - (centre_.x - std::min(std::min(A_.x, B_.x), C_.x));
  double shiftY = height / 2.0 - (centre_.y - std::min(std::min(A_.y, B_.y), C_.y));

  return rectangle_t({width, height, {centre_.x + shiftX, centre_.y + shiftY}});
}

void orlov::Triangle::move(const point_t & point) noexcept
{
  double dx = point.x - centre_.x;
  double dy = point.y - centre_.y;

  A_.x += dx;
  A_.y += dy;
  B_.x += dx;
  B_.y += dy;
  C_.x += dx;
  C_.y += dy;

  centre_ = point;
}

void orlov::Triangle::move(const double dx, const double dy) noexcept
{
  A_.x += dx;
  A_.y += dy;
  B_.x += dx;
  B_.y += dy;
  C_.x += dx;
  C_.y += dy;

  centre_.x += dx;
  centre_.y += dy;
}

void orlov::Triangle::scale(const double factor)
{
  if (factor <= 0.0)
  {
    throw std::invalid_argument("Factor must be > 0");
  }

  A_.x = centre_.x + ((A_.x - centre_.x) * factor);
  A_.y = centre_.y + ((A_.y - centre_.y) * factor);
  B_.x = centre_.x + ((B_.x - centre_.x) * factor);
  B_.y = centre_.y + ((B_.y - centre_.y) * factor);
  C_.x = centre_.x + ((C_.x - centre_.x) * factor);
  C_.y = centre_.y + ((C_.y - centre_.y) * factor);
}

void orlov::Triangle::rotate(const double angle)
{
  double tmpAngle = (angle * M_PI) / 180.0;
  point_t newA;
  point_t newB;
  point_t newC;
  newA.x = centre_.x + (A_.x - centre_.x) * cos(tmpAngle) - (A_.y - centre_.y) * sin(tmpAngle);
  newA.y = centre_.y + (A_.x - centre_.x) * sin(tmpAngle) + (A_.y - centre_.y) * cos(tmpAngle);
  A_ = newA;
  newB.x = centre_.x + (B_.x - centre_.x) * cos(tmpAngle) - (B_.y - centre_.y) * sin(tmpAngle);
  newB.y = centre_.y + (B_.x - centre_.x) * sin(tmpAngle) + (B_.y - centre_.y) * cos(tmpAngle);
  B_ = newB;
  newC.x = centre_.x + (C_.x - centre_.x) * cos(tmpAngle) - (C_.y - centre_.y) * sin(tmpAngle);
  newC.y = centre_.y + (C_.x - centre_.x) * sin(tmpAngle) + (C_.y - centre_.y) * cos(tmpAngle);
  C_ = newC;
}
