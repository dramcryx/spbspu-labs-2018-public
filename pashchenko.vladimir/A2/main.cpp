#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

int main()
{
  pashchenko::point_t pointRect = { 45.6, 13.0 };
  pashchenko::point_t pointCircle = { 10.5, -3.0 };

  pashchenko::point_t pointT1 = { 1, 2 };
  pashchenko::point_t pointT2 = { 7.5, 6 };
  pashchenko::point_t pointT3 = { 10.5, 5 };

  pashchenko::Rectangle rect(pointRect, 20, 5.5);
  pashchenko::Triangle triangle(pointT1, pointT2, pointT3);
  pashchenko::Circle circle(pointCircle, 4.5);

  std::cout << "Rectangle area (Before rescaling):  " << rect.getArea() << std::endl;
  rect.scale(5);
  std::cout << "Rectangle (After rescaling, k = 5):" << rect.getArea() << std::endl;

  std::cout << "Triangle area (Before rescaling): " << triangle.getArea() << std::endl;
  triangle.scale(3);
  std::cout << "Triangle (After rescaling, k = 3): " << triangle.getArea() << std::endl;

  std::cout << "Circle area (Before rescaling): " << circle.getArea() << std::endl;
  circle.scale(2.5);
  std::cout << "Circle area (After rescaling, k = 2,5): " << circle.getArea() << std::endl;

  return 0;
}
