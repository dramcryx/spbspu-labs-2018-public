#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>

#include "rectangle.hpp"
#include "triangle.hpp"
#include "circle.hpp"

BOOST_AUTO_TEST_SUITE(RectangleSuite)

BOOST_AUTO_TEST_CASE(MovingSquareConst)
{
  pashchenko::Rectangle rect({2, 3}, 5, 8);
  rect.move({10, 3});
  BOOST_CHECK_CLOSE(rect.getArea(), 40, 0.001);
}

BOOST_AUTO_TEST_CASE(Scale)
{
  pashchenko::Rectangle rect({2, 3}, 5, 8);
  rect.scale(2);
  BOOST_CHECK_CLOSE(rect.getArea(), 160, 0.001);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_a)
{
  pashchenko::Rectangle rect({2, 3}, 5, 8);
  BOOST_CHECK_THROW(rect.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_b)
{
  pashchenko::Rectangle rect({2, 3}, 5, 8);
  BOOST_CHECK_THROW(rect.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorWrongParameter_a)
{
  BOOST_CHECK_THROW(pashchenko::Rectangle({0, 1}, -3, 5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorWrongParameter_b)
{
  BOOST_CHECK_THROW(pashchenko::Rectangle({0, 1}, 3, -5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorWrongParameter_c)
{
  BOOST_CHECK_THROW(pashchenko::Rectangle({0, 1}, 0, 5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorWrongParameter_d)
{
  BOOST_CHECK_THROW(pashchenko::Rectangle({0, 1}, 3, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(CircleSuit)

BOOST_AUTO_TEST_CASE(MovingSquareConst)
{
  pashchenko::Circle circle({4, 8}, 3);
  double squareBefore = circle.getArea();
  circle.move({0, 9});
  BOOST_CHECK_CLOSE(circle.getArea(), squareBefore, 0.001);
}

BOOST_AUTO_TEST_CASE(Scale)
{
  pashchenko::Circle circle({4, 8}, 3);
  double squareBefore = circle.getArea();
  circle.scale(3);
  BOOST_CHECK_CLOSE(circle.getArea(), squareBefore * 3 * 3, 0.001);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_a)
{
  pashchenko::Circle circle({4, 8}, 3);
  BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_b)
{
  pashchenko::Circle circle({4, 8}, 3);
  BOOST_CHECK_THROW(circle.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorWrongParameter_a)
{
  BOOST_CHECK_THROW(pashchenko::Circle({4, 8}, -3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TriangleSuite)

BOOST_AUTO_TEST_CASE(MovingSquareConst)
{
  pashchenko::Triangle triangle({ 2, 3 }, { 5, 9 }, { 8, 1 });
  double squareBefore = triangle.getArea();
  triangle.move(10, 3);
  BOOST_CHECK_CLOSE(triangle.getArea(), squareBefore, 0.001);
}

BOOST_AUTO_TEST_CASE(Scale)
{
  pashchenko::Triangle triangle({ 2, 3 }, { 5, 9 }, { 8, 1 });
  double squareBefore = triangle.getArea();
  triangle.scale(2);
  BOOST_CHECK_CLOSE(triangle.getArea(), squareBefore * 4, 0.001);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_a)
{
  pashchenko::Triangle triangle({ 2, 3 }, { 5, 9 }, { 8, 1 });
  BOOST_CHECK_THROW(triangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_b)
{
  pashchenko::Triangle triangle({ 2, 3 }, { 5, 9 }, { 8, 1 });
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
