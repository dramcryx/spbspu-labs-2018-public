#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>

#include "rectangle.hpp"
#include "triangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

BOOST_AUTO_TEST_SUITE(RectangleSuite)

BOOST_AUTO_TEST_CASE(MovingSquareConst)
{
  pashchenko::Rectangle rect({2, 3}, 5, 8);
  rect.move({10, 3});
  BOOST_CHECK_CLOSE(rect.getArea(), 40, 0.001);
}

BOOST_AUTO_TEST_CASE(Scale)
{
  pashchenko::Rectangle rect({2, 3}, 5, 8);
  rect.scale(2);
  BOOST_CHECK_CLOSE(rect.getArea(), 160, 0.001);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_a)
{
  pashchenko::Rectangle rect({2, 3}, 5, 8);
  BOOST_CHECK_THROW(rect.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_b)
{
  pashchenko::Rectangle rect({2, 3}, 5, 8);
  BOOST_CHECK_THROW(rect.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorWrongParameter_a)
{
  BOOST_CHECK_THROW(pashchenko::Rectangle({0, 1}, -3, 5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorWrongParameter_b)
{
  BOOST_CHECK_THROW(pashchenko::Rectangle({0, 1}, 3, -5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorWrongParameter_c)
{
  BOOST_CHECK_THROW(pashchenko::Rectangle({0, 1}, 0, 5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorWrongParameter_d)
{
  BOOST_CHECK_THROW(pashchenko::Rectangle({0, 1}, 3, 0), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(CircleSuit)

BOOST_AUTO_TEST_CASE(MovingSquareConst)
{
  pashchenko::Circle circle({4, 8}, 3);
  double squareBefore = circle.getArea();
  circle.move({0, 9});
  BOOST_CHECK_CLOSE(circle.getArea(), squareBefore, 0.001);
}

BOOST_AUTO_TEST_CASE(Scale)
{
  pashchenko::Circle circle({4, 8}, 3);
  double squareBefore = circle.getArea();
  circle.scale(3);
  BOOST_CHECK_CLOSE(circle.getArea(), squareBefore * 3 * 3, 0.001);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_a)
{
  pashchenko::Circle circle({4, 8}, 3);
  BOOST_CHECK_THROW(circle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_b)
{
  pashchenko::Circle circle({4, 8}, 3);
  BOOST_CHECK_THROW(circle.scale(-5), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ConstructorWrongParameter_a)
{
  BOOST_CHECK_THROW(pashchenko::Circle({4, 8}, -3), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TriangleSuite)

BOOST_AUTO_TEST_CASE(MovingSquareConst)
{
  pashchenko::Triangle triangle({ 2, 3 }, { 5, 9 }, { 8, 1 });
  double squareBefore = triangle.getArea();
  triangle.move(10, 3);
  BOOST_CHECK_CLOSE(triangle.getArea(), squareBefore, 0.001);
}

BOOST_AUTO_TEST_CASE(Scale)
{
  pashchenko::Triangle triangle({ 2, 3 }, { 5, 9 }, { 8, 1 });
  double squareBefore = triangle.getArea();
  triangle.scale(2);
  BOOST_CHECK_CLOSE(triangle.getArea(), squareBefore * 4, 0.001);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_a)
{
  pashchenko::Triangle triangle({ 2, 3 }, { 5, 9 }, { 8, 1 });
  BOOST_CHECK_THROW(triangle.scale(0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ScaleWrongParameter_b)
{
  pashchenko::Triangle triangle({ 2, 3 }, { 5, 9 }, { 8, 1 });
  BOOST_CHECK_THROW(triangle.scale(-1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(CompositeShapeSuite)

BOOST_AUTO_TEST_CASE(AddOneShape)
{
  std::shared_ptr<pashchenko::Shape> rectC = std::make_shared<pashchenko::Rectangle>(pashchenko::Rectangle({ 45.6, 13.0 }, 20, 5.5));
  pashchenko::CompositeShape exampleCS;
  exampleCS.addElement(rectC);
  BOOST_REQUIRE_EQUAL(exampleCS.getSize(), 1);
}

BOOST_AUTO_TEST_CASE(AddTwoShapes)
{
  std::shared_ptr<pashchenko::Shape> rectC = std::make_shared<pashchenko::Rectangle>(pashchenko::Rectangle({ 45.6, 13.0 }, 20, 5.5));
  std::shared_ptr<pashchenko::Shape> circleC = std::make_shared<pashchenko::Circle>(pashchenko::Circle({ 10.5, -3.0 }, 4.5));
  pashchenko::CompositeShape exampleCS;
  exampleCS.addElement(rectC);
  exampleCS.addElement(circleC);
  BOOST_REQUIRE_EQUAL(exampleCS.getSize(), 2);
}

BOOST_AUTO_TEST_CASE(CheckUpdatedArea)
{
  std::shared_ptr<pashchenko::Shape> rectC = std::make_shared<pashchenko::Rectangle>(pashchenko::Rectangle({ 45.6, 13.0 }, 20, 5.5));
  std::shared_ptr<pashchenko::Shape> circleC = std::make_shared<pashchenko::Circle>(pashchenko::Circle({ 10.5, -3.0 }, 4.5));
  pashchenko::CompositeShape exampleCS;
  exampleCS.addElement(rectC);
  exampleCS.addElement(circleC);
  BOOST_CHECK_CLOSE(exampleCS.getArea(), 173.617, 0.001);
}

BOOST_AUTO_TEST_SUITE_END()
