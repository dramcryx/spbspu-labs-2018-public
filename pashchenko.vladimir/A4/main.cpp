#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace pashchenko;

int main()
{
  point_t pointRect = { 0, 0 };
  point_t pointCircle = { 10.5, -3.0 };

  point_t pointTF = { 1, 2 };
  point_t pointTS = { 7.5, 6 };
  point_t pointTT = { 10.5, 5 };

  Rectangle rect(pointRect, 20, 5.5);
  Triangle triangle(pointTF, pointTS, pointTT);
  Circle circle(pointCircle, 4.5);
  Circle circleS({ 45.6, 13.0 }, 4.5);
  Circle circleT({ 42.3, 15.0 }, 4.5);

  std::shared_ptr<Shape> rectC = std::make_shared<Rectangle>(rect);
  std::shared_ptr<Shape> triangleC = std::make_shared<Triangle>(triangle);
  std::shared_ptr<Shape> circleC = std::make_shared<Circle>(circle);
  std::shared_ptr<Shape> circleCS = std::make_shared<Circle>(circleS);
  std::shared_ptr<Shape> circleCT = std::make_shared<Circle>(circleT);

  CompositeShape cs;
  cs.addElement(rectC);
  std::cout << " X: " << cs.getFrameRect().pos.x << " Y: " << cs.getFrameRect().pos.y << std::endl;
  std::cout << " W: " << cs.getFrameRect().width << " H: " << cs.getFrameRect().height << std::endl;
  cs.rotate(90);
  std::cout << " X: " << cs.getFrameRect().pos.x << " Y: " << cs.getFrameRect().pos.y << std::endl;
  std::cout << " W: " << cs.getFrameRect().width << " H: " << cs.getFrameRect().height << std::endl;

  Matrix matrixNew(rectC);
  matrixNew.addShape(triangleC);
  matrixNew.addShape(circleC);
  matrixNew.addShape(circleCS);
  matrixNew.addShape(circleCT);

  return 0;
}
