#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include "circle.hpp"

pashchenko::Circle::Circle(const point_t & position, const double & radius) :
  radius_(radius),
  currPos_(position)
{
  if (radius_ < 0.0)
    throw std::invalid_argument("Radius is less than zero!");
}

void pashchenko::Circle::move(const point_t & newP) noexcept
{
  currPos_ = newP;
}

void pashchenko::Circle::move(const double & nX, const double & nY) noexcept
{
  currPos_.x += nX;
  currPos_.y += nY;
}

double pashchenko::Circle::getArea() const noexcept
{
  return radius_ * radius_ * M_PI;
}

pashchenko::rectangle_t pashchenko::Circle::getFrameRect() const noexcept
{
  return { currPos_, 2 * radius_, 2 * radius_ };
}

pashchenko::point_t pashchenko::Circle::getPosition() const noexcept
{
  return currPos_;
}

void pashchenko::Circle::scale(const double coefficient)
{
  if (coefficient <= 0)
    throw std::invalid_argument("Coefficient is less than or equal to zero");
  radius_ *= coefficient;
}

void pashchenko::Circle::rotate(const double /*newAngle*/) noexcept
{
}
