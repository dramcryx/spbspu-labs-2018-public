#ifndef CIRCLE_HPP_FILE
#define CIRCLE_HPP_FILE

#include "shape.hpp"

namespace pashchenko {
  class Circle : public Shape
  {
  public:
    Circle(const point_t & position, const double & radius);

    virtual void move(const point_t & newP) noexcept;
    virtual void move(const double &nX, const double &nY) noexcept;
    virtual void scale(const double coefficient);
    virtual void rotate(const double /*newAngle*/) noexcept;

    virtual double getArea() const noexcept;
    virtual rectangle_t getFrameRect() const noexcept;
    virtual point_t getPosition() const noexcept;

  private:
    double radius_;
    point_t currPos_;
  };
}

#endif
