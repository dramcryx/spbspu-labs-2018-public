#include "composite-shape.hpp"

#define _USE_MATH_DEFINES
#include <math.h>

pashchenko::CompositeShape::CompositeShape() : 
  shapes_(), 
  currArea_(0)
{
}

pashchenko::CompositeShape::CompositeShape(const std::shared_ptr<Shape>& newShape) : 
  shapes_()
{
  shapes_.addElement(newShape);
  currArea_ += newShape->getArea();
}

pashchenko::CompositeShape::CompositeShape(CompositeShape && newCompShape) : 
  shapes_(newCompShape.shapes_)
{
}

pashchenko::CompositeShape::CompositeShape(const CompositeShape & newCompShape) :
  shapes_(newCompShape.shapes_)
{
}

pashchenko::CompositeShape & pashchenko::CompositeShape::operator=(const CompositeShape & newShape) noexcept
{
  if (this != & newShape)
    shapes_ = newShape.shapes_;

  return *this;
}

std::shared_ptr<pashchenko::Shape> pashchenko::CompositeShape::operator[](const unsigned int index) const
{
  if (index >= shapes_.getSize())
    throw std::out_of_range("Out of range!");

  return shapes_[index];
}

void pashchenko::CompositeShape::move(const point_t & newPos) noexcept
{
  const point_t currPos = getFrameRect().pos;
  move(newPos.x - currPos.x, newPos.y - currPos.y);
}

void pashchenko::CompositeShape::move(const double & nX, const double & nY) noexcept
{
  for (size_t i = 0; i < shapes_.getSize(); i++)
    shapes_[i]->move(nX, nY);
}

double pashchenko::CompositeShape::getArea() const noexcept
{
  return currArea_;
}

pashchenko::rectangle_t pashchenko::CompositeShape::getFrameRect() const noexcept
{
  if (shapes_.getSize() <= 0)
    return { { 0.0, 0.0 }, 0.0, 0.0 };
  else 
  {
    rectangle_t shapeFR = shapes_[0]->getFrameRect();
    double top = shapeFR.pos.y + shapeFR.height / 2;
    double bottom = shapeFR.pos.y - shapeFR.height / 2;
    double left = shapeFR.pos.x - shapeFR.width / 2;
    double right = shapeFR.pos.x + shapeFR.width / 2;

    for (size_t i = 1; i < shapes_.getSize(); i++) 
    {
      shapeFR = shapes_[i]->getFrameRect();
      double currTop = shapeFR.pos.y + shapeFR.height / 2;
      double currBottom = shapeFR.pos.y - shapeFR.height / 2;
      double currLeft = shapeFR.pos.x - shapeFR.width / 2;
      double currRight = shapeFR.pos.x + shapeFR.width / 2;

      if (currTop > top)
        top = currTop;
      if (currBottom < bottom)
        bottom = currBottom;
      if (currLeft < left)
        left = currLeft;
      if (currRight > right)
        right = currRight;
    }

    return { { (left + (right - left) / 2), (bottom + (top - bottom) / 2) }, (right - left), (top - bottom) };
  }
}

void pashchenko::CompositeShape::scale(const double coefficient)
{
  if (coefficient <= 0)
    throw std::invalid_argument("Coefficient is less than or equal to zero");

  const point_t currPos = getFrameRect().pos;
  for (size_t i = 0; i < shapes_.getSize(); i++)
  {
    const point_t shapePos = shapes_[i]->getPosition();
    shapes_[i]->move({ currPos.x + (shapePos.x - currPos.x) * coefficient,
      currPos.y + (shapePos.y - currPos.y) * coefficient });
    
    shapes_[i]->scale(coefficient);
  }
}

void pashchenko::CompositeShape::rotate(const double newAngle) noexcept
{
  const point_t currPos = getFrameRect().pos;

  for (size_t i = 0; i < shapes_.getSize(); ++i)
  {
    const point_t shapePos = shapes_[i]->getPosition();
    const double cosAngle = cos(newAngle * M_PI / 180.0);
    const double sinAngle = sin(newAngle * M_PI / 180.0);
    shapes_[i]->move({ currPos.x + (shapePos.x - currPos.x) * cosAngle - (shapePos.y - currPos.y) * sinAngle,
      currPos.y + (shapePos.y - currPos.y) * cosAngle + (shapePos.x - currPos.x) * sinAngle });

    shapes_[i]->rotate(newAngle);

  }
}

pashchenko::point_t pashchenko::CompositeShape::getPosition() const noexcept
{
  return getFrameRect().pos;
}

size_t pashchenko::CompositeShape::getSize() const noexcept
{
  return shapes_.getSize();
}

void pashchenko::CompositeShape::addElement(const std::shared_ptr<pashchenko::Shape>& newShape)
{
  if (this == newShape.get())
    throw std::invalid_argument("This element is equal to current composite shape!");

  currArea_ += newShape->getArea();
  shapes_.addElement(newShape);
}

void pashchenko::CompositeShape::removeElement(const unsigned int index)
{
  if (shapes_.getSize() <= 0)
    throw std::length_error("List is empty!");

  if (index >= shapes_.getSize())
    throw std::out_of_range("Out of range!");

  currArea_ -= shapes_[index]->getArea();
  shapes_.removeElement(index);
}

void pashchenko::CompositeShape::clearList() noexcept
{
  shapes_.clearList();
}
