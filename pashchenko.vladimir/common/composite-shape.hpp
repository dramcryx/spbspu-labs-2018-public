#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"
#include "shapes_list.hpp"

namespace pashchenko {
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    // Variants of adding new shapes
    CompositeShape(const std::shared_ptr<Shape> & newShape);
    CompositeShape(CompositeShape && newShape);
    CompositeShape(const CompositeShape & newShape);
    CompositeShape &operator=(const CompositeShape & newShape) noexcept;
    std::shared_ptr<Shape> operator[](const unsigned int index) const;

    virtual void move(const point_t & newP) noexcept;
    virtual void move(const double & nX, const double & nY) noexcept;

    virtual double getArea() const noexcept;
    virtual rectangle_t getFrameRect() const noexcept;
    virtual void scale(const double coefficient);
    virtual void rotate(const double newAngle) noexcept;

    virtual point_t getPosition() const noexcept;

    size_t getSize() const noexcept;
    void addElement(const std::shared_ptr<Shape> & newShape);
    void removeElement(const unsigned int index);
    void clearList() noexcept;

  private:
    //std::unique_ptr<std::shared_ptr<Shape>[]> array_shape;
    using arrayType = ShapesList<std::shared_ptr<Shape>>;
    arrayType shapes_;
    double currArea_;
  };
}

#endif
