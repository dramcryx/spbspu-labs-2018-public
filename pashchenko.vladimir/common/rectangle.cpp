#include "rectangle.hpp"
#include <iostream>

#define _USE_MATH_DEFINES
#include <math.h>

pashchenko::Rectangle::Rectangle(const point_t & position, const double width, const double height) :
  vertices_{ { position.x - width / 2, position.y + height / 2 },
    { position.x + width / 2, position.y + height / 2 },
    { position.x + width / 2, position.y - height / 2 },
    { position.x - width / 2, position.y - height / 2 } }
{
  if (width <= 0.0)
    throw std::invalid_argument("Width is less than zero");

  if (height <= 0.0)
    throw std::invalid_argument("Height is less than zero");
}

void pashchenko::Rectangle::move(const point_t & newP) noexcept
{
  const point_t centre = getPosition();

  for (int i = 0; i < 4; ++i)
  {
    vertices_[i].x += newP.x - centre.x;
    vertices_[i].y += newP.y - centre.y;
  }
}

void pashchenko::Rectangle::move(const double & nX, const double & nY) noexcept
{
  for (int i = 0; i < 4; ++i)
    vertices_[i].x += nX;
  for (int i = 0; i < 4; ++i)
    vertices_[i].y += nY;
}

double pashchenko::Rectangle::getArea() const noexcept
{
  return getWidth() * getHeight();
}

double pashchenko::Rectangle::getWidth() const noexcept
{
  return sqrt(pow(vertices_[0].x - vertices_[1].x, 2) + pow(vertices_[0].y - vertices_[1].y, 2));
}

double pashchenko::Rectangle::getHeight() const noexcept
{
  return sqrt(pow(vertices_[0].x - vertices_[3].x, 2) + pow(vertices_[0].y - vertices_[3].y, 2));
}

pashchenko::point_t pashchenko::Rectangle::getPosition() const noexcept
{
  return getFrameRect().pos;
}

pashchenko::rectangle_t pashchenko::Rectangle::getFrameRect() const noexcept
{
  double top = vertices_[0].y;
  double bottom = vertices_[0].y;
  double left = vertices_[0].x;
  double right = vertices_[0].x;

  for (int i = 1; i < 4; ++i)
  {
    if (vertices_[i].x < left)
      left = vertices_[i].x;
    if (vertices_[i].x > right)
      right = vertices_[i].x;
    if (vertices_[i].y < bottom)
      bottom = vertices_[i].y;
    if (vertices_[i].y > top)
      top = vertices_[i].y;
  }

  return { { left + (right - left) / 2, bottom + (top - bottom) / 2 }, right - left, top - bottom };
}

void pashchenko::Rectangle::scale(const double coefficient)
{
  if (coefficient <= 0)
    throw std::invalid_argument("Coefficient is less than or equal to zero");
  const point_t centre = getPosition();

  for (int i = 0; i < 4; ++i)
  {
    const point_t vector = { (vertices_[i].x - centre.x) * coefficient, (vertices_[i].y - centre.y) * coefficient };
    vertices_[i] = { centre.x + vector.x, centre.y + vector.y };
  }
}

void pashchenko::Rectangle::rotate(const double newAngle) noexcept
{
  const point_t centre = getPosition();
  const double cosAngle = cos(newAngle * M_PI / 180.0);
  const double sinAngle = sin(newAngle * M_PI / 180.0);

  for (int i = 0; i < 4; ++i)
  {
    vertices_[i] = { centre.x + (vertices_[i].x - centre.x) * cosAngle -
      (vertices_[i].y - centre.y) * sinAngle, centre.y + (vertices_[i].y - centre.y) * cosAngle +
      (vertices_[i].x - centre.x) * sinAngle };
  }
}
