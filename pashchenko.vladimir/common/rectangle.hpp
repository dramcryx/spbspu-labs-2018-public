#ifndef RECTANGLE_HPP_FILE
#define RECTANGLE_HPP_FILE

#include "shape.hpp"

namespace pashchenko {
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t & position, const double width, const double height);

    virtual void move(const point_t & newP) noexcept;
    virtual void move(const double & nX, const double & nY) noexcept;
    virtual void scale(const double coefficient);
    virtual void rotate(const double newAngle) noexcept;

    virtual double getArea() const noexcept;
    virtual rectangle_t getFrameRect() const noexcept;

    virtual double getWidth() const noexcept;
    virtual double getHeight() const noexcept;
    point_t getPosition() const noexcept;

  private:
    point_t vertices_[4];
  };
}

#endif
