#ifndef SHAPES_LIST_HPP_FILE
#define SHAPES_LIST_HPP_FILE

#include <stdexcept>
#include <memory>

namespace pashchenko {
  template <class T>
  class ShapesList
  {
  public:
    ShapesList();
    ShapesList(const ShapesList<T> & incomeEl);
    ShapesList(ShapesList<T> && incomeEl);

    ShapesList<T> &operator = (const ShapesList<T> & incomeEl);
    T operator[] (const unsigned int index) const;

    size_t getSize() const noexcept;
    void addElement(const T & incomeEl);
    void removeElement(unsigned int index);
    void clearList();

  private:
    using arrayType = std::unique_ptr<T[]>;
    arrayType listShapes_;
    size_t size_;
  };
}

//-----------------Implementation--------------------//

template <class T>
pashchenko::ShapesList<T>::ShapesList() :
  listShapes_(new T[0]),
  size_(0)
{
}

template <class T>
pashchenko::ShapesList<T>::ShapesList(const ShapesList<T> & incomeEl) :
  listShapes_(new T[incomeEl.getSize()]),
  size_(incomeEl.getSize())
{
}

template<class T>
pashchenko::ShapesList<T>::ShapesList(ShapesList<T>&& incomeEl) :
  size_(incomeEl.size_)
{
  listShapes_.swap(incomeEl.listShapes_);
}

template<class T>
pashchenko::ShapesList<T>& pashchenko::ShapesList<T>::operator=(const ShapesList<T>& incomeEl)
{
  if (this != &incomeEl)
  {
    size_ = incomeEl.getSize();
    listShapes_.reset(new T[incomeEl.getSize()]);
    for (size_t i = 0; i < size_; ++i)
      listShapes_[i] = incomeEl.listShapes_[i];

  }
  return *this;
}

template<class T>
T pashchenko::ShapesList<T>::operator[](const unsigned int index) const
{
  if (size_ <= index)
    throw std::out_of_range("Out of range!");
  return listShapes_[index];
}

template<class T>
size_t pashchenko::ShapesList<T>::getSize() const noexcept
{
  return size_;
}

template<class T>
void pashchenko::ShapesList<T>::addElement(const T & incomeEl)
{
  arrayType arrayNew(new T[size_ + 1]);
  for (size_t i = 0; i < size_; ++i)
    arrayNew[i] = listShapes_[i];

  //arrayNew[size_++] = incomeEl;
  arrayNew[size_] = incomeEl;
  listShapes_.swap(arrayNew);

  size_++;
}

template<class T>
void pashchenko::ShapesList<T>::removeElement(unsigned int index)
{
  if (size_ <= 0)
    throw std::out_of_range("List is empty now!");

  if (index >= size_)
    throw std::out_of_range("Out of range!");

  for (size_t i = index; i < size_ - 1; ++i)
    listShapes_[i] = listShapes_[i + 1];

  size_--;
}

template<class T>
void pashchenko::ShapesList<T>::clearList()
{
  listShapes_.reset();
  size_ = 0;
}

#endif
