#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "base-types.hpp"

using namespace perepelitsa;

void test(Shape& obj)
{
  std::cout << "Start parameters";
  obj.info();
  obj.move(5, 10);
  obj.move({ 33.58, 26.43 });
  obj.scale(2);
  obj.getFrameRect();
  obj.getArea();
  std::cout << "Final parameters";
  obj.info();
}

int main()
{
  Rectangle rect({ 20, 30 }, 5, 6);
  Circle circ({ 14, 18 }, 3);
  Triangle trian({ 20, 42 }, { 34, 33 }, { 15, 10 });

  test(rect);
  test(circ);
  test(trian);

  return 0;
}
