#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double EPSILON = 0.00001;

BOOST_AUTO_TEST_SUITE(RectangleTests)

  BOOST_AUTO_TEST_CASE(AbsoluteMoving)
  {
    perepelitsa::Rectangle rect({ 20, 30 }, 5, 6);
    double widthBefore = rect.getFrameRect().width;
    double heightBefore = rect.getFrameRect().height;
    rect.move({ 33.58, 26.43 });
    BOOST_CHECK_EQUAL(widthBefore, rect.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, rect.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(RelativeMoving)
  {
    perepelitsa::Rectangle rect({ 20, 30 }, 5, 6);
    double widthBefore = rect.getFrameRect().width;
    double heightBefore = rect.getFrameRect().height;
    rect.move(5, 10);
    BOOST_CHECK_EQUAL(widthBefore, rect.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, rect.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingToPointArea)
  {
    perepelitsa::Rectangle rect({ 20, 30 }, 5, 6);
    double areaBefore = rect.getArea();
    rect.move({ 5, 10 });
    BOOST_CHECK_EQUAL(areaBefore, rect.getArea());
  }

  BOOST_AUTO_TEST_CASE(MovingOnPointArea)
  {
    perepelitsa::Rectangle rect({ 20, 30 }, 5, 6);
    double areaBefore = rect.getArea();
    rect.move(33.58, 26.43);
    BOOST_CHECK_EQUAL(areaBefore, rect.getArea());
  }

  BOOST_AUTO_TEST_CASE(Scaling)
  {
    const double ratio = 3.3;
    perepelitsa::Rectangle rect({ 20, 30 }, 5, 6);
    double areaBeforeScaling = rect.getArea();
    rect.scale(ratio);
    BOOST_CHECK_CLOSE(ratio * ratio * areaBeforeScaling, rect.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
  {
    perepelitsa::Rectangle rect({ 20, 30 }, 5, 6);
    BOOST_CHECK_THROW(rect.scale(-5.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidConstructorParameters)
  {
    BOOST_CHECK_THROW(perepelitsa::Rectangle rect({ 20, 30 }, 5, -6), std::invalid_argument);
    BOOST_CHECK_THROW(perepelitsa::Rectangle rect({ 20, 30 }, -5, 6), std::invalid_argument);
    BOOST_CHECK_THROW(perepelitsa::Rectangle rect({ 20, 30 }, -5, -6), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Circle)

  BOOST_AUTO_TEST_CASE(MovingToPoint)
  {
    perepelitsa::Circle circle({ 14, 18 }, 3);
    double widthBefore = circle.getFrameRect().width;
    double heightBefore = circle.getFrameRect().height;
    circle.move({ 11.31, 10 });
    BOOST_CHECK_EQUAL(widthBefore, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, circle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(RelativeMoving)
  {
    perepelitsa::Circle circle({ 14, 18 }, 3);
    double widthBefore = circle.getFrameRect().width;
    double heightBefore = circle.getFrameRect().height;
    circle.move(5.5, 21.1);
    BOOST_CHECK_EQUAL(widthBefore, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, circle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingToPointArea)
  {
    perepelitsa::Circle circle({ 14, 18 }, 3);
    double areaBefore = circle.getArea();
    circle.move({ 11.31, 10 });
    BOOST_CHECK_EQUAL(areaBefore, circle.getArea());
  }

  BOOST_AUTO_TEST_CASE(RelativeMovingArea)
  {
    perepelitsa::Circle circle({ 14, 18 }, 3);
    double areaBefore = circle.getArea();
    circle.move(5.5, 21.1);
    BOOST_CHECK_EQUAL(areaBefore, circle.getArea());
  }

  BOOST_AUTO_TEST_CASE(Scaling)
  {
    const double ratio = 3.7;

    perepelitsa::Circle circle({ 14, 18 }, 3);
    double areaBeforeScaling = circle.getArea();
    circle.scale(ratio);
    BOOST_CHECK_CLOSE(ratio * ratio * areaBeforeScaling, circle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
  {
    perepelitsa::Circle circle({ 14, 18 }, 3);
  }

  BOOST_AUTO_TEST_CASE(InvalidConstructorParameters)
  {
    BOOST_CHECK_THROW(perepelitsa::Circle circle({ 14, 18 }, -3), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TriangleTest)

  BOOST_AUTO_TEST_CASE(RelativeMoving)
  {
    perepelitsa::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move(5.0, 5.0);
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(MovingOnPoint)
  {
    perepelitsa::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move({ 5.0, 5.0 });
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(MovingOnPointArea)
  {
    perepelitsa::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move({ 1.0, 1.0 });
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(MovingToPointArea)
  {
    perepelitsa::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move(1.0, 1.0);
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Scaling)
  {
    perepelitsa::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double  k = 2.0;
    double triangleWidthBeforeScale = triangle.getFrameRect().width;
    double triangleHeightBeforeScale = triangle.getFrameRect().height;
    double triangleAreaBeforeScale = triangle.getArea();
    triangle.scale(k);
    BOOST_CHECK_CLOSE(k * k * triangleAreaBeforeScale, triangle.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, k * triangleWidthBeforeScale, EPSILON);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, k * triangleHeightBeforeScale, EPSILON);
    BOOST_CHECK_THROW(triangle.scale(-2), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
