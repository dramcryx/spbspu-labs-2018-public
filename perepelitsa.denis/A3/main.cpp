#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"


int main()
{
  perepelitsa::point_t p = {143.0, 239.0};
  std::shared_ptr<perepelitsa::Shape> rectPtr (new perepelitsa::Rectangle(p, 19.0, 40));
  std::shared_ptr<perepelitsa::Shape> circlePtr (new perepelitsa::Circle(p, 9.0));
  std::shared_ptr<perepelitsa::Shape> trianglePtr (new perepelitsa::Triangle({3.0, 8.0}, {5.0, 5.0}, {1.0, 0.0}));

  perepelitsa::CompositeShape comp_shape(rectPtr);
  comp_shape.addShape(circlePtr);
  comp_shape.addShape(trianglePtr);

  std::cout<< comp_shape.getArea() << "Start value:" << "\n";
  comp_shape.move({17.0, 19.0});
  std::cout<< comp_shape.getArea() << "Middle value:" << "\n";
  comp_shape.move(32.0, 41.0);
  comp_shape.scale(10.0);
  std::cout<< comp_shape.getArea() << "Final value:" << "\n";
  comp_shape.deleteShape(1);

  return 0;
}
