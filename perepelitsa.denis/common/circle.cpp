#include "circle.hpp"
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdexcept>
#include <iostream>

perepelitsa::Circle::Circle(const point_t &center, double radius) :
  center_(center),
  radius_(radius)
{
  if (radius_ < 0.0)
  {
    radius_ = 0.0;
    throw std::invalid_argument("Error, invalid parameters");
  }
}

double perepelitsa::Circle::getArea() const
{
  return radius_*radius_*M_PI;
}

perepelitsa::rectangle_t perepelitsa::Circle::getFrameRect() const
{
  return rectangle_t{ center_, radius_ * 2, radius_ * 2 };
}

void perepelitsa::Circle::move(double dx, double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void perepelitsa::Circle::move(const point_t &p)
{
  center_ = p;
}

void perepelitsa::Circle::info() const
{
  std::cout << "Radius: " << radius_ << " Center of Circle: x = " << center_.x << " y = " << center_.y << std::endl;
}

void perepelitsa::Circle::rotate(const double /*degrees*/)
{
}

void perepelitsa::Circle::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Wrong scale coefficient!");
  }
  radius_ *= ratio;
}
