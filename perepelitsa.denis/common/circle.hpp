#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"
#include "base-types.hpp"

namespace perepelitsa
{
  class Circle : 
    public Shape 
  {
  public:
    Circle(const point_t &center, double radius);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &p) override;
    void info() const override;
    void scale(const double ratio) override;
    void rotate(double degrees) override;

  private:
    point_t center_;
    double radius_;
  };
}

#endif
