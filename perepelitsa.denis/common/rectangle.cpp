#include <cmath>
#include <algorithm>
#include "rectangle.hpp"
#include <stdexcept>
#include <iostream>


perepelitsa::Rectangle::Rectangle(const point_t &centre, double width, double height) :
  centre_(centre),
  width_(width),
  height_(height),
  angle_(0.0),
  a_({(centre.x - width / 2.0), (centre.y + height / 2.0)}),
  b_({(centre.x + width / 2.0), (centre.y + height / 2.0)}),
  c_({(centre.x + width / 2.0), (centre.y - height / 2.0)}),
  d_({(centre.x - width / 2.0), (centre.y - height / 2.0)})
{
  if (width < 0.0 || height < 0.0)
  {
    throw std::invalid_argument("Wronge parameters!");
  }
}

double perepelitsa::Rectangle::getArea() const
{
  return width_ * height_;
}

perepelitsa::rectangle_t perepelitsa::Rectangle::getFrameRect() const
{
  perepelitsa::rectangle_t rect;
  rect.pos = centre_;
  rect.height = std::max(std::max(a_.y, b_.y), std::max(c_.y, d_.y)) - std::min(std::min(a_.y, b_.y), std::min(c_.y, d_.y));
  rect.width = std::max(std::max(a_.x, b_.x), std::max(c_.x, d_.x)) - std::min(std::min(a_.x, b_.x), std::min(c_.x, d_.x));
  return (rect);

}

void perepelitsa::Rectangle::move(double dx, double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}
 
void perepelitsa::Rectangle::move(const point_t &newpoint)
{
  centre_ = newpoint;
}

void perepelitsa::Rectangle::info() const
{
  std::cout << "Width: " << width_ << " Height: " << height_;
  std::cout << " Centre: " << "x = " << centre_.x << " y = " << centre_.y << std::endl;
}

void perepelitsa::Rectangle::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Wrong Rectangle scale coefficient!");
  }
  height_ *= ratio;
  width_ *= ratio;
}

perepelitsa::point_t perepelitsa::Rectangle::getPosition() const
{
  return {(corners_[0].x + corners_[2].x) / 2, (corners_[1].y + corners_[3].y) / 2};
}


void perepelitsa::Rectangle::rotate(const double degrees)

{

  double sine = sin(degrees * M_PI / 180);

  double cose = cos(degrees * M_PI / 180);


  point_t centre = getPosition();

  for (size_t i = 0; i < 4; i++)

  {

    corners_[i] = { centre.x + (corners_[i].x - centre.x) * cose - (corners_[i].y - centre.y) * sine,

    centre.y + (corners_[i].y - centre.y) * cose + (corners_[i].x - centre.x) * sine };

  }

}
