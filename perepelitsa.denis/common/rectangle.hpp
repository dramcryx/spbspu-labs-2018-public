#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include "shape.hpp"
#include "base-types.hpp"

namespace perepelitsa
{
  class Rectangle : 
    public Shape
  {
  public:
    Rectangle(const point_t &center, double width, double height);

    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &newpoint) override;
    void info() const override;
    void scale(const double ratio) override;
    void rotate(double derees) override;

  private:
    point_t centre_;
    double width_;
    double height_;
    double angle_;
    double degrees_;
    point_t corners_[4];
    point_t getPosition() const;
    point_t a_, b_, c_, d_;
  };
}

#endif
