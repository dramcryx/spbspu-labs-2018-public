#ifndef LAB1_IMPL_H
#define LAB1_IMPL_H

#include <cstring>
#include <iostream>
#include <functional>

void doPartOne(int argc, char *argv[]);
void doPartTwo(int argc, char *argv[]);
void doPartThree(int argc, char *argv[]);
void doPartFour(int argc, char *argv[]);

namespace detail
{
  template<typename T>
  struct AccessByOperator
  {
    typename T::reference operator()(T& container, size_t i)
    {
      return container[i];
    }
    size_t getBegin(const T&) const
    {
      return 0;
    }
    size_t getEnd(const T& container) const
    {
      return container.size();
    }
  };

  template<typename T>
  struct AccessByAt
  {
    typename T::reference operator()(T& container, size_t i)
    {
      return container.at(i);
    }
    size_t getBegin(const T&) const
    {
      return 0;
    }
    size_t getEnd(const T& container) const
    {
      return container.size();
    }
  };

  template<typename T>
  struct AccessByIterator
  {
    typename T::reference operator()(T&, typename T::iterator iter)
    {
      return *iter;
    }
    typename T::iterator getBegin(T& container)
    {
      return container.begin();
    }
    typename T::iterator getEnd(T& container)
    {
      return container.end();
    }
  };

  template<typename Container, typename Access, typename Compare>
  void sort(Container& container, Access access, Compare compare)
  {
    for(auto i = access.getBegin(container); i != access.getEnd(container); i++)
    {
      for(auto j = i; j != access.getEnd(container); j++)
      {
        if(compare(access(container, i), access(container, j)))
        {
          std::swap(access(container, i), access(container, j));
        }
      }
    }
  }

  template<typename T>
  std::function<bool(T,T)> getDirection(const char* arg)
  {
    if(strcmp(arg, "ascending") == 0)
    {
      return std::function<bool(T,T)>(std::greater<T>());
    }
    if(strcmp(arg, "descending") == 0)
    {
      return std::function<bool(T,T)>(std::less<T>());
    }
    throw std::invalid_argument("Incorrect parameters for direction");
  }

  template<typename T>
  void print(T& container)
  {
    for (typename T::iterator i = container.begin(); i != container.end(); i++)
    {
      std::cout << *i << " ";
    }
    std::cout << std::endl;
  }
}

#endif
