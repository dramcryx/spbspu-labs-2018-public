#include <cstdlib>
#include <iostream>
#include <stdexcept>

#include "impl.hpp"

int main(int argc, char *argv[])
{
  try
  {
    if(argc > 1)
    {
      switch(std::atoi(argv[1]))
      {
      case 1:
        doPartOne(argc, argv);
        break;
      case 2:
        doPartTwo(argc, argv);
        break;
      case 3:
        doPartThree(argc, argv);
        break;
      case 4:
        doPartFour(argc, argv);
        break;
      default:
        std::cerr << "Incorrect input parameters";
        return 1;
      }
    }
    else
    {
      std::cerr << "Incorrect input parameters";
      return 1;
    }
  }
  catch(const std::exception &err)
  {
    std::cerr << err.what() << std::endl;
    return 2;
  }
  return 0;
}
