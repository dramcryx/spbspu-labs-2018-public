#include "impl.hpp"

#include <forward_list>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <vector>

void doPartOne(int argc, char *argv[])
{
  if(argc != 3)
  {
    throw std::invalid_argument("Incorrect parameters for first task");
  }

  auto direction = detail::getDirection<int>(argv[2]);
  std::vector<int> vector_a(std::istream_iterator<int>(std::cin),
      std::istream_iterator<int>());

  if(!std::cin.eof())
  {
    throw std::ios_base::failure("Incorrect input in first task");
  }

  if(vector_a.empty())
  {
    return;
  }

  std::vector<int> vector_b(vector_a);
  std::forward_list<int> list_c(vector_a.begin(), vector_a.end());

  detail::sort(vector_a, detail::AccessByOperator<std::vector<int>>(), direction);
  detail::sort(vector_b, detail::AccessByAt<std::vector<int>>(), direction);
  detail::sort(list_c, detail::AccessByIterator<std::forward_list<int>>(), direction);

  detail::print(vector_a);
  detail::print(vector_b);
  detail::print(list_c);
}
