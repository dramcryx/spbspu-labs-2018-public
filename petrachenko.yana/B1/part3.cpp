#include "impl.hpp"

#include <iostream>
#include <iterator>
#include <vector>

void doPartThree(int argc, char*[])
{
  if (argc != 2)
  {
    throw std::invalid_argument("Incorrect parameters for third task");
  }

  std::vector<int> vector3(std::istream_iterator<int>(std::cin),
    std::istream_iterator<int>());

  if(!std::cin.eof())
  {
    throw std::ios_base::failure("Incorrect input in third task");
  }

  if(vector3.empty())
  {
    return;
  }

  if(vector3.back() != 0)
  {
    throw std::invalid_argument("Missed zero");
  }

  vector3.pop_back();

  if(vector3.empty())
  {
    return;
  }

  std::vector<int>::iterator iter = vector3.begin();

  if (vector3.back() == 1)
  {
    while(iter != vector3.end())
    {
      if(*iter % 2 == 0)
      {
        iter = vector3.erase(iter);
      }
      else
      {
        iter++;
      }
    }
  }
  else if(vector3.back() == 2)
  {
    while(iter != vector3.end())
    {
      if(*iter % 3 == 0)
      {
        iter = vector3.insert(++iter, 3, 1);
        std::advance(iter, 2);
      }
      iter++;
    }
  }
  detail::print(vector3);
}
