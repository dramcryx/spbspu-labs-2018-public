#include "impl.hpp"

#include <iostream>
#include <random>
#include <vector>

void fillRandom(double * array, int size)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);

  for(int i = 0; i < size; i++)
  {
    array[i] = distribution(gen);
  }
}

void doPartFour(int argc, char *argv[])
{
  if(argc != 4)
  {
    throw std::invalid_argument("Incorrect parameters for fourth task");
  }

  auto direction = detail::getDirection<double>(argv[2]);

  std::vector<double> vector4(std::stoi(argv[3]));
  fillRandom(&vector4[0], vector4.size());
  detail::print(vector4);
  detail::sort(vector4, detail::AccessByIterator<std::vector<double>>(), direction);
  detail::print(vector4);
}
