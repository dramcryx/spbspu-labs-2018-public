#include <iostream>
#include <stdexcept>

#include "tasksheader.hpp"

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    std::cerr << "сhoose task number (1/2/3/4)\n";
    return 1;
  }
  try
  {
    if(argc > 1)
    { 
      int variant = std::atoi(argv[1]);
      switch(variant)
      {
      case 1:
        TaskOne(argc, argv);
        break;
      case 2:
        TaskTwo(argc, argv);
        break;
      case 3:
        TaskThree(argc, argv);
        break;
      case 4:
        TaskFour(argc, argv);
        break;
      default:
        std::cerr << "Parameters are wrong.";
        return 1;
      }
    }
    else
    {
      std::cerr << "Parameters are wrong.";
      return 1;
    }
  }
  catch(const std::exception &error)
  {
    std::cerr << error.what() << std::endl;
    return 2;
  }
  return 0;
}
