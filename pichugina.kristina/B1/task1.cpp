#include <forward_list>
#include <iterator>
#include <stdexcept>
#include <vector>

#include "tasksheader.hpp"

void TaskOne(int argc, char *argv[])
{
  if(argc != 3)
  {
    throw std::invalid_argument("Incorrect parameters for first task");
  }

  auto direction = pichugina::getDirection<int>(argv[2]);
  std::vector<int> vector_a(std::istream_iterator<int>(std::cin),
      std::istream_iterator<int>());

  if(!std::cin.eof())
  {
    throw std::ios_base::failure("Incorrect input in first task");
  }

  if(vector_a.empty())
  {
    return;
  }

  std::vector<int> vector_b(vector_a);
  std::forward_list<int> list_c(vector_a.begin(), vector_a.end());

  pichugina::sort(vector_a, pichugina::AccessByOperator<std::vector<int>>(), direction);
  pichugina::sort(vector_b, pichugina::AccessByAt<std::vector<int>>(), direction);
  pichugina::sort(list_c, pichugina::AccessByIterator<std::forward_list<int>>(), direction);

  pichugina::print(vector_a);
  pichugina::print(vector_b);
  pichugina::print(list_c);
}
