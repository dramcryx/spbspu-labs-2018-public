#include <iostream>
#include <iterator>
#include <vector>

#include "tasksheader.hpp"

void TaskThree(int argc, char*[])
{
  if (argc != 2)
  {
    throw std::invalid_argument("Incorrect parameters for third task");
  }
  std::vector<int> vector_a;
  int cap = 0;
  int value = 0;
  while (std::cin) 
  {
    if (std::cin >> value)
    {
      if (value % 3 == 0)
      {
        cap++;
      }
      if (value == 0)
      {
        break;
      }
      vector_a.push_back(value);
      
    }
    else
    {
      if (std::cin.eof())
      {
        break;
      }
      else
      {
        throw std::invalid_argument ("Input error");
      }
    }
  }

  if ((!std::cin.eof()) && (value != 0)) 
  {
    throw std::invalid_argument ("Input error");
  }
  
  if (value != 0)
  {
    throw std::invalid_argument ("Missing zero");
  }
  
  if (vector_a.empty())
  {
    return;
  }
 
  switch (vector_a.back()) 
  {
  case 1:
  {
    std::vector<int>::iterator iterStart = vector_a.begin();
    while (iterStart != vector_a.end())
    {
      if (*iterStart % 2 == 0)
      {
        iterStart = vector_a.erase(iterStart);
      }
      else 
      {
        ++iterStart;
      }
    }
    break;
  }
  case 2:
  {
    vector_a.reserve(vector_a.capacity() + 3 * cap);
    std::vector<int>::iterator iterStart = vector_a.begin();
    while (iterStart != vector_a.end())
    {
      if (*iterStart % 3 == 0)
      {
        iterStart = vector_a.insert(iterStart + 1, 3, 1) + 2;
      }
      ++iterStart;
    }
    break;
  }
  default:
  {
  }
  }
  
  for(std::vector<int>::iterator i = vector_a.begin(); i < vector_a.end(); ++i)
  {
    std::cout << *i << " ";
  }
}
