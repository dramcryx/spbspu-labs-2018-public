#include <iostream>
#include <random>
#include <vector>

#include "tasksheader.hpp"

void fillRandom(double * array, int size)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);

  for(int i = 0; i < size; i++)
  {
    array[i] = distribution(gen);
  }
}

void TaskFour(int argc, char *argv[])
{
  if(argc != 4)
  {
    throw std::invalid_argument("Incorrect parameters for fourth task");
  }

  auto direction = pichugina::getDirection<double>(argv[2]);

  std::vector<double> vector4(std::stoi(argv[3]));
  fillRandom(&vector4[0], vector4.size());
  pichugina::print(vector4);
  pichugina::sort(vector4, pichugina::AccessByIterator<std::vector<double>>(), direction);
  pichugina::print(vector4);
}
