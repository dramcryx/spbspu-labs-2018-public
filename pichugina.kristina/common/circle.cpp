#include "circle.hpp"

#include <stdexcept>
#include <math.h>

pichugina::Circle::Circle(const pichugina::point_t & centre, const double radius):
  centre_(centre),
  radius_(radius),
  angle_(0)
{
  if (radius < 0.0)
  {
    throw std::invalid_argument("Invalid circle");
  }
}

double pichugina::Circle::getArea() const noexcept
{
  return M_PI * radius_ * radius_;
}

double pichugina::Circle::getAngle() const noexcept
{
  return angle_;
}

pichugina::rectangle_t pichugina::Circle::getFrameRect() const noexcept
{
  return {centre_, 2 * radius_, 2 * radius_};
}

void pichugina::Circle::move(const pichugina::point_t & new_centre) noexcept
{
  centre_ = new_centre;
}

void pichugina::Circle::move(const double dx, const double dy) noexcept
{
  centre_.x += dx;
  centre_.y += dy;
}

void pichugina::Circle::rotate(const double a) noexcept
{
  angle_ += a;
  if (angle_ >= 360)
  {
    angle_ = fmod(angle_, 360);
  }
}

void pichugina::Circle::scale(const double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Invalid factor");
  }
  radius_ *= factor;
}
