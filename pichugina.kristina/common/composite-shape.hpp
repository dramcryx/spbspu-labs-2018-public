#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>

#include "shape.hpp"

namespace pichugina
{
  class CompositeShape:
    public pichugina::Shape
  {
  public:
    CompositeShape(const std::shared_ptr <pichugina::Shape> new_shape);
    CompositeShape(const pichugina::CompositeShape &copy);
    CompositeShape(pichugina::CompositeShape &&move) = default;
    CompositeShape & operator=(const pichugina::CompositeShape &copy);
    CompositeShape & operator=(pichugina::CompositeShape &&move);
    double getArea() const noexcept override;
    double getAngle() const noexcept;
    pichugina::rectangle_t getFrameRect() const noexcept override;
    void move(const pichugina::point_t & new_centre) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double factor) override;
    void rotate(const double a) noexcept override;
    void addShape(std::shared_ptr <pichugina::Shape> new_shape);
    std::shared_ptr < pichugina::Shape > getShape(int const shapeNumber) const;
    void removeShape(const int shapeNumber);
    void deleteShapes() noexcept;
    int getSize() const noexcept;
  private:
    std::unique_ptr <std::shared_ptr <pichugina::Shape>[] > parts_;
    int size_;
    double angle_;
  };
}

#endif
