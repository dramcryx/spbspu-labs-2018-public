#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

namespace pichugina
{
  class Rectangle:
    public pichugina::Shape
  {
  public:
    Rectangle(const pichugina::point_t & center, const double widht, const double height);
    double getArea() const noexcept override;
    double getAngle() const noexcept;
    void scale(const double factor) override;
    void rotate(const double a) noexcept override;
    pichugina::rectangle_t getFrameRect() const noexcept override;
    void move(const pichugina::point_t & new_centre) noexcept override;
    void move(const double dx, const double dy) noexcept override;
  private:
    pichugina::point_t centre_;
    double width_;
    double height_;
    double angle_;
  };
}

#endif
