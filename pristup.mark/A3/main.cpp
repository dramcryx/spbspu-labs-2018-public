#include <iostream>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
    
    std::shared_ptr<pristup::Shape> rectPtr
        = std::make_shared<pristup::Rectangle>(pristup::Rectangle({5.0, 10.0, {50.0, 100.0}}));
    pristup::CompositeShape shape(rectPtr);

    std::shared_ptr<pristup::Shape> circlePtr
        = std::make_shared<pristup::Circle>(pristup::Circle(6.0, {12.0, 12.0}));
    shape.addShape(circlePtr);
    
    std::shared_ptr<pristup::Shape> trianglePtr
        = std::make_shared<pristup::Triangle>(pristup::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);
    
    std::cout<< shape.getArea() << "\n";
    
    shape.move({100.0, 50.0});
    std::cout<< shape.getArea() << "\n";
    
    shape.move(30.0, 40.0);
    shape.scale(2.0);
    std::cout<< shape.getArea() << "\n";
    
    shape.delShape(0);
    
    return 0;
}

