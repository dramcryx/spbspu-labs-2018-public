#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>
#include "shape.hpp"


namespace pristup
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape(const std::shared_ptr<pristup::Shape> shape_ptr);
    CompositeShape(const pristup::CompositeShape & obj);
    CompositeShape(pristup::CompositeShape && obj);
    
    CompositeShape &operator =(const pristup::CompositeShape & obj);
    CompositeShape &operator =(pristup::CompositeShape && obj);
    std::shared_ptr<Shape> operator [](const size_t index);
    
    void addShape(const std::shared_ptr <pristup::Shape> shape_ptr);
    void delShape(const size_t index);
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t & point) noexcept override;
    void move(double dx, double dy) noexcept override;
    void scale(double k) override;
    void rotate( double a) noexcept override;
    
  private:
    std::unique_ptr <std::shared_ptr <pristup::Shape>[]> shapes_;
    size_t shapeAmount_;
    double angle_;
  };
}

#endif


