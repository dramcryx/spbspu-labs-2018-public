#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace pristup
{
    class MatrixShape
    {
    public:
        MatrixShape(const std::shared_ptr<Shape> & shapeElement);
        MatrixShape(const MatrixShape & obj);
        MatrixShape(MatrixShape && obj);
        MatrixShape & operator =(const MatrixShape & obj);
        MatrixShape & operator =(MatrixShape && obj);
        std::unique_ptr<std::shared_ptr<Shape>[]>::pointer operator [](const size_t index);
        void addShape(const std::shared_ptr<Shape> & shapeElement);
        
    private:
        using m_type = std::unique_ptr<std::shared_ptr<Shape>[]>;
        m_type matrix_;
        size_t row_, column_;
        bool checkOverlap(const std::shared_ptr<Shape> & shape1, const std::shared_ptr<Shape> & shape2) const;
    };
}

#endif

