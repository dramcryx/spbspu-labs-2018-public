#include <iostream>

#include "../common/circle.hpp"
#include "../common/base-types.hpp"
#include "../common/composite-shape.hpp"
#include "../common/rectangle.hpp"
#include "../common/matrix.hpp"

using std::cout;
using std::endl;

using namespace razuvaev;

int main() {
  try {
    std::shared_ptr<Rectangle> rec  = std::make_shared <Rectangle>(1,1,0,0);
    std::shared_ptr<Rectangle> rec2 = std::make_shared <Rectangle>(2,2,0,0);
    std::shared_ptr<Rectangle> rec3 = std::make_shared <Rectangle>(4,4,0,0);
    std::shared_ptr<Rectangle> rec4 = std::make_shared <Rectangle>(6,6,0,0);
    Rectangle rec5(15,10,1,7);
    std::shared_ptr<CompositeShape> CompShp = std::make_shared<CompositeShape>(&rec5);  
    
    Circle cir1(3,5,9);
    
    CompShp->addShape(&cir1);
    CompShp->rotate(90);
    CompShp->writeInfoOfObject();
    
    std::shared_ptr<Circle> cir2 = std::make_shared<Circle>(1,500,500);
    std::shared_ptr<Circle> cir3 = std::make_shared<Circle>(10,-500,1);
    
    Matrix matrix;
    matrix.addShape(rec);
    matrix.addShape(cir3);
    matrix.addShape(rec2);
    matrix.addShape(rec3);
    matrix.addShape(rec4);  
    matrix.addShape(cir2);
    matrix.addShape(CompShp);
    matrix.writeInfo();
  } catch (const std::exception& e) {
    cout << e.what();
    return 1;
  }
  return 0; 
}
