#include <iostream>
#include <cmath>

#include "../common/circle.hpp"

using std::cout;
using std::endl;
using namespace razuvaev;

Circle::Circle(double radius, double x_center, double y_center) :
  radius_(radius),
  center_({x_center,y_center}),
  angle_(0)
{
  if (radius < 0.0) {  
    throw std::invalid_argument("Radius < 0!");
  }
}

double Circle::getArea() const {
  return radius_*radius_*M_PI;
}

rectangle_t Circle::getFrameRect() const {
  return {2*radius_, 2*radius_, center_};
}

void Circle::move(point_t c) {
  center_ = c;
}

void Circle::move(double dx, double dy) {
  center_.x += dx;
  center_.y += dy;
}

void Circle::writeInfoOfObject() const {
  cout << "CIRCLE"<< endl
    << " Angle: " << angle_ << endl
    << " Area of circle is " << getArea() << endl
    << " FRAME RECTANGLE" << endl 
    << "  Height: " << getFrameRect().height << endl
    << "  Width: " << getFrameRect().width << endl 
    << "  cords (x, y): (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")" << endl
    << " Cords of center of rectangle (x, y): (" << center_.x << ", " << center_.y << ")" << endl << endl;
}

void Circle::scale(double ratio) {
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Ratio < 0!");
  }
  radius_ *= ratio;
}

double Circle::getHeight() {
    return radius_*2;
}

double Circle::getWidth() {
    return radius_*2;
}

double Circle::getRadius() {
  return radius_;
}

void Circle::rotate(double deg) {
  angle_ -= deg;
  if (angle_ < 0.0) {
    angle_ += 360.0;
   
  }
  while (angle_ > 360.0) {
    angle_ -= 360.0;
  }
}

double Circle::getAngle() const {
  return angle_;
}

void Circle::setAngle(double deg) {
  angle_ = deg;
}
