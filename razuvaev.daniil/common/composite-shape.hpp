#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include <memory>

#include "../common/base-types.hpp"
#include "../common/shape.hpp"

namespace razuvaev {
  class CompositeShape : public Shape {
  public:
    CompositeShape();
    CompositeShape(Shape *obj); 
    ~CompositeShape() = default;

    //actions
    void move(point_t c) override;
    void move (double dx, double dy) override;
    void scale(double ratio) override;
    void rotate(double deg) override;
    void addShape(Shape *obj);
    void deleteShape(int index);

    //setters and getters
    void        setAngle(double deg) override;
    rectangle_t getFrameRect() const override;
    double      getArea() const override;
    double      getAngle() const override;
    
    //write info
    void writeInfoOfObject() const override;

  private:
    std::unique_ptr<Shape *[]> arr_;
    int size_;
    double angle_;
  };
}

#endif
