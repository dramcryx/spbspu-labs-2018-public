#include <cmath>
#include <iostream>

#include "../common/rectangle.hpp"

using std::cout;
using std::endl;
using namespace razuvaev;

Rectangle::Rectangle(double height, double width, double x_center, double y_center) :
  height_ (height),
  width_  (width),
  center_ ({x_center,y_center}),
  angle_  (0),
  topLeft_({x_center-width/2,y_center+height/2})
{
  if (height < 0.0 or width < 0.0) {
    throw std::invalid_argument("Width and height < 0!");
  }
}

void Rectangle::writeInfoOfObject() const {
  cout << "RECTANGLE"<< endl
    << " Angle: " << angle_ << endl
    << " Area of rectangle is " << getArea() << endl
    << " FRAME RECTANGLE" << endl 
    << "  Height: " << getFrameRect().height << endl
    << "  Width: " << getFrameRect().width << endl 
    << "  x, y cords: " << getFrameRect().pos.x << " " << getFrameRect().pos.y << endl
    << " Cords of center of rectangle (x, y): (" << center_.x << ", " << center_.y << ")" << endl
    << " Left top coord (x, y): (" << topLeft_.x << ", " << topLeft_.y << ")" << endl << endl;
}

double Rectangle::getArea() const {
  return height_*width_;
} 

void Rectangle::move(point_t center) {
  move(center_.x-center.x, center_.y-center.y);  
}

void Rectangle::move(double dx, double dy) {
  center_.x += dx;
  center_.y += dy;
  topLeft_.x+= dx;
  topLeft_.y+= dy;
}

rectangle_t Rectangle::getFrameRect() const {
  return {width_,height_,center_};
}

void Rectangle::scale(double ratio) {
  if (ratio < 0.0) {
    throw std::invalid_argument("ratio < 0!");
  }
  height_ *= ratio;
  width_  *= ratio;
}

double Rectangle::getHeight() {
  return height_;
}

double Rectangle::getWidth() {
  return width_;
}

void Rectangle::rotate(double deg) {
  angle_ -= deg;
  while (angle_ < 0.0) {
    angle_ += 360.0;
  }
  while (angle_ > 360.0) {
    angle_ -= 360.0;
  }
    double x = center_.x;
    double y = center_.y;
    double x0 = topLeft_.x;
    double y0 = topLeft_.y;
    double degRad = deg * (M_PI/180);
    double rx = x0 - x;
    double ry = y0 - y;
    double c = cos(degRad);
    double s = sin(degRad);
    topLeft_.x = x + rx * c - ry * s;
    topLeft_.y = y + rx * s + ry * c;
}

double Rectangle::getAngle() const {
  return angle_;
}

void Rectangle::setAngle(double deg) {
  angle_ = deg;
}
