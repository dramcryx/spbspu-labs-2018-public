#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "../common/shape.hpp"
#include "../common/base-types.hpp"

namespace razuvaev {
  class Rectangle : public Shape {
  public:       
    Rectangle(double height, double width, double x_center, double y_center);
    
    //actions
    void move(point_t center) override;
    void move(double dx, double dy) override; 
    void scale(double ratio) override;
    void rotate(double deg) override;
    
    //setters and getters
    void        setAngle(double deg) override;
    double      getHeight();
    double      getWidth();
    rectangle_t getFrameRect() const override;
    double      getArea() const override;
    double      getAngle() const override;
    
    //write info
    void writeInfoOfObject() const override;
  
  private:
    double height_;
    double width_;
    point_t center_;
    double angle_;
    point_t topLeft_;
  };
}
#endif
