#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "../common/base-types.hpp"

namespace razuvaev {
  class Shape {
  public:  
    virtual ~Shape() = default;

    //actions
    virtual void move(point_t center) = 0;       // Move to point
    virtual void move(double dx, double dy) = 0; // Shifting by x,y
    virtual void scale(double ratio) = 0;
    virtual void rotate(double deg) = 0;
    
    //setters and getters
    virtual void        setAngle(double deg) = 0;
    virtual rectangle_t getFrameRect() const = 0; 
    virtual double      getArea() const = 0; 
    virtual double      getAngle() const = 0;
    
    //write info
    virtual void writeInfoOfObject() const = 0;  
  };
}
#endif
