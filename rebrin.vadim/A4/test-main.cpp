#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <memory>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

#include <boost/test/included/unit_test.hpp>

const double EPS = 1e-4;

BOOST_AUTO_TEST_SUITE(CompositeShapeOperatorsTest)

  BOOST_AUTO_TEST_CASE(ConstructorTest)
  {
    std::shared_ptr <rebrin::Shape> emptyShape = nullptr;
    BOOST_CHECK_THROW(rebrin::CompositeShape comp(emptyShape), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    std::shared_ptr <rebrin::Shape> rect =
    std::make_shared <rebrin::Rectangle> (rebrin::Rectangle({1.0, 2.0, {-3.0, -4.0}}));
    std::shared_ptr <rebrin::Shape> circ =
    std::make_shared<rebrin::Circle>(rebrin::Circle({13.0, 5.0}, 2.0));
    rebrin::CompositeShape otherComp(rect);
    otherComp.addShape(circ);

    rebrin::CompositeShape comp1{otherComp};

    BOOST_CHECK_CLOSE(comp1.getArea(), otherComp.getArea(), EPS);
    BOOST_CHECK_EQUAL(comp1.getFrameRect().pos.x, otherComp.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(comp1.getFrameRect().pos.y, otherComp.getFrameRect().pos.y);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    std::shared_ptr <rebrin::Shape> rect =
    std::make_shared <rebrin::Rectangle> (rebrin::Rectangle({1.0, 2.0, {-3.0, -4.0}}));
    std::shared_ptr <rebrin::Shape> circ =
    std::make_shared<rebrin::Circle>(rebrin::Circle({13.0, 5.0}, 2.0));
    rebrin::CompositeShape otherComp(rect);
    otherComp.addShape(circ);

    double otherArea = otherComp.getArea();
    rebrin::rectangle_t otherFrame = otherComp.getFrameRect();

    rebrin::CompositeShape comp2{std::move(otherComp)};

    BOOST_CHECK_EQUAL(otherComp.getFrameRect().width, 0.0);
    BOOST_CHECK_EQUAL(otherComp.getFrameRect().height, 0.0);
    BOOST_CHECK_EQUAL(otherComp.getFrameRect().pos.x, 0.0);
    BOOST_CHECK_EQUAL(otherComp.getFrameRect().pos.y, 0.0);

    BOOST_CHECK_CLOSE(comp2.getArea(), otherArea, EPS);
    BOOST_CHECK_EQUAL(comp2.getFrameRect().pos.x, otherFrame.pos.x);
    BOOST_CHECK_EQUAL(comp2.getFrameRect().pos.y, otherFrame.pos.y);
  }

  BOOST_AUTO_TEST_CASE(CopyAssigmentOperatorTest)
  {
    std::shared_ptr <rebrin::Shape> rect =
    std::make_shared <rebrin::Rectangle> (rebrin::Rectangle({1.0, 2.0, {-3.0, -4.0}}));
    std::shared_ptr <rebrin::Shape> circ =
    std::make_shared<rebrin::Circle>(rebrin::Circle({13.0, 5.0}, 2.0));
    rebrin::CompositeShape otherComp(rect);
    otherComp.addShape(circ);
    rebrin::CompositeShape comp1(rect);

    comp1 = otherComp;

    BOOST_CHECK_CLOSE(comp1.getArea(), otherComp.getArea(), EPS);
    BOOST_CHECK_EQUAL(comp1.getFrameRect().pos.x, otherComp.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(comp1.getFrameRect().pos.y, otherComp.getFrameRect().pos.y);
  }

  BOOST_AUTO_TEST_CASE(MoveAssigmentOperatorTest)
  {
    std::shared_ptr <rebrin::Shape> rect =
    std::make_shared <rebrin::Rectangle> (rebrin::Rectangle({1.0, 2.0, {-3.0, -4.0}}));
    std::shared_ptr <rebrin::Shape> circ =
    std::make_shared<rebrin::Circle>(rebrin::Circle({13.0, 5.0}, 2.0));
    rebrin::CompositeShape otherComp(rect);
    otherComp.addShape(circ);

    double otherArea = otherComp.getArea();
    rebrin::rectangle_t otherFrame = otherComp.getFrameRect();

    rebrin::CompositeShape comp2(rect);
    comp2 = std::move(otherComp);

    BOOST_CHECK_EQUAL(otherComp.getFrameRect().width, 0.0);
    BOOST_CHECK_EQUAL(otherComp.getFrameRect().height, 0.0);
    BOOST_CHECK_EQUAL(otherComp.getFrameRect().pos.x, 0.0);
    BOOST_CHECK_EQUAL(otherComp.getFrameRect().pos.y, 0.0);

    BOOST_CHECK_CLOSE(comp2.getArea(), otherArea, EPS);
    BOOST_CHECK_EQUAL(comp2.getFrameRect().pos.x, otherFrame.pos.x);
    BOOST_CHECK_EQUAL(comp2.getFrameRect().pos.y, otherFrame.pos.y);
  }

  BOOST_AUTO_TEST_CASE (IndexOperatorTest)
  {
    std::shared_ptr <rebrin::Shape> rect =
    std::make_shared<rebrin::Rectangle>(rebrin::Rectangle({1.0, 2.0, {-3.0, -4.0}}));
    rebrin::CompositeShape comp(rect);
    BOOST_CHECK_THROW(comp[7], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE (Rotating)

  BOOST_AUTO_TEST_CASE(RectangleTest)
  {
    rebrin::Rectangle rect ({5.0, 4.0, {0.0, 0.0}});
    rebrin::Rectangle originalRect = rect;
    rect.rotate(30);

    BOOST_CHECK_EQUAL(rect.getArea(), originalRect.getArea());
    BOOST_CHECK_EQUAL(rect.getFrameRect().pos.x, originalRect.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(rect.getFrameRect().pos.y, originalRect.getFrameRect().pos.y);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeTest)
  {
    std::shared_ptr <rebrin::Shape> rect =
    std::make_shared<rebrin::Rectangle>(rebrin::Rectangle({1.0, 2.0, {-3.0, -4.0}}));
    std::shared_ptr <rebrin::Shape> circ =
    std::make_shared<rebrin::Circle>(rebrin::Circle({13.0, 5.0}, 2.0));
    rebrin::CompositeShape comp(rect);
    comp.addShape(circ);
    rebrin:: CompositeShape originalComp{comp};
    comp.rotate(90);

    BOOST_CHECK_EQUAL(comp.getArea(), originalComp.getArea());
    BOOST_CHECK_CLOSE(comp.getFrameRect().pos.x, originalComp.getFrameRect().pos.x, EPS);
    BOOST_CHECK_CLOSE(comp.getFrameRect().pos.y, originalComp.getFrameRect().pos.y, EPS);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Matrix)

  BOOST_AUTO_TEST_CASE(ConstructorTest)
  {
    rebrin::Matrix matr;
    BOOST_CHECK_EQUAL(matr.getLayers(), 0);
    BOOST_CHECK_EQUAL(matr.getLayerSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    std::shared_ptr <rebrin::Shape> rect =
    std::make_shared<rebrin::Rectangle>(rebrin::Rectangle({1.0, 2.0, {-3.0, -4.0}}));
    std::shared_ptr <rebrin::Shape> circ =
    std::make_shared<rebrin::Circle>(rebrin::Circle({13.0, 5.0}, 2.0));
    rebrin::Matrix otherMatr;
    otherMatr.addShape(rect);
    otherMatr.addShape(circ);
    rebrin::Matrix matr{otherMatr};
    BOOST_CHECK_EQUAL(matr.getLayers(), otherMatr.getLayers());
    BOOST_CHECK_EQUAL(matr.getLayerSize(), otherMatr.getLayerSize());
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    std::shared_ptr <rebrin::Shape> rect =
    std::make_shared<rebrin::Rectangle>(rebrin::Rectangle({1.0, 2.0, {-3.0, -4.0}}));
    std::shared_ptr <rebrin::Shape> circ =
    std::make_shared<rebrin::Circle>(rebrin::Circle({13.0, 5.0}, 2.0));
    rebrin::Matrix otherMatr;
    otherMatr.addShape(rect);
    otherMatr.addShape(circ);
    double otherLayers = otherMatr.getLayers();
    double otherMaxInLayer = otherMatr.getLayerSize();
    rebrin::Matrix matr{std::move(otherMatr)};

    BOOST_CHECK_EQUAL(matr.getLayers(), otherLayers);
    BOOST_CHECK_EQUAL(matr.getLayerSize(), otherMaxInLayer);
    BOOST_CHECK_EQUAL(otherMatr.getLayers(), 0);
    BOOST_CHECK_EQUAL(otherMatr.getLayerSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CopyAssigmentOperatorTest)
  {
    std::shared_ptr <rebrin::Shape> rect =
    std::make_shared<rebrin::Rectangle>(rebrin::Rectangle({1.0, 2.0, {-3.0, -4.0}}));
    std::shared_ptr <rebrin::Shape> circ =
    std::make_shared<rebrin::Circle>(rebrin::Circle({13.0, 5.0}, 2.0));
    rebrin::Matrix otherMatr;
    otherMatr.addShape(rect);
    otherMatr.addShape(circ);
    rebrin::Matrix matr = otherMatr;
    BOOST_CHECK_EQUAL(matr.getLayers(), otherMatr.getLayers());
    BOOST_CHECK_EQUAL(matr.getLayerSize(), otherMatr.getLayerSize());
  }

  BOOST_AUTO_TEST_CASE(MoveAssigmentOperatorTest)
  {
    std::shared_ptr <rebrin::Shape> rect =
    std::make_shared<rebrin::Rectangle>(rebrin::Rectangle({1.0, 2.0, {-3.0, -4.0}}));
    std::shared_ptr <rebrin::Shape> circ =
    std::make_shared<rebrin::Circle>(rebrin::Circle({13.0, 5.0}, 2.0));
    rebrin::Matrix otherMatr;
    otherMatr.addShape(rect);
    otherMatr.addShape(circ);
    double otherLayers = otherMatr.getLayers();
    double otherMaxInLayer = otherMatr.getLayerSize();
    rebrin::Matrix matr = std::move(otherMatr);

    BOOST_CHECK_EQUAL(matr.getLayers(), otherLayers);
    BOOST_CHECK_EQUAL(matr.getLayerSize(), otherMaxInLayer);
    BOOST_CHECK_EQUAL(otherMatr.getLayers(), 0);
    BOOST_CHECK_EQUAL(otherMatr.getLayerSize(), 0);
  }

  BOOST_AUTO_TEST_CASE(CreatingTest)
  {
    std::shared_ptr <rebrin::Shape> rect0 =
    std::make_shared<rebrin::Rectangle>(rebrin::Rectangle({2.0, 1.0, {-5.0, 5.0}}));
    std::shared_ptr <rebrin::Shape> circ1 =
    std::make_shared<rebrin::Circle>(rebrin::Circle({0.0, 0.0}, 3.0));
    std::shared_ptr <rebrin::Shape> rect2 =
    std::make_shared<rebrin::Rectangle>(rebrin::Rectangle({4.0, 1.0, {-1.0, 0.0}}));
    rebrin::CompositeShape comp(rect0);
    comp.addShape(circ1);
    comp.addShape(rect2);
    rebrin::Matrix matr = comp.createMatrix();

    BOOST_CHECK_EQUAL(matr.getLayers(), 2);
    BOOST_CHECK_EQUAL(matr.getLayerSize(), 2);
    BOOST_CHECK_EQUAL(matr[0][0], rect0);
    BOOST_CHECK_EQUAL(matr[0][1], circ1);
    BOOST_CHECK_EQUAL(matr[1][0], rect2);
  }

BOOST_AUTO_TEST_SUITE_END()
