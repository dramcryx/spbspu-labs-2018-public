#include "matrix.hpp"
#include <iostream>
#include <stdexcept>
#include <memory>

using namespace rebrin;

Matrix::Matrix() :
  layers_(0),
  layer_size_(0),
  matrix_(nullptr)
{

}

Matrix::Matrix(const Matrix & matrix) :
  layers_(matrix.layers_),
  layer_size_(matrix.layer_size_),
  matrix_(new std::shared_ptr <Shape> [layers_ * layer_size_])
{
  for (int i = 0; i < (layers_ * layer_size_); i++) {
    matrix_[i] = matrix.matrix_[i];
  }
}

Matrix::Matrix(Matrix && matrix) :
  layers_(matrix.layers_),
  layer_size_(matrix.layer_size_),
  matrix_(std::move(matrix.matrix_))
{
  matrix.layers_ = 0;
  matrix.layer_size_ = 0;
  matrix.matrix_.reset();
}

Matrix & Matrix::operator= (const Matrix & matrix)
{
  if (this != &matrix)
  {
    layers_ = matrix.layers_;
    layer_size_ = matrix.layer_size_;
    matrix_.reset(new std::shared_ptr<Shape> [layers_ * layer_size_] );
    for(int i = 0; i < (layers_ * layer_size_); i++)
    {
      matrix_[i] = matrix.matrix_[i];
    }
  }
  return *this;
}

Matrix & Matrix::operator= (Matrix && matrix)
{
  layers_ = matrix.layers_;
  layer_size_ = matrix.layer_size_;
  matrix_ = std::move(matrix.matrix_);
  matrix.layers_ = 0;
  matrix.layer_size_ = 0;
  return *this;
}

std::unique_ptr<std::shared_ptr<Shape>[]> Matrix::operator[](const int index) const
{
  if ((index < 0) || (index >= layers_))
  {
    throw std::out_of_range("Bad index");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> layer(new std::shared_ptr<Shape>[layer_size_]);
  for (int i = 0; i < layer_size_; i++)
  {
    layer[i] = matrix_[index * layer_size_ + i];
  }
  return layer;
}

void Matrix::addShape(const std::shared_ptr <Shape> & shape)
{
  if (shape == nullptr) {
    throw std::invalid_argument("Shape is nullptr");
  }
  if ((layers_ == 0) && (layer_size_ == 0)) {
    std::unique_ptr<std::shared_ptr<Shape>[]> newMatrix(new std::shared_ptr<Shape>[1]);
    layer_size_++;
    layers_++;
    newMatrix[0] = shape;
    matrix_.swap(newMatrix);
    return;
  }
  int i = 0;
  for (; i < layers_; i++) {
    int j = 0;
    for(; j < layer_size_; j++) {
      if (matrix_[i * layer_size_ + j] == nullptr) {
        matrix_[i * layer_size_ + j] = shape;
        return;
      }
      if (isOverlapping(matrix_[i * layer_size_ + j], shape)) {
        break;
      }
    }
    if (j == layer_size_) {
      std::unique_ptr<std::shared_ptr<Shape>[]> newMatrix(new std::shared_ptr <Shape> [layers_ * (layer_size_ + 1)]);
      for (int k = 0; k < layers_; k++) {
        for (j = 0; j < layer_size_; j++) {
          newMatrix[k * layer_size_ + j + k] = matrix_[k * layer_size_ + j];
        }
      }
      layer_size_++;
      newMatrix[(i + 1) * layer_size_ - 1] = shape;
      matrix_.swap(newMatrix);
      return;
    }
  }
  if (i == layers_) {
    std::unique_ptr< std::shared_ptr<Shape>[] > newMatrix(new std::shared_ptr <Shape> [(layers_+1) * (layer_size_)]);
    for (int k = 0; k < layers_ * layer_size_; k++) {
      newMatrix[k] = matrix_[k];
    }
    newMatrix[layers_ * layer_size_] = shape;
    layers_++;
    matrix_.swap(newMatrix);
  }
}

int Matrix::getLayers() const
{
  return layers_;
}

int Matrix::getLayerSize() const
{
  return layer_size_;
}

bool Matrix::isOverlapping(const std::shared_ptr <Shape> & shape1, const std::shared_ptr <Shape> & shape2) const
{

  double firstRectLeft = shape1->getFrameRect().pos.x - shape1->getFrameRect().width / 2.0;
  double firstRectRight = shape1->getFrameRect().pos.x + shape1->getFrameRect().width / 2.0;
  double firstRectTop = shape1->getFrameRect().pos.y + shape1->getFrameRect().height / 2.0;
  double firstRectBottom = shape1->getFrameRect().pos.y - shape1->getFrameRect().height / 2.0;

  double secondRectLeft = shape2->getFrameRect().pos.x - shape2->getFrameRect().width / 2.0;
  double secondRectRight = shape2->getFrameRect().pos.x + shape2->getFrameRect().width / 2.0;
  double secondRectTop = shape2->getFrameRect().pos.y + shape2->getFrameRect().height / 2.0;
  double secondRectBottom = shape2->getFrameRect().pos.y - shape2->getFrameRect().height / 2.0;

  if ((firstRectLeft >= secondRectRight) || (firstRectRight <= secondRectLeft)
    || (firstRectTop <= secondRectBottom) || (firstRectBottom >= secondRectTop)) {
    return false;
  } else {
    return true;
  }

}
