#include <stdexcept>
#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
#include "rectangle.hpp"


rebrin::Rectangle::Rectangle(const rectangle_t& parameters) :
  parameters_(parameters),
  angle_(0)
{
  if (parameters.width < 0.0)
  {
    throw std::invalid_argument("Width must be >= 0");
  }
  if (parameters.height < 0.0)
  {
    throw std::invalid_argument("Height must be >= 0");
  }
}

double rebrin::Rectangle::getArea () const
{
  return parameters_.width * parameters_.height;
}

rebrin::rectangle_t rebrin::Rectangle::getFrameRect() const
{
  double left_top_x =
      parameters_.pos.x - (parameters_.width / 2) * cos(angle_ * M_PI / 180) -
      (parameters_.height / 2) * sin(angle_ * M_PI / 180);
  double left_bottom_y =
      parameters_.pos.y - (parameters_.height / 2) * cos(angle_ * M_PI / 180) -
      (parameters_.width / 2) * sin(angle_ * M_PI / 180);
  double right_top_y =
      parameters_.pos.y + (parameters_.height / 2 ) * cos(angle_ * M_PI / 180) +
      (parameters_.width / 2) * sin(angle_ * M_PI / 180);
  double right_bottom_x =
      parameters_.pos.x + (parameters_.width / 2) * cos(angle_ * M_PI / 180) +
      (parameters_.height / 2) * sin(angle_ * M_PI / 180);

  double frame_rect_width = std::abs(right_bottom_x - left_top_x);
  double frame_rect_height = std::abs(left_bottom_y - right_top_y);

  return {frame_rect_width, frame_rect_height, {parameters_.pos}};
}

void rebrin::Rectangle::move(const double dx, const double dy)
{
  parameters_.pos.x += dx;
  parameters_.pos.y += dy;
}

void rebrin::Rectangle::move(const point_t& pos)
{
  parameters_.pos = pos;
}

void rebrin::Rectangle::scale(const double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Factor must be >= 0");
  }

  parameters_.width *= factor;
  parameters_.height *= factor;
}

void rebrin::Rectangle::rotate(const double alpha)
{
  angle_ += alpha;

  while (angle_ > 180)
  {
    angle_ -= 360;
  }

  while (angle_ < -180)
  {
    angle_ += 360;
  }
}
