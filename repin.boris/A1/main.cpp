#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

int main()
{
  Circle shape1({0,0},3);
  Shape *circle = &shape1;
  circle->printInformation();
  std::cout << "Move to (5,-5) and move on (13,10);\n\n";
  circle->move({5,-5});
  circle->move(13,10);
  circle->printInformation();

  Rectangle shape2({0,4},10,20);
  Shape *rectangle = &shape2;
  rectangle->printInformation();
  std::cout << "Move to point (5,-30) and move on (3,-4);\n\n";
  rectangle->move({5,-30});
  rectangle->move(3,-4);
  rectangle->printInformation();

  Triangle shape3({0,0},{0,4},{3,0});
  Shape *triangle = &shape3;
  triangle->printInformation();
  std::cout << "Move to point (-10,4) and move on (-7,18);\n\n";
  triangle->move({-10,4});
  triangle->move(-7,18);
  triangle->printInformation();

  return 0;
}
