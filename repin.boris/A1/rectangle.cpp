#include "rectangle.hpp"
#include <iostream>
#include <cassert>

Rectangle::Rectangle(const point_t &point, const double &width, const double &height):
  position_(point),
  width_(width),
  height_(height)
{
  assert(width >= 0 && height >= 0);
}

double Rectangle::getArea() const
{
  return (width_ * height_);
}

double Rectangle::getWidth() const
{
  return width_;
}

double Rectangle::getHeight() const
{
  return height_;
}

point_t Rectangle::getPosition() const
{
  return position_;
}

rectangle_t Rectangle::getFrameRect() const
{
  rectangle_t rectangle = {width_, height_, position_};
  return rectangle;
}

void Rectangle::move(const point_t &point)
{
  position_ = point;
}

void Rectangle::move(const double &dx, const double &dy)
{
  position_.x += dx;
  position_.y += dy;
}

void Rectangle::printInformation() const
{
  std::cout << "Position - (" << getPosition().x << "," << getPosition().y << ");\n"
    << "Width -" << getWidth() << ";\n"
    << "Height -" << getHeight() << ";\n"
    << "Rectangle area - " << getArea() << ";\n"
    << "Rectangle around Rectangle:\n"
    << "  width - " << getFrameRect().width << ";\n"
    << "  height - " << getFrameRect().height << ";\n"
    << "  position - (" << getFrameRect().pos.x << ","
    << getFrameRect().pos.y << ");\n\n";
}
