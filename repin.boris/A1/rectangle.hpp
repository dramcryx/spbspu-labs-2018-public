#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(const point_t &point, const double &width, const double &height);
  double getArea() const;
  double getWidth() const;
  double getHeight() const;
  point_t getPosition() const;
  void printInformation() const;
  rectangle_t getFrameRect() const;
  void move(const point_t &point);
  void move(const double &dx, const double &dy);

private:
  point_t position_;
  double width_;
  double height_;
};

#endif
