#include <iostream>
#include <stdexcept>
#include "rectangle.hpp"
#include "circle.hpp"

using namespace repin;

int main()
{
  try
  {
    Rectangle shape_1({0, -1}, 20, 5);
    Shape *rectangle = &shape_1;
    std::cout << "rectangle was created\n\n";

    rectangle->printData();

    double dx = -3;
    double dy = 20;

    rectangle->move(dx, dy);
    std::cout << "\nrectangle was moved on (" << dx << ", " << dy << ")\n\n";

    rectangle->printData();

    Circle shape_2({8, 1}, 6);
    Shape *circle = &shape_2;
    std::cout << "\ncircle was created\n\n";

    circle->printData();

    point_t pos = {0, 9};

    circle->move(pos);
    std::cout << "\ncircle was moved to {" << pos.x << ", " << pos.y << "}" << "\n\n";

    circle->printData();
  }
  catch (const std::invalid_argument &e)
  {
    std::cerr << e.what() << "\n";
    return 1;
  }

  return 0;
}
