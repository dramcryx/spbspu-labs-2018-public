#ifndef CIRCLE_HPP_INCLUDED
#define CIRCLE_HPP_INCLUDED
#include "shape.hpp"

namespace repin
{
  class Circle:
    public Shape
  {
  public:
    Circle(const point_t &pos, double radius);
    double getRadius() const noexcept;
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void printFeatures() const noexcept override;
    void move(double dx, double dy) noexcept override;
    void move(const point_t &pos) noexcept override;
    void scale(double k) override;

  private:
    point_t pos_;
    double radius_;
  };
}

#endif
