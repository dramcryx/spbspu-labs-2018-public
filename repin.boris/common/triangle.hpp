#ifndef TRIANGLE_HPP_INCLUDED
#define TRIANGLE_HPP_INCLUDED
#include "shape.hpp"

namespace repin
{
  class Triangle:
    public Shape
  {
  public:
    Triangle(const point_t &, const point_t &, const point_t &);
    point_t getCenter() const noexcept;
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void printFeatures() const noexcept override;
    void move(double dx, double dy) noexcept override;
    void move(const point_t &pos) noexcept override;
    void scale(double k) override;

  private:
    point_t corners_[3];
  };
}

#endif
