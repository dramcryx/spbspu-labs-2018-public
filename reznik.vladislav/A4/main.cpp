#include <iostream>
#include <memory>

#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using shared = std::shared_ptr<reznik::Shape>;

int main()
{

  reznik::Circle circle{10.01, { 5.32, 27.63 } };
  reznik::Rectangle rectangle{7.13, 4.82 , { 25.04, 6.12 } };
  shared circlePtr = std::make_shared<reznik::Circle >(circle);
  shared rectanglePtr = std::make_shared<reznik::Rectangle >(rectangle);


  reznik::Circle circle1{1.0 , { -2.0, -2.0 } };
  reznik::Rectangle rectangle1{2.0, 2.0,  { 2.0, 2.0 }};
  shared circlePtr1 = std::make_shared<reznik:: Circle >(circle1);
  shared rectanglePtr1 = std::make_shared<reznik:: Rectangle >(rectangle1);
  reznik::CompositeShape compositeShape;
  compositeShape.addShape(circlePtr1);
  rectangle1.printInfo();
  circle1.printInfo();

  compositeShape.addShape(rectanglePtr1);
  compositeShape.rotate(45);
  compositeShape.scale(2.0);
  compositeShape.addShape(rectanglePtr);

  reznik::Circle circleM{ 2.0 , { -2.0, -2.0 }};
  reznik::Rectangle rectangleM1{2.0, 2.0 , { -2.0, 0.0 } };
  reznik::Rectangle rectangleM2{ 6.0, 3.0, { 1.0, 1.0 } };
  reznik::Rectangle rectangleM3{ 2.0, 4.0 ,{ 3.0, 1.0 } };
  reznik::Rectangle rectangleM4{4.0, 4.0,  { 3.0, 3.0 } };
  compositeShape.printInfo();
  shared comp = std::make_shared< reznik::CompositeShape >(compositeShape);
  shared circlePtrM = std::make_shared<reznik:: Circle >(circleM);
  shared rectanglePtrM1 = std::make_shared<reznik:: Rectangle >(rectangleM1);
  shared rectanglePtrM2 = std::make_shared<reznik:: Rectangle >(rectangleM2);
  shared rectanglePtrM3 = std::make_shared<reznik:: Rectangle >(rectangleM3);
  shared rectanglePtrM4 = std::make_shared<reznik:: Rectangle >(rectangleM4);

  reznik::Matrix matrix(circlePtrM);
  matrix.addElement(comp);
  matrix.addElement(rectanglePtrM1);
  matrix.addElement(rectanglePtrM2);
  matrix.addElement(rectanglePtrM3);
  matrix.addElement(rectanglePtrM4);

  std::cout << "Matrix:" << std::endl;
  matrix.printInfo();

  return 0;
}
