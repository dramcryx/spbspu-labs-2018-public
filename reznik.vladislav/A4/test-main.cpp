#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using uniqshared = std::unique_ptr<std::shared_ptr<reznik::Shape>[]>;
using shared = std::shared_ptr<reznik::Shape>;

BOOST_AUTO_TEST_SUITE(CompositeShape)
  const double Accuracy = 0.00001;

  BOOST_AUTO_TEST_CASE(Constructor)
  {

    BOOST_CHECK_THROW(reznik::CompositeShape testCompositeShape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Constructor_Copying)
  {
    reznik::Rectangle rect1(3, 4,{5, 6});
    reznik::Rectangle rect2(7, 8, {9, 1});
    reznik::Circle circ(11,{12, 13});
    reznik::Rectangle rect3(4, 1,{6, 7});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared circPtr = std::make_shared<reznik::Circle>(circ);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(circPtr);
    testcompositeShape.addShape(rectPtr3);

    reznik::CompositeShape testcompositeShape1(testcompositeShape);
    for (int i = 0; i < testcompositeShape.getSize(); ++i) {
      reznik::rectangle_t rectangle = testcompositeShape[i]->getFrameRect();
      reznik::rectangle_t rectangle1 = testcompositeShape1[i]->getFrameRect();
      BOOST_CHECK_EQUAL(rectangle.height, rectangle1.height);
      BOOST_CHECK_EQUAL(rectangle.width, rectangle1.width);
      BOOST_CHECK_EQUAL(rectangle.pos.x, rectangle1.pos.x);
      BOOST_CHECK_EQUAL(rectangle.pos.y, rectangle1.pos.y);
    }
  }

  BOOST_AUTO_TEST_CASE(Constructor_Moving)
  {
    reznik::Rectangle rect1(3, 4,{5, 6});
    reznik::Rectangle rect2(7, 8, {9, 1} );
    reznik::Circle circ(11,{12, 13});
    reznik::Rectangle rect3(4, 1,{6, 7});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared circPtr = std::make_shared<reznik::Circle>(circ);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape;
    testcompositeShape.addShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(circPtr);
    testcompositeShape.addShape(rectPtr3);
    reznik::CompositeShape testcompositeShape1(testcompositeShape);
    reznik::CompositeShape testcompositeShape2(std::move(testcompositeShape));
    for (int i = 0; i < testcompositeShape2.getSize(); ++i) {
      reznik::rectangle_t rectangle = testcompositeShape1[i]->getFrameRect();
      reznik::rectangle_t rectangle1 = testcompositeShape2[i]->getFrameRect();
      BOOST_CHECK_EQUAL(rectangle.height, rectangle1.height);
      BOOST_CHECK_EQUAL(rectangle.width, rectangle1.width);
      BOOST_CHECK_EQUAL(rectangle.pos.x, rectangle1.pos.x);
      BOOST_CHECK_EQUAL(rectangle.pos.y, rectangle1.pos.y);
    }
  }

  BOOST_AUTO_TEST_CASE(Assignment_Copy)
  {
    reznik::Rectangle rect1(3, 4,{5, 6});
    reznik::Rectangle rect2(7, 8, {9, 1});
    reznik::Circle circ(11,{12, 13});
    reznik::Rectangle rect3(4, 1,{6, 7});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared circPtr = std::make_shared<reznik::Circle>(circ);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape;
    testcompositeShape.addShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(circPtr);
    testcompositeShape.addShape(rectPtr3);
    reznik::CompositeShape testcompositeShape1(rectPtr1);
    testcompositeShape1 = testcompositeShape;
    for (int i = 0; i < testcompositeShape.getSize(); ++i) {
      reznik::rectangle_t rectangle = testcompositeShape[i]->getFrameRect();
      reznik::rectangle_t rectangle1 = testcompositeShape1[i]->getFrameRect();
      BOOST_CHECK_EQUAL(rectangle.height, rectangle1.height);
      BOOST_CHECK_EQUAL(rectangle.width, rectangle1.width);
      BOOST_CHECK_EQUAL(rectangle.pos.x, rectangle1.pos.x);
      BOOST_CHECK_EQUAL(rectangle.pos.y, rectangle1.pos.y);
    }
  }

  BOOST_AUTO_TEST_CASE(Assignment_Move)
  {
    reznik::Rectangle rect1(3, 4,{5, 6});
    reznik::Rectangle rect2(7, 8, {9, 1});
    reznik::Circle circ(11,{12, 13});
    reznik::Rectangle rect3(4, 1,{6, 7});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared circPtr = std::make_shared<reznik::Circle>(circ);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(circPtr);
    testcompositeShape.addShape(rectPtr3);
    reznik::CompositeShape testcompositeShape1(testcompositeShape);
    reznik::CompositeShape testcompositeShape2(rectPtr1);
    testcompositeShape2 = std::move(testcompositeShape);
    for (int i = 0; i < testcompositeShape2.getSize(); ++i) {
      reznik::rectangle_t rectangle = testcompositeShape1[i]->getFrameRect();
      reznik::rectangle_t rectangle1 = testcompositeShape2[i]->getFrameRect();
      BOOST_CHECK_EQUAL(rectangle.height, rectangle1.height);
      BOOST_CHECK_EQUAL(rectangle.width, rectangle1.width);
      BOOST_CHECK_EQUAL(rectangle.pos.x, rectangle1.pos.x);
      BOOST_CHECK_EQUAL(rectangle.pos.y, rectangle1.pos.y);
    }
  }

  BOOST_AUTO_TEST_CASE(Move_Test)
  {
    reznik::Rectangle rect1(1, 1,{1.5, 1.5});
    reznik::Rectangle rect2(1, 1,{2.5, 2.5});
    reznik::Rectangle rect3(1, 1,{3.5, 3.5});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testCompositeShape;
    testCompositeShape.addShape(rectPtr1);
    testCompositeShape.addShape(rectPtr3);
    testCompositeShape.addShape(rectPtr2);

    testCompositeShape.move({15, 10});
    BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().pos.x, 15, Accuracy);
    BOOST_CHECK_CLOSE(testCompositeShape.getFrameRect().pos.y, 10, Accuracy);
  }


  BOOST_AUTO_TEST_CASE(Relative_Move_Test_CS)
  {
    reznik::Rectangle rect1(1, 1,{1.5, 1.5});
    reznik::Rectangle rect2(1, 1,{2.5, 2.5});
    reznik::Rectangle rect3(1, 1,{3.5, 3.5});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(rectPtr3);
    testcompositeShape.move(-1, 1);
    BOOST_CHECK_CLOSE(testcompositeShape.getFrameRect().pos.x, 1.5, Accuracy);
    BOOST_CHECK_CLOSE(testcompositeShape.getFrameRect().pos.y, 3.5, Accuracy);
  }

  BOOST_AUTO_TEST_CASE(Area_Test_CS)
  {
    reznik::Rectangle rect1(1, 1,{1.5, 1.5});
    reznik::Rectangle rect2(1, 1,{2.5, 2.5});
    reznik::Rectangle rect3(1, 1,{3.5, 3.5});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(rectPtr3);
    BOOST_REQUIRE_EQUAL(testcompositeShape.getArea(), 3);
  }

  BOOST_AUTO_TEST_CASE(Area_Test_Move_CS)
  {
    reznik::Rectangle rect1(1, 1,{1.5, 1.5});
    reznik::Rectangle rect2(1, 1,{2.5, 2.5});
    reznik::Rectangle rect3(1, 1,{3.5, 3.5});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(rectPtr3);
    testcompositeShape.move(5, 5);
    BOOST_CHECK_EQUAL(testcompositeShape.getArea(), 3);
  }

  BOOST_AUTO_TEST_CASE(Frame_Rect_Test_CS)
  {
    reznik::Rectangle rect1(1, 1,{1.5, 1.5});
    reznik::Rectangle rect2(1, 1,{2.5, 2.5});
    reznik::Rectangle rect3(1, 1,{3.5, 3.5});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(rectPtr3);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.x, 2.5, Accuracy);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.y, 2.5, Accuracy);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().width, 3, Accuracy);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().height, 3, Accuracy);
  }

  BOOST_AUTO_TEST_CASE(Scale_Test_CS)
  {
    reznik::Rectangle rect1(1, 1,{1.5, 1.5});
    reznik::Rectangle rect2(1, 1,{2.5, 2.5});
    reznik::Rectangle rect3(1, 1,{3.5, 3.5});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(rectPtr3);
    testcompositeShape.scale(4);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getArea(), 48, Accuracy);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.x, 2.5, Accuracy);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.y, 2.5, Accuracy);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().width, 12, Accuracy);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().height, 12, Accuracy);

  }


  BOOST_AUTO_TEST_CASE(Remove_Element_Test_CS)
  {
    reznik::Rectangle rect1(1, 1,{1.5, 1.5});
    reznik::Rectangle rect2(1, 1,{2.5, 2.5});
    reznik::Rectangle rect3(1, 1,{3.5, 3.5});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(rectPtr3);
    testcompositeShape.deleteShape(0);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.x, 3, Accuracy);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().pos.y, 3, Accuracy);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().width, 2, Accuracy);
    BOOST_CHECK_CLOSE_FRACTION(testcompositeShape.getFrameRect().height, 2, Accuracy);
  }


  BOOST_AUTO_TEST_CASE(Invalid_Argument_Scale_Test_CS)
  {
    reznik::Circle circ(10,{0, 0});
    shared circPtr = std::make_shared<reznik::Circle>(circ);
    reznik::CompositeShape testcompositeShape(circPtr);
    BOOST_CHECK_THROW(testcompositeShape.scale(-1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Argument_RemoveShape_Test_CS)
  {
    reznik::Circle circ(10,{0, 0});
    shared circPtr = std::make_shared<reznik::Circle>(circ);
    reznik::CompositeShape testcompositeShape(circPtr);
    BOOST_CHECK_THROW(testcompositeShape.deleteShape(2), std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(Clear_Test_CS)
  {
    reznik::Circle circ1(10,{0, 0});
    reznik::Circle circ2(20,{10, 10});
    reznik::Circle circ3(30,{20, 20});
    shared circPtr1 = std::make_shared<reznik::Circle>(circ1);
    shared circPtr2 = std::make_shared<reznik::Circle>(circ2);
    shared circPtr3 = std::make_shared<reznik::Circle>(circ3);
    reznik::CompositeShape testcompositeShape(circPtr1);
    testcompositeShape.addShape(circPtr2);
    testcompositeShape.addShape(circPtr3);
    testcompositeShape.deleteShape(2);
    BOOST_REQUIRE_EQUAL(testcompositeShape.getSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(Out_Of_Range_Error_Get_Element_Test_CS)
  {
    reznik::Circle circ(10,{0, 0});
    shared circPtr = std::make_shared<reznik::Circle>(circ);
    reznik::CompositeShape testcompositeShape(circPtr);
    BOOST_CHECK_THROW(testcompositeShape[2], std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(rotate)
  {
    reznik::Rectangle rect1(3, 4,{5, 6});
    reznik::Rectangle rect2(7, 8,{9, 1});
    reznik::Circle circ(11,{12, 13});
    reznik::Rectangle rect3(4, 1,{6, 7});
    shared rectPtr1 = std::make_shared<reznik::Rectangle>(rect1);
    shared rectPtr2 = std::make_shared<reznik::Rectangle>(rect2);
    shared circPtr = std::make_shared<reznik::Circle>(circ);
    shared rectPtr3 = std::make_shared<reznik::Rectangle>(rect3);
    reznik::CompositeShape testcompositeShape(rectPtr1);
    testcompositeShape.addShape(rectPtr2);
    testcompositeShape.addShape(circPtr);
    testcompositeShape.addShape(rectPtr3);

    reznik::CompositeShape testcompositeShape1(testcompositeShape);
    testcompositeShape1.rotate(70);
    BOOST_CHECK_CLOSE(testcompositeShape.getArea(), testcompositeShape1.getArea(), Accuracy);
    for (int i = 0; i < 3; ++i)
    {
      BOOST_CHECK_CLOSE(testcompositeShape[i]->getArea(), testcompositeShape1[i]->getArea(), Accuracy);
    }

  }

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(MatrixTests)
  const double EPS = 0.00001;

  BOOST_AUTO_TEST_CASE(Constructor1)
  {
    BOOST_CHECK_THROW(reznik::Matrix testMatrix(nullptr), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(Constructor2)
  {
    reznik::Circle testCircle(10.0,{100.0, 0.0});
    shared circlePtr = std::make_shared<reznik::Circle>(testCircle);
    reznik::Matrix testMatrix(circlePtr);
    BOOST_CHECK_THROW(testMatrix[-1], std::invalid_argument);
  }


  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    reznik::Circle testCircleM(10.0,{-10.0, 0.0});
    reznik::Rectangle testRectangleM1(20.0, 40.0,{20.0, 30.0});
    reznik::Rectangle testRectangleM2(20.0, 40.0,{30.0, 0.0});
    shared circlePtrM = std::make_shared<reznik::Circle>(testCircleM);
    shared rectanglePtrM1 = std::make_shared<reznik::Rectangle>(testRectangleM1);
    shared rectanglePtrM2 = std::make_shared<reznik::Rectangle>(testRectangleM2);
    reznik::Matrix testMatrix1(circlePtrM);
    testMatrix1.addElement(rectanglePtrM1);
    testMatrix1.addElement(rectanglePtrM2);
    reznik::Matrix testMatrix2(testMatrix1);
    uniqshared layer0 = testMatrix2[0];
    uniqshared layer1 = testMatrix2[1];
    BOOST_CHECK(layer0[0] == circlePtrM);
    BOOST_CHECK(layer0[1] == rectanglePtrM1);
    BOOST_CHECK(layer1[0] == rectanglePtrM2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerNumber(), 2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    reznik::Circle testCircleM(10.0,{-10.0, 0.0});
    reznik::Rectangle testRectangleM1(20.0, 40.0,{20.0, 30.0});
    reznik::Rectangle testRectangleM2(20.0, 40.0,{30.0, 0.0});
    shared circlePtrM = std::make_shared<reznik::Circle>(testCircleM);
    shared rectanglePtrM1 = std::make_shared<reznik::Rectangle>(testRectangleM1);
    shared rectanglePtrM2 = std::make_shared<reznik::Rectangle>(testRectangleM2);
    reznik::Matrix testMatrix1(circlePtrM);
    testMatrix1.addElement(rectanglePtrM1);
    testMatrix1.addElement(rectanglePtrM2);
    reznik::Matrix testMatrix2(std::move(testMatrix1));
    uniqshared layer0 = testMatrix2[0];
    uniqshared layer1 = testMatrix2[1];
    BOOST_CHECK(layer0[0] == circlePtrM);
    BOOST_CHECK(layer0[1] == rectanglePtrM1);
    BOOST_CHECK(layer1[0] == rectanglePtrM2);
    BOOST_REQUIRE_EQUAL(testMatrix1.getLayerNumber(), 0);
    BOOST_REQUIRE_EQUAL(testMatrix1.getLayerSize(), 0);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerNumber(), 2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    reznik::Circle testCircleM2(20.0,{40.0, 30.0});
    reznik::Circle testCircleM1(10.0,{-10.0, 0.0});
    reznik::Rectangle testRectangleM1(20.0, 40.0,{20.0, 30.0});
    reznik::Rectangle testRectangleM2(20.0, 40.0,{30.0, 0.0});
    shared circlePtrM1 = std::make_shared<reznik::Circle>(testCircleM1);
    shared circlePtrM2 = std::make_shared<reznik::Circle>(testCircleM2);
    shared rectanglePtrM1 = std::make_shared<reznik::Rectangle>(testRectangleM1);
    shared rectanglePtrM2 = std::make_shared<reznik::Rectangle>(testRectangleM2);
    reznik::Matrix testMatrix1(circlePtrM1);
    testMatrix1.addElement(rectanglePtrM1);
    testMatrix1.addElement(rectanglePtrM2);
    reznik::Matrix testMatrix2(circlePtrM2);
    testMatrix2 = testMatrix1;
    uniqshared layer0 = testMatrix2[0];
    uniqshared layer1 = testMatrix2[1];
    BOOST_CHECK(layer0[0] == circlePtrM1);
    BOOST_CHECK(layer0[1] == rectanglePtrM1);
    BOOST_CHECK(layer1[0] == rectanglePtrM2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerNumber(), 2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    reznik::Circle testCircleM2(20.0,{40.0, 30.0});
    reznik::Circle testCircleM1(10.0,{-10.0, 0.0});
    reznik::Rectangle testRectangleM1(20.0, 40.0,{20.0, 30.0});
    reznik::Rectangle testRectangleM2(20.0, 40.0,{30.0, 0.0});
    shared circlePtrM1 = std::make_shared<reznik::Circle>(testCircleM1);
    shared circlePtrM2 = std::make_shared<reznik::Circle>(testCircleM2);
    shared rectanglePtrM1 = std::make_shared<reznik::Rectangle>(testRectangleM1);
    shared rectanglePtrM2 = std::make_shared<reznik::Rectangle>(testRectangleM2);
    reznik::Matrix testMatrix1(circlePtrM1);
    testMatrix1.addElement(rectanglePtrM1);
    testMatrix1.addElement(rectanglePtrM2);
    reznik::Matrix testMatrix2(circlePtrM2);
    testMatrix2 = std::move(testMatrix1);
    uniqshared layer0 = testMatrix2[0];
    uniqshared layer1 = testMatrix2[1];
    BOOST_CHECK(layer0[0] == circlePtrM1);
    BOOST_CHECK(layer0[1] == rectanglePtrM1);
    BOOST_CHECK(layer1[0] == rectanglePtrM2);
    BOOST_REQUIRE_EQUAL(testMatrix1.getLayerNumber(), 0);
    BOOST_REQUIRE_EQUAL(testMatrix1.getLayerSize(), 0);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerNumber(), 2);
    BOOST_REQUIRE_EQUAL(testMatrix2.getLayerSize(), 2);
  }

  BOOST_AUTO_TEST_CASE(MatrixGetFramerectTest)
  {
    reznik::Circle testCircleM2(20.0,{40.0, 30.0});
    reznik::Circle testCircleM1(10.0,{-10.0, 0.0});
    reznik::Rectangle testRectangleM1(20.0, 40.0,{20.0, 30.0});
    reznik::Rectangle testRectangleM2(20.0, 40.0,{30.0, 0.0});
    shared circlePtr1 = std::make_shared<reznik::Circle>(testCircleM1);
    shared circlePtr2 = std::make_shared<reznik::Circle>(testCircleM2);
    shared rectanglePtr1 = std::make_shared<reznik::Rectangle>(testRectangleM1);
    shared rectanglePtr2 = std::make_shared<reznik::Rectangle>(testRectangleM2);
    reznik::Matrix testMatrix(circlePtr1);
    testMatrix.addElement(rectanglePtr1);
    testMatrix.addElement(rectanglePtr2);
    testMatrix.addElement(circlePtr2);
    uniqshared layer0 = testMatrix[0];
    uniqshared layer1 = testMatrix[1];
    uniqshared layer2 = testMatrix[2];
    BOOST_CHECK(layer0[0] == circlePtr1);
    BOOST_CHECK(layer0[1] == rectanglePtr1);
    BOOST_CHECK(layer1[0] == rectanglePtr2);
    BOOST_CHECK(layer1[1] == nullptr);
    BOOST_CHECK(layer2[0] == circlePtr2);
    BOOST_CHECK(layer2[1] == nullptr);
    BOOST_CHECK_CLOSE_FRACTION(layer0[0]->getFrameRect().pos.x, -10.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(layer0[1]->getFrameRect().pos.x, 20.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(layer1[0]->getFrameRect().pos.x, 30.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(layer2[0]->getFrameRect().pos.x, 40.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(layer0[0]->getFrameRect().pos.y, 0.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(layer0[1]->getFrameRect().pos.y, 30.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(layer1[0]->getFrameRect().pos.y, 0.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(layer2[0]->getFrameRect().pos.y, 30.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(addCs)
  {
    reznik::Circle testCircleM2(20.0,{40.0, 30.0});
    reznik::Circle testCircleM1(10.0,{-10.0, 0.0});
    reznik::Rectangle testRectangleM1(20.0, 40.0,{20.0, 30.0});
    reznik::Rectangle testRectangleM2(20.0, 40.0,{30.0, 0.0});
    shared circlePtr1 = std::make_shared<reznik::Circle>(testCircleM1);
    shared circlePtr2 = std::make_shared<reznik::Circle>(testCircleM2);
    shared rectanglePtr1 = std::make_shared<reznik::Rectangle>(testRectangleM1);
    shared rectanglePtr2 = std::make_shared<reznik::Rectangle>(testRectangleM2);
    reznik::CompositeShape testCompasiteShape(rectanglePtr1);
    testCompasiteShape.addShape(rectanglePtr2);
    testCompasiteShape.addShape(circlePtr2);

    size_t size = testCompasiteShape.getSize();
    std::shared_ptr<reznik::CompositeShape> array = std::make_shared<reznik::CompositeShape>(testCompasiteShape);
    reznik::Matrix testMatrix(circlePtr1);
    testMatrix.addComposite(array, size);

    uniqshared layer0 = testMatrix[0];
    uniqshared layer1 = testMatrix[1];
    uniqshared layer2 = testMatrix[2];
    BOOST_CHECK(layer0[0] == circlePtr1);
    BOOST_CHECK(layer0[1] == rectanglePtr1);
    BOOST_CHECK(layer1[0] == rectanglePtr2);
    BOOST_CHECK(layer1[1] == nullptr);
    BOOST_CHECK(layer2[0] == circlePtr2);
    BOOST_CHECK(layer2[1] == nullptr);
  }

BOOST_AUTO_TEST_SUITE_END()
