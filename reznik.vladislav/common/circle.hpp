#ifndef CIRCLE_H
#define CIRCLE_H
#include "base-types.hpp"
#include "shape.hpp"

namespace reznik
{
  class Circle :
    public reznik::Shape
  {
  public:
    Circle(double radius, const reznik::point_t& cir);
    reznik::rectangle_t getFrameRect() const override;
    double getArea() const override;
    double getAngle() const override;
    void scale(double k) override;
    void rotate(const double a) override;
    void move(double dx,double dy) override;
    void move(const reznik::point_t& center) override;
    void printInfo() const override;
  private:
    double radius_;
    reznik::point_t cir_;
    double angle_;
  };
}
#endif // Circle_H

