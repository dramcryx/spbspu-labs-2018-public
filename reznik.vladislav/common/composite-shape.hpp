#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <memory>
#include "base-types.hpp"
#include "shape.hpp"

namespace reznik
{
  using uniqshared = std::unique_ptr<std::shared_ptr<reznik::Shape>[]>;
  using shared = std::shared_ptr<reznik::Shape>;
  class CompositeShape :
    public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const shared &shape);
    CompositeShape(const CompositeShape& copy);
    CompositeShape(CompositeShape && other);

    CompositeShape & operator=(const CompositeShape& copy);
    CompositeShape & operator=(CompositeShape && other);

    rectangle_t getFrameRect() const override;
    double getArea() const override;
    double getAngle() const override;
    void scale(double k) override;
    void rotate(const double a) override;
    void move(double dx, double dy) override;
    void move(const point_t& center) override;
    void addShape(const shared shape);
    int getSize() const;
    shared & operator[] (size_t index);
    void printInfo() const override;
    void deleteShape(size_t index);
    void clear();
  private:
    uniqshared arr_;
    size_t count_;
    double angle_;
  };
}

#endif
