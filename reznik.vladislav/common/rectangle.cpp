#include  <stdexcept>
#include <cmath>
#include <math.h>
#include "rectangle.hpp"

reznik::Rectangle::Rectangle(double width,double height, const reznik::point_t& rec):
  width_(width),
  height_(height),
  rec_(rec),
  angle_(0)
{
  if(width_ < 0 || height_ < 0)
  {
    throw std:: invalid_argument("Wrong parametrs");
  }
}

double reznik::Rectangle::getArea() const
{
  return(width_ *height_);
}

double reznik::Rectangle::getAngle() const
{
  return angle_;
}


reznik::rectangle_t reznik::Rectangle::getFrameRect() const
{
  const double sine = sin(angle_* M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  const double height = fabs(height_ * cosine) + fabs(width_ * sine);
  const double width = fabs(width_ * cosine) + fabs(height_ * sine);
  return {width, height, rec_};

}

void reznik::Rectangle::move(double dx,double dy)
{
  rec_.x += dx;
  rec_.y += dy;
}

void reznik::Rectangle::move(const reznik::point_t& center)
{
  rec_ = center;
}

void reznik::Rectangle::scale(double k)
{
  if (k < 0)
  {
    throw std:: invalid_argument("Wrong parametrs");
  }
  else
  {
    width_ *= k;
    height_ *= k;
  }
}

void reznik::Rectangle::rotate(const double a)
{
  angle_ += a;
  if (angle_ >= 360)
  {
    angle_ = fmod(angle_, 360);
  }
}

void reznik::Rectangle::printInfo() const
{
  std::cout << "Rectangle:  " << "  Width - " << width_ << ";  Height - " << height_ << ";"<<std::endl;
  std::cout << "  Center - x = " << rec_.x << ", y = " << rec_.y << ";"<<std::endl;

}

