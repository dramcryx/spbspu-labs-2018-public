#ifndef RECTANGLE_H
#define RECTANGLE_H
#include <memory>
#include "shape.hpp"

namespace reznik
{
  class Rectangle :
    public reznik::Shape
  {
  public:
    Rectangle(double width,double height, const reznik::point_t& rec);
    reznik::rectangle_t getFrameRect() const override;
    double getArea() const override;
    double getAngle() const override;
    void scale(double k) override;
    void rotate(const double a) override;
    void move(double dx,double dy) override;
    void move(const reznik::point_t& center) override;
    void printInfo() const;
  private:
    double width_;
    double height_;
    reznik::point_t rec_;
    double angle_;
  };
}
#endif // RECTANGLE_H
