#ifndef SHAPE_H
#define SHAPE_H
#include <iostream>
#include <memory>
#include "base-types.hpp"

namespace reznik
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual reznik::rectangle_t getFrameRect() const = 0;
    virtual double getArea() const = 0;
    virtual double getAngle() const = 0;
    virtual void scale(double k) = 0;
    virtual void rotate(const double a) = 0;
    virtual void move(double dx,double dy) = 0;
    virtual void move(const reznik::point_t& center) = 0;
    virtual void  printInfo() const = 0;
  };
}
#endif // SHAPE_H 
