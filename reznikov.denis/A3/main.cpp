#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include <iostream>
#include <memory>

void inform(const reznikov::CompositeShape & shapes)
{
  const reznikov::rectangle_t pos_ = shapes.getFrameRect();
  std::cout << "Quantity" << shapes.getSize() << std::endl;
  std::cout << "Center" << pos_.pos.x << ", " << pos_.pos.y << std::endl;
  std::cout << "Width" << pos_.width << std::endl;
  std::cout << "Height:" << pos_.height << std::endl;
  std::cout << "Area:" << shapes.getArea() << std::endl;
  
}

int main()
{
  
  reznikov::CompositeShape objects;
  objects.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle{2, 2,{ 2, 2}}));
  objects.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Circle{ 130, {130, 130}}));
  inform(objects);
  reznikov::CompositeShape objects1;
  objects1.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Rectangle{4, 4,{ 4, 4}}));
  objects1.addShape(std::shared_ptr< reznikov::Shape >(new reznikov::Circle{ 260, {260, 260}}));
  inform(objects1);
  reznikov::CompositeShape objects2(objects1);
  objects1 = std::move(objects);
  objects2.removeElement(0);
  inform(objects);
  objects.deleteAllElement();
  objects1.deleteAllElement();
  objects2.deleteAllElement();
  inform(objects2);
  return 0;
}
