#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

int main()
{
  std::shared_ptr<reznikov::Shape> rect1 = std::make_shared<reznikov::Rectangle>(reznikov::Rectangle(11, 11, { 11, 11 }));
  std::shared_ptr<reznikov::Shape> rect2 = std::make_shared<reznikov::Rectangle>(reznikov::Rectangle(22, 22, { 22, 22 }));
  std::shared_ptr<reznikov::Shape> circ = std::make_shared<reznikov::Circle>(reznikov::Circle(33, { 33, 33 }));
  reznikov::Matrix matrix(rect1);
  matrix.addShape(rect2);
  matrix.addShape(circ);
  std::shared_ptr<reznikov::Shape> rect3 = std::make_shared<reznikov::Rectangle>(reznikov::Rectangle( 44, 44, { 44, 44 }));
  matrix.addShape(rect3);
  return 0;
}
