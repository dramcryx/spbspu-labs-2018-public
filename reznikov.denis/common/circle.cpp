#include "circle.hpp"
#include <iostream>
#include <cmath>


reznikov::Circle::Circle(const double radius, const point_t &center) :
  radius_(radius),
  center_(center)
{
  if (radius <= 0)
  {
    throw std::invalid_argument("Invalid circle radius. Radius must be above zero.");
  }
}

double reznikov::Circle::getArea() const noexcept
{
  return M_PI * radius_ * radius_;
}

reznikov::rectangle_t reznikov::Circle::getFrameRect() const noexcept
{
  return{ 2.0 * radius_, 2.0 * radius_, center_ };
}

void reznikov::Circle::move(const point_t &point) noexcept
{
  center_ = point;
}

void reznikov::Circle::move(const double dx, const double dy) noexcept
{
  center_.x += dx;
  center_.y += dy;
}

void reznikov::Circle::scale(const double coefficient)
{
  if (coefficient < 0.0) {
    throw std::invalid_argument("Invalid scale coefficient. Scale coefficient must be above zero.");
  }
  else
  {
    radius_ *= coefficient;
  }
}

void reznikov::Circle::inf() const noexcept
{
  std::cout << "Circle:" << std::endl;
  std::cout << "  Radius: " << radius_ << "  Position:" << std::endl;
  std::cout << "    x: " << center_.x << "   y: " << center_.y << std::endl;
  std::cout << "  Area: " << this->getArea() << std::endl;
}

double reznikov::Circle::getRadius() const noexcept
{
  return radius_;
}

reznikov::point_t reznikov::Circle::getPosition() const noexcept
{
  return center_;
}

void reznikov::Circle::rotate(double /*angle*/) noexcept
{
}


