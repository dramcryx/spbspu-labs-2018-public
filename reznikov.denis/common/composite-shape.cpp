#include "composite-shape.hpp"

#include <iostream>
#include <memory>
#include <stdexcept>
#include <cmath>

reznikov::CompositeShape::CompositeShape() : 
  elements_(nullptr),
  depth_(0)
{
}

reznikov::CompositeShape::CompositeShape(const std::shared_ptr< reznikov::Shape > &shape) :
  elements_(new std::shared_ptr< reznikov::Shape >[1]),
  depth_(0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("Free pointer");
  }
  elements_[depth_] = shape;
}

reznikov::CompositeShape::CompositeShape(const reznikov::CompositeShape & composite_shape) :
  elements_(new std::shared_ptr< reznikov::Shape >[composite_shape.depth_]),
  depth_(composite_shape.depth_)
{
  for (size_t a = 0; a < depth_; a++)
  {
    elements_[a] = composite_shape.elements_[a];
  }
}

reznikov::CompositeShape::CompositeShape(reznikov::CompositeShape && composite_shape) noexcept :
  depth_(composite_shape.depth_)
{
  elements_ = std::move(composite_shape.elements_);
  composite_shape.elements_.reset();
  composite_shape.depth_ = 0;
}

reznikov::CompositeShape & reznikov::CompositeShape::operator=(const reznikov::CompositeShape & compsoite_shape)
{
  if (this != &compsoite_shape)
  {
    elements_.reset(new std::shared_ptr< reznikov::Shape > [compsoite_shape.depth_] );
    depth_ = compsoite_shape.depth_;
    for (size_t i = 0; i < depth_; ++i)
    {
      elements_[i] = compsoite_shape.elements_[i];
    }
  }
  return *this;
}

reznikov::CompositeShape & reznikov::CompositeShape::operator=(reznikov::CompositeShape && composite_shape) noexcept
{
  if (this != &composite_shape)
  {
    depth_ = composite_shape.depth_;
    elements_ = std::move(composite_shape.elements_);
    composite_shape.elements_ = nullptr;
    composite_shape.depth_ = 0;
  }
  return *this;
}

const std::shared_ptr< reznikov::Shape > & reznikov::CompositeShape::operator[](size_t size) const
{
  if (size >= depth_)
  {
    throw std::out_of_range("Size array must be less than size of the container!");
  }
  return elements_[size];
}

double reznikov::CompositeShape::getArea() const noexcept
{
  double area = 0;
  for (size_t a = 0; a < depth_; a++)
  {
    area += elements_[a]->getArea();
  }
  return area;
}

reznikov::rectangle_t reznikov::CompositeShape::getFrameRect() const noexcept
{
  if (depth_ == 0)
  {
    return reznikov::rectangle_t{ 0, 0, {0, 0}};
  }
  reznikov::rectangle_t compositeFrame = elements_[0]->getFrameRect();
  double top_side = compositeFrame.pos.y + compositeFrame.height / 2;
  double right_side = compositeFrame.pos.x + compositeFrame.width / 2;
  double lower_side = compositeFrame.pos.y - compositeFrame.height / 2;
  double left_side = compositeFrame.pos.x - compositeFrame.width / 2;

  for (size_t a = 1; a < depth_; a++)
  {
    compositeFrame = elements_[a]->getFrameRect();
    double other_top_side = compositeFrame.pos.y + compositeFrame.height / 2;
    double other_right_side = compositeFrame.pos.x + compositeFrame.width / 2;
    double other_lower_side = compositeFrame.pos.y - compositeFrame.height / 2;
    double other_left_side = compositeFrame.pos.x - compositeFrame.width / 2;

    if (top_side < other_top_side)
    {
      top_side = other_top_side;
    }
    if (right_side < other_right_side)
    {
      right_side = other_right_side;
    }
    if (lower_side > other_lower_side)
    {
      lower_side = other_lower_side;
    }
    if (left_side > other_left_side)
    {
      left_side = other_left_side;
    }
  }
  return rectangle_t{(right_side - left_side), (top_side - lower_side), {(left_side + (right_side - left_side ) / 2.0), (lower_side + (top_side - lower_side) / 2.0)} };
}

void reznikov::CompositeShape::move(const reznikov::point_t & point) noexcept
{
  const reznikov::point_t Pos = getFrameRect().pos;
  move(point.x - Pos.x, point.y - Pos.y);
}

void reznikov::CompositeShape::move(const double dx,const double dy) noexcept
{
  for (size_t a = 0; a < depth_; a++)
  {
    elements_[a]->move(dx, dy);
  }
}

void reznikov::CompositeShape::scale(const double coefficient)
{
  if (coefficient < 0.0)
  {
    throw std::invalid_argument("Invalid scale coefficient.Scale coefficient must be above zero.");
  }
  const reznikov::point_t pos = getFrameRect().pos;
  for (size_t a = 0; a < depth_; a++)
  {
    reznikov::point_t pos_shape = elements_[a]->getPosition();
    reznikov::point_t pos_for_move = { pos.x + coefficient * (pos_shape.x - pos.x),pos.y + coefficient * (pos_shape.y - pos.y )};
    elements_[a]->move(pos_for_move);
    elements_[a]->scale(coefficient);
  }
}

void reznikov::CompositeShape::addShape(const std::shared_ptr< reznikov::Shape > &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("pointer can't be zero");
  }
  std::unique_ptr< std::shared_ptr< reznikov::Shape >[] > array(new std::shared_ptr< reznikov::Shape > [depth_ + 1]);
  for (size_t a = 0; a < depth_ ; a++)
  {
     array[a] = elements_[a];
  }
  array[depth_] = shape;
  depth_++;
  elements_.swap(array);
}

void reznikov::CompositeShape::deleteAllElement() noexcept
{
  depth_ = 0;
  elements_ = nullptr;
}

void reznikov::CompositeShape::removeElement(size_t position)
{
  if (position >= depth_)
  {
    throw std::out_of_range("Size array must be less than size of the container.");
  }
  if (depth_ == 0)
  {
    throw std::invalid_argument("CompositeShape is empty");
  }
  std::unique_ptr<std::shared_ptr < reznikov::Shape >[]> array (new std::shared_ptr< reznikov::Shape > [depth_ - 1]);
  for (size_t a = 0; a < position; a++)
  {
    array[a] = elements_[a];
  }
  for (size_t a = position; a < depth_ - 1; ++a)
  {
    array[a] = elements_[a + 1];
  }
  elements_.swap(array);
  depth_--;
}

reznikov::point_t reznikov::CompositeShape::getPosition() const noexcept
{
  return getFrameRect().pos;
}

size_t reznikov::CompositeShape::getSize() const noexcept
{
  return depth_;
}

void reznikov::CompositeShape::inf() const noexcept 
{
  std::cout << "CompositeShape" << std::endl;
}

void reznikov::CompositeShape::rotate(double angle) noexcept
{
  double cosA = cos(angle * M_PI / 180);
  double sinA = sin(angle * M_PI / 180);

  point_t center = getFrameRect().pos;

  for (size_t i = 0; i < depth_; i++)
  {
    point_t shape_center = elements_[i]->getFrameRect().pos;

    elements_[i]->move({ center.x + cosA * (shape_center.x - center.x)
      - sinA * (shape_center.y - center.y),
      center.y + cosA * (shape_center.y - center.y)
      + sinA * (shape_center.x - center.x) });
    elements_[i]->rotate(angle);
  }
}


