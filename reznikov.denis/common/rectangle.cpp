#include "rectangle.hpp"
#include <iostream>
#include <cmath>

reznikov::Rectangle::Rectangle(double width, double height, const point_t &center) :
  width_(width),
  height_(height),
  center_(center),
  dots_{ { center.x - width / 2, center.y - height / 2 },
         { center.x - width / 2, center.y + height / 2 },
         { center.x + width / 2, center.y + height / 2 },
         { center.x + width / 2, center.y - height / 2 } }
{
  if ((width_ < 0.0) || (height_ < 0.0))
  {
    throw std::invalid_argument("Invalid rectangle height or width.They must be above zero");
  }
}

reznikov::rectangle_t reznikov::Rectangle::getFrameRect() const noexcept
{
  double right = dots_[0].x;
  double left = dots_[0].x;
  double top = dots_[0].y;
  double bottom = dots_[0].y;
  for (int i = 1; i < 4; i++)
  {
    if (dots_[i].x > right)
    {
      right = dots_[i].x;
    }
    if (dots_[i].x < left)
    {
      left = dots_[i].x;
    }
    if (dots_[i].y > top)
    {
      top = dots_[i].y;
    }
    if (dots_[i].y < bottom)
    {
      bottom = dots_[i].y;
    }
  }
  return{(top - bottom), (right - left),{ ((right + left) / 2), ((top + bottom) / 2) } };
}



double reznikov::Rectangle::getArea() const noexcept
{
  return width_ * height_;
}

void reznikov::Rectangle::move(const point_t &point) noexcept
{
  for (int i = 0; i < 4; i++)
  {
    dots_[i].x += point.x - center_.x;
    dots_[i].y += point.y - center_.y;
  }
  center_ = point;
}

void reznikov::Rectangle::move(const double dx, const double dy) noexcept
{
  center_.x += dx;
  center_.y += dy;
  for (int i = 0; i < 4; i++)
  {
    dots_[i].x += dx;
    dots_[i].y += dy;
  }
}



void reznikov::Rectangle::inf() const noexcept
{
  std::cout << "Rectangle:" << std::endl;
  std::cout << "Width:" << width_ << std::endl;
  std::cout << "Height:" << height_ << std::endl;
  std::cout << "Position:" << "x:" << center_.x << "y:" << center_.y << std::endl;
  std::cout << "Area:" << this->getArea() << std::endl;
}

reznikov::point_t reznikov::Rectangle::getPosition() const  noexcept
{
  return getFrameRect().pos;
}

void reznikov::Rectangle::scale(const double coefficient)
{
  if (coefficient <= 0.0)
  {
    throw std::invalid_argument("Wrong input data, ratio must be greater than 0");
  }
  height_ *= coefficient;
  width_ *= coefficient;
  dots_[0] = { center_.x + width_ / 2, center_.y + height_ / 2 };
  dots_[1] = { center_.x - width_ / 2, center_.y + height_ / 2 };
  dots_[2] = { center_.x - width_ / 2, center_.y - height_ / 2 };
  dots_[3] = { center_.x + width_ / 2, center_.y - height_ / 2 };
}

void reznikov::Rectangle::rotate(double angle) noexcept
{
  double sinA = sin(angle * M_PI / 180);
  double cosA = cos(angle * M_PI / 180);
  point_t center = getFrameRect().pos;
  for (int i = 0; i < 4; i++)
  {
    dots_[i] = { center.x + (dots_[i].x - center.x) * cosA
      - (dots_[i].y - center.y) * sinA, center.y + (dots_[i].x - center.x) * sinA
      + (dots_[i].y - center.y) * cosA };
  }
}



