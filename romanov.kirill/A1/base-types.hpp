#ifndef BASE_TYPES_H
#define BASE_TYPES_H

#include <iostream>

struct point_t
{
  double x, y;
};

std::ostream & operator <<(std::ostream& os, const point_t& r);

struct rectangle_t
{
  double width, height;
  point_t pos;
};

std::ostream & operator <<(std::ostream& os, const rectangle_t& right);

#endif

