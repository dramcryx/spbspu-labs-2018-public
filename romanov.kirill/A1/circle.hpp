#ifndef CIRCLE_H
#define CIRCLE_H

#include "shape.hpp"

class Circle : public Shape
{
public:
  Circle(double r, const point_t& center);
  rectangle_t getFrameRect() const override;
  double getArea() const override;
  void move(const point_t& r) override;
  void move(const double dx, const double dy) override;
private:
  double r_;
  point_t center_;
};

#endif

