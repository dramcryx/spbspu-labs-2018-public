#include "triangle.hpp"
#include <iostream>
#include <cmath>

Triangle::Triangle(const std::array<point_t, 3>& vertices) : 
  vertices_(vertices)
{
}


double Triangle::getArea() const
{
  const double ab = sqrt(pow((vertices_[1].x - vertices_[0].x), 2)
    + pow((vertices_[1].y - vertices_[0].y), 2)),
    bc = sqrt(pow((vertices_[1].x - vertices_[2].x), 2)
      + pow((vertices_[1].y - vertices_[2].y), 2)),
    ac = sqrt(pow((vertices_[2].x - vertices_[0].x), 2)
      + pow((vertices_[2].y - vertices_[0].y), 2)),
    p = (ab + bc + ac) / 2;
  return sqrt(p * (p - bc) * (p - ab) * (p - ac));
}

rectangle_t Triangle::getFrameRect() const
{
  double xMin = vertices_[0].x,
    xMax = vertices_[0].x,
    yMin = vertices_[0].y,
    yMax = vertices_[0].y;
  for (const point_t& i : vertices_)
  {
    if (i.x > xMax)
    {
      xMax = i.x;
    }
    else if (i.x < xMin)
    {
      xMin = i.x;
    }
    if (i.y > yMax)
    {
      yMax = i.y;
    }
    else if (i.y < yMin)
    {
      yMin = i.y;
    }
  }
  return rectangle_t{ xMax - xMin,
    yMax - yMin,
    point_t { (xMax - xMin) / 2 + xMin,
    (yMax - yMin) / 2 + yMin} };
}

void Triangle::move(const point_t& r)
{
  double offsetX = 0, offsetY = 0;
  for (const point_t& i : vertices_)
  {
    offsetX += i.x;
    offsetY += i.y;
  }
  move(r.x - offsetX / 3, r.y - offsetY / 3);
}

void Triangle::move(const double dx, const double dy)
{
  for (point_t& i : vertices_)
  {
    i.x += dx;
    i.y += dy;
  }
}
