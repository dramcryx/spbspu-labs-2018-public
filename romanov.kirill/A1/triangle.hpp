#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <array>
#include "shape.hpp"

class Triangle : public Shape
{
public:
  Triangle(const std::array<point_t, 3>& vertices);
  rectangle_t getFrameRect() const override;
  double getArea() const override;
  void move(const point_t& r) override;
  void move(const double dx, const double dy) override;
private:
  std::array<point_t, 3> vertices_;
};

#endif

