#ifndef BOOST_TEST_MAIN
#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <limits>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

using namespace romanov;

const std::array<point_t, 3> vertices = {{{ 0, 0 }, { 0, 1 }, { 1, 0 }}};
const std::array<point_t, 3> vertices2 = {{{ 0, 0 }, { 0, 1 }, { 5, 0 }}};
const double accurancy = 1e-10;

BOOST_AUTO_TEST_CASE(circleArea1)
{
  const Circle tmp(3, { 0, 0 });
  BOOST_CHECK(tmp.getArea() == (M_PI * 3 * 3));
}

BOOST_AUTO_TEST_CASE(circleArea2)
{
  const Circle tmp1(5, { 1, -1 });
  BOOST_CHECK(tmp1.getArea() == (M_PI * 5 * 5));
}

BOOST_AUTO_TEST_CASE(rectangleArea1)
{
  const Rectangle tmp(3, 2, { 0, 0 });
  BOOST_CHECK(tmp.getArea() == 3 * 2);
}

BOOST_AUTO_TEST_CASE(rectangleArea2)
{
  const Rectangle tmp1(5, 10, { -12, 123 });
  BOOST_CHECK(tmp1.getArea() == 5 * 10);
}

BOOST_AUTO_TEST_CASE(triangleArea1)
{
  Triangle tmp(vertices);
  BOOST_CHECK_CLOSE_FRACTION(tmp.getArea(), 0.5, accurancy);
}

BOOST_AUTO_TEST_CASE(triangleArea2)
{
  Triangle tmp(vertices2);
  BOOST_CHECK_CLOSE_FRACTION(tmp.getArea(), 2.5, accurancy);
}

BOOST_AUTO_TEST_CASE(rectangleMove)
{
  Rectangle tmp(3, 2, { 0, 0 });
  tmp.move({123.213, -32.123});
  BOOST_CHECK_EQUAL(tmp.getArea(), 3 * 2);
}

BOOST_AUTO_TEST_CASE(rectangleMoveRelative)
{
  Rectangle tmp(3, 2, { 0, 0 });
  tmp.move(123.213, -32.123);
  BOOST_CHECK_EQUAL(tmp.getArea(), 3 * 2);
}

BOOST_AUTO_TEST_CASE(circleMove)
{
  Circle tmp(5, { 1, -1 });
  tmp.move({123.213, -32.123});
  BOOST_CHECK_EQUAL(tmp.getFrameRect().width, 10);
}

BOOST_AUTO_TEST_CASE(circleMoveRelative)
{
  Circle tmp(5, { 1, -1 });
  tmp.move(123.213, -32.123);
  BOOST_CHECK_EQUAL(tmp.getFrameRect().width, 10);
}

BOOST_AUTO_TEST_CASE(triangleMove)
{
  Triangle tmp(vertices);
  tmp.move({0, 3});
  BOOST_CHECK_CLOSE_FRACTION(tmp.getFrameRect().width, 1, accurancy);
}

BOOST_AUTO_TEST_CASE(triangleMoveRelative)
{
  Triangle tmp(vertices);
  tmp.move(2, 3);
  BOOST_CHECK_CLOSE_FRACTION(tmp.getFrameRect().width, 1, accurancy);
}

BOOST_AUTO_TEST_CASE(rectangleScale)
{
   Rectangle tmp(4, 10, {21, 321});
   double k = 0.3;
   tmp.scale(k);
   BOOST_CHECK_CLOSE_FRACTION(tmp.getArea(), 40 * k * k, accurancy);
}

BOOST_AUTO_TEST_CASE(circleScale)
{
   Circle tmp(4, {21, 321});
   double k = 0.3;
   tmp.scale(k);
   BOOST_CHECK_CLOSE_FRACTION(tmp.getArea(), M_PI * 16 * k * k, accurancy);
}

BOOST_AUTO_TEST_CASE(triangleScale)
{
   Triangle tmp(vertices);
   double k = 0.3;
   tmp.scale(k);
   BOOST_CHECK_CLOSE_FRACTION(tmp.getArea(), 0.5 * k * k, accurancy);
}

#endif
