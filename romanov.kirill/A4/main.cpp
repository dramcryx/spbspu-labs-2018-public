#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using shared = std::shared_ptr <romanov::Shape>;

int main()
{
  try
  {
    shared Cir = std::make_shared<romanov::Circle>(romanov::Circle(1, { 1,1 }));
    romanov::CompositeShape CompShape(Cir);
    shared Rec1 = std::make_shared<romanov::Rectangle>(romanov::Rectangle(2, 2, { -1, -1 }));
    shared Rec2 = std::make_shared<romanov::Rectangle>(romanov::Rectangle(2, 2, { 1, 0 }));
    CompShape.addShape(Rec1);
    CompShape.addShape(Rec2);

    CompShape.getInform(std::cout);

    romanov::Matrix Matrix(Cir);
    Matrix.addShape(Rec1);
    Matrix.addShape(Rec2);

    Matrix.getInform(std::cout);
    std::cout << std::endl;

    CompShape.rotate(45);
    std::cout << "Shapes after rotate:" << std::endl;
    CompShape.getInform(std::cout);
    std::cout << std::endl;
  }
  catch (std::exception &cerr)
  {
   std::cerr << cerr.what() << std::endl;
  }

  return 0;
}
