#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <cmath>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace romanov;
const double accuracy = 0.0001;
using shared = std::shared_ptr <Shape>;

BOOST_AUTO_TEST_CASE(TurnRectangle)
{
  class Rectangle Rec(2, 3, {-1, -1});
  Rec.rotate(90);
  BOOST_CHECK_CLOSE_FRACTION(Rec.getFrameRect().width, 3, accuracy);
  BOOST_CHECK_CLOSE_FRACTION(Rec.getFrameRect().height, 2, accuracy);
  BOOST_CHECK_CLOSE_FRACTION(Rec.getFrameRect().pos.x, -1, accuracy);
  BOOST_CHECK_CLOSE_FRACTION(Rec.getFrameRect().pos.y, -1, accuracy);
}

BOOST_AUTO_TEST_CASE(TurnCircle)
{
  Circle Cir(1, { 1,1 });
  Cir.rotate(90);
  BOOST_CHECK_CLOSE_FRACTION(Cir.getFrameRect().width, 2*1, accuracy);
  BOOST_CHECK_CLOSE_FRACTION(Cir.getFrameRect().height, 2*1, accuracy);
  BOOST_CHECK_CLOSE_FRACTION(Cir.getFrameRect().pos.x, 1, accuracy);
  BOOST_CHECK_CLOSE_FRACTION(Cir.getFrameRect().pos.y, 1, accuracy);
}

BOOST_AUTO_TEST_CASE(TurnCompShape)
{
  shared Cir = std::make_shared<Circle>(romanov::Circle(1, { 0, 0 }));
  shared Rec = std::make_shared<Rectangle>(romanov::Rectangle(3, 4, { 0, 0 }));
  CompositeShape Composite(Rec);
  Composite.addShape(Cir);
  const double area = Composite.getArea();
  Composite.rotate(90);
  BOOST_CHECK_CLOSE_FRACTION(area, Composite.getArea(), accuracy);
  BOOST_CHECK_CLOSE_FRACTION(Composite.getFrameRect().width, 4, accuracy);
  BOOST_CHECK_CLOSE_FRACTION(Composite.getFrameRect().height, 3, accuracy);
  BOOST_CHECK_CLOSE_FRACTION(Composite.getFrameRect().pos.x, 0, accuracy);
  BOOST_CHECK_CLOSE_FRACTION(Composite.getFrameRect().pos.y, 0, accuracy);
}

BOOST_AUTO_TEST_CASE(IncorrectConstrucor)
{
  shared Rectangle = nullptr;
  BOOST_CHECK_THROW(Matrix Matrix(Rectangle), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(CopyConstructor)
{
  shared Rec = std::make_shared<class Rectangle>(Rectangle(2, 2, { 1, 0 }));
  Matrix Mtr(Rec);
  Matrix Mtr2(Mtr);
  BOOST_CHECK(Mtr(0, 0) == Mtr2(0, 0));
}

BOOST_AUTO_TEST_CASE(MoveConstructor)
{
 shared Rec = std::make_shared<class Rectangle>(Rectangle(2, 2, { 1, 0 }));
 Matrix Mtr(Rec);
 Matrix Mtr2(std::move(Mtr));
 BOOST_CHECK(Mtr2(0, 0) == Rec);
}

BOOST_AUTO_TEST_CASE(OperatorBrackets)
{
  shared Rec1 = std::make_shared<class Rectangle>(Rectangle(2, 2, { -1, -1 }));
  shared Rec2 = std::make_shared<class Rectangle>(Rectangle(2, 2, { 1,0 }));
  Matrix Mtr(Rec1);
  Mtr.addShape(Rec2);
  BOOST_CHECK(Mtr(0, 0) == Rec1);
  BOOST_CHECK(Mtr(0, 1) == Rec2);
}

BOOST_AUTO_TEST_CASE(OperatorBracketsIncorrectParametr)
{
  shared Rec1 = std::make_shared<class Rectangle>(Rectangle(2, 2, { -1, -1 }));
  Matrix Mtr(Rec1);
  BOOST_CHECK_THROW(Mtr(0, 1), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(addShape)
{
  shared Cir = std::make_shared<class Circle>(Circle(1, { 1,1 }));
  shared Rec1 = std::make_shared<class Rectangle>(Rectangle(2, 2, { -1, -1 }));
  shared Rec2 = std::make_shared<class Rectangle>(Rectangle(2, 2, { 1,0 }));
  Matrix Mtr(Cir);
  Mtr.addShape(Rec1);
  Mtr.addShape(Rec2);
  BOOST_CHECK(Mtr(0, 0) == Cir);
  BOOST_CHECK(Mtr(0, 1) == Rec1);
  BOOST_CHECK(Mtr(1, 0) == Rec2);
}

BOOST_AUTO_TEST_CASE(addShapeIncorrectParametr)
{
  shared Rec1 = std::make_shared<class Rectangle>(Rectangle(2, 2, { -1, -1 }));
  shared Rec2 = nullptr;
  Matrix Mtr(Rec1);
  BOOST_CHECK_THROW(Mtr.addShape(Rec2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(addShapeSameShapes)
{
  shared Rec = std::make_shared<class Rectangle>(Rectangle(2, 2, { -1, -1 }));
  Matrix Mtr(Rec);
  BOOST_CHECK_THROW(Mtr.addShape(Rec), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(Intersection)
{
  shared Cir = std::make_shared<class Circle>(Circle(1, { 1,1 }));
  shared Rec1 = std::make_shared<class Rectangle>(Rectangle(2, 2, { -1, -1 }));
  shared Rec2 = std::make_shared<class Rectangle>(Rectangle(2, 2, { 1, 0 }));
  Matrix Mtr(Cir);
  BOOST_CHECK(Mtr.Intersection(Cir,Rec2));
  BOOST_CHECK(!Mtr.Intersection(Cir, Rec1));
}

BOOST_AUTO_TEST_CASE(IntersectionIncorrectParametr)
{
  shared Cir = std::make_shared<class Circle>(Circle(1, { 1,1 }));
  shared Rec = nullptr;
  Matrix Mtr(Cir);
  BOOST_CHECK_THROW(Mtr.Intersection(Cir, Rec), std::invalid_argument);
}
