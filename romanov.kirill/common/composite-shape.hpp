#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include <memory>
#include "shape.hpp"

namespace romanov {
  class CompositeShape : public Shape
  {
  public:
    CompositeShape();
    CompositeShape(const std::shared_ptr<Shape> &shape);

    ~CompositeShape() = default;

    CompositeShape(const CompositeShape &CompShape);
    CompositeShape(CompositeShape &&CompShape);

    CompositeShape &operator=(const CompositeShape &CompShape);
    CompositeShape &operator=(CompositeShape &&CompShape);

    std::shared_ptr<Shape> operator[](size_t n) const;

    void addShape(const std::shared_ptr<Shape> &shape);
    void removeShape(size_t n);
    void resetShape();

    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void scale(const double ScaleK) override;

    void move(const double dx, double dy) noexcept override;
    void move(const point_t &newpos) noexcept override;

    void getInform(std::ostream &out) const override;

    void rotate(const double angle) noexcept override;

  protected:
    std::unique_ptr <std::shared_ptr<Shape>[]> m_array;
    size_t m_size;
  };
}

#endif

