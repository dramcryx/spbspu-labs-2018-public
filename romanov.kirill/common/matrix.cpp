#include <cmath>
#include <stdexcept>
#include <iostream>
#define _USE_MATH_DEFINES
#include "matrix.hpp"

romanov::Matrix::Matrix(const std::shared_ptr<romanov::Shape> &shape) :
  matrix_array(new std::shared_ptr<romanov::Shape>[1]),
  NumCol(1),
  NumLine(1)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("There is not shape");
  }

  matrix_array[0] = shape;
}

romanov::Matrix::Matrix(const Matrix &Matrix) :
  matrix_array(new std::shared_ptr<romanov::Shape>[Matrix.NumLine * Matrix.NumCol]),
  NumCol(Matrix.NumCol),
  NumLine(Matrix.NumLine)
{
  for (size_t i = 0; i < NumCol * NumLine; i++)
    matrix_array[i] = Matrix.matrix_array[i];
}

romanov::Matrix::Matrix(Matrix &&Matrix) :
  matrix_array(nullptr),
  NumCol(Matrix.NumCol),
  NumLine(Matrix.NumLine)
  {
    matrix_array.swap(Matrix.matrix_array);
    Matrix.NumLine = 0;
    Matrix.NumCol = 0;
  }

std::shared_ptr<romanov::Shape> romanov::Matrix::operator()(const double &line, const double &collumn) const
{
  size_t i = line * NumCol + collumn;
  if ((i >= NumLine*NumCol) || (line >= NumLine) || (collumn >= NumCol))
  {
    throw std::out_of_range("Operator () : out of size range");
  }
  return matrix_array[i];
}

romanov::Matrix &romanov::Matrix::operator=(const Matrix &matrix)
{
  Matrix matrix_(matrix);
  this->matrix_array.swap(matrix_.matrix_array);
  this->NumLine = matrix_.NumLine;
  this->NumCol = matrix_.NumCol;
  return *this;
}

romanov::Matrix &romanov::Matrix::operator=(Matrix &&Matrix)
{
  this->matrix_array.swap(Matrix.matrix_array);
  this->NumCol = Matrix.NumCol;
  this->NumLine = Matrix.NumLine;
  Matrix.matrix_array.reset();
  Matrix.NumLine = 0;
  Matrix.NumCol = 0;
  return *this;
}

void romanov::Matrix::addShape(const std::shared_ptr<romanov::Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("There is not shape");
  }

  for (size_t i = 0; i < NumCol * NumLine; i++)
  {
    if (matrix_array[i] == shape)
    {
      throw std::invalid_argument("The shape is already in matrix");
    }
  }

  size_t layer = 1;

  for (int i = NumLine - 1; i >= 0; i--)
  {
    for (int j = NumCol - 1; j >= 0; j--)
    {
      if (matrix_array[i * NumCol + j] != nullptr)
      {
        if (Intersection(matrix_array[i * NumCol + j], shape))
        {
          layer = i + 1;

          if (layer + 1 > NumLine)
          {
            std::unique_ptr <std::shared_ptr<romanov::Shape>[]> new_array(new std::shared_ptr<romanov::Shape>[(NumLine + 1)*NumCol]);

            for (size_t i = 0; i < NumLine * NumCol; i++)
              new_array[i] = matrix_array[i];

            new_array[NumLine * NumCol] = shape;

            for (size_t i = NumLine * NumCol + 1; i < (NumLine + 1) * NumCol; i++)
              new_array[i] = nullptr;

            matrix_array.swap(new_array);
            NumLine++;
            return;
          }
          else
          {
            for (size_t i = layer * NumLine; i < layer * NumLine + NumCol; i++)
              if (matrix_array[i] == nullptr)
              {
                matrix_array[i] = shape;
                return;
              }

            std::unique_ptr <std::shared_ptr<romanov::Shape>[]> new_array(new std::shared_ptr<romanov::Shape>[NumLine * (NumCol + 1)]);

            for (size_t i = 0; i < NumLine; i++)
              for (size_t j = 0; j < NumCol; j++)
                new_array[i * (NumCol + 1) + j] = matrix_array[i*NumCol + j];

            for (size_t i = 0; i < NumLine; i++)
              new_array[i * (NumCol + 1) + NumCol] = nullptr;

            new_array[layer * (NumCol + 1) + NumCol] = shape;
            matrix_array.swap(new_array);
            NumCol++;
            return;
          }
        }
      }
    }
  }

  for (size_t i = 0; i < NumCol; i++)
    if (matrix_array[i] == nullptr)
    {
      matrix_array[i] = shape;
      return;
    }

  std::unique_ptr <std::shared_ptr<romanov::Shape>[]> new_array(new std::shared_ptr<romanov::Shape>[NumLine * (NumCol + 1)]);

  for (size_t i = 0; i < NumLine; i++)
    for (size_t j = 0; j < NumCol; j++)
      new_array[i * (NumCol + 1) + j] = matrix_array[i*NumCol + j];

  for (size_t i = 0; i < NumLine; i++)
    new_array[i * (NumCol + 1) + NumCol] = nullptr;

  new_array[NumCol] = shape;
  matrix_array.swap(new_array);
  NumCol++;
  return;
}

bool romanov::Matrix::Intersection(const std::shared_ptr<romanov::Shape> &shape1, const std::shared_ptr<romanov::Shape> &shape2)
{
  if (shape1 == nullptr || shape2 == nullptr)
  {
    throw std::invalid_argument("The shapes can not be compared");
  }

  rectangle_t Shape1 = shape1->getFrameRect();
  rectangle_t Shape2 = shape2->getFrameRect();

  return ((abs(Shape1.pos.x - Shape2.pos.x) < ((Shape2.width + Shape1.width) / 2)) && (abs(Shape1.pos.y - Shape2.pos.y) < ((Shape2.height+ Shape1.height) / 2)));
}

void romanov::Matrix::getInform(std::ostream &out) const
{
  out << "Information for the matrix: \n";
  for (size_t i = 0; i < NumLine; i++)
  {
    out << "The number of layer is " << i + 1 << "\n";
    for (size_t j = 0; j < NumCol; j++)
    {
      if (matrix_array[i * NumCol + j] != nullptr)
      {
        out << "The number of shape in the layer is " << j + 1 << "\n";
        matrix_array[i * NumCol + j]->getInform(out);
      }
    }
  }
}

std::ostream &operator<<(std::ostream &out, const romanov::Matrix &matrix)
{
  matrix.getInform(out);
  return out;
}
