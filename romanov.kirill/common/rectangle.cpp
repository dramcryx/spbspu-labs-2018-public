#include <cmath>
#include <stdexcept>
#define _USE_MATH_DEFINES
#include "rectangle.hpp"

romanov::Rectangle::Rectangle(const double width, const double height, const point_t& center) : 
  rect_{ width, height, center },
  alpha_(0)
{
  if ((height < 0.0) || (width < 0.0))
  {
    throw std::invalid_argument("Invalid width / height");
  }
}

double romanov::Rectangle::getArea() const noexcept
{
  return rect_.width * rect_.height;
}

romanov::rectangle_t romanov::Rectangle::getFrameRect() const noexcept
{
    double SinA = abs(sin(alpha_));
    double CosA = sqrt(1 - pow(sin(alpha_), 2));

    double height = rect_.width * SinA + rect_.height * CosA;
    double width = rect_.width * CosA + rect_.height * SinA;

    return { width, height, rect_.pos };
}

void romanov::Rectangle::move(const point_t& r) noexcept
{
  rect_.pos = r;
}

void romanov::Rectangle::move(const double dx, const double dy) noexcept
{
  rect_.pos.x += dx;
  rect_.pos.y += dy;
}

void romanov::Rectangle::scale(const double scaleK)
{
  if (scaleK < 0.0)
  {
    throw std::invalid_argument("ScaleK < 0");
  }
  rect_.height *= scaleK;
  rect_.width *= scaleK;
}

void romanov::Rectangle::getInform(std::ostream &out) const
{
  const rectangle_t rect = getFrameRect();
  out << "Information for the rectangle:\n"
  << "Area:" << getArea() << "\n"
  << "Frame:\n"
  << "width=" << rect.width << "\n"
  << "height=" << rect.height << "\n"
  << "x=" << rect.pos.x << "\n"
  << "y=" << rect.pos.y << "\n";
}

void romanov::Rectangle::rotate(const double angle) noexcept
{
  const double angleConverted = angle * M_PI / 180; //Convertion from degrees to radians.
  alpha_ += angleConverted;
}
