#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"
namespace romanov
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const double width, const double height, const point_t& center);
    rectangle_t getFrameRect() const noexcept override;
    double getArea() const noexcept override;
    void move(const point_t& r) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double scaleK) override;
    void getInform(std::ostream &out) const override;
    void rotate(const double angle) noexcept override;

  private:
    rectangle_t rect_;
    double alpha_;
   };
}
#endif
