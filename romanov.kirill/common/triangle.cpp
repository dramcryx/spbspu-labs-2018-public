#include <cmath>
#include <iostream>
#define _USE_MATH_DEFINES
#include "triangle.hpp"

romanov::Triangle::Triangle(const std::array<point_t, 3>& vertices) : 
  vertices_(vertices)
{
}

double romanov::Triangle::getArea() const noexcept
{
  const double ab = sqrt(pow((vertices_[1].x - vertices_[0].x), 2)
    + pow((vertices_[1].y - vertices_[0].y), 2)),
    bc = sqrt(pow((vertices_[1].x - vertices_[2].x), 2)
      + pow((vertices_[1].y - vertices_[2].y), 2)),
    ac = sqrt(pow((vertices_[2].x - vertices_[0].x), 2)
      + pow((vertices_[2].y - vertices_[0].y), 2)),
    p = (ab + bc + ac) / 2;
  return sqrt(p * (p - bc) * (p - ab) * (p - ac));
}

romanov::rectangle_t romanov::Triangle::getFrameRect() const noexcept
{
  double xMin = vertices_[0].x,
    xMax = vertices_[0].x,
    yMin = vertices_[0].y,
    yMax = vertices_[0].y;
  for (const point_t& i : vertices_)
  {
    if (i.x > xMax)
    {
      xMax = i.x;
    }
    else if (i.x < xMin)
    {
      xMin = i.x;
    }
    if (i.y > yMax)
    {
      yMax = i.y;
    }
    else if (i.y < yMin)
    {
      yMin = i.y;
    }
  }
  return rectangle_t{ yMax - yMin,
    xMax - xMin,
    point_t { (xMax - xMin) / 2 + xMin,
    (yMax - yMin) / 2 + yMin} };
}

void romanov::Triangle::move(const point_t& r) noexcept
{
  double offsetX = 0, offsetY = 0;
  for (const point_t& i : vertices_)
  {
    offsetX += i.x;
    offsetY += i.y;
  }
  move(r.x - offsetX / 3, r.y - offsetY / 3);
}

void romanov::Triangle::move(const double dx, const double dy) noexcept
{
  for (point_t& i : vertices_)
  {
    i.x += dx;
    i.y += dy;
  }
}

void romanov::Triangle::scale(const double scaleK)
{
  if (scaleK < 0.0)
  {
    throw std::invalid_argument("ScaleK < 0");
  }
  point_t center = { 0, 0 };
  for (const point_t& i : vertices_)
  {
    center.x += i.x;
    center.y += i.y;
  }
  center.x /= 3;
  center.y /= 3;
  for (point_t& i : vertices_)
  {
    i = { (center.x - i.x) * scaleK + center.x, (center.y - i.y) * scaleK + center.y };
  }
}

void romanov::Triangle::getInform(std::ostream &out) const
{
    const rectangle_t rect = getFrameRect();
    out << "Information for the triangle:\n"
    << "Area:" << getArea() << "\n"
    << "Frame:\n"
    << "width=" << rect.width << "\n"
    << "height=" << rect.height << "\n"
    << "x=" << rect.pos.x << "\n"
    << "y=" << rect.pos.y << "\n\n";
}

void romanov::Triangle::rotate(const double angle) noexcept
{
   point_t centre = Triangle::getFrameRect().pos;
   const double angleConverted = angle * M_PI / 180; //Convertion from degrees to radians.
   const double sin_a = sin(angleConverted);
   const double cos_a = cos(angleConverted);
   for (size_t i = 0; i < vertices_.size(); i++) {
      vertices_[i] = { centre.x + (vertices_[i].x - centre.x) * cos_a - (vertices_[i].y - centre.y) * sin_a,
         centre.y + (vertices_[i].y - centre.y) * cos_a + (vertices_[i].x - centre.x) * sin_a };
   }
}
