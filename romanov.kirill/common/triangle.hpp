#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include <array>
#include "shape.hpp"

namespace romanov
{
  class Triangle : public Shape
  {
  public:
    Triangle(const std::array<point_t, 3>& vertices);
    rectangle_t getFrameRect() const noexcept override;
    double getArea() const noexcept override;
    void move(const point_t& r) noexcept override;
    void move(const double dx, const double dy) noexcept override;
    void scale(const double scaleK) override;
    void getInform(std::ostream &out) const override;
    void rotate(const double angle) noexcept override;
  private:
    rectangle_t rect_;
    std::array<point_t, 3> vertices_;
  };
}
#endif

