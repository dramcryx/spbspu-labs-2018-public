#include "circle.hpp"

#include <iostream>
#include <cmath>
#include <stdexcept>

Circle::Circle (const point_t center, const double radius):
  center_(center),
  radius_(radius)
{
  if (radius < 0.0) {
    throw std::invalid_argument("Circle's radius must not be negative!");
  }
}

double Circle::getArea() const{
  return (M_PI * radius_ * radius_);
}

rectangle_t Circle::getFrameRect() const{
  return (rectangle_t {center_, 2 * radius_, 2 * radius_});
}

//Moving the circle along the axis
void Circle::move(const double dx, const double dy){
  center_.x += dx;
  center_.y += dy;
}

//Moving the circle to a certain point
void Circle::move(const point_t point){
  center_ = point;
}

void Circle::getInfo() const{
  std::cout << "This is a circle whith center in (" << center_.x << "," << center_.y << ")" << std::endl;
  std::cout << "Its radius is " << radius_ << std::endl;
  std::cout << "Its area is " << getArea() << std::endl;
  rectangle_t rect = getFrameRect();
  std::cout << "Its frame rectangle is centered in (" << rect.pos.x << "," << rect.pos.y << ")" << std::endl;
  std::cout << "Its frame rectangle has height and width " << rect.height << " " << rect.width << std::endl << std::endl;
}

