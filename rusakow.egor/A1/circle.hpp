#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

#include <cmath>
#include <iostream>

class Circle: public Shape
{
public:
  Circle(const point_t center, const double radius);

  double getArea() const override;

  rectangle_t getFrameRect() const override;

  //Moves circle by its center by dx and dy units along the axis
  void move(const double dx,const double dy) override;

  //Moves circle by its center? placing it in said point
  void move(const point_t point) override;

  void getInfo() const override;

private:
  point_t center_;
  double radius_;
};

#endif // CIRCLE_HPP

