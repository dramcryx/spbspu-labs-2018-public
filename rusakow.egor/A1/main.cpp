#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "shape.hpp"

#include <iostream>
#include <stdexcept>

void test (Shape & obj){
  std::cout << "Object's' stats before transforming:" << std::endl;

  obj.getInfo();

  obj.move({90, 90});
  obj.move(10, 10);

  std::cout << "Object's stats after transforming:" << std::endl;
  obj.getInfo();
}

int main(){
  try{
    Circle circ {{0, 0}, 7};
    Rectangle rect {{3, 4}, 10, 5};
    Triangle tri {{2,0}, {0, -2}, {3, 5}};

    test (circ);
    test (rect);
    test (tri);
  }
  catch (std::exception & e){
    std::cerr << e.what() << std::endl;
  }

  return (0);
}
