#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>

Rectangle::Rectangle (const point_t center, const double height, const double width):
  center_(center),
  height_(height),
  width_(width)
{
  if ((height < 0.0) || (width < 0.0)) {
    throw std::invalid_argument("Rectangle's width and height must no be negative!" );
  }
}

double Rectangle::getArea() const{
  return (height_ * width_);
}

rectangle_t Rectangle::getFrameRect() const{
  return (rectangle_t {center_, height_, width_} );
}

//writes info
void Rectangle::getInfo() const{
  std::cout<< "Rectangle's center is in (" << center_.x << "," << center_.y << ")" << std::endl;
  std::cout << "Its width is " << width_ << std::endl;
  std::cout << "Its height is " << height_ << std::endl;
  std::cout << "Its area is " << getArea() << std::endl << std::endl;
}

//Moves the rectangle by dx and dy
void Rectangle::move(const double dx, const double dy){
  center_.x += dx;
  center_.y += dy;
}

//Moves the rectangle to a certain point
void Rectangle::move(const point_t point){
  center_ = point;
}

