#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

#include <iostream>

class Rectangle: public Shape
{
public:
  Rectangle(const point_t center, const double height, const double width);

  double getArea() const override;

  rectangle_t getFrameRect() const override;

  //Moves rectangle by its centre placing it in said point
  void move(const point_t point) override;

  //Moves rectangle by its center moving it by said dx and dy along the axis
  void move(const double dx, const double dy) override;


  void getInfo() const override;

private:
  point_t center_;
  double height_;
  double width_;
};

#endif // RECTANGLE_HPP

