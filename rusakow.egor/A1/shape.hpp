#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

class Shape{
public:
  virtual ~Shape() = default;

  virtual double getArea() const = 0;

  virtual rectangle_t getFrameRect() const = 0;

  //Moves figure's center to the point
  virtual void move(point_t point) = 0;

  //Moves figure's center by dx and dy
  virtual void move(double dx, double dy) = 0;

  virtual void getInfo() const = 0;

};
#endif // SHAPE_HPP

