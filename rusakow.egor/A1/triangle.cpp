#include "triangle.hpp"

#include <cmath>
#include <iostream>
#include <stdexcept>

Triangle::Triangle (const point_t a, const point_t b, const point_t c):
  a_(a),
  b_(b),
  c_(c)
{
  center_ = {(a_.x + b_.x + c_.x) / 3.0, (a_.y + b_.y + c_.y) / 3.0};
  if (getArea() <= 0.0) {
    throw std::invalid_argument("Triangle with such vertex coordinates does not exist!");
  }
}

//calculates triangles area using Heron's formula
double Triangle::getArea() const{
  double ab = sqrt((a_.x - b_.x) * (a_.x - b_.x) + (a_.y - b_.y) * (a_.y - b_.y));
  double bc = sqrt((b_.x - c_.x) * (b_.x - c_.x) + (b_.y - c_.y) * (b_.y - c_.y));
  double ca = sqrt((c_.x - a_.x) * (c_.x - a_.x) + (c_.y - a_.y) * (c_.y - a_.y));
  double p = (ab + bc + ca) / 2.0;
  return std::sqrt(p * (p - ab) * (p - bc) * (p - ca));
}

rectangle_t Triangle::getFrameRect() const{
  double rightEdge = std::max(std::max(a_.x, b_.x), c_.x);
  double leftEdge = std::min(std::min(a_.x, b_.x), c_.x);
  double upperEdge = std::max(std::max(a_.y, b_.y), c_.y);
  double lowerEdge = std::min(std::min(a_.y, b_.y), c_.y);
  return {{(rightEdge + leftEdge) / 2.0, (upperEdge + lowerEdge) / 2.0}, upperEdge - lowerEdge, rightEdge - leftEdge};
}

//Move by dx and dy
void Triangle::move(const double dx, const double dy){
  center_.x += dx;
  center_.y += dy;
  a_.y += dy;
  a_.x += dx;
  b_.y += dy;
  b_.x += dx;
  c_.y += dy;
  c_.x += dx;
}

//Move to the point
void Triangle::move(const point_t point){
  double dx = point.x - center_.x;
  double dy = point.y - center_.y;
  center_ = point;
  a_.x += dx;
  a_.y += dy;
  b_.x += dx;
  b_.y += dy;
  c_.x += dx;
  c_.y += dy;
}


void Triangle::getInfo() const{
  std::cout << "Triangle's center is in " << center_.x << " " << center_.y << std::endl;
  std::cout << "Its vertexes' coordinates are:" << std::endl;
  std::cout << a_.x << " " << a_.y << std::endl;
  std::cout << b_.x << " " << b_.y << std::endl;
  std::cout << c_.x << " " << c_.y << std::endl;
  std::cout << "Triangle's area is " << getArea() << std::endl;
  std::cout << "Its frame rectangle is centered in ";
  rectangle_t rect = getFrameRect();
  std::cout << rect.pos.x << ", " << rect.pos.y << std::endl;
  std::cout << "Its height is " << rect.height << ", and width is " << rect.height << std::endl << std::endl;
}

