#include "../common/circle.hpp"
#include "../common/rectangle.hpp"
#include "../common/triangle.hpp"
#include "../common/shape.hpp"

#include <iostream>
#include <stdexcept>

void test (rusakow::Shape & obj){
  std::cout << "Object's' stats before transforming:" << std::endl;

  obj.getInfo();

  obj.move ({90.0, 90.0});
  obj.move (10.0, 10.0);

  obj.scale (2.0);

  std::cout << "Object's stats after transforming:" << std::endl;
  obj.getInfo();
}

int main(){
  try{
    rusakow::Circle circ {{0.0, 0.0}, 7.0};
    rusakow::Rectangle rect {{3.0, 4.0}, 10.0, 5.0};
    rusakow::Triangle tri {{2.0, 0.0}, {0.0, -2.0}, {3.0, 5.0}};

    test (circ);
    test (rect);
    test (tri);
  }
  catch (const std::invalid_argument & e){
    std::cerr << e.what() << std::endl;
  }

  return (0);
}
