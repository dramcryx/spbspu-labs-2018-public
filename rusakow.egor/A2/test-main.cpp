#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include "../common/rectangle.hpp"
#include "../common/circle.hpp"
#include "../common/triangle.hpp"

#include <stdexcept>

const double approximation = 0.001;

BOOST_AUTO_TEST_SUITE(rectangle_tests)

  BOOST_AUTO_TEST_CASE(rectangle_move_by_x_y)
  {
    rusakow::Rectangle rect({0.0, 0.0}, 10.0, 9.0);
    const rusakow::point_t center_before({rect.getFrameRect().pos.x, rect.getFrameRect().pos.y});
    const double area = rect.getArea();
    const double width = rect.getFrameRect().width;
    const double height = rect.getFrameRect().height;
    rect.move(3.25, 8.1);
    BOOST_CHECK_CLOSE(center_before.x, rect.getFrameRect().pos.x - 3.25, approximation);
    BOOST_CHECK_CLOSE(center_before.y, rect.getFrameRect().pos.y - 8.1, approximation);
    BOOST_CHECK_CLOSE(width, rect.getFrameRect().width, approximation);
    BOOST_CHECK_CLOSE(height, rect.getFrameRect().height, approximation);
    BOOST_CHECK_CLOSE(area, rect.getArea(), approximation);
  }

  BOOST_AUTO_TEST_CASE(rectangle_move_to_x_y)
  {
    rusakow::Rectangle rect({-2.6, -23.5}, 12.3, 4.89);
    const double area = rect.getArea();
    const double width = rect.getFrameRect().width;
    const double height = rect.getFrameRect().height;
    rect.move({-0.45, 2.4});
    BOOST_CHECK_CLOSE(width, rect.getFrameRect().width, approximation);
    BOOST_CHECK_CLOSE(height, rect.getFrameRect().height, approximation);
    BOOST_CHECK_CLOSE(area, rect.getArea(), approximation);
  }

  BOOST_AUTO_TEST_CASE(rectangle_scale)
  {
    rusakow::Rectangle rect({-0.4, 4.5}, 4.5, 5.4);
    const double area = rect.getArea();
    const double width = rect.getFrameRect().width;
    const double height = rect.getFrameRect().height;
    const double coefficient = 2.5;
    rect.scale(coefficient);
    BOOST_CHECK_CLOSE(area * coefficient * coefficient, rect.getArea(), approximation);
    BOOST_CHECK_CLOSE(height * coefficient, rect.getFrameRect().height, approximation);
    BOOST_CHECK_CLOSE(width * coefficient, rect.getFrameRect().width, approximation);
  }

  BOOST_AUTO_TEST_CASE(rectangle_wrong_data)
  {
    BOOST_CHECK_THROW(rusakow::Rectangle({0.0, -5.0}, 0, -10), std::invalid_argument);
    rusakow::Rectangle rect({0.0, 5.0}, 5, 5);
    BOOST_CHECK_THROW(rect.scale(-0.32), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(circle_tests)

  BOOST_AUTO_TEST_CASE(circle_move_by_x_y)
  {
    rusakow::Circle circl({-2.0, 2.0}, 5.2);
    const rusakow::point_t center_before({circl.getFrameRect().pos.x, circl.getFrameRect().pos.y});
    const double area = circl.getArea();
    const double width = circl.getFrameRect().width;
    const double height = circl.getFrameRect().height;
    circl.move(1.0, 2.0);
    BOOST_CHECK_CLOSE(center_before.x, circl.getFrameRect().pos.x - 1.0, approximation);
    BOOST_CHECK_CLOSE(center_before.y, circl.getFrameRect().pos.y - 2.0, approximation);
    BOOST_CHECK_CLOSE(width, circl.getFrameRect().width, approximation);
    BOOST_CHECK_CLOSE(height, circl.getFrameRect().height, approximation);
    BOOST_CHECK_CLOSE(area, circl.getArea(), approximation);
  }

  BOOST_AUTO_TEST_CASE(circle_move_to_x_y)
  {
    rusakow::Circle circl({3.5, -2.3}, 2.0);
    const double area = circl.getArea();
    const double width = circl.getFrameRect().width;
    const double height = circl.getFrameRect().height;
    circl.move({0.4, 2.3});
    BOOST_CHECK_CLOSE(width, circl.getFrameRect().width, approximation);
    BOOST_CHECK_CLOSE(height, circl.getFrameRect().height, approximation);
    BOOST_CHECK_CLOSE(area, circl.getArea(), approximation);
  }

  BOOST_AUTO_TEST_CASE(circle_scale)
  {
    rusakow::Circle circl({-2.3, -1.9}, 3.0);
    const double area = circl.getArea();
    const double coefficient = 2.7;
    circl.scale(coefficient);
    BOOST_CHECK_CLOSE(area * coefficient * coefficient, circl.getArea(), approximation);
  }

  BOOST_AUTO_TEST_CASE(circle_wrong_data)
  {
    BOOST_CHECK_THROW(rusakow::Circle({0.5, 1.5}, -5.3), std::invalid_argument);
    rusakow::Circle circl({2.34, 5.12}, 5.3);
    BOOST_CHECK_THROW(circl.scale(-2.5), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(triangle_tests)

  BOOST_AUTO_TEST_CASE(triangle_get_area)
  {
    rusakow::Triangle triangl({0.0, 4.0}, {0.0, 0.0}, {4.0, 0.0});
    BOOST_CHECK_CLOSE(8.0, triangl.getArea(), approximation);
  }

  BOOST_AUTO_TEST_CASE(triangle_move_by_x_y)
  {
    rusakow::Triangle triangl({-2.56, 1.23}, {0.0, -1.2}, {2.0, 1.23});
    const double area = triangl.getArea();
    const double width = triangl.getFrameRect().width;
    const double height = triangl.getFrameRect().height;
    triangl.move(5.2, -1.23);
    BOOST_CHECK_CLOSE(width, triangl.getFrameRect().width, approximation);
    BOOST_CHECK_CLOSE(height, triangl.getFrameRect().height, approximation);
    BOOST_CHECK_CLOSE(area, triangl.getArea(), approximation);
  }

  BOOST_AUTO_TEST_CASE(triangle_move_to_x_y)
  {
    rusakow::Triangle triangl({-2.28, 0.0}, {1.0, 3.24}, {6.23, -3.45});
    const double area = triangl.getArea();
    const double width = triangl.getFrameRect().width;
    const double height = triangl.getFrameRect().height;
    triangl.move({-2.32, 2.32});
    BOOST_CHECK_CLOSE(width, triangl.getFrameRect().width, approximation);
    BOOST_CHECK_CLOSE(height, triangl.getFrameRect().height, approximation);
    BOOST_CHECK_CLOSE(area, triangl.getArea(), approximation);
  }

  BOOST_AUTO_TEST_CASE(triangle_scale)
  {
    rusakow::Triangle triangl({0.0, 4.3}, {0.0, 0.0}, {3.23, -2.34});
    const double area = triangl.getArea();
    const double coefficient = 0.23;
    triangl.scale(coefficient);
    BOOST_CHECK_CLOSE(area * coefficient * coefficient, triangl.getArea(), approximation);
  }

  BOOST_AUTO_TEST_CASE(triangle_wrong_data)
  {
    BOOST_CHECK_THROW(rusakow::Triangle({2.1, 2.1}, {2.1, -1.5}, {2.1, -1.5}), std::invalid_argument);
    rusakow::Triangle triangl({0.0, 2.3}, {2.34, -3.4}, {6.56, 3.45});
    BOOST_CHECK_THROW(triangl.scale(-1.23), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

