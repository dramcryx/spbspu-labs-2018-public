#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>
#include "../common/rectangle.hpp"
#include "../common/circle.hpp"
#include "../common/triangle.hpp"
#include "../common/composite-shape.hpp"

#include <stdexcept>

const double approximation = 0.001;

BOOST_AUTO_TEST_SUITE (CompositeShape_tests)

  BOOST_AUTO_TEST_CASE (Composite_shape_matches_single_shape){
    rusakow::Triangle trian({0.0, 0.0},{7.0, 5.0},{7.0, 0.0});
    std::shared_ptr <rusakow::Shape> trianPtr = std::make_shared <rusakow::Triangle> (trian);
    rusakow::CompositeShape compSh;
    compSh.addShape (trianPtr);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().height, trian.getFrameRect().height, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().width, trian.getFrameRect().width, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().pos.x, trian.getFrameRect().pos.x, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().pos.y, trian.getFrameRect().pos.y, approximation);
    BOOST_CHECK_CLOSE (compSh.getArea(), trian.getArea(), approximation);
  }

  BOOST_AUTO_TEST_CASE (Composite_shape_area_is_sum_of_subareas){
    rusakow::Circle circ ({6.0, 2.0}, 1.0);
    std::shared_ptr <rusakow::Shape> circPtr = std::make_shared <rusakow::Circle> (circ);
    rusakow::Triangle trian ({2.0, 4.0}, {2.0, 6.0}, {4.0, 4.0});
    std::shared_ptr <rusakow::Shape> trianPtr = std::make_shared <rusakow::Triangle> (trian);
    rusakow::Rectangle rect ({2.0, 2.0}, 2.0, 2.0);
    std::shared_ptr <rusakow::Shape> rectPtr = std::make_shared <rusakow::Rectangle> (rect);
    rusakow::CompositeShape compSh;
    compSh.addShape (circPtr);
    compSh.addShape (trianPtr);
    compSh.addShape (rectPtr);
    BOOST_CHECK_CLOSE (compSh.getArea(), (circ.getArea() + trian.getArea() + rect.getArea()), approximation);
  }

  BOOST_AUTO_TEST_CASE (Composite_shape_scaling_proportionality){
    rusakow::CompositeShape compSh;
    rusakow::Circle circ ({6.0, 2.0}, 1.0);
    std::shared_ptr <rusakow::Shape> circPtr = std::make_shared <rusakow::Circle> (circ);
    rusakow::Triangle trian ({2.0, 4.0}, {2.0, 6.0}, {4.0, 4.0});
    std::shared_ptr <rusakow::Shape> trianPtr = std::make_shared <rusakow::Triangle> (trian);
    rusakow::Rectangle rect ({2.0, 2.0}, 2.0, 2.0);
    std::shared_ptr <rusakow::Shape> rectPtr = std::make_shared <rusakow::Rectangle> (rect);
    compSh.addShape (circPtr);
    compSh.addShape (trianPtr);
    compSh.addShape (rectPtr);
    double areaBeforeScaling = compSh.getArea();
    rusakow::rectangle_t frameRectBeforeScaling = compSh.getFrameRect();
    double coefficient = 2.0;
    compSh.scale (coefficient);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().height, frameRectBeforeScaling.height * coefficient, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().width, frameRectBeforeScaling.width * coefficient, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().pos.x, frameRectBeforeScaling.pos.x, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().pos.y, frameRectBeforeScaling.pos.y, approximation);
    BOOST_CHECK_CLOSE (compSh.getArea(), areaBeforeScaling * coefficient * coefficient, approximation);
  }

  BOOST_AUTO_TEST_CASE (Moving_to_a_certain_position){
    rusakow::CompositeShape compSh;
    rusakow::Circle circ ({6.0, 2.0}, 1.0);
    std::shared_ptr <rusakow::Shape> circPtr = std::make_shared <rusakow::Circle> (circ);
    rusakow::Triangle trian ({2.0, 4.0}, {2.0, 6.0}, {4.0, 4.0});
    std::shared_ptr <rusakow::Shape> trianPtr = std::make_shared <rusakow::Triangle> (trian);
    rusakow::Rectangle rect ({2.0, 2.0}, 2.0, 2.0);
    std::shared_ptr <rusakow::Shape> rectPtr = std::make_shared <rusakow::Rectangle> (rect);
    compSh.addShape (circPtr);
    compSh.addShape (trianPtr);
    compSh.addShape (rectPtr);
    double areaBeforeMoving = compSh.getArea();
    rusakow::rectangle_t frRectBeforeMoving = compSh.getFrameRect();
    compSh.move ({10.0, 10.0});
    BOOST_CHECK_CLOSE (compSh.getArea(), areaBeforeMoving, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().height, frRectBeforeMoving.height, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().width, frRectBeforeMoving.width, approximation);
  }

  BOOST_AUTO_TEST_CASE (Moving_by_vector){
    rusakow::CompositeShape compSh;
    rusakow::Circle circ ({6.0, 2.0}, 1.0);
    std::shared_ptr <rusakow::Shape> circPtr = std::make_shared <rusakow::Circle> (circ);
    rusakow::Triangle trian ({2.0, 4.0}, {2.0, 6.0}, {4.0, 4.0});
    std::shared_ptr <rusakow::Shape> trianPtr = std::make_shared <rusakow::Triangle> (trian);
    rusakow::Rectangle rect ({2.0, 2.0}, 2.0, 2.0);
    std::shared_ptr <rusakow::Shape> rectPtr = std::make_shared <rusakow::Rectangle> (rect);
    compSh.addShape (circPtr);
    compSh.addShape (trianPtr);
    compSh.addShape (rectPtr);
    double areaBeforeMoving = compSh.getArea();
    rusakow::rectangle_t frRectBeforeMoving = compSh.getFrameRect();
    compSh.move (10.0, 10.0);
    BOOST_CHECK_CLOSE (compSh.getArea(), areaBeforeMoving, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().height, frRectBeforeMoving.height, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().width, frRectBeforeMoving.width, approximation);
  }

  BOOST_AUTO_TEST_CASE (Wrong_scaling_parameter){
    rusakow::CompositeShape compSh;
    rusakow::Circle circ ({6.0, 2.0}, 1.0);
    std::shared_ptr <rusakow::Shape> circPtr = std::make_shared <rusakow::Circle> (circ);
    compSh.addShape (circPtr);
    BOOST_CHECK_THROW (compSh.scale(-0.58), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE (Wrong_index){
    rusakow::CompositeShape compSh;
    rusakow::Circle circ ({6.0, 2.0}, 1.0);
    std::shared_ptr <rusakow::Shape> circPtr = std::make_shared <rusakow::Circle> (circ);
    compSh.addShape (circPtr);
    BOOST_CHECK_THROW (compSh[2], std::out_of_range);
  }

    BOOST_AUTO_TEST_CASE (Adding_nullptr_to_CompositeShape){
    std::shared_ptr <rusakow::Shape> ghostShape = nullptr;
    rusakow::CompositeShape compSh;
    BOOST_CHECK_THROW (compSh.addShape(ghostShape), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Moving_and_copying)

  BOOST_AUTO_TEST_CASE (Copying_and_moving){
    rusakow::CompositeShape compSh;
    rusakow::Circle circ ({6.0, 2.0}, 1.0);
    std::shared_ptr <rusakow::Shape> circPtr = std::make_shared <rusakow::Circle> (circ);
    rusakow::Triangle trian ({2.0, 4.0}, {2.0, 6.0}, {4.0, 4.0});
    std::shared_ptr <rusakow::Shape> trianPtr = std::make_shared <rusakow::Triangle> (trian);
    rusakow::Rectangle rect ({2.0, 2.0}, 2.0, 2.0);
    std::shared_ptr <rusakow::Shape> rectPtr = std::make_shared <rusakow::Rectangle> (rect);
    compSh.addShape (circPtr);
    compSh.addShape (trianPtr);
    compSh.addShape (rectPtr);
    rusakow::CompositeShape compShCopy (compSh);
    BOOST_CHECK_CLOSE (compSh.getArea(), compShCopy.getArea(), approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().pos.x, compShCopy.getFrameRect().pos.x, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().pos.y, compShCopy.getFrameRect().pos.y, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().height, compShCopy.getFrameRect().height, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().width, compShCopy.getFrameRect().width, approximation);
  }

BOOST_AUTO_TEST_SUITE_END()

