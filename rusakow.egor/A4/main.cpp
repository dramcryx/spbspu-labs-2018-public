#include "../common/rectangle.hpp"
#include "../common/circle.hpp"
#include "../common/composite-shape.hpp"
#include "../common/matrix.hpp"
#include "../common/polygon.hpp"
#include "../common/triangle.hpp"


int main() {
  try {
    std::shared_ptr <rusakow::Shape> rectPtr (new rusakow::Rectangle ({3.0, 1.0}, 2.0, 1.0));
    std::shared_ptr <rusakow::Shape> circPtr (new rusakow::Circle ({1.0, 2.0}, 1.0));
    std::shared_ptr <rusakow::Shape> polygPtr (new rusakow::Polygon ({{5.0, 0.0}, {3.0, 2.0}, {5.0, 4.0}, {7.0, 2.0}}));
    std::shared_ptr <rusakow::Shape> trianPtr (new rusakow::Triangle ({10.0, 10.0}, {12.0, 11.0}, {11.0, 12.0}));

    rusakow::CompositeShape compSh;
    compSh.addShape (circPtr);
    compSh.addShape (polygPtr);
    compSh.getInfo();
    compSh.rotate(90);
    compSh.getInfo();

    rusakow::Matrix matr (compSh);
    std::cout << "Making a matrix from Composite shape..." << std::endl;
    std::cout<<"Matrix consists of "<<matr.getLayersNum()<<" layers"<< std::endl << std::endl;

    std::cout << "Adding a shape to matrix:" << std::endl;
    rectPtr -> getInfo();
    matr.add (rectPtr);
    std::cout<<"Now matrix consists of "<<matr.getLayersNum()<<" layers"<< std::endl;

    std::cout << "Adding anotrer shape to matrix:" << std::endl;
    trianPtr -> getInfo();
    matr.add (trianPtr);
    std::cout<<"Now matrix still consists of "<<matr.getLayersNum()<<" layers"<< std::endl;
  }
  catch (std::exception &err){
    std::cerr << err.what() << std::endl;
  }
  return 0;
}

