#define BOOST_TEST_MAIN

#include "../common/rectangle.hpp"
#include "../common/circle.hpp"
#include "../common/composite-shape.hpp"
#include "../common/triangle.hpp"
#include "../common/polygon.hpp"
#include "../common/matrix.hpp"

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>

const double approximation = 0.001;

BOOST_AUTO_TEST_SUITE (Polygon_tests)

  BOOST_AUTO_TEST_CASE (Wrong_polygone){
    BOOST_CHECK_THROW (rusakow::Polygon ({{0.0, 0.0}, {4.0, 0.0}, {1.0, 1.0}, {0.0, 4.0}}), std::invalid_argument);
    BOOST_CHECK_THROW (rusakow::Polygon ({{0.0, 0.0}, {1.0, 1.0}, {2.0, 2.0}, {4.0, 4.0}}), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE  (Constantness_of_area_tests){
    rusakow::Polygon polyg ({{0.0, -2.0}, {2.0, 0.0}, {0.0, 2.0}, {-2.0, 0.0}});
    BOOST_CHECK_CLOSE (polyg.getArea(), 8.0, approximation);
    polyg.move ({10.0, 10.0});
    BOOST_CHECK_CLOSE (polyg.getArea(), 8.0, approximation);
    polyg.move (-10.5, -10.3);
    BOOST_CHECK_CLOSE (polyg.getArea(), 8.0, approximation);
  }

  BOOST_AUTO_TEST_CASE  (Frame_rectangle_test){
    rusakow::Polygon polyg ({{0.0, -2.0}, {2.0, 0.0}, {0.0, 2.0}, {-2.0, 0.0}});
    BOOST_CHECK_CLOSE (polyg.getFrameRect().height, 4, approximation);
    BOOST_CHECK_CLOSE (polyg.getFrameRect().width, 4, approximation);
  }

  BOOST_AUTO_TEST_CASE  (Scale_tests){
    rusakow::Polygon polyg ({{0.0, -2.0}, {2.0, 0.0}, {0.0, 2.0}, {-2.0, 0.0}});
    double area = polyg.getArea();
    rusakow::rectangle_t frameRect = polyg.getFrameRect();
    double coefficient = 3.0;
    polyg.scale(coefficient);
    BOOST_CHECK_CLOSE (polyg.getArea(), area * coefficient * coefficient, approximation);
    BOOST_CHECK_CLOSE (polyg.getFrameRect().height, frameRect.height * coefficient, approximation);
    BOOST_CHECK_CLOSE (polyg.getFrameRect().width, frameRect.width * coefficient, approximation);
    BOOST_CHECK_CLOSE (polyg.getFrameRect().pos.x, frameRect.pos.x, approximation);
    BOOST_CHECK_CLOSE (polyg.getFrameRect().pos.y, frameRect.pos.y, approximation);
  }

BOOST_AUTO_TEST_SUITE_END ()

BOOST_AUTO_TEST_SUITE (Rotation_test)

  BOOST_AUTO_TEST_CASE (Rectangle_rotation){
    rusakow::Rectangle rect ({0.0, 0.0}, 4.0, 2.0);
    rusakow::rectangle_t frameRect = rect.getFrameRect();
    double area = rect.getArea();
    rect.rotate(90.0);
    BOOST_CHECK_CLOSE (rect.getFrameRect().height, frameRect.width, approximation);
    BOOST_CHECK_CLOSE (rect.getFrameRect().width, frameRect.height, approximation);
    BOOST_CHECK_CLOSE (rect.getFrameRect().pos.x, frameRect.pos.x, approximation);
    BOOST_CHECK_CLOSE (rect.getFrameRect().pos.y, frameRect.pos.y, approximation);
    BOOST_CHECK_CLOSE (rect.getArea(), area, approximation);
  }

BOOST_AUTO_TEST_CASE (Rectangle_rotation_not90){
  rusakow::Rectangle rect ({0.0, 0.0}, 2.0, 2.0);
  rusakow::rectangle_t frameRect = rect.getFrameRect();
  double area = rect.getArea();
  rect.rotate(45.0);
  BOOST_CHECK_CLOSE (rect.getFrameRect().height, frameRect.width * std::sqrt(2.0), approximation);
  BOOST_CHECK_CLOSE (rect.getFrameRect().width, frameRect.height * std::sqrt(2.0), approximation);
  BOOST_CHECK_CLOSE (rect.getFrameRect().pos.x, frameRect.pos.x, approximation);
  BOOST_CHECK_CLOSE (rect.getFrameRect().pos.y, frameRect.pos.y, approximation);
  BOOST_CHECK_CLOSE (rect.getArea(), area, approximation);
}

BOOST_AUTO_TEST_CASE (Circle_rotation){
  rusakow::Circle circ ({0.0, 0.0}, 4.0);
  rusakow::rectangle_t frameRect = circ.getFrameRect();
  double area = circ.getArea();
  circ.rotate(90.0);
  BOOST_CHECK_CLOSE (circ.getFrameRect().height, frameRect.width, approximation);
  BOOST_CHECK_CLOSE (circ.getFrameRect().width, frameRect.height, approximation);
  BOOST_CHECK_CLOSE (circ.getFrameRect().pos.x, frameRect.pos.x, approximation);
  BOOST_CHECK_CLOSE (circ.getFrameRect().pos.y, frameRect.pos.y, approximation);
  BOOST_CHECK_CLOSE (circ.getArea(), area, approximation);
  }

  BOOST_AUTO_TEST_CASE (Triangle_rotation){
    rusakow::Triangle trian ({0.0, 0.0}, {4.0, 0.0}, {0.0, 8.0});
    rusakow::rectangle_t frameRect = trian.getFrameRect();
    double area = trian.getArea();
    trian.rotate(90.0);
    BOOST_CHECK_CLOSE (trian.getFrameRect().height, frameRect.width, approximation);
    BOOST_CHECK_CLOSE (trian.getFrameRect().width, frameRect.height, approximation);
    BOOST_CHECK_CLOSE (trian.getArea(), area, approximation);
  }

  BOOST_AUTO_TEST_CASE (Triangle_rotation_not90){
    rusakow::Triangle trian ({-2.0, 0.0}, {2.0, 0.0}, {0.0, -2.0 * std::sqrt(3)});
    rusakow::rectangle_t frameRect = trian.getFrameRect();
    double area = trian.getArea();
    trian.rotate(60.0);
    BOOST_CHECK_CLOSE (trian.getFrameRect().height, frameRect.height, approximation);
    BOOST_CHECK_CLOSE (trian.getFrameRect().width, frameRect.width, approximation);
    BOOST_CHECK_CLOSE (trian.getArea(), area, approximation);
  }

  BOOST_AUTO_TEST_CASE (Polygon_rotation){
    rusakow::Polygon polyg ({{0.0, 0.0}, {1.0, 2.0}, {5.0, 0.0}, {1.0, -2.0}});
    rusakow::rectangle_t frameRect = polyg.getFrameRect();
    double area = polyg.getArea();
    polyg.rotate(90.0);
    BOOST_CHECK_CLOSE (polyg.getFrameRect().height, frameRect.width, approximation);
    BOOST_CHECK_CLOSE (polyg.getFrameRect().width, frameRect.height, approximation);
    BOOST_CHECK_CLOSE (polyg.getArea(), area, approximation);
  }

  BOOST_AUTO_TEST_CASE (Polygon_rotation_not90){
    rusakow::Polygon polyg ({{0.0, 0.0}, {2.0, 2.0}, {4.0, 0.0}, {2.0, -2.0}});
    rusakow::rectangle_t frameRect = polyg.getFrameRect();
    double area = polyg.getArea();
    polyg.rotate(45.0);
    BOOST_CHECK_CLOSE (polyg.getFrameRect().height, frameRect.height / std::sqrt(2), approximation);
    BOOST_CHECK_CLOSE (polyg.getFrameRect().width, frameRect.width / std::sqrt(2), approximation);
    BOOST_CHECK_CLOSE (polyg.getArea(), area, approximation);
  }

  BOOST_AUTO_TEST_CASE (Composite_shape_rotation){
    rusakow::CompositeShape compSh;
    rusakow::Circle circ ({2.0, 4.0}, 1.0);
    std::shared_ptr <rusakow::Shape> circPtr = std::make_shared <rusakow::Circle> (circ);
    rusakow::Triangle trian ({0.0, 6.0}, {0.0, 8.0}, {2.0, 6.0});
    std::shared_ptr <rusakow::Shape> trianPtr = std::make_shared <rusakow::Triangle> (trian);
    rusakow::Rectangle rect ({1.0, 1.0}, 2.0, 2.0);
    std::shared_ptr <rusakow::Shape> rectPtr = std::make_shared <rusakow::Rectangle> (rect);
    compSh.addShape (circPtr);
    compSh.addShape (trianPtr);
    compSh.addShape (rectPtr);
    rusakow::rectangle_t frameRect = compSh.getFrameRect();
    double area = compSh.getArea();
    compSh.rotate(90.0);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().height, frameRect.width, approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().width, frameRect.height, approximation);
    BOOST_CHECK_CLOSE (compSh.getArea(), area, approximation);
  }

  BOOST_AUTO_TEST_CASE (Composite_shape_rotation_not90){
    rusakow::CompositeShape compSh;
    rusakow::Rectangle rect1 ({1.0, 1.0}, 2.0, 2.0);
    std::shared_ptr <rusakow::Shape> rectPtr1 = std::make_shared <rusakow::Rectangle> (rect1);
    rusakow::Rectangle rect2 ({-1.0, -1.0}, 2.0, 2.0);
    std::shared_ptr <rusakow::Shape> rectPtr2 = std::make_shared <rusakow::Rectangle> (rect2);
    compSh.addShape (rectPtr1);
    compSh.addShape (rectPtr2);
    rusakow::rectangle_t frameRect = compSh.getFrameRect();
    double area = compSh.getArea();
    compSh.rotate(45.0);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().width, 2 * std::sqrt(2.0), approximation);
    BOOST_CHECK_CLOSE (compSh.getFrameRect().height, frameRect.height * std::sqrt(2.0), approximation);
    BOOST_CHECK_CLOSE (compSh.getArea(), area, approximation);
  }

BOOST_AUTO_TEST_SUITE_END ()

BOOST_AUTO_TEST_SUITE (Matrix_tests)

  BOOST_AUTO_TEST_CASE (Composite_shape_processing1){
    rusakow::CompositeShape compSh;
    rusakow::Circle circ ({2.0, 4.0}, 1.0);
    std::shared_ptr <rusakow::Shape> circPtr = std::make_shared <rusakow::Circle> (circ);
    rusakow::Triangle trian ({0.0, 6.0}, {0.0, 8.0}, {2.0, 6.0});
    std::shared_ptr <rusakow::Shape> trianPtr = std::make_shared <rusakow::Triangle> (trian);
    rusakow::Rectangle rect ({1.0, 1.0}, 2.0, 2.0);
    std::shared_ptr <rusakow::Shape> rectPtr = std::make_shared <rusakow::Rectangle> (rect);
    compSh.addShape (circPtr);
    compSh.addShape (trianPtr);
    compSh.addShape (rectPtr);
    rusakow::Matrix matr (compSh);
    size_t layers = 1;
    BOOST_CHECK_EQUAL (matr.getLayersNum(), layers);
  }

  BOOST_AUTO_TEST_CASE (Composite_shape_processing2){
    rusakow::CompositeShape compSh;
    rusakow::Circle circ ({2.0, 2.0}, 1.0);
    std::shared_ptr <rusakow::Shape> circPtr = std::make_shared <rusakow::Circle> (circ);
    rusakow::Triangle trian ({0.0, 3.0}, {0.0, 1.0}, {2.0, 3.0});
    std::shared_ptr <rusakow::Shape> trianPtr = std::make_shared <rusakow::Triangle> (trian);
    rusakow::Rectangle rect ({1.0, 1.0}, 2.0, 2.0);
    std::shared_ptr <rusakow::Shape> rectPtr = std::make_shared <rusakow::Rectangle> (rect);
    compSh.addShape (circPtr);
    compSh.addShape (trianPtr);
    compSh.addShape (rectPtr);
    rusakow::Matrix matr (compSh);
    size_t layers = 3;
    BOOST_CHECK_EQUAL (matr.getLayersNum(), layers);
  }

  BOOST_AUTO_TEST_CASE (Single_shapes_processing){
    std::shared_ptr <rusakow::Shape> polygPtr (new rusakow::Polygon ({{-2.0, 0.0}, {0.0, -2.0}, {2.0, 0.0}, {0.0, 2.0}}));
    rusakow::Matrix matr (polygPtr);
    size_t layers = 1;
    BOOST_CHECK_EQUAL (matr.getLayersNum(), layers);
    std::shared_ptr <rusakow::Shape> circPtr (new rusakow::Circle ({2.0, 2.0}, 1.0));
    std::shared_ptr <rusakow::Shape> trianPtr (new rusakow::Triangle ({0.0, 3.0}, {0.0, 1.0}, {2.0, 3.0}));
    std::shared_ptr <rusakow::Shape> rectPtr (new rusakow::Rectangle ({10.0, 10.0}, 2.0, 2.0));
    matr.add (circPtr);
    layers = 2;
    BOOST_CHECK_EQUAL (matr.getLayersNum(), layers);
    matr.add (trianPtr);
    layers = 3;
    BOOST_CHECK_EQUAL (matr.getLayersNum(), layers);
    matr.add (rectPtr);
    BOOST_CHECK_EQUAL (matr.getLayersNum(), layers);
  }

BOOST_AUTO_TEST_SUITE_END ()












