#ifndef BASETYPES_HPP
#define BASETYPES_HPP

namespace rusakow {

  struct point_t{
    double x;
    double y;
  };

  struct rectangle_t{
    point_t pos;
    double height;
    double width;
  };

}

#endif // BASETYPES_HPP

