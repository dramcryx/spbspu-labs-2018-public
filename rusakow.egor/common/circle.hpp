#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

#include <cmath>
#include <iostream>

namespace rusakow {

  class Circle:
      public Shape{
  public:
    Circle(const rusakow::point_t center, const double rad);

    ~Circle() = default;

    double getArea() const override;

    rusakow::rectangle_t getFrameRect() const override;

    //Moves circle by its center by dx and dy units along the axis
    void move(const double dx,const double dy) override;

    //Moves circle by its center? placing it in said point
    void move(const rusakow::point_t point) override;

    void getInfo() const override;

    //Proportionally scales the circle
    void scale(const double coeff) override;

    rusakow::point_t getCenter() const override;

    void rotate (const double) override;

  private:
    rusakow::point_t center_;
    double radius_;
  };

}

#endif // CIRCLE_HPP

