#include "composite-shape.hpp"

#include <iostream>
#include <cmath>

rusakow::CompositeShape::CompositeShape():
  center_({0.0, 0.0}),
  count_(0),
  members_(nullptr)
{
}

rusakow::CompositeShape::CompositeShape(const CompositeShape & compSh):
  center_(compSh.center_),
  count_(compSh.count_),
  members_(new std::shared_ptr <rusakow::Shape> [count_])
{
  for (size_t i = 0; i < count_; i++){
    members_[i] = compSh.members_[i];
  }
}

rusakow::CompositeShape::CompositeShape(CompositeShape && compSh):
  center_(compSh.center_),
  count_(compSh.count_)
{
  members_ = std::move(compSh.members_);
  compSh.count_ = 0;
  compSh.members_.reset();
}

std::shared_ptr<rusakow::Shape> rusakow::CompositeShape::operator [] (const size_t index) const
{
  if ((index >= count_) )
  {
    throw std::out_of_range("ind is out of range");
  }
  return members_[index];
}


void rusakow::CompositeShape::addShape (const std::shared_ptr<Shape> & newShape){
  if (newShape == nullptr) {
    throw (std::invalid_argument("Shape you are trying to add does not exist!"));
  }
  else
  {
    std::unique_ptr <std::shared_ptr <Shape> []> new_array (new std::shared_ptr <Shape> [count_ + 1]);
    for (size_t i = 0; i < count_; i++){
      new_array[i] = members_[i];
    }
    new_array[count_] = newShape;
    count_++;
    members_.swap(new_array);
    double maxX = newShape->getFrameRect().pos.x + newShape->getFrameRect().width / 2.0;
    double maxY = newShape->getFrameRect().pos.y + newShape->getFrameRect().height / 2.0;
    double minX = newShape->getFrameRect().pos.x - newShape->getFrameRect().width / 2.0;
    double minY = newShape->getFrameRect().pos.y - newShape->getFrameRect().height / 2.0;
    for (size_t i = 0; i < (count_); i++){
      maxX = std::max (maxX, members_[i]->getFrameRect().pos.x + members_[i]->getFrameRect().width / 2.0);
      maxY = std::max (maxY, members_[i]->getFrameRect().pos.y + members_[i]->getFrameRect().height / 2.0);
      minX = std::min (minX, members_[i]->getFrameRect().pos.x - members_[i]->getFrameRect().width / 2.0);
      minY = std::min (minY, members_[i]->getFrameRect().pos.y - members_[i]->getFrameRect().height / 2.0);
    }
    center_.x = (minX + maxX) / 2.0;
    center_.y = (minY + maxY) / 2.0;
  }
}

double rusakow::CompositeShape::getArea() const{
  double overallArea = 0.0;
  for (size_t i = 0; i < (count_); i++){
    overallArea += members_[i]->getArea();
  }
  return (overallArea);
}

rusakow::rectangle_t rusakow::CompositeShape::getFrameRect() const{
  double maxX = members_[0]->getFrameRect().pos.x + members_[0]->getFrameRect().width / 2.0;
  double minX = members_[0]->getFrameRect().pos.x - members_[0]->getFrameRect().width / 2.0;
  double maxY = members_[0]->getFrameRect().pos.y + members_[0]->getFrameRect().height / 2.0;
  double minY = members_[0]->getFrameRect().pos.y - members_[0]->getFrameRect().height / 2.0;
  for (size_t i = 0; i < count_; i++){
    maxX = std::max(maxX, members_[i]->getFrameRect().pos.x + members_[i]->getFrameRect().width / 2.0);
    maxY = std::max(maxY, members_[i]->getFrameRect().pos.y + members_[i]->getFrameRect().height / 2.0);
    minX = std::min(minX, members_[i]->getFrameRect().pos.x - members_[i]->getFrameRect().width / 2.0);
    minY = std::min(minY, members_[i]->getFrameRect().pos.y - members_[i]->getFrameRect().height / 2.0);
  }
  return (rusakow::rectangle_t {center_, (maxY - minY), (maxX - minX)});
}

void rusakow::CompositeShape::move(double dx, double dy){
  for (size_t i = 0; i < count_; i++){
    members_[i]->move(dx, dy);
  }
  center_.x += dx;
  center_.y += dy;
}

void rusakow::CompositeShape::move (rusakow::point_t point) {
  double dx = point.x - center_.x;
  double dy = point.y - center_.y;
  for (size_t i = 0; i < count_; i++){
    members_[i]->move (dx, dy);
  }
  center_ = point;
}

void rusakow::CompositeShape::getInfo() const{
  std::cout << "This composite shape consists of " << count_ << " members" << std::endl;
  std::cout << "It is centered in (" << center_.x << ", " << center_.y << ")" << std::endl;
  std::cout << "Its overall area is " << getArea() << std::endl;
  rusakow::rectangle_t rect = getFrameRect();
  std::cout << "Its frame rectangle has height " << rect.height << " and width " << rect.width << std::endl <<std::endl;
}

void rusakow::CompositeShape::scale (const double coeff){
  if (coeff < 0){
    throw (std::invalid_argument ("Scaling coefficient must not be negative!"));
  }
  else
  {
    for (size_t i = 0; i < count_; i++){
      rusakow::point_t subCenter = members_[i]->getCenter();
      members_[i]->move ((subCenter.x - center_.x) * (coeff - 1.0), (subCenter.y - center_.y) * (coeff - 1.0));
      members_[i]->scale (coeff);
    }
  }
}

rusakow::point_t rusakow::CompositeShape::getCenter() const{
  return (center_);
}

void rusakow::CompositeShape::rotate (const double angle){
  double angCos = std::cos(angle * M_PI / 180);
  double angSin = std::sin(angle * M_PI / 180);
  for (size_t i = 0; i < count_; i++){
    rusakow::point_t subCenter = members_[i]->getCenter();
    rusakow::point_t newSubCenter;
    newSubCenter.x = center_.x + angCos * (subCenter.x - center_.x) - angSin * (subCenter.y - center_.y);
    newSubCenter.y = center_.y + angCos * (subCenter.y - center_.y) + angSin * (subCenter.x - center_.x);
    members_[i]->move(newSubCenter);
    members_[i]->rotate(angle);
  }
}

size_t rusakow::CompositeShape::getSize() const{
  return(count_);
}
