#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"

#include <memory>

namespace rusakow {

  class CompositeShape:
      public Shape{
  public:
    CompositeShape();
    std::shared_ptr<Shape> operator [] (const size_t index) const;
    CompositeShape (CompositeShape && compSh);
    CompositeShape(const CompositeShape & compSh);
    void addShape(const std::shared_ptr <Shape> & newShape);
    double getArea() const override;
    rusakow::rectangle_t  getFrameRect() const override;
    void move (double dx, double dy) override;
    void move (rusakow::point_t point) override;
    void getInfo() const override;
    void scale(const double coeff) override;
    rusakow::point_t getCenter() const override;
    void rotate (const double angle) override;
    size_t getSize() const;

  private:
    rusakow::point_t center_;
    size_t count_;
    std::unique_ptr <std::shared_ptr <Shape> []> members_;
  };

}


#endif // COMPOSITESHAPE_HPP

