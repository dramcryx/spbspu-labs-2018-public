#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

#include "shape.hpp"
#include "composite-shape.hpp"

namespace rusakow {

  class Matrix {
  public:
    Matrix();

    Matrix(const std::shared_ptr <Shape> object);

    Matrix(const CompositeShape& comp);

    std::unique_ptr<std::shared_ptr<Shape>[]> operator[](const int index) const;

    size_t getLayersNum() const noexcept;

    void add(const std::shared_ptr<Shape> shape) noexcept;

    void add(const CompositeShape& object);
  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> matrix_;
    int layers_;
    int numberOfShapes_;
    bool checkOverlap(const int index, std::shared_ptr<Shape> object) const noexcept;
  };
}

#endif
