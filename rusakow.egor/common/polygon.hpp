#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <cstdlib>
#include <memory>

#include "shape.hpp"


namespace rusakow {

  class Polygon:
      public Shape{
  public:
    Polygon (std::initializer_list <point_t> points);

    point_t operator[] (size_t index) const;

    rusakow::rectangle_t getFrameRect() const override;

    double getArea() const override;

    void move (double dx, double dy) override;

    void move (rusakow::point_t point) override;

    void getInfo() const override;

    void scale (const double coeff) override;

    rusakow::point_t getCenter() const override;

    void rotate (const double angle) override;

  private:
    std::unique_ptr <point_t[]> vertexes_;
    size_t count_;
    rusakow::point_t center_;
  };

}

#endif // POLYGON_HPP

