#include "rectangle.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>

rusakow::Rectangle::Rectangle (const rusakow::point_t center, const double height, const double width):
  center_(center),
  height_(height),
  width_(width),
  a_({(center.x - width / 2.0), (center.y + height / 2.0)}),
  b_({(center.x + width / 2.0), (center.y + height / 2.0)}),
  c_({(center.x + width / 2.0), (center.y - height / 2.0)}),
  d_({(center.x - width / 2.0), (center.y - height / 2.0)})
{
  if ((height < 0) || (width < 0)) {
    throw std::invalid_argument ("Rectangle's width and height must");
  }
}

double rusakow::Rectangle::getArea() const{
  return (height_ * width_);
}

rusakow::rectangle_t rusakow::Rectangle::getFrameRect() const{
  rusakow::rectangle_t rect;
  rect.pos = center_;
  rect.height = std::max(std::max(a_.y, b_.y), std::max(c_.y, d_.y)) - std::min(std::min(a_.y, b_.y), std::min(c_.y, d_.y));
  rect.width = std::max(std::max(a_.x, b_.x), std::max(c_.x, d_.x)) - std::min(std::min(a_.x, b_.x), std::min(c_.x, d_.x));
  return (rect);
}

void rusakow::Rectangle::getInfo() const{
  std::cout << "Rectangle's center is in (" << center_.x << "," << center_.y << ")" << std::endl;
  std::cout << "Its width is " << width_ << std::endl;
  std::cout << "Its height is " << height_ << std::endl;
  std::cout << "Its area is " << getArea() << std::endl;
  rusakow::rectangle_t rect = getFrameRect();
  std::cout << "Its frame rectangle has height " << rect.height << " and width " << rect.width << std::endl << std::endl;
}

//Moves the rectangle by dx and dy
void rusakow::Rectangle::move(const double dx, const double dy){
  center_.x += dx;
  center_.y += dy;
}

//Moves the rectangle to a certain point
void rusakow::Rectangle::move(const rusakow::point_t point){
  center_ = point;
}

void rusakow::Rectangle::scale(const double coeff){
  if (coeff < 0.0){
    throw std::invalid_argument("Scaling multiplier must not be negative!");
  }
  else
  {
    width_ *= coeff;
    height_ *= coeff;
    a_ = {(center_.x - width_ / 2.0), (center_.y + height_ / 2.0)};
    b_ = {(center_.x + width_ / 2.0), (center_.y + height_ / 2.0)};
    c_ = {(center_.x + width_ / 2.0), (center_.y - height_ / 2.0)};
    d_ = {(center_.x - width_ / 2.0), (center_.y - height_ / 2.0)};
  }
}

rusakow::point_t rusakow::Rectangle::getCenter() const{
  return (center_);
}

void rusakow::Rectangle::rotate (const double angle){
  double angSin = std::sin (angle * M_PI / 180);
  double angCos = std::cos (angle * M_PI / 180);
  a_ = {center_.x + angCos * (a_.x - center_.x) - angSin * (a_.y - center_.y), center_.y + angCos * (a_.y - center_.y) + (a_.x - center_.x) * angSin};
  b_ = {center_.x + angCos * (b_.x - center_.x) - angSin * (b_.y - center_.y), center_.y + angCos * (b_.y - center_.y) + (b_.x - center_.x) * angSin};
  c_ = {center_.x + angCos * (c_.x - center_.x) - angSin * (c_.y - center_.y), center_.y + angCos * (c_.y - center_.y) + (c_.x - center_.x) * angSin};
  d_ = {center_.x + angCos * (d_.x - center_.x) - angSin * (d_.y - center_.y), center_.y + angCos * (d_.y - center_.y) + (d_.x - center_.x) * angSin};
}

