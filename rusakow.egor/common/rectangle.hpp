#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include <iostream>

#include "shape.hpp"

namespace rusakow {

  class Rectangle:
      public Shape{
  public:
    Rectangle(const rusakow::point_t center, const double height, const double width);

    ~Rectangle() = default;

    double getArea() const override;

    rusakow::rectangle_t getFrameRect() const override;

    //Moves rectangle by its centre placing it in certain point
    void move(const rusakow::point_t point) override;

    //Moves rectangle by its center moving it by said dx and dy along the axis
    void move(const double dx, const double dy) override;

    void getInfo() const override;

    void scale(const double coeff) override;

    rusakow::point_t getCenter() const override;

    void rotate (const double angle) override;

  private:
    rusakow::point_t center_;
    double height_;
    double width_;
    //Rectangle vertexes from upper left, clockwise
    rusakow::point_t a_, b_, c_, d_;
  };

}

#endif // RECTANGLE_HPP

