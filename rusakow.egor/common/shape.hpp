#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

namespace rusakow {

  class Shape{
  public:
    virtual ~Shape() = default;

    virtual double getArea() const = 0;

    virtual rusakow::rectangle_t getFrameRect() const = 0;

    //Moves figure's center to the point
    virtual void move(const rusakow::point_t point) = 0;

    //Moves figure's center by dx and dy
    virtual void move(const double dx, const double dy) = 0;

    virtual void getInfo() const = 0;

    //Proportionally scales the figure
    virtual void scale(const double coeff) = 0;

    virtual rusakow::point_t getCenter() const = 0;

    virtual void rotate (const double angle) = 0;

  };

}

#endif // SHAPE_HPP

