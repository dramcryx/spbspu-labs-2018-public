#include "triangle.hpp"

#include <cmath>
#include <iostream>
#include <stdexcept>

rusakow::Triangle::Triangle (const rusakow::point_t a, const rusakow::point_t b, const rusakow::point_t c):
  a_(a),
  b_(b),
  c_(c)
{
  center_ = {(a_.x + b_.x + c_.x) / 3.0, (a_.y + b_.y + c_.y) / 3.0};
  if (getArea() <= 0.0) {
    throw std::invalid_argument("Triangle with such coordinates does not exist!");
  }
}

double rusakow::Triangle::getArea() const{
  double ab = sqrt((a_.x - b_.x) * (a_.x - b_.x) + (a_.y - b_.y) * (a_.y - b_.y));
  double bc = sqrt((b_.x - c_.x) * (b_.x - c_.x) + (b_.y - c_.y) * (b_.y - c_.y));
  double ca = sqrt((c_.x - a_.x) * (c_.x - a_.x) + (c_.y - a_.y) * (c_.y - a_.y));
  double p = (ab + bc + ca) / 2.0;
  return (std::sqrt(p * (p - ab) * (p - bc) * (p - ca)));
}

rusakow::rectangle_t rusakow::Triangle::getFrameRect() const{
  double rightEdge = std::max(std::max(a_.x, b_.x), c_.x);
  double leftEdge = std::min(std::min(a_.x, b_.x), c_.x);
  double upperEdge = std::max(std::max(a_.y, b_.y), c_.y);
  double lowerEdge = std::min(std::min(a_.y, b_.y), c_.y);
  return {{(rightEdge + leftEdge) / 2.0, (upperEdge + lowerEdge) / 2.0}, upperEdge - lowerEdge, rightEdge - leftEdge};
}

//Move by dx and dy
void rusakow::Triangle::move(const double dx, const double dy){
  center_.x += dx;
  center_.y += dy;
  a_.y += dy;
  a_.x += dx;
  b_.y += dy;
  b_.x += dx;
  c_.y += dy;
  c_.x += dx;
}

//Move to the point
void rusakow::Triangle::move(const rusakow::point_t point){
  double dx = point.x - center_.x;
  double dy = point.y - center_.y;
  center_ = point;
  a_.x += dx;
  a_.y += dy;
  b_.x += dx;
  b_.y += dy;
  c_.x += dx;
  c_.y += dy;
}

void rusakow::Triangle::getInfo() const{
  std::cout << "Triangle's center is in " << center_.x << " " << center_.y << std::endl;
  std::cout << "Its vertexes' coordinates are:" << std::endl;
  std::cout << a_.x << " " << a_.y << std::endl;
  std::cout << b_.x << " " << b_.y << std::endl;
  std::cout << c_.x << " " << c_.y << std::endl;
  std::cout << "Triangle's area is " << getArea() << std::endl;
  std::cout << "Its frame rectangle is centered in ";
  rectangle_t rect = getFrameRect();
  std::cout << rect.pos.x << ", " << rect.pos.y << std::endl;
  std::cout << "Its height is " << rect.height << ", and width is " << rect.width << std::endl << std::endl;
}

void rusakow::Triangle::scale(const double coeff){
  if (coeff < 0.0) {
    throw std::invalid_argument("The scale multiplier must not be negative!");
  }
  else
  {
    a_.x = center_.x + (a_.x - center_.x) * coeff;
    a_.y = center_.y + (a_.y - center_.y) * coeff;
    b_.x = center_.x + (b_.x - center_.x) * coeff;
    b_.y = center_.y + (b_.y - center_.y) * coeff;
    c_.x = center_.x + (c_.x - center_.x) * coeff;
    c_.y = center_.y + (c_.y - center_.y) * coeff;
  }
}

rusakow::point_t rusakow::Triangle::getCenter() const{
  return (center_);
}

void rusakow::Triangle::rotate (const double angle){
  double angCos = std::cos (angle * M_PI / 180);
  double angSin = std::sin (angle * M_PI / 180);
  a_ = {center_.x + angCos * (a_.x - center_.x) - angSin * (a_.y - center_.y), center_.y + angCos * (a_.y - center_.y) + (a_.x - center_.x) * angSin};
  b_ = {center_.x + angCos * (b_.x - center_.x) - angSin * (b_.y - center_.y), center_.y + angCos * (b_.y - center_.y) + (b_.x - center_.x) * angSin};
  c_ = {center_.x + angCos * (c_.x - center_.x) - angSin * (c_.y - center_.y), center_.y + angCos * (c_.y - center_.y) + (c_.x - center_.x) * angSin};
}

