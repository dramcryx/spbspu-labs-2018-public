#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

namespace rusakow {

  class Triangle:
      public Shape{
  public:
    //Constuctor
    Triangle(const rusakow::point_t a, const rusakow::point_t b, const rusakow::point_t c);

    //Destructor
    ~Triangle() = default;

    //Calculates triangle's area
    double getArea() const override;

    //Returns triangle's frame rectangle
    rusakow::rectangle_t getFrameRect() const override;

    //Moves triangle by its centre moving it by said dx and dy along the axis
    void move(const double dx, const double dy) override;

    //Moves triangle's centre in said point
    void move(const rusakow::point_t point) override;

    //Writes information on the triangle
    void getInfo() const override;

    //Proportianally scales the triangle
    void scale(const double coeff) override;

    rusakow::point_t getCenter() const override;

    void rotate (const double angle) override;

  private:
    //Triangle's centre
    rusakow::point_t center_;
    //Triangle's vertexes
    rusakow::point_t a_, b_, c_;
  };

}

#endif // TRIANGLE_HPP

