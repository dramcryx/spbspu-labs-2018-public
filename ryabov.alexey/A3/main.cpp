#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

using namespace ryabov;

int main()
{
  Shape *rect1 = nullptr;
  Shape *rect2 = nullptr;
  Shape *circ1 = nullptr;
  CompositeShape *compShape1 = nullptr;
  CompositeShape *compShape2 = nullptr;

  try {
    rectangle_t frame = { 5, 5, { 10, 10 }};
    rect1 = new Rectangle(1, 4);
    rect2 = new Rectangle(frame);
    circ1 = new Circle(3.0);
    compShape1 = new CompositeShape();
    compShape2 = new CompositeShape(rect1);

    std::cout << "Area of rect1 is " << rect1->getArea() << std::endl;
    std::cout << "Area of compShape2 is " << compShape2->getArea() << std::endl;
    std::cout << "Position of rect1 is " << rect1->getFrameRect().pos.x << " " << rect1->getFrameRect().pos.y << std::endl;
    std::cout << "Position of compShape2 is " << compShape2->getFrameRect().pos.x << " " << compShape2->getFrameRect().pos.y << std::endl;
    rect1->move(2, 3);
    compShape2->move(2, 3);
    std::cout << "Position of rect1 is " << rect1->getFrameRect().pos.x << " " << rect1->getFrameRect().pos.y << std::endl;
    std::cout << "Position of compShape2 is " << compShape2->getFrameRect().pos.x << " " << compShape2->getFrameRect().pos.y << std::endl;
    std::cout << "FrameRect of rect1 is " << rect1->getFrameRect().width << " " << rect1->getFrameRect().height << std::endl;
    std::cout << "FrameRect of compShape2 is " << compShape2->getFrameRect().width << " " << compShape2->getFrameRect().height << std::endl;

    compShape1->addShape(rect1);
    compShape1->addShape(rect2);
    compShape1->addShape(circ1);

    std::cout << "Area of rect1 + rect2 + circ1: " << rect1->getArea() + rect2->getArea() + circ1->getArea() << std::endl;
    std::cout << "Area of compShape1: " << compShape1->getArea() << std::endl;

    compShape1->removeShape(0);

    std::cout << "Area of rect2 + circ1: " << rect2->getArea() + circ1->getArea() << std::endl;
    std::cout << "Area of compShape1: " << compShape1->getArea() << std::endl;
  }
  catch (const std::invalid_argument &e) {
    std::cerr << e.what() << std::endl;
    delete rect1;
    delete rect2;
    delete circ1;
    delete compShape1;
    delete compShape2;
    return 1;
  }
  catch (const std::bad_alloc &ba) {
    std::cerr << ba.what() << std::endl;
    delete rect1;
    delete rect2;
    delete circ1;
    delete compShape1;
    delete compShape2;
    return 2;
  }

  delete rect1;
  delete rect2;
  delete circ1;
  delete compShape1;
  delete compShape2;

  return 0;
}
