#define BOOST_TEST_MODULE MyTest
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"

const double tolerance = 1e-4;

BOOST_AUTO_TEST_SUITE(CompositeShapeTest)
double widthRect = 1, heightRect = 4, radius = 5;
ryabov::rectangle_t frame;
ryabov::point_t posPrev, posCur, pos;
double areaPrev, areaCur;
ryabov::Rectangle rect(widthRect, heightRect);
ryabov::Circle circ(radius);
ryabov::CompositeShape compShape;

BOOST_AUTO_TEST_CASE(AddingShapes)
{
  areaPrev = compShape.getArea();
  BOOST_CHECK_CLOSE(areaPrev, 0, tolerance);
  compShape.addShape(&rect);
  compShape.addShape(&circ);
  areaCur = compShape.getArea();
  BOOST_CHECK_CLOSE(areaCur, circ.getArea() + rect.getArea(), tolerance);
}

BOOST_AUTO_TEST_CASE(MovingToPoint)
{
  pos = { 10, 20 };
  areaPrev = compShape.getArea();
  compShape.move(pos);
  posCur = compShape.getFrameRect().pos;
  areaCur = compShape.getArea();
  BOOST_CHECK_CLOSE(areaPrev, areaCur, tolerance);
  BOOST_CHECK((posCur.x == pos.x) && (posCur.y == pos.y));
}

BOOST_AUTO_TEST_CASE(MovingAxes)
{
  posPrev = compShape.getFrameRect().pos;
  pos = { posPrev.x + 2, posPrev.y + 3 };
  areaPrev = compShape.getArea();
  compShape.move(2, 3);
  posCur = compShape.getFrameRect().pos;
  areaCur = compShape.getArea();
  BOOST_CHECK_CLOSE(areaPrev, areaCur, tolerance);
  BOOST_CHECK((posCur.x == pos.x) && (posCur.y == pos.y));
}

BOOST_AUTO_TEST_CASE(Scaling)
{
  double factor = 3.0;
  rect.scale(factor);
  circ.scale(factor);
  compShape.scale(factor);
  areaCur = compShape.getArea();
  BOOST_CHECK_CLOSE(rect.getArea() + circ.getArea(), areaCur, tolerance);
}

BOOST_AUTO_TEST_CASE(RemovingShapes) {
  compShape.removeShape(0);
  compShape.removeShape(0);
  areaCur = compShape.getArea();
  BOOST_CHECK_CLOSE(areaCur, 0, tolerance);
}

BOOST_AUTO_TEST_CASE(InvalidArguments)
{
  compShape.addShape(&circ);
  BOOST_CHECK_THROW(ryabov::CompositeShape(-1), std::invalid_argument);
  BOOST_CHECK_THROW(ryabov::CompositeShape(nullptr, 4), std::invalid_argument);
  BOOST_CHECK_THROW(ryabov::CompositeShape(nullptr, -4), std::invalid_argument);
  BOOST_CHECK_THROW(ryabov::CompositeShape(&rect, -4), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.scale(-2), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.addShape(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(compShape.addShape(&circ), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
