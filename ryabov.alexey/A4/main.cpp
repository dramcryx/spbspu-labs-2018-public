#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "shapeSpliter.hpp"

using namespace ryabov;

int main()
{
  Shape *blueRect = nullptr;
  Shape *greenCirc = nullptr;
  Shape *redRect = nullptr;
  Shape *circ1 = nullptr;
  Shape *circ2 = nullptr;
  CompositeShape *compShape1 = nullptr;

  try {
    ShapeSpliter shapeSplinter;
    blueRect = new Rectangle(5, 2.5, {-30, 30});
    greenCirc = new Circle(20);
    redRect = new Rectangle(10, 5, {-9, -9});
    circ1 = new Circle(5, { -100, -100});
    circ2 = new Circle(20, { 10, 10});
    compShape1 = new CompositeShape(blueRect);

    compShape1->addShape(circ1);
    compShape1->addShape(circ2);

    std::cout << "blueRect frame: width: " << blueRect->getFrameRect().width
              << " height: " << blueRect->getFrameRect().height << std::endl;
    blueRect->rotate(90);
    std::cout << "blueRect frame: width: " << blueRect->getFrameRect().width
              << " height: " << blueRect->getFrameRect().height << std::endl;

    shapeSplinter.addShape(redRect);
    shapeSplinter.addShape(greenCirc);
    shapeSplinter.addShape(blueRect);
    shapeSplinter.addShape(compShape1);
    shapeSplinter.showLayers();
  }
  catch (const std::invalid_argument &e) {
    std::cerr << e.what() << std::endl;
    delete blueRect;
    delete greenCirc;
    delete redRect;
    delete circ1;
    delete circ2;
    delete compShape1;
    return 1;
  }
  catch (const std::bad_alloc &ba) {
    std::cerr << ba.what() << std::endl;
    delete blueRect;
    delete greenCirc;
    delete redRect;
    delete circ1;
    delete circ2;
    delete compShape1;
    return 2;
  }

  delete blueRect;
  delete greenCirc;
  delete redRect;
  delete circ1;
  delete circ2;
  delete compShape1;

  return 0;
}
