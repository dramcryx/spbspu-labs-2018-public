#define BOOST_TEST_MODULE MyTest
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "shapeSpliter.hpp"

const double tolerance = 1e-4;

BOOST_AUTO_TEST_SUITE(RotateTest)
ryabov::rectangle_t frame, prevFrame;

BOOST_AUTO_TEST_CASE(Rectangle)
{
  ryabov::Rectangle rect(10, 5);
  prevFrame = rect.getFrameRect();
  rect.rotate(90);
  frame = rect.getFrameRect();
  BOOST_CHECK_CLOSE(frame.width, prevFrame.height, tolerance);
  BOOST_CHECK_CLOSE(frame.height, prevFrame.width, tolerance);
  BOOST_CHECK_CLOSE(frame.pos.x, prevFrame.pos.x, tolerance);
  BOOST_CHECK_CLOSE(frame.pos.y, prevFrame.pos.y, tolerance);
}

BOOST_AUTO_TEST_CASE(Circle)
{
  ryabov::Circle circ(10);
  prevFrame = circ.getFrameRect();
  circ.rotate(90);
  frame = circ.getFrameRect();
  BOOST_CHECK_CLOSE(frame.width, prevFrame.width, tolerance);
  BOOST_CHECK_CLOSE(frame.height, prevFrame.height, tolerance);
  BOOST_CHECK_CLOSE(frame.pos.x, prevFrame.pos.x, tolerance);
  BOOST_CHECK_CLOSE(frame.pos.y, prevFrame.pos.y, tolerance);
}

BOOST_AUTO_TEST_CASE(CompositeShape)
{
  ryabov::Rectangle rect1(10, 10);
  ryabov::Rectangle rect2(10, 10, { 0.0, 10.0 });
  ryabov::CompositeShape compShape(&rect1);
  compShape.addShape(&rect2);
  prevFrame = compShape.getFrameRect();
  compShape.rotate(90);
  frame = compShape.getFrameRect();
  BOOST_CHECK_CLOSE(frame.width, prevFrame.height, tolerance);
  BOOST_CHECK_CLOSE(frame.height, prevFrame.width, tolerance);
  BOOST_CHECK_CLOSE(frame.pos.x, prevFrame.pos.x, tolerance);
  BOOST_CHECK_CLOSE(frame.pos.y, prevFrame.pos.y, tolerance);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTest)
ryabov::Rectangle rect123(10, 5, {-9, -9});
ryabov::Matrix matrix(10, 10);

BOOST_AUTO_TEST_CASE(InvalidArguments)
{
  BOOST_CHECK_THROW(ryabov::Matrix(-10, 10), std::invalid_argument);
  BOOST_CHECK_THROW(ryabov::Matrix(10, -10), std::invalid_argument);
  BOOST_CHECK_THROW(matrix.setElement(-1, 1, &rect123), std::invalid_argument);
  BOOST_CHECK_THROW(matrix.setElement(1, -1, &rect123), std::invalid_argument);
  BOOST_CHECK_THROW(matrix.setElement(1, 1, nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(matrix.resize(1, -10), std::invalid_argument);
  BOOST_CHECK_THROW(matrix.resize(-10, 1), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(ShapeSpliterTest)
ryabov::Rectangle redRect(10, 5, {-9, -9});
ryabov::Circle greenCirc(20);
ryabov::Rectangle blueRect(5, 2.5, {-30, 30});
ryabov::Circle circ1(5, { -100, -100 });
ryabov::Circle circ2(20, { 10, 10});
ryabov::CompositeShape compShape(&circ1);
ryabov::ShapeSpliter shapeSplinter;

BOOST_AUTO_TEST_CASE(AddingShapes)
{
  compShape.addShape(&circ2);
  BOOST_CHECK(shapeSplinter.getLayersCount() == 0);
  shapeSplinter.addShape(&redRect);
  BOOST_CHECK(shapeSplinter.getLayersCount() == 1);
  ryabov::Shape** layer = shapeSplinter.getLayer(0);
  BOOST_CHECK(layer[0] == &redRect);
  delete [] layer;
}

BOOST_AUTO_TEST_CASE(Layers)
{
  shapeSplinter.addShape(&greenCirc);
  BOOST_CHECK(shapeSplinter.getLayersCount() == 2);
  shapeSplinter.addShape(&blueRect);
  BOOST_CHECK(shapeSplinter.getLayersCount() == 2);
  shapeSplinter.addShape(&compShape);
  BOOST_CHECK(shapeSplinter.getLayersCount() == 3);

  ryabov::Shape** layer = shapeSplinter.getLayer(0);
  BOOST_CHECK((layer[0] == &redRect) && (layer[1] = &blueRect));
  delete [] layer;

  layer = shapeSplinter.getLayer(1);
  BOOST_CHECK(layer[0] == &greenCirc);
  delete [] layer;

  layer = shapeSplinter.getLayer(2);
  BOOST_CHECK(layer[0] == &compShape);
  delete [] layer;
}

BOOST_AUTO_TEST_CASE(InvalidArguments)
{
  BOOST_CHECK_THROW(shapeSplinter.addShape(nullptr), std::invalid_argument);
  BOOST_CHECK_THROW(shapeSplinter.addShape(&redRect), std::invalid_argument);
  BOOST_CHECK_THROW(shapeSplinter.getLayer(-1), std::invalid_argument);
  BOOST_CHECK_THROW(shapeSplinter.getLayer(100), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
