#define _USE_MATH_DEFINES
#include "composite-shape.hpp"
#include <stdexcept>
#include <cmath>

using namespace ryabov;

CompositeShape::CompositeShape(int size) :
  size_(size),
  shapesCount_(0)
{
  if (size < 1) {
    throw std::invalid_argument("Error: size must be positive.");
  } else {
    array_ = new Shape * [size];
  }
}

CompositeShape::CompositeShape(Shape *shape, int size) :
  size_(size),
  shapesCount_(0)
{
  if (size < 1) {
    throw std::invalid_argument("Error: size must be positive.");
  } else {
    if (shape) {
      array_ = new Shape * [size];
      array_[0] = shape;
      shapesCount_ += 1;
    } else {
      throw std::invalid_argument("Error: invalid pointer.");
    }
  }
}

CompositeShape::~CompositeShape()
{
  clear();
}

double CompositeShape::getArea() const
{
  double totalArea = 0;
  for (int i = 0; i < shapesCount_; i++) {
    totalArea += array_[i]->getArea();
  }
  return totalArea;
}

rectangle_t CompositeShape::getFrameRect() const
{
  rectangle_t rect = array_[0]->getFrameRect();
  double minX = rect.pos.x - rect.width / 2;
  double maxX = rect.pos.x + rect.width / 2;
  double minY = rect.pos.y - rect.height / 2;
  double maxY = rect.pos.y  + rect.height / 2;
  double totalX = rect.pos.x, totalY = rect.pos.y;

  for (int i = 1; i < shapesCount_; i++) {
    rect = array_[i]->getFrameRect();
    double left = rect.pos.x - rect.width / 2;
    double right = rect.pos.x + rect.width / 2;
    double bottom = rect.pos.y - rect.height / 2;
    double top = rect.pos.y  + rect.height / 2;

    if (left < minX) {
      minX = left;
    }
    if (right > maxX) {
      maxX = right;
    }
    if (bottom < minY) {
      minY = bottom;
    }
    if (top > maxY) {
      maxY = top;
    }
    totalX += rect.pos.x;
    totalY += rect.pos.y;
  }

  return {
    maxX - minX,
    maxY - minY,
    { totalX / shapesCount_, totalY / shapesCount_ }
  };
}

point_t CompositeShape::move(const point_t &coords)
{
  double totalX = 0, totalY = 0;
  for (int i = 0; i < shapesCount_; i++) {
    array_[i]->move(coords);
    point_t pos = array_[i]->getFrameRect().pos;
    totalX += pos.x;
    totalY += pos.y;
  }
  return { totalX / shapesCount_, totalY / shapesCount_ };
}

point_t CompositeShape::move(double dx, double dy)
{
  double totalX = 0, totalY = 0;
  for (int i = 0; i < shapesCount_; i++) {
    array_[i]->move(dx, dy);
    point_t pos = array_[i]->getFrameRect().pos;
    totalX += pos.x;
    totalY += pos.y;
  }
  return { totalX / shapesCount_, totalY / shapesCount_ };
}

void CompositeShape::scale(double factor)
{
  if (factor <= 0) {
    throw std::invalid_argument("Error: Invalid argument: factor must be > 0.");
  } else {
    for (int i = 0; i < shapesCount_; i++) {
      array_[i]->scale(factor);
    }
  }
}

void CompositeShape::addShape(Shape *shape)
{
  if (shape) {
    for (int i = 0; i < shapesCount_; i++) {
      if (shape == array_[i]) {
        throw std::invalid_argument("Error: This shape is already added.");
      }
    }

    if (shapesCount_ >= size_) {
      size_ += 10;
      Shape **newArray = new Shape * [size_];
      for (int i = 0; i < shapesCount_; i++) {
        newArray[i] = array_[i];
      }
      delete [] array_;
      newArray[shapesCount_] = shape;
      array_ = newArray;
    } else {
      array_[shapesCount_] = shape;
    }

    shapesCount_ += 1;
  } else {
    throw std::invalid_argument("Error: invalid pointer.");
  }
}

bool CompositeShape::removeShape(int inx)
{
  for (int i = 0; i < size_; i++) {
    if (i == inx) {
      for ( ; i < size_ - 1; i++) {
        array_[i] = array_[i+1];
      }
      array_[size_ - 1] = nullptr;
      shapesCount_ -= 1;

      return true;
    }
  }

  return false;
}

void CompositeShape::clear()
{
  delete [] array_;
  array_ = nullptr;
  size_ = 0;
  shapesCount_ = 0;
}

void CompositeShape::rotate(double deg)
{
  double rad = deg * (M_PI / 180);
  double centerX = getFrameRect().pos.x;
  double centerY = getFrameRect().pos.y;

  for (int i = 0; i < shapesCount_; i++) {
    double x = array_[i]->getFrameRect().pos.x;
    double y = array_[i]->getFrameRect().pos.y;

    array_[i]->move({
      (centerX + (x - centerX) * cos(rad) - (y - centerY) * sin(rad)),
      (centerY + (y - centerY) * cos(rad) + (x - centerX) * sin(rad))
    });
    array_[i]->rotate(deg);
  }
}
