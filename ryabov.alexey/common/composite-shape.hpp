#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

namespace ryabov {
  class CompositeShape : public Shape {
    public:
      CompositeShape(int size = 1);
      CompositeShape(Shape *shape, int size = 1);
      ~CompositeShape();
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      point_t move(const point_t &coords) override;
      point_t move(double dx, double dy) override;
      void scale(double factor) override;
      void rotate(double deg) override;
      void addShape(Shape *shape);
      bool removeShape(int inx);
      void clear();

    private:
      Shape **array_;
      int size_;
      int shapesCount_;
  };
}

#endif
