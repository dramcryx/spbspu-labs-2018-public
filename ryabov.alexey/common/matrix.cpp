#include "matrix.hpp"
#include <stdexcept>
#include <iostream>

using namespace ryabov;

Matrix::Matrix(int rows, int cols) :
  rows_(rows),
  cols_(cols)
{
  matrix_ = createMatrix(rows, cols);
}

Matrix::~Matrix()
{
  clearMatrix();
  rows_ = 0;
  cols_ = 0;
}

Shape* Matrix::operator()(int row, int col)
{
  if (matrix_[row][col]) {
    return matrix_[row][col];
  } else {
    return nullptr;
  }
}

void Matrix::setElement(int row, int col, Shape *shape)
{
  if (row < 0 || col < 0 || row >= rows_ || col >= cols_) {
    throw std::invalid_argument("Error: Invalid argument: row or colum out of range.");
  }
  if (shape) {
    matrix_[row][col] = shape;
  } else {
    throw std::invalid_argument("Error: Invalid argument: shape can't be nullptr");
  }
}

void Matrix::resize(int rows, int cols)
{
  Shape ***newMatrix = createMatrix(rows, cols);

  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      newMatrix[i][j] = matrix_[i][j];
    }
  }

  clearMatrix();

  matrix_ = newMatrix;
  rows_ = rows;
  cols_ = cols;
}

void Matrix::showMatrix() const
{
  for (int i = 0; i < rows_; i++) {
    for (int j = 0; j < cols_; j++) {
      std::cout << (matrix_[i][j] ? "Shape" : "-----") << " ";
    }
    std::cout << std::endl;
  }
}

void Matrix::clearMatrix()
{
  for (int i = 0; i < rows_; i++) {
    delete [] matrix_[i];
    matrix_[i] = nullptr;
  }
  delete [] matrix_;
  matrix_ = nullptr;
}

Shape*** Matrix::createMatrix(int rows, int cols)
{
  if (rows < 1 || cols < 1) {
    throw std::invalid_argument("Error: Invalid argument: rows and columns of Matrix should be > 0.");
  } else {
    Shape ***newMatrix = new Shape ** [rows];

    for (int i = 0; i < rows; i++) {
      newMatrix[i] = new Shape * [cols];  

      for (int j = 0; j < cols; j++) {
        newMatrix[i][j] = nullptr;
      }
    }

    return newMatrix;
  }
}
