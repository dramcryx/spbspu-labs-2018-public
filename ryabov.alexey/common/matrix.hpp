#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"

namespace ryabov {
  class Matrix {
    public:
      Matrix(int rows = 1, int cols = 1);
      ~Matrix();
      Shape* operator () (int row, int col);
      void setElement(int row, int col, Shape *shape);
      void resize(int rows, int cols);
      void showMatrix() const;
    private:
      Shape ***matrix_;
      int rows_, cols_;
      void clearMatrix();
      Shape*** createMatrix(int rows, int cols);
  };
}

#endif
