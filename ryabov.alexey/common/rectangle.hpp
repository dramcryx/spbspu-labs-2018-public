#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"
#include <array>

namespace ryabov {
  class Rectangle : public Shape {
    public:
      Rectangle(double width, double height, const point_t &pos = {0.0, 0.0});
      Rectangle(const rectangle_t &frame);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      point_t move(const point_t &coords) override;
      point_t move(double dx, double dy) override;
      void scale(double factor) override;
      void rotate(double deg) override;

    private:
      double width_;
      double height_;
      rectangle_t frame_;
      std::array<point_t, 4> vertices_;
      void generateVerticesCoords(double width, double height, point_t currentPos);
  };
}

#endif
