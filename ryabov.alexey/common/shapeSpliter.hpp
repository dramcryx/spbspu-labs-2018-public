#ifndef SHAPE_SPLITER_HPP
#define SHAPE_SPLITER_HPP

#include "matrix.hpp"

namespace ryabov {
  class ShapeSpliter {
    public:
      ShapeSpliter();
      void addShape(Shape *shape);
      Shape** getLayer(int num);
      int getLayersCount();
      void showLayers() const;
    private:
      Matrix layers_;
      int rows_;
      int cols_;
      bool isOverlapingShapes(Shape *shape1, Shape *shape2) const;
  };
}

#endif
