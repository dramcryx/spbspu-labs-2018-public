#include <iostream>
#include <memory>

#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"

using namespace salykin;

void printShapeInfo(const Shape & shape)
{
  std::cout << "Area = " << shape.getArea() << std::endl;
  const rectangle_t frame = shape.getFrameRect();
  std::cout << "FrameRectangle: " << std::endl;
  std::cout << "  Center: (" << frame.pos.x << ", " << frame.pos.y << ")" << std::endl;
  std::cout << "  Width = " << frame.width << std::endl;
  std::cout << "  Height = " << frame.height << std::endl << std::endl;
}

int main()
{
  try{
  std::cout << "Creating the Circle(3.33, {17.6, 10.2})..." << std::endl;
  Circle crcl(3.33, {17.6, 10.2});
  std::cout << "Circle info: " << std::endl;
  printShapeInfo(crcl);
  
  std::cout << "Creating the Rectangle(8.25, 3.14, {25.0, 25.0})..." << std::endl;
  Rectangle rctngl(8.25, 3.14, {25.0, 25.0});
  std::cout << "Rectangle info: " << std::endl;
  printShapeInfo(rctngl);

  std::cout << "Creating the CompositeShape(Circle)..." << std::endl;
  std::shared_ptr <Shape> crclPtr = std::make_shared <Circle>(crcl);
  CompositeShape compShp(crclPtr);
  std::cout << "CompositeShape size: " << compShp.getSize() << std::endl << std::endl;
  
  std::cout << "Adding the Rectangle to the CompositeShape..." << std::endl;
  std::shared_ptr <Shape> rctnglPtr = std::make_shared <Rectangle>(rctngl);
  compShp.addShape(rctnglPtr);
  std::cout << "CompositeShape size: " << compShp.getSize() << std::endl << std::endl;

  std::cout << "CompositeShape pos: {" << compShp.getPos().x
            << ", " << compShp.getPos().y << "}" << std::endl
            << "Moving the CompositeShape..." << std::endl;
  compShp.move(5.0, 10.0);
  std::cout << "CompositeShape pos: {" << compShp.getPos().x
            << ", " << compShp.getPos().y << "}" << std::endl << std::endl;

  std::cout << "Scaling the CompositeShape..." << std::endl;
  compShp.scale(2.0);
  std::cout << "CompositeShape info: " << std::endl;
  printShapeInfo(compShp);

  std::cout << "Removing the Circle from the CompositeShape..." << std::endl;
  compShp.removeShape(1);
  std::cout << "CompositeShape size: " << compShp.getSize() << std::endl << std::endl;

  std::cout << "Clearing the CompositeShape..." << std::endl;
  compShp.clear();
  std::cout << "CompositeShape size: " << compShp.getSize() << std::endl;
  } catch(std::invalid_argument & ia) {
    std::cerr << ia.what() << std::endl;
    return 1;
  } 
  return 0;
}
