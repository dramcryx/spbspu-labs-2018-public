#include <iostream>
#include <memory>

#include "rectangle.hpp"
#include "circle.hpp"
#include "matrix.hpp"

using namespace salykin;

void printShapeInfo(const Shape & shape)
{
  std::cout << "Area = " << shape.getArea() << std::endl;
  std::cout << "Angle = " << shape.getAngle() << std::endl;
  const rectangle_t frame = shape.getFrameRect();
  std::cout << "FrameRectangle: " << std::endl;
  std::cout << "  Center: (" << frame.pos.x << ", " << frame.pos.y << ")" << std::endl;
  std::cout << "  Width = " << frame.width << std::endl;
  std::cout << "  Height = " << frame.height << std::endl << std::endl;
}

int main()
{
  try
  {
    std::cout << "Creating the Circle(3.33, {60, 60})..." << std::endl;
    Circle crcl(3.33, {60, 60});
    std::cout << "Circle info: " << std::endl;
    printShapeInfo(crcl);

    std::cout << "Rotating the Circle(50 degrees)..." << std::endl;
    crcl.rotate(50);
    std::cout << "Circle info: " << std::endl;
    printShapeInfo(crcl);
  
    std::cout << "Creating the Rectangle(8.25, 3.14, {25.0, 25.0})..." << std::endl;
    Rectangle rctngl(8.25, 3.14, {25.0, 25.0});
    std::cout << "Rectangle info: " << std::endl;
    printShapeInfo(rctngl);

    std::cout << "Rotating the Rectangle(-198 degrees)..." << std::endl;
    rctngl.rotate(-198);
    std::cout << "Rectangle info: " << std::endl;
    printShapeInfo(rctngl);

    std::cout << "Creating the CompositeShape(Circle)..." << std::endl;
    std::shared_ptr <Shape> crclPtr = std::make_shared <Circle>(crcl);
    CompositeShape compShp(crclPtr);
    std::cout << "CompositeShape info: " << std::endl;
    printShapeInfo(compShp);
  
    std::cout << "Adding the Rectangle to the CompositeShape..." << std::endl;
    std::shared_ptr <Shape> rctnglPtr = std::make_shared <Rectangle>(rctngl);
    compShp.addShape(rctnglPtr);
    std::cout << "CompositeShape info: " << std::endl;
    printShapeInfo(compShp);

    std::cout << "Rotating the CompositeShape(3333 degrees)..." << std::endl;
    compShp.rotate(3333);
    std::cout << "CompositeShape info: " << std::endl;
    printShapeInfo(compShp);
  }
  catch(std::invalid_argument & ia)
  {
    std::cerr << ia.what() << std::endl;
    return 1;
  }
  catch(std::out_of_range & oor)
  {
    std::cerr << oor.what() << std::endl;
    return 2;
  }
  return 0;
}
