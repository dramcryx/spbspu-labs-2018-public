#include <stdexcept>
#include <memory>
#include <cmath>

#include "composite-shape.hpp"

salykin::CompositeShape::CompositeShape(std::shared_ptr <salykin::Shape> shape):
  list_(nullptr),
  size_(0),
  angle_(0)
{
  if (!shape)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  
  addShape(shape);
}

salykin::CompositeShape::CompositeShape(const salykin::CompositeShape & shapeCopy):
  size_(shapeCopy.size_),
  angle_(shapeCopy.angle_)
{
  std::unique_ptr<std::shared_ptr<Shape>[]> CopyList(new std::shared_ptr<Shape>[size_]);
  {
    for (unsigned int i = 0; i < size_; ++i)
    {
      CopyList[i] = shapeCopy.list_[i];
    }
    list_.swap(CopyList);
  }
}

salykin::CompositeShape::CompositeShape(salykin::CompositeShape && shapeMove):
  list_(std::move(shapeMove.list_)),
  size_(shapeMove.size_),
  angle_(shapeMove.angle_)
{
  shapeMove.list_.reset();
  shapeMove.size_ = 0;
  shapeMove.angle_ = 0;
}

salykin::CompositeShape & salykin::CompositeShape::operator=(const salykin::CompositeShape & shapeCopy)
{
  if (this == & shapeCopy)
  {
    return *this;
  }
  size_ = shapeCopy.size_;
  angle_ = shapeCopy.angle_;
  std::unique_ptr<std::shared_ptr<salykin::Shape>[]> new_list_(new std::shared_ptr<salykin::Shape>[size_]);
  for (unsigned int i = 0; i < size_; ++i)
  {
    new_list_[i] = shapeCopy.list_[i];
  }
  list_.swap(new_list_);
  
  return *this;
}

salykin::CompositeShape & salykin::CompositeShape::operator=(salykin::CompositeShape && shapeMove)
{
  if (this != & shapeMove)
  {
    list_.swap(shapeMove.list_);
    size_ = shapeMove.size_;
    shapeMove.list_.reset();
    shapeMove.size_ = 0;
    shapeMove.angle_ = 0;
  }
  return *this;
}

double salykin:: CompositeShape::getArea() const
{
  double area = 0;
  for (unsigned int i = 0; i < size_; ++i)
  {
    area += list_[i]->getArea();
  }
  return area;
}

salykin::rectangle_t salykin::CompositeShape::getFrameRect() const
{
  double width;
  double height;
  salykin::point_t center;
  if (size_ != 0)
  {
    salykin::rectangle_t frameRect = list_[0]->getFrameRect();
    double left = frameRect.pos.x - (frameRect.width / 2);
    double right = frameRect.pos.x + (frameRect.width / 2);
    double bottom = frameRect.pos.y - (frameRect.height / 2);
    double top = frameRect.pos.y + (frameRect.height / 2);
    for (unsigned int i = 1; i < size_; ++i)
    {
      frameRect = list_[i]->getFrameRect();
      double left_i = frameRect.pos.x - (frameRect.width / 2);
      double right_i = frameRect.pos.x + (frameRect.width / 2);
      double bottom_i = frameRect.pos.y - (frameRect.height / 2);
      double top_i = frameRect.pos.y + (frameRect.height / 2);

      if (left_i < left)
      {
        left = left_i;
      }
      if (right_i > right)
      {
        right = right_i;
      }
      if (bottom_i < bottom)
      {
        bottom = bottom_i;
      }
      if (top_i > top)
      {
        top = top_i;
      }
    }
    width = right - left;
    height = top - bottom;
    center = {((left + right) / 2), ((top + bottom) / 2)};
  }
  else
  {
    width = 0;
    height = 0;
    center = {0, 0};
  }
  return{width, height, center};
}

salykin::point_t salykin::CompositeShape::getPos() const
{
  double x = getFrameRect().pos.x;
  double y = getFrameRect().pos.y;
  return{x, y};
}

void salykin::CompositeShape::move(const double dx, const double dy)
{
  for (unsigned int i = 0; i < size_; ++i)
  {
    list_[i]->move(dx, dy);
  }
}

void salykin::CompositeShape::move(const salykin::point_t & newPos)
{
  const salykin::point_t curPos = getPos();
  double dx = (newPos.x - curPos.x);
  double dy = (newPos.y - curPos.y);
  move(dx, dy);
}

void salykin::CompositeShape::scale(const double multiplier)
{
  if (multiplier < 0)
  {
    throw std::invalid_argument("Invalid multiplier");
  }
  
  const salykin::point_t curPos = getPos();
  for (unsigned int i = 0; i < size_; ++i)
  {
    const salykin::point_t shapePos = list_[i]->getPos();
    list_[i]->move((multiplier - 1) * (shapePos.x - curPos.x), (multiplier - 1) * (shapePos.y - curPos.y));
    list_[i]->scale(multiplier);
  }
}

void salykin::CompositeShape::rotate(const double deg)
{
  angle_ += deg;
  if (fabs(angle_) >= 360)
  {
    angle_ = fmod(angle_, 360);
  }
  const double sn = sin(deg * M_PI / 180);
  const double csn = cos(deg * M_PI / 180);
  const salykin::point_t curPos = getPos();
  for (unsigned int i = 0; i < size_; ++i)
  {
    const salykin::point_t shapePos = list_[i]->getPos();
    list_[i]->move({(shapePos.x - curPos.x) * csn + (shapePos.y - curPos.y) * sn + curPos.x,
        (shapePos.y - curPos.y) * csn + (shapePos.x - curPos.x) * sn + curPos.y});
    list_[i]->rotate(deg);
  } 
}

double salykin::CompositeShape::getAngle() const
{
  return angle_;
}

void salykin::CompositeShape::addShape(const std::shared_ptr <salykin::Shape> newShape)
{
  if (!newShape)
  {
    throw std::invalid_argument("Invalid pointer");
  }
  
  for (unsigned int i = 0; i < size_; ++i)
  {
    if (newShape == list_[i])
    {
      throw std::invalid_argument("This shape is already a part of this CompositeShape");
    }
  }

  std::unique_ptr <std::shared_ptr <salykin::Shape>[]> newList(new std::shared_ptr <salykin::Shape>[size_ + 1]);
  for (unsigned int i = 0; i < size_; ++i)
  {
    newList[i] = list_[i];
  }
  newList[size_++] = newShape;
  list_.swap(newList);
}

std::shared_ptr <salykin::Shape> salykin::CompositeShape::getShape(unsigned int const index) const
{
  if (index >= size_)
  {
    throw std::out_of_range("Invalid index");
  }
  return list_[index];
}

void salykin::CompositeShape::removeShape(const unsigned int index)
{
  if (size_ == 0)
  {
    throw std::invalid_argument("Empty CompositeShape");
  }
  else
  {
    if ((index <= 0) || (index > size_))
    {
      throw std::invalid_argument("Invalid index");
    }
  }

  if (size_ == 1)
  {
    clear();
  }
  else
  {
    std::unique_ptr <std::shared_ptr <salykin::Shape>[]> newParts(new std::shared_ptr <salykin::Shape>[size_ - 1]);
    for (unsigned int i = 0; i < (index - 1); ++i)
    {
      newParts[i] = list_[i];
    }
    for (unsigned int i = index; i < size_; ++i)
    {
      newParts[i - 1] = list_[i];
    }
    list_.swap(newParts);
    --size_;
  }
}

void salykin::CompositeShape::clear()
{
  list_.reset();
  list_ = nullptr;
  size_ = 0;
}

unsigned int salykin::CompositeShape::getSize() const
{
  return size_;
}
