#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>

#include "composite-shape.hpp"

namespace salykin
{
  class Matrix
  {
  public:
    Matrix(const std::shared_ptr <Shape> newShape);
    Matrix(const Matrix & matrixCopy);
    Matrix(Matrix && matrixMove);
    Matrix(const CompositeShape compShp);
    ~Matrix();

    Matrix & operator=(const Matrix & matrixCopy);
    Matrix & operator=(Matrix && matrixMove);
    bool operator==(const Matrix & matrix) const;
    bool operator!=(const Matrix & matrix) const;
    std::unique_ptr <std::shared_ptr <Shape>[]> operator[](const unsigned int layerNum) const;
    
    void addShape(const std::shared_ptr <Shape> newShape);
    unsigned int getLayersNum() const;
    unsigned int getLayerSize(const unsigned int layerNum) const;
    unsigned int getMaxLayerSize() const;
  private:
    std::unique_ptr <std::shared_ptr <Shape>[]> list_;
    unsigned int layersNum_;
    unsigned int layerSize_;
    bool overlapCheck(const unsigned int num, std::shared_ptr <Shape> shape) const;
  };
}

#endif
