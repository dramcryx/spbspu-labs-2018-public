#define _USE_MATH_DEFINES
#define NDEBUG
#include "circle.hpp"
#include <iostream>
#include <cmath>
#include <assert.h>

Circle::Circle(const point_t &cr, const double rs): cr(cr), r(rs)
{
  if(r <= 0)
  {
    throw std::invalid_argument("Invalid circle parameters");
  }
}

void Circle::move(const point_t &pt)
{
  cr = pt;
}

void Circle::move(const double xx, const double yy)
{
  cr.x += xx;
  cr.y += yy;
}

double Circle::getArea() const
{
  return M_PI * r * r;
}

rectangle_t Circle::getFrameRect() const
{
  return { r * 2, r * 2, cr };
}
