#include "rectangle.hpp"
#include <iostream>
#include <assert.h>

Rectangle::Rectangle(const rectangle_t &param) : par(param)
{
  if(par.height < 0 || par.width < 0)
  {
    throw std::invalid_argument("Invalid rectangle parameters");
  }
}

void Rectangle::move(const point_t &pt)
{
  par.pos = pt;
}

void Rectangle::move(const double xx, const double yy)
{
  par.pos.x += xx;
  par.pos.y += yy;
}

double Rectangle::getArea() const
{
  return par.height * par.width;
}

rectangle_t Rectangle::getFrameRect() const
{
  return par;
}
