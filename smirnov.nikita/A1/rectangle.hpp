#include "shape.hpp"
#include "base-types.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(const rectangle_t &par);
  double getArea() const;
  rectangle_t getFrameRect() const;
  void move(const double xx, const double yy);
  void move(const point_t &pt);
private:
  rectangle_t par;
};
