#include <cmath>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <assert.h>
#include "triangle.hpp"

Triangle::Triangle(const point_t & firstPnt, const point_t & secondPnt, const point_t & thirdPnt) : firstPnt_(firstPnt), secondPnt_(secondPnt), thirdPnt_(thirdPnt)
{
  if (getArea() <= 0)
  {
    throw std::invalid_argument("Invalid triangle parameters");
  }
}

double Triangle::getArea() const
{
  double firstSide = sqrt(pow(secondPnt_.x - firstPnt_.x, 2) + pow(secondPnt_.y - firstPnt_.y, 2));
  double secondSide = sqrt(pow(thirdPnt_.x - firstPnt_.x, 2) + pow(thirdPnt_.y - firstPnt_.y, 2));
  double thirdSide = sqrt(pow(thirdPnt_.x - secondPnt_.x, 2) + pow(thirdPnt_.y - secondPnt_.y, 2));
  double halfPer = (firstSide + secondSide + thirdSide) / 2;
  return sqrt(halfPer * (halfPer - firstSide) * (halfPer - secondSide) * (halfPer - thirdSide));
}

rectangle_t Triangle::getFrameRect() const
{
  double maxY = std::max(std::max(firstPnt_.y, secondPnt_.y), thirdPnt_.y);
  double maxX = std::max(std::max(firstPnt_.x, secondPnt_.x), thirdPnt_.x);
  double minY = std::min(std::min(firstPnt_.y, secondPnt_.y), thirdPnt_.y);
  double minX = std::min(std::min(firstPnt_.x, secondPnt_.x), thirdPnt_.x);
  return { sqrt(pow(maxX - minX, 2)), sqrt(pow(maxY - minY, 2)),{ (minX + maxX) / 2, (minY + maxY) / 2 } };
}

void Triangle::move(const point_t &pt)
{
  point_t pos = { (firstPnt_.x + secondPnt_.x + thirdPnt_.x) / 3,(firstPnt_.y + secondPnt_.y + thirdPnt_.y) / 3 };
  firstPnt_.x += pt.x - pos.x;
  firstPnt_.y += pt.y - pos.y;
  secondPnt_.x += pt.x - pos.x;
  secondPnt_.y += pt.y - pos.y;
  thirdPnt_.x += pt.x - pos.x;
  thirdPnt_.y += pt.y - pos.y;
}

void Triangle::move(double xx, double yy)
{
  firstPnt_ = { firstPnt_.x + xx, firstPnt_.y+ yy };
  secondPnt_ = { secondPnt_.x + xx, secondPnt_.y + yy };
  thirdPnt_ = { thirdPnt_.x + xx, thirdPnt_.y + yy };
}
