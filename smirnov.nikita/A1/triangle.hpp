#include "shape.hpp"
#include "base-types.hpp"

class Triangle : public Shape
{
public:
  Triangle(const point_t & firstPnt, const point_t & secondPnt, const point_t & thirdPnt);
  double getArea() const;
  rectangle_t getFrameRect() const;
  void move(const point_t &pt);
  void move(const double xx, const double yy);
private:
  point_t firstPnt_;
  point_t secondPnt_;
  point_t thirdPnt_;
};
