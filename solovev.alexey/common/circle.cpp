#include <iostream>
#include <stdexcept>
#include <cmath>
#include "circle.hpp"

solovev::Circle::Circle(double rad, point_t pos):
  rad_(rad),
  center_(pos)
{
  if (rad<0.0)
  {
    throw std::invalid_argument("Radius must be>=0");
  }
}

double solovev::Circle::getArea() const noexcept
{
  return (M_PI * rad_ * rad_);
}

solovev::rectangle_t solovev::Circle::getFrameRect() const noexcept
{
  return {2.0*rad_, 2.0*rad_, center_};
}

void solovev::Circle::move (solovev::point_t pos) noexcept
{
  center_ = pos;
}

void solovev::Circle::move(double dx, double dy) noexcept
{
  center_.x += dx;
  center_.y += dy;
}

void solovev::Circle::scale(double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("Factor is less than 0");
  }
  else rad_ *= factor;
}

void solovev::Circle::rotate(double /*angle*/) noexcept
{
}

void solovev::Circle::printInfo() const noexcept
{
  std::cout<<"x: "<<center_.x<<std::endl;
  std::cout<<"y: "<<center_.y<<std::endl;
  std::cout<<"radius: "<<rad_<<std::endl;
  std::cout<<"Area: "<<getArea()<<std::endl;
}
