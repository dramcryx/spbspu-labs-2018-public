#ifndef CIRCLE_HPP_INCLUDED
#define CIRCLE_HPP_INCLUDED
#include "shape.hpp"

namespace solovev {
  class Circle:
    public Shape
  {
    public:
      Circle(double rad, point_t pos);
      double getArea() const noexcept override;
      rectangle_t getFrameRect() const noexcept override;
      virtual void move (double dx, double dy) noexcept override;
      virtual void move (point_t pos) noexcept override;
      virtual void scale (double factor) override;
      virtual void rotate (double angle) noexcept override;
      void printInfo() const noexcept override;
    private:
      double rad_;
      point_t center_;
  };
}

#endif


