#ifndef RECTANGLE_HPP_INCLUDED
#define RECTANGLE_HPP_INCLUDED
#include "shape.hpp"

namespace solovev{
  class Rectangle:
    public Shape
  {
  public:
    Rectangle(point_t pos, double width, double height);

    rectangle_t getFrameRect() const noexcept override;
    double getArea() const noexcept override;
    virtual void move (point_t pos) noexcept override;
    virtual void move (double dx, double dy) noexcept override;
    virtual void scale (double factor) override;
    virtual void rotate (double angle) noexcept override;
    void printInfo() const noexcept override;

  private:
    double width_, height_;
    point_t center_;
    point_t vertices_[4];
  };
}
#endif
