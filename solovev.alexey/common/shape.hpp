#ifndef SHAPE_HPP_INCLUDED
#define SHAPE_HPP_INCLUDED
#include "base-types.hpp"

namespace solovev {
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const noexcept = 0;
    virtual rectangle_t getFrameRect() const noexcept = 0;
    virtual void move(const point_t pos) noexcept = 0;
    virtual void move(const double dx, const double dy) noexcept = 0;
    virtual void scale(double factor) = 0;
    virtual void rotate(double angle) noexcept = 0;
    virtual void printInfo() const = 0;
  };
}
#endif
