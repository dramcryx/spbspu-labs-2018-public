#include <iostream>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace sorvenkov;

int main()
{
  std::shared_ptr<Rectangle> rectangle1 = std::make_shared<Rectangle>(Rectangle( 2,2 ,{ 3, 3}));
  std::shared_ptr<Rectangle> rectangle2 = std::make_shared<Rectangle>(Rectangle( 2,2 ,{ 3, 3}));
  std::shared_ptr<Circle> circle1 = std::make_shared<Circle>(Circle(2, {10, 10}));
  std::shared_ptr<Circle> circle2 = std::make_shared<Circle>(Circle(10, {5, 5}));
  std::shared_ptr<CompositeShape> compositeshape = std::make_shared<CompositeShape>(CompositeShape(circle2));
  compositeshape->add(rectangle1);
  Matrix ShapeMat(rectangle2);
  ShapeMat.add(circle1);
  ShapeMat.addFromCompositeShape(compositeshape, 2);
  ShapeMat.OutInf(ShapeMat);
  std::cout << "Rotate Tests: " << std::endl;
  std::cout << "size: " <<compositeshape->getSize() <<std::endl;
  std::cout << "x: " << compositeshape->getFrameRect().pos.x << "  y: " << compositeshape->getFrameRect().pos.y<< std::endl;
  compositeshape->rotate(45);
  std::cout << "size: " <<compositeshape->getSize() <<std::endl;
  std::cout << "x: " << compositeshape->getFrameRect().pos.x << "  y: " << compositeshape->getFrameRect().pos.y<< std::endl;
  std::shared_ptr<Rectangle> rectangle3 = std::make_shared<Rectangle>(Rectangle( 3,3 ,{ 3, 3}));
  compositeshape->add(rectangle3);
  compositeshape->rotate(90);
  std::cout << "size: " <<compositeshape->getSize() <<std::endl;
  std::cout << "x: " << compositeshape->getFrameRect().pos.x << "  y: " << compositeshape->getFrameRect().pos.y << std::endl;
  return 0;
}
