#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <cmath>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include <boost/test/included/unit_test.hpp>

using namespace sorvenkov;

const double accuracy = 0.01;

class BOOST_CHECK_EQUAL;
BOOST_AUTO_TEST_SUITE(TEST_MATRIX)

  BOOST_AUTO_TEST_CASE(MATRIX_LAYERS)
  {
    sorvenkov::Rectangle rect1(4, 2, {0, 0});
    sorvenkov::Rectangle rect2(2, 4, {2, 0});
    sorvenkov::Rectangle rect3(2, 2, {-2, 3});

    std::shared_ptr<Shape> rect1s = std::make_shared<sorvenkov::Rectangle>(rect1);
    std::shared_ptr<Shape> rect2s = std::make_shared<sorvenkov::Rectangle>(rect2);
    std::shared_ptr<Shape> rect3s = std::make_shared<sorvenkov::Rectangle>(rect3);

    Matrix matrix(rect1s);
    matrix.add(rect2s);
    matrix.add(rect3s);

    BOOST_CHECK_EQUAL(matrix.getLayersCount() == 2, true);
  }

  BOOST_AUTO_TEST_CASE(GET_SHAPE)
  {
    sorvenkov::Rectangle rect1(4, 2, {0, 0});
    sorvenkov::Rectangle rect2(2, 4, {2, 0});
    sorvenkov::Rectangle rect3(2, 2, {-2, 3});

    std::shared_ptr<Shape> rect1s = std::make_shared<sorvenkov::Rectangle>(rect1);
    std::shared_ptr<Shape> rect2s = std::make_shared<sorvenkov::Rectangle>(rect2);
    std::shared_ptr<Shape> rect3s = std::make_shared<sorvenkov::Rectangle>(rect3);

    Matrix matrix(rect1s);
    matrix.add(rect2s);
    matrix.add(rect3s);

    BOOST_CHECK_EQUAL(matrix.getShape({0, 0}), rect1s);
    BOOST_CHECK_EQUAL(matrix.getShape({0, 1}), rect3s);
    BOOST_CHECK_EQUAL(matrix.getShape({1, 0}), rect2s);
  }

  BOOST_AUTO_TEST_CASE(ADD_FROM_COMPOSITE_SHAPE)
  {
    sorvenkov::Rectangle rect1(4, 2, {0, 0});
    sorvenkov::Rectangle rect2(2, 4, {2, 0});
    sorvenkov::Rectangle rect3(2, 2, {-2, 3});

    std::shared_ptr<Shape> rect1s = std::make_shared<sorvenkov::Rectangle>(rect1);
    std::shared_ptr<Shape> rect2s = std::make_shared<sorvenkov::Rectangle>(rect2);
    std::shared_ptr<Shape> rect3s = std::make_shared<sorvenkov::Rectangle>(rect3);

    CompositeShape cShape(rect1s);
    cShape.add(rect2s);
    cShape.add(rect3s);

    size_t sizes = cShape.size();
    std::shared_ptr<sorvenkov::CompositeShape> c_shape_ptr = std::make_shared<sorvenkov::CompositeShape>(cShape);
    Matrix matrix(rect1s);
    matrix.addFromCompositeShape(c_shape_ptr, sizes);

    BOOST_CHECK_EQUAL(matrix.getLayersCount() == 3, true);
  }

  BOOST_AUTO_TEST_CASE(GET_NUM_LAYERS)
  {
    sorvenkov::Rectangle rect1(4, 2, {0, 0});
    sorvenkov::Rectangle rect2(2, 4, {2, 0});
    sorvenkov::Rectangle rect3(2, 2, {-2, 3});

    std::shared_ptr<Shape> rect1s = std::make_shared<sorvenkov::Rectangle>(rect1);
    std::shared_ptr<Shape> rect2s = std::make_shared<sorvenkov::Rectangle>(rect2);
    std::shared_ptr<Shape> rect3s = std::make_shared<sorvenkov::Rectangle>(rect3);

    Matrix matrix(rect1s);
    matrix.add(rect2s);
    matrix.add(rect3s);

    matrix.getLayersCount();
    BOOST_CHECK_EQUAL(matrix.getLayersCount(), 2);
  }

  BOOST_AUTO_TEST_CASE(COPY_OPERATOR)
  {
    sorvenkov::Rectangle rect1(4, 2, {0, 0});
    sorvenkov::Rectangle rect2(2, 4, {2, 0});
    sorvenkov::Rectangle rect3(2, 2, {-2, 3});

    std::shared_ptr<Shape> rect1s = std::make_shared<sorvenkov::Rectangle>(rect1);
    std::shared_ptr<Shape> rect2s = std::make_shared<sorvenkov::Rectangle>(rect2);
    std::shared_ptr<Shape> rect3s = std::make_shared<sorvenkov::Rectangle>(rect3);

    CompositeShape cShape(rect1s);
    cShape.add(rect2s);
    cShape.add(rect3s);

    size_t sizes = cShape.size();
    std::shared_ptr<sorvenkov::CompositeShape> c_shape_ptr = std::make_shared<sorvenkov::CompositeShape>(cShape);
    Matrix matrix(rect1s);
    matrix.addFromCompositeShape(c_shape_ptr, sizes);

    Matrix matrix1(rect1s);
    matrix1 = matrix;

    std::size_t q1 = 0;
    std::size_t q2 = 1;
    std::size_t q3 = 2;
    BOOST_CHECK_EQUAL(matrix1.getShape(std::make_pair(q1, q1)), rect1s);
    BOOST_CHECK_EQUAL(matrix1.getShape(std::make_pair(q3, q1)), rect2s);
    BOOST_CHECK_EQUAL(matrix1.getShape(std::make_pair(q1, q2)), rect3s);
  }

  BOOST_AUTO_TEST_CASE(MOVE_OPERATOR)
  {
    sorvenkov::Rectangle rect1(4, 2, {0, 0});
    sorvenkov::Rectangle rect2(2, 4, {2, 0});
    sorvenkov::Rectangle rect3(2, 2, {-2, 3});

    std::shared_ptr<Shape> rect1s = std::make_shared<sorvenkov::Rectangle>(rect1);
    std::shared_ptr<Shape> rect2s = std::make_shared<sorvenkov::Rectangle>(rect2);
    std::shared_ptr<Shape> rect3s = std::make_shared<sorvenkov::Rectangle>(rect3);

    CompositeShape cShape(rect1s);
    cShape.add(rect2s);
    cShape.add(rect3s);

    size_t sizes = cShape.size();
    std::shared_ptr<sorvenkov::CompositeShape> c_shape_ptr = std::make_shared<sorvenkov::CompositeShape>(cShape);
    Matrix matrix(rect1s);
    matrix.addFromCompositeShape(c_shape_ptr, sizes);

    Matrix matrix1(rect1s);
    matrix1 = std::move(matrix);

    std::size_t q1 = 0;
    std::size_t q2 = 1;
    std::size_t q3 = 2;
    BOOST_CHECK_EQUAL(matrix1.getShape(std::make_pair(q1, q1)), rect1s);
    BOOST_CHECK_EQUAL(matrix1.getShape(std::make_pair(q3, q1)), rect2s);
    BOOST_CHECK_EQUAL(matrix1.getShape(std::make_pair(q1, q2)), rect3s);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Test_rotate)
  BOOST_AUTO_TEST_CASE(Test_Rotate_CompositeShape)
  {
    sorvenkov::Circle new_circle(10,{0, 0});
    sorvenkov::Rectangle new_rectangle(20, 20,{40, 0});
    std::shared_ptr <sorvenkov::Shape> new_circlePtr = std::make_shared <sorvenkov::Circle> (new_circle);
    std::shared_ptr <sorvenkov::Shape> new_rectanglePtr = std::make_shared <sorvenkov::Rectangle> (new_rectangle);
    sorvenkov::CompositeShape new_compositeshape(new_circlePtr);
    new_compositeshape.add(new_rectanglePtr);
    new_compositeshape.rotate(90);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getFrameRect().height, 60, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getFrameRect().width, 20, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getShape(0)->getFrameRect().pos.x, 30, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getShape(0)->getFrameRect().pos.y, -30, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getShape(1)->getFrameRect().pos.x, 30, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getShape(1)->getFrameRect().pos.y, 10, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getArea(), (new_circle.getArea() + new_rectangle.getArea()), accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getAngle(), 90, accuracy);
  }

    BOOST_AUTO_TEST_CASE(Test_Rotate_Rectangle)
  {
    sorvenkov::Rectangle testRec(5.0, 6.0, {3.0, 4.0});
    double initialArea = testRec.getArea();
    sorvenkov::point_t initialCenter = testRec.getCenter();
    testRec.rotate(30);
    BOOST_CHECK_CLOSE(testRec.getArea(),initialArea,accuracy);
    BOOST_CHECK_CLOSE(testRec.getCenter().x,initialCenter.x,accuracy);
    BOOST_CHECK_CLOSE(testRec.getCenter().y,initialCenter.y,accuracy);
  }

  BOOST_AUTO_TEST_CASE(Test_Rotate_Triagle)
  {
    sorvenkov::Triangle testTri({1.0,3.0},{4.0,1.0},{6.0,4.0});
    double initialArea = testTri.getArea();
    sorvenkov::point_t initialCenter = testTri.getCenter();
    testTri.rotate(30);
    BOOST_CHECK_CLOSE(testTri.getArea(),initialArea,accuracy);
    BOOST_CHECK_CLOSE(testTri.getCenter().x,initialCenter.x,accuracy);
    BOOST_CHECK_CLOSE(testTri.getCenter().y,initialCenter.y,accuracy);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TEST_GFR)

 BOOST_AUTO_TEST_CASE(GetFrameRect_Test_compositeshape)
  {
    sorvenkov::Circle new_circle( 10,{0, 0});
    sorvenkov::Rectangle new_rectangle( 40, 20,{10, 20});
    std::shared_ptr <sorvenkov::Shape> new_circlePtr = std::make_shared <sorvenkov::Circle> (new_circle);
    std::shared_ptr <sorvenkov::Shape> new_rectanglePtr = std::make_shared <sorvenkov::Rectangle> (new_rectangle);
    sorvenkov::CompositeShape new_compositeshape(new_circlePtr);
    new_compositeshape.add(new_rectanglePtr);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getFrameRect().height, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getFrameRect().width, 40, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getFrameRect().pos.x, 10, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_compositeshape.getFrameRect().pos.y, 10, accuracy);
  }
  
 BOOST_AUTO_TEST_CASE(GetFrameRect_Test_rectangle)
  {
    sorvenkov::Rectangle new_rectangle( 20, 20,{40, 0});
    std::shared_ptr <sorvenkov::Shape> new_rectanglePtr = std::make_shared <sorvenkov::Rectangle> (new_rectangle);
    new_rectangle.rotate(45);
    BOOST_CHECK_CLOSE_FRACTION(new_rectangle.getFrameRect().height, 28.2842, accuracy);
    BOOST_CHECK_CLOSE_FRACTION(new_rectangle.getFrameRect().width, 28.2842, accuracy);
  }

  BOOST_AUTO_TEST_SUITE_END()
