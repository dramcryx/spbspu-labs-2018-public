#include "composite-shape.hpp"

#include <iostream>
#include <iomanip>
#include <memory>
#include <cmath>
#include <math.h>

sorvenkov::CompositeShape::CompositeShape(std::shared_ptr<sorvenkov::Shape> shape) :
  subpart_(nullptr),
  size_(0),
  angle_(0)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }
  add(shape);
}

sorvenkov::CompositeShape::CompositeShape(const sorvenkov::CompositeShape &copy) :
  size_(copy.size_),
  angle_(copy.angle_)
{
  std::unique_ptr<std::shared_ptr<Shape>[]> list(new std::shared_ptr<sorvenkov::Shape>[size_]);
  {
    for (size_t i = 0; i < size_; ++i) 
    {
      list[i] = copy.subpart_[i];
    }
    subpart_ = std::move(list);
  }
}

sorvenkov::CompositeShape::CompositeShape(sorvenkov::CompositeShape &&copy) noexcept:
  subpart_(std::move(copy.subpart_)),
  size_(copy.size_)
{
  copy.size_ = 0;
}


sorvenkov::CompositeShape & sorvenkov::CompositeShape::operator=(const sorvenkov::CompositeShape & copy)
{
  if (this != & copy) 
  {
    subpart_ = std::unique_ptr<std::shared_ptr<sorvenkov::Shape>[]>(new std::shared_ptr<sorvenkov::Shape>[copy.size_]);
    size_ = copy.size_;
    for (size_t i = 0; i < size_; ++i) 
    {
      subpart_[i] = copy.subpart_[i];
    }
  }
  return *this;

}

sorvenkov::CompositeShape & sorvenkov::CompositeShape::operator=(sorvenkov::CompositeShape &&copy) noexcept
{
  if (this != & copy) 
  {
    size_ = copy.size_;
    subpart_ = std::move(copy.subpart_);
    copy.size_ = 0;
  }
  return *this;

}

double sorvenkov::CompositeShape::getAngle() const noexcept
{
  return angle_;
}

std::shared_ptr<sorvenkov::Shape> & sorvenkov::CompositeShape::operator[](size_t index) const
{
  if (index >= size_) 
  {
    throw std::out_of_range("Index beyond the boundaries of the array!");
  }
  return subpart_[index];

}

void sorvenkov::CompositeShape::add(const std::shared_ptr<sorvenkov::Shape> shape)
{
  std::unique_ptr<std::shared_ptr<Shape>[]> list(new std::shared_ptr<sorvenkov::Shape>[size_ + 1]);
  for (size_t i = 0; i < size_; i++) 
  {
    list[i] = subpart_[i];
  }
  list[size_++] = shape;
  subpart_ = std::move(list);
}

std::shared_ptr < sorvenkov::Shape > sorvenkov::CompositeShape::getShape(int const size_t) const
{
  return subpart_[size_t];
}

sorvenkov::point_t sorvenkov::CompositeShape::getCenter() const
{
  return getFrameRect().pos;
}


void sorvenkov::CompositeShape::remove(size_t i)
{
  if (size_ == 0) 
  {
    throw std::invalid_argument("Composite shape is empty");
  }
  if (i >= size_) 
  {
    throw std::out_of_range("Index goes beyond the boundaries of the array!");
  }

  --size_;
  for (size_t j = i; j < size_; ++j) 
  {
    subpart_[j] = subpart_[j + 1];
  }
}

size_t sorvenkov::CompositeShape::size() const
{
  return size_;
}

double sorvenkov::CompositeShape::getArea() const
{
  double area = 0;
  for (size_t i = 0; i < size_; ++i) 
  {
    area += subpart_[i]->getArea();
  }
  return area;
}

sorvenkov::rectangle_t sorvenkov::CompositeShape::getFrameRect() const
{
  if (size_ == 0) 
  {
    return {0, 0, {0, 0}};
  }

  double max_x = subpart_[0]->getFrameRect().pos.x + subpart_[0]->getFrameRect().width / 2;
  double max_y = subpart_[0]->getFrameRect().pos.y + subpart_[0]->getFrameRect().height / 2;
  double min_x = subpart_[0]->getFrameRect().pos.x + subpart_[0]->getFrameRect().width / 2;
  double min_y = subpart_[0]->getFrameRect().pos.y - subpart_[0]->getFrameRect().height / 2;
  double width = 0, height = 0, x = 0, y = 0;

  for (size_t i = 1; i < size_; ++i) 
  {
    width = subpart_[i]->getFrameRect().width;
    height = subpart_[i]->getFrameRect().height;
    x = subpart_[i]->getFrameRect().pos.x;
    y = subpart_[i]->getFrameRect().pos.y;
    max_x = (x + width / 2 > max_x) ? (x + width / 2) : max_x;
    max_y = (y + height / 2 > max_y) ? (y + height / 2) : max_y;
    min_x = (x - width / 2 < min_x) ? (x - width / 2) : min_x;
    min_y = (y - height / 2 < min_y) ? (y - height / 2) : min_y;
  }
  return {max_x - min_x, max_y - min_y, {min_x + (max_x - min_x) / 2, min_y + (max_y - min_y) / 2}};
}

int sorvenkov::CompositeShape::getSize() const noexcept
{
  return size_;
}

void sorvenkov::CompositeShape::move(const sorvenkov::point_t &center)
{
  for (size_t i = 0; i < size_; ++i) 
  {
    subpart_[i]->move(center.x - getFrameRect().pos.x, center.y - getFrameRect().pos.y);
  }
}

void sorvenkov::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; ++i) 
  {
    subpart_[i]->move(dx, dy);
  }
}

void sorvenkov::CompositeShape::scale(double ratio)
{
  if (ratio < 0) 
  {
    throw std::invalid_argument("Scale multiplier for CompositeShape must be greater than zero");
  }
  const sorvenkov::point_t pos = getFrameRect().pos;
  for (size_t i = 0; i < size_; ++i) 
  {
    subpart_[i]->scale(ratio);
    const sorvenkov::point_t ShapePos = subpart_[i]->getFrameRect().pos;
    subpart_[i]->move({pos.x + (ShapePos.x - pos.x) * ratio, pos.y + (ShapePos.y - pos.y) * ratio});
  }
}

void sorvenkov::CompositeShape::rotate(const double a) 
{
  angle_ += a;
  if (angle_ >= 360)
  {
    angle_ = fmod(angle_, 360);
  }
  const double sine = sin(a * M_PI / 180);
  const double cosine = cos(a * M_PI / 180);
  const sorvenkov::point_t currentCentre = getFrameRect().pos;
  for (size_t i = 0; i < size_; ++i)
  {
    const sorvenkov::point_t shapeCentre = subpart_[i]->getFrameRect().pos;
    subpart_[i]->move({(shapeCentre.x - currentCentre.x) * cosine + (shapeCentre.y - currentCentre.y) * sine + currentCentre.x,
      (shapeCentre.y - currentCentre.y) * cosine + (shapeCentre.x - currentCentre.x) * sine + currentCentre.y});
    subpart_[i]->rotate(a);
  } 
}
