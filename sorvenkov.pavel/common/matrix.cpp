#include "matrix.hpp"

#include <cmath>
#include <iostream>
#include <memory>
#include <stdexcept>
#include "base-types.hpp"

using namespace sorvenkov;

sorvenkov::Matrix::Matrix(std::shared_ptr<sorvenkov::Shape> shape) :
  laycount_(0),
  shpcount(0),
  sizes_(nullptr),
  shapes_(nullptr)
{
  add(shape);
}

sorvenkov::Matrix::Matrix(const sorvenkov::Matrix &matrix) :
  laycount_(matrix.getLayersCount()),
  shpcount(matrix.getShapesCount()),
  sizes_(new size_t[matrix.getLayersCount()]),
  shapes_(new std::shared_ptr<Shape>[matrix.getShapesCount()])
{
  if (laycount_ <= 0) 
  {
    throw std::invalid_argument("Number of layers must be greater than zero");
  }

  if (shpcount <= 0) 
  {
    throw std::invalid_argument("Number of shapes must be greater than zero");
  }

  for (size_t i = 0; i < laycount_; i++) 
  {
    sizes_[i] = matrix.sizes_[i];
  }

  for (size_t i = 0; i < shpcount; i++) 
  {
    shapes_[i] = matrix.shapes_[i];
  }
}

Matrix::Matrix(Matrix &&matrix) :
  sizes_(std::move(matrix.sizes_)),
  shapes_(std::move(matrix.shapes_))
{
  matrix.laycount_ = 0;
  matrix.shpcount = 0;
}

Matrix &Matrix::operator=(const Matrix &matrix)
{
  laycount_ = matrix.laycount_;
  shpcount = matrix.shpcount;
  sizes_ = std::unique_ptr<size_t[]>(new size_t[laycount_]);
  shapes_ = std::unique_ptr<std::shared_ptr<Shape>[]>(new std::shared_ptr<Shape>[shpcount]);

  for (size_t i = 0; i < matrix.getLayersCount(); i++) 
  {
    sizes_[i] = matrix.sizes_[i];
  }

  for (size_t i = 0; i < matrix.getShapesCount(); i++) 
  {
    shapes_[i] = matrix.shapes_[i];
  }
  return *this;
}

Matrix &Matrix::operator=(Matrix &&matrix)
{
  if (this != &matrix) 
  {
    shpcount = matrix.shpcount;
    laycount_ = matrix.laycount_;
    shapes_ = std::move(matrix.shapes_);
    sizes_ = std::move(matrix.sizes_);
    matrix.laycount_ = 0;
    matrix.shpcount = 0;
  }
  return *this;

}

std::shared_ptr<Shape> Matrix::operator[](std::pair<size_t, size_t> num) const
{
  return getShape(num);
}

void Matrix::add(std::shared_ptr<Shape> shape)
{
  if (shape == nullptr) 
  {
    throw std::invalid_argument("Shape has a nullptr value");
  }

  std::unique_ptr<std::shared_ptr<Shape>[]> tempShp(new std::shared_ptr<Shape>[shpcount + 1]);
  bool pointPos = false;
  size_t bias = 0;

  for (size_t i = 0; i < laycount_; i++) 
  {
    pointPos = true;

    for (size_t j = 0; j < sizes_[i]; j++) 
    {
      rectangle_t rect1 = shapes_[bias + j]->getFrameRect();
      rectangle_t rect2 = shape->getFrameRect();
      if (fabs(rect1.pos.x - rect2.pos.x) <= rect1.width / 2 + rect2.width / 2 &&
          fabs(rect1.pos.y - rect2.pos.y) <= rect1.height / 2 + rect2.height / 2) 
      {
        pointPos = false;
      }

      tempShp[bias + j] = shapes_[bias + j];
    }

    bias += sizes_[i];

    if (pointPos) 
    {
      tempShp[bias] = shape;
      for (size_t j = bias; j < shpcount; j++) 
      {
        tempShp[j + 1] = shapes_[j];
      }
      sizes_[i]++;
      shpcount++;
      break;
    }
  }

  if (!pointPos) 
  {
    tempShp[shpcount++] = shape;

    std::unique_ptr<size_t[]> sizesTmp(new size_t[laycount_ + 1]);
    for (size_t i = 0; i < laycount_; i++) 
    {
      sizesTmp[i] = sizes_[i];
    }

    sizesTmp[laycount_++] = 1;
    sizes_.swap(sizesTmp);
  }

  shapes_.swap(tempShp);
}

void Matrix::addFromCompositeShape(const std::shared_ptr<CompositeShape> &c_shape, size_t size)
{
  if (size == 0) 
  {
    throw std::invalid_argument("A composite shape does not contain any shapes!");
  }
  for (size_t i = 0; i < size; i++) 
  {
    add((*c_shape.get())[i]);
  }
}

void Matrix::OutInf(Matrix &matrix) const
{
  std::cout << "Number of layers: " << matrix.getLayersCount() << std::endl;
  std::cout << "Number of shapes: " << matrix.getShapesCount() << std::endl;
  for (size_t i = 0; i < matrix.getLayersCount(); i++) 
  {
    std::cout << "Layer num: " << i + 1 << std::endl;
    std::cout << "Number of shapes in layer: " << matrix.getLayerSize(i) << std::endl;
  }
}

std::shared_ptr<Shape> Matrix::getShape(std::pair<size_t, size_t> num) const
{
  if (num.first > laycount_) 
  {
    throw std::invalid_argument("There is no such layer");
  }

  if (num.second > sizes_[num.first]) 
  {
    throw std::invalid_argument("There is no such shape");
  }

  size_t numShapes = 0;
  for (size_t i = 0; i < num.first; i++) 
  {
    numShapes += sizes_[i];
  }

  return shapes_[num.second + numShapes];
}

size_t Matrix::getLayersCount() const
{
  return laycount_;
}

size_t Matrix::getLayerSize(size_t n) const
{
  if (n > laycount_) 
  {
    throw std::invalid_argument("This layer does not exist no such layer!");
  }
  return sizes_[n];
}

size_t Matrix::getShapesCount() const
{
  return shpcount;
}
