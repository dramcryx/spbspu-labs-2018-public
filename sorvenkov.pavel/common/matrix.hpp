#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <cstdlib>
#include <memory>

#include "shape.hpp"
#include "base-types.hpp"
#include "composite-shape.hpp"

namespace sorvenkov
{
  class Matrix
  {
  public:
    ~Matrix() = default;
    explicit Matrix(std::shared_ptr<sorvenkov::Shape> shape);
    Matrix(const Matrix &matrix);
    Matrix(Matrix &&matrix);
    Matrix &operator=(const Matrix &matrix);
    Matrix &operator=(Matrix &&matrix);
    std::shared_ptr<Shape> operator[](std::pair<size_t, size_t> num) const;

    void add(std::shared_ptr<Shape> shape);
    void addFromCompositeShape(const std::shared_ptr<CompositeShape> &c_shape, size_t size);
    void OutInf(Matrix &matrix) const;

    std::shared_ptr<Shape> getShape(std::pair<size_t, size_t> num) const;
    size_t getLayersCount() const;
    size_t getLayerSize(size_t n) const;
    size_t getShapesCount() const;

  private:
    size_t laycount_;
    size_t shpcount;
    std::unique_ptr<size_t[]> sizes_;
    std::unique_ptr<std::shared_ptr<Shape>[]> shapes_;

  };
}

#endif
