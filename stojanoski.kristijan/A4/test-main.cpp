#define BOOST_TEST_MODULE LAB_A4
#include <boost/test/included/unit_test.hpp>
#include <cmath>
#include <stdexcept>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "shape-matrix.hpp"

const double EPSILON = 0.001;

BOOST_AUTO_TEST_SUITE(simple_shapes)
BOOST_AUTO_TEST_CASE(rectangle_move_dxy)
{
  stojanoski::Rectangle r({ -9, 27 }, 15, 29);
  const double width_before = r.getFrameRect().width;
  const double height_before = r.getFrameRect().height;
  const double area_before = r.getArea();
  r.move(6.66, 32.38);
  BOOST_CHECK_CLOSE(width_before, r.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, r.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, r.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(rectangle_move_point)
{
  stojanoski::Rectangle r({ -91, 127.4 }, 15.8, 1.29);
  const double width_before = r.getFrameRect().width;
  const double height_before = r.getFrameRect().height;
  const double area_before = r.getArea();
  r.move({ 1.81, -57.11 });
  BOOST_CHECK_CLOSE(width_before, r.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, r.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, r.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(rectangle_scale)
{
  stojanoski::Rectangle r({ 13,18 }, 8, 5);
  const double area_before = r.getArea();
  const double k = 1.8;
  r.scale(k);
  BOOST_CHECK_CLOSE(area_before * k * k, r.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(rectangle_invalid_constructor)
{
  BOOST_CHECK_THROW(stojanoski::Rectangle({ 1, 1 }, -1, 0), std::invalid_argument);
  BOOST_CHECK_THROW(stojanoski::Rectangle({ 1, 1 }, 1, -2), std::invalid_argument);
  BOOST_CHECK_THROW(stojanoski::Rectangle({ 1, 1 }, -1, 2), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(rectangle_invalid_scale)
{
  stojanoski::Rectangle r({ 1, 5 }, 10, 12);
  BOOST_CHECK_THROW(r.scale(-3), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(rectangle_rotate_const_area)
{
  stojanoski::Rectangle r({ 1, 7 }, 5, 41);
  const double area_before = r.getArea();
  r.rotate(82);
  BOOST_CHECK_CLOSE(area_before, r.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(rectangle_rotate_const_center)
{
  stojanoski::Rectangle r({ 1, 7 }, 5, 41);
  const stojanoski::point_t pos_before = r.getFrameRect().pos;
  r.rotate(-56);
  const stojanoski::point_t newPos = r.getFrameRect().pos;
  BOOST_CHECK_CLOSE(pos_before.x, newPos.x, EPSILON);
  BOOST_CHECK_CLOSE(pos_before.y, newPos.y, EPSILON);
}
BOOST_AUTO_TEST_CASE(rectangle_rotate_360_const)
{
  stojanoski::Rectangle r({ 1, 7 }, 5, 41);
  const stojanoski::rectangle_t frame_before = r.getFrameRect();
  r.rotate(360);
  stojanoski::rectangle_t newFrame = r.getFrameRect();
  BOOST_CHECK_CLOSE(frame_before.pos.x, newFrame.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.pos.y, newFrame.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.height, newFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.width, newFrame.width, EPSILON);
}
BOOST_AUTO_TEST_CASE(rectangle_rotate_90)
{
  stojanoski::Rectangle r({ 1, 7 }, 5, 41);
  const stojanoski::rectangle_t frame_before = r.getFrameRect();
  r.rotate(90);
  stojanoski::rectangle_t newFrame = r.getFrameRect();
  BOOST_CHECK_CLOSE(frame_before.height, newFrame.width, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.width, newFrame.height, EPSILON);
}
BOOST_AUTO_TEST_CASE(circle_move_dxy)
{
  stojanoski::Circle c({ -9, 27 }, 20);
  const double width_before = c.getFrameRect().width;
  const double height_before = c.getFrameRect().height;
  const double area_before = c.getArea();
  c.move(5.51, 0);
  BOOST_CHECK_CLOSE(width_before, c.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, c.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, c.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(circle_move_point)
{
  stojanoski::Circle c({ -91, 127.4 }, 2);
  const double width_before = c.getFrameRect().width;
  const double height_before = c.getFrameRect().height;
  const double area_before = c.getArea();
  c.move({ 1.81, -57.11 });
  BOOST_CHECK_CLOSE(width_before, c.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, c.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, c.getArea(), EPSILON);
}

BOOST_AUTO_TEST_CASE(circle_invalid_constructor)
{
  BOOST_CHECK_THROW(stojanoski::Circle({ 1, 1 }, -10), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(circle_invalid_scale)
{
  stojanoski::Circle c({ 1, 5 }, 5);
  BOOST_CHECK_THROW(c.scale(-2), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(triangle_move_dxy)
{
  stojanoski::Triangle t({ -9, 27 }, { 1,-1 }, { 15,81 });
  const double width_before = t.getFrameRect().width;
  const double height_before = t.getFrameRect().height;
  const double area_before = t.getArea();
  t.move(-3.316, 1.257);
  BOOST_CHECK_CLOSE(width_before, t.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, t.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, t.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(triangle_move_point)
{
  stojanoski::Triangle t({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  const double width_before = t.getFrameRect().width;
  const double height_before = t.getFrameRect().height;
  const double area_before = t.getArea();
  t.move({ 45.8, -3.38 });
  BOOST_CHECK_CLOSE(width_before, t.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, t.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, t.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(triangle_scale)
{
  stojanoski::Triangle t({ 13,18 }, { 5, 22 }, { 12, 2 });
  const double area_before = t.getArea();
  const double k = 1.82;
  t.scale(k);
  BOOST_CHECK_CLOSE(area_before * k * k, t.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(triangle_invalid_constructor)
{
  BOOST_CHECK_THROW(stojanoski::Triangle({ 1, 1 }, { 1, 1 }, { 5, 0 }), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(triangle_invalid_scale)
{
  stojanoski::Triangle t({ 1, 5 }, { 5, 1 }, { 0, 0 });
  BOOST_CHECK_THROW(t.scale(-15), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(triangle_rotate_const_area)
{
  stojanoski::Triangle t({ 1, 7 }, { 51,18 }, { 41,0 });
  const double area_before = t.getArea();
  t.rotate(182);
  BOOST_CHECK_CLOSE(area_before, t.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(triangle_rotate_const_center)
{
  stojanoski::Triangle t({ 1, 7 }, { 51,18 }, { 41,0 });
  const stojanoski::point_t pos_before = t.getFrameRect().pos;
  t.rotate(-56);
  const stojanoski::point_t newPos = t.getFrameRect().pos;
  BOOST_CHECK_CLOSE(pos_before.x, newPos.x, EPSILON);
  BOOST_CHECK_CLOSE(pos_before.y, newPos.y, EPSILON);
}
BOOST_AUTO_TEST_CASE(triangle_rotate_360_const)
{
  stojanoski::Triangle t({ 1, 7 }, { 51,18 }, { 41,0 });
  const stojanoski::rectangle_t frame_before = t.getFrameRect();
  t.rotate(360);
  stojanoski::rectangle_t newFrame = t.getFrameRect();
  BOOST_CHECK_CLOSE(frame_before.pos.x, newFrame.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.pos.y, newFrame.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.height, newFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.width, newFrame.width, EPSILON);
}
BOOST_AUTO_TEST_CASE(triangle_rotate_90)
{
  stojanoski::Triangle t({ 1, 7 }, { 51,18 }, { 41,0 });
  const stojanoski::rectangle_t frame_before = t.getFrameRect();
  t.rotate(90);
  stojanoski::rectangle_t newFrame = t.getFrameRect();
  BOOST_CHECK_CLOSE(frame_before.height, newFrame.width, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.width, newFrame.height, EPSILON);
}
BOOST_AUTO_TEST_CASE(point_rotate_around_self)
{
  stojanoski::point_t p = { 14, -39 };
  p.rotateAroundCenter(p, 140);
  BOOST_CHECK_CLOSE(p.x, 14, EPSILON);
  BOOST_CHECK_CLOSE(p.y, -39, EPSILON);
}
BOOST_AUTO_TEST_CASE(point_rotate_360)
{
  stojanoski::point_t p = { 214, -339 };
  p.rotateAroundCenter({ 15,81 }, 360);
  BOOST_CHECK_CLOSE(p.x, 214, EPSILON);
  BOOST_CHECK_CLOSE(p.y, -339, EPSILON);
}
BOOST_AUTO_TEST_CASE(point_rotate_0)
{
  stojanoski::point_t p = { 5, 41 };
  p.rotateAroundCenter({ 1,1 }, 0);
  BOOST_CHECK_CLOSE(p.x, 5, EPSILON);
  BOOST_CHECK_CLOSE(p.y, 41, EPSILON);
}
BOOST_AUTO_TEST_CASE(point_rotate_and_back)
{
  stojanoski::point_t p = { 5, 25 };
  p.rotateAroundCenter({ 1,1 }, 45);
  BOOST_CHECK(p.x < 5);
  BOOST_CHECK(p.y < 25);
  p.rotateAroundCenter({ 1,1 }, -45);
  BOOST_CHECK_CLOSE(p.x, 5, EPSILON);
  BOOST_CHECK_CLOSE(p.y, 25, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_matching_shape)
{
  stojanoski::Circle c({ 11,4 }, 8);
  stojanoski::CompositeShape cs;
  cs.addShape(c);
  BOOST_CHECK_CLOSE(cs.getArea(), c.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().height, c.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().width, c.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().pos.x, 11, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().pos.y, 4, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_1)
{
  stojanoski::Circle circ({ 61, 18 }, 7.23);
  stojanoski::Triangle triang({ -61.5, 2 }, { 71, 0 }, { 14.4, 3.14 });
  stojanoski::Rectangle rect({ 1, 5 }, 10, 12);

  stojanoski::CompositeShape compshape;
  compshape.addShape(circ);
  compshape.addShape(triang);
  compshape.addShape(rect);
  BOOST_CHECK_CLOSE(compshape.getArea(), circ.getArea() + triang.getArea() + rect.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_2)
{
  stojanoski::Circle c1({ -91, 127.4 }, 2);
  stojanoski::Triangle t1({ -9, 27 }, { 1,-1 }, { 15,81 });
  stojanoski::CompositeShape cs1;
  cs1.addShape(c1);
  cs1.addShape(t1);

  stojanoski::Rectangle r1({ 13,18 }, 8, 5);
  stojanoski::Rectangle r2({ 1, 5 }, 10, 12);
  stojanoski::Triangle t2({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  stojanoski::CompositeShape cs2;
  cs2.addShape(r1);
  cs2.addShape(r2);
  cs2.addShape(t2);

  stojanoski::CompositeShape cs3;
  cs3.addShape(cs1);
  cs3.addShape(cs2);
  BOOST_CHECK_CLOSE(cs3.getArea(), cs1.getArea() + cs2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs3.getArea(), c1.getArea() + t1.getArea() + r1.getArea()
    + r2.getArea() + t2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_3)
{
  stojanoski::Circle c({ -191, 127.4 }, 22);
  stojanoski::Rectangle r({ 13,128 }, 84, 35);
  stojanoski::Triangle t({ -11, -12.7 }, { -5, -121 }, { 48.44, 291.666 });
  stojanoski::CompositeShape cs;
  cs.addShape(c);
  cs.addShape(r);
  cs.addShape(t);
  BOOST_CHECK_CLOSE(c.getArea(), cs[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(r.getArea(), cs[1].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(t.getArea(), cs[2].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_getArea_4)
{
  stojanoski::Circle c({ -191, 127.4 }, 22);
  stojanoski::Rectangle r({ 13,128 }, 84, 35);
  stojanoski::CompositeShape cs1;
  cs1.addShape(c);
  cs1.addShape(r);

  stojanoski::Triangle t({ -11, -12.7 }, { -5, -121 }, { 48.44, 291.666 });
  stojanoski::CompositeShape cs2;
  cs2.addShape(cs1);
  cs2.addShape(t);

  BOOST_CHECK_CLOSE(cs1.getArea(), cs2[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(t.getArea(), cs2[1].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_dxy)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Triangle({ -9, 27 }, { 1,-1 }, { 15,81 }));
  cs.addShape(stojanoski::Circle({ 7,19 }, 6));
  const double width_before = cs.getFrameRect().width;
  const double height_before = cs.getFrameRect().height;
  const double area_before = cs.getArea();
  cs.move(-3.316, 1.257);
  BOOST_CHECK_CLOSE(width_before, cs.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, cs.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, cs.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_point)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Triangle({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 }));
  const double width_before = cs.getFrameRect().width;
  const double height_before = cs.getFrameRect().height;
  const double area_before = cs.getArea();
  cs.move({ 45.8, -3.38 });
  BOOST_CHECK_CLOSE(width_before, cs.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(height_before, cs.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(area_before, cs.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_assignment)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ -91, 127.4 }, 2));
  stojanoski::CompositeShape cs2;
  cs2.addShape(stojanoski::Rectangle({ 13,18 }, 8, 5));
  cs2 = cs1;
  BOOST_CHECK_CLOSE(cs1.getArea(), cs2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().height, cs2.getFrameRect().height, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().width, cs2.getFrameRect().width, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().pos.x, cs2.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().pos.y, cs2.getFrameRect().pos.y, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_self_assignment)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ -91, 127.4 }, 2));
  stojanoski::CompositeShape cs2;
  cs2.addShape(cs1[0]);
  cs2 = cs2;
  BOOST_CHECK(cs2.size() == cs1.size());
  BOOST_CHECK_CLOSE(cs1.getArea(), cs2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_multiple_assignment)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ -91, 127.4 }, 2));
  stojanoski::CompositeShape cs2;
  cs2.addShape(stojanoski::Rectangle({ -1, 5 }, 4, 8));
  stojanoski::CompositeShape cs3;
  cs3.addShape(stojanoski::Triangle({ 1,5 }, { 8,1 }, { 2,7 }));

  cs1 = cs2 = cs3;

  BOOST_CHECK(cs1.size() == 1);
  BOOST_CHECK(cs2.size() == 1);
  BOOST_CHECK(cs3.size() == 1);
  BOOST_CHECK_CLOSE(cs1.getArea(), cs2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), cs3.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_complex_assignment)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ -91, 127.4 }, 2));
  stojanoski::CompositeShape cs2;
  cs2.addShape(stojanoski::Rectangle({ -1, 5 }, 4, 8));
  stojanoski::CompositeShape cs3;
  cs3.addShape(cs1);

  cs1 = cs2 = cs3;

  BOOST_CHECK(cs1.size() == 1);
  BOOST_CHECK(cs2.size() == 1);
  BOOST_CHECK(cs3.size() == 1);
  BOOST_CHECK_CLOSE(cs1.getArea(), cs2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), cs3.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_constructor)
{
  stojanoski::Circle c({ 1,2 }, 3);
  stojanoski::CompositeShape cs1;
  cs1.addShape(c);
  stojanoski::CompositeShape cs2(std::move(cs1));
  BOOST_CHECK(cs1.size() == 0);
  BOOST_CHECK_CLOSE(cs2.getArea(), c.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_move_operator)
{
  stojanoski::Circle c({ 1,2 }, 3);
  stojanoski::Rectangle r({ 1,1 }, 4, 3);
  stojanoski::CompositeShape cs1;
  cs1.addShape(c);
  stojanoski::CompositeShape cs2;
  cs2.addShape(r);

  cs2 = std::move(cs1);

  BOOST_CHECK(cs1.size() == 0);
  BOOST_CHECK_CLOSE(cs2.getArea(), c.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_copy_shape)
{
  stojanoski::Rectangle r({ 11.89, -54.1 }, 61.1, 84.8);
  stojanoski::CompositeShape cs;
  cs.addShape(r);

  r.move(-74, 61);
  BOOST_CHECK(std::fabs(r.getFrameRect().pos.x - cs.getFrameRect().pos.x) > EPSILON);
  BOOST_CHECK(std::fabs(r.getFrameRect().pos.y - cs.getFrameRect().pos.y) > EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale)
{
  stojanoski::Rectangle r1({ 0.5, 0.5 }, 1, 1);
  stojanoski::Rectangle r2({ 3, 3 }, 2, 2);
  stojanoski::CompositeShape cs1;
  cs1.addShape(r1);
  cs1.addShape(r2);

  cs1.scale(0.5);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().pos.x, 2, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().pos.y, 2, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().width, 2, EPSILON);
  BOOST_CHECK_CLOSE(cs1.getFrameRect().height, 2, EPSILON);

  BOOST_CHECK_CLOSE(cs1[0].getFrameRect().pos.x, 1.25, EPSILON);
  BOOST_CHECK_CLOSE(cs1[0].getFrameRect().pos.y, 1.25, EPSILON);
  BOOST_CHECK_CLOSE(cs1[0].getFrameRect().width, 0.5, EPSILON);
  BOOST_CHECK_CLOSE(cs1[0].getFrameRect().height, 0.5, EPSILON);

  BOOST_CHECK_CLOSE(cs1[1].getFrameRect().pos.x, 2.5, EPSILON);
  BOOST_CHECK_CLOSE(cs1[1].getFrameRect().pos.y, 2.5, EPSILON);
  BOOST_CHECK_CLOSE(cs1[1].getFrameRect().width, 1, EPSILON);
  BOOST_CHECK_CLOSE(cs1[1].getFrameRect().height, 1, EPSILON);

}
BOOST_AUTO_TEST_CASE(compositeShape_scale_area_1)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 13,18 }, 8));
  cs.addShape(stojanoski::Triangle({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 }));
  cs.addShape(stojanoski::Triangle({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 }));
  const double area_before = cs.getArea();
  const double k = 0.77;
  cs.scale(k);
  BOOST_CHECK_CLOSE(area_before * k * k, cs.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale_area_2)
{
  stojanoski::Circle c({ 13,18 }, 8);
  stojanoski::Triangle t({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 });

  stojanoski::CompositeShape cs;
  cs.addShape(c);
  cs.addShape(t);

  const double k = 1.337;
  cs.scale(k);
  c.scale(k);
  t.scale(k);
  BOOST_CHECK_CLOSE(c.getArea() + t.getArea(), cs.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale_area_3)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ -91, 127.4 }, 2));
  cs1.addShape(stojanoski::Triangle({ -9, 27 }, { 1,-1 }, { 15,81 }));

  stojanoski::CompositeShape cs2;
  cs2.addShape(stojanoski::Rectangle({ 13,18 }, 8, 5));
  cs2.addShape(stojanoski::Rectangle({ 1, 5 }, 10, 12));
  cs2.addShape(stojanoski::Triangle({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 }));

  stojanoski::CompositeShape cs3(cs1);
  cs3.addShape(cs2);

  const double k = 2.017;
  cs1.scale(k);
  cs2.scale(k);
  cs3.scale(k);

  stojanoski::CompositeShape cs4(cs1);
  cs4.addShape(cs2);

  BOOST_CHECK_CLOSE(cs3.getArea(), cs1.getArea() + cs2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs4.getArea(), cs3.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_scale_center)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 13,18 }, 8));
  cs.addShape(stojanoski::Triangle({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 }));
  const stojanoski::point_t center_before = cs.getFrameRect().pos;
  const double k = 1.37;
  cs.scale(k);
  BOOST_CHECK_CLOSE(center_before.x, cs.getFrameRect().pos.x, EPSILON);
  BOOST_CHECK_CLOSE(center_before.y, cs.getFrameRect().pos.y, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_remove_test_1)
{
  stojanoski::Triangle t1({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 });
  stojanoski::Triangle t2({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  stojanoski::CompositeShape cs1;
  cs1.addShape(t1);
  cs1.addShape(t2);

  BOOST_CHECK_CLOSE(cs1[0].getArea(), t1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1[1].getArea(), t2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), t1.getArea() + t2.getArea(), EPSILON);

  cs1.removeShape(1);

  BOOST_CHECK(cs1.size() == 1);
  BOOST_CHECK_CLOSE(cs1[0].getArea(), t1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), t1.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_remove_test_2)
{
  stojanoski::Triangle t1({ -14.21, 8.99 }, { 121.12, 66.55 }, { 1.23, -3.21 });
  stojanoski::Triangle t2({ -11, 2.7 }, { -5, -21 }, { 8.44, 91.666 });
  stojanoski::CompositeShape cs1;
  cs1.addShape(t1);
  cs1.addShape(t2);

  BOOST_CHECK_CLOSE(cs1[0].getArea(), t1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1[1].getArea(), t2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), t1.getArea() + t2.getArea(), EPSILON);

  cs1.removeShape(0);

  BOOST_CHECK(cs1.size() == 1);
  BOOST_CHECK_CLOSE(cs1[0].getArea(), t2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs1.getArea(), t2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_invalid_scale_1)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 1, 2 }, 3));
  BOOST_CHECK_THROW(cs.scale(-1), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(compositeShape_invalid_scale_2)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 1, 2 }, 3));
  BOOST_CHECK_THROW(cs.scale(-2), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(compositeShape_operator_out_of_range)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 1,2 }, 3));
  BOOST_CHECK_THROW(cs[5], std::out_of_range);
}
BOOST_AUTO_TEST_CASE(compositeShape_removeShape_out_of_range)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 1,2 }, 3));
  BOOST_CHECK_THROW(cs.removeShape(3), std::out_of_range);
}
BOOST_AUTO_TEST_CASE(compositeShape_empty_getArea)
{
  stojanoski::CompositeShape cs;
  BOOST_CHECK_CLOSE(cs.getArea(), 0, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_empty_getFrameRect)
{
  stojanoski::CompositeShape cs;
  BOOST_CHECK_CLOSE(cs.getFrameRect().height, 0, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().width, 0, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().pos.x, 0, EPSILON);
  BOOST_CHECK_CLOSE(cs.getFrameRect().pos.y, 0, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_copy_constructor)
{
  stojanoski::CompositeShape cs1;
  cs1.addShape(stojanoski::Circle({ 1,2 }, 3));
  cs1.addShape(stojanoski::Rectangle({ 4,5 }, 6, 7));

  stojanoski::CompositeShape cs2(cs1);

  BOOST_CHECK(cs2.size() == cs1.size());
  BOOST_CHECK_CLOSE(cs2[0].getArea(), cs1[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(cs2[1].getArea(), cs1[1].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_const_copy_1)
{
  stojanoski::Circle c({ 5,10 }, 15);
  const double initialArea = c.getArea();

  stojanoski::CompositeShape cs;
  cs.addShape(c);

  cs.scale(3);

  BOOST_CHECK_CLOSE(c.getArea(), initialArea, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_const_copy_2)
{
  stojanoski::Circle c({ 5,10 }, 15);
  const double initialArea = c.getArea();

  stojanoski::CompositeShape cs;
  cs.addShape(c);

  cs[0].scale(3);

  BOOST_CHECK_CLOSE(c.getArea(), initialArea, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_rotate_const_area)
{
  stojanoski::Triangle t({ 1, 7 }, { 51,18 }, { 41,0 });
  stojanoski::Circle c({ 5,10 }, 15);
  stojanoski::Rectangle r({ 15,20 }, 3, 4);

  stojanoski::CompositeShape cs;
  cs.addShape(t);
  cs.addShape(c);
  cs.addShape(r);

  const double area_before = cs.getArea();
  cs.rotate(182);
  BOOST_CHECK_CLOSE(area_before, cs.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_rotate_360_const)
{
  stojanoski::Triangle t({ 1, 7 }, { 51,18 }, { 41,0 });
  stojanoski::Circle c({ 5,10 }, 15);
  stojanoski::Rectangle r({ 15,20 }, 3, 4);

  stojanoski::CompositeShape cs;
  cs.addShape(t);
  cs.addShape(c);
  cs.addShape(r);

  const stojanoski::rectangle_t frame_before = cs.getFrameRect();
  cs.rotate(360);
  stojanoski::rectangle_t newFrame = cs.getFrameRect();

  BOOST_CHECK_CLOSE(frame_before.pos.x, newFrame.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.pos.y, newFrame.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.height, newFrame.height, EPSILON);
  BOOST_CHECK_CLOSE(frame_before.width, newFrame.width, EPSILON);
}
BOOST_AUTO_TEST_CASE(compositeShape_equals_singleShape)
{
  stojanoski::Triangle t({ 1, 7 }, { 51,18 }, { 41,0 });

  stojanoski::CompositeShape cs;
  cs.addShape(t);

  t.rotate(115);
  stojanoski::rectangle_t frameT = t.getFrameRect();
  cs.rotate(115);
  stojanoski::rectangle_t frameCs = cs.getFrameRect();

  BOOST_CHECK_CLOSE(t.getArea(), cs.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(frameT.pos.x, frameCs.pos.x, EPSILON);
  BOOST_CHECK_CLOSE(frameT.pos.y, frameCs.pos.y, EPSILON);
  BOOST_CHECK_CLOSE(frameT.height, frameCs.height, EPSILON);
  BOOST_CHECK_CLOSE(frameT.width, frameCs.width, EPSILON);
}

BOOST_AUTO_TEST_CASE(shapeMatrix_addShape)
{
  stojanoski::ShapeMatrix sm;
  BOOST_CHECK(sm.size() == 0);
  BOOST_CHECK(sm.isBalanced() == true);
  sm.addShape(stojanoski::Circle({ 2,2 }, 4));
  BOOST_CHECK(sm.size() == 1);
  BOOST_CHECK(sm[0].size() == 1);
  BOOST_CHECK(sm.isBalanced() == true);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_matching_shape)
{
  stojanoski::Circle c({ 11,4 }, 8);
  stojanoski::ShapeMatrix sm;
  sm.addShape(c);
  BOOST_CHECK_CLOSE(sm[0][0].getArea(), c.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_removeShape)
{
  stojanoski::Circle c({ 11,4 }, 8);
  stojanoski::ShapeMatrix sm;
  sm.addShape(c);
  sm.removeShape(0, 0);
  BOOST_CHECK(sm.size() == 0);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_layers)
{
  stojanoski::Circle c1({ 1,1 }, 5);
  stojanoski::Circle c2({ 2,2 }, 10); // ^overlaps c1
  stojanoski::Circle c3({ 15,15 }, 1); // ^no overlap

  stojanoski::ShapeMatrix sm;
  sm.addShape(c2);
  sm.addShape(c1);
  sm.addShape(c3);
  sm.addShape(c3);
  sm.addShape(c2);

  BOOST_CHECK(sm.size() == 3);
  BOOST_CHECK(sm[0].size() == 2);
  BOOST_CHECK(sm[1].size() == 2);
  BOOST_CHECK(sm[2].size() == 1);
  BOOST_CHECK_CLOSE(sm[0][0].getArea(), c2.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm[0][1].getArea(), c3.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm[1][0].getArea(), c1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm[1][1].getArea(), c3.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm[2][0].getArea(), c2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_removeLayer)
{
  stojanoski::Circle c1({ 1,1 }, 5);
  stojanoski::Circle c2({ 2,2 }, 10); // ^overlaps c1
  stojanoski::Circle c3({ 15,15 }, 1); // ^no overlap

  stojanoski::ShapeMatrix sm;
  sm.addShape(c2);
  sm.addShape(c1);
  sm.addShape(c3);
  sm.addShape(c3);
  sm.addShape(c2);

  sm.removeShape(0, 0);
  BOOST_CHECK(sm[0].size() == 1);

  sm.removeShape(0, 0);
  BOOST_CHECK(sm.size() == 2);

  BOOST_CHECK(sm[0].size() == 2);
  BOOST_CHECK(sm[1].size() == 1);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_balance)
{
  stojanoski::Circle c1({ 1,1 }, 5);
  stojanoski::Circle c2({ 2,2 }, 10); // ^overlaps c1
  stojanoski::Circle c3({ 15,15 }, 1); // ^no overlap

  stojanoski::ShapeMatrix sm;
  sm.addShape(c2);
  sm.addShape(c1);
  sm.addShape(c3);
  sm.addShape(c3);
  sm.addShape(c2);

  sm.removeShape(0, 0);
  BOOST_CHECK(sm[0].size() == 1);
  BOOST_CHECK_CLOSE(sm[0][0].getArea(), c3.getArea(), EPSILON);
  BOOST_CHECK(sm.isBalanced() == false);

  sm.balance();
  BOOST_CHECK(sm.isBalanced() == true);
  BOOST_CHECK(sm.size() == 2);
  BOOST_CHECK(sm[0].size() == 2);
  BOOST_CHECK_CLOSE(sm[0][1].getArea(), c1.getArea(), EPSILON);
  BOOST_CHECK(sm[1].size() == 2);
  BOOST_CHECK_CLOSE(sm[1][1].getArea(), c2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_assignment)
{
  stojanoski::Circle c1({ 1,1 }, 14);
  stojanoski::Circle c2({ 4,4 }, 10);
  stojanoski::ShapeMatrix sm1;
  sm1.addShape(c1);
  sm1.addShape(c2);

  stojanoski::ShapeMatrix sm2;
  sm2 = sm1;

  BOOST_CHECK(sm1.size() == sm2.size());
  BOOST_CHECK_CLOSE(sm1[0][0].getArea(), sm2[0][0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm1[1][0].getArea(), sm2[1][0].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(unbalancedShapeMatrix_assignment)
{
  stojanoski::Circle c1({ 1,1 }, 5);
  stojanoski::Circle c2({ 2,2 }, 10); // ^overlaps c1
  stojanoski::Circle c3({ 15,15 }, 1); // ^no overlap
  stojanoski::ShapeMatrix sm;
  sm.addShape(c2);
  sm.addShape(c1);
  sm.addShape(c3);

  sm.removeShape(0, 0);
  BOOST_CHECK(sm.size() == 2);
  BOOST_CHECK(sm.isBalanced() == false);

  stojanoski::ShapeMatrix sm2;
  sm2 = sm;
  BOOST_CHECK(sm2.size() == 2);
  BOOST_CHECK(sm2.isBalanced() == false);
  BOOST_CHECK_CLOSE(sm2[0][0].getArea(), sm[0][0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm2[1][0].getArea(), sm[1][0].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_self_assignment)
{
  stojanoski::Circle c1({ 1,1 }, 14);
  stojanoski::Circle c2({ 4,4 }, 10);
  stojanoski::ShapeMatrix sm1;
  sm1.addShape(c1);
  sm1.addShape(c2);

  sm1 = sm1;

  BOOST_CHECK(sm1.size() == 2);
  BOOST_CHECK_CLOSE(sm1[0][0].getArea(), c1.getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm1[1][0].getArea(), c2.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_multiple_assignment)
{
  stojanoski::Circle c1({ 1,1 }, 14);
  stojanoski::ShapeMatrix sm1;
  stojanoski::ShapeMatrix sm2;
  stojanoski::ShapeMatrix sm3;
  sm3.addShape(c1);

  sm1 = sm2 = sm3;

  BOOST_CHECK(sm1.size() == 1);
  BOOST_CHECK(sm2.size() == 1);
  BOOST_CHECK_CLOSE(sm1[0][0].getArea(), sm3[0][0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm2[0][0].getArea(), sm3[0][0].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_copy_constructor)
{
  stojanoski::Circle c1({ 1,1 }, 14);
  stojanoski::ShapeMatrix sm1;
  sm1.addShape(c1);
  stojanoski::ShapeMatrix sm2(sm1);
  BOOST_CHECK(sm2.size() == 1);
  BOOST_CHECK_CLOSE(sm2[0][0].getArea(), c1.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_move_assignment)
{
  stojanoski::Circle c1({ 1,1 }, 14);
  stojanoski::ShapeMatrix sm1;
  sm1.addShape(c1);
  stojanoski::ShapeMatrix sm2;
  sm2 = std::move(sm1);

  BOOST_CHECK(sm1.size() == 0);
  BOOST_CHECK(sm2.size() == 1);
  BOOST_CHECK_CLOSE(sm2[0][0].getArea(), c1.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_move_constructor)
{
  stojanoski::Circle c1({ 1,1 }, 14);
  stojanoski::ShapeMatrix sm1;
  sm1.addShape(c1);
  stojanoski::ShapeMatrix sm2(std::move(sm1));

  BOOST_CHECK(sm1.size() == 0);
  BOOST_CHECK(sm2.size() == 1);
  BOOST_CHECK_CLOSE(sm2[0][0].getArea(), c1.getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_operator_out_of_range)
{
  stojanoski::ShapeMatrix sm;
  sm.addShape(stojanoski::Circle({ 1,2 }, 3));
  BOOST_CHECK_THROW(sm[5][10], std::out_of_range);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_removeShape_out_of_range)
{
  stojanoski::ShapeMatrix sm;
  BOOST_CHECK_THROW(sm.removeShape(0, 0), std::out_of_range);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_intensive_crashtest)
{
  stojanoski::ShapeMatrix sm;
  for (int t = 0; t < 100; t++)
  {
    int add = std::rand() % 10;
    for (int a = 0; a < add; a++)
    {
      double radius = static_cast<double>(std::fabs((std::rand() % 30)) + 1);
      assert(radius > 0);

      stojanoski::Circle c({ static_cast<double>(std::rand() % 100), static_cast<double>(std::rand() % 100) }, radius);
      sm.addShape(c);
    }
    int rem = std::rand() % 5;
    for (int r = 0; r < rem; r++)
    {
      if (sm.size() > 0)
      {
        size_t i = std::rand() % sm.size();
        size_t j = std::rand() % sm[i].size();
        sm.removeShape(i, j);

        int balance = std::rand() % 50;
        if (balance % 3 == 0)
        {
          sm.balance();
        }
      }
    }
    for (size_t i = 0; i < sm.size(); i++)
    {
      for (size_t j = 0; j < sm[i].size(); j++)
      {
        double a = sm[i][j].getArea();
        assert(a > 0);
      }
    }

  }
}
BOOST_AUTO_TEST_CASE(shapeMatrix_compositeShape_decomposition_1)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 4,6 }, 4));
  cs.addShape(stojanoski::Rectangle({ 5,8 }, 4, 6));
  cs.addShape(stojanoski::Circle({ 15,15 }, 1));

  stojanoski::ShapeMatrix sm;
  sm.addCompositeShapeElements(cs);

  BOOST_CHECK_CLOSE(sm[0][0].getArea(), cs[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm[0][1].getArea(), cs[2].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm[1][0].getArea(), cs[1].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_compositeShape_decomposition_2)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Circle({ 0,0 }, 1));
  cs.addShape(stojanoski::Circle({ 0,0 }, 1));
  cs.addShape(stojanoski::Circle({ 5,5 }, 2));
  cs.addShape(stojanoski::Circle({ 5,5 }, 2));
  cs.addShape(stojanoski::Circle({ 10,10 }, 3));
  cs.addShape(stojanoski::Circle({ 10,10 }, 3));

  stojanoski::ShapeMatrix sm;
  sm.addCompositeShapeElements(cs);

  BOOST_CHECK(sm.size() == 2);
  BOOST_CHECK(sm[0].size() == 3);
  BOOST_CHECK(sm[1].size() == 3);

  BOOST_CHECK_CLOSE(sm[0][0].getArea(), cs[0].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm[0][1].getArea(), cs[2].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm[0][2].getArea(), cs[4].getArea(), EPSILON);

  BOOST_CHECK_CLOSE(sm[1][0].getArea(), cs[1].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm[1][1].getArea(), cs[3].getArea(), EPSILON);
  BOOST_CHECK_CLOSE(sm[1][2].getArea(), cs[5].getArea(), EPSILON);
}
BOOST_AUTO_TEST_CASE(shapeMatrix_compositeShape_decomposition_rotation)
{
  stojanoski::CompositeShape cs;
  cs.addShape(stojanoski::Rectangle({ 0,0 }, 6, 10));
  cs.addShape(stojanoski::Rectangle({ 8,0 }, 4, 8));

  stojanoski::ShapeMatrix sm;
  sm.addCompositeShapeElements(cs);
  BOOST_CHECK(sm.size() == 1);

  cs[0].rotate(90);
  cs[1].rotate(90);

  stojanoski::ShapeMatrix sm2;
  sm2.addCompositeShapeElements(cs);
  BOOST_CHECK(sm2.size() == 2);
}
BOOST_AUTO_TEST_SUITE_END()
