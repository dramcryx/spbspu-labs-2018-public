#include <cmath>
#include <algorithm>
#include "base-types.hpp"

using namespace stojanoski;

void point_t::rotateAroundCenter(const point_t &center, double degrees)
{
  double angle = (degrees * M_PI) / 180;

  double cosA = std::cos(angle);
  double sinA = std::sin(angle);

  double tx = x - center.x;
  double ty = y - center.y;

  x = tx*cosA - ty*sinA + center.x;
  y = tx*sinA + ty*cosA + center.y;
}
