#ifndef BASETYPES_H
#define BASETYPES_H

namespace stojanoski
{
  struct point_t
  {
    double x;
    double y;
    void rotateAroundCenter(const point_t &center, double degrees);
  };
  struct rectangle_t
  {
    point_t pos;
    double width;
    double height;
  };
}
#endif
