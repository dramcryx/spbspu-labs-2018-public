﻿#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace std;
using namespace subtselnaya;

void printResults(const Shape & s)
{
  cout << "  Area is " << s.getArea() << endl;
  rectangle_t r = s.getFrameRect();
  cout << "  Frame rectangle has width - " << r.width << ", height - " << r.height << endl;
  cout << "    Center is in (" << r.pos.x << ", " << r.pos.y << ')' << endl;
}

void printResults(const CompositeShape & s)
{
  cout <<"  CompositeShape.size = "<< s.size() << endl;
  cout <<"  CompositeShape.angle = "<< s.angle() << endl;
  cout << "  Area is " << s.getArea() << endl;
  rectangle_t r = s.getFrameRect();
  cout << "  Frame rectangle has width - " << r.width << ", height - " << r.height << endl;
  cout << "    Center is in (" << r.pos.x << ", " << r.pos.y << ')' << endl;
}

int main(){

  Rectangle rect(6.9, 7.3, {8.5, 2.5});
  cout << "Rectangle has width - 6.9, height - 7.3" << std::endl;
  cout << "  Center is in (8.5, 2.5)" << std::endl;
  printResults(rect);

  cout << "Moving rectangle to point (60.3, 30.8)" << endl;
  rect.move({60.3, 30.8});

  cout << "Moving rectangle with dx = 10.0 dy = 9.6" << endl;
  rect.move(10.0, 9.6);

  cout << "Scaling rectangle with 3.8" << endl;
  rect.scale(3.8);

  cout << endl;

  Circle circ({10.6, 20.2}, 5.0);
  cout << "Circle has radius - 5.0" << std::endl;
  cout << "  Center is in (10.6, 20.2)" << std::endl;
  printResults(circ);

  cout << "Moving circle to point (40.0, 30.0) " << endl;
  circ.move({40.0, 30.0});

  cout << "Moving circle with dx = 7.4 dy = 10.7 " << endl;
  circ.move(7.4, 10.7);

  cout << "Scaling circle with 3.8" << endl;
  circ.scale(3.8);

  cout << endl;

  cout << "Creating CompositeShape" << endl;
  
  rect = {1.0, 1.0, {0.0, 0.0}};
  circ = {{3.0, 3.0}, 1.0};

  CompositeShape composhape;
  CompositeShape::ptr_type rectPtr = make_shared < Rectangle > (rect);
  CompositeShape::ptr_type circPtr = make_shared < Circle > (circ);

  cout << "Adding rectangle" << endl;
  composhape.addShape(rectPtr);
  printResults(composhape);

  cout << "Adding circle" << endl;
  composhape.addShape(circPtr);
  printResults(composhape);

  cout << "Scaling CompositeShapee with 2.0" << endl;
  composhape.scale(2.0);
  cout << "New parameters of CompositeShape:" << endl;
  printResults(composhape);


  std::cout << "Moving CompositeShape to point (-3.0 , -3.0)" << endl;
  composhape.move({-3.0, -3.0});
  printResults(composhape);

  cout << "Moving CompositeShape with dx = 3.0 dy = 3.0" << endl;
  composhape.move(3.0, 3.0);
  printResults(composhape);

  cout << "Removing first shape" << endl;
  composhape.removeShape(1);
  printResults(composhape);

  circ = {{20.0, 0.0}, 10.0};
  cout << "Сirlce before rotation:" << endl;
  printResults(circ);
  circ.rotate(90);
  cout << "Сirlce after rotation (90 degrees):" << endl;
  printResults(circ);

  rect = {40.0, 20.0, {0.0, 0.0}};
  cout << "Rectangle before rotation:" << endl;
  printResults(rect);
  rect.rotate(90);
  cout << "Rectangle after rotation (90 degrees):" << endl;
  printResults(rect);

  circPtr = make_shared < Circle > (circ);
  rectPtr = make_shared < Rectangle > (rect);

  composhape.addShape(circPtr);
  cout << "Composite Shape = Circle:" << endl;
  printResults(composhape);

  composhape.addShape(rectPtr);
  cout << "Composite Shape = Circle + Rectangle:" << endl;
  printResults(composhape);

  composhape.rotate(90);
  cout << "Composite Shape after rotation (90 degrees):" << endl;
  printResults(composhape);

  Circle circM1 ({ -10.0, 0.0 }, 10.0);
  Circle circM2 ({ 40.0, 30.0 }, 20.0);
  Rectangle rectM1 (20.0, 40.0, {20.0, 30.0});
  Rectangle rectM2 (20.0, 40.0, {30.0, 0.0});

  Matrix::ptr_type circPtrM1 = make_shared < Circle > (circM1);
  Matrix::ptr_type circPtrM2 = make_shared < Circle > (circM2);
  Matrix::ptr_type rectPtrM1 = make_shared < Rectangle > (rectM1);
  Matrix::ptr_type rectPtrM2 = make_shared < Rectangle > (rectM2);

  Matrix matrix(circPtrM1);

  matrix.addShape(rectPtrM1);
  matrix.addShape(rectPtrM2);
  matrix.addShape(circPtrM2);

  unique_ptr < Matrix::ptr_type[] > layer0 = matrix[0];
  unique_ptr < Matrix::ptr_type[] > layer1 = matrix[1];
  unique_ptr < Matrix::ptr_type[] > layer2 = matrix[2];

  cout << "Matrix" << endl;

  if (layer0[0] == circPtrM1)
  {
    cout << "Layer 0, element 0 - circle 1" << endl;
  }
      if (layer0[1] == rectPtrM1)
      {
        cout << "Layer 0, element 1 - rectangle 1" << endl;
      }
      if (layer1[0] == rectPtrM2)
      {
        cout << "Layer 1, element 0 - rectangle 2" << endl;
      }
      if (layer1[1] == nullptr)
      {
        cout << "Layer 1, element 1 - nullptr" << endl;
      }
      if (layer2[0] == circPtrM2)
      {
        cout << "Layer 2, element 0 - circle 2" << endl;
      }
      if (layer2[1] == nullptr)
      {
        cout << "Layer 2, element 1 - nullptr" << endl;
      }
  return 0;
}
