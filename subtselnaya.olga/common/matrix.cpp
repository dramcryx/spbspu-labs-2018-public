#include <stdexcept>
#include <iostream>
#include <memory>
#include "matrix.hpp"

using namespace subtselnaya;

Matrix::Matrix(const ptr_type & newShape):
  shapes_(nullptr),
  layersNumber_(0u),
  layerSize_(0u)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Invalid pointer!");
  }
  addShape(newShape);
}

Matrix::Matrix(const Matrix & matrix):
  shapes_(new ptr_type[matrix.layersNumber_ * matrix.layerSize_]),
  layersNumber_(matrix.layersNumber_),
  layerSize_(matrix.layerSize_)
{
  for (size_t i = 0u; i < matrix.layersNumber_ * matrix.layerSize_; ++i)
  {
    shapes_[i] = matrix.shapes_[i];
  }
}

Matrix::Matrix(Matrix && matrix):
  shapes_(nullptr),
  layersNumber_(matrix.layersNumber_),
  layerSize_(matrix.layerSize_)
{
  shapes_.swap(matrix.shapes_);
  matrix.layersNumber_ = 0u;
  matrix.layerSize_ = 0u;
}

Matrix::~Matrix()
{
  shapes_.reset();
  shapes_ = nullptr;
  layersNumber_ = 0u;
  layerSize_ = 0u;
}

Matrix & Matrix::operator =(const Matrix & matrix)
{
  if (this != & matrix)
  {
    layersNumber_ = matrix.layersNumber_;
    layerSize_ = matrix.layerSize_;
    std::unique_ptr < ptr_type[] > new_shapes(new ptr_type[matrix.layersNumber_ * matrix.layerSize_]);
    for (size_t i = 0u; i < matrix.layersNumber_ * matrix.layerSize_; ++i)
    {
      new_shapes[i] = matrix.shapes_[i];
    }
    shapes_.swap(new_shapes);
  }
  return * this;
}

Matrix & Matrix::operator =(Matrix && matrix)
{
  if (this != & matrix)
  {
    layersNumber_ = matrix.layersNumber_;
    layerSize_ = matrix.layerSize_;
    shapes_.reset();
    shapes_.swap(matrix.shapes_);
    matrix.layersNumber_ = 0u;
    matrix.layerSize_ = 0u;
  }
  return *this;
}

bool Matrix::operator ==(const Matrix & matrix) const
{
  if ((this -> layersNumber_ != matrix.layersNumber_) || (this -> layerSize_ != matrix.layerSize_))
  {
    return false;
  }
  for (size_t i = 0u; i < layersNumber_ * layerSize_; ++i)
    {
      if (this -> shapes_[i] != matrix.shapes_[i])
      {
        return false;
      }
    }
  return true;
}

bool Matrix::operator !=(const Matrix & matrix) const
{
  if ((this -> layersNumber_ != matrix.layersNumber_) || (this -> layerSize_ != matrix.layerSize_))
  {
    return true;
  }
  for (size_t i = 0u; i < layersNumber_ * layerSize_; ++i)
    {
      if (this -> shapes_[i] != matrix.shapes_[i])
      {
        return true;
      }
    }
  return false;
}

std::unique_ptr < Matrix::ptr_type[] > Matrix::operator [](const size_t layerIndex) const
{
  if (layersNumber_ == 0)
  {
    throw std::out_of_range("Matrix is empty!");
  }
  if (layerIndex > layersNumber_ - 1)
  {
    throw std::invalid_argument("Invalid layer index!");
  }
  std::unique_ptr < ptr_type[] > layer(new ptr_type[layerSize_]);
  for (size_t i = 0u; i < layerSize_; ++i)
  {
    layer[i] = shapes_[layerIndex * layerSize_ + i];
  }
  return layer;
}

size_t Matrix::getLayersNumber() const noexcept
{
  return layersNumber_;
}

size_t Matrix::getLayerSize() const noexcept
{
  return layerSize_;
}

void Matrix::addShape(const Matrix::ptr_type & newShape)
{
  if (layersNumber_ == 0)
  {
    ++layersNumber_;
    ++layerSize_;
    std::unique_ptr < ptr_type[] > new_shapes(new ptr_type[layersNumber_ * layerSize_]);
    shapes_.swap(new_shapes);
    shapes_[0] = newShape;
  }
  else
  {
    bool addedShape = false;

    for (size_t i = 0u; !addedShape; ++i)
    {
      for (size_t j = 0u; j < layerSize_; ++j)
      {
        if (shapes_[i * layerSize_ + j] == nullptr)
        {
          shapes_[i * layerSize_ + j] = newShape;
          addedShape = true;
          break;
        }
        else
        {
          if (checkOverlapping(i * layerSize_ + j, newShape))
          {
            break;
          }
        }
        if (j == (layerSize_ - 1))
        {
          ++layerSize_;
          std::unique_ptr < ptr_type[] > new_shapes(new ptr_type[layersNumber_ * layerSize_]);
          for (size_t n = 0u; n < layersNumber_; ++n)
          {
            for (size_t m = 0u; m < layerSize_ - 1; ++m)
            {
              new_shapes[n * layerSize_ + m] = shapes_[n * (layerSize_ - 1) + m];
            }
            new_shapes[(n + 1) * layerSize_ - 1] = nullptr;
          }
          new_shapes[(i + 1) * layerSize_ - 1] = newShape;
          shapes_.swap(new_shapes);
          addedShape = true;
          break;
        }
      }
      if ((i == (layersNumber_ - 1)) && !(addedShape))
      {
        ++layersNumber_;
        std::unique_ptr < ptr_type[] > new_shapes(new ptr_type[layersNumber_ * layerSize_]);
        for (size_t n = 0u; n < (layersNumber_ - 1) * layerSize_; ++n)
        {
          new_shapes[n] = shapes_[n];
        }
        for (size_t n = (layersNumber_ - 1) * layerSize_; n < layersNumber_ * layerSize_; ++n)
        {
          new_shapes[n] = nullptr;
        }
        new_shapes[(layersNumber_ - 1) * layerSize_] = newShape;
        shapes_.swap(new_shapes);
        addedShape = true;
      }
    }
  }
}

bool Matrix::checkOverlapping(const size_t index, ptr_type newShape) const noexcept
{
  rectangle_t newShapeRect = newShape -> getFrameRect();
  rectangle_t matrixShapeRect = shapes_[index] -> getFrameRect();

  point_t newShapePoints[4] =
  {
    { newShapeRect.pos.x - newShapeRect.width / 2.0,
      newShapeRect.pos.y + newShapeRect.height / 2.0 },
    { newShapeRect.pos.x + newShapeRect.width / 2.0,
      newShapeRect.pos.y + newShapeRect.height / 2.0 },
    { newShapeRect.pos.x + newShapeRect.width / 2.0,
      newShapeRect.pos.y - newShapeRect.height / 2.0 },
    { newShapeRect.pos.x - newShapeRect.width / 2.0,
      newShapeRect.pos.y - newShapeRect.height / 2.0 },
  };

  point_t matrixShapePoints[4] =
  {
    { matrixShapeRect.pos.x - matrixShapeRect.width / 2.0,
      matrixShapeRect.pos.y + matrixShapeRect.height / 2.0 },
    { matrixShapeRect.pos.x + matrixShapeRect.width / 2.0,
      matrixShapeRect.pos.y + matrixShapeRect.height / 2.0 },
    { matrixShapeRect.pos.x + matrixShapeRect.width / 2.0,
      matrixShapeRect.pos.y - matrixShapeRect.height / 2.0 },
    { matrixShapeRect.pos.x - matrixShapeRect.width / 2.0,
      matrixShapeRect.pos.y - matrixShapeRect.height / 2.0 },
  };

  for (int i = 0; i < 4; ++i)
  {
    if (((newShapePoints[i].x >= matrixShapePoints[0].x) && (newShapePoints[i].x <= matrixShapePoints[2].x)
      && (newShapePoints[i].y >= matrixShapePoints[3].y) && (newShapePoints[i].y <= matrixShapePoints[1].y))
      || ((matrixShapePoints[i].x >= newShapePoints[0].x) && (matrixShapePoints[i].x <= newShapePoints[2].x)
       && (matrixShapePoints[i].y >= newShapePoints[3].y) && (matrixShapePoints[i].y <= newShapePoints[1].y)))
    {
      return true;
    }
  }
  return false;
}
