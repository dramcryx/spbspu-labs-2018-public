#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "shape.hpp"

namespace subtselnaya
{
  class Matrix {
  public:
    using ptr_type = std::shared_ptr < Shape >;

    Matrix(const ptr_type & newShape);
    Matrix(const Matrix & matrix);
    Matrix(Matrix && matrix);
    ~Matrix();

    Matrix & operator =(const Matrix & matrix);
    Matrix & operator =(Matrix && matrix);
    bool operator ==(const Matrix & matrix) const;
    bool operator !=(const Matrix & matrix) const;
    std::unique_ptr < ptr_type[] > operator [](const size_t layerIndex) const;

    size_t getLayersNumber() const noexcept;
    size_t getLayerSize() const noexcept;
    void addShape(const ptr_type & newShape);

  private:
    std::unique_ptr < ptr_type[] > shapes_;
    size_t layersNumber_;
    size_t layerSize_;

    bool checkOverlapping(const size_t index, ptr_type newShape) const noexcept;
  };
}

#endif
