#include "rectangle.hpp"
#include <stdexcept>

Rectangle::Rectangle(const point_t &centre, const double width, const double height) :
  centre_(centre),
  width_(width),
  height_(height)
{
  if (width_ <= 0.0 || height_ <= 0.0)
  {
    throw std::invalid_argument("Init. error: incorrect height or/and width or/and center");
  }
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  rectangle_t rect;
  rect.width = width_;
  rect.height = height_;
  rect.pos = centre_;
  return rect;
}

void Rectangle::move(const point_t &point)
{
  centre_ = point;
}

void Rectangle::move(const double x_offset, const double y_offset)
{
  centre_.x += x_offset;
  centre_.y += y_offset;
}
