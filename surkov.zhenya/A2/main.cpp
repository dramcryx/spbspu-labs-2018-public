#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include <iostream>

using namespace surkov;

int main()
{
  Rectangle rect({ 5, 5 }, 10, 4);
  std::cout << "Rectangle\n" << "\tArea before scale = " << rect.getArea() << std::endl;
  rect.scale(2);
  std::cout << "\tk = 2, Area after scale = " << rect.getArea() << std::endl;

  Circle circ({ 10, 10 }, 4);
  std::cout << "Circle\n" << "\tArea before scale = " << circ.getArea() << std::endl;
  circ.scale(2);
  std::cout << "\tk = 2, Area after scale = " << circ.getArea() << std::endl;

  Triangle trng({ 10.6, 13.2 }, { 15.2, 18.0 }, { 11.1, 15.6 });
  std::cout << "Triangle\n" << "\tArea  before scale = " << trng.getArea() << std::endl;
  trng.scale(10);
  std::cout << "\tk = 10, Area after scale = " << trng.getArea() << std::endl;

return 0;
}
