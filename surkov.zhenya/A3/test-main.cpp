#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

using namespace surkov;

const double EPS = 0.000001;

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)

  BOOST_AUTO_TEST_CASE(CompositeShapeMove1Test)
  {
    Triangle trian({ 0, 0 }, { 4, 0 }, { 0, 2 });
    Circle circ({ 0, 0 }, 1);
    Rectangle rect({ 1, 1 }, 2, 4);
    std::shared_ptr< Shape > rectPtr = std::make_shared< Rectangle >(rect);
    std::shared_ptr< Shape > circPtr = std::make_shared< Circle >(circ);
    std::shared_ptr< Shape > trianPtr = std::make_shared< Triangle >(trian);
    CompositeShape comp(rectPtr);
    comp.add(circPtr);
    comp.add(trianPtr);
    comp.move({ 1, 1 });
    BOOST_CHECK_CLOSE_FRACTION(comp.getArea(),circ.getArea() + rect.getArea() + trian.getArea(), EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 1, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 1, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().width, 5, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().height, 4, EPS);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeMove2Test)
  {
    Triangle trian({ 0, 0 }, { 4, 0 }, { 0, 2 });
    Circle circ({ 0, 0 }, 1);
    Rectangle rect({ 1, 1 }, 2, 4);
    std::shared_ptr< Shape > rectPtr = std::make_shared< Rectangle >(rect);
    std::shared_ptr< Shape > circPtr = std::make_shared< Circle >(circ);
    std::shared_ptr< Shape > trianPtr = std::make_shared< Triangle >(trian);
    CompositeShape comp(rectPtr);
    comp.add(circPtr);
    comp.add(trianPtr);
    comp.move(5.5, 3.0);
    BOOST_CHECK_CLOSE_FRACTION(comp.getArea(),circ.getArea() + rect.getArea() + trian.getArea(), EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.x, 7, EPS);
    BOOST_CHECK_CLOSE_FRACTION(comp.getFrameRect().pos.y, 4, EPS);
  }


  BOOST_AUTO_TEST_CASE(CompositeShapeRemoveFigureTest)
  {
    Triangle trian({ 0, 0 }, { 4, 0 }, { 0, 2 });
    Circle circ({ 0, 0 }, 1);
    Rectangle rect({ 1, 1 }, 2, 4);
    std::shared_ptr< Shape > rectPtr = std::make_shared< Rectangle >(rect);
    std::shared_ptr< Shape > circPtr = std::make_shared< Circle >(circ);
    std::shared_ptr< Shape > trianPtr = std::make_shared< Triangle >(trian);
    CompositeShape comp(rectPtr);
    comp.add(circPtr);
    comp.add(trianPtr);
    comp.removeShape(2);
    BOOST_CHECK_EQUAL(comp.getCount(), 2);
    BOOST_CHECK_EQUAL(comp.getArea(), rect.getArea() + trian.getArea());
    BOOST_CHECK_EQUAL(comp.getFrameRect().pos.x, 2);
    BOOST_CHECK_EQUAL(comp.getFrameRect().pos.y, 1);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeRemoveAllTest)
  {
    Triangle trian({ 0, 0 }, { 4, 0 }, { 0, 2 });
    Circle circ({ 0, 0 }, 1);
    Rectangle rect({ 1, 1 }, 2, 4);
    std::shared_ptr< Shape > rectPtr = std::make_shared< Rectangle >(rect);
    std::shared_ptr< Shape > circPtr = std::make_shared< Circle >(circ);
    std::shared_ptr< Shape > trianPtr = std::make_shared< Triangle >(trian);
    CompositeShape comp(rectPtr);
    comp.add(circPtr);
    comp.add(trianPtr);
    comp.removeAll();
    BOOST_CHECK_EQUAL(comp.getCount(), 0);
    BOOST_CHECK_EQUAL(comp.getArea(), 0);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeScaleInvalidTest)
  {
    Triangle trian({ 0, 0 }, { 4, 0 }, { 0, 2 });
    Circle circ({ 0, 0 }, 1);
    Rectangle rect({ 1, 1 }, 2, 4);
    std::shared_ptr< Shape > rectPtr = std::make_shared< Rectangle >(rect);
    std::shared_ptr< Shape > circPtr = std::make_shared< Circle >(circ);
    std::shared_ptr< Shape > trianPtr = std::make_shared< Triangle >(trian);
    CompositeShape comp(rectPtr);
    comp.add(circPtr);
    comp.add(trianPtr);
    BOOST_CHECK_THROW(comp.scale(-2), std::invalid_argument);
    BOOST_CHECK_THROW(comp.scale(0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeRemoveInvalidTest)
  {
    Triangle trian({ 0, 0 }, { 4, 0 }, { 0, 2 });
    Circle circ({ 0, 0 }, 1);
    Rectangle rect({ 1, 1 }, 2, 4);
    std::shared_ptr< Shape > rectPtr = std::make_shared< Rectangle >(rect);
    std::shared_ptr< Shape > circPtr = std::make_shared< Circle >(circ);
    std::shared_ptr< Shape > trianPtr = std::make_shared< Triangle >(trian);
    CompositeShape comp(rectPtr);
    comp.add(circPtr);
    comp.add(trianPtr);
    BOOST_CHECK_THROW(comp.removeShape(11), std::out_of_range);
    BOOST_CHECK_THROW(comp.removeShape(-1), std::out_of_range);
    comp.removeAll();
    BOOST_CHECK_THROW(comp.removeShape(1), std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeAddShapeInvalidTest)
  {
    Triangle trian({ 0, 0 }, { 4, 0 }, { 0, 2 });
    Circle circ({ 0, 0 }, 1);
    Rectangle rect({ 1, 1 }, 2, 4);
    std::shared_ptr< Shape > rectPtr = std::make_shared< Rectangle >(rect);
    std::shared_ptr< Shape > circPtr = std::make_shared< Circle >(circ);
    std::shared_ptr< Shape > trianPtr = std::make_shared< Triangle >(trian);
    CompositeShape comp(rectPtr);
    comp.add(circPtr);
    comp.add(trianPtr);
    BOOST_CHECK_THROW(comp.add(rectPtr), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()
