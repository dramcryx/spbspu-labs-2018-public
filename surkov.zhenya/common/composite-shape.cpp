#include "composite-shape.hpp"
#include <cmath>
#include <stdexcept>
#include <memory>

namespace surkov
{

  CompositeShape::CompositeShape(const std::shared_ptr <Shape>shape):
    shapes(nullptr),
    count(0)
  {
    if (shape == nullptr)
    {
      throw std::invalid_argument("Object can't be nullptr!");
    }
    add(shape);

  }

  CompositeShape::~CompositeShape() 
  {};


void surkov::CompositeShape::removeAll()
{
  shapes.reset();
  shapes = nullptr;
  count = 0;
}

int surkov::CompositeShape::getCount() const
{
  return count;
}


  void surkov::CompositeShape::add(const std::shared_ptr<Shape> shape)
  {
    if (shape == nullptr)
    { 
      throw std::invalid_argument("null ptr");
    }

    for (int i = 0; i < count; ++i)
    {
      if (shape == shapes[i])
      {
        throw std::invalid_argument("such figure is already exists");
      }
    }

    std::unique_ptr < std::shared_ptr < surkov::Shape >[] > newShape(new std::shared_ptr < surkov::Shape >[count + 1]);
    for (int i = 0; i < count; i++)
    {
      newShape[i] = shapes[i];
    }
    newShape[count] = shape;
    shapes.swap(newShape);
    count++; 
  }


void surkov::CompositeShape::removeShape(const int index)
{
  if (count == 0)
  {
    throw std::out_of_range("empty");
  }
  else
  {
    if ((index <= 0) || (index >= count))
    {
      throw std::out_of_range("invalid index");
    }
  }
  if (count == 1)
  {
    removeAll();
  }
  else
  {
    std::unique_ptr< std::shared_ptr< surkov::Shape > [] > newShape(new std::shared_ptr< surkov::Shape > [count - 1]);
    for (int i = 0; i < (index - 1); i++)
    {
      newShape[i] = shapes[i];
    }
    for (int i = index; i < count; i++)
    {
      newShape[i-1] = shapes[i];
    }
    shapes.swap(newShape);
    count--;
  }
}


  point_t CompositeShape::findCentre()const
  {
    int i;
    double x_r, x_l, y_u, y_d;
    rectangle_t frameRect;

    frameRect = shapes[0]->getFrameRect();
    x_r = frameRect.pos.x + frameRect.width / 2;
    x_l = frameRect.pos.x - frameRect.width / 2;
    y_u = frameRect.pos.y + frameRect.height / 2;
    y_d = frameRect.pos.y - frameRect.height / 2;

    for (i = 0; i < count; i++) 
    {
      frameRect = shapes[i]->getFrameRect();
      if ((frameRect.pos.x + frameRect.width / 2) > x_r) 
      {
        x_r = frameRect.pos.x + frameRect.width / 2;
      } 

      if ((frameRect.pos.x - frameRect.width / 2) < x_l)
      {
        x_l = frameRect.pos.x - frameRect.width / 2;
      }

      if ((frameRect.pos.y + frameRect.height/ 2) > y_u)
      {
        y_u = frameRect.pos.y + frameRect.height / 2;
      }

      if ((frameRect.pos.y - frameRect.height / 2) < y_d)
      {
        y_d = frameRect.pos.y - frameRect.height / 2;
      }
    }
    return{ ((x_r - x_l) / 2) + x_l, ((y_u - y_d) / 2) + y_d };
  }


  double CompositeShape::getArea() const 
  {
    double S = 0;
    for (int i = 0; i < count; i++) 
    {
      S += (shapes[i])->getArea();
    }
    return S;
  }


  surkov::rectangle_t surkov::CompositeShape::getFrameRect() const
  {
    rectangle_t rect;
    double x_r, x_l, y_u, y_d;

    surkov::rectangle_t frameRect = shapes[0]->getFrameRect();
    x_r = frameRect.pos.x + frameRect.width / 2;
    x_l = frameRect.pos.x - frameRect.width / 2;
    y_u = frameRect.pos.y + frameRect.height / 2;
    y_d = frameRect.pos.y - frameRect.height / 2;

    for (int i = 0; i < count; i++)
    {
      frameRect = shapes[i]->getFrameRect();
      if ((frameRect.pos.x + frameRect.width / 2) > x_r)
      {
        x_r = frameRect.pos.x + frameRect.width / 2;
      }

      if ((frameRect.pos.x - frameRect.width / 2) < x_l)
      {
        x_l = frameRect.pos.x - frameRect.width / 2;
      }

      if ((frameRect.pos.y + frameRect.height / 2) > y_u)
      {
        y_u = frameRect.pos.y + frameRect.height / 2;
      }

      if ((frameRect.pos.y - frameRect.height / 2) < y_d)
      {
        y_d = frameRect.pos.y - frameRect.height / 2;
      }
    }

    rect.width = x_r - x_l;
    rect.height = y_u - y_d;
    rect.pos = findCentre();
    return rect;
  } 

  void surkov::CompositeShape::scale(const double coef)
  {
    if (coef < 1.0) 
    {
      throw std::invalid_argument("Error: Invalid argument: factor must be > 0.");
    }
    const surkov::point_t pos = getFrameRect().pos;

    for (int i = 0; i < count; ++i)
    {
      surkov::point_t posFigure = shapes[i]->getFrameRect().pos;
      double dy = posFigure.y - pos.y;
      double dx = posFigure.x - pos.x;
      shapes[i]->move((coef - 1) * dx, (coef - 1) * dy);
      shapes[i]->scale(coef);
    }
  }


  void CompositeShape::move(const double x_offset, const double y_offset)
  {
    for (int i = 0; i < count; i++) 
    {
      (shapes[i])->move(x_offset, y_offset);
    }

  }

  void CompositeShape::move(const point_t &point) 
  {
  point_t centre = findCentre();
  double dx = point.x - centre.x;
  double dy = point.y - centre.y;
  move(dx, dy);
  }

}
