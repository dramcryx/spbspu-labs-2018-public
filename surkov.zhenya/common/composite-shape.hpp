#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP 

#include "shape.hpp"
#include <memory>

namespace surkov
{
  class CompositeShape : public Shape
  {
  public:
    CompositeShape(const std::shared_ptr <Shape>shape);
    ~CompositeShape();
    point_t findCentre() const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &point) override;
    void move(const double x_offset, const double y_offset)override;
    void scale(double coef)override;
    void add(const std::shared_ptr<Shape> shape);
    
    void removeShape(const int index);
    void removeAll();
    int getCount() const;

  private:
    std::unique_ptr < std::shared_ptr<surkov::Shape>[] > shapes;
    int count;

  };

}

#endif
