#include "triangle.hpp"
#include <iostream>
#include <cmath>

using namespace surkov;

Triangle::Triangle(const point_t & a, const point_t & b, const point_t & c) :
  a_(a),
  b_(b),
  c_(c),
  centre({ (a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3 })
{
  if ((fabs((a_.x - c_.x) * (b_.y - c_.y) - (b_.x - c_.x) * (a_.y - c_.y)) * 0.5) == 0)
  {
    throw std::invalid_argument("Invalid argument");
  }
}


double Triangle::getArea() const
{
  return fabs((a_.x - c_.x) * (b_.y - c_.y) - (b_.x - c_.x) * (a_.y - c_.y)) * 0.5;
}

rectangle_t Triangle::getFrameRect() const
{
  double Xmin = a_.x < b_.x ? (a_.x < c_.x ? a_.x : c_.x) : (b_.x < c_.x ? b_.x : c_.x);
  double Xmax = a_.x > b_.x ? (a_.x > c_.x ? a_.x : c_.x) : (b_.x > c_.x ? b_.x : c_.x);
  double Ymin = a_.y < b_.y ? (a_.y < c_.y ? a_.y : c_.y) : (b_.y < c_.y ? b_.y : c_.y);
  double Ymax = a_.y > b_.y ? (a_.y < c_.y ? a_.y : c_.y) : (b_.y > c_.y ? b_.y : c_.y);
  return{ (Xmax - Xmin), (Ymax - Ymin),{ ((Xmax - Xmin) / 2 + Xmin), ((Ymax - Ymin) / 2 + Ymin) } };
}

void Triangle::scale(double coeff)
{
  if (coeff < 0.0) 
  {
    throw std::invalid_argument("Scale coefficient of the triangle must be > 0!");
  }
  point_t centre = { (a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3 };
  a_ = {centre.x + coeff * (a_.x - centre.x), centre.y + coeff * (a_.y - centre.y)};
  b_ = {centre.x + coeff * (b_.x - centre.x), centre.y + coeff * (b_.y - centre.y)};
  c_ = {centre.x + coeff * (c_.x - centre.x), centre.y + coeff * (c_.y - centre.y)};
}

point_t Triangle::getCenter() const
{
  return centre;
}

void Triangle::move(const point_t &point)
{
  centre.x = point.x;
  centre.y = point.y;
  a_ = { a_.x + (point.x - centre.x), a_.x + (point.y - centre.y) };
  b_ = { b_.x + (point.x - centre.x), b_.x + (point.y - centre.y) };
  c_ = { c_.x + (point.x - centre.x), c_.x + (point.y - centre.y) };
}


void Triangle::move(const double x_offset, const double y_offset)
{
  a_ = { a_.x + x_offset, a_.y + y_offset };
  c_ = { c_.x + x_offset, c_.y + y_offset };
  b_ = { b_.x + x_offset, b_.y + y_offset };
  centre.x += x_offset;
  centre.y += y_offset;
}

