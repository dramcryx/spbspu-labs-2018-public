#ifndef circle_hpp  
#define circle_hpp

#include "shape.hpp"
#include "base-types.hpp"

class Circle : public Shape
{
public:
  Circle(double circlerad, point_t cpoint); 
  double getArea() const override; 
  rectangle_t getFrameRect() const override;
  void move(const double dX,const double dY) override; 
  void move(const point_t point) override;
  void dataoutput() const override;
private:
  double cR_;
  point_t pos_;
};

#endif
