#ifndef rectangle_hpp
#define rectangle_hpp
#include "shape.hpp"
#include "base-types.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(double w, double h, double xrect, double yrect);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const double dX, const double dY) override;
  void move(const point_t point) override;
  void dataoutput() const override;
private: 
  rectangle_t rctngl_;
};

#endif
