#include "shape.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "base-types.hpp"
using namespace tsikalyuk; 

int main()
{
  point_t point = {534,348};

  Rectangle rectangle(300,200,500,300);
  Shape *RCT = &rectangle;
  RCT -> dataoutput();
  RCT -> move(40,30);
  RCT -> scale(2);
  RCT -> dataoutput();

  point = {765,395};

  Circle circle(50,{200,400});
  Shape *CRL = &circle;
  CRL -> dataoutput();
  CRL -> scale(3);
  CRL -> move(point);
  CRL -> dataoutput();

  point = {543,123};

  Triangle triangle({10,10},{20,40},{50,18});
  Shape *TRG = &triangle;
  TRG -> dataoutput();
  TRG -> scale(2);
  TRG -> move(point);
  TRG -> dataoutput();

  return 0;
}
