#define BOOST_TEST_MODULE boost_test
#include <boost/test/included/unit_test.hpp>
#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include <cmath>
#include "shape.hpp"
#include "base-types.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#define USE_DEFINE_MATH
#define EPSILON 0.000001
using namespace tsikalyuk;

BOOST_AUTO_TEST_SUITE(circle_testing)

BOOST_AUTO_TEST_CASE(cr_move_framerect_chk)
{
  Circle cr(6,{100,100});
  cr.move(50, 10);
  BOOST_CHECK_CLOSE(cr.getFrameRect().width,6, EPSILON);
  BOOST_CHECK_CLOSE(cr.getFrameRect().height,6, EPSILON);
  BOOST_CHECK_CLOSE(cr.getFrameRect().pos.x,150, EPSILON);
  BOOST_CHECK_CLOSE(cr.getFrameRect().pos.y,110, EPSILON);
}

BOOST_AUTO_TEST_CASE(cr_move_area_chk)
{
  Circle cr(6,{100,100});
  cr.move(50, 10);
  BOOST_CHECK_CLOSE(cr.getArea(), M_PI*6*6 , EPSILON);
}

BOOST_AUTO_TEST_CASE(cr_scale_area_chk)
{
  Circle cr(3,{50,50});
  cr.scale(4);
  BOOST_CHECK_CLOSE(cr.getArea(), M_PI*4*4*3*3, EPSILON);
}

BOOST_AUTO_TEST_CASE(cr_invalid_radius_chk)
{
  BOOST_CHECK_THROW(Circle cr(0,{50,50}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(cr_invalid_scale_chk)
{
  Circle cr(5,{50,50});
  BOOST_CHECK_THROW(cr.scale(-7), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(rectangle_testing)

BOOST_AUTO_TEST_CASE(rc_move_framerect_chk)
{
  Rectangle rc(75, 75, 6, 6);
  rc.move(50, 10);

  BOOST_CHECK_CLOSE(rc.getFrameRect().height,75, EPSILON);
  BOOST_CHECK_CLOSE(rc.getFrameRect().width,75, EPSILON);
  BOOST_CHECK_CLOSE(rc.getFrameRect().pos.x,56, EPSILON);
  BOOST_CHECK_CLOSE(rc.getFrameRect().pos.y,16, EPSILON);
}

BOOST_AUTO_TEST_CASE(rc_move_area_chk)
{
  Rectangle rc(75, 75, 6, 6);
  rc.move(50, 10);
  BOOST_CHECK_CLOSE(rc.getArea(),5625, EPSILON);
}

BOOST_AUTO_TEST_CASE(rc_scale_area_chk)
{
  Rectangle rc(9, 8, 6, 6);
  rc.scale(4);
  BOOST_CHECK_CLOSE(rc.getArea(), 1152 , EPSILON);
}

BOOST_AUTO_TEST_CASE(rc_invalid_side_chk)
{
  BOOST_CHECK_THROW(Rectangle rc(40,-30, 90, 50), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(rc_invalid_scale_chk)
{
  Rectangle rc(75, 75, 6, 6);
  BOOST_CHECK_THROW(rc.scale(-7), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(triangle_testing)

BOOST_AUTO_TEST_CASE(tr_move_framerect_chk)
{
  Triangle tr({20,20},{20,40},{40,20});
  tr.move(30, 10);
  BOOST_CHECK_CLOSE(tr.getFrameRect().height,20, EPSILON);
  BOOST_CHECK_CLOSE(tr.getFrameRect().width,20, EPSILON);
  BOOST_CHECK_CLOSE(tr.getFrameRect().pos.x,60, EPSILON);
  BOOST_CHECK_CLOSE(tr.getFrameRect().pos.y,40, EPSILON);

}

BOOST_AUTO_TEST_CASE(tr_move_area_chk)
{
  Triangle tr({20,20},{20,40},{40,20});
  tr.move(30, 10);
  BOOST_CHECK_CLOSE(tr.getArea(),200, EPSILON);
}

BOOST_AUTO_TEST_CASE(tr_scale_area_chk)
{
  Triangle tr({20,20},{20,40},{40,20});
  tr.scale(4);
  BOOST_CHECK_CLOSE(tr.getArea(),5000, EPSILON);
}

BOOST_AUTO_TEST_CASE(tr_invalid_radius_chk)
{
  BOOST_CHECK_THROW(Triangle tr({20,20},{30,20},{40,20}), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(tr_invalid_scale_chk)
{
  Triangle tr({20,20},{20,40},{40,20});
  BOOST_CHECK_THROW(tr.scale(-7), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
