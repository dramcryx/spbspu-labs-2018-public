#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <stdexcept>
#include "shape.hpp"
#include "base-types.hpp"
#include "circle.hpp"
using namespace tsikalyuk;

Circle::Circle (double circlerad, point_t cpoint):
  cR_(circlerad),
  pos_(cpoint)
{
  if (cR_ <= 0.0)               
    throw std::invalid_argument("radius of circle <= 0");
}

double Circle::getArea() const 
{
  return M_PI*cR_*cR_;
}
rectangle_t Circle::getFrameRect() const 
{
  return {cR_,cR_,pos_.x,pos_.y};
}

void Circle::move(const double dX,const double dY) 
{
  pos_.x += dX;
  pos_.y += dY;
}

void Circle::move(const point_t point) 
{
  pos_.x = point.x;
  pos_.y = point.y;
}

void Circle::scale(const double x)
{
  if (x <= 0.0)
  throw std::invalid_argument("argument x <= 0");
  cR_ *= x;
}

void Circle::dataoutput() const 
{
  std::cout<<"area_of_circle_="<<getArea()<<std::endl;
  std::cout<<"framerect_width_height_X_Y_="<<getFrameRect().width<<" "<<getFrameRect().height<<" ";
  std::cout<<getFrameRect().pos.x<<" "<<getFrameRect().pos.y<<std::endl;
  std::cout<<"pos_ition_X_Y_"<<pos_.x<<" "<<pos_.y<<std::endl;
}
