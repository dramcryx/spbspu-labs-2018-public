#ifndef rectangle_hpp
#define rectangle_hpp
#include "shape.hpp"
#include "base-types.hpp"

namespace tsikalyuk
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(double w, double h, double xrect, double yrect);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    double getHeight();
    double getWidth();
    void move(const double dX, const double dY) override;
    void move(const point_t point) override;
    void scale(const double x) override;
    void dataoutput() const override;
  private: 
    rectangle_t rctngl_;
  };
}
#endif
