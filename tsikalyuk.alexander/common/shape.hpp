#ifndef shape_hpp
#define shape_hpp
#include "base-types.hpp"

namespace tsikalyuk
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0; 
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(const double dX,const double dY) = 0; 
    virtual void move(const point_t point) = 0;
    virtual void scale(const double x) = 0; 
    virtual void dataoutput() const = 0;
  };
}

#endif
