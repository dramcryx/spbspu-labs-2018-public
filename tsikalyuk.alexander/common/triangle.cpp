#include <cmath>
#include <iostream>
#include <stdexcept>
#include "shape.hpp"
#include "base-types.hpp"
#include "triangle.hpp"
using namespace tsikalyuk;

Triangle::Triangle(const point_t &point1, const point_t &point2, const point_t &point3):
  dot1_(point1),
  dot2_(point2),
  dot3_(point3),
  pos_({(point1.x + point2.x + point3.x)/3,(point1.y + point2.y + point3.y)/3})
{
  if (((dot1_.x == dot2_.x) && (dot2_.x == dot3_.x)) || ((dot1_.y == dot2_.y) && (dot2_.y == dot3_.y)))
  throw  std::invalid_argument ("Three points lie on a line");
}

double Triangle::getArea() const
{
  return( std::abs(((dot2_.x - dot1_.x)*(dot3_.y - dot1_.y)-(dot3_.x - dot1_.x)*(dot2_.y- dot1_.y))/2) );
}

rectangle_t Triangle::getFrameRect() const
{
  double maxX = 0;
  double maxY = 0;
  double minX = 0;
  double minY = 0;
  maxX = std::max(std::max(dot1_.x,dot2_.x),dot3_.x);
  minX = std::min(std::min(dot1_.x,dot2_.x),dot3_.x);
  maxY= std::max(std::max(dot1_.y,dot2_.y),dot3_.y);
  minY = std::min(std::min(dot1_.y,dot2_.y),dot3_.y);
  rectangle_t returnrect;
  returnrect.pos.x = (maxX + minX)/2;
  returnrect.pos.y = (maxY + minY)/2;
  returnrect.width  = maxX - minX;
  returnrect.height = maxY - minY;
  return returnrect;
}

void Triangle::move(const double dX,const double dY)
{
  dot1_.x += dX;
  dot1_.y += dY;
  dot2_.x += dX;
  dot2_.y += dY;
  dot3_.x += dX;
  dot3_.y += dY;
  pos_.x += dX;
  pos_.y += dY;
}

void Triangle::move(const point_t point)
{
  move(point.x - pos_.x,point.y - pos_.y);
}

void Triangle::scale(const double x)
{
  if (x <= 0)
  throw std::invalid_argument("argument x <= 0");
  dot1_.x = dot1_.x+(dot1_.x-pos_.x)*x;
  dot1_.y = dot1_.y+(dot1_.y-pos_.y)*x;
  dot2_.x = dot2_.x+(dot2_.x-pos_.x)*x;
  dot2_.y = dot2_.y+(dot2_.y-pos_.y)*x;
  dot3_.x = dot3_.x+(dot3_.x-pos_.x)*x;
  dot3_.y = dot3_.y+(dot3_.y-pos_.y)*x;
}

void Triangle::dataoutput() const
{
  std::cout<<"area_of_trianglele_="<<getArea()<<std::endl;
  std::cout<<"framerect_width_height_X_Y_="<<getFrameRect().width<<" "<<getFrameRect().height<<" ";
  std::cout<<getFrameRect().pos.x<<" "<<getFrameRect().pos.y<<std::endl;
  std::cout<<"position_X_Y_"<<pos_.x<<" "<<pos_.y<<std::endl;
}
