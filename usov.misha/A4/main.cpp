#include <iostream>
#include <cmath>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace usov;

int main()
{
  std::shared_ptr<Rectangle> rect1 = std::make_shared<Rectangle>(Rectangle(2, 2,{ 3,3 }));
  std::shared_ptr<Rectangle> rect3 = std::make_shared<Rectangle>(Rectangle(2, 2, { 1,1 }));
  std::shared_ptr<Circle> circ1 = std::make_shared<Circle>(Circle(2,{10,10}));
  std::shared_ptr<Circle> circ2 = std::make_shared<Circle>(Circle(10,{5,5}));

  std::shared_ptr<CompositeShape> composite = std::make_shared<CompositeShape>(CompositeShape(circ2));
  composite->addShape(rect1);

  Matrix Matr;
  Matr.addShape(rect3);
  Matr.addShape(circ1);
  Matr.addFromComposite(composite);
  Matr.print(Matr);
  return 0;
}
