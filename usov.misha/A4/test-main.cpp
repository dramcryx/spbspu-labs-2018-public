#define BOOST_TEST_MAIN

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace usov;
const double epsilon = 0.001;

BOOST_AUTO_TEST_SUITE(MatrixTestConstructor)

  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<usov::Rectangle>(usov::Rectangle(1, 1, { 0,0 }));
    std::shared_ptr<Shape> rect2 = std::make_shared<usov::Rectangle>(usov::Rectangle(2, 2, { 0,0 }));
    std::shared_ptr<Shape> rect3 = std::make_shared<usov::Rectangle>(usov::Rectangle(3, 3, { 0,0 }));
    Matrix Matrix1;
    Matrix1.addShape(rect1);
    Matrix1.addShape(rect2);
    Matrix1.addShape(rect3);
    Matrix Matrix2(Matrix1);
    BOOST_CHECK_EQUAL(Matrix2.getNumOfLayers(),Matrix1.getNumOfLayers());
    BOOST_CHECK_EQUAL(Matrix2.getNumOfShape(),Matrix1.getNumOfShape());
    BOOST_CHECK_EQUAL(Matrix2.getShape(0),Matrix2.getShape(0));
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<usov::Rectangle>(usov::Rectangle(1, 1, { 0,0 }));
    std::shared_ptr<Shape> rect2 = std::make_shared<usov::Rectangle>(usov::Rectangle(2, 2, { 0,0 }));
    std::shared_ptr<Shape> rect3 = std::make_shared<usov::Rectangle>(usov::Rectangle(3, 3, { 0,0 }));
    Matrix Matrix1;
    Matrix1.addShape(rect1);
    Matrix1.addShape(rect2);
    Matrix1.addShape(rect3);
    Matrix Matrix2(std::move(Matrix1));
    BOOST_CHECK_EQUAL(Matrix2.getNumOfLayers(),3);
    BOOST_CHECK_EQUAL(Matrix2.getNumOfShape(),3);
    BOOST_CHECK_EQUAL(Matrix2.getShape(0),rect1);
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<usov::Rectangle>(usov::Rectangle(1, 1, { 0,0 }));
    std::shared_ptr<Shape> rect2 = std::make_shared<usov::Rectangle>(usov::Rectangle(2, 2, { 0,0 }));
    std::shared_ptr<Shape> rect3 = std::make_shared<usov::Rectangle>(usov::Rectangle(3, 3, { 0,0 }));
    Matrix Matrix1;
    Matrix1.addShape(rect1);
    Matrix1.addShape(rect2);
    Matrix1.addShape(rect3);
    Matrix Matrix2;
    Matrix2 = Matrix1;
    BOOST_CHECK_EQUAL(Matrix2.getNumOfLayers(),Matrix1.getNumOfLayers());
    BOOST_CHECK_EQUAL(Matrix2.getNumOfShape(),Matrix1.getNumOfShape());
    BOOST_CHECK_EQUAL(Matrix2.getShape(0),Matrix2.getShape(0));
  }

  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<usov::Rectangle>(usov::Rectangle(1, 1, { 0,0 }));
    std::shared_ptr<Shape> rect2 = std::make_shared<usov::Rectangle>(usov::Rectangle(2, 2, { 0,0 }));
    std::shared_ptr<Shape> rect3 = std::make_shared<usov::Rectangle>(usov::Rectangle(3, 3, { 0,0 }));
    Matrix Matrix1;
    Matrix1.addShape(rect1);
    Matrix1.addShape(rect2);
    Matrix1.addShape(rect3);
    Matrix Matrix2;
    Matrix2 = std::move(Matrix1);
    BOOST_CHECK_EQUAL(Matrix2.getNumOfLayers(),3);
    BOOST_CHECK_EQUAL(Matrix2.getNumOfShape(),3);
    BOOST_CHECK_EQUAL(Matrix2.getShape(0),rect1);
  }

  BOOST_AUTO_TEST_CASE(GET_SHAPES)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<usov::Rectangle>(usov::Rectangle(1, 1, { 0,0 }));
    std::shared_ptr<Shape> rect2 = std::make_shared<usov::Rectangle>(usov::Rectangle(2, 2, { 0,0 }));
    std::shared_ptr<Shape> rect3 = std::make_shared<usov::Rectangle>(usov::Rectangle(3, 3, { 0,0 }));
    Matrix Matrix1;
    Matrix1.addShape(rect1);
    Matrix1.addShape(rect2);
    Matrix1.addShape(rect3);
    BOOST_CHECK_EQUAL(Matrix1.getShape(0),rect1);
    BOOST_CHECK_EQUAL(Matrix1.getShape(1),rect2);
    BOOST_CHECK_EQUAL(Matrix1.getShape(2),rect3);
  }

  BOOST_AUTO_TEST_CASE(GET_FROM_COMPOSITE)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<usov::Rectangle>(usov::Rectangle(1, 1, { 0,0 }));
    std::shared_ptr<Shape> rect2 = std::make_shared<usov::Rectangle>(usov::Rectangle(2, 2, { 0,0 }));
    std::shared_ptr<Shape> rect3 = std::make_shared<usov::Rectangle>(usov::Rectangle(3, 3, { 0,0 }));

    std::shared_ptr<CompositeShape> composite = std::make_shared<CompositeShape>(CompositeShape(rect1));
    composite->addShape(rect2);

    Matrix Matrix1;
    Matrix1.addFromComposite(composite);
    Matrix1.addShape(rect3);
    BOOST_CHECK_EQUAL(Matrix1.getNumOfLayers(),3);
  }

  BOOST_AUTO_TEST_CASE(RECTANGLE_ROTATE_TEST)
  {
    usov::Rectangle Rect(4,6,{0,0});
    rectangle_t Frame = Rect.getFrameRect();
    Rect.rotate(90);
    BOOST_CHECK_CLOSE(Rect.getVertex(0).x,3,epsilon);
    BOOST_CHECK_CLOSE(Rect.getVertex(3).x,3,epsilon);
    BOOST_CHECK_CLOSE(Rect.getVertex(0).y,-2,epsilon);
    BOOST_CHECK_CLOSE(Rect.getVertex(3).y,2,epsilon);
    BOOST_CHECK_CLOSE(Rect.getFrameRect().width, Frame.width, epsilon);
    BOOST_CHECK_CLOSE(Rect.getFrameRect().height,Frame.height, epsilon);
    BOOST_CHECK_CLOSE(Rect.getFrameRect().pos.x, Frame.pos.x, epsilon);
    BOOST_CHECK_CLOSE(Rect.getFrameRect().pos.y, Frame.pos.y, epsilon);
  }

  BOOST_AUTO_TEST_CASE(ROTATE_COMPOSITE_SHAPE)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<usov::Rectangle>(usov::Rectangle(2, 2, { 1,1 }));
    std::shared_ptr<Shape> rect2 = std::make_shared<usov::Rectangle>(usov::Rectangle(2, 2, { 2,2 }));
    std::shared_ptr<Shape> rect3 = std::make_shared<usov::Rectangle>(usov::Rectangle(2, 2, { 3,3 }));

    CompositeShape Composite;
    Composite.addShape(rect1);
    Composite.addShape(rect2);
    Composite.addShape(rect3);
    rectangle_t Frame = Composite.getFrameRect();
    double area = Composite.getArea();
    Composite.rotate(90);

    BOOST_CHECK_CLOSE(Composite.getFrameRect().pos.x,Frame.pos.x,epsilon);
    BOOST_CHECK_CLOSE(Composite.getFrameRect().pos.x,Frame.pos.y,epsilon);
    BOOST_CHECK_CLOSE(Composite.getFrameRect().width, Frame.width, epsilon);
    BOOST_CHECK_CLOSE(Composite.getFrameRect().height, Frame.height, epsilon);
    BOOST_CHECK_CLOSE(Composite.getArea(), area, epsilon);

    Composite.rotate(60);

    BOOST_CHECK_CLOSE(Composite.getFrameRect().pos.x, Frame.pos.x, epsilon);
    BOOST_CHECK_CLOSE(Composite.getFrameRect().pos.x, Frame.pos.y, epsilon);
    BOOST_CHECK_CLOSE(Composite.getFrameRect().width, 4.73205, epsilon);
    BOOST_CHECK_CLOSE(Composite.getFrameRect().height, 2.73205, epsilon);
    BOOST_CHECK_CLOSE(Composite.getArea(), area, epsilon);

    Composite.rotate(45);

    BOOST_CHECK_CLOSE(Composite.getFrameRect().pos.x, Frame.pos.x, epsilon);
    BOOST_CHECK_CLOSE(Composite.getFrameRect().pos.x, Frame.pos.y, epsilon);
    BOOST_CHECK_CLOSE(Composite.getFrameRect().width, 3.41421, epsilon);
    BOOST_CHECK_CLOSE(Composite.getFrameRect().height, 4.44948, epsilon);
    BOOST_CHECK_CLOSE(Composite.getArea(), area, epsilon);
  }
BOOST_AUTO_TEST_SUITE_END()
