#include <cmath>
#include "matrix.hpp"

usov::Matrix::Matrix():
  numOfShapes_(0),
  numOfLayers_(0),
  shapes_(nullptr),
  sizeOfLayer_(nullptr)
{}

usov::Matrix::Matrix(const Matrix & copy):
  numOfShapes_(copy.numOfShapes_),
  numOfLayers_(copy.numOfLayers_),
  shapes_(new std::shared_ptr<Shape>[numOfShapes_]),
  sizeOfLayer_(new int[numOfLayers_])
{
  for(int i = 0; i < numOfShapes_; i++)
  {
    shapes_[i] = copy.shapes_[i];
  }
  for(int i = 0; i < numOfLayers_; i++)
  {
    sizeOfLayer_[i] = copy.sizeOfLayer_[i];
  }
}

usov::Matrix::Matrix(Matrix &&move):
  numOfShapes_(move.numOfShapes_),
  numOfLayers_(move.numOfLayers_)
{
  shapes_.swap(move.shapes_);
  sizeOfLayer_.swap(move.sizeOfLayer_);
  move.shapes_.reset();
  move.sizeOfLayer_.reset();
  move.numOfLayers_ = 0;
  move.numOfShapes_ = 0;
}

usov::Matrix &usov::Matrix::operator=(const Matrix & copy)
{
  numOfLayers_ = copy.numOfLayers_;
  numOfShapes_ = copy.numOfShapes_;

  shapes_ = std::unique_ptr<std::shared_ptr<Shape>[]>(new std::shared_ptr<Shape>[numOfShapes_]);
  sizeOfLayer_ = std::unique_ptr<int[]>(new int[numOfLayers_]);


  for(int i = 0; i < numOfLayers_; i++)
  {
    sizeOfLayer_[i] = copy.sizeOfLayer_[i];
  }

  for(int i = 0; i < numOfShapes_; i++)
  {
    shapes_[i] = copy.shapes_[i];
  }
  return *this;
}

usov::Matrix &usov::Matrix::operator=(Matrix && move)
{
  if(this != &move)
  {
    sizeOfLayer_.swap(move.sizeOfLayer_);
    shapes_.swap(move.shapes_);
    numOfLayers_ = move.numOfLayers_;
    numOfShapes_ = move.numOfShapes_;
    move.numOfLayers_ = 0;
    move.numOfShapes_ = 0;
    move.sizeOfLayer_.reset();
    move.shapes_.reset();
  }
  else
  {
    throw std::invalid_argument("Error: moving the same object");
  }
  return *this;
}

int usov::Matrix::getNumOfLayers()
{
  return numOfLayers_;
}

int usov::Matrix::getNumOfShape()
{
  return numOfShapes_;
}

void usov::Matrix::addFromComposite(const std::shared_ptr<CompositeShape> &shape)
{
  if(shape->getSize() == 0)
  {
    throw std::invalid_argument("Error: composite shape does not contain any figures");
  }
  for(int i = 0; i < shape->getSize(); i++)
  {
    addShape((*shape)[i]);
  }
}

void usov::Matrix::addShape(const std::shared_ptr<Shape> shape)
{
  if (shape == nullptr)
   {
     throw std::invalid_argument("Shape has a nullptr value");
   }
  std::unique_ptr<std::shared_ptr<Shape>[]> shapesTmp(new std::shared_ptr<Shape>[numOfShapes_ + 1]);
  bool imposition = false;
  int  offset = 0;

  for (int  i = 0; i < numOfLayers_; i++)
  {
    imposition = true;
    for (int  j = 0; j < sizeOfLayer_[i]; j++)
    {
      rectangle_t rect1 = shapes_[offset + j]->getFrameRect();
      rectangle_t rect2 = shape->getFrameRect();
      if (fabs(rect1.pos.x - rect2.pos.x) <= rect1.width / 2 + rect2.width / 2 &&
          fabs(rect1.pos.y - rect2.pos.y) <= rect1.height / 2 + rect2.height / 2)
      {
        imposition = false;
      }
      shapesTmp[offset + j] = shapes_[offset + j];
    }
    offset += sizeOfLayer_[i];

    if (imposition)
    {
      shapesTmp[offset] = shape;
      for (int  j = offset; j < numOfShapes_; j++)
      {
        shapesTmp[j + 1] = shapes_[j];
      }
      sizeOfLayer_[i]++;
      numOfShapes_++;
      break;
    }
  }

  if(!imposition)
  {
    shapesTmp[numOfShapes_++] = shape;
    std::unique_ptr<int []> sizesTmp(new int [numOfLayers_+ 1]);
    for (int  i = 0; i < numOfLayers_; i++)
    {
      sizesTmp[i] = sizeOfLayer_[i];
    }
    sizesTmp[numOfLayers_++] = 1;
    sizeOfLayer_.swap(sizesTmp);
  }
  shapes_.swap(shapesTmp);
}

std::shared_ptr<usov::Shape> usov::Matrix::getShape(int number)const
{
  return shapes_[number];
}

int usov::Matrix::getLayerSize(int n)
{
  if(n > numOfLayers_)
  {
    throw std::invalid_argument("Error: This matrix does not exist this layer");
  }
  return sizeOfLayer_[n];
}

void usov::Matrix::print(Matrix &matrix)
{
  std::cout << "Count of layers: " <<  matrix.getNumOfLayers() << std::endl;
  std::cout << "Count of shapes: " << matrix.getNumOfShape() << std::endl;
  std::cout << std::endl;
  for(int i = 0; i < matrix.getNumOfLayers(); i++)
  {
    std::cout << "Layer number: " << i + 1 << std::endl;
    std::cout << "Count shapes in layer: " << matrix.getLayerSize(i) << std::endl;
  }
}
