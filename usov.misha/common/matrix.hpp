#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "composite-shape.hpp"

namespace usov {
 class Matrix
  {
  public:
    Matrix();
    Matrix(const Matrix & copy);
    Matrix(Matrix && move);
    Matrix &operator=(const Matrix & copy);
    Matrix &operator=(Matrix && move);

    int getNumOfLayers();
    int getNumOfShape();
    void addShape(const std::shared_ptr<Shape> shape);
    std::shared_ptr<Shape> getShape(int number) const;
    void addFromComposite(const std::shared_ptr<CompositeShape> &shape);
    int getLayerSize(int n);
    void print(Matrix &matrix);
  private:
    int numOfShapes_;
    int numOfLayers_;
    std::unique_ptr<std::shared_ptr<Shape>[]> shapes_;
    std::unique_ptr<int []> sizeOfLayer_;
  };
}

#endif //MATRIX_HPP
