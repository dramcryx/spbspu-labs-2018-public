#include <iostream>
#include <memory>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "shape.hpp"
#include "matrix.hpp"

using namespace vasilev;

int main()
{
  std::shared_ptr<Rectangle> rectptr1 = std::make_shared<Rectangle>(Rectangle(2, 2,{ 3,3 }));
  std::shared_ptr<Rectangle> rectptr2 = std::make_shared<Rectangle>(Rectangle(2, 2, { 1,1 }));
  std::shared_ptr<Circle> circptr1 = std::make_shared<Circle>(Circle(2,{10,10}));
  std::shared_ptr<Circle> circptr2 = std::make_shared<Circle>(Circle(10,{5,5}));
  std::shared_ptr<CompositeShape> compptr = std::make_shared<CompositeShape>(CompositeShape(circptr2));
  compptr->addShape(rectptr1);

  Matrix matr;
  matr.addShape(rectptr2);
  matr.addShape(circptr1);
  matr.addComposite(compptr);
  matr.getInfo();
  return 0;
}
