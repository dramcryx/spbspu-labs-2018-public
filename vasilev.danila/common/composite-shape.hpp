#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace vasilev
{
class CompositeShape :
    public Shape
{
public:
  CompositeShape(std::shared_ptr<Shape> figure);
  CompositeShape(const CompositeShape &figure);
  CompositeShape(CompositeShape &&figure);
  Shape &operator[](unsigned int n) const;
  CompositeShape &operator=(CompositeShape &&figure);
  CompositeShape &operator=(const CompositeShape &figure);
  void addShape(const std::shared_ptr<Shape> newshape);
  void deleteShape(unsigned int n);
  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const double dx, const double dy) override;
  void move(const point_t &point) override;
  void scale(const double mult) override;
  unsigned int getQuantity() const;
  std::shared_ptr<Shape> getShape(unsigned int n) const;
  void getInfo() const override;
  void rotate(double angle);
private:
  std::unique_ptr<std::shared_ptr<Shape>[]> arr_;
  unsigned int quant_;
};
}
#endif // COMPOSITESHAPE_HPP
