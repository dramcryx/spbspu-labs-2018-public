#define _USE_MATH_DEFINES
#include <iostream>
#include <stdexcept>
#include <cmath>
#include "rectangle.hpp"
namespace vasilev
{
Rectangle::Rectangle(double width, double height, const point_t &center_point):
  apex_{
    {center_point.x - width / 2, center_point.y - height / 2},
    {center_point.x - width / 2, center_point.y + height / 2},
    {center_point.x + width / 2, center_point.y + height / 2},
    {center_point.x + width / 2, center_point.y - height / 2}},
  rectangle_center_(center_point)
{
  if ((width < 0.0) || (height < 0.0))
  {
    throw std::invalid_argument("Parameters can't be lower thatn 0");
  }
}
double Rectangle::getWidth()
{
  return (apex_[3].x - apex_[0].x);
}
double Rectangle::getHeight()
{
  return (apex_[1].y - apex_[0].y);
}
double Rectangle::getArea() const
{
  return ((apex_[3].x - apex_[0].x) * (apex_[1].y - apex_[0].y));
}
void Rectangle::getInfo() const
{
  std::cout << std::endl << "h: "<< (apex_[1].y - apex_[0].y) <<"\t"<<"w: " << (apex_[3].x - apex_[0].x) << std::endl;
  std::cout << "Down left(x,y): (" << apex_[0].x << ", " << apex_[0].y << ")" << std::endl;
  std::cout << "Up left(x,y): (" << apex_[1].x << ", " << apex_[1].y << ")" << std::endl;
  std::cout << "Up right(x,y): (" << apex_[2].x << ", " << apex_[2].y << ")" << std::endl;
  std::cout << "Down right(x,y): (" << apex_[3].x << ", " << apex_[3].y << ")" << std::endl;
  std::cout << "Area: " << getArea() << std::endl;
  std::cout << "Point(x, y): (" << rectangle_center_.x << ", " << rectangle_center_.y << ")" << std::endl;
  std::cout << "Frame rectangle(h,w,center(x,y)): " << getFrameRect().height << ", " << getFrameRect().width
      << ", (" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << ")" << std::endl;
}
rectangle_t Rectangle::getFrameRect() const
{
  return {(apex_[3].x - apex_[0].x), (apex_[1].y - apex_[0].y), rectangle_center_};
}
void Rectangle::move(const double dx, const double dy)
{
  for (int i = 0; i < 4; i++)
  {
    apex_[i].x += dx;
    apex_[i].y += dy;
  }
  rectangle_center_.x += dx;
  rectangle_center_.y += dy;
}
void Rectangle::move(const point_t &point)
{
  for (int i = 0; i < 4; i++)
  {
    apex_[i].x += point.x - rectangle_center_.x;
    apex_[i].y += point.y - rectangle_center_.y;
  }
  rectangle_center_ = point;
}
void Rectangle::scale(const double mult)
{
  if (mult < 0.0)
  {
    throw std::invalid_argument("INVALID_MULTIPLIER");
  }
  else
  {
    for (int i = 0; i < 4; i++)
    {
      apex_[i].x *= mult;
      apex_[i].y *= mult;
    }
  }
}
void Rectangle::rotate(double angle)
{
  angle = (angle * M_PI)/ 180;
  for(int i = 0; i < 4; i++)
  {
    apex_[i] = {rectangle_center_.x +(apex_[i].x - rectangle_center_.x) * cos(angle)-
        (apex_[i].y - rectangle_center_.y) * sin(angle),
            rectangle_center_.y + (apex_[i].y - rectangle_center_.y) * cos(angle) +
                (apex_[i].x - rectangle_center_.x) * sin(angle)};
  }
}
point_t Rectangle::getApex(int n) const
{
  return apex_[n];
}
}
