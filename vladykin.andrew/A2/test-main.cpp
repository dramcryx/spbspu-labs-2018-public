#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <iostream>
#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"

const double tolerance = 0.000001;

BOOST_AUTO_TEST_SUITE(rectangleTests)

  BOOST_AUTO_TEST_CASE(Constructor)
  {
    BOOST_CHECK_THROW(vladykin::Rectangle rectangle({0.0, 0.0}, -1.0, 2.0), std::invalid_argument);
    BOOST_CHECK_THROW(vladykin::Rectangle rectangle({0.0, 0.0}, 1.0, -2.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(moveToPoint)
  {
    vladykin::Rectangle rectangle({0.0, 0.0}, 10.0, 20.0);
    rectangle.move({4.0, 4.0});
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 10.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 20.0,tolerance);
    BOOST_CHECK_CLOSE(rectangle.getArea(), 200.0, tolerance);
  }

  BOOST_AUTO_TEST_CASE(moveXY)
  {
    vladykin::Rectangle rectangle({0.0, 0.0}, 10.0, 20.0);
    rectangle.move(4.0, 4.0);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 10.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 20.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getArea(), 200.0, tolerance);
  }

  BOOST_AUTO_TEST_CASE(scale)
  {
    vladykin::Rectangle rectangle({0.0, 0.0}, 10.0, 20.0);
    rectangle.scale(2.0);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().width, 20.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getFrameRect().height, 40.0, tolerance);
    BOOST_CHECK_CLOSE(rectangle.getArea(), 800.0, tolerance);
    BOOST_CHECK_THROW(rectangle.scale(-1.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(circleTests)

  BOOST_AUTO_TEST_CASE(Constructor)
  {
    BOOST_CHECK_THROW(vladykin::Circle circle({0.0, 0.0}, -5.0),std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(moveToPoint)
  {
    vladykin::Circle circle({0.0, 0.0}, 5.0);
    double area = circle.getArea();
    circle.move({1.0, 1.0});
    BOOST_CHECK_CLOSE(circle.getFrameRect().width / 2.0, 5.0, tolerance);
    BOOST_CHECK_CLOSE(circle.getArea(), area, tolerance);
  }

  BOOST_AUTO_TEST_CASE(moveXY)
  {
    vladykin::Circle circle({0.0, 0.0}, 5.0);
    double area = circle.getArea();
    circle.move(5.0, 5.0);
    BOOST_CHECK_CLOSE(circle.getFrameRect().width / 2.0, 5.0, tolerance);
    BOOST_CHECK_CLOSE(circle.getArea(), area, tolerance);
  }

  BOOST_AUTO_TEST_CASE(scale)
  {
    vladykin::Circle circle({0.0, 0.0}, 5.0);
    double area = circle.getArea();
    double coeff = 2.0;
    circle.scale(coeff);
    BOOST_CHECK_CLOSE(circle.getFrameRect().width / 2.0, 10.0, tolerance);
    BOOST_CHECK_CLOSE(circle.getArea(), coeff*coeff*area, tolerance);
    BOOST_CHECK_THROW(circle.scale(-2.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(triangleTests)

  BOOST_AUTO_TEST_CASE(Constructor)
  {
    BOOST_CHECK_THROW(vladykin::Triangle triangle({1.0, 2}, {1.0, 3}, {1.0, 4}), std::invalid_argument);
    BOOST_CHECK_THROW(vladykin::Triangle triangle({2, 1.0}, {3.0, 1.0}, {4.0, 1.0}), std::invalid_argument);
  }
  BOOST_AUTO_TEST_CASE(moveToPoint)
  {
    vladykin::Triangle triangle({0.0, -3.0}, {0.0, 3.0}, {3.0, 0.0});
    const double area = triangle.getArea();
    vladykin::rectangle_t rect(triangle.getFrameRect());
    triangle.move({1.0, 1.0});
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, rect.width, tolerance);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, rect.height, tolerance);
    BOOST_CHECK_CLOSE(triangle.getArea(), area, tolerance);
  }

  BOOST_AUTO_TEST_CASE(moveXY)
  {
    vladykin::Triangle triangle({ 0.0, -3.0 }, { 0.0, 3.0 }, { 3.0, 0.0 });
    const double area = triangle.getArea();
    vladykin::rectangle_t rect(triangle.getFrameRect());
    triangle.move(3.0, 3.0);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, rect.width, tolerance);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, rect.height, tolerance);
    BOOST_CHECK_CLOSE(triangle.getArea(), area, tolerance);
  }

  BOOST_AUTO_TEST_CASE(scale)
  {
    vladykin::Triangle triangle({ 0.0, -3.0 }, { 0.0, 3.0 }, { 3.0, 0.0 });
    const double area = triangle.getArea();
    vladykin::rectangle_t rect(triangle.getFrameRect());
    const double coeff = 2.0;
    triangle.scale(coeff);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, rect.width * coeff, tolerance);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, rect.height * coeff, tolerance);
    BOOST_CHECK_CLOSE(triangle.getArea(), coeff*coeff*area, tolerance);
    BOOST_CHECK_THROW(triangle.scale(-3.0), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
