#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

int main()
{
  try
  {
    std::shared_ptr<vladykin::Shape> rect = std::make_shared<vladykin::Rectangle>(vladykin::Rectangle({ 3.0, 2.0 }, 5.0, 7.0));
    std::shared_ptr<vladykin::Shape> circ = std::make_shared<vladykin::Circle>(vladykin::Circle({ -2.0, 2.0 }, 5.0));
    std::shared_ptr<vladykin::Shape> tria = std::make_shared<vladykin::Triangle>(vladykin::Triangle({ 2.0, 3.0 }, { -2.0 , 3.0 }, {0.0, -1.0}));
    vladykin::CompositeShape compShape(rect);
    compShape.addShape(circ);
    compShape.addShape(tria);
    std::cout << "Composite shape area is " << compShape.getArea() << std::endl;
    compShape.showSize();
    std::cout << "Width of frame rectangle is " << compShape.getFrameRect().width << std::endl;
    std::cout << "Height of frame rectangle is " << compShape.getFrameRect().height << std::endl;
    compShape.scale(2.0);
    std::cout << "After scaling: " << std::endl;
    std::cout << "Composite shape area is " << compShape.getArea() << std::endl;
    compShape.move({ 10.0, 10.0 });
    std::cout << "The center of the Composite Shape was moved." << std::endl;
    compShape.showPos();
    std::cout << "Width of frame rectangle is " << compShape.getFrameRect().width << std::endl;
    std::cout << "Height of frame rectangle is " << compShape.getFrameRect().height << std::endl;
    compShape.move(-10.0, -10.0);
    std::cout << "The center of the Composite Shape was moved." << std::endl;
    compShape.showPos();
    std::cout << "Width of frame rectangle is " << compShape.getFrameRect().width << std::endl;
    std::cout << "Height of frame rectangle is " << compShape.getFrameRect().height << std::endl;
    compShape.deleteShape(0);
    std::cout << "The first figure was deleted. " << std::endl;
    std::cout << "Composite shape area is " << compShape.getArea() << std::endl;
    compShape.showSize();
    std::cout << "Width of frame rectangle is " << compShape.getFrameRect().width << std::endl;
    std::cout << "Height of frame rectangle is " << compShape.getFrameRect().height << std::endl;
  }
  catch (std::invalid_argument & error)
  {
    std::cerr << error.what() << std::endl;
    return 1;
  }
  return 0;
}

