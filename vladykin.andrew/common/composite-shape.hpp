#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"
#include <memory>
#include <iostream>

namespace vladykin
{
  class CompositeShape : public Shape
  {
  public:
     CompositeShape();
     CompositeShape(const CompositeShape &obj);
     CompositeShape(CompositeShape &&obj);
     CompositeShape(const std::shared_ptr<Shape> &obj);
     CompositeShape &operator = (const CompositeShape &obj);
     CompositeShape &operator = (CompositeShape &&obj);
     std::shared_ptr<Shape> &operator [] (const size_t index) const;
     void addShape(const std::shared_ptr<Shape> &obj);
     void deleteShape(const size_t index);
     void showPos() const override;
     void showSize() const override;
     void move(const double dx, const double dy) override;
     void move(const point_t &newPos) override;
     void scale(const double coeff) override;
     void rotate(const double angle) override;
     double getArea() const override;
     rectangle_t getFrameRect() const override;
     size_t getSize() const;
  private:
    std::unique_ptr<std::shared_ptr<Shape>[]> shapes_;
    size_t size_;
  };
}

#endif
