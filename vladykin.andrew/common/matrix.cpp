#include "matrix.hpp"
#include <iostream>
#include <cmath>

vladykin::Matrix::Matrix() :
  matrix_(nullptr),
  rows_(0),
  columns_(0)
{
}

vladykin::Matrix::Matrix(const std::shared_ptr<Shape> &obj) :
  matrix_(new std::shared_ptr<Shape>[1]),
  rows_(1),
  columns_(1)
{
  if (obj == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }
  matrix_[0] = obj;
}

vladykin::Matrix::Matrix(const CompositeShape &obj) :
  Matrix()
{
  addCompShape(obj);
}

vladykin::Matrix::Matrix(const Matrix &matrix) :
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  std::unique_ptr <std::shared_ptr<Shape>[]> newMatrix(new std::shared_ptr<Shape>[rows_ * columns_]);
  for (size_t i = 0; i < (rows_ * columns_); i++)
  {
    newMatrix[i] = matrix.matrix_[i];
  }
  matrix_.swap(newMatrix);
}

vladykin::Matrix::Matrix(Matrix &&matrix) :
  rows_(matrix.rows_),
  columns_(matrix.columns_)
{
  matrix_ = std::move(matrix.matrix_);
  matrix.matrix_ = (nullptr);
  matrix.rows_ = 0;
  matrix.columns_ = 0;
}

vladykin::Matrix & vladykin::Matrix::operator=(const Matrix &matrix)
{
  if (this != &matrix)
  {
    rows_ = matrix.rows_;
    columns_ = matrix.columns_;
    std::unique_ptr <std::shared_ptr<Shape>[]> newMatrix(new std::shared_ptr<Shape>[rows_ * columns_]);

    for (size_t i = 0; i < (rows_ * columns_); i++)
    {
      newMatrix[i] = matrix.matrix_[i];
    }
    matrix_.swap(newMatrix);
  }
  return *this;
}

vladykin::Matrix & vladykin::Matrix::operator=(Matrix &&matrix)
{
  if (this != &matrix)
  {
    rows_ = matrix.rows_;
    columns_ = matrix.columns_;
    matrix_ = std::move(matrix.matrix_);
    matrix.matrix_ = (nullptr);
    matrix.rows_ = 0;
    matrix.columns_ = 0;
  }
  return *this;
}


std::unique_ptr <std::shared_ptr<vladykin::Shape>[]> vladykin::Matrix::operator[](const size_t index) const
{
  if (index >= rows_)
  {
    throw std::invalid_argument("index out of range");
  }
  std::unique_ptr<std::shared_ptr<vladykin::Shape>[]> layer(
    new std::shared_ptr<vladykin::Shape>[columns_]);
  for (size_t i = 0; i < columns_; i++)
  {
    layer[i] = matrix_[index * columns_ + i];
  }
  return layer;
}


void vladykin::Matrix::addShape(const std::shared_ptr<Shape> &shape)
{
  if (shape == nullptr)
  {
    throw std::invalid_argument("invalid pointer");
  }
  size_t i = rows_ * columns_;
  size_t desired_row = 1;

  while (i > 0)
  {
    i--;
    if (checkOverlap(matrix_[i], shape))
    {
      desired_row = i / columns_ + 2;
    }
  }

  size_t rows_temp = rows_;
  size_t columns_temp = columns_;
  size_t free_columns = 0;

  if (desired_row > rows_)
  {
    rows_temp++;
    free_columns = columns_;
  }
  else
  {
    size_t j = (desired_row - 1) * columns_;
    while (j < (desired_row * columns_))
    {
      if (matrix_[j] == nullptr)
      {
        free_columns++;
      }
      j++;
    }

    if (free_columns == 0)
    {
      columns_temp++;
      free_columns = 1;
    }
  }

  std::unique_ptr <std::shared_ptr<Shape>[]> matrix_temp(new std::shared_ptr<Shape>[rows_temp * columns_temp]);

  for (size_t i = 0; i < rows_temp; i++)
  {
    for (size_t j = 0; j < columns_temp; j++)
    {
      if (i >= rows_ || j >= columns_)
      {
        matrix_temp[i * columns_temp + j] = nullptr;
        continue;
      }
      matrix_temp[i * columns_temp + j] = matrix_[i * columns_ + j];
    }
  }

  matrix_temp[desired_row * columns_temp - free_columns] = shape;

  matrix_.swap(matrix_temp);
  rows_ = rows_temp;
  columns_ = columns_temp;
}


size_t vladykin::Matrix::getColumns() const
{
  return columns_;
}

size_t vladykin::Matrix::getRows() const
{
  return rows_;
}

void vladykin::Matrix::showSize() const
{
  size_t i = 0;
  size_t j = 1;
  size_t k = 1;
  std::cout << "Layer - " << k << std::endl;
  while (i < rows_*columns_) 
  {
    if (j <= columns_) 
    {
      std::cout << "Column - " << j++ << std::endl;
    }
    else
    { 
      std::cout << "Layer - " << ++k << std::endl;
      std::cout << "Column - 1 "  << std::endl;
      j = 2;  
    }
    if (matrix_[i] != nullptr)
    {
      matrix_[i]->showPos();
      matrix_[i]->showSize();
      std::cout << std::endl;
    }
    else
    {
      std::cout << "Empty " << std::endl << std::endl;
    }
    i++;
  }
}

void vladykin::Matrix::addCompShape(const CompositeShape &composite)
{
  for (size_t i = 0; i < composite.getSize(); i++)
  {
    addShape(composite[i]);
  }
}

bool vladykin::Matrix::checkOverlap(const std::shared_ptr<Shape> &shape_1, const std::shared_ptr<Shape> &shape_2) const
{
  if (shape_1 == nullptr || shape_2 == nullptr)
  {
    return false;
  }

  rectangle_t shape_1_frame_rect = shape_1->getFrameRect();
  rectangle_t shape_2_frame_rect = shape_2->getFrameRect();

  return ((fabs(shape_1_frame_rect.pos.x - shape_2_frame_rect.pos.x)
    < ((shape_1_frame_rect.width / 2) + (shape_2_frame_rect.width / 2)))
    && ((fabs(shape_1_frame_rect.pos.y - shape_2_frame_rect.pos.y)
      < ((shape_1_frame_rect.height / 2) + (shape_2_frame_rect.height / 2)))));
}
