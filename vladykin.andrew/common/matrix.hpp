#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include "composite-shape.hpp"
#include <memory>

namespace vladykin
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const std::shared_ptr<Shape> &obj);
    Matrix(const CompositeShape &obj);
    Matrix(const Matrix &matrix);
    Matrix(Matrix &&matrix);
    Matrix &operator=(const Matrix &matrix);
    Matrix &operator=(Matrix &&matrix);
    std::unique_ptr <std::shared_ptr<Shape>[]> operator[](const size_t index) const;
    void addShape(const std::shared_ptr<Shape> &obj);
    size_t getColumns() const;
    size_t getRows() const;
    void showSize() const;
    void addCompShape(const CompositeShape &composite);
    bool checkOverlap(const std::shared_ptr<Shape> &shape_1, const std::shared_ptr<Shape> &shape_2) const;
  private:
    std::unique_ptr <std::shared_ptr<Shape>[]> matrix_;
    size_t rows_, columns_;
  };
};

#endif
