#define _USE_MATH_DEFINES
#include <iostream>
#include "polygon.hpp"
#include <algorithm>
#include <cmath>


vladykin::Polygon::Polygon(std::initializer_list<vladykin::point_t> vertices)
{
  if (vertices.size()<3)
  {
    throw std::invalid_argument("The polygon must consist of at least three points.");
  }
  count_ = vertices.size();
  vertices_ = std::unique_ptr<point_t[]>(new point_t[count_]);
  int i = 0;
  for (std::initializer_list<point_t>::const_iterator vertex = vertices.begin(); vertex != vertices.end(); ++vertex)
  {
    vertices_[i] = *vertex;
    i++;
  }
  if (getArea() <= 0.0)
  {
    throw std::invalid_argument("Wrong coordinates of vertices of a polygon.");
  }
  if (!checkConvex())
  {
    throw std::invalid_argument("Polygon is concave");
  }
  pos_ = { 0.0, 0.0 };
  for (size_t i = 0; i < count_; i++)
  {
    pos_.x += vertices_[i].x;
    pos_.y += vertices_[i].y;
  }
  pos_.x = pos_.x / count_;
  pos_.y = pos_.y / count_;
}

vladykin::point_t vladykin::Polygon::operator[](size_t index) const
{
  if (index >= count_)
  {
    throw std::invalid_argument("Index is out range");
  }
  return vertices_[index];
}

double vladykin::Polygon::getArea() const
{
  {
    double area = ((vertices_[count_ - 1].x + vertices_[0].x)*(vertices_[count_ - 1].y - vertices_[0].y));
    for (size_t i = 0; i < count_ - 1; i++)
    {
      area += ((vertices_[i].x + vertices_[i + 1].x)*(vertices_[i].y - vertices_[i + 1].y));
    }
    return abs(area) / 2.0;
  }
}

vladykin::rectangle_t vladykin::Polygon::getFrameRect() const
{
  double right = vertices_[0].x;
  double left = vertices_[0].x;
  double top = vertices_[0].y;
  double bottom = vertices_[0].y;
  for (size_t i = 1; i < count_; i++)
  {
    right = std::max(vertices_[i].x, right);
    left = std::min(vertices_[i].x, left);
    top = std::max(vertices_[i].y, top);
    bottom = std::min(vertices_[i].y, bottom);
  }
  rectangle_t frame;
  frame.pos.x = (right + left) / 2;
  frame.pos.y = (top + bottom) / 2;
  frame.width = right - left;
  frame.height = top - bottom;
  return frame;
}

void vladykin::Polygon::move(const point_t & pos)
{
  double dx = pos.x - pos_.x;
  double dy = pos.y - pos_.y;
  move(dx, dy);
}

void vladykin::Polygon::move(double dx, double dy)
{
  for (size_t i = 0; i < count_; ++i)
  {
    vertices_[i].x += dx;
    vertices_[i].y += dy;
  }
}

void vladykin::Polygon::showPos() const
{
  std::cout << "Coordinates of the center of the polygon: ";
  std::cout << "X = " << pos_.x << " Y = " << pos_.y << std::endl;
}

void vladykin::Polygon::showSize() const 
{
  size_t i;
  for (i = 0; i < count_; i++)
  {
    std::cout << "Pos " << i << " {" << vertices_[i].x << " ; " << vertices_[i].y << "}" << std::endl;
  }
}

void vladykin::Polygon::scale(double coeff)
{
  if (coeff <= 0)
  {
    throw std::invalid_argument("Coefficient <= 0");
  }
  for (size_t i = 0; i < count_; i++)
  {
    vertices_[i].x = (vertices_[i].x - pos_.x) * coeff + pos_.x;
    vertices_[i].y = (vertices_[i].y - pos_.y) * coeff + pos_.y;
  }
}

void vladykin::Polygon::rotate(double angle)
{
  const double sinAngle = sin(angle * M_PI / 180);
  const double cosAngle = cos(angle * M_PI / 180);
  for (size_t i = 0; i < count_; i++)
  {
    vertices_[i] = { pos_.x + cosAngle * (vertices_[i].x - pos_.x) - sinAngle * (vertices_[i].y - pos_.y),
      pos_.y + cosAngle * (vertices_[i].y - pos_.y) + sinAngle * (vertices_[i].x - pos_.x) };
  }
}

bool vladykin::Polygon::checkConvex() const
{
  for (size_t i = 0; i < (count_ - 1); i++)
  {
    double firstVertex = (vertices_[i + 1].y - vertices_[i].y) * (vertices_[0].x - vertices_[i].x)
      - (vertices_[i + 1].x - vertices_[i].x) * (vertices_[0].y - vertices_[i].y);
    double secondVertex = 0;
    for (size_t j = 1; j < count_; j++)
    {
      secondVertex = (vertices_[i + 1].y - vertices_[i].y) * (vertices_[j].x - vertices_[i].x)
        - (vertices_[i + 1].x - vertices_[i].x) * (vertices_[j].y - vertices_[i].y);
      if (firstVertex * secondVertex >= 0)
      {
        firstVertex = secondVertex;
      }
      else
      {
        return false;
      }
    }
  }
  return true;
}
