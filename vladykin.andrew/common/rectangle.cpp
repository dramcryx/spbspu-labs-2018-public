#define _USE_MATH_DEFINES

#include "rectangle.hpp"
#include <iostream>
#include <cmath>

vladykin::Rectangle::Rectangle(const point_t &pos, const double width, const double height):
  pos_(pos),
  width_(width),
  height_(height)
{
  if (width_ <= 0.0)
  {
    throw std::invalid_argument("Width <= 0 ");
  }
  if (height_ <= 0.0)
  {
    throw std::invalid_argument("Height <= 0");
  }
  vertex_[0] = { pos.x - width / 2 , pos.y - height / 2 };
  vertex_[1] = { pos.x - width / 2 , pos.y + height / 2 };
  vertex_[2] = { pos.x + width / 2 , pos.y + height / 2 };
  vertex_[3] = { pos.x + width / 2 , pos.y - height / 2 };

}

double vladykin::Rectangle::getArea() const
{
  return (width_ * height_);
}

vladykin::rectangle_t vladykin::Rectangle::getFrameRect() const
{
  double right = vertex_[0].x;
  double left = vertex_[0].x;
  double top = vertex_[0].y;
  double bottom = vertex_[0].y;
  for (size_t i = 1; i < 4; i++)
  {
    if (vertex_[i].x > right)
    {
      right = vertex_[i].x;
    }
    if (vertex_[i].x < left)
    {
      left = vertex_[i].x;
    }
    if (vertex_[i].y > top)
    {
      top = vertex_[i].y;
    }
    if (vertex_[i].y < bottom)
    {
      bottom = vertex_[i].y;
    }
  }
  return { (right - left), (top - bottom), {((right + left) / 2.0), ((top + bottom) / 2.0) } };
}

void vladykin::Rectangle::showSize() const
{
  std::cout << "The size of the rectangle: ";
  std::cout << "Width = " << width_ << " Height = " << height_ << std::endl;
}

void vladykin::Rectangle::showPos() const
{
  std::cout << "Coordinates of the center of the rectangle: ";
  std::cout << "X = " << pos_.x << " Y = " << pos_.y << std::endl;
}

void vladykin::Rectangle::move(const point_t &pos)
{
  for (size_t i = 0; i < 4; i++)
  {
    vertex_[i].x += pos.x - pos_.x;
    vertex_[i].y += pos.y - pos_.y;
  }
  pos_ = pos;
}

void vladykin::Rectangle::move(const double dx, const double dy)
{
  for (size_t i = 0; i < 4; i++)
  {
    vertex_[i].x += dx;
    vertex_[i].y += dy;
  }
  pos_.x += dx;
  pos_.y += dy;
}

void vladykin::Rectangle::scale(const double coeff)
{
  if (coeff < 0.0)
  {
    throw std::invalid_argument("Coefficient < 0");
  }
  width_ *= coeff;
  height_ *= coeff;
  vertex_[0] = { pos_.x + width_ / 2, pos_.y + height_ / 2 };
  vertex_[1] = { pos_.x - width_ / 2, pos_.y + height_ / 2 };
  vertex_[2] = { pos_.x - width_ / 2, pos_.y - height_ / 2 };
  vertex_[3] = { pos_.x + width_ / 2, pos_.y - height_ / 2 };
}

void vladykin::Rectangle::rotate(const double angle)
{
  double sinAngle = sin(angle * M_PI / 180);
  double cosAngle = cos(angle * M_PI / 180);
  for (int i = 0; i < 4; i++)
  {
    vertex_[i] = { pos_.x + (vertex_[i].x - pos_.x) * cosAngle
      - (vertex_[i].y - pos_.y) * sinAngle, pos_.y + (vertex_[i].x - pos_.x) * sinAngle
      + (vertex_[i].y - pos_.y) * cosAngle };
  }
}
