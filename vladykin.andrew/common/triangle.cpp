#define _USE_MATH_DEFINES

#include "triangle.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

vladykin::Triangle::Triangle(const point_t &a, const point_t &b, const point_t &c) :
  A_(a),
  B_(b),
  C_(c)
{
  if (getArea() == 0.0)
  {
    throw std::invalid_argument("Wrong coordinates of vertices of a triangle.");
  }
  pos_.x = (A_.x + B_.x + C_.x) / 3.0;
  pos_.y = (A_.y + B_.y + C_.y) / 3.0;
}

double vladykin::Triangle::getArea() const
{
  double area = (( (A_.x - C_.x)*(B_.y - C_.y) - (B_.x - C_.x)*(A_.y - C_.y) ) / 2.0);
  if (area < 0.0)
  {
    area *= -1.0;
  }
  return area;
}

vladykin::rectangle_t vladykin::Triangle::getFrameRect() const
{
  double minX = std::min(std::min(A_.x, B_.x), C_.x);
  double maxX = std::max(std::max(A_.x, B_.x), C_.x);
  double minY = std::min(std::min(A_.y, B_.y), C_.y);
  double maxY = std::max(std::max(A_.y, B_.y), C_.y);
  return { maxX - minX , maxY - minY , { (maxX - minX) / 2.0 , (maxY - minY) / 2.0 } };
}

void vladykin::Triangle::showSize() const
{
  std::cout << "Coordinates of vertices of a triangle: ";
  std::cout << "A(" << A_.x << ";" << A_.y << "), ";
  std::cout << "B(" << B_.x << ";" << B_.y << "), ";
  std::cout << "C(" << C_.x << ";" << C_.y << "). " << std::endl;
}

void vladykin::Triangle::showPos() const
{
  std::cout << "Coordinates of the center of the triangle: ";
  std::cout << "X = " << pos_.x << " Y = " << pos_.y << std::endl;
}

void vladykin::Triangle::move(const point_t &pos)
{
  A_.x += pos.x - pos_.x;
  A_.y += pos.y - pos_.y;
  B_.x += pos.x - pos_.x;
  B_.y += pos.y - pos_.y;
  C_.x += pos.x - pos_.x;
  C_.y += pos.y - pos_.y;
  pos_.x = pos.x;
  pos_.y = pos.y;
}

void vladykin::Triangle::move(const double dx, const double dy)
{
  A_.x += dx;
  A_.y += dy;
  B_.x += dx;
  B_.y += dy;
  C_.x += dx;
  C_.y += dy;
  pos_.x += dx;
  pos_.y += dy;
}

void vladykin::Triangle::scale(const double coeff)
{
  if (coeff <= 0.0)
  {
    throw std::invalid_argument("Coefficient <= 0");
  }
  A_ = { (A_.x - pos_.x) * coeff + pos_.x, (A_.y - pos_.y) * coeff + pos_.y };
  B_ = { (B_.x - pos_.x) * coeff + pos_.x, (B_.y - pos_.y) * coeff + pos_.y };
  C_ = { (C_.x - pos_.x) * coeff + pos_.x, (C_.y - pos_.y) * coeff + pos_.y };
}

void vladykin::Triangle::rotate(const double angle)
{
  double sinAngle = sin(angle * M_PI / 180);
  double cosAngle = cos(angle * M_PI / 180);
  A_= {pos_.x + (A_.x - pos_.x) * cosAngle - (A_.y - pos_.y) * sinAngle,
    pos_.y + (A_.x - pos_.x) * sinAngle + (A_.y - pos_.y) * cosAngle };
  B_ = { pos_.x + (B_.x - pos_.x) * cosAngle - (B_.y - pos_.y) * sinAngle,
    pos_.y + (B_.x - pos_.x) * sinAngle + (B_.y - pos_.y) * cosAngle };
  C_ = { pos_.x + (C_.x - pos_.x) * cosAngle - (C_.y - pos_.y) * sinAngle,
    pos_.y + (C_.x - pos_.x) * sinAngle + (C_.y - pos_.y) * cosAngle };
}
