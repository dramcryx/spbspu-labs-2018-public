#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <iostream>
#include <memory>
#include "shape.hpp"

namespace  vlasov
{
  class Matrix
  {
  public:
    Matrix(const std::shared_ptr<Shape> shape);
    Matrix();
    ~Matrix();

    Matrix(const Matrix & matrix);
    void addShape(const std::shared_ptr<Shape> newshape);
    int getLayerSize();
    int getShapeSize();
  private:
    int layerSize_;
    int shapeSize_;
    std::unique_ptr<std::shared_ptr<Shape>[]> shapeArr_;
    bool crossing(const std::shared_ptr<vlasov::Shape> shapeCurr, const std::shared_ptr<vlasov::Shape> shapeNew) ;

  };
}
#endif //MATRIX_HPP
