#include <stdexcept>
#include <cmath>
#include "rectangle.hpp"

using namespace vlasov;

vlasov::Rectangle::Rectangle(const rectangle_t widthHeightPos):
  width_(widthHeightPos.width),
  height_(widthHeightPos.height),
  center_(widthHeightPos.pos)
{
  if (width_ < 0.0 || height_ < 0.0)
  {
    throw std::invalid_argument("Check that width/height are positive numbers");
  }
  degree_ = 0;
};

double vlasov::Rectangle::getArea() const
{
  return height_ * width_;
};

rectangle_t vlasov::Rectangle::getFrameRect() const
{
  return {height_,width_,{center_.x,center_.y}};
};

void vlasov::Rectangle::move(const point_t &poss)
{
  center_=poss;
};

void vlasov::Rectangle::move(const double xOffset, const double yOffset)
{
  center_.x += xOffset;
  center_.y += yOffset;
};

void vlasov::Rectangle::scale(double coef_)
{
  if (coef_ > 0.0) {
    width_ *= coef_;
    height_ *= coef_;
  } else throw std::invalid_argument("Check that scaling number is >0");
};

void Rectangle::rotate(const double degree)
{
  if (degree_ < 0)
  {
    throw std::invalid_argument("Degree must be > 0");
  }
  degree_ +=degree;
  point_t up = {width_/2, height_/2};
  point_t down = {width_/2, -height_/2};
  double radians = degree_*M_PI/180;
  point_t edge[2] = {{0.0,0.0},{0.0,0.0}};
  edge[0].x = fabs(up.x*cos(radians) - up.y*sin(radians));
  edge[0].y = fabs(up.y*cos(radians) + up.x*sin(radians));
  edge[1].x = fabs(down.x*cos(radians) - down.y*sin(radians));
  edge[1].y = fabs(down.y*cos(radians) + down.x*sin(radians));
  if (edge[0].x < edge[1].x)
  {
    width_ = edge[1].x*2;
  }
  else
  {
    width_ = edge[0].x*2;
  }
  if (edge[0].y < edge[1].y)
  {
    height_ = edge[1].y*2;
  }
  else
  {
    height_ = edge[0].y*2;
  }
}
