#include <iostream>
#include <memory>

#include "composite-shape.hpp"
#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include "matrix.hpp"

using namespace vlasov;

void testShape(Shape &myShape)
{
    std::cout << "Area: " << myShape.getArea() << "\n";
    std::cout << "Get FrameRect.\n";
    std::cout << "height: " << myShape.getFrameRect().height << "\n";
    std::cout << "width: " << myShape.getFrameRect().width << "\n";
    std::cout << "Ox: "<< myShape.getFrameRect().pos.x << "\n";
    std::cout << "Oy: "<< myShape.getFrameRect().pos.y << "\n";

    myShape.rotate(90);
    std::cout << "Rotate\n";
    std::cout << "Area: " << myShape.getArea() << "\n";
    std::cout << "height: " << myShape.getFrameRect().height << "\n";
    std::cout << "width: " << myShape.getFrameRect().width << "\n";
    std::cout << "Ox: " << myShape.getFrameRect().pos.x << "\n";
    std::cout << "Oy: " << myShape.getFrameRect().pos.y << "\n";
}

int main()
{
    try {
        Rectangle myRect({ {11, 23}, 12, 45});
        Circle myCirc(12, {4, 21});

        Rectangle compRect({ {16, 67}, 31, 33});
        Circle compCirc(2, {11, 32});
        CompositeShape myComp;
        auto ptrRectComp = std::make_shared<Rectangle>(compRect);
        auto ptrCircComp = std::make_shared<Circle>(compCirc);
        myComp.add(ptrCircComp);
        myComp.add(ptrRectComp);

        std::cout << "\nRectangle:\n\n";
        testShape(myRect);

        std::cout << "\nCircle:\n\n";
        testShape(myCirc);

        std::cout << "\nCompositeShape:\n\n";
        testShape(myComp);

        auto ptrRect = std::make_shared<Rectangle>(myRect);
        auto ptrCirc = std::make_shared<Circle>(myCirc);

        Matrix myMatrix;
        myMatrix.add(ptrRect);
        myMatrix.add(ptrCirc);
        std::cout << "number of Layers:" << myMatrix.getNumber();
        std::cout << "\nnumber of elemnts in Layer:" << myMatrix.getSize() << "\n";

    }
    catch(const std::invalid_argument &e)
    {
        std::cerr << "error: invalid argument(s): " << e.what();
        return 1;
    }
    catch(const std::bad_alloc &e)
    {
        std::cerr << "error: allocation of memory" << e.what();
        return 1;
    }
    catch(const std::exception &e)
    {
        std::cerr << "Unknown error" << e.what();
    }

}
