#define BOOST_TEST_MAIN
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace vlasov;

const double EPSILON = 1e-6;

BOOST_AUTO_TEST_SUITE(RectangleSuite)

  BOOST_AUTO_TEST_CASE(Rotate)
  {
    vlasov::Rectangle rectangle({ 19.64, 8.35 }, 7.0, 16.0 );
    rectangle.rotate(90);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().width, 16.0, EPSILON);
    BOOST_CHECK_CLOSE_FRACTION(rectangle.getFrameRect().height, 7.0, EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CircleSuite)

  BOOST_AUTO_TEST_CASE(Rotate)
  {
      Circle circle{5.0, {2.0, 3.0}};

      circle.rotate(55.0);

      rectangle_t frame = circle.getFrameRect();

      BOOST_CHECK_EQUAL(frame.pos.x, 2.0);
      BOOST_CHECK_EQUAL(frame.pos.y, 3.0);
      BOOST_CHECK_CLOSE(frame.width, 10.0, EPSILON);
      BOOST_CHECK_CLOSE(frame.height, 10.0, EPSILON);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompostieShapeSuite)

  BOOST_AUTO_TEST_CASE(Rotate)
  {
      CompositeShape cs;
      const rectangle_t re = { {2.0, 5.0}, 7.0, 8.0 };
      std::shared_ptr<vlasov::Rectangle> rect = std::make_shared<vlasov::Rectangle>(re.pos, re.width, re.height);
      cs.add(rect);
      point_t c = {10.0, 20.0};
      std::shared_ptr< Circle > circle = std::make_shared< Circle >(9.0, c);
      cs.add(circle);

      rectangle_t beforeRotate = cs.getFrameRect();

      cs.rotate(90.0);

      rectangle_t afterRotate = cs.getFrameRect();

      BOOST_CHECK_CLOSE(beforeRotate.pos.x, afterRotate.pos.x, EPSILON);
      BOOST_CHECK_CLOSE(beforeRotate.pos.y, afterRotate.pos.y, EPSILON);
      BOOST_CHECK_CLOSE(beforeRotate.height, afterRotate.width, EPSILON);
      BOOST_CHECK_CLOSE(beforeRotate.width, afterRotate.height, EPSILON);
  }


BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixSuite)

  BOOST_AUTO_TEST_CASE(CopyConstructor)
  {
      Matrix matrix2;
      const rectangle_t re = {{12.0, 15.0}, 7.0, 8.0 };
      std::shared_ptr< vlasov::Rectangle > rect = std::make_shared< vlasov::Rectangle >(re.pos, re.width, re.height);
      matrix2.add(rect);
      point_t c = {10.0, 20.0};
      std::shared_ptr< Circle > circle = std::make_shared< Circle >(9.0, c);
      matrix2.add(circle);

      Matrix matrix1{matrix2};

      BOOST_CHECK_EQUAL(matrix1.getNumber(), matrix2.getNumber());
      BOOST_CHECK_EQUAL(matrix1[0][0], matrix2[0][0]);
      BOOST_CHECK_EQUAL(matrix1[1][0], matrix2[1][0]);
  }

  BOOST_AUTO_TEST_CASE(MoveConstrucotr)
  {
      Matrix matrix2;
      const rectangle_t re = {{12.0, 15.0}, 7.0, 8.0 };
      std::shared_ptr< vlasov::Rectangle > rect = std::make_shared< vlasov::Rectangle >(re.pos, re.width, re.height);
      matrix2.add(rect);
      point_t c = {10.0, 20.0};
      std::shared_ptr< Circle > circle = std::make_shared< Circle >(9.0, c);
      matrix2.add(circle);

      Matrix matrix1{std::move(matrix2)};

      BOOST_CHECK_EQUAL(matrix1.getNumber(), 2);
      BOOST_CHECK_EQUAL(matrix1.getSize(), 1);
      BOOST_CHECK_EQUAL(matrix2.getNumber(), 0);
      BOOST_CHECK_EQUAL(matrix2.getSize(), 0);

      BOOST_CHECK_EQUAL(matrix1[0][0], rect);
      BOOST_CHECK_EQUAL(matrix1[1][0], circle);

      BOOST_CHECK_THROW(matrix2[0], std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(AssignmentCopy)
  {
      Matrix matrix2, matrix1;

      const rectangle_t re = { {12.0, 15.0}, 7.0, 8.0 };
      std::shared_ptr< vlasov::Rectangle > rect = std::make_shared< vlasov::Rectangle >(re.pos, re.width, re.height);
      matrix2.add(rect);
      point_t c = {10.0, 20.0};
      std::shared_ptr< Circle > circle = std::make_shared< Circle >(9.0, c);
      matrix2.add(circle);

      matrix1 = matrix2;

      BOOST_CHECK_EQUAL(matrix1.getNumber(), matrix2.getNumber());
      BOOST_CHECK_EQUAL(matrix1[0][0], matrix2[0][0]);
      BOOST_CHECK_EQUAL(matrix1[1][0], matrix2[1][0]);
  }

  BOOST_AUTO_TEST_CASE(AssignmentMove)
  {
    Matrix matrix2, matrix1;

    const rectangle_t re = { {12.0, 15.0}, 7.0, 8.0 };
    std::shared_ptr< vlasov::Rectangle > rect = std::make_shared< vlasov::Rectangle >(re.pos, re.width, re.height);
    matrix2.add(rect);
    point_t c = {10.0, 20.0};
    std::shared_ptr< Circle > circle = std::make_shared< Circle >(9.0, c);
    matrix2.add(circle);

      matrix1 = std::move(matrix2);

      BOOST_CHECK_EQUAL(matrix1.getNumber(), 2);
      BOOST_CHECK_EQUAL(matrix1.getSize(), 1);
      BOOST_CHECK_EQUAL(matrix2.getNumber(), 0);
      BOOST_CHECK_EQUAL(matrix2.getSize(), 0);

      BOOST_CHECK_EQUAL(matrix1[0][0], rect);
      BOOST_CHECK_EQUAL(matrix1[1][0], circle);

      BOOST_CHECK_THROW(matrix2[0], std::out_of_range);
  }

  BOOST_AUTO_TEST_CASE(InvalidIndexindOperator)
  {
      Matrix matrix;
      BOOST_CHECK_THROW(matrix[0], std::out_of_range);
      const rectangle_t re = { {12.0, 15.0}, 7.0, 8.0 };
      std::shared_ptr< vlasov::Rectangle > rect = std::make_shared< vlasov::Rectangle >(re.pos, re.width, re.height);
      matrix.add(rect);

      BOOST_CHECK_THROW(matrix[1], std::out_of_range);
  }

BOOST_AUTO_TEST_SUITE_END()
