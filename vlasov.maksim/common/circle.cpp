#include "circle.hpp"
#include <iostream>
#include <cmath>

using namespace vlasov;

Circle::Circle(double radius, point_t position) :
  radius_(radius),
  angle_(0)
{
  pos = position; //using point_t Shape::pos
  if (radius_ < 0.0) {
    throw std::invalid_argument ("WARNING: invalid parameter of Circle\n");
  }
}

double Circle::getArea() const {
  return radius_*radius_*M_PI;
}

rectangle_t Circle::getFrameRect() const {
  return{pos, 2 * radius_, 2 * radius_};
}

void Circle::scale(double multiplier) {
  if (multiplier< 0.0){
    throw std::invalid_argument("WARNING: signed scaling is not suitable!\n");
  }
  radius_*=multiplier;
}

void Circle::rotate(double angle) {
  angle_ += angle;
}
