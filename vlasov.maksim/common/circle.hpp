#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "shape.hpp"

namespace vlasov {
  class Circle : public Shape {
    public:
    Circle(double radius, point_t position);

    double getArea() const override;

    rectangle_t getFrameRect() const override;

    void scale(double multiplier) override;

    void rotate(double angle) override;

    private:
    double radius_;
    double angle_;
  };
}
#endif 
