#include <iostream>
#include <memory>
#include <algorithm>
#include "matrix.hpp"

vlasov::Matrix::Matrix():
  mShapes_(nullptr),
  layersNumber_(0),
  layerSize_(0)
{}

vlasov::Matrix::Matrix(const std::shared_ptr<Shape> inShape):
  mShapes_(nullptr),
  layersNumber_(0),
  layerSize_(0)
{
  if (inShape == nullptr)
  {
    throw std::invalid_argument("Adding a null pointer?");
  }
  add(inShape);
}

vlasov::Matrix::Matrix(const Matrix & inMatrix):
  mShapes_(new std::shared_ptr<Shape>[inMatrix.layerSize_ * inMatrix.layersNumber_]),
  layersNumber_(inMatrix.layersNumber_),
  layerSize_(inMatrix.layerSize_)
{
  for (int i = 0; i < layersNumber_ * layerSize_; i++){
    mShapes_[i] = inMatrix.mShapes_[i];
  }
}

vlasov::Matrix::Matrix(Matrix && rhs) noexcept:
  mShapes_(nullptr),
  layersNumber_(rhs.layersNumber_),
  layerSize_(rhs.layerSize_)
{
  mShapes_.swap(rhs.mShapes_);
  rhs.layersNumber_ = 0;
  rhs.layerSize_= 0;
}

vlasov::Matrix &vlasov::Matrix::operator=(const vlasov::Matrix & rhs) {
  if (this == &rhs){
    return *this;
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> tmp
    (new std::shared_ptr<Shape>[rhs.layersNumber_ * rhs.layerSize_]);
  layersNumber_ = rhs.layersNumber_;
  layerSize_ = rhs.layerSize_;

  for (int i = 0; i < layersNumber_ ; i ++) {
    tmp[i] = rhs.mShapes_[i];
  }

  mShapes_.swap(tmp);
  return *this;
}

vlasov::Matrix &vlasov::Matrix::operator=(vlasov::Matrix &&rhs) noexcept{
  layersNumber_ = rhs.layersNumber_;
  layerSize_ = rhs.layerSize_;
  mShapes_.reset();
  mShapes_.swap(rhs.mShapes_);
  rhs.layersNumber_ = 0;
  rhs.layerSize_ = 0;
  return *this;
}

bool vlasov::Matrix::operator==(const vlasov::Matrix &rhs) const {
  if ((layersNumber_ == rhs.layersNumber_) || (layerSize_ == rhs.layerSize_))
  {
    return true;
  }
  else
  {
    bool equal = true;
    for (int i = 0; i < layersNumber_ * layerSize_; ++i)
    {
      if (!(mShapes_[i] == rhs.mShapes_[i]))
      {
        equal = false;
        break;
      }
    }
    if (equal)
    {
      return true;
    }
  }
  return false;
}

bool vlasov::Matrix::operator!=(const vlasov::Matrix &rhs) const {
  if ((layersNumber_ != rhs.layersNumber_) || (layerSize_ != rhs.layerSize_))
  {
    return true;
  }
  else
  {
    bool notEqual = false;
    for (int i = 0; i < layersNumber_ * layerSize_; ++i)
    {
      if (mShapes_[i] != rhs.mShapes_[i])
      {
        notEqual = true;
        break;
      }
    }
    if (notEqual)
    {
      return true;
    }
  }
  return false;
}


std::unique_ptr<std::shared_ptr<vlasov::Shape>[]> vlasov::Matrix::operator[](const int index) const {
  if (index >= layersNumber_ || index < 0) {
    throw std::out_of_range("Out of range!");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> layer(new std::shared_ptr<Shape>[layerSize_]);

  for (int i = 0; i < layerSize_; i++){
    layer[i] = mShapes_[index * layerSize_ + i];
  }

  return layer;
}

void vlasov::Matrix::add(const std::shared_ptr<vlasov::Shape> inShape) noexcept {
  if (layersNumber_ == 0)
  {
    std::unique_ptr<std::shared_ptr<Shape>[]> newElements(new std::shared_ptr<Shape>[(layersNumber_ + 1) * (layerSize_ + 1)]);
    layerSize_++;
    layersNumber_++;
    mShapes_.swap(newElements);
    mShapes_[0] = inShape;
  }
  else
  {
    bool addedShape = false;
    for (int i = 0; !addedShape ; ++i)
    {
      for (int j = 0; j < layerSize_; ++j)
      {
        if (!mShapes_[i * layerSize_ + j])
        {
          mShapes_[i * layerSize_ + j] = inShape;
          addedShape = true;
          break;
        }
        else
        {
          if (intersect(i * layerSize_ + j, inShape))
          {
            break;
          }
        }

        if (j == (layerSize_ - 1))
        {
          std::unique_ptr<std::shared_ptr<Shape>[]> newElements(
            new std::shared_ptr<Shape>[layersNumber_ * (layerSize_ + 1)]);
          layerSize_++;
          for (int n = 0; n < layersNumber_; ++n)
          {
            for (int m = 0; m < layerSize_ - 1; ++m)
            {
              newElements[n * layerSize_ + m] = mShapes_[n * (layerSize_ - 1) + m];
            }
            newElements[(n + 1) * layerSize_ - 1] = nullptr;
          }
          newElements[(i + 1) * layerSize_ - 1] = inShape;
          mShapes_.swap(newElements);
          addedShape = true;
          break;
        }
      }
      if ((i == (layersNumber_ - 1)) && !addedShape)
      {
        std::unique_ptr<std::shared_ptr<Shape>[]> newElements(
          new std::shared_ptr<Shape>[(layersNumber_ + 1) * layerSize_]);
        layersNumber_++;
        for (int n = 0; n < ((layersNumber_ - 1) * layerSize_); ++n)
        {
          newElements[n] = mShapes_[n];
        }
        for (int n = ((layersNumber_ - 1) * layerSize_) ; n < (layersNumber_ * layerSize_); ++n)
        {
          newElements[n] = nullptr;
        }
        newElements[(layersNumber_ - 1) * layerSize_ ] = inShape;
        mShapes_.swap(newElements);
        addedShape = true;
      }
    }
  }
}


bool vlasov::Matrix::intersect(const int index,
                               const std::shared_ptr<vlasov::Shape> &rhs) noexcept {
  rectangle_t inShape = rhs->getFrameRect();
  rectangle_t ownFrame = mShapes_[index]->getFrameRect();
  point_t inShapePoints[4] = {
    {inShape.pos.x - inShape.width / 2.0, inShape.pos.y + inShape.height / 2.0},
    {inShape.pos.x + inShape.width / 2.0, inShape.pos.y + inShape.height / 2.0},
    {inShape.pos.x + inShape.width / 2.0, inShape.pos.y - inShape.height / 2.0},
    {inShape.pos.x - inShape.width / 2.0, inShape.pos.y - inShape.height / 2.0}
  };

  point_t matrixPoints[4] = {
    {ownFrame.pos.x - ownFrame.width / 2.0, ownFrame.pos.y + ownFrame.height / 2.0},
    {ownFrame.pos.x + ownFrame.width / 2.0, ownFrame.pos.y + ownFrame.height / 2.0},
    {ownFrame.pos.x + ownFrame.width / 2.0, ownFrame.pos.y - ownFrame.height / 2.0},
    {ownFrame.pos.x - ownFrame.width / 2.0, ownFrame.pos.y - ownFrame.height / 2.0}
  };

  for (int i = 0; i < 4; ++i)
  {
    if (((inShapePoints[i].x >= matrixPoints[0].x) && (inShapePoints[i].x <= matrixPoints[2].x)
         && (inShapePoints[i].y >= matrixPoints[3].y) && (inShapePoints[i].y <= matrixPoints[1].y))
        || ((matrixPoints[i].x >= inShapePoints[0].x) && (matrixPoints[i].x <= inShapePoints[2].x)
            && (matrixPoints[i].y >= inShapePoints[3].y) && (matrixPoints[i].y <= inShapePoints[1].y)))
    {
      return true;
    }
  }
  return false;

}

int vlasov::Matrix::getNumber() const noexcept {
  return layersNumber_;
}

int vlasov::Matrix::getSize() const noexcept {
  return layerSize_;
}
