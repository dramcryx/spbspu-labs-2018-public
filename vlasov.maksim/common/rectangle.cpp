#include <iostream>
#include <cmath>
#include "rectangle.hpp"

using namespace vlasov;

Rectangle::Rectangle(point_t position, double width, double height) :
  width_(width),
  height_(height),
  angle_(0.0)
{
  pos = position;
  if (height_ < 0.0 || width_ < 0.0) {
    throw std::invalid_argument("WARNING: invalid parameter of Rectangle\n");
  }
};

rectangle_t Rectangle::getFrameRect() const {
  double newWidth = width_ * fabs(cos(angle_ / 180 * M_PI)) + height_ * fabs(sin(angle_ / 180 * M_PI));
  double newHeight = width_ * fabs(sin(angle_ / 180 * M_PI)) + height_ * fabs(cos(angle_ / 180 * M_PI));
  return {pos, newWidth, newHeight};
}

double Rectangle::getArea() const {
  return width_*height_;
}

void Rectangle::scale(double multiplier){
  if (multiplier < 0.0){
    throw std::invalid_argument("WARNING: signed scaling is not suitable!\n");
  }
  width_*=multiplier;
  height_*=multiplier;
}

void Rectangle::rotate(double angle) {
  angle_ += angle;
}
