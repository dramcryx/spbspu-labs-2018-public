#include <iostream>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using UP = std::unique_ptr < std::shared_ptr <volkov::Shape>[] >;
using SP = std::shared_ptr <volkov::Shape>;

void printFrameRect(const volkov::Shape & entity)
{
  const volkov::rectangle_t frame = entity.getFrameRect();
  std::cout << "Centre- " << frame.pos.x << ", " << frame.pos.y << std::endl;
  std::cout << "Height- " << frame.height << std::endl;
  std::cout << "Width- " << frame.width << std::endl;
}
void printArea(const volkov::Shape & entity)
{
  std::cout << "Area- " << entity.getArea() << std::endl;
}

int main()
{
  try{
    
    volkov::Rectangle rect({1,2}, 4, 2);

    std::cout << "Rectangle  " << std::endl;
    std::cout << "Rectangle options " << std::endl;
    printArea(rect);
    printFrameRect(rect);
    
    std::cout << "Increase 2 times " << std::endl;
    rect.scale(2);
    printArea(rect);
    printFrameRect(rect);
    
    std::cout << "Turn on 45 degrees " << std::endl;
    rect.rotate(45);
    printFrameRect(rect);
    printArea(rect);
    
    std::cout << "Move to x, y " << std::endl;
    rect.move(20, 35);
    printFrameRect(rect);
    
    std::cout << "Move to point " << std::endl;
    rect.move({20, 35});
    printFrameRect(rect);
    
    volkov::Circle circ({0,0}, 1);

    std::cout << "Circle " << std::endl;
    std::cout << "FrameRect options " << std::endl;
    printArea(circ);
    printFrameRect(circ);
    
    std::cout << "Increase 2 times " << std::endl;
    circ.scale(2);
    printArea(circ);
    printFrameRect(circ);
    
    std::cout << "Turn on 45 degrees " << std::endl;
    circ.rotate(45);
    printFrameRect(circ);
    
    std::cout << "Move to x, y " << std::endl;
    circ.move(15, 30);
    printFrameRect(circ);
    
    std::cout << "Move to point " << std::endl;
    circ.move({45, 5});
    printFrameRect(circ);

    SP circP = std::make_shared <volkov::Circle> (circ);
    SP rectP = std::make_shared <volkov::Rectangle> (rect);
    volkov::CompositeShape composite_shape(circP);


    std::cout <<"CompositeShape " << std::endl;
    std::cout << "CompositeShape parameters " << std::endl;
    printArea(composite_shape);
    printFrameRect(composite_shape);
    
    std::cout << "Add rectangle in compositeshape " << std::endl;
    composite_shape.addShape(rectP);
    printArea(composite_shape);
    printFrameRect(composite_shape);
    
    std::cout << "Increase compositeshape in 2 times " << std::endl;
    composite_shape.scale(2);
    printArea(composite_shape);
    printFrameRect(composite_shape);
    
    std::cout << "Turn on 90 degrees " << std::endl;
    composite_shape.rotate(90);
    printFrameRect(composite_shape);
    
    std::cout << "Remove circle from compositeshape " << std::endl;
    composite_shape.removeShape(1);
    printArea(composite_shape);
    printFrameRect(composite_shape);
    
    std::cout << "Delete all shapes " << std::endl;
    composite_shape.deleteShapes();
    printArea(composite_shape);
    printFrameRect(composite_shape);

    volkov::Rectangle rect2({7,7}, 4, 2);
    SP rectP2 = std::make_shared <volkov::Rectangle> (rect2);
    
    volkov::Matrix mat(circP);
    mat.addShape(rectP);
    mat.addShape(rectP2);

    std::cout << "Martix  " << std::endl;
    UP layer1 = mat[0];
    UP layer2 = mat[1];

    if (layer1[0] == circP)
    {
      std::cout << "First layer [1] - Circle" << std::endl;
    }
    if (layer1[1] == rectP2)
    {
      std::cout << "First layer [2] - Rectangle 2" << std::endl;
    }
    if (layer2[0] == rectP)
    {
      std::cout << "Second layer [1] - Rectangle 1" << std::endl;
    }
  } 
  catch(std::invalid_argument & d) 
  {
    std::cerr << d.what() << std::endl;
  } 
  catch(std::out_of_range & d) 
  {
    std::cerr << d.what() << std::endl;
  } 
  catch(...) 
  {
    std::cerr << "Exeption was found" << std::endl;
  }

  return 0;
}
