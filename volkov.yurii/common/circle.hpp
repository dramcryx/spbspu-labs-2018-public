#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include <iostream>
#include "shape.hpp"

namespace volkov
{
  class Circle: public volkov::Shape
  {
  public:
    Circle(const volkov::point_t & centre, const double radius);
    friend std::ostream & volkov::operator <<(std::ostream & out, const Circle & circle);
    double getRadius() const;
    double getArea() const override;
    double getAngle() const;
    volkov::rectangle_t getFrameRect() const override;
    void move(const double dx, const double dy) override;
    void move(const volkov::point_t & pos) override;
    void scale(const double f) override;
    void rotate(const double t) override;
  private:
    volkov::point_t centre_;
    double radius_;
    double angle_;
  };

  std::ostream & operator << (std::ostream & out, const Circle & circle);
}

#endif //CIRCLE_HPP

