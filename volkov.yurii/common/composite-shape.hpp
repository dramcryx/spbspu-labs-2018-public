#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP
#include <memory>
#include "shape.hpp"

namespace volkov
{
  using SP = std::shared_ptr <volkov::Shape>;
  
  class CompositeShape:
    public volkov::Shape
  {
  public:
    CompositeShape(const SP shape_n);
    CompositeShape(const volkov::CompositeShape &copy);
    CompositeShape(volkov::CompositeShape &&move) = default;
    CompositeShape & operator=(const volkov::CompositeShape &copy);
    CompositeShape & operator=(volkov::CompositeShape &&move);
    double getArea() const override;
    double getAngle() const;
    volkov::rectangle_t getFrameRect() const override;
    void move(const volkov::point_t & centre_n) override;
    void move(const double dx, const double dy) override;
    void scale(const double f) override;
    void rotate(const double t) override;
    void addShape(SP shape_n);
    SP getShape(int const shN) const;
    void removeShape(const int shN);
    void deleteShapes();
    int getSize() const;
  private:
    std::unique_ptr <SP[]> parts_;
    int size_;
    double angle_;
  };
}
#endif
