#include <memory>
#include <stdexcept>
#include "matrix.hpp"

using UP = std::unique_ptr < std::shared_ptr <volkov::Shape>[]>;
using SP = std::shared_ptr <volkov::Shape>;

volkov::Matrix::Matrix(const SP shape_n):
  parts_(nullptr),
  layersN_(0),
  laySize_(0)
{
  if (shape_n == nullptr)
  {
    throw std::invalid_argument("Null pointer ");
  }
  addShape(shape_n);
}

volkov::Matrix::Matrix(const volkov::Matrix & matrix_n):
  parts_(new SP[matrix_n.layersN_ * matrix_n.laySize_]),
  layersN_(matrix_n.layersN_), laySize_(matrix_n.laySize_)
{
  for (int i = 0; i< layersN_ * laySize_; ++i)
  {
    parts_[i] = matrix_n.parts_[i];
  }
}

volkov::Matrix::Matrix(volkov::Matrix && matrix_n):
  parts_(nullptr),
  layersN_(matrix_n.layersN_),
  laySize_(matrix_n.laySize_)
{
  parts_.swap(matrix_n.parts_);
  matrix_n.layersN_ = 0;
  matrix_n.laySize_ = 0;
}

volkov::Matrix & volkov::Matrix::operator=(const volkov::Matrix & matrix_n)
{
  if (this != &matrix_n)
  {
    UP new_parts_(new SP[matrix_n.layersN_ * matrix_n.laySize_]);
    layersN_ = matrix_n.layersN_;
    laySize_ = matrix_n.laySize_;               
    for (int i = 0; i< layersN_ * laySize_; ++i)
    {
        new_parts_[i] = matrix_n.parts_[i];
    }
    parts_.swap(new_parts_);
  }
  return *this;
}

volkov::Matrix & volkov::Matrix::operator=(volkov::Matrix && matrix_n)
{
  if (this != &matrix_n)
  {
    layersN_ = matrix_n.layersN_;
    laySize_ = matrix_n.laySize_;
    parts_.reset();
    parts_.swap(matrix_n.parts_);
    matrix_n.layersN_ = 0;
    matrix_n.laySize_ = 0;
  }
  return *this;
}

bool volkov::Matrix::operator==(const volkov::Matrix & matrix_n) const
{
  if ((this->layersN_ == matrix_n.layersN_) && (this->laySize_ == matrix_n.laySize_))
  {
    bool equal = true;
    for (int i = 0; i < layersN_ * laySize_; ++i)
    {
      if (!(this->parts_[i] == matrix_n.parts_[i]))
      {
        equal = false;
      }
    }
    if (equal)
    {
      return true;
    }
  }
  return false;
}

bool volkov::Matrix::operator!=(const volkov::Matrix & matrix_n) const
{
  return !(*this == matrix_n);
}

UP volkov::Matrix::operator[] (const int layerN) const
{
  if ((layerN < 0) || (layerN >= layersN_))
  {
    throw std::out_of_range("Invalid layer number!!");
  }
  UP layer_n(new SP[laySize_]);
  for (int i = 0; i < laySize_; ++i)
  {
    layer_n[i] = parts_[layerN * laySize_ + i];
  }
  return layer_n;
}

void volkov::Matrix::addShape(const std::shared_ptr<volkov::Shape> shape_n)
{
  if (layersN_ == 0)
  {
    ++laySize_;
    ++layersN_;
    UP new_parts_(new SP[layersN_ * laySize_]);
    parts_.swap(new_parts_);
    parts_[0] = shape_n;
  }
  else
  {
    bool addedShape = false;
    for (int i = 0; !addedShape; ++i)
    {
      for (int j =0; j <laySize_; ++j)
      {
        if (!parts_[i * laySize_ + j])
        {
          parts_[i * laySize_ + j] = shape_n;
          addedShape = true;
          break;
        }
        else
        {
          if (overLappingCheck(i * laySize_ +j, shape_n))
          {
            break;
          }
        }
        if (j == (laySize_ - 1))
        {
          laySize_++;
          UP new_parts_(new SP[layersN_ * laySize_]);
          for (int n = 0; n < layersN_; ++n)
          {
            for (int m = 0; m < laySize_ - 1; ++m)
            {
              new_parts_[n * laySize_ + m] = parts_[n * (laySize_ - 1) + m];
            }
            new_parts_[(n + 1) * laySize_ - 1] = nullptr;
          }
          new_parts_[(i +1) * laySize_ - 1] = shape_n;
          parts_.swap(new_parts_);
          addedShape = true;
          break;
        }
      }
      if ((i == (layersN_ - 1)) && !addedShape)
      {
        layersN_++;
        UP new_parts_(new SP[layersN_ * laySize_]);
        for (int n = 0; n < ((layersN_ - 1) * laySize_); ++n)
        {
          new_parts_[n] = parts_[n];
        }
        for (int n = ((layersN_ - 1) * laySize_); n < (layersN_ * laySize_); ++n)
        {
          new_parts_[n] = nullptr;
        }
        new_parts_[(layersN_ - 1) * laySize_] = shape_n;
        parts_.swap(new_parts_);
        addedShape = true;
      }
    }
  }
}

volkov::Matrix::Matrix(const volkov::CompositeShape &composite_shape):
  parts_(nullptr),
  layersN_(0),
  laySize_(0)
{
  if (composite_shape.getSize() == 0)
  {
    throw std::invalid_argument("Empty CompositeShape ");
  }
  for (int i = 0; i < composite_shape.getSize(); ++i)
  {
    addShape(composite_shape.getShape(i));
  }
}

bool volkov::Matrix::overLappingCheck(const int number, std::shared_ptr<volkov::Shape> shape_n) const
{
  volkov::rectangle_t shapeFrameRect1 = shape_n->getFrameRect();
  volkov::rectangle_t shapeFrameRect2 = parts_[number]->getFrameRect();
  point_t ShapePoints_n[4] = {
    {shapeFrameRect1.pos.x - shapeFrameRect1.width / 2, shapeFrameRect1.pos.y + shapeFrameRect1.height / 2},
    {shapeFrameRect1.pos.x + shapeFrameRect1.width / 2, shapeFrameRect1.pos.y + shapeFrameRect1.height / 2},
    {shapeFrameRect1.pos.x + shapeFrameRect1.width / 2, shapeFrameRect1.pos.y - shapeFrameRect1.height / 2},
    {shapeFrameRect1.pos.x - shapeFrameRect1.width / 2, shapeFrameRect1.pos.y - shapeFrameRect1.height / 2}
  };
  point_t MatrixPoints_n[4] = {
    {shapeFrameRect2.pos.x - shapeFrameRect2.width / 2, shapeFrameRect2.pos.y + shapeFrameRect2.height / 2},
    {shapeFrameRect2.pos.x + shapeFrameRect2.width / 2, shapeFrameRect2.pos.y + shapeFrameRect2.height / 2},
    {shapeFrameRect2.pos.x + shapeFrameRect2.width / 2, shapeFrameRect2.pos.y - shapeFrameRect2.height / 2},
    {shapeFrameRect2.pos.x - shapeFrameRect2.width / 2, shapeFrameRect2.pos.y - shapeFrameRect2.height / 2}
  };
  for (int i = 0; i < 4; ++i)
  {
    if (((ShapePoints_n[i].x >= MatrixPoints_n[0].x) && (ShapePoints_n[i].x <= MatrixPoints_n[2].x)
      && (ShapePoints_n[i].y >= MatrixPoints_n[3].y) && (ShapePoints_n[i].y <= MatrixPoints_n[1].y))
        || ((ShapePoints_n[i].x <= MatrixPoints_n[0].x) && (ShapePoints_n[i].x >= MatrixPoints_n[2].x)
          && (ShapePoints_n[i].y <= MatrixPoints_n[0].y) && (ShapePoints_n[i].y >= MatrixPoints_n[2].y)))
    {
      return true;
    }
  }
  return false;
}

int volkov::Matrix::getLayersN() const
{
  return layersN_;
}

int volkov::Matrix::getLaySize(const int layerN) const
{
  int b = 0;
  for (int i=layerN; i < layerN * laySize_; ++i)
  {
    if (parts_ != nullptr)
    {
      ++b;
    }
  }
  return b;
}

int volkov::Matrix::getMaxLaySize() const
{
  return laySize_;
}
