#ifndef MATRIX_HPP
#define MATRIX_HPP
#include <memory>
#include "shape.hpp"
#include "composite-shape.hpp"

namespace volkov
{
  class Matrix
  {
    public:
      Matrix(const std::shared_ptr<volkov::Shape> shape_n);
      Matrix(const Matrix & matrix);
      Matrix(Matrix && matrix_n);
      Matrix & operator=(const Matrix & matrix_n);
      Matrix & operator=(Matrix && matrix_n);
      bool operator==(const Matrix & matrix_n) const;
      bool operator!=(const Matrix & matrix_n) const;
      Matrix(const volkov::CompositeShape &composite_shape);
      std::unique_ptr< std::shared_ptr<volkov::Shape >[] > operator[](const int layerN) const;
      void addShape(const std::shared_ptr<volkov::Shape> shape_n);
      int getLayersN() const;
      int getMaxLaySize() const;
      int getLaySize(const int layerN) const;
    private:
      std::unique_ptr< std::shared_ptr<volkov::Shape>[] > parts_;
      int layersN_;
      int laySize_;
      bool overLappingCheck(const int number, std::shared_ptr<volkov::Shape> shape_n) const;
  };
}

#endif
