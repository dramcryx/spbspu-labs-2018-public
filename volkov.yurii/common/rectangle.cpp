#include <stdexcept>
#include <cassert>
#include <math.h>
#include "rectangle.hpp"

volkov::Rectangle::Rectangle(const volkov::point_t & centre, const double width, const double height):
  centre_(centre),
  width_(width),
  height_(height),
  angle_(0)
{
  if ((width < 0.0) || (height < 0.0))
  {
    throw std::invalid_argument("erroneus rectangle");
  }
}

double volkov::Rectangle::getWidth() const
{
  return width_;
}

double volkov::Rectangle::getHeight() const
{
  return height_;
}

double volkov::Rectangle::getArea() const
{
  return width_ * height_;
}

double volkov::Rectangle::getAngle() const
{
  return angle_;
}

volkov::rectangle_t volkov::Rectangle::getFrameRect() const
{
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  const double width = fabs(width_ * cosine) + fabs(height_ * sine);
  const double height =  fabs(height_ * cosine) + fabs(width_ * sine);
  return {centre_, width, height};
}

void volkov::Rectangle::move(const volkov::point_t & n_centre)
{
  centre_ = n_centre;
}

void volkov::Rectangle::move(const double dx, const double dy)
{
  centre_.x += dx;
  centre_.y += dy;
}

void volkov::Rectangle::scale(const double f)
{
  if (f < 0.0)
  {
    throw std::invalid_argument("f be sure >= 0");
  }
  else
  {
  width_ *= f;
  height_ *= f;
  }
}

void volkov::Rectangle::rotate(const double t)
{
  angle_ += t;
  if (fabs(angle_) >= 360)
  {
    angle_ = fmod(angle_, 360);
  }
}

std::ostream & volkov::operator <<(std::ostream & out, const Rectangle & rect)
{
  out << "Area: " << rect.getArea() << std::endl;
  out <<"pos (" << rect.getFrameRect().pos.x << ", " << rect.getFrameRect().pos.y << ")" << std::endl;
  out << "width: " << rect.getFrameRect().width << std::endl;
  out << "height: " << rect.getFrameRect().height;

  return out;
}
