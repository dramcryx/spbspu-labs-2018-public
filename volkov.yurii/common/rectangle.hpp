#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include <iostream>
#include "shape.hpp"

namespace volkov
{
  class Rectangle: 
    public volkov::Shape
  {
  public:
    Rectangle(const volkov::point_t & center, const double width, const double height);
    friend std::ostream & volkov::operator<<(std::ostream & out, const Rectangle & rect);
    double getWidth() const;
    double getHeight() const;
    double getAngle() const;
    double getArea() const override;
    volkov::rectangle_t getFrameRect() const override;
    void move(const double dx, const double dy) override;
    void move (const volkov::point_t & n_centre) override;
    void scale(const double f) override;
    void rotate(const double t) override;
  private:
    volkov::point_t centre_;
    double width_, height_;
    double angle_;
  };
  std::ostream & operator <<(std::ostream & out, const Rectangle & rect);
}

#endif // RECTANGLE_HPP
