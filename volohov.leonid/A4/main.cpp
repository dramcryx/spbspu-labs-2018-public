#include <iostream>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

using namespace volohov;

void printInfo(const volohov::Shape & object)
{
  std::cout << "Area: " << object.getArea() << std::endl;
  const volohov::rectangle_t frame = object.getFrameRect();
  std::cout << "FrameRect parametrs:" << std::endl;
  std::cout << "Width:" << frame.width << "; " << "Height:" << frame.height << std::endl;
  std::cout << "Center: " << frame.pos.x << ", " << frame.pos.y << std::endl;
  std::cout << "_______________________________________" << std::endl;
}

int main()
{
  try
  {
    volohov::Rectangle rectangle({1.0, 0.5}, 2.0, 1.0);
    std::cout << "Rectangle" << std::endl;
    printInfo(rectangle);
    volohov::Circle circle({5.0, 3.0}, 1.0);
    std::cout << "Circle" << std::endl;
    printInfo(circle);
    std::shared_ptr <volohov::Shape> rectanglePtr =
      std::make_shared <volohov::Rectangle>(rectangle);
    std::shared_ptr <volohov::Shape> circlePtr =
      std::make_shared <volohov::Circle>(circle);
    std::cout << "Create composite shape" << std::endl;
    volohov::CompositeShape shapes(rectanglePtr);
    printInfo(shapes);
    std::cout << "Amount of shapes = " << shapes.getCount() << std::endl;
    std::cout << "Add circle" << std::endl;
    shapes.addShape(circlePtr);
    printInfo(shapes);
    std::cout << "Amount of shapes = " << shapes.getCount() << std::endl;
    shapes.scale(2.0);
    std::cout << "Composite shape after scaling on 2:" << std::endl;
    printInfo(shapes);
    std::cout << "Amount of shapes = " << shapes.getCount() << std::endl;
    shapes.move({0.0, 0.0});
    std::cout << "Composite shape after moving" << std::endl;
    printInfo(shapes);
    std::cout << "Amount of shapes = " << shapes.getCount() << std::endl;
    rectangle.rotate(180);
    std::cout << "Rectangle after rotating on 180 degrees" << std::endl;
    printInfo(rectangle);
    circle.rotate(46);
    std::cout << "Circle after rotating on 46 degrees" << std::endl;
    printInfo(circle);
    shapes.rotate(90);
    std::cout << "Composite shape after rotating on 90 degrees" << std::endl;
    printInfo(shapes);
    shapes.removeAll();
    volohov::Circle circleM{{-2.0, -2.0}, 2.0};
    volohov::Rectangle rectangleM1{{-2.0, 0.0}, 2.0, 2.0};
    volohov::Rectangle rectangleM2{{1.0, 1.0}, 6.0, 3.0};
    volohov::Rectangle rectangleM3{{3.0, 1.0}, 2.0, 4.0};
    volohov::Rectangle rectangleM4{{3.0, 3.0}, 4.0, 4.0};
    std::shared_ptr< volohov::Shape > circlePtrM = std::make_shared<volohov::Circle >(circleM);
    std::shared_ptr< volohov::Shape > rectanglePtrM1 = std::make_shared<volohov::Rectangle >(rectangleM1);
    std::shared_ptr< volohov::Shape > rectanglePtrM2 = std::make_shared<volohov::Rectangle >(rectangleM2);
    std::shared_ptr< volohov::Shape > rectanglePtrM3 = std::make_shared<volohov::Rectangle >(rectangleM3);
    std::shared_ptr< volohov::Shape > rectanglePtrM4 = std::make_shared<volohov::Rectangle >(rectangleM4);
    volohov::CompositeShape shapes2(circlePtrM);
    shapes2.addShape(rectanglePtrM1);
    shapes2.addShape(rectanglePtrM2);
    shapes2.addShape(rectanglePtrM3);
    shapes2.addShape(rectanglePtrM4);
    volohov::Matrix matrix(shapes2);
    std::cout << "Matrix:" << std::endl;
    std::cout << "Amount of layers: " << matrix.getCount() << std::endl;
    std::cout << "Size of layers: " << matrix.getSize() << std::endl;
    if (matrix[0][0] == circlePtrM)
    {
      std::cout << " element in first layer is circle" << std::endl;
   }
    if (matrix[0][1] == rectanglePtrM3)
    {
      std::cout << " element in first layer is rectangle 3" << std::endl;
   }
    if (matrix[1][0] == rectanglePtrM1)
    {
      std::cout << "element in second layer is rectangle 1" << std::endl;
   }
    if (matrix[1][1] == rectanglePtrM4)
    {
      std::cout << "element in second layer is rectangle 4" << std::endl;
   }
    if (matrix[2][0] == rectanglePtrM2)
    {
      std::cout << "element in third layer is rectangle 2" << std::endl;
   }
    if (matrix[2][1] == nullptr)
    {
      std::cout << "element in third layer is nullptr" << std::endl;
   }
 }
  catch (std::invalid_argument & e)
  {
    std::cerr << e.what() << std::endl;
 }
  catch (std::out_of_range & e)
  {
    std::cerr << e.what() << std::endl;
 }
  catch (...)
  {
    std::cerr << "Exception occured" << std::endl;
 }
  return 0;
}
