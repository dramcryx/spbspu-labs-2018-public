#include "composite-shape.hpp"
#include <stdexcept>
#include <cmath>

using namespace volohov;

volohov::CompositeShape::CompositeShape(std::shared_ptr<volohov::Shape> shape):
  counter_(0),
  array_(nullptr),
  angle_(0.0)
{
  if (!shape)
  {
    throw std::invalid_argument("Empty shape");
  }
  else
  {
    addShape(shape);
  }
}

volohov::CompositeShape::CompositeShape(const volohov::CompositeShape & compositeshape):
  counter_(compositeshape.counter_),
  array_(new std::shared_ptr<volohov::Shape>[compositeshape.counter_])
{
  for (int i = 0; i < counter_; i ++)
  {
    array_[i] = compositeshape.array_[i];
  }
}

volohov::CompositeShape & CompositeShape::operator = (const volohov::CompositeShape & compositeshape)
{
  counter_ = compositeshape.counter_;
  array_.reset(new std::shared_ptr <volohov::Shape>[counter_]);
  for (int i = 0; i < counter_; i++)
  {
    array_[i] = compositeshape.array_[i];
  }
  return *this;
}

std::shared_ptr<volohov::Shape> volohov::CompositeShape::operator[](const int index) const
{
  if ((index < 0) || (index >= counter_))
  {
    throw std::out_of_range("Wrong index");
  }
  return array_[index];
}

double volohov::CompositeShape::getArea() const
{
  double area = 0;
  for (int i = 0; i < counter_; i++)
  {
    area = area + array_[i] -> getArea();
  }
  return(area);
}

volohov::rectangle_t volohov::CompositeShape::getFrameRect() const
{
  if (counter_ <= 0)
  {
    throw std::invalid_argument("Composite shape is empty");
  }
  else
  {
    volohov::rectangle_t rectangle = array_[0] -> getFrameRect();
    double left = rectangle.pos.x + (rectangle.width / 2);
    double top = rectangle.pos.y + (rectangle.height / 2);
    double right = rectangle.pos.x - (rectangle.width / 2);
    double bottom = rectangle.pos.y - (rectangle.height / 2);
    for (int i = 1; i < counter_; i++)
    {
      rectangle = array_[i] -> getFrameRect();
      left = std::max(left, rectangle.pos.x + (rectangle.width / 2));
      top = std::max(top, rectangle.pos.y + (rectangle.height / 2));
      right = std::min(right, rectangle.pos.x - (rectangle.width / 2));
      bottom = std::min(bottom, rectangle.pos.y - (rectangle.height / 2));
    }
    return {{(left + right) / 2, (top + bottom) / 2}, left - right, top - bottom};
  }
}

void volohov::CompositeShape::move(const volohov::point_t & newCenter)
{
  move(newCenter.x - getFrameRect().pos.x, newCenter.y - getFrameRect().pos.y);
}


void volohov::CompositeShape::move(const double dx,const double dy)
{
  for (int i = 0; i < counter_; i++)
  {
    array_[i] -> move(dx,dy);
  }
}

void volohov::CompositeShape::scale(const double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Invalid coefficient");
  }
  else
  {
    volohov::point_t center = getFrameRect().pos;
    for (int i = 0; i < counter_; i++)
    {
      volohov::point_t shape_center = array_[i] -> getFrameRect().pos;
      array_[i] -> move((coefficient - 1) * (shape_center.x - center.x),
        (coefficient - 1) * (shape_center.y - center.y));
      array_[i] -> scale(coefficient);
    }
  }
}

void volohov::CompositeShape::rotate(const double angle)
{
  volohov::point_t centre = CompositeShape::getFrameRect().pos;
  const double sine = sin((angle * M_PI) / 180);
  const double cosine = cos((angle * M_PI) / 180);
  for (int i = 0; i < counter_; i++)
  {
    volohov::point_t centre_shape = array_[i] -> getFrameRect().pos;
    array_[i] -> move({ centre.x + (centre_shape.x - centre.x) * cosine - (centre_shape.y - centre.y) * sine,
      centre.y + (centre_shape.y - centre.y) * cosine + (centre_shape.x - centre.x) * sine });
    array_[i] -> rotate(angle);
  }
  angle_ += angle;
  angle_ = fmod(angle_, 360.0);
}

void volohov::CompositeShape::addShape(const std::shared_ptr<volohov::Shape> newShape)
{
  if (!newShape)
  {
    throw std::invalid_argument("Empty shape");
  }
  for (int i = 0; i < counter_; i++)
  {
    if (newShape == array_[i])
    {
      throw std::invalid_argument("Wait. This shape is already added");
    }
  }
  std::unique_ptr<std::shared_ptr<volohov::Shape>[]> temp_array
    (new std::shared_ptr<volohov::Shape>[counter_ + 1]);
  for (int i = 0; i < counter_; i ++)
  {
    temp_array[i] = array_[i];
  }
  temp_array[counter_ ++] = newShape;
  array_.swap(temp_array);
}

void volohov::CompositeShape::removeShape(const int index)
{
  if ((index < 0) || (index >= counter_))
  {
    throw std::invalid_argument("Invalid index");
  }
  if (counter_ <= 0)
  {
    throw std::invalid_argument("Composite shape is empty");
  }
  if (counter_ == 1)
  {
    removeAll();
    return;
  }
  else
  {
    std::unique_ptr<std::shared_ptr<volohov::Shape>[]> temp_array
      (new std::shared_ptr<volohov::Shape>[counter_ - 1]);
    for (int i = 0; i < index; i ++)
    {
      temp_array[i] = array_[i];
    }
    for (int i = index; i < counter_ - 1; i ++)
    {
      temp_array[i] = array_[i + 1];
    }
    array_.swap(temp_array);
    --counter_;
  }
}

void volohov::CompositeShape::removeAll()
{
  array_.reset(nullptr);
  counter_ = 0;
}

int volohov::CompositeShape::getCount() const
{
  return counter_;
}
