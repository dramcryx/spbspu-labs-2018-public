#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <memory>
#include "shape.hpp"
#include "composite-shape.hpp"

namespace volohov
{
  class Matrix
  {
  public:
    Matrix(const CompositeShape shapes);
    Matrix(const Matrix & matrix);
    Matrix & operator = (const Matrix & matrix);
    std::unique_ptr <std::shared_ptr<Shape>[]>::pointer operator[](int index) const;
    void addShape(const std::shared_ptr<Shape> shape) noexcept;
    void addLayer(std::shared_ptr<Shape> shape);
    bool checkIntersection(std::shared_ptr<Shape> shape, const int index) const noexcept;
    int getCount();
    int getSize();
  private:
    int counter_;
    int size_;
    std::unique_ptr<std::shared_ptr<Shape>[]> shapes_;
  };
}

#endif //MATRIX_HPP
