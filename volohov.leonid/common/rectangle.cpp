#include "rectangle.hpp"
#include <iostream>
#include <cmath>

using namespace volohov;

volohov::Rectangle::Rectangle(const volohov::point_t & center, const double width, const double height):
  center_(center),
  width_(width),
  height_(height),
  angle_(0.0)
{
  if ((width_ < 0.0) || (height_ < 0.0))
  {
    throw std::invalid_argument("Wait. Invalid width or height");
  }
}

double volohov::Rectangle::getArea() const
{
  return width_ * height_;
}

volohov::rectangle_t volohov::Rectangle::getFrameRect() const
{
  const double sine = sin(angle_ * M_PI / 180);
  const double cosine = cos(angle_ * M_PI / 180);
  return {center_, height_ * abs(sine) + width_ * abs(cosine), height_ * abs(cosine) + width_ * abs(sine)};

  
}

void volohov::Rectangle::move(const volohov::point_t & newCenter)
{
  center_ = newCenter;
}

void volohov::Rectangle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}

void volohov::Rectangle::scale(const double coeff)
{
  if (coeff < 0.0)
  {
    throw std::invalid_argument("Wait. Invalid coefficient");
  }
  width_ *= coeff;
  height_ *= coeff;
}

void volohov::Rectangle::rotate(const double angle)
{
  angle_ += angle;
  if (abs(angle_) >= 360.0)
  {
    angle_ = std::fmod(angle_, 360.0);
  }
}
