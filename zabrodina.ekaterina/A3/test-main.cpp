#define BOOST_TEST_MAIN
//#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>

#include <stdexcept>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

const double accuracy = 0.0001;

BOOST_AUTO_TEST_SUITE(compositeShape_tests)

BOOST_AUTO_TEST_CASE(compositeShape_matching_shape)
{
  zabrodina::Circle circl({2.0, 1.34}, 10.6);
  zabrodina::CompositeShape compSh(&circl);
  BOOST_CHECK_CLOSE(compSh.getArea(), circl.getArea(), accuracy);
  BOOST_CHECK_CLOSE(compSh.getFrameRect().height, circl.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(compSh.getFrameRect().width, circl.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(compSh.getFrameRect().pos.x, 2.0, accuracy);
  BOOST_CHECK_CLOSE(compSh.getFrameRect().pos.y, 1.34, accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_getArea)
{
  zabrodina::Circle circl({3.2, 1.23}, 8.9);
  zabrodina::CompositeShape compSh(&circl);
  zabrodina::Triangle triangl({2.78, -2.3}, {6.45, 2.2}, { 10.4, -5.0});
  compSh.addShape(&triangl);
  zabrodina::Rectangle rectangl({2.32, -5.06}, 2, 4);
  compSh.addShape(&rectangl);
  BOOST_CHECK_CLOSE(compSh.getArea(), circl.getArea() + triangl.getArea() + rectangl.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_getArea_figuers_area_changed)
{
  zabrodina::Circle circl({0.23, -2.8}, 2.12);
  zabrodina::CompositeShape compSh(&circl);
  zabrodina::Rectangle rectangl({1.23, 5.34}, 12.0, 13.8);
  compSh.addShape(&rectangl);
  zabrodina::Triangle triangl({1.05, 2.46}, {2.34, -5.5 }, {4.5, 10.5});
  compSh.addShape(&triangl);
  BOOST_CHECK_CLOSE(circl.getArea(), compSh[0] -> getArea(), accuracy);
  BOOST_CHECK_CLOSE(rectangl.getArea(), compSh[1] -> getArea(), accuracy);
  BOOST_CHECK_CLOSE(triangl.getArea(), compSh[2] -> getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_move_OX_OY)
{
  zabrodina::Rectangle rectangl({0.34, -5.6}, 10.5, 5.6);
  zabrodina::CompositeShape compSh(&rectangl);
  zabrodina::Circle circl({2.12,3.23}, 10.5);
  compSh.addShape(&circl);
  const double width = compSh.getFrameRect().width;
  const double height = compSh.getFrameRect().height;
  const double area = compSh.getArea();
  compSh.move(4.23, 5.34);
  BOOST_CHECK_CLOSE(width, compSh.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, compSh.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, compSh.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_move_on)
{
  zabrodina::Circle circl({4.23, -5.34}, 4.34);
  zabrodina::CompositeShape compSh(&circl);
  zabrodina::Rectangle rectangl({0.34, -5.6}, 10.5, 5.6);
  compSh.addShape(&rectangl);
  const double width = compSh.getFrameRect().width;
  const double height = compSh.getFrameRect().height;
  const double area = compSh.getArea();
  compSh.move({2.0, 2.4});
  BOOST_CHECK_CLOSE(width, compSh.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, compSh.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, compSh.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_scale_area_1)
{
  zabrodina::Circle circl({0.5, -8.2}, 4.5);
  zabrodina::CompositeShape compSh(&circl);
  zabrodina::Rectangle rectangl({6.5, -2.3}, 2.1, 3.4);
  compSh.addShape(&rectangl);
  zabrodina::Triangle triangl({-3.4, 0.43}, { 1.02, 5.4 }, { 3.54, -8.54});
  compSh.addShape(&triangl);
  const double area = compSh.getArea();
  const double coefficient = 0.45;
  compSh.scale(coefficient);
  BOOST_CHECK_CLOSE(area * coefficient * coefficient, compSh.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_scale_area_2)
{
  zabrodina::Circle circl({1.3,2.97}, 4.6);
  zabrodina::CompositeShape compSh(&circl);
  zabrodina::point_t centerBefore = compSh.getFrameRect().pos;
  const double coefficient = 2.34;
  compSh.scale(coefficient);
  circl.scale(coefficient);
  BOOST_CHECK_CLOSE(circl.getArea(), compSh.getArea(), accuracy);
  BOOST_CHECK_CLOSE(centerBefore.x, compSh.getFrameRect().pos.x, accuracy);
  BOOST_CHECK_CLOSE(centerBefore.y, compSh.getFrameRect().pos.y, accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_wrong_scale_data_1)
{
  zabrodina::Rectangle rectangl({0.4, 0.5}, 10.4, 5.6);
  zabrodina::CompositeShape compSh(&rectangl);
  BOOST_CHECK_THROW(compSh.scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShape_wrong_scale_data_2)
{
  zabrodina::Circle circl({-1.9, 2.3}, 3.0);
  zabrodina::CompositeShape compSh(&circl);
  BOOST_CHECK_THROW(compSh.scale(-10.9), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShape_out_of_range)
{
  zabrodina::Circle circl({2.45, -2.1}, 7.89);
  zabrodina::CompositeShape compSh(&circl);
  BOOST_CHECK_THROW(compSh[2], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()
