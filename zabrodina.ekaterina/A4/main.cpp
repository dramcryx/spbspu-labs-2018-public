#include <iostream>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "polygon.hpp"
#include <cmath>

void showHowItWorks(zabrodina::Shape *ptr)
{
  ptr->printInf();
  ptr->move({5.0,2.0});
  std::cout << "Move to {2,5}: " << std::endl;
  ptr->printInf();
  ptr->move(5.0,2.0);
  std::cout << "Move on x=2,y=5: " << std::endl;
  ptr->printInf();
  ptr->scale(2.0);
  std::cout << "Scale by 2: " << std::endl;
  ptr->printInf();
  ptr->rotate(30.0);
  std::cout << "Rotate by 30 degree: " << std::endl;
  ptr->printInf();
  std::cout << std::endl;
}


int main()
{
  try
  {
    zabrodina::Rectangle rect({1.0,4.0},6.0,2.0);
    std::cout << "RECTANGLE:" << std::endl;
    showHowItWorks(&rect);
    zabrodina::Circle circl({1.0,7.0},5.0);
    std::cout << "CIRCLE:" << std::endl;
    showHowItWorks(&circl);
    zabrodina::Triangle triangl({0.0,0.0}, {3.0,4.0}, {0.0,6.0});
    std::cout << "TRIANGLE:" << std::endl;
    showHowItWorks(&triangl);
    std::cout << "POLYGON:" << std::endl;
    zabrodina::Polygon polygon1{{-4.0,0.0},{-1.0,4.0},{2.0,3.0},{2.0,0.0},{-3.0,-3.0}};
    showHowItWorks(&polygon1);
    std::cout << "Create Composite Shape" << std::endl;
    zabrodina::CompositeShape compos;
    compos.addShape(&rect);
    compos.addShape(&circl);
    compos.addShape(&triangl);
    compos.addShape(&polygon1);
    compos.printInf();
    std::cout << "Move Composite_Shape to {1,6}:" << std::endl;
    compos.move({1.0,6.0});
    compos.printInf();
    std::cout << "Move Composite_Shape on x=1, y=6:" << std::endl;
    compos.move(1.0,6.0);
    compos.printInf();
    std::cout << "Scale Composite_Shape by 5.4" << std::endl;
    compos.scale(5.4);
    compos.printInf();
    std::cout << "Rotate Composite_Shape by 45 degree" << std::endl;
    compos.scale(45.0);
    compos.printInf();
    zabrodina::Matrix matrix_arr(std::make_shared<zabrodina::CompositeShape>(&compos));
    zabrodina::Circle circl2({0.0,7.0},9.0);
    matrix_arr.addShape(std::make_shared<zabrodina::Circle>(circl2));
    matrix_arr.addShape(std::make_shared<zabrodina::Rectangle>(rect));
    matrix_arr.addShape(std::make_shared<zabrodina::CompositeShape>(&compos));
    std::cout << "Matrix of shapes:" << std::endl;
    matrix_arr.printInf();
  }
  catch(const std::invalid_argument &e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}
