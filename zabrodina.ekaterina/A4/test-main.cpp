#define BOOST_TEST_MAIN

#include <boost/test/included/unit_test.hpp>

#include <stdexcept>
#include <string>
#include <memory>
#include <cmath>

#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include "base-types.hpp"
#include "polygon.hpp"

const double accuracy = 0.0001;

BOOST_AUTO_TEST_SUITE(rectangle_tests)
BOOST_AUTO_TEST_CASE(rectangle_move_on_x_y)
{
  zabrodina::Rectangle rect({0.0, 0.0}, 10.0, 9.0);
  const zabrodina::point_t center_before({rect.getFrameRect().pos.x, rect.getFrameRect().pos.y});
  const double area = rect.getArea();
  const double width = rect.getFrameRect().width;
  const double height = rect.getFrameRect().height;
  rect.move(3.25, 8.1);
  BOOST_CHECK_CLOSE(center_before.x, rect.getFrameRect().pos.x - 3.25, accuracy);
  BOOST_CHECK_CLOSE(center_before.y, rect.getFrameRect().pos.y - 8.1, accuracy);
  BOOST_CHECK_CLOSE(width, rect.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, rect.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, rect.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(rectangle_move_to_x_y)
{
  zabrodina::Rectangle rect({-2.6, -23.5}, 12.3, 4.89);
  const double area = rect.getArea();
  const double width = rect.getFrameRect().width;
  const double height = rect.getFrameRect().height;
  rect.move({-0.45, 2.4});
  BOOST_CHECK_CLOSE(width, rect.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, rect.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, rect.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(rectangle_scale)
{
  zabrodina::Rectangle rect({-0.4, 4.5}, 4.5, 5.4);
  const double area = rect.getArea();
  const double width = rect.getFrameRect().width;
  const double height = rect.getFrameRect().height;
  const double k = 2.5;
  rect.scale(k);
  BOOST_CHECK_CLOSE(area * k * k, rect.getArea(), accuracy);
  BOOST_CHECK_CLOSE(height * k, rect.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(width * k, rect.getFrameRect().width, accuracy);
}
BOOST_AUTO_TEST_CASE(rectangle_wrong_data)
{
  BOOST_CHECK_THROW(zabrodina::Rectangle({0.0, -5.0}, 0, -10), std::invalid_argument);
  zabrodina::Rectangle rect({0.0, 5.0}, 5, 5);
  BOOST_CHECK_THROW(rect.scale(0.0), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(circle_tests)
BOOST_AUTO_TEST_CASE(circle_move_on_x_y)
{
  zabrodina::Circle circl({-2.0, 2.0}, 5.2);
  const zabrodina::point_t center_before({circl.getFrameRect().pos.x, circl.getFrameRect().pos.y});
  const double area = circl.getArea();
  const double width = circl.getFrameRect().width;
  const double height = circl.getFrameRect().height;
  circl.move(1.0, 2.0);
  BOOST_CHECK_CLOSE(center_before.x, circl.getFrameRect().pos.x - 1.0, accuracy);
  BOOST_CHECK_CLOSE(center_before.y, circl.getFrameRect().pos.y - 2.0, accuracy);
  BOOST_CHECK_CLOSE(width, circl.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, circl.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, circl.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(circle_move_to_x_y)
{
  zabrodina::Circle circl({3.5, -2.3}, 2.0);
  const double area = circl.getArea();
  const double width = circl.getFrameRect().width;
  const double height = circl.getFrameRect().height;
  circl.move({0.4, 2.3});
  BOOST_CHECK_CLOSE(width, circl.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, circl.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, circl.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(circle_scale)
{
  zabrodina::Circle circl({-2.3, -1.9}, 3.0);
  const double area = circl.getArea();
  const double k = 2.7;
  circl.scale(k);
  BOOST_CHECK_CLOSE(area * k * k, circl.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(circle_wrong_data)
{
  BOOST_CHECK_THROW(zabrodina::Circle({0.5, 1.5}, -5.3), std::invalid_argument);
  zabrodina::Circle circl({2.34, 5.12}, 5.3);
  BOOST_CHECK_THROW(circl.scale(-2.5), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(triangle_tests)
BOOST_AUTO_TEST_CASE(triangle_get_area)
{
  zabrodina::Triangle triangl({0.0, 4.0}, {0.0, 0.0}, {4.0, 0.0});
  BOOST_CHECK_CLOSE(8.0, triangl.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(triangle_move_on_x_y)
{
  zabrodina::Triangle triangl({-2.56, 1.23}, {0.0, -1.2}, {2.0, 1.23});
  const double area = triangl.getArea();
  const double width = triangl.getFrameRect().width;
  const double height = triangl.getFrameRect().height;
  triangl.move(5.2, -1.23);
  BOOST_CHECK_CLOSE(width, triangl.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, triangl.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, triangl.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(triangle_move_to_x_y)
{
  zabrodina::Triangle triangl({-2.28, 0.0}, {1.0, 3.24}, {6.23, -3.45});
  const double area = triangl.getArea();
  const double width = triangl.getFrameRect().width;
  const double height = triangl.getFrameRect().height;
  triangl.move({-2.32, 2.32});
  BOOST_CHECK_CLOSE(width, triangl.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, triangl.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, triangl.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(triangle_scale)
{
  zabrodina::Triangle triangl({0.0, 4.3}, {0.0, 0.0}, {3.23, -2.34});
  const double area = triangl.getArea();
  const double k = 0.23;
  triangl.scale(k);
  BOOST_CHECK_CLOSE(area * k * k, triangl.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(triangle_wrong_data)
{
  BOOST_CHECK_THROW(zabrodina::Triangle({2.1, 2.1}, {2.1, -1.5}, {2.1, -1.5}), std::invalid_argument);
  zabrodina::Triangle triangl({0.0, 2.3}, {2.34, -3.4}, {6.56, 3.45});
  BOOST_CHECK_THROW(triangl.scale(-1.23), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(polygon_tests)
BOOST_AUTO_TEST_CASE(polygon_move_on_x_y)
{
  zabrodina::Polygon polygon1{{-4.0,0.0},{-1.0,4.0},{2.0,0.0},{-3.0,-3.0}};
  const zabrodina::point_t center_before({polygon1.getFrameRect().pos.x, polygon1.getFrameRect().pos.y});
  const double area = polygon1.getArea();
  const double width = polygon1.getFrameRect().width;
  const double height = polygon1.getFrameRect().height;
  polygon1.move(5.35, 80.1);
  BOOST_CHECK_CLOSE(center_before.x, polygon1.getFrameRect().pos.x - 5.35, accuracy);
  BOOST_CHECK_CLOSE(center_before.y, polygon1.getFrameRect().pos.y - 80.1, accuracy);
  BOOST_CHECK_CLOSE(width, polygon1.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, polygon1.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, polygon1.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(polygon_move_to_x_y)
{
  zabrodina::Polygon polygon1{{-4.0,0.0},{-1.0,4.0},{2.0,0.0},{-3.0,-3.0}};
  const double area = polygon1.getArea();
  const double width = polygon1.getFrameRect().width;
  const double height = polygon1.getFrameRect().height;
  polygon1.move({-5.45, 25.4});
  BOOST_CHECK_CLOSE(width, polygon1.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, polygon1.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, polygon1.getArea(), accuracy);
}
BOOST_AUTO_TEST_CASE(polygon_scale)
{
  zabrodina::Polygon polygon1{{-2.0,-2.0},{-2.0,2.0},{2.0,2.0},{2.0,-2.0}};
  const double area = polygon1.getArea();
  const double width = polygon1.getFrameRect().width;
  const double height = polygon1.getFrameRect().height;
  const double k = 4.0;
  polygon1.scale(k);
  BOOST_CHECK_CLOSE(area * k * k, polygon1.getArea(), accuracy);
  BOOST_CHECK_CLOSE(height * k, polygon1.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(width * k, polygon1.getFrameRect().width, accuracy);
}
BOOST_AUTO_TEST_CASE(polygon_wrong_coefficient)
{
  zabrodina::Polygon polygon1{{-4.0,0.0},{-1.0,4.0},{2.0,0.0},{-3.0,-3.0}};
  BOOST_CHECK_THROW(polygon1.scale(0.0), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(polygon_wrong_vertexdata)
{
  BOOST_CHECK_THROW(zabrodina::Polygon ({{-4.0,0.0},{-1.0,4.0}}), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(polygon_wrong_area_data)
{
  BOOST_CHECK_THROW(zabrodina::Polygon ({{-4.0,0.0},{-1.0,0.0},{2.0,0.0}}), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE(polygon_concave)
{
  
  BOOST_CHECK_THROW(zabrodina::Polygon ({{-4.0,-4.0},{0.0,-2.0},{4.0,-4.0},{0.0,3.0}}), std::invalid_argument);
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(compositeShape_tests)
BOOST_AUTO_TEST_CASE(compositeShape_matching_shape)
{
  zabrodina::Circle circl({2.0, 1.34}, 10.6);
  zabrodina::CompositeShape compSh(&circl);
  BOOST_CHECK_CLOSE(compSh.getArea(), circl.getArea(), accuracy);
  BOOST_CHECK_CLOSE(compSh.getFrameRect().height, circl.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(compSh.getFrameRect().width, circl.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(compSh.getFrameRect().pos.x, 2.0, accuracy);
  BOOST_CHECK_CLOSE(compSh.getFrameRect().pos.y, 1.34, accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_getArea)
{
  zabrodina::Circle circl({3.2, 1.23}, 8.9);
  zabrodina::CompositeShape compSh(&circl);
  zabrodina::Triangle triangl({2.78, -2.3}, {6.45, 2.2}, { 10.4, -5.0});
  compSh.addShape(&triangl);
  zabrodina::Rectangle rectangl({2.32, -5.06}, 2, 4);
  compSh.addShape(&rectangl);
  BOOST_CHECK_CLOSE(compSh.getArea(), circl.getArea() + triangl.getArea() + rectangl.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_getArea_figuers_area_changed)
{
  zabrodina::Circle circl({0.23, -2.8}, 2.12);
  zabrodina::CompositeShape compSh(&circl);
  zabrodina::Rectangle rectangl({1.23, 5.34}, 12.0, 13.8);
  compSh.addShape(&rectangl);
  zabrodina::Triangle triangl({1.05, 2.46}, {2.34, -5.5 }, {4.5, 10.5});
  compSh.addShape(&triangl);
  BOOST_CHECK_CLOSE(circl.getArea(), compSh[0] -> getArea(), accuracy);
  BOOST_CHECK_CLOSE(rectangl.getArea(), compSh[1] -> getArea(), accuracy);
  BOOST_CHECK_CLOSE(triangl.getArea(), compSh[2] -> getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_move_OX_OY)
{
  zabrodina::Rectangle rectangl({0.34, -5.6}, 10.5, 5.6);
  zabrodina::CompositeShape compSh(&rectangl);
  zabrodina::Circle circl({2.12,3.23}, 10.5);
  compSh.addShape(&circl);
  const double width = compSh.getFrameRect().width;
  const double height = compSh.getFrameRect().height;
  const double area = compSh.getArea();
  compSh.move(4.23, 5.34);
  BOOST_CHECK_CLOSE(width, compSh.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, compSh.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, compSh.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_move_on)
{
  zabrodina::Circle circl({4.23, -5.34}, 4.34);
  zabrodina::CompositeShape compSh(&circl);
  zabrodina::Rectangle rectangl({0.34, -5.6}, 10.5, 5.6);
  compSh.addShape(&rectangl);
  const double width = compSh.getFrameRect().width;
  const double height = compSh.getFrameRect().height;
  const double area = compSh.getArea();
  compSh.move({2.0, 2.4});
  BOOST_CHECK_CLOSE(width, compSh.getFrameRect().width, accuracy);
  BOOST_CHECK_CLOSE(height, compSh.getFrameRect().height, accuracy);
  BOOST_CHECK_CLOSE(area, compSh.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_scale_area_1)
{
  zabrodina::Circle circl({0.5, -8.2}, 4.5);
  zabrodina::CompositeShape compSh(&circl);
  zabrodina::Rectangle rectangl({6.5, -2.3}, 2.1, 3.4);
  compSh.addShape(&rectangl);
  zabrodina::Triangle triangl({-3.4, 0.43}, { 1.02, 5.4 }, { 3.54, -8.54});
  compSh.addShape(&triangl);
  const double area = compSh.getArea();
  const double coefficient = 0.45;
  compSh.scale(coefficient);
  BOOST_CHECK_CLOSE(area * coefficient * coefficient, compSh.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_scale_area_2)
{
  zabrodina::Circle circl({1.3,2.97}, 4.6);
  zabrodina::CompositeShape compSh(&circl);
  zabrodina::point_t centerBefore = compSh.getFrameRect().pos;
  const double coefficient = 2.34;
  compSh.scale(coefficient);
  circl.scale(coefficient);
  BOOST_CHECK_CLOSE(circl.getArea(), compSh.getArea(), accuracy);
  BOOST_CHECK_CLOSE(centerBefore.x, compSh.getFrameRect().pos.x, accuracy);
  BOOST_CHECK_CLOSE(centerBefore.y, compSh.getFrameRect().pos.y, accuracy);
}

BOOST_AUTO_TEST_CASE(compositeShape_wrong_scale_data_1)
{
  zabrodina::Rectangle rectangl({0.4, 0.5}, 10.4, 5.6);
  zabrodina::CompositeShape compSh(&rectangl);
  BOOST_CHECK_THROW(compSh.scale(0.0), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShape_wrong_scale_data_2)
{
  zabrodina::Circle circl({-1.9, 2.3}, 3.0);
  zabrodina::CompositeShape compSh(&circl);
  BOOST_CHECK_THROW(compSh.scale(-10.9), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(compositeShape_out_of_range)
{
  zabrodina::Circle circl({2.45, -2.1}, 7.89);
  zabrodina::CompositeShape compSh(&circl);
  BOOST_CHECK_THROW(compSh[2], std::out_of_range);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE(testMatrix)

BOOST_AUTO_TEST_CASE(Rotate_area)
{
  zabrodina::Rectangle rectangle({5.0, 2.4}, 2.5, 3.2);
  double area_before = rectangle.getArea();
  rectangle.rotate(35.0);
  BOOST_CHECK_CLOSE(rectangle.getArea(), area_before, accuracy);
}

BOOST_AUTO_TEST_CASE(Rotate_center)
{
  zabrodina::Triangle triangle({0.0, -5.0}, {0.0, 2.0}, {5.0, -3.0});
  zabrodina::point_t centerBefore;
  centerBefore.x = triangle.findCenter().x;
  centerBefore.y = triangle.findCenter().y;
  triangle.rotate(45.0);
  BOOST_CHECK_CLOSE(triangle.findCenter().x, centerBefore.x, accuracy);
  BOOST_CHECK_CLOSE(triangle.findCenter().y, centerBefore.y, accuracy);
}


BOOST_AUTO_TEST_CASE(Rotate_rectangle)
{
  zabrodina::Rectangle rectangle({0.0, 0.0}, 2.0, 2.0);
  zabrodina::rectangle_t frame_rect_before = rectangle.getFrameRect();
  rectangle.rotate(45.0);
  zabrodina::rectangle_t frame_rect = rectangle.getFrameRect();
  BOOST_CHECK_CLOSE(frame_rect.pos.x, frame_rect_before.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.pos.y, frame_rect_before.pos.y, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.width, frame_rect_before.width * sqrt(2), accuracy);
  BOOST_CHECK_CLOSE(frame_rect.height, frame_rect_before.height * sqrt(2), accuracy);
}

BOOST_AUTO_TEST_CASE(Rotate_triangle)
{
  zabrodina::Triangle triangle({-3.0, 2.0}, {-6.0, 4.5}, {-4.0, -2.4});
  zabrodina::rectangle_t frame_rect_before = triangle.getFrameRect();
  triangle.rotate(360.0);
  zabrodina::rectangle_t frame_rect = triangle.getFrameRect();
  BOOST_CHECK_CLOSE(frame_rect.pos.x, frame_rect_before.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.pos.y, frame_rect_before.pos.y, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.width, frame_rect_before.width, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.height, frame_rect_before.height, accuracy);
}

BOOST_AUTO_TEST_CASE(Rotate_polygon)
{
  zabrodina::Polygon polygon1{{-2.0,-2.0},{-2.0,2.0},{2.0,2.0},{2.0,-2.0}};
  zabrodina::rectangle_t frame_rect_before = polygon1.getFrameRect();
  polygon1.rotate(90.0);
  zabrodina::rectangle_t frame_rect = polygon1.getFrameRect();
  BOOST_CHECK_CLOSE(frame_rect.pos.x, frame_rect_before.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.pos.y, frame_rect_before.pos.y, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.width, frame_rect_before.width, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.height, frame_rect_before.height, accuracy);
}

BOOST_AUTO_TEST_CASE(Rotate_composite_shape)
{
  zabrodina::Rectangle rect({0.0,0.0},2.0,2.0);
  zabrodina::CompositeShape composite_shape(&rect);
  zabrodina::Rectangle rect1({5.0,5.0},4.0,4.0);
  composite_shape.addShape(&rect1);
  zabrodina::rectangle_t frame_rect_before= composite_shape.getFrameRect();
  composite_shape.rotate(90.0);
  zabrodina::rectangle_t frame_rect = composite_shape.getFrameRect();
  BOOST_CHECK_CLOSE(frame_rect.pos.x, frame_rect_before.pos.x, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.pos.y, frame_rect_before.pos.y, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.width, frame_rect_before.width, accuracy);
  BOOST_CHECK_CLOSE(frame_rect.height, frame_rect_before.height, accuracy);
}

BOOST_AUTO_TEST_CASE(Matrix_add_shape)
{
  zabrodina::Triangle triangl({0.0,6.0}, {3.0,4.0}, {0.0,-6.0});
  zabrodina::Matrix matrix_arr(std::make_shared<zabrodina::Triangle>(triangl));
  BOOST_CHECK_CLOSE(matrix_arr[0][0]->getArea(), triangl.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(Matrix_add_lines)
{
  zabrodina::Triangle triangl({-1.0,6.0}, {6.0,2.0}, {0.0,-6.0});
  zabrodina::Matrix matrix_arr(std::make_shared<zabrodina::Triangle>(triangl));
  zabrodina::Rectangle rect({0.0,0.0},6.0,2.0);
  matrix_arr.addShape(std::make_shared<zabrodina::Rectangle>(rect));
  BOOST_CHECK_CLOSE(matrix_arr[1][0]->getArea(), rect.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(Matrix_add_columns)
{
  zabrodina::Triangle triangl({0.0,6.0}, {6.0,2.0}, {0.0,-6.0});
  zabrodina::Matrix matrix_arr(std::make_shared<zabrodina::Triangle>(triangl));
  zabrodina::Rectangle rect({-5.0,-3.0},2.0,2.0);
  matrix_arr.addShape(std::make_shared<zabrodina::Rectangle>(rect));
  BOOST_CHECK_CLOSE(matrix_arr[0][1]->getArea(), rect.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(Matrix_add_composite_shape)
{
  zabrodina::Rectangle rect({1.0,4.0},6.0,2.0);
  zabrodina::Triangle triangl({0.0,6.0}, {6.0,2.0}, {0.0,-6.0});
  zabrodina::CompositeShape compos(&rect);
  compos.addShape(&triangl);
  zabrodina::Matrix matrix_arr(std::make_shared<zabrodina::CompositeShape>(&compos));
  BOOST_CHECK_CLOSE(matrix_arr[0][0]->getArea(), compos.getArea(), accuracy);
}

BOOST_AUTO_TEST_CASE(Matrix_wrong_data)
{
  BOOST_CHECK_THROW(zabrodina::Matrix matrix_arr(nullptr), std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()
