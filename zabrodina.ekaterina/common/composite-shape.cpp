#include "composite-shape.hpp"

#include <iostream>
#include <cmath>

zabrodina::CompositeShape::CompositeShape() noexcept:
count_(0),
shape_array_(nullptr)
{}

zabrodina::CompositeShape::CompositeShape(Shape * newShape):
count_(1),
shape_array_(new Shape*[1]),
pos_({0,0})
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Error: Object has not been created yet or already deleted");
  }
  shape_array_[0] = newShape;
  pos_ = shape_array_[0] -> getFrameRect().pos;
}

void zabrodina::CompositeShape::addShape(Shape * newShape)
{
  if (newShape == nullptr)
  {
    throw std::invalid_argument("Error: Object has not been created yet or already deleted");
  }
  
  std::unique_ptr<Shape*[]> new_arr(new Shape *[count_ + 1]);
  for(int i = 0; i < count_; i++)
  {
    new_arr[i]=shape_array_[i];
  }
  new_arr[count_] = newShape;
  count_++;
  shape_array_.swap(new_arr);
  double maxX = newShape -> getFrameRect().width/2 + newShape -> getFrameRect().pos.x;
  double maxY = newShape -> getFrameRect().height/2 + newShape -> getFrameRect().pos.y;
  double minX = newShape -> getFrameRect().pos.x - newShape -> getFrameRect().width/2;
  double minY = newShape -> getFrameRect().pos.y - newShape -> getFrameRect().height/2;

  for (int i = 1; i < count_; i++)
  {
    if (maxX < shape_array_[i] -> getFrameRect().width/2 + shape_array_[i] -> getFrameRect().pos.x)
    {
      maxX = shape_array_[i] -> getFrameRect().width/2 + shape_array_[i] -> getFrameRect().pos.x;
    }
    
    if (maxY < shape_array_[i] -> getFrameRect().height/2 + shape_array_[i] -> getFrameRect().pos.y)
    {
      maxY = shape_array_[i] -> getFrameRect().height/2 + shape_array_[i] -> getFrameRect().pos.y;
    }
    
    if (minX > shape_array_[i] -> getFrameRect().pos.x - shape_array_[i] -> getFrameRect().width/2)
    {
      minX = shape_array_[i] -> getFrameRect().pos.x - shape_array_[i] -> getFrameRect().width/2;
    }
    
    if (minY > shape_array_[i] -> getFrameRect().pos.y - shape_array_[i] -> getFrameRect().height/2)
    {
      minY = shape_array_[i] -> getFrameRect().pos.y - shape_array_[i] -> getFrameRect().height/2;
    }
  }

  pos_.x = (maxX + minX)/2;
  pos_.y = (maxY + minY)/2;
}

double zabrodina::CompositeShape::getArea() const noexcept
{
  double totalArea = 0.0;
  for (int i = 0; i < count_; i++)
  {
    totalArea += shape_array_[i] -> getArea();
  }
  return totalArea;
}

zabrodina::rectangle_t zabrodina::CompositeShape::getFrameRect() const noexcept
{
  rectangle_t frameRect = shape_array_[0] -> getFrameRect();
  double maxX = frameRect.width/2 + frameRect.pos.x;
  double maxY = frameRect.height/2 + frameRect.pos.y;
  double minX = frameRect.pos.x - frameRect.width/2;
  double minY = frameRect.pos.y - frameRect.height/2;
  for (int i = 1; i < count_; i++)
  {
    frameRect = shape_array_[i] -> getFrameRect();
    if (maxX < frameRect.width/2 + frameRect.pos.x)
    {
        maxX = frameRect.width/2 + frameRect.pos.x;
    }
  
    if (maxY < frameRect.height/2 + frameRect.pos.y)
    {
        maxY = frameRect.height/2 + frameRect.pos.y;
    }
  
    if (minX > frameRect.pos.x - frameRect.width/2)
    {
        minX = frameRect.pos.x - frameRect.width/2;
    }
  
    if (minY > frameRect.pos.y - frameRect.height/2)
    {
        minY = frameRect.pos.y - frameRect.height/2;
    }
  }
  return {maxX - minX, maxY - minY, {(maxX + minX)/2, (maxY + minY)/2}};
}

void zabrodina::CompositeShape::move(const point_t &newPos) noexcept
{
  double OX = newPos.x - pos_.x;
  double OY = newPos.y - pos_.y;
  move (OX, OY);
}

void zabrodina::CompositeShape::move(double OX, double OY) noexcept
{
  for (int i = 0; i < count_; i++)
  {
    shape_array_[i] -> move(OX, OY);
  }
  pos_.x += OX;
  pos_.y += OY;
}

void zabrodina::CompositeShape::scale(double coefficient)
{
  if (coefficient > 0)
  {
    for (int i = 0; i < count_; i++)
    {
      double dX = shape_array_[i] -> getFrameRect().pos.x - pos_.x;
      double dY = shape_array_[i] -> getFrameRect().pos.y - pos_.y;
      shape_array_[i] -> move ((coefficient-1)*dX, (coefficient-1)*dY);
      shape_array_[i] -> scale(coefficient);
    }
  }
  else
  {
    throw std::invalid_argument ("Error: coefficient <= 0");
  }
}

void zabrodina::CompositeShape::printInf() const noexcept
{
  std::cout << "Composite_Shape is composed of " << count_ << " shapes"<<std::endl;
  std::cout << "Center of Composite_Shape: {" << pos_.x << ", " << pos_.y << "}, " << std::endl;
  std::cout << "Area of Composite_Shape: " << getArea() << std::endl;
  std::cout << "Frame of Composite_Shape:" << std::endl;
  std::cout << "Center {" << getFrameRect().pos.x << ", " << getFrameRect().pos.y << "}" << std::endl;
  std::cout << "Width: " << getFrameRect().width << ", Height: " << getFrameRect().height <<  std::endl;
  for (int i = 0; i < count_; i++)
  {
    std::cout << "SHAPE " << i+1 << ":" << std::endl;
    shape_array_[i] -> printInf();
  }
  std::cout << std::endl;
}

void zabrodina::CompositeShape::deleteShapes() noexcept
{
  shape_array_.reset();
  shape_array_ = nullptr;
  count_ = 0;
}
    
zabrodina::Shape * zabrodina::CompositeShape::operator[](const int index) const
{
  if ((index >= count_))
  {
    throw std::out_of_range("Error: Index is out of range");
  }
  return shape_array_[index];
}
    
void zabrodina::CompositeShape::rotate(const double angle)
{
  double angle_cos = cos(angle * M_PI / 180);
  double angle_sin = sin(angle * M_PI / 180);
  point_t center = getFrameRect().pos;
  for (int i = 0; i < count_; i++)
  {
    point_t shape_center = shape_array_[i]->getFrameRect().pos;
    shape_array_[i]->move({center.x + angle_cos * (shape_center.x - center.x) - angle_sin * (shape_center.y - center.y),
      center.y + angle_cos * (shape_center.y - center.y) + angle_sin * (shape_center.x - center.x)});
    shape_array_[i]->rotate(angle);
  }
}
    
std::string zabrodina::CompositeShape::getName() const noexcept
{
  return "CS";
}
