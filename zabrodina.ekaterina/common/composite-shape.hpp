#ifndef COMPOSITE_SHAPE_HPP
#define COMPOSITE_SHAPE_HPP

#include "shape.hpp"

#include <iostream>
#include <memory>
#include <string>

namespace zabrodina
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape() noexcept;
    CompositeShape(Shape * newShape);
    void addShape(Shape * newShape);
    double getArea() const noexcept override;
    rectangle_t getFrameRect() const noexcept override;
    void move(const point_t &newPos) noexcept override;
    void move(const double Ox, const double Oy) noexcept override;
    void scale(const double coefficient) override;
    void printInf() const noexcept override;
    void deleteShapes() noexcept;
    Shape * operator[](const int index) const;
    void rotate(const double angle) override;
    std::string getName() const noexcept override;
    int count_;
  private:
    std::unique_ptr<Shape*[]> shape_array_;
    point_t pos_;
  };
}

#endif /* composite_shape_hpp */
