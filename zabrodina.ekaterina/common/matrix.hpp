#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "shape.hpp"
#include "composite-shape.hpp"
#include <iostream>
#include <memory>

namespace zabrodina
{
class Matrix
{
  public:
    Matrix();
    Matrix(CompositeShape &compos);
    Matrix(const std::shared_ptr<Shape> &newShape);
    std::unique_ptr <std::shared_ptr<Shape>[]>::pointer operator[](size_t index) const;
    void addShape(const std::shared_ptr<Shape> &);
    void addCompositeShape(CompositeShape &compos);
    void printInf() const;
  private:
    std::unique_ptr <std::shared_ptr<Shape>[]> matrix_;
    size_t lines_;
    size_t columns_;
    bool checkOverlapOfShapes(const std::shared_ptr<Shape> &, const std::shared_ptr<Shape> &) const;
};
}

#endif /* Matrix_hpp */
