#include <iostream>
#include <stdexcept>
#include <cmath>
#include "polygon.hpp"


zabrodina::Polygon::Polygon(std::initializer_list<point_t> vertexes)
{
  if (vertexes.size()<3)
  {
    throw std::invalid_argument("Error: Polygons must contain more than two vertices");
  }
  count_ = vertexes.size();
  polygonVertex_ = std::unique_ptr<point_t[]>(new point_t[count_]);
  int i = 0;
  for (std::initializer_list<point_t>::const_iterator vertex = vertexes.begin(); vertex != vertexes.end(); ++vertex)
  {
    polygonVertex_[i] = *vertex;
    i++;
  }
  if (getArea() <= 0.0)
  {
    polygonVertex_.reset();
    throw std::invalid_argument("Error: Zero area");
  }
  if (!checkConvex())
  {
    polygonVertex_.reset();
    throw std::invalid_argument("Error: Polygon is concave");
  }
  pos_ = {0.0, 0.0};
  for (size_t i = 0; i < count_; i++)
  {
    pos_.x += polygonVertex_[i].x;
    pos_.y += polygonVertex_[i].y;
  }
  pos_.x = pos_.x / count_;
  pos_.y = pos_.y / count_;
}

zabrodina::point_t zabrodina::Polygon::operator[](size_t index) const
{
  if (index >= count_)
  {
    throw std::invalid_argument("Error: Out of the range of the array of points");
  }
  return polygonVertex_[index];
}

double zabrodina::Polygon::getArea() const
{
  {
    double area = ((polygonVertex_[count_ - 1].x + polygonVertex_[0].x)*(polygonVertex_[count_ - 1].y - polygonVertex_[0].y));
    for (size_t i = 0; i < count_ - 1; i++)
    {
      area += ((polygonVertex_[i].x + polygonVertex_[i + 1].x)*(polygonVertex_[i].y - polygonVertex_[i + 1].y));
    }
    return abs(area) / 2;
  }
}

zabrodina::rectangle_t zabrodina::Polygon::getFrameRect() const
{
  double right = polygonVertex_[0].x;
  double left = polygonVertex_[0].x;
  double top = polygonVertex_[0].y;
  double bottom = polygonVertex_[0].y;
  for (size_t i = 1; i < count_; i++)
  {
    right = std::max(polygonVertex_[i].x, right);
    left = std::min(polygonVertex_[i].x, left);
    top = std::max(polygonVertex_[i].y, top);
    bottom = std::min(polygonVertex_[i].y, bottom);
  }
  rectangle_t frame;
  frame.pos.x = (right + left) / 2;
  frame.pos.y = (top + bottom) / 2;
  frame.width = right - left;
  frame.height = top - bottom;
  return frame;
}

void zabrodina::Polygon::move(const point_t & pos)
{
  double Ox = pos.x - pos_.x;
  double Oy = pos.y - pos_.y;
  move(Ox, Oy);
}

void zabrodina::Polygon::move(double Ox, double Oy)
{
  for (size_t i = 0; i < count_; ++i)
  {
    polygonVertex_[i].x += Ox;
    polygonVertex_[i].y += Oy;
  }
}

void zabrodina::Polygon::printInf() const
{
  std::cout << "The polygon has " << count_ << " sides"<< std::endl;
  for (size_t i = 0; i < count_; i++)
  {
    std::cout << "Vertex " << i + 1 << ": ( " << polygonVertex_[i].x << "," << polygonVertex_[i].y << ")" << std::endl;
  }
  std::cout << "Area of polygon: " << getArea() << std::endl;
  std::cout << "Center of a polygon: " << "(" << pos_.x << "," << pos_.y << ")" << std::endl;
  rectangle_t frame = getFrameRect();
  std::cout << "Frame Center: ("<< frame.pos.x << "," << frame.pos.y << ")" << std::endl;
  std::cout << "Frame Width: " << frame.width << "   Frame Height: " << frame.height << std::endl;
}

std::string zabrodina::Polygon::getName() const noexcept
{
  return "P";
}

void zabrodina::Polygon::scale(double coefficient)
{
  if (coefficient <= 0)
  {
    throw std::invalid_argument("Error: Coefficient <= 0");
  }
  for (size_t i = 0; i < count_; i++)
  {
    polygonVertex_[i].x *= coefficient;
    polygonVertex_[i].y *= coefficient;
  }
  point_t pos = { pos_.x , pos_.y };
  pos_.x = pos_.x * coefficient;
  pos_.y = pos_.y * coefficient;
  Polygon::move(pos);
}

void zabrodina::Polygon::rotate(double angle)
{
  double angleInRadians = angle * M_PI / 180;
  const point_t center = pos_;
  const double sinOfAngle = sin(angleInRadians);
  const double cosOfAngle = cos(angleInRadians);
  for (size_t i = 0; i < count_; i++)
  {
    polygonVertex_[i] = {center.x + cosOfAngle * (polygonVertex_[i].x - center.x) - sinOfAngle * (polygonVertex_[i].y - center.y),
      center.y + cosOfAngle * (polygonVertex_[i].y - center.y) + sinOfAngle * (polygonVertex_[i].x - center.x)};
  }
}

bool zabrodina::Polygon::checkConvex() const
{
  for (size_t i = 0; i < (count_ - 1); i++)
  {
    double firstVertex = (polygonVertex_[i + 1].y - polygonVertex_[i].y) * (polygonVertex_[0].x - polygonVertex_[i].x)
    - (polygonVertex_[i + 1].x - polygonVertex_[i].x) * (polygonVertex_[0].y - polygonVertex_[i].y);
    double secondVertex = 0;
    for (size_t j = 1; j < count_; j++)
    {
      secondVertex = (polygonVertex_[i + 1].y - polygonVertex_[i].y) * (polygonVertex_[j].x - polygonVertex_[i].x)
      - (polygonVertex_[i + 1].x - polygonVertex_[i].x) * (polygonVertex_[j].y - polygonVertex_[i].y);
      if (firstVertex * secondVertex >= 0)
      {
        firstVertex = secondVertex;
      }
      else
      {
        return false;
      }
    }
  }
  return true;
}
