#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double EPSILON = 1e-7;

BOOST_AUTO_TEST_SUITE(Rectangle)
  BOOST_AUTO_TEST_CASE(MovingToPoint)
  {
    zagorodnev::Rectangle r ({ 25, 35 }, 10, 11);
    double widthBefore = r.getFrameRect().width;
    double heightBefore = r.getFrameRect().height;
    r.move ({ 41.32, 19.19 });
    BOOST_CHECK_EQUAL(widthBefore, r.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, r.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(RelativeMoving)
  {
    zagorodnev::Rectangle r ({ 25, 35 }, 10, 11);
    double widthBefore = r.getFrameRect().width;
    double heightBefore = r.getFrameRect().height;
    r.move (6, 5);
    BOOST_CHECK_EQUAL(widthBefore, r.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, r.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingToPointArea)
  {
    zagorodnev::Rectangle r ({ 25, 35 }, 10, 11);
    double areaBefore = r.getArea();
    r.move (5, 10);
    BOOST_CHECK_EQUAL(areaBefore, r.getArea());
  }

  BOOST_AUTO_TEST_CASE(MovingOnPointArea)
  {
    zagorodnev::Rectangle r ({ 25, 35 }, 10, 11);
    double areaBefore = r.getArea();
    r.move ({ 44.31, 18.19 });
    BOOST_CHECK_EQUAL(areaBefore, r.getArea());
  }

  BOOST_AUTO_TEST_CASE(Scaling)
  {
    const double scal = 2.0;
    zagorodnev::Rectangle r ({ 25, 35 }, 10, 11);
    double areaBeforeScaling = r.getArea();
    r.scale(scal);
    BOOST_CHECK_CLOSE(scal*scal*areaBeforeScaling, r.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
  {
    zagorodnev::Rectangle r ({ 25, 35 }, 10, 11);
    BOOST_CHECK_THROW (r.scale(-5.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidConstructorParameters)
  {
    BOOST_CHECK_THROW( zagorodnev::Rectangle r ({ 25, 35 }, -5, -15), std::invalid_argument );
    BOOST_CHECK_THROW( zagorodnev::Rectangle r ({ 25, 35 }, -10, 0), std::invalid_argument );
    BOOST_CHECK_THROW( zagorodnev::Rectangle r ({ 25, 35 }, 50, -10), std::invalid_argument );
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Circle)
  BOOST_AUTO_TEST_CASE(MovingToPoint)
  {
    zagorodnev::Circle circle ({ 24, 28 }, 5);
    double widthBefore = circle.getFrameRect().width;
    double heightBefore = circle.getFrameRect().height;
    circle.move ({ 11.31, 10 });
    BOOST_CHECK_EQUAL(widthBefore, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, circle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(RelativeMoving)
  {
    zagorodnev::Circle circle ({ 24, 28 }, 5);
    double widthBefore = circle.getFrameRect().width;
    double heightBefore = circle.getFrameRect().height;
    circle.move (5.5, 21.1);
    BOOST_CHECK_EQUAL(widthBefore, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, circle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingToPointArea)
  {
    zagorodnev::Circle circle ({ 24, 28 }, 5);
    double areaBefore = circle.getArea();
    circle.move ({ 11.31, 10 });
    BOOST_CHECK_CLOSE(areaBefore, circle.getArea(),EPSILON);
  }

  BOOST_AUTO_TEST_CASE(RelativeMovingArea)
  {
    zagorodnev::Circle circle ({ 24, 28 }, 5);
    double areaBefore = circle.getArea();
    circle.move (5.5, 21.1);
    BOOST_CHECK_CLOSE(areaBefore, circle.getArea(),EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Scaling)
  {
    const double ratio = 3.0;
    zagorodnev::Circle circle ({ 24, 28 }, 5);
    double areaBeforeScaling = circle.getArea();
    circle.scale(ratio);
    BOOST_CHECK_CLOSE(ratio*ratio*areaBeforeScaling, circle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
  {
    zagorodnev::Circle circle ({ 24, 28 }, 5);
    BOOST_CHECK_THROW(circle.scale(-3.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidConstructorParameters)
  {
    BOOST_CHECK_THROW(zagorodnev::Circle circle ({ 24, 28 }, -5), std::invalid_argument );
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Triangle)
  BOOST_AUTO_TEST_CASE(RelativeMoving)
  {
    zagorodnev::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move( 5.0, 5.0 );
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(MovingOnPoint)
  {
    zagorodnev::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move({5.0, 5.0});
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(MovingOnPointArea)
  {
    zagorodnev::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move({1.0, 1.0});
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(MovingToPointArea)
  {
    zagorodnev::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move(1.0, 1.0);
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Scaling)
  {
    zagorodnev::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double  k = 2.0;
    double triangleWidthBeforeScale = triangle.getFrameRect().width;
    double triangleHeightBeforeScale = triangle.getFrameRect().height;
    double triangleAreaBeforeScale = triangle.getArea();
    triangle.scale(k);
    BOOST_CHECK_CLOSE(k * k * triangleAreaBeforeScale, triangle.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, k * triangleWidthBeforeScale, EPSILON);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, k * triangleHeightBeforeScale, EPSILON);
    BOOST_CHECK_THROW(triangle.scale(-2), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

