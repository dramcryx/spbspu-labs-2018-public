#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "composite-shape.hpp"

double E = 0.001;

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)
  BOOST_AUTO_TEST_CASE(CopyConstructor)
  {
    std::shared_ptr<zagorodnev::Shape> rectanglePtr
    = std::make_shared<zagorodnev::Rectangle>(zagorodnev::Rectangle({168.0, 256.0}, 20.0, 58.0));
    zagorodnev::CompositeShape shape(rectanglePtr);

    std::shared_ptr<zagorodnev::Shape> circlePtr
    = std::make_shared<zagorodnev::Circle>(zagorodnev::Circle({10.0, 10.0}, 5.0));
    shape.addShape(circlePtr);

    zagorodnev::CompositeShape newTest(shape);
    BOOST_CHECK_EQUAL(shape[0], newTest[0]);
    BOOST_CHECK_EQUAL(shape[1], newTest[1]);
  }

  BOOST_AUTO_TEST_CASE(MoveConstructor)
  {
    std::shared_ptr<zagorodnev::Shape> rectanglePtr
    = std::make_shared<zagorodnev::Rectangle>(zagorodnev::Rectangle({168.0, 256.0}, 20.0, 58.0));
    zagorodnev::CompositeShape shape( rectanglePtr );

    std::shared_ptr<zagorodnev::Shape> trianglePtr
    = std::make_shared<zagorodnev::Triangle>(zagorodnev::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape( trianglePtr );

    zagorodnev::CompositeShape newTest(std::move(shape));
    BOOST_CHECK_EQUAL( newTest[0], rectanglePtr );
    BOOST_CHECK_EQUAL( newTest[1], trianglePtr );

    size_t quantity = 2;
    BOOST_CHECK_EQUAL(newTest.getSize(), quantity);
  }

  BOOST_AUTO_TEST_CASE(InvarienceOfArea)
  {
    std::shared_ptr<zagorodnev::Shape> rectanglePtr
    = std::make_shared<zagorodnev::Rectangle>(zagorodnev::Rectangle({168.0, 256.0}, 20.0, 58.0));
    zagorodnev::CompositeShape shape(rectanglePtr);

    std::shared_ptr<zagorodnev::Shape> circlePtr
    = std::make_shared<zagorodnev::Circle>(zagorodnev::Circle({10.0, 10.0}, 5.0));
    shape.addShape(circlePtr);

    std::shared_ptr<zagorodnev::Shape> trianglePtr
    = std::make_shared<zagorodnev::Triangle>(zagorodnev::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);

    double area = shape.getArea();
    shape.move(16.0, 32.0);
    BOOST_REQUIRE_CLOSE_FRACTION(shape.getArea(), area, E);
  }

  BOOST_AUTO_TEST_CASE(InvarienceOfCoords)
  {
    const double RATIO = 1.6;
    std::shared_ptr<zagorodnev::Shape> rectanglePtr
    = std::make_shared<zagorodnev::Rectangle>(zagorodnev::Rectangle({128.0, 256.0}, 24.0, 48.0));
    zagorodnev::CompositeShape shape(rectanglePtr);

    std::shared_ptr<zagorodnev::Shape> circlePtr
    = std::make_shared<zagorodnev::Circle>(zagorodnev::Circle({128.0, 256.0}, 4.0));
    shape.addShape(circlePtr);

    std::shared_ptr<zagorodnev::Shape> trianglePtr
    = std::make_shared<zagorodnev::Triangle>(zagorodnev::Triangle({2.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);
    double posX = shape.getFrameRect().pos.x;
    double posY = shape.getFrameRect().pos.y;
    shape.scale(RATIO);

    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.x, posX,E);
    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.y, posY,E);
  }

  BOOST_AUTO_TEST_CASE(Scale)
  {
    const double RATIO = 1.6;
    std::shared_ptr<zagorodnev::Shape> rectanglePtr
    = std::make_shared<zagorodnev::Rectangle>(zagorodnev::Rectangle({128.0, 256.0}, 24.0, 48.0));
    zagorodnev::CompositeShape shape(rectanglePtr);

    std::shared_ptr<zagorodnev::Shape> circlePtr
    = std::make_shared<zagorodnev::Circle>(zagorodnev::Circle({128.0, 256.0}, 4.0));
    shape.addShape(circlePtr);

    std::shared_ptr<zagorodnev::Shape> trianglePtr
    = std::make_shared<zagorodnev::Triangle>(zagorodnev::Triangle({2.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);
    double area = shape.getArea();
    double posX = shape.getFrameRect().pos.x;
    double posY = shape.getFrameRect().pos.y;
    shape.scale(RATIO);

    BOOST_CHECK_CLOSE(shape.getArea(), area*RATIO*RATIO, E);
    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.x, posX, E);
    BOOST_CHECK_CLOSE(shape.getFrameRect().pos.y, posY, E);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentConstructor)
  {
    std::shared_ptr<zagorodnev::Shape> rectanglePtr
    = std::make_shared<zagorodnev::Rectangle>(zagorodnev::Rectangle({168.0, 256.0}, 20.0, 58.0));
    zagorodnev::CompositeShape shape(rectanglePtr);
    std::shared_ptr<zagorodnev::Shape> circlePtr
    = std::make_shared<zagorodnev::Circle>(zagorodnev::Circle({10.0, 10.0}, 5.0));
    shape.addShape(circlePtr);
    std::shared_ptr<zagorodnev::Shape> trianglePtr
    = std::make_shared<zagorodnev::Triangle>(zagorodnev::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    shape.addShape(trianglePtr);
    BOOST_REQUIRE_THROW(zagorodnev::CompositeShape shape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentAddShape)
  {
    std::shared_ptr<zagorodnev::Shape> rectanglePtr
    = std::make_shared<zagorodnev::Rectangle>(zagorodnev::Rectangle({168.0, 256.0}, 20.0, 58.0));
    zagorodnev::CompositeShape shape(rectanglePtr);

    BOOST_REQUIRE_THROW(shape.addShape(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentScale)
  {
    std::shared_ptr<zagorodnev::Shape> rectanglePtr
    = std::make_shared<zagorodnev::Rectangle>(zagorodnev::Rectangle({168.0, 256.0}, 20.0, 58.0));
    zagorodnev::CompositeShape shape(rectanglePtr);

    BOOST_CHECK_THROW(shape.scale(-1.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(OPERATOR_COPY)
  {
    zagorodnev::Rectangle rect1({1.5, 1.5}, 1, 1);
    zagorodnev::Rectangle rect2({2.5, 2.5}, 1, 1);
    zagorodnev::Rectangle rect3({3.5, 3.5}, 1, 1);

    std::shared_ptr<zagorodnev::Shape> RectanglePtr1 = std::make_shared<zagorodnev::Rectangle>(rect1);
    std::shared_ptr<zagorodnev::Shape> RectanglePtr2 = std::make_shared<zagorodnev::Rectangle>(rect2);
    std::shared_ptr<zagorodnev::Shape> RectanglePtr3 = std::make_shared<zagorodnev::Rectangle>(rect3);

    zagorodnev::CompositeShape testcompositeShape(RectanglePtr1);
    testcompositeShape.addShape(RectanglePtr2);
    testcompositeShape.addShape(RectanglePtr3);

    zagorodnev::Rectangle rect4({1.5, 1.5}, 2, 1);
    zagorodnev::Rectangle rect5({2.5, 2.5}, 2, 1);
    zagorodnev::Rectangle rect6({3.5, 3.5}, 2, 1);

    std::shared_ptr<zagorodnev::Shape> RectanglePtr4 = std::make_shared<zagorodnev::Rectangle>(rect4);
    std::shared_ptr<zagorodnev::Shape> RectanglePtr5 = std::make_shared<zagorodnev::Rectangle>(rect5);
    std::shared_ptr<zagorodnev::Shape> RectanglePtr6 = std::make_shared<zagorodnev::Rectangle>(rect6);

    zagorodnev::CompositeShape testcompositeShape1(RectanglePtr4);
    testcompositeShape1.addShape(RectanglePtr5);
    testcompositeShape1.addShape(RectanglePtr6);

    testcompositeShape1 = testcompositeShape;

    for (size_t i = 0; i < testcompositeShape.getSize(); ++i) {
      zagorodnev::rectangle_t rectangle = testcompositeShape[i]->getFrameRect();
      zagorodnev::rectangle_t rectangle1 = testcompositeShape1[i]->getFrameRect();
      BOOST_CHECK_EQUAL(rectangle.height, rectangle1.height);
      BOOST_CHECK_EQUAL(rectangle.width, rectangle1.width);
      BOOST_CHECK_EQUAL(rectangle.pos.x, rectangle1.pos.x);
      BOOST_CHECK_EQUAL(rectangle.pos.y, rectangle1.pos.y);
    }
  }

  BOOST_AUTO_TEST_CASE(OPERATOR_MOVE)
  {
    zagorodnev::Rectangle rect1({1.5, 1.5}, 1, 1);
    zagorodnev::Rectangle rect2({2.5, 2.5}, 1, 1);
    zagorodnev::Rectangle rect3({3.5, 3.5}, 1, 1);

    std::shared_ptr<zagorodnev::Shape> RectanglePtr1 = std::make_shared<zagorodnev::Rectangle>(rect1);
    std::shared_ptr<zagorodnev::Shape> RectanglePtr2 = std::make_shared<zagorodnev::Rectangle>(rect2);
    std::shared_ptr<zagorodnev::Shape> RectanglePtr3 = std::make_shared<zagorodnev::Rectangle>(rect3);

    zagorodnev::CompositeShape testcompositeShape(RectanglePtr1);
    testcompositeShape.addShape(RectanglePtr2);
    testcompositeShape.addShape(RectanglePtr3);

    zagorodnev::Rectangle rect4({1.5, 1.5}, 2, 1);
    zagorodnev::Rectangle rect5({2.5, 2.5}, 2, 1);
    zagorodnev::Rectangle rect6({3.5, 3.5}, 2, 1);

    std::shared_ptr<zagorodnev::Shape> RectanglePtr4 = std::make_shared<zagorodnev::Rectangle>(rect1);
    std::shared_ptr<zagorodnev::Shape> RectanglePtr5 = std::make_shared<zagorodnev::Rectangle>(rect5);
    std::shared_ptr<zagorodnev::Shape> RectanglePtr6 = std::make_shared<zagorodnev::Rectangle>(rect6);

    zagorodnev::CompositeShape testcompositeShape1(RectanglePtr4);
    testcompositeShape.addShape(RectanglePtr5);
    testcompositeShape.addShape(RectanglePtr6);

    testcompositeShape1 = std::move(testcompositeShape);

    for (size_t i = 0; i < testcompositeShape.getSize(); ++i) {
      zagorodnev::rectangle_t rectangle = testcompositeShape[i]->getFrameRect();
      zagorodnev::rectangle_t rectangle1 = testcompositeShape1[i]->getFrameRect();
      BOOST_CHECK_EQUAL(rectangle.height, rectangle1.height);
      BOOST_CHECK_EQUAL(rectangle.width, rectangle1.width);
      BOOST_CHECK_EQUAL(rectangle.pos.x, rectangle1.pos.x);
      BOOST_CHECK_EQUAL(rectangle.pos.y, rectangle1.pos.y);
    }
  }
  
BOOST_AUTO_TEST_SUITE_END()
