#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
//#include <dbghelp.h>
#include "rectangle.hpp"
#include "circle.hpp"
#include "cmath"
#include "triangle.hpp"
#include "polygon.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

const double E = 0.001;

BOOST_AUTO_TEST_SUITE(Rotate_Tests)

  BOOST_AUTO_TEST_CASE(CircleTest)
  {
    zagorodnev::point_t point = {12.0, 16.0};
    zagorodnev::Circle circle(point, 29.0);
    zagorodnev::rectangle_t rectBefore = circle.getFrameRect();
    circle.rotate(45.0);
    zagorodnev::rectangle_t rectAfter = circle.getFrameRect();
    BOOST_CHECK_CLOSE(rectBefore.pos.x, rectAfter.pos.x, E);
    BOOST_CHECK_CLOSE(rectBefore.pos.y, rectAfter.pos.y, E);
  }

  BOOST_AUTO_TEST_CASE(RectangleTest)
  {
    zagorodnev::point_t point = {12.0, 16.0};
    zagorodnev::Rectangle rect(point, 10.0, 20.0);
    zagorodnev::rectangle_t rectBefore = rect.getFrameRect();
    rect.rotate(55.0);
    zagorodnev::rectangle_t rectAfter = rect.getFrameRect();
    BOOST_CHECK_CLOSE(rectBefore.height, rectAfter.height, E);
    BOOST_CHECK_CLOSE(rectBefore.width, rectAfter.width, E);
    BOOST_CHECK_CLOSE(rectBefore.pos.x, rectAfter.pos.x, E);
    BOOST_CHECK_CLOSE(rectBefore.pos.y, rectAfter.pos.y, E);
  }
  BOOST_AUTO_TEST_CASE(TriangleTest)
  {
     zagorodnev::Triangle triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0});
     zagorodnev::rectangle_t rectBefore = triangle.getFrameRect();
     triangle.rotate(50.0);
     zagorodnev::rectangle_t rectAfter = triangle.getFrameRect();
     BOOST_CHECK_CLOSE(rectBefore.pos.x, rectAfter.pos.x, E);
     BOOST_CHECK_CLOSE(rectBefore.pos.y, rectAfter.pos.y, E);
  }

  BOOST_AUTO_TEST_CASE(CompositeShapeTest)
  {
    zagorodnev::point_t point = {12.0, 16.0};
    std::shared_ptr<zagorodnev::Shape> rectPtr (new zagorodnev::Rectangle(point, 50.0, 40.0));
    std::shared_ptr<zagorodnev::Shape> circlePtr (new zagorodnev::Circle(point, 56.0));
    std::shared_ptr<zagorodnev::Shape> trianglePtr (new zagorodnev::Triangle({3.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    zagorodnev::CompositeShape comp_shape(rectPtr);
    comp_shape.addShape(circlePtr);
    comp_shape.addShape(trianglePtr);
    zagorodnev::rectangle_t comp_shapeBefore = comp_shape.getFrameRect();
    comp_shape.rotate(60.0);
    zagorodnev::rectangle_t comp_shapeAfter = comp_shape.getFrameRect();
    BOOST_CHECK_CLOSE(comp_shapeBefore.height, comp_shapeAfter.height, E);
    BOOST_CHECK_CLOSE(comp_shapeBefore.width, comp_shapeAfter.width, E);
    BOOST_CHECK_CLOSE(comp_shapeBefore.pos.x, comp_shapeAfter.pos.x, E);
    BOOST_CHECK_CLOSE(comp_shapeBefore.pos.y, comp_shapeAfter.pos.y, E);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Matrix_Tests)

  BOOST_AUTO_TEST_CASE(AddShapeTest)
  {
    zagorodnev::point_t point = {12.0, 16.0};
    std::shared_ptr<zagorodnev::Shape> rectPtr (new zagorodnev::Rectangle(point, 50.0, 40.0));
    std::shared_ptr<zagorodnev::Shape> circlePtr (new zagorodnev::Circle(point, 56.0));
    std::shared_ptr<zagorodnev::Shape> trianglePtr(new zagorodnev::Triangle({2.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    zagorodnev::Matrix matrix(rectPtr);
    matrix.addShape(circlePtr);
    matrix.addShape(trianglePtr);

    BOOST_CHECK_CLOSE(matrix[0][0] -> getFrameRect().width, rectPtr -> getFrameRect().width, E);
    BOOST_CHECK_CLOSE(matrix[0][0] -> getFrameRect().height, rectPtr -> getFrameRect().height, E);
    BOOST_CHECK_CLOSE(matrix[1][0] -> getFrameRect().width, circlePtr -> getFrameRect().width, E);
    BOOST_CHECK_CLOSE(matrix[1][0] -> getFrameRect().height, circlePtr -> getFrameRect().height, E);
    BOOST_CHECK_CLOSE(matrix[2][0] -> getFrameRect().width, trianglePtr -> getFrameRect().width, E);
    BOOST_CHECK_CLOSE(matrix[2][0] -> getFrameRect().height, trianglePtr -> getFrameRect().height, E);
  }

  BOOST_AUTO_TEST_CASE(AddCompositeShapeTest)
  {
    zagorodnev::point_t point = {12.0, 16.0};
    std::shared_ptr<zagorodnev::Shape> rectPtr1 (new zagorodnev::Rectangle(point, 20.0, 300.0));
    std::shared_ptr<zagorodnev::Shape> circlePtr (new zagorodnev::Circle(point, 76.0));
    std::shared_ptr<zagorodnev::Shape> trianglePtr(new zagorodnev::Triangle({2.0, 6.0}, {4.0, 4.0}, {7.0, 5.0}));
    point = {24.0, 14.0};
    std::shared_ptr<zagorodnev::Shape> rectPtr2 (new zagorodnev::Rectangle(point, 33.0, 60.0));
    zagorodnev::CompositeShape comp_shape(rectPtr1);
    zagorodnev::Matrix matrix(rectPtr1);
    comp_shape.addShape(rectPtr2);
    matrix.addShape(rectPtr2);

    std::shared_ptr<zagorodnev::Shape> compPtr (new zagorodnev::CompositeShape(comp_shape));
    matrix.addShape(compPtr);

    BOOST_CHECK_CLOSE_FRACTION(matrix[0][0] -> getFrameRect().width, rectPtr1 -> getFrameRect().width, E);
    BOOST_CHECK_CLOSE_FRACTION(matrix[0][0] -> getFrameRect().height, rectPtr1 -> getFrameRect().height, E);
  }

  BOOST_AUTO_TEST_CASE(InvalidArgumentConstructorTest)
  {
    BOOST_REQUIRE_THROW(zagorodnev::Matrix matrix_shape(nullptr), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(PolygonTests)

  BOOST_AUTO_TEST_CASE(ConstructorAreaTest)
  {
    size_t size = 4;
    zagorodnev::point_t ver[size] = {{0, 0},
      {0, 0},
      {0, 0},
      {0, 0}};
    std::unique_ptr<zagorodnev::point_t[]> array(new zagorodnev::point_t[size]);
    for (int i = 0; i < 4; ++i)
      array[i] = ver[i];

    BOOST_CHECK_THROW(zagorodnev::Polygon pol(array, size), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(ConstructorSizeTest)
  {
    size_t size = 3;
    zagorodnev::point_t ver[size] = {{0, 1},
      {1, 0},
      {3, 3}};
    std::unique_ptr<zagorodnev::point_t[]> array(new zagorodnev::point_t[size]);
    for (int i = 0; i < 3; ++i)
      array[i] = ver[i];

    BOOST_CHECK_THROW(zagorodnev::Polygon pol(array, size), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Constructor_convex)
  {
    size_t size = 4;
    zagorodnev::point_t ver[size] = {{-2, 1},
      {-5, 1},
      {-3, 2},
      {-2, 4}};
    std::unique_ptr<zagorodnev::point_t[]> array(new zagorodnev::point_t[size]);
    for (int i = 0; i < 4; ++i)
      array[i] = ver[i];

    BOOST_CHECK_THROW(zagorodnev::Polygon pol(array, size), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Move_ToPoint)
  {
    size_t size = 4;
    zagorodnev::point_t ver[size] = {{0, 0}, {2, 2}, {4, 2}, {2, 0}};
    std::unique_ptr<zagorodnev::point_t[]> array(new zagorodnev::point_t[size]);
    for (int i = 0; i < 4; ++i)
      array[i] = ver[i];
    zagorodnev::Polygon pol(array, size);
    pol.move({4.0, 5.0});
    BOOST_CHECK_CLOSE(pol.getFrameRect().width, 4, E);
    BOOST_CHECK_CLOSE(pol.getFrameRect().height, 2, E);
    BOOST_CHECK_CLOSE(pol.getFrameRect().pos.x, 4, E);
    BOOST_CHECK_CLOSE(pol.getFrameRect().pos.y, 5, E);
    BOOST_CHECK_CLOSE(pol.getArea(), 4, E);
  }

  BOOST_AUTO_TEST_CASE(Move_Delta)
  {
    size_t size = 4;
    zagorodnev::point_t ver[size] = {
      { 0, 0 },
      { 2, 2 },
      { 4, 2 },
      { 2, 0 }};
    std::unique_ptr<zagorodnev::point_t[]> array(new zagorodnev::point_t[size]);
    for (int i = 0; i < 4; ++i)
      array[i] = ver[i];
    zagorodnev::Polygon pol(array, size);
    pol.move(4.0, 5.0);
    BOOST_CHECK_CLOSE(pol.getFrameRect().width, 4, E);
    BOOST_CHECK_CLOSE(pol.getFrameRect().height, 2, E);
    BOOST_CHECK_CLOSE(pol.getFrameRect().pos.x, 6, E);
    BOOST_CHECK_CLOSE(pol.getFrameRect().pos.y, 6, E);
    BOOST_CHECK_CLOSE(pol.getArea(), 4, E);
  }

  BOOST_AUTO_TEST_CASE(getArea)
  {
    size_t size = 4;
    zagorodnev::point_t ver[size] = {{0, 0},
      {2, 2},
      {4, 2},
      {2, 0}};
    std::unique_ptr<zagorodnev::point_t[]> array(new zagorodnev::point_t[size]);
    for (int i = 0; i < 4; ++i)
      array[i] = ver[i];
    zagorodnev::Polygon pol(array, size);
    BOOST_CHECK_CLOSE(pol.getArea(), 4, E);
  }


  BOOST_AUTO_TEST_CASE(scale)
  {
    size_t size = 4;
    zagorodnev::point_t ver[size] = {{0, 0},
      {2, 2},
      {4, 2},
      {2, 0}};
    std::unique_ptr<zagorodnev::point_t[]> array(new zagorodnev::point_t[size]);
    for (int i = 0; i < 4; ++i)
      array[i] = ver[i];
    zagorodnev::Polygon pol(array, size);
    pol.scale(2);
    BOOST_CHECK_CLOSE(pol.getArea(), 16, E);
  }

  BOOST_AUTO_TEST_CASE(rotate)
  {
    size_t size = 4;
    zagorodnev::point_t ver[size] = {{0, 0},
      {2, 2},
      {4, 2},
      {2, 0}};
    std::unique_ptr<zagorodnev::point_t[]> array(new zagorodnev::point_t[size]);
    for (int i = 0; i < 4; ++i)
      array[i] = ver[i];
    zagorodnev::Polygon pol1(array, size);
    zagorodnev::Polygon pol2(array, size);
    double angle = 45.0;
    pol1.rotate(angle);
    BOOST_CHECK_CLOSE(pol1.getFrameRect().pos.x, pol2.getFrameRect().pos.x, E);
    BOOST_CHECK_CLOSE(pol1.getFrameRect().pos.y, pol2.getFrameRect().pos.y, E);
    BOOST_CHECK_CLOSE(pol1.getArea(), pol2.getArea(), E);
  }

BOOST_AUTO_TEST_SUITE_END()
