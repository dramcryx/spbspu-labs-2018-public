#ifndef BASETYPES_H
#define BASETYPES_H

namespace zagorodnev
{
    struct point_t
    {
        double x;
        double y;
    };
    struct rectangle_t
    {
        point_t pos;
        double width;
        double height;
    };
}
#endif
