#ifndef COMPOSITESHAPE_HPP
#define COMPOSITESHAPE_HPP

#include "shape.hpp"
#include <memory>

namespace zagorodnev
{
  class CompositeShape: public Shape
  {
  public:
    CompositeShape(std::shared_ptr<zagorodnev::Shape> shape_ptr);
    CompositeShape(const zagorodnev::CompositeShape & obj);
    CompositeShape(zagorodnev::CompositeShape && obj);

    CompositeShape &operator =(const zagorodnev::CompositeShape & obj);
    CompositeShape &operator =(zagorodnev::CompositeShape && obj);
    std::shared_ptr<Shape> operator [](size_t index);

    void addShape(std::shared_ptr <zagorodnev::Shape> shape_ptr);
    void deleteShape(size_t index);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t & point) override;
    void move(double dx, double dy) override;
    void scale(double ratio) override;
    size_t getSize() const noexcept;
    void rotate(double alpha) override;

  private:
    std::unique_ptr <std::shared_ptr <zagorodnev::Shape>[]> shapes_;
    point_t center_;
    size_t size_;
    double alpha_;
  };
}
#endif
