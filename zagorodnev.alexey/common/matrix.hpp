#ifndef A3_MATRIX_HPP
#define A3_MATRIX_HPP
#include "shape.hpp"
#include "composite-shape.hpp"
#include <memory>

namespace zagorodnev
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const std::shared_ptr<zagorodnev::Shape> shape_ptr);
    Matrix(const zagorodnev::Matrix & obj);
    Matrix(zagorodnev::Matrix && obj);
    ~Matrix();

    Matrix &operator =(const zagorodnev::Matrix & obj);
    Matrix &operator =(zagorodnev::Matrix && obj);
    std::unique_ptr<std::shared_ptr<zagorodnev::Shape>[]> operator [](const size_t index);

    void addShape(const std::shared_ptr <zagorodnev::Shape> shape_ptr);

  private:
    std::unique_ptr<std::shared_ptr<zagorodnev::Shape>[]> matrix_;
    size_t layersNumber_;
    size_t layersSize_;

    bool checkOverlapping(const int index, std::shared_ptr<zagorodnev::Shape> obj) const;
  };
}

#endif
