

#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <memory>
#include "shape.hpp"

namespace zagorodnev
{
  class Polygon : public zagorodnev::Shape
  {
  public:
    explicit Polygon(const std::unique_ptr<zagorodnev::point_t[]> & array, size_t size);
      zagorodnev::point_t &operator[](unsigned int i) const;
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(const point_t &pos) override;
    void move(double Ox, double Oy) override;
    void scale(double ratio) override;
    void rotate(double angle) override;
    point_t getCenter() const;
    bool isConvex() const;

  private:
    size_t size_;
    std::unique_ptr<zagorodnev::point_t[]> ver_;

    point_t center_;
  };
}
#endif
