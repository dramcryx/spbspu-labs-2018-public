#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "shape.hpp"
#include "base-types.hpp"

namespace zagorodnev
{
  class Rectangle : public Shape
  {
  public:
    Rectangle(const point_t &center, double width, double height);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void move(double dx, double dy) override;
    void move(const point_t &p) override;
    void scale(double ratio) override;
    void rotate(double alpha) override;


  private:
    point_t center_;
    double width_;
    double height_;
    double alpha_;
    point_t corners_[4];
    point_t getPosition() const;
  };
}
#endif
