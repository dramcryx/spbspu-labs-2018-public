#ifndef SHAPE_H
#define SHAPE_H
#include <memory>
#include "base-types.hpp"

namespace zagorodnev
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void move(double dx, double dy) = 0;
    virtual void move(const point_t &p) = 0;
    virtual void scale(double ratio) = 0;
    virtual void rotate(double alpha) = 0;
  };
}
#endif
