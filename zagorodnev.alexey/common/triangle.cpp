#include "triangle.hpp"
#include <algorithm>
#include <stdexcept>
#include <math.h>
#include "base-types.hpp"

zagorodnev::Triangle::Triangle(const point_t &p1, const point_t &p2, const point_t &p3) :
  a_(p1),
  b_(p2),
  c_(p3)
{
  if(getArea() <= 0.0)
  {
    throw std::invalid_argument("Invalid triangle parameters!");
  }
}
double zagorodnev::Triangle::getArea() const
{
  double ab = getDistance(a_, b_);
  double ac = getDistance(a_, c_);
  double bc = getDistance(b_, c_);
  double s = (ab + ac + bc) / 2;
  return std::sqrt(s*(s - ab)*(s - ac)*(s - bc));
}
zagorodnev::rectangle_t zagorodnev::Triangle::getFrameRect() const
{
  using namespace std;
  double left = min(min(a_.x, b_.x), c_.x);
  double bot = min(min(a_.y, b_.y), c_.y);
  double h = fabs(max(max(a_.y, b_.y), c_.y) - bot);
  double w = fabs(max(max(a_.x, b_.x), c_.x) - left);
  return rectangle_t{ findCenter(), w, h };
}
void zagorodnev::Triangle::move(double dx, double dy)
{
  a_.x += dx;
  b_.x += dx;
  c_.x += dx;
  a_.y += dy;
  b_.y += dy;
  c_.y += dy;
}
void zagorodnev::Triangle::move(const point_t &p)
{
  move(p.x - findCenter().x, p.y - findCenter().y);
}

zagorodnev::point_t zagorodnev::Triangle::findCenter() const
{
  return point_t{ (a_.x + b_.x + c_.x) / 3, (a_.y + b_.y + c_.y) / 3 };
}

double zagorodnev::Triangle::getDistance(const point_t &p1, const point_t &p2)
{
  return std::sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y));
}

void zagorodnev::Triangle::scale(const double scal)
{
  if (scal < 0.0)
  {
    throw std::invalid_argument("Invalid triangle scale coefficient!");
  }
  double CenterX = findCenter().x;
  double CenterY = findCenter().y;
  a_.x = (a_.x - CenterX) * scal + CenterX;
  a_.y = (a_.y - CenterY) * scal + CenterY;
  b_.x = (b_.x - CenterX) * scal + CenterX;
  b_.y = (b_.y - CenterY) * scal + CenterY;
  c_.x = (c_.x - CenterX) * scal + CenterX;
  c_.y = (c_.y - CenterY) * scal + CenterY;
}

void zagorodnev::Triangle::rotate(double angle)
{
  point_t centre = Triangle::getFrameRect().pos;
  angle = (angle * M_PI) / 180;
  const double sin_a = sin(angle);
  const double cos_a = cos(angle);
  a_ = { centre.x + (a_.x - centre.x) * cos_a - (a_.y - centre.y) * sin_a, centre.y + (a_.y - centre.y) * cos_a + (a_.x - centre.x) * sin_a };
  b_ = { centre.x + (b_.x - centre.x) * cos_a - (b_.y - centre.y) * sin_a, centre.y + (b_.y - centre.y) * cos_a + (b_.x - centre.x) * sin_a };
  c_ = { centre.x + (c_.x - centre.x) * cos_a - (c_.y - centre.y) * sin_a, centre.y + (c_.y - centre.y) * cos_a + (c_.x - centre.x) * sin_a };
}
