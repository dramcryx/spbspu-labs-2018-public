#define BOOST_TEST_MODULE main
#define BOOST_TEST_DYN_LINK

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include <memory>
#include "circle.hpp"
#include "rectangle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"

BOOST_AUTO_TEST_SUITE(MovingToPointTests)

BOOST_AUTO_TEST_CASE(testRectangleInMovingToPoint)
  {
    zagorodniy::Rectangle myRectangle({5.0, 6.0}, 7.0, 8.0);
  zagorodniy::rectangle_t rectangleOne = myRectangle.getFrameRect();
  double areaOne = myRectangle.getArea();

  myRectangle.move({1.0,2.5});

  BOOST_CHECK_EQUAL(areaOne, myRectangle.getArea());
  BOOST_CHECK_EQUAL(rectangleOne.width, myRectangle.getFrameRect().width);
  BOOST_CHECK_EQUAL(rectangleOne.height, myRectangle.getFrameRect().height);
  }

BOOST_AUTO_TEST_CASE(testCircleInMovingToPoint)
  {
    zagorodniy::Circle myCircle({10.0, 11.0}, 9.0);
  zagorodniy::rectangle_t circleOne = myCircle.getFrameRect();
  double areaOne = myCircle.getArea();

  myCircle.move({1.0,2.5});

  BOOST_CHECK_EQUAL(areaOne, myCircle.getArea());
  BOOST_CHECK_EQUAL(circleOne.width, myCircle.getFrameRect().width);
  BOOST_CHECK_EQUAL(circleOne.height, myCircle.getFrameRect().height);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MovingTests)

BOOST_AUTO_TEST_CASE(testRectangleInMoving)
  {
    zagorodniy::Rectangle myRectangle({5.0, 6.0}, 7.0, 8.0);
  zagorodniy::rectangle_t rectangleOne = myRectangle.getFrameRect();
  double areaOne = myRectangle.getArea();

  myRectangle.move(3.5,4.3);

  BOOST_CHECK_EQUAL(areaOne, myRectangle.getArea());
  BOOST_CHECK_EQUAL(rectangleOne.width, myRectangle.getFrameRect().width);
  BOOST_CHECK_EQUAL(rectangleOne.height, myRectangle.getFrameRect().height);
  }

BOOST_AUTO_TEST_CASE(testCircleInMoving)
  {
    zagorodniy::Circle myCircle({10.0, 11.0}, 9.0);
  zagorodniy::rectangle_t circleOne = myCircle.getFrameRect();
  double areaOne = myCircle.getArea();

  myCircle.move(3.5,4.3);

  BOOST_CHECK_EQUAL(areaOne, myCircle.getArea());
  BOOST_CHECK_EQUAL(circleOne.width, myCircle.getFrameRect().width);
  BOOST_CHECK_EQUAL(circleOne.height, myCircle.getFrameRect().height);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(ScalingTests)

BOOST_AUTO_TEST_CASE(testRectangleInScaling)
  {
    zagorodniy::Rectangle myRectangle({5.0, 6.0}, 7.0, 8.0);
  double areaOne = myRectangle.getArea();

  myRectangle.scale(2.0);

  BOOST_CHECK_CLOSE(2.0 * 2.0 * areaOne, myRectangle.getArea(), 0.001);
  }

BOOST_AUTO_TEST_CASE(testCircleInScaling)
  {
    zagorodniy::Circle myCircle({10.0, 11.0}, 9.0);
  double areaOne = myCircle.getArea();

  myCircle.scale(2.0);

  BOOST_CHECK_CLOSE(2.0 * 2.0 * areaOne, myCircle.getArea(), 0.001);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(ParametersTests)

BOOST_AUTO_TEST_CASE(testRectangleParameters)
  {
    BOOST_CHECK_THROW( zagorodniy::Rectangle({5.0, 6.0}, 7.0, -8.0), std::invalid_argument );
  BOOST_CHECK_THROW( zagorodniy::Rectangle({5.0, 6.0}, -7.0, 8.0), std::invalid_argument );
  BOOST_CHECK_THROW( zagorodniy::Rectangle({5.0, 6.0}, -7.0, -8.0), std::invalid_argument );
  }

BOOST_AUTO_TEST_CASE(testCircleParameters)
  {
    BOOST_CHECK_THROW( zagorodniy::Circle({10.0, 11.0}, -9.0), std::invalid_argument );
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(IncreaseCoefficientTests)

BOOST_AUTO_TEST_CASE(testIncreaseCoefficientInRectangle)
  {
    zagorodniy::Rectangle myRectangle({5.0, 6.0}, 7.0, 8.0);
  BOOST_CHECK_THROW( myRectangle.scale(-2.0), std::invalid_argument );
  }

BOOST_AUTO_TEST_CASE(testIncreaseCoefficientInCircle)
  {
    zagorodniy::Circle myCircle({10.0, 11.0}, 9.0);
  BOOST_CHECK_THROW( myCircle.scale(-2.0), std::invalid_argument );
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeTests)

  zagorodniy::Circle myCircle({10.0, 11.0}, 5.0);
  zagorodniy::Rectangle myRectangle({5.0, 6.0}, 7.0, 8.0);
  std::shared_ptr< zagorodniy::Shape > circlePtr = std::make_shared< zagorodniy::Circle >(myCircle);
  std::shared_ptr< zagorodniy::Shape > rectanglePtr = std::make_shared< zagorodniy::Rectangle >
    (myRectangle);

  BOOST_AUTO_TEST_CASE(testConstructorAndAdding)
  {
    zagorodniy::CompositeShape myCS(circlePtr);
    myCS.addShape(rectanglePtr);
    BOOST_CHECK_EQUAL(myCS.getCount(), 2);
  }

  BOOST_AUTO_TEST_CASE(wrongConstructorParameter)
  {
    zagorodniy::CompositeShape myCS(circlePtr);
    BOOST_CHECK_THROW(zagorodniy::CompositeShape myCS(nullptr), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(areaTest)
  {
    zagorodniy::CompositeShape myCS(circlePtr);
    myCS.addShape(rectanglePtr);
    BOOST_CHECK_CLOSE(myCS.getArea(), myRectangle.getArea() + myCircle.getArea(), 0.001);
  }

  BOOST_AUTO_TEST_CASE(frameRectTest)
  {
    zagorodniy::CompositeShape myCS(circlePtr);
    myCS.addShape(rectanglePtr);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().pos.x, 8.25);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().pos.y, 9.0);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().width, 13.5);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().height, 14.0);
  }

  BOOST_AUTO_TEST_CASE(moveFor)
  {
    zagorodniy::CompositeShape myCS(circlePtr);
    myCS.addShape(rectanglePtr);
    zagorodniy::rectangle_t rectangleOne = myCS.getFrameRect();
    myCS.move(4.5, 5.5);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().pos.x, rectangleOne.pos.x + 4.5);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().pos.y, rectangleOne.pos.y + 5.5);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().width, rectangleOne.width);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().height, rectangleOne.height);
  }

  BOOST_AUTO_TEST_CASE(moveTo)
  {
    zagorodniy::CompositeShape myCS(circlePtr);
    myCS.addShape(rectanglePtr);
    zagorodniy::rectangle_t rectangleOne = myCS.getFrameRect();
    myCS.move({2.0, 3.0});
    BOOST_CHECK_EQUAL(myCS.getFrameRect().pos.x, 2.0);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().pos.y, 3.0);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().width, rectangleOne.width);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().height, rectangleOne.height);
  }

  BOOST_AUTO_TEST_CASE(scale)
  {
    zagorodniy::CompositeShape myCS(circlePtr);
    myCS.addShape(rectanglePtr);
    zagorodniy::rectangle_t rectangleOne = myCS.getFrameRect();
    myCS.scale(2.0);
    myCS.getFrameRect();
    BOOST_CHECK_EQUAL(myCS.getFrameRect().pos.x, rectangleOne.pos.x);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().pos.y, rectangleOne.pos.y);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().width, rectangleOne.width * 2.0);
    BOOST_CHECK_EQUAL(myCS.getFrameRect().height, rectangleOne.height * 2.0);
  }

  BOOST_AUTO_TEST_CASE(negativeScaleParameters)
  {
    zagorodniy::CompositeShape myCS(circlePtr);
    myCS.addShape(rectanglePtr);
    BOOST_CHECK_THROW(myCS.scale(-2.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(removeShape)
  {
    zagorodniy::CompositeShape myCS(circlePtr);
    myCS.addShape(rectanglePtr);
    myCS.removeShape(1);
    BOOST_CHECK_EQUAL(myCS.getCount(), 1);
  }

  BOOST_AUTO_TEST_CASE(removeBiggerShape)
  {
    zagorodniy::CompositeShape myCS(circlePtr);
    myCS.addShape(rectanglePtr);
    BOOST_CHECK_THROW(myCS.removeShape(8), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(RotationTests)

  BOOST_AUTO_TEST_CASE(RectangleRotate) {
    zagorodniy::Rectangle Rectangle({3.0, 3.0}, 2.0, 2.0);
    zagorodniy::rectangle_t Frame = Rectangle.getFrameRect();
    Rectangle.rotate(390.0);
    BOOST_CHECK_EQUAL(Rectangle.getAngle(), 30.0);
    BOOST_CHECK_EQUAL(Frame.pos.x, Rectangle.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(Frame.pos.y, Rectangle.getFrameRect().pos.y);
    BOOST_CHECK_CLOSE_FRACTION(Rectangle.getFrameRect().width, Frame.height * sin(30 * M_PI / 180) + Frame.width * cos(30 * M_PI / 180), 0.00001);
    BOOST_CHECK_CLOSE_FRACTION(Rectangle.getFrameRect().height, Frame.height * cos(30 * M_PI / 180) + Frame.width * sin(30 * M_PI / 180), 0.00001);
}

  BOOST_AUTO_TEST_CASE(CircleRotate) {
    zagorodniy::Circle Circle({3.0, 3.0}, 2.0);
    zagorodniy::rectangle_t Frame = Circle.getFrameRect();
    Circle.rotate(390.0);
    BOOST_CHECK_EQUAL(Circle.getAngle(), 30.0);
    BOOST_CHECK_EQUAL(Frame.pos.x, Circle.getFrameRect().pos.x);
    BOOST_CHECK_EQUAL(Frame.pos.y, Circle.getFrameRect().pos.y);
    BOOST_CHECK_CLOSE_FRACTION(Circle.getFrameRect().width, Frame.width, 0.00001);
    BOOST_CHECK_CLOSE_FRACTION(Circle.getFrameRect().height, Frame.height, 0.00001);
  }

  BOOST_AUTO_TEST_CASE(CSRotate) {
    zagorodniy::Rectangle myRectangle1({2.0, 3.0}, 2.0, 2.0);
    zagorodniy::Rectangle myRectangle2({1.0, 1.0}, 4.0, 2.0);
    std::shared_ptr< zagorodniy::Shape > rectanglePtr1 = std::make_shared< zagorodniy::Rectangle >(myRectangle1);
    std::shared_ptr< zagorodniy::Shape > rectanglePtr2 = std::make_shared< zagorodniy::Rectangle >(myRectangle2);
    zagorodniy::CompositeShape myCS(rectanglePtr1);
    myCS.addShape(rectanglePtr2);
    zagorodniy::rectangle_t Frame = myCS.getFrameRect();
    myCS.rotate(405.0);
    BOOST_CHECK_EQUAL(myCS.getAngle(), 45.0);
    BOOST_CHECK_CLOSE_FRACTION(myCS.getFrameRect().pos.x, 1.707, 0.001);
    BOOST_CHECK_CLOSE_FRACTION(myCS.getFrameRect().pos.y, 2.0, 0.001);
    BOOST_CHECK_CLOSE_FRACTION(myCS.getFrameRect().width, (Frame.height * fabs(sin(45.0 * M_PI / 180.0)) + Frame.width * fabs(cos(45.0 * M_PI / 180.0))) / 4 * 3 , 0.00001);
    BOOST_CHECK_CLOSE_FRACTION(myCS.getFrameRect().height, Frame.height * fabs(cos(45.0 * M_PI / 180.0)) + Frame.width * fabs(sin(45.0 * M_PI / 180.0)), 0.00001);
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTests)

  BOOST_AUTO_TEST_CASE(Matrix_Test) {
    zagorodniy::Rectangle testRect1({0.0, 0.0}, 4.0, 4.0);
    zagorodniy::Circle testCir1({2.0, -2.0}, 1.0);
    zagorodniy::Circle testCir2({4.0, 4.0}, 1.0);
    zagorodniy::Rectangle testRect2({3.0, -3.0}, 4.0, 4.0);
    std::shared_ptr<zagorodniy::Shape> testRectP1 = std::make_shared<zagorodniy::Rectangle>(testRect1);
    std::shared_ptr<zagorodniy::Shape> testCirP1 = std::make_shared<zagorodniy::Circle>(testCir1);
    std::shared_ptr<zagorodniy::Shape> testCirP2 = std::make_shared<zagorodniy::Circle>(testCir2);
    std::shared_ptr<zagorodniy::Shape> testRectP2 = std::make_shared<zagorodniy::Rectangle>(testRect2);
    zagorodniy::Matrix matrix(testRectP1);
    matrix.addShape(testCirP1);
    matrix.addShape(testCirP2);
    matrix.addShape(testRectP2);
    std::unique_ptr<std::shared_ptr<zagorodniy::Shape>[]> layer0 = matrix[0];
    std::unique_ptr<std::shared_ptr<zagorodniy::Shape>[]> layer1 = matrix[1];
    std::unique_ptr<std::shared_ptr<zagorodniy::Shape>[]> layer2 = matrix[2];
    BOOST_CHECK(layer0[0] == testRectP1);
    BOOST_CHECK(layer1[0] == testCirP1);
    BOOST_CHECK(layer0[1] == testCirP2);
    BOOST_CHECK(layer2[0] == testRectP2);
  }

BOOST_AUTO_TEST_SUITE_END()
