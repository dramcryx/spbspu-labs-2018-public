#include "matrix.hpp"
#include <memory>
#include <stdexcept>

using namespace zagorodniy;

Matrix::Matrix(std::shared_ptr <Shape> shape):
  elements_(nullptr),
  layersNumber_(0),
  layerSize_(0) {
  if (shape == nullptr) {
    throw std::invalid_argument("Wrong pointer");
  }
  addShape(shape);
}

Matrix::Matrix(const Matrix & matrix):
  elements_(new std::shared_ptr< Shape >[matrix.layersNumber_ * matrix.layerSize_]),
  layersNumber_(matrix.layersNumber_),
  layerSize_(matrix.layerSize_) {
  for (int i = 0; i < layersNumber_ * layerSize_; ++i) {
    elements_[i] = matrix.elements_[i];
  }
}

Matrix::~Matrix() {
  elements_.reset();
  elements_ = nullptr;
  layerSize_ = 0;
  layersNumber_ = 0;
}

std::unique_ptr<std::shared_ptr<Shape>[]> zagorodniy::Matrix::operator[](const int index) const
{
  if ((index < 0) || (index >= layersNumber_))
  {
    throw std::out_of_range("Invalid index");
  }
  std::unique_ptr<std::shared_ptr<Shape>[]> layer(new std::shared_ptr<Shape>[layerSize_]);
  for (int i = 0; i < layerSize_; ++i)
  {
    layer[i] = elements_[index * layerSize_ + i];
  }
  return layer;
}

void Matrix::addShape(std::shared_ptr< Shape > shape) {
  if (layersNumber_ == 0) {
    ++layerSize_;
    ++layersNumber_;
    std::unique_ptr< std::shared_ptr< Shape >[] > newElements(
      new std::shared_ptr< Shape >[layersNumber_ * layerSize_]);
    elements_.swap(newElements);
    elements_[0] = shape;
  } else {
    bool addedShape = false;
    for (int i = 0; !addedShape ; ++i) {
      for (int j = 0; j < layerSize_; ++j) {
        if (!elements_[i * layerSize_ + j]) {
          elements_[i * layerSize_ + j] = shape;
          addedShape = true;
          break;
        } else {
          if (checkOverlapping(i * layerSize_ + j, shape)) {
            break;
          }
        }
        if (j == (layerSize_ - 1)) {
          layerSize_++;
          std::unique_ptr< std::shared_ptr< Shape >[] > newElements(
            new std::shared_ptr< Shape >[layersNumber_ * layerSize_]);
          for (int n = 0; n < layersNumber_; ++n) {
            for (int m = 0; m < layerSize_ - 1; ++m) {
              newElements[n * layerSize_ + m] = elements_[n * (layerSize_ - 1) + m];
            }
            newElements[(n + 1) * layerSize_ - 1] = nullptr;
          }
          newElements[(i + 1) * layerSize_ - 1] = shape;
          elements_.swap(newElements);
          addedShape = true;
          break;
        }
      }
      if ((i == (layersNumber_ - 1)) && !addedShape) {
        layersNumber_++;
        std::unique_ptr< std::shared_ptr< Shape >[] > newElements(
          new std::shared_ptr< Shape >[layersNumber_ * layerSize_]);
        for (int n = 0; n < ((layersNumber_ - 1) * layerSize_); ++n) {
          newElements[n] = elements_[n];
        }
        for (int n = ((layersNumber_  - 1) * layerSize_) ; n < (layersNumber_ * layerSize_); ++n) {
          newElements[n] = nullptr;
        }
        newElements[(layersNumber_ - 1 ) * layerSize_ ] = shape;
        elements_.swap(newElements);
        addedShape = true;
      }
    }
  }
}

bool Matrix::checkOverlapping(int index, std::shared_ptr< Shape > shape) const {
  rectangle_t nShapeFrameRect = shape->getFrameRect();
  rectangle_t mShapeFrameRect = elements_[index]->getFrameRect();
  point_t newPoints[4] = {
    {nShapeFrameRect.pos.x - nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y + nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x + nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y + nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x + nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y - nShapeFrameRect.height / 2.0},
    {nShapeFrameRect.pos.x - nShapeFrameRect.width / 2.0, nShapeFrameRect.pos.y - nShapeFrameRect.height / 2.0}
  };

  point_t matrixPoints[4] = {
    {mShapeFrameRect.pos.x - mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y + mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x + mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y + mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x + mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y - mShapeFrameRect.height / 2.0},
    {mShapeFrameRect.pos.x - mShapeFrameRect.width / 2.0, mShapeFrameRect.pos.y - mShapeFrameRect.height / 2.0}
  };

  for (int i = 0; i < 4; ++i) {
    if (((newPoints[i].x >= matrixPoints[0].x) && (newPoints[i].x <= matrixPoints[2].x)
         && (newPoints[i].y >= matrixPoints[3].y) && (newPoints[i].y <= matrixPoints[1].y))
        || ((matrixPoints[i].x >= newPoints[0].x) && (matrixPoints[i].x <= newPoints[2].x)
            && (matrixPoints[i].y >= newPoints[3].y) && (matrixPoints[i].y <= newPoints[1].y))) {
      return true;
    }
  }
  return false;
}
