#ifndef A4_MATRIX_HPP
#define A4_MATRIX_HPP

#include <memory>
#include "shape.hpp"

namespace zagorodniy {
  class Matrix {
  public:
    explicit Matrix(std::shared_ptr< Shape > shape);
    Matrix(const Matrix & matrix);
    ~Matrix();
    std::unique_ptr<std::shared_ptr<Shape>[]> operator[](int index) const;
    void addShape(std::shared_ptr< Shape > shape);
    bool checkOverlapping(int index, std::shared_ptr< Shape > shape) const;

  private:
    std::unique_ptr< std::shared_ptr< Shape >[] > elements_;
    int layersNumber_;
    int layerSize_;
  };
}

#endif
