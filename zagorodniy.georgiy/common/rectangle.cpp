#include "rectangle.hpp"
#include <iostream>
#include <cmath>

using namespace zagorodniy;

Rectangle::Rectangle(const point_t & center, const double wdt, const double hgh):
  pos_(center),
  angle_(0) {
    if ((wdt >= 0.0) && (hgh >= 0.0)) {
      width_ = wdt;
      height_ = hgh;
    }
    else {
      throw std::invalid_argument("Wrong parameters");
    }
}

double Rectangle::getArea() const {
  return(height_ * width_);
}

rectangle_t Rectangle::getFrameRect() const {
  double thisSin = fabs(sin(angle_ * M_PI / 180.0));
  double thisCos = fabs(cos(angle_ * M_PI / 180.0));
  double width = height_ * fabs(thisSin) + width_ * fabs(thisCos);
  double height = height_ * fabs(thisCos) + width_ * fabs(thisSin);
  return rectangle_t{pos_, width, height};
}

void Rectangle::move(const point_t & newCenter) {
  pos_ = newCenter;
}

void Rectangle::move(const double dx, const double dy) {
  pos_.x += dx;
  pos_.y += dy;
}

void Rectangle::scale(double coef) {
  if (coef < 0.0) {
    throw std::invalid_argument("Wrong coefficient");
  }
  else {
    width_ *= coef;
    height_ *= coef;
  }
}

void Rectangle::rotate(double angle) {
  angle_ =+ angle;
  if (angle_ >= 360.0) {
    angle_ = fmod(angle_, 360.0);
  }
}

double Rectangle::getAngle() {
  return angle_;
}
