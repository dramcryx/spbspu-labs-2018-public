#include <cmath>
#include <memory>
#include <iostream>
#include "matrix.hpp"
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
/*#include "../common/matrix.cpp"
#include "../common/rectangle.cpp"
#include "../common/circle.cpp"
#include "../common/composite-shape.cpp"
*/
using namespace zasyadko;

int main()
{
  Rectangle rect({10,10},10,10);
  std::shared_ptr<Shape > rectptr = std::make_shared<Rectangle >(rect);
  
  Circle testCircle({20.0,20.0},10.0);
  std::shared_ptr<Shape > circptr = std::make_shared<Circle >(testCircle);
  
  Circle testCircle2({30.0,30.0},10.0);
  std::shared_ptr<Shape > circptr2 = std::make_shared<Circle >(testCircle2);
  
  Rectangle rect2({10.0,10.0},10,10);
  std::shared_ptr<Shape > rectptr2 = std::make_shared<Rectangle >(rect2);

  CompositeShape comp(circptr2);
  comp.addShape(rectptr2);
  comp.print();
  comp.rotate(45);
  comp.print();
  comp.getShapeInfo(0)->print();
  comp.getShapeInfo(1)->print();
  
  std::shared_ptr<CompositeShape > compptr = std::make_shared<CompositeShape >(comp);
  
  Matrix test(compptr);
  //test.adding(circptr);
  //test.addCompShape(compptr);
  test.printInfo();
  
  return 0;
}
