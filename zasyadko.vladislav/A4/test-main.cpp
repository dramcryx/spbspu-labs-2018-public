#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include "rectangle.hpp"
#include "circle.hpp"
#include "composite-shape.hpp"
#include "matrix.hpp"
#include <memory>
#include <stdexcept>
#include <boost/test/included/unit_test.hpp>

using namespace zasyadko;

const double EPSILON = 0.00001;

BOOST_AUTO_TEST_SUITE(RectangleTesting)
  BOOST_AUTO_TEST_CASE(RectangleRotating)
  {
    Rectangle testRect({10,10},10,10);
    rectangle_t frame = testRect.getFrameRect();
    testRect.rotate(45);
    BOOST_CHECK_CLOSE(testRect.getFrameRect().pos.x,frame.pos.x,EPSILON);
    BOOST_CHECK_CLOSE(testRect.getFrameRect().pos.y,frame.pos.y,EPSILON);
    BOOST_CHECK_CLOSE(testRect.getArea(),frame.height*frame.width,EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CircleTesting)
  BOOST_AUTO_TEST_CASE(CircleRotating)
  {
    Circle testCircle({10,10},10);
    rectangle_t frame = testCircle.getFrameRect();
    testCircle.rotate(45);
    BOOST_CHECK_CLOSE(testCircle.getRadius(),frame.width/2.0,EPSILON);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().pos.x,frame.pos.x,EPSILON);
    BOOST_CHECK_CLOSE(testCircle.getFrameRect().pos.y,frame.pos.y,EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(CompositeShapeTesting)
  BOOST_AUTO_TEST_CASE(CompositeRotating)
  {
    Circle testCircle({30.0,30.0},10.0);
    Rectangle testRect({10.0,10.0},10,10);
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(testRect);
    std::shared_ptr<Shape> circ = std::make_shared<Circle>(testCircle);
    CompositeShape testComp(rect);
    testComp.addShape(circ);
    testComp.rotate(45);
    BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.x,22.5,EPSILON);
    BOOST_CHECK_CLOSE(testComp.getFrameRect().pos.y,21.4644660940672,EPSILON);
    BOOST_CHECK_CLOSE(testComp.getShapeInfo(0)->getFrameRect().pos.x,22.5,EPSILON);
    BOOST_CHECK_CLOSE(testComp.getShapeInfo(0)->getFrameRect().pos.y,4.82233,EPSILON);
    BOOST_CHECK_CLOSE(testComp.getShapeInfo(1)->getFrameRect().pos.x,22.5,EPSILON);
    BOOST_CHECK_CLOSE(testComp.getShapeInfo(1)->getFrameRect().pos.y,33.1066,EPSILON);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(MatrixTesting)
  BOOST_AUTO_TEST_CASE(MatrixAdd)
  {
    Circle testCircle({10,10},10);
    Rectangle testRect({10,10},10,10);
    std::shared_ptr<Shape> rect = std::make_shared<Rectangle>(testRect);
    std::shared_ptr<Shape> circ = std::make_shared<Circle>(testCircle);
    
    Matrix test(rect);
    test.adding(circ);
    
    BOOST_CHECK_EQUAL(test.getSize(),2);
    BOOST_CHECK_EQUAL(test.getRowAmount(),2);
  }
  
  BOOST_AUTO_TEST_CASE(GetShapes)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<Rectangle>(Rectangle({ 10,10 },2, 2));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({ 10,10 },4, 4));
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>(Rectangle({ 10,10 },6, 6));
    Matrix testMatrix(rect1);
    testMatrix.adding(rect2);
    testMatrix.adding(rect3);
    BOOST_CHECK_EQUAL(testMatrix.getShape(0,0),rect1);
    BOOST_CHECK_EQUAL(testMatrix.getShape(1,0),rect2);
    BOOST_CHECK_EQUAL(testMatrix.getShape(2,0),rect3);
  }
  
  BOOST_AUTO_TEST_CASE(CopyConstructorTest)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<Rectangle>(Rectangle({ 10,10 },2, 2));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({ 10,10 },4, 4));
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>(Rectangle({ 10,10 },6, 6));
    Matrix testMatrix(rect1);
    testMatrix.adding(rect2);
    testMatrix.adding(rect3);
    Matrix Matrix2(testMatrix);
    BOOST_CHECK_EQUAL(Matrix2.getRowAmount(),testMatrix.getRowAmount());
    BOOST_CHECK_EQUAL(Matrix2.getSize(),testMatrix.getSize());
    BOOST_CHECK_EQUAL(Matrix2.getShape(0,0),testMatrix.getShape(0,0));
  }

  BOOST_AUTO_TEST_CASE(MoveConstructorTest)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<Rectangle>(Rectangle({ 10,10 },2, 2));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({ 10,10 },4, 4));
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>(Rectangle({ 10,10 },6, 6));
    Matrix testMatrix(rect1);
    testMatrix.adding(rect2);
    testMatrix.adding(rect3);
    Matrix Matrix2(std::move(testMatrix));
    BOOST_CHECK_EQUAL(Matrix2.getRowAmount(),3);
    BOOST_CHECK_EQUAL(Matrix2.getSize(),3);
    BOOST_CHECK_EQUAL(Matrix2.getShape(0,0),rect1);
  }

  BOOST_AUTO_TEST_CASE(CopyOperatorTest)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<Rectangle>(Rectangle({ 10,10 },2, 2));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({ 10,10 },4, 4));
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>(Rectangle({ 10,10 },6, 6));
    Matrix testMatrix(rect1);
    testMatrix.adding(rect2);
    testMatrix.adding(rect3);
    Matrix Matrix2(rect3);
    Matrix2 = testMatrix;
    BOOST_CHECK_EQUAL(Matrix2.getRowAmount(),testMatrix.getRowAmount());
    BOOST_CHECK_EQUAL(Matrix2.getSize(),testMatrix.getSize());
    BOOST_CHECK_EQUAL(Matrix2.getShape(0,0),testMatrix.getShape(0,0));
  }

  BOOST_AUTO_TEST_CASE(MatrixAddCompShape)
  {
  Circle testCircle({30.0,30.0},10.0);
  std::shared_ptr<Shape > circptr = std::make_shared<Circle >(testCircle);
  Rectangle rect({10.0,10.0},10,10);
  std::shared_ptr<Shape > rectptr = std::make_shared<Rectangle >(rect);
  CompositeShape test(circptr);
  test.addShape(rectptr);
  std::shared_ptr<CompositeShape > compptr = std::make_shared<CompositeShape >(test);
  Matrix testMatrix(compptr);
  BOOST_CHECK_EQUAL(testMatrix.getSize(), test.getAmount());
  BOOST_CHECK_EQUAL(testMatrix.getShape(0,0), test.getShapeInfo(0));
  }
  
  BOOST_AUTO_TEST_CASE(MoveOperatorTest)
  {
    std::shared_ptr<Shape> rect1 = std::make_shared<Rectangle>(Rectangle({ 10,10 },2, 2));
    std::shared_ptr<Shape> rect2 = std::make_shared<Rectangle>(Rectangle({ 10,10 },4, 4));
    std::shared_ptr<Shape> rect3 = std::make_shared<Rectangle>(Rectangle({ 10,10 },6, 6));
    Matrix testMatrix(rect1);
    testMatrix.adding(rect2);
    testMatrix.adding(rect3);
    Matrix Matrix2(rect3);
    Matrix2 = std::move(testMatrix);
    BOOST_CHECK_EQUAL(Matrix2.getRowAmount(),3);
    BOOST_CHECK_EQUAL(Matrix2.getSize(),3);
    BOOST_CHECK_EQUAL(Matrix2.getShape(0,0),rect1);
  }
BOOST_AUTO_TEST_SUITE_END()
