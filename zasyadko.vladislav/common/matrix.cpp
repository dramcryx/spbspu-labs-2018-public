#include "matrix.hpp"
#include <cmath>
#include <stdexcept>
#include <iostream>

using namespace zasyadko;

Matrix::Matrix(const std::shared_ptr< Shape > shape):
  shapes_(nullptr),
  size_(0),
  rowAmount_(0),
  amountInLayer_(nullptr)
{
  if(shape == nullptr)
  {
    throw std::invalid_argument("shape is null");
  }
  adding(shape);
}

Matrix::Matrix(const std::shared_ptr <CompositeShape> &compShape):
  shapes_(nullptr),
  size_(0),
  rowAmount_(0),
  amountInLayer_(nullptr)
{
  if(compShape == nullptr)
  {
    throw std::invalid_argument("shape is null");
  }
  addCompShape(compShape);
}

Matrix::Matrix(const Matrix & copymatrix):
  shapes_(new std::shared_ptr<Shape> [copymatrix.size_]),
  size_(copymatrix.size_),
  rowAmount_(copymatrix.rowAmount_),
  amountInLayer_(new int[copymatrix.size_])
{
  for(int i = 0; i < copymatrix.size_; i++)
  {
    shapes_[i] = copymatrix.shapes_[i];
  }
  for(int i = 0; i < copymatrix.rowAmount_; i++)
  {
    amountInLayer_[i] = copymatrix.amountInLayer_[i];
  }
}

Matrix::Matrix(Matrix && movematrix):
  shapes_(nullptr),
  size_(0),
  rowAmount_(0),
  amountInLayer_(nullptr)
{
  if(this != &movematrix)
  {
    shapes_.swap(movematrix.shapes_);
    amountInLayer_.swap(movematrix.amountInLayer_);
    rowAmount_ = movematrix.rowAmount_;
    size_ = movematrix.size_;
    movematrix.size_ = 0;
    movematrix.rowAmount_ = 0;
    movematrix.shapes_.reset();
    movematrix.amountInLayer_.reset();
  }
  else
  {
    throw std::invalid_argument("same matrix");
  }
}

Matrix & Matrix::operator =(const Matrix & copymatrix)
{
  if(this != & copymatrix)
  {
    size_ = copymatrix.size_;
    rowAmount_ = copymatrix.rowAmount_;
    std::unique_ptr< std::shared_ptr< Shape >[] > newshapes(new std::shared_ptr< Shape >[size_]);
    amountInLayer_ = std::unique_ptr<int[]>(new int[rowAmount_]);
    for(int i = 0; i < size_;i++)
    {
      newshapes[i] = copymatrix.shapes_[i];
    }
    shapes_.swap(newshapes);
    for(int i = 0; i < rowAmount_;i++)
    {
      amountInLayer_[i] = copymatrix.amountInLayer_[i];
    }
  }
  return *this;
}

Matrix & Matrix::operator =(Matrix && movematrix)
{
  if(this != &movematrix)
  {
    size_ = movematrix.size_;
    rowAmount_ = movematrix.rowAmount_;
    shapes_.swap(movematrix.shapes_);
    amountInLayer_.swap(movematrix.amountInLayer_);
    movematrix.rowAmount_ = 0;
    movematrix.size_ = 0;
  }
  return *this;
}

int Matrix::getSize() const
{
  return size_;
}

int Matrix::getAmountInLayer(int n) const
{
  if(n>= rowAmount_)
  {
    std::out_of_range("No such layer yet");
  }
  return amountInLayer_[n];
}

int Matrix::getRowAmount() const
{
  return rowAmount_;
}

void Matrix::addCompShape(const std::shared_ptr <CompositeShape> &compShape)
{
  if(compShape->getAmount()==0)
  {
    std::invalid_argument("Composite Shape has nothing in");
  }
  for(int i = 0;i < compShape->getAmount();i++)
  {
    adding((*compShape).getShapeInfo(i));
  }
  }

void Matrix::adding(const std::shared_ptr <Shape> new_shape)
{
  if (new_shape == nullptr)
  {
    throw std::invalid_argument("invalid shape pointer");
  }
  
  std::unique_ptr<std::shared_ptr<Shape>[]> newshapes(new std::shared_ptr<Shape>[size_+1]);
  int position = 0;
  bool overlap = true;
  
  for(int i = 0; i < rowAmount_; i++)
  {
    overlap = false;
    for(int j = 0; j < amountInLayer_[i];j++)
    {
      overlap = overlapCheck(shapes_[position+j],new_shape);
      newshapes[position+j] = shapes_[position+j];
    }
    position+=amountInLayer_[i];
    
    if(!overlap)
    {
      newshapes[position] = new_shape;
      for(int k = position; k < size_; k++)
      {
        newshapes[k+1] = shapes_[k];
      }
      size_++;
      amountInLayer_[i]++;
      break;
    }
  }
  if(overlap)
  {
    newshapes[size_++] = new_shape;
    std::unique_ptr<int[]> temp_amountInLayer(new int[rowAmount_+1]);
    for(int i = 0;i < rowAmount_; i++)
    {
      temp_amountInLayer[i] = amountInLayer_[i];
    }
    temp_amountInLayer[rowAmount_++] = 1;
    amountInLayer_.swap(temp_amountInLayer);
  }
  shapes_.swap(newshapes);
}

bool Matrix::overlapCheck(const std::shared_ptr<Shape> sh1, const std::shared_ptr<Shape> sh2) const
{
  bool status = false;
  rectangle_t rect_1 = sh1->getFrameRect();
  rectangle_t rect_2 = sh2->getFrameRect();
  if (!((rect_1.pos.y - rect_1.height/2) > (rect_2.pos.y + rect_2.height/2) or
     (rect_1.pos.y + rect_1.height/2) < (rect_2.pos.y - rect_2.height/2) or
     (rect_1.pos.x + rect_1.width/2) < (rect_2.pos.x - rect_2.width/2) or
     (rect_1.pos.x - rect_1.width/2) > (rect_2.pos.x + rect_2.width/2)))
    {
      status = true;
    }
  return status;
}

std::shared_ptr<Shape> Matrix::getShape(const int numb, const int column)
{
  if((amountInLayer_[numb] < column)||(rowAmount_ < numb))
  {
    throw std::invalid_argument("index does not exist");
  }
  int index = 0;
  for(int i = 0;i < numb;i++)
  {
    index+=amountInLayer_[i];
  }
  index+=(column);
  return shapes_[index];
}

void Matrix::printInfo()
{
  std::cout << "number of shapes " << size_ << std::endl;
  std::cout << "number of layers " << rowAmount_ << std::endl;
}
