#include <iostream>
#include <stdexcept>
#include "rectangle.hpp"
#include <cmath>

using namespace zasyadko;

Rectangle::Rectangle(const point_t & center, const double & height, const double & width) :
  cntr_(center),
  height_(height),
  width_(width)
{
  if ((width_ < 0.0) || (height_ < 0.0))
  {
    throw std::invalid_argument("Invalid Parameters!");
  }
  vertices[0].x = center.x+ width_/2;
  vertices[0].y = center.y+ height_/2;
  vertices[1].x = center.x- width_/2;
  vertices[1].y = center.y+ height_/2;
  vertices[2].x = center.x- width_/2;
  vertices[2].y = center.y- height_/2;
  vertices[3].x = center.x+ width_/2;
  vertices[3].y = center.y- height_/2;
  
}

void Rectangle::rotate(const double degree)
{ 
  double radians = (degree*M_PI)/180;

  for(int i = 0;i < 4;i++)
  {
    vertices[i].x = cntr_.x + (vertices[i].x - cntr_.x)*cos(radians) - (vertices[i].y - cntr_.y)*sin(radians);
    vertices[i].y = cntr_.y + (vertices[i].y - cntr_.y)*cos(radians) + (vertices[i].x - cntr_.x)*sin(radians);
  }
}

void Rectangle::move(const point_t & Center)
{
  cntr_ = Center;
}

void Rectangle::move(const double add_x, const double add_y)
{
  cntr_.x += add_x;
  cntr_.y += add_y;
}

double Rectangle::getArea() const
{
  return width_*height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  return {cntr_, height_, width_};
}

void Rectangle::print() const
{
  std::cout << "Rectangle center:" << cntr_.x << "; " << cntr_.y << std::endl;
  std::cout << "Rectangle height:" << height_ << std::endl;
  std::cout << "Rectangle width:" << width_ << std::endl;
  std::cout << "Rectangle area:" << getArea() << std::endl;
  for(int i = 0;i < 4;i++)
  {
    std::cout << "dot : "<< i <<" "<<vertices[i].x <<" "<< vertices[i].y<<std::endl;
  }
}        

void Rectangle::scale(const double factor)
{
  if (factor < 0.0)
  {
    throw std::invalid_argument("INVALID FACTOR");
  }
  else
  {
    height_ *= factor;
    width_ *= factor;
  }
}
