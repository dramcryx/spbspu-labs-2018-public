#include "circle.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include <stdexcept>

Circle::Circle(const point_t &c, const double r) :
  radius_(r),
  center_(c)
{
  if (r <= 0.0)
  {
    throw std::invalid_argument("invalid circle parameters!");
  }
}

rectangle_t Circle::getFrameRect() const
{
  rectangle_t rec;
  rec.pos = center_;
  rec.height = 2 * radius_;
  rec.width = 2 * radius_;

  return rec;
}

double Circle::getArea() const
{
  return M_PI*2*pow(radius_,2);
}

void Circle::move(const point_t &newCenter)
{
  center_ = newCenter;
}

void Circle::move(const double dx, const double dy)
{
  center_.x += dx;
  center_.y += dy;
}
