#include <iostream>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

int main() {
  Rectangle rec(2, 3, 200, 300);
  Circle circ({ 2, 3 }, 20);
  Triangle trian{ { 2. , 4. },{ 5. , 9. },{ 8. , 4. } };
  try{
    std::cout << "[Rectangle]" << std::endl;
    std::cout << "Area of Rectangle - " << rec.getArea() << std::endl;
    std::cout << "Pos of Center(x, y) - (" << rec.getFrameRect().pos.x << ", " << rec.getFrameRect().pos.y << ")" << std::endl;
    std::cout << "Size of Rectangle(Width, Height) - (" << rec.getFrameRect().width << ", " << rec.getFrameRect().height << ")" << std::endl;

    point_t P;
    P.x = 10;
    P.y = 15;

    rec.move(P);
    std::cout << "NEW Pos of Center(x, y) - (" << rec.getFrameRect().pos.x << ", " << rec.getFrameRect().pos.y << ")" << std::endl;

    rec.move(5, 5);
    std::cout << "NEW Pos of Center(x, y) - (" << rec.getFrameRect().pos.x << ", " << rec.getFrameRect().pos.y << ")" << std::endl;

    std::cout << "\n--------------------------\n\n";

    std::cout << "[Circle]" << std::endl;

    std::cout << "Area of Circle - " << circ.getArea() << std::endl;
    std::cout << "Pos of Center(x, y) - (" << circ.getFrameRect().pos.x << ", " << circ.getFrameRect().pos.y << ")" << std::endl;

    P.x = 10;
    P.y = 15;

    std::cout << "Pos of Center(x, y) - (" << circ.getFrameRect().pos.x << ", " << circ.getFrameRect().pos.y << ")" << std::endl;

    circ.move(P);
    std::cout << "NEW Pos of Center(x, y) - (" << circ.getFrameRect().pos.x << ", " << circ.getFrameRect().pos.y << ")" << std::endl;

    circ.move(5, 5);
    std::cout << "NEW Pos of Center(x, y) - (" << circ.getFrameRect().pos.x << ", " << circ.getFrameRect().pos.y << ")" << std::endl;

    std::cout << "\n--------------------------\n\n";

    std::cout << "[Triangle]" << std::endl;
    point_t center = trian.getCenter();
    std::cout << "Center : (" << center.x << " , " << center.y << ")" << std::endl;
    std::cout << "Vertexes of triangle : (" << center.x + trian.getRadVects('A').x << " , " << center.y +
        trian.getRadVects('A').y << ") (" << center.x + trian.getRadVects('B').x << " , " << center.y +
        trian.getRadVects('B').y << ") (" << center.x + trian.getRadVects('C').x << " , " << center.y +
        trian.getRadVects('C').y << ")" << std::endl;
    std::cout << "Height of frame rectangle : " << trian.getFrameRect().height << std::endl;
    std::cout << "Width of frame rectangle  : " << trian.getFrameRect().width << std::endl;
    std::cout << "Center of frame rectangle : (" << trian.getFrameRect().pos.x << " , "
        << trian.getFrameRect().pos.y << ")" << std::endl;
    std::cout << "Area   : " << trian.getArea() << std::endl;
    std::cout << std::endl << "{Moving}" << std::endl;

    trian.move({ 3. , 3. });
    center = trian.getCenter();
    std::cout << "...to point : (" << center.x << " , " << center.y << ")" << std::endl;
    std::cout << "Vertexes of triangle now: (" << center.x + trian.getRadVects('A').x << " , " << center.y +
        trian.getRadVects('A').y << ") (" << center.x + trian.getRadVects('B').x << " , " << center.y +
        trian.getRadVects('B').y << ") (" << center.x + trian.getRadVects('C').x << " , " << center.y +
        trian.getRadVects('C').y << ")" << std::endl;
    std::cout << "Center of frame rectangle now : (" << trian.getFrameRect().pos.x << " , "
        << trian.getFrameRect().pos.y << ")" << std::endl << std::endl;

    trian.move(3., 3.);
    center = trian.getCenter();
    std::cout << "...by datum lines : (" << center.x << " , " << center.y << ")" << std::endl;
    std::cout << "Vertexes of triangle now: (" << center.x + trian.getRadVects('A').x << " , " << center.y +
        trian.getRadVects('A').y << ") (" << center.x + trian.getRadVects('B').x << " , " << center.y +
        trian.getRadVects('B').y << ") (" << center.x + trian.getRadVects('C').x << " , " << center.y +
        trian.getRadVects('C').y << ")" << std::endl;
    std::cout << "Center of frame rectangle now : (" << trian.getFrameRect().pos.x << " , "
        << trian.getFrameRect().pos.y << ")" << std::endl << std::endl;
  }
  catch(const std::invalid_argument &e) {
    std::cerr << e.what() <<std::endl;
    return 1;
  }
  return 0;
}
