#include "rectangle.hpp"
#include <stdexcept>

double Rectangle::getArea() const
{
  return size_.width * size_.height;
}

rectangle_t Rectangle::getFrameRect() const
{
  return size_;
}

void Rectangle::move(const point_t &newCenter)
{
  size_.pos = newCenter;
}

void Rectangle::move(const double dx, const double dy)
{
  size_.pos.x += dx;
  size_.pos.y += dy;
}

Rectangle::Rectangle(const double x, const double y, const double h, const double w) :
  width_(w),
  height_(h)
{
  size_.height = height_;
  size_.width = width_;
  size_.pos.x = x;
  size_.pos.y = y;

  if (size_.height <= 0.0 || size_.width <= 0.0)
  {
    throw std::invalid_argument("Init. error: incorrect height or/and width");
  }
}
