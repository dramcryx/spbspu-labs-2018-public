#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "shape.hpp"

class Rectangle : public Shape
{
public:
  Rectangle(const double x, const double y, const double height, const double width);

  double getArea() const override;
  rectangle_t getFrameRect() const override;
  void move(const point_t &newCenter) override;
  void move(const double dx, const double dy) override;

private:
  rectangle_t size_;
  double width_, height_;
};

#endif
