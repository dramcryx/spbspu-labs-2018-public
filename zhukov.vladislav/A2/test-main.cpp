#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include "rectangle.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include <boost/test/included/unit_test.hpp>
#include <stdexcept>
#include <cmath>

using namespace zhukov;

const double EPS = 0.00001;

BOOST_AUTO_TEST_SUITE(Rectangle_Tests)

  BOOST_AUTO_TEST_CASE(Absolute_Move_Test_Rect)
  {
    Rectangle rect({ 11.1, 15.6 }, 10.9, 5.5);
    rect.move( { 53.3, 20.4 } );
    BOOST_CHECK(rect.getFrameRect().pos.x == 53.3 && rect.getFrameRect().pos.y == 20.4);
  }

  BOOST_AUTO_TEST_CASE(Relative_Move_Test_Rect)
  {
    Rectangle rect({ 11.1, 15.6 }, 10.9, 5.5);
    rect.move(-10.1, 0.4);
    BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().pos.x, 1.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(rect.getFrameRect().pos.y, 16.0, EPS);
  }

  BOOST_AUTO_TEST_CASE(Area_Test_Rect)
  {
    Rectangle rect({ 11.1, 15.6 }, 5, 5);
    BOOST_REQUIRE_EQUAL(rect.getArea(), 25);
  }

  BOOST_AUTO_TEST_CASE(Area_Test_With_Move_Rect)
  {
    Rectangle rect({ 11.1, 15.6 }, 5, 5);
    rect.move(10, 5);
    BOOST_REQUIRE_EQUAL(rect.getArea(), 25);
  }

  BOOST_AUTO_TEST_CASE(Frame_Rect_Test_Rect)
  {
    Rectangle rect({ 11.1, 15.6 }, 5, 5);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().width, 5);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().height, 5);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().pos.x, 11.1);
    BOOST_REQUIRE_EQUAL(rect.getFrameRect().pos.y, 15.6);
  }

  BOOST_AUTO_TEST_CASE(Scale_Test_Rect)
  {
    Rectangle rect({ 0, 0 }, 5, 5);
    rect.scale(10);
    BOOST_REQUIRE_EQUAL(rect.getArea(), 25 * 100);
  }

  BOOST_AUTO_TEST_CASE(Inavalid_Argumend_Constructor_Test_Rect)
  {
    BOOST_CHECK_THROW(Rectangle({ 0, 0 }, -1, -1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Argument_Scale_Test_Rect)
  {
    Rectangle rect({ 0, 0 }, 5, 5);
    BOOST_CHECK_THROW(rect.scale(-1), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Circle_Test)

  BOOST_AUTO_TEST_CASE(Absolute_Move_Test_Circle)
  {
    Circle circ( { 10, 0 }, 10);
    circ.move( { 15, 20} );
    BOOST_CHECK(circ.getFrameRect().pos.x == 15 && circ.getFrameRect().pos.y == 20);
  }

  BOOST_AUTO_TEST_CASE(Relative_Move_Test_Circle)
  {
    Circle circ( { 10, 0 }, 10);
    circ.move(-5, 8);
    BOOST_CHECK_CLOSE_FRACTION(circ.getFrameRect().pos.x, 5, EPS);
    BOOST_CHECK_CLOSE_FRACTION(circ.getFrameRect().pos.y, 8, EPS);
  }

  BOOST_AUTO_TEST_CASE(Area_Test_Circle)
  {
    Circle circ( { 10, 0 }, 10);
    BOOST_REQUIRE_EQUAL(circ.getArea(), 100 * M_PI);
  }

  BOOST_AUTO_TEST_CASE(Area_Test_With_Move_Circle)
  {
    Circle circ( { 10, 0 }, 10);
    circ.move(10, 5);
    BOOST_REQUIRE_EQUAL(circ.getArea(), 100 * M_PI);
  }

  BOOST_AUTO_TEST_CASE(Scale_Test_Circle)
  {
    Circle circ( { 10, 0 }, 10);
    circ.scale(2);
    BOOST_REQUIRE_EQUAL(circ.getArea(), 100 * M_PI * 4);
  }

  BOOST_AUTO_TEST_CASE(Frame_Rect_Test_Circle)
  {
    Circle circ( { 3, 0 }, 15);
    BOOST_REQUIRE_EQUAL(circ.getFrameRect().width, 30);
    BOOST_REQUIRE_EQUAL(circ.getFrameRect().height, 30);
    BOOST_REQUIRE_EQUAL(circ.getFrameRect().pos.x, 3);
    BOOST_REQUIRE_EQUAL(circ.getFrameRect().pos.y, 0);
  }

  BOOST_AUTO_TEST_CASE(Inavalid_Argumend_Constructor_Test_Circle)
  {
    BOOST_CHECK_THROW(Circle( { 0, 0 }, -1), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(Invalid_Argument_Scale_Test_Circle)
  {
    Circle circ( { 0, 0 }, 10);
    BOOST_CHECK_THROW(circ.scale(-1), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(testTrian)

  BOOST_AUTO_TEST_CASE(testTrianMove1)
  {
    Triangle trian({ 1.0, 3.0 }, { 5.0, 3.0 }, { 2.0, 4.0 });
    trian.move({ 11.3, 0.7 });
    BOOST_CHECK_EQUAL(trian.getCenter().x, 11.3);
    BOOST_CHECK_EQUAL(trian.getCenter().y, 0.7);
  }

  BOOST_AUTO_TEST_CASE(testTrianMove2)
  {
    Triangle trian({ 1.0, 3.0 }, { 5.0, 3.0 }, { 3.0, 6.0 });
    rectangle_t frame = trian.getFrameRect();
    trian.move(18.1, 5.0);
    BOOST_CHECK_CLOSE_FRACTION(trian.getCenter().x, 21.1, EPS);
    BOOST_CHECK_CLOSE_FRACTION(trian.getCenter().y, 9.0, EPS);
    BOOST_CHECK_CLOSE_FRACTION(trian.getFrameRect().width, frame.width, EPS);
    BOOST_CHECK_CLOSE_FRACTION(trian.getFrameRect().height, frame.height, EPS);
  }

  BOOST_AUTO_TEST_CASE(testTrianMoveArea)
  {
    Triangle trian({ 1.0, 3.0 }, { 5.0, 3.0 }, { 2.0, 4.0 });
    double area = trian.getArea();
    trian.move(18.1, 5.0);
    BOOST_CHECK_CLOSE_FRACTION(area, trian.getArea(), EPS);
  }

 BOOST_AUTO_TEST_CASE(testTrianScaleArea)
  {
    Triangle trian({ 1.0, 3.0 }, { 5.0, 3.0 }, { 2.0, 4.0 });
    double area = trian.getArea();
    trian.scale(2.5);
    BOOST_CHECK_CLOSE_FRACTION(trian.getArea(), area * 2.5 * 2.5, EPS);
  }

  BOOST_AUTO_TEST_CASE(testTrianInitScale)
  {
    Triangle trian({ 1.0, 3.0 }, { 5.0, 3.0 }, { 2.0, 4.0 });
    BOOST_CHECK_THROW(trian.scale(-5), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(testCircInit)
  {
    BOOST_CHECK_THROW(Triangle trian({ 1.0, 3.0 }, { 1.0, 3.0 }, { 1.0, 4.0 }), std::invalid_argument);
  }
BOOST_AUTO_TEST_SUITE_END();
