#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace zhukov
{
  class Circle : public Shape 
  {
  public:
    Circle (const point_t &center, const double rad);
    double getArea() const override;
    rectangle_t getFrameRect() const override;
    void scale(const double coef) override;
    void move(const point_t &point) override;
    void move(const double x_offset, const double y_offset) override;

  private:
    double rad_;
    point_t center_;
  };

}
#endif
