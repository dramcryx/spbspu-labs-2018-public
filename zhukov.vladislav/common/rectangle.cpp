#include "rectangle.hpp"
#include <stdexcept>

using namespace zhukov;

Rectangle::Rectangle(const point_t &center, const double width, const double height) :
  center_(center),
  width_(width),
  height_(height)
{
  if (width_ <= 0.0 || height_ <= 0.0)
  {
    throw std::invalid_argument("Init. error: incorrect height or/and width or/and center");
  }
}

double Rectangle::getArea() const
{
  return width_ * height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  rectangle_t rect;
  rect.width = width_;
  rect.height = height_;
  rect.pos = center_;
  return rect;
}

void Rectangle::move(const point_t &point)
{
  center_ = point;
}

void Rectangle::move(const double x_offset, const double y_offset)
{
  center_.x += x_offset;
  center_.y += y_offset;
}

void Rectangle::scale(const double coef) 
{
  if (coef < 1.0) {
    throw std::invalid_argument("Error: Invalid argument: factor must be > 0.");
  }
  width_ *= coef;
  height_ *= coef;
}
