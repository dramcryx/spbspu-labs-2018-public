#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "base-types.hpp"

namespace zhukov 
{
  class Shape
  {
  public:
    virtual ~Shape() = default;
    virtual double getArea() const = 0;
    virtual rectangle_t getFrameRect() const = 0;
    virtual void scale(const double coef) = 0;
    virtual void move(const point_t &point) = 0;
    virtual void move(const double x1, const double y1) = 0;
  };
}
#endif
