#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP
#include "shape.hpp"

namespace zhukov
{
  class Triangle : public Shape
  {
    public:
      Triangle(const point_t &a, const point_t &b, const point_t &c);
      double getArea() const override;
      rectangle_t getFrameRect() const override;
      point_t getCenter() const;
      void scale(const double coef) override;
      void move(const point_t &point) override;
      void move(const double x_offset, const double y_offset) override;

    private:
      point_t a_, b_, c_, center;
      rectangle_t size_;
  };
}
#endif
