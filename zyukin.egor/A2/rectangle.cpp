#include "rectangle.hpp"
#include <stdexcept>
#include "base-types.hpp"

zyukin::Rectangle::Rectangle(const point_t &center, double width, double height):
  rectangle_({ center, width, height })
{
  if(width < 0.0 || height < 0.0)
  {
    throw std::invalid_argument("Invalid Rectangle parameters!");
  }
}

double zyukin::Rectangle::getArea() const
{
  return rectangle_.width * rectangle_.height;
}

zyukin::rectangle_t zyukin::Rectangle::getFrameRect() const
{
  return rectangle_;
}

void zyukin::Rectangle::move(double dx, double dy)
{
  rectangle_.pos.x += dx;
  rectangle_.pos.y += dy;
}

void zyukin::Rectangle::move(const point_t &p)
{
  rectangle_.pos = p;
}

void zyukin::Rectangle::scale(double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("Wrong Rectangle scale coefficient!");
  }
  rectangle_.height *= ratio;
  rectangle_.width *= ratio;
}
