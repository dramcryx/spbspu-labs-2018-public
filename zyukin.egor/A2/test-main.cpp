#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <stdexcept>
#include <boost/test/included/unit_test.hpp>
#include "circle.hpp"
#include "rectangle.hpp"
#include "triangle.hpp"

const double EPSILON = 1e-7;

BOOST_AUTO_TEST_SUITE(Rectangle)
  BOOST_AUTO_TEST_CASE(MovingToPoint)
  {
    zyukin::Rectangle r ({ 20, 30 }, 5, 6);
    double widthBefore = r.getFrameRect().width;
    double heightBefore = r.getFrameRect().height;
    r.move ({ 44.31, 18.19 });
    BOOST_CHECK_EQUAL(widthBefore, r.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, r.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(RelativeMoving)
  {
    zyukin::Rectangle r ({ 20, 30 }, 5, 6);
    double widthBefore = r.getFrameRect().width;
    double heightBefore = r.getFrameRect().height;
    r.move (5, 10);
    BOOST_CHECK_EQUAL(widthBefore, r.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, r.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingToPointArea)
  {
    zyukin::Rectangle r ({ 20, 30 }, 5, 6);
    double areaBefore = r.getArea();
    r.move ({5, 10});BOOST_CHECK_EQUAL(areaBefore, r.getArea());
  }

  BOOST_AUTO_TEST_CASE(MovingOnPointArea)
  {
    zyukin::Rectangle r ({ 20, 30 }, 5, 6);
    double areaBefore = r.getArea();
    r.move ( 44.31, 18.19 );
    BOOST_CHECK_EQUAL(areaBefore, r.getArea());
  }

  BOOST_AUTO_TEST_CASE(Scaling)
  {
    const double ratio = 3.3;
    zyukin::Rectangle r ({ 20, 30 }, 5, 6);
    double areaBeforeScaling = r.getArea();
    r.scale(ratio);
    BOOST_CHECK_CLOSE(ratio * ratio * areaBeforeScaling, r.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
  {
    zyukin::Rectangle r ({ 20, 30 }, 5, 6);
    BOOST_CHECK_THROW (r.scale(-5.0), std::invalid_argument);
  }

  BOOST_AUTO_TEST_CASE(InvalidConstructorParameters)
  {
    BOOST_CHECK_THROW( zyukin::Rectangle r ({ 20, 30 }, 5, -6), std::invalid_argument );
    BOOST_CHECK_THROW( zyukin::Rectangle r ({ 20, 30 }, -5, 6), std::invalid_argument );
    BOOST_CHECK_THROW( zyukin::Rectangle r ({ 20, 30 }, -5, -6), std::invalid_argument );
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Circle)

  BOOST_AUTO_TEST_CASE(MovingToPoint)
  {
    zyukin::Circle circle ({ 14, 18 }, 3);
    double widthBefore = circle.getFrameRect().width;
    double heightBefore = circle.getFrameRect().height;
    circle.move ({ 11.31, 10 });
    BOOST_CHECK_EQUAL(widthBefore, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, circle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(RelativeMoving)
  {
    zyukin::Circle circle ({ 14, 18 }, 3);
    double widthBefore = circle.getFrameRect().width;
    double heightBefore = circle.getFrameRect().height;
    circle.move (5.5, 21.1);
    BOOST_CHECK_EQUAL(widthBefore, circle.getFrameRect().width);
    BOOST_CHECK_EQUAL(heightBefore, circle.getFrameRect().height);
  }

  BOOST_AUTO_TEST_CASE(MovingToPointArea)
  {
    zyukin::Circle circle ({ 14, 18 }, 3);
    double areaBefore = circle.getArea();
    circle.move ({ 11.31, 10 });
    BOOST_CHECK_EQUAL(areaBefore, circle.getArea());
  }

  BOOST_AUTO_TEST_CASE(RelativeMovingArea)
  {
    zyukin::Circle circle ({ 14, 18 }, 3);
    double areaBefore = circle.getArea();
    circle.move (5.5, 21.1);
    BOOST_CHECK_EQUAL(areaBefore, circle.getArea());
  }

  BOOST_AUTO_TEST_CASE(Scaling)
  {
    const double ratio = 3.7;

    zyukin::Circle circle ({ 14, 18 }, 3);
    double areaBeforeScaling = circle.getArea();
    circle.scale(ratio);
    BOOST_CHECK_CLOSE(ratio * ratio * areaBeforeScaling, circle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(InvalidScaleParameter)
  {
    zyukin::Circle circle ({ 14, 18 }, 3);
  }

  BOOST_AUTO_TEST_CASE(InvalidConstructorParameters)
  {
    BOOST_CHECK_THROW( zyukin::Circle circle ({ 14, 18 }, -3), std::invalid_argument );
  }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(TriangleTest)

  BOOST_AUTO_TEST_CASE(RelativeMoving)
  {
    zyukin::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move( 5.0, 5.0 );
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(MovingOnPoint)
  {
    zyukin::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move({5.0, 5.0});
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(MovingOnPointArea)
  {
    zyukin::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move({1.0, 1.0});
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(MovingToPointArea)
  {
    zyukin::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double triangleWidthBeforeMove = triangle.getFrameRect().width;
    double triangleHeightBeforeMove = triangle.getFrameRect().height;
    double triangleAreaBeforeMove = triangle.getArea();
    triangle.move(1.0, 1.0);
    BOOST_CHECK_CLOSE(triangleWidthBeforeMove, triangle.getFrameRect().width, EPSILON);
    BOOST_CHECK_CLOSE(triangleHeightBeforeMove, triangle.getFrameRect().height, EPSILON);
    BOOST_CHECK_CLOSE(triangleAreaBeforeMove, triangle.getArea(), EPSILON);
  }

  BOOST_AUTO_TEST_CASE(Scaling)
  {
    zyukin::Triangle triangle({ 0.0, 0.0 }, { 6.0, 12.0 }, { 12.0, 0.0 });
    double  k = 2.0;
    double triangleWidthBeforeScale = triangle.getFrameRect().width;
    double triangleHeightBeforeScale = triangle.getFrameRect().height;
    double triangleAreaBeforeScale = triangle.getArea();
    triangle.scale(k);
    BOOST_CHECK_CLOSE(k * k * triangleAreaBeforeScale, triangle.getArea(), EPSILON);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().width, k * triangleWidthBeforeScale, EPSILON);
    BOOST_CHECK_CLOSE(triangle.getFrameRect().height, k * triangleHeightBeforeScale, EPSILON);
    BOOST_CHECK_THROW(triangle.scale(-2), std::invalid_argument);
  }

BOOST_AUTO_TEST_SUITE_END()
