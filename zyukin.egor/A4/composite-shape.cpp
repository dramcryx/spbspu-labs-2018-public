#include "composite-shape.hpp"
#include <stdexcept>
#include <cmath>

zyukin::CompositeShape::CompositeShape(const std::shared_ptr<zyukin::Shape> shape_ptr):
  shapes_(nullptr),
  size_(0),
  alpha_(0.0)

{
  if (shape_ptr == nullptr)
  {
    throw std::invalid_argument("Error. Null pointer.");
  }

  addShape(shape_ptr);
}

zyukin::CompositeShape::CompositeShape(const zyukin::CompositeShape & obj):
  size_(obj.size_),
  alpha_(obj.alpha_)
{
  shapes_ = std::unique_ptr<std::shared_ptr<zyukin::Shape>[]>(new std::shared_ptr<zyukin::Shape>[size_]);
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i] = obj.shapes_[i];
  }
}

zyukin::CompositeShape::CompositeShape(zyukin::CompositeShape && obj):
  shapes_(std::move(obj.shapes_)),
  size_(obj.size_),
  alpha_(obj.alpha_)
{
  obj.size_ = 0;
  obj.shapes_ = nullptr;
}

zyukin::CompositeShape &zyukin::CompositeShape::operator =(const zyukin::CompositeShape & obj)
{
  if (this != &obj)
  {
    size_ = obj.size_;
    shapes_.reset(new std::shared_ptr<zyukin::Shape>[size_]);
    for (size_t i = 0; i < size_; i++)
    {
      shapes_[i] = obj.shapes_[i];
    }
  }
  return *this;
}

zyukin::CompositeShape &zyukin::CompositeShape::operator =(zyukin::CompositeShape && obj)
{
 if (this == &obj)
 {
   return *this;
 }

 size_ = (obj.size_);
 shapes_ = std::move(obj.shapes_);
 alpha_ = (obj.alpha_);

 obj.size_ = 0;
 obj.shapes_ = nullptr;
 obj.alpha_ = 0.0;

 return *this;
}

std::shared_ptr<zyukin::Shape> zyukin::CompositeShape::operator [](size_t index)
{
  if(index >= size_)
  {
    throw std::out_of_range("CompositeShape index out of range");
  }

  return shapes_[index];
}

void zyukin::CompositeShape::addShape(std::shared_ptr <zyukin::Shape> shape_ptr)
{
  if (shape_ptr == nullptr)
  {
    throw std::invalid_argument("Error. Null pointer.");
  }

  std::unique_ptr<std::shared_ptr <zyukin::Shape>[]> local_array(new std::shared_ptr<zyukin::Shape>[size_+1]);
  for (size_t i = 0; i < size_; i++)
  {
    local_array[i] = shapes_[i];
  }
  local_array[size_++] = shape_ptr;
  shapes_.swap(local_array);
}

void zyukin::CompositeShape::deleteShape(const size_t index)
{
  if (index >= size_)
  {
    throw std::invalid_argument("Error. Index is out of range.");
  }

  if (size_ == 0)
  {
    throw std::invalid_argument("Error. Container of shape is empty.");
  }

  std::unique_ptr<std::shared_ptr<Shape>[]> local_array(new std::shared_ptr<zyukin::Shape>[size_ - 1]);
  for (size_t i = 0; i < index; i++)
  {
    local_array[i] = shapes_[i];
  }
  for (size_t i = index; i < size_ - 1; i++)
  {
    local_array[i] = shapes_[i + 1];
  }

  shapes_.swap(local_array);
  --size_;
}

double zyukin::CompositeShape::getArea() const
{
  double area = 0.0;
  for (size_t i = 0; i < size_; i++)
  {
    area += shapes_[i] -> getArea();
  }
  return area;
}

zyukin::rectangle_t zyukin::CompositeShape::getFrameRect() const
{
  rectangle_t frameRect = shapes_[0] -> getFrameRect();
  double maxX = frameRect.pos.x + frameRect.width / 2;
  double minX = frameRect.pos.x - frameRect.width / 2;
  double maxY = frameRect.pos.y + frameRect.height / 2;
  double minY = frameRect.pos.y - frameRect.height / 2;
  for (size_t i = 1; i < size_; i++)
  {
    frameRect = shapes_[i] -> getFrameRect();
    if (maxX < frameRect.pos.x + frameRect.width / 2)
    {
      maxX = frameRect.pos.x + frameRect.width / 2;
    }
    if (minX > frameRect.pos.x - frameRect.width / 2)
    {
      minX = frameRect.pos.x - frameRect.width / 2;
    }
    if (maxY < frameRect.pos.y + frameRect.height / 2)
    {
      maxY = frameRect.pos.y + frameRect.height / 2;
    }
    if (minY > frameRect.pos.y - frameRect.height / 2)
    {
      minY = frameRect.pos.y - frameRect.height / 2;
    }
  }
  return {{(maxX + minX) / 2, (maxY + minY) / 2}, maxX - minX, maxY - minY};
}

void zyukin::CompositeShape::move(double dx, double dy)
{
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i] -> move(dx, dy);
  }
}

void zyukin::CompositeShape::move(const zyukin::point_t &point)
{
  point_t center = getFrameRect().pos;
  double dx = point.x - center.x;
  double dy = point.y - center.y;
  for (size_t i = 0; i < size_; i++)
  {
    shapes_[i] -> move(dx, dy);
  }
}

void zyukin::CompositeShape::scale(const double ratio)
{
  if (ratio < 0.0)
  {
    throw std::invalid_argument("coefficient should be not negative");
  }
  point_t comp_shape_pos = getFrameRect().pos;
  for (size_t i =0; i < size_; i++)
  {
    point_t shape_pos = shapes_[i] -> getFrameRect().pos;
    shapes_[i] -> move((ratio-1) * (shape_pos.x - comp_shape_pos.x), (ratio-1) * (shape_pos.y - comp_shape_pos.y));
    shapes_[i] -> scale(ratio);
  }
}

void zyukin::CompositeShape::rotate(double angle)
{
  point_t centre = CompositeShape::getFrameRect().pos;
  const double sin_a = std::sin((angle * M_PI) / 180);
  const double cos_a = std::cos((angle * M_PI) / 180);
  for (size_t i = 0; i < size_; i++)
  {
    point_t centre_shape = shapes_[i]->getFrameRect().pos;
    shapes_[i]->move({ centre.x + (centre_shape.x - centre.x) * cos_a - (centre_shape.y - centre.y) * sin_a,
                      centre.y + (centre_shape.y - centre.y) * cos_a + (centre_shape.x - centre.x) * sin_a });
    shapes_[i]->rotate(angle);
  }
}
