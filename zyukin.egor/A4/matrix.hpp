#ifndef A3_MATRIX_HPP
#define A3_MATRIX_HPP
#include "shape.hpp"
#include "composite-shape.hpp"
#include <memory>

namespace zyukin
{
  class Matrix
  {
  public:
    Matrix();
    Matrix(const std::shared_ptr<zyukin::Shape> shape_ptr);
    Matrix(const zyukin::Matrix & obj);
    Matrix(zyukin::Matrix && obj);
    ~Matrix();

    Matrix &operator =(const zyukin::Matrix & obj);
    Matrix &operator =(zyukin::Matrix && obj);
    std::unique_ptr<std::shared_ptr<zyukin::Shape>[]> operator [](const size_t index);

    void addShape(const std::shared_ptr <zyukin::Shape> shape_ptr);

  private:
    std::unique_ptr<std::shared_ptr<zyukin::Shape>[]> matrix_;
    size_t layersNumber_;
    size_t layersSize_;

    bool checkOverlapping(const int index, std::shared_ptr<zyukin::Shape> obj) const;
  };
}

#endif
